#include "../INC/LinearSystemSolver.h"

void LinearSystemSolver::PostMatrix(const Mat& A, const std::string& title)
{
	std::string filename = title + TO_STRING(MYRANK()) +".txt";
	PetscInt first_row, last_row;
	MatGetOwnershipRange(A, &first_row, &last_row);
	PetscInt ncols;
	int_t number = 1;
	int_t nContent = 0;
	const PetscInt* cols;
	const PetscScalar* vals;
	for (int_t irow = 0; irow < last_row - first_row; irow++)
	{
		MatGetRow(A, irow + first_row, &ncols, &cols, &vals);
		nContent += ncols;
		MatRestoreRow(A, irow + first_row, &ncols, &cols, &vals);
	}
	std::ofstream file;
	file.open(filename, std::ios::trunc);
	if (file.is_open())
	{
		file << nContent << std::endl;
		for (int_t irow = 0; irow < last_row - first_row; irow++)
		{
			MatGetRow(A, irow + first_row, &ncols, &cols, &vals);
			for (int_t icolumn = 0; icolumn < ncols; icolumn++)
			{
				file << number++ << "\t" << irow + first_row << "\t" << cols[icolumn] << "\t" << vals[icolumn] << std::endl;
			}
			MatRestoreRow(A, irow + first_row, &ncols, &cols, &vals);
		}			
		file.close();
	}
	else MASTER_ERROR("file cannot be opened");
}

void LinearSystemSolver::PostVector(const Vec& x, const std::string& title)
{
	std::string filename = title + TO_STRING(MYRANK()) + ".txt";
	PetscInt first_row, last_row;
	VecGetOwnershipRange(x, &first_row, &last_row);
	int_t number = 1;
	const PetscScalar* vals;
	std::ofstream file;
	file.open(filename, std::ios::trunc);
	if (file.is_open())
	{
		file << last_row - first_row << std::endl;
		VecGetArrayRead(x, &vals);
		for (int_t irow = 0; irow < last_row - first_row; irow++)
			file << number++ << "\t" << irow + first_row << "\t" << vals[irow] << std::endl;
		VecRestoreArrayRead(x, &vals);
	}
	else MASTER_ERROR("file cannot be opened");
}