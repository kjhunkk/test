
#include "../INC/BGEquation.h"

BGEquation::BGEquation()
{
	num_states_ = 1;
	variable_names_.push_back("var");

	setName("Burgers");
	Equation::getInstance().addRegistry(getName(), this);
}
void BGEquation::setBoundaries(const std::vector<int_t>& tag_numbers)
{
	int_t num_bdries = tag_numbers.size();
	boundaries_.resize(num_bdries);
	for (int_t ibdry = 0; ibdry < num_bdries; ibdry++){
		const std::string& type = BoundaryCondition::getInstance().getBCType(tag_numbers[ibdry]);
		const std::string& name = BoundaryCondition::getInstance().getBCName(tag_numbers[ibdry]);
		const std::vector<real_t>& references = BoundaryCondition::getInstance().getBCReferences(tag_numbers[ibdry]);

		boundaries_[ibdry] = BoundaryFactory<BGBoundary>::getInstance().getBoundary(type, tag_numbers[ibdry], name, references);
		boundaries_[ibdry]->setFlux(flux_);
	}
}

BGConstant::BGConstant()
{
	readData("BGEquation", INPUT() + "equation.dat");
	setName("Constant");
	BGInitializer::getInstance().addRegistry(getName(), this);
}
void BGConstant::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	while (getline(infile, text)){
		if (text.find("~>dimension_", 0) != std::string::npos)
			infile >> dimension_;
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}
const std::vector<real_t> BGConstant::Problem(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 1.0);
	return results;
}
const std::vector<real_t> BGConstant::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 1.0);
	return results;
}

BGDoubleSine::BGDoubleSine()
{
	readData("BGEquation", INPUT() + "equation.dat");
	setName("DoubleSine");
	BGInitializer::getInstance().addRegistry(getName(), this);
}
void BGDoubleSine::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	while (getline(infile, text)){
		if (text.find("~>dimension_", 0) != std::string::npos)
			infile >> dimension_;
		if (text.find("~>sine_wavelength_", 0) != std::string::npos){
			sine_wavelength_.resize(3);
			infile >> sine_wavelength_[0] >> sine_wavelength_[1] >> sine_wavelength_[2];
		}
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}
const std::vector<real_t> BGDoubleSine::Problem(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 1.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		for (int_t idim = 0; idim < dimension_; idim++){
			if (std::abs(sine_wavelength_[idim]) < 1E-10) continue;
			results[ipoint] *= std::sin(2 * 3.14159265358979323846264338327950288*coord[ipoint*dimension_ + idim] / sine_wavelength_[idim]);
		}
	}
	return results;
}
const std::vector<real_t> BGDoubleSine::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 1.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		for (int_t idim = 0; idim < dimension_; idim++) {
			if (std::abs(sine_wavelength_[idim]) < 1E-10) continue;
			results[ipoint] *= std::sin(2 * 3.14159265358979323846264338327950288*coord[ipoint*dimension_ + idim] / sine_wavelength_[idim]);
		}
	}
	return results;
}

BGUserDefine::BGUserDefine()
{
	readData("BGEquation", INPUT() + "equation.dat");
	setName("UserDefine");
	BGInitializer::getInstance().addRegistry(getName(), this);
}
void BGUserDefine::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	while (getline(infile, text)){
		if (text.find("~>dimension_", 0) != std::string::npos)
			infile >> dimension_;
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}
const std::vector<real_t> BGUserDefine::Problem(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 0.0);
	return results;
}
const std::vector<real_t> BGUserDefine::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 0.0);
	return results;
}