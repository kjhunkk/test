#include "../INC/LinearSystemGMRES.h"

LinearSystemGMRES::LinearSystemGMRES()
{
	setName("GMRES");
	LinearSystemSolver::getInstance().addRegistry(getName(), this);

	// GMRES condition
	GMRES_max_iter_ = 30;
	pc_side_ = PC_RIGHT;
	pc_type_ = PCILU; // PETSC_DEFAULT = PCILU
	rel_tol = 1.0e-10; // relative tolerance : PETSC_DEFAULT = 1.0e-5
	abs_tol = 1.0e-50; // absolute tolerance : PETSC_DEFAULT = 1.0e-50
	div_tol = 1.0e50; // divergence tolerance : PETSC_DEFAULT = 1.0e5
	Krylov_max_iter = 10000; // maximum iterations : PETSC_DEFAULT = 10000

	// Krylov create
	ierr_ = KSPCreate(MPI_COMM_WORLD, &ksp_);
}

LinearSystemGMRES::~LinearSystemGMRES()
{
	ierr_ = KSPDestroy(&ksp_);
}

void LinearSystemGMRES::Initialize()
{
	const Config& config = Config::getInstance();

	// Krylov solver
	ierr_ = KSPSetFromOptions(ksp_);
	ierr_ = KSPSetType(ksp_, KSPGMRES);
	if (config.getNonzeroGuess().compare("yes") == 0)
	{
		ierr_ = KSPSetInitialGuessNonzero(ksp_, PETSC_TRUE);
		nonzero_guess_ = PETSC_TRUE;
	}
		
	ierr_ = KSPGMRESSetRestart(ksp_, GMRES_max_iter_);
	ierr_ = KSPGMRESSetOrthogonalization(ksp_, KSPGMRESModifiedGramSchmidtOrthogonalization);

	// Preconditioner
	ierr_ = KSPGetPC(ksp_, &pc_);
	ierr_ = KSPSetPCSide(ksp_, pc_side_);
	if (pc_type_ != PCILU)
	{	ierr_ = PCSetType(pc_, pc_type_);	}

	// Tolerance
	ierr_ = KSPSetTolerances(ksp_, rel_tol, abs_tol, div_tol, Krylov_max_iter);
}

PetscErrorCode LinearSystemGMRES::Solve(const Mat& A, const Vec& b, Vec& solution)
{
	ierr_ = KSPSetOperators(ksp_, A, A); CHKERRQ(ierr_);
	ierr_ = KSPSolve(ksp_, b, solution); CHKERRQ(ierr_);
	ierr_ = KSPGetIterationNumber(ksp_, &iteration_);
	return ierr_;
}