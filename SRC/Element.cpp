
#include "../INC/Element.h"

ElementFactory::ElementFactory() {
	registry_.resize(static_cast<size_t>(ElemType::NUM_OF_ELEMENT));
}

ElementFactory::~ElementFactory() {
	for (auto&& element : registry_){
		if (element != static_cast<Element*>(0)){
			delete element;
		}
	}
}

ElementFactory& ElementFactory::getInstance() { static ElementFactory instance; return instance; }

Element* ElementFactory::getElement(ElemType element_type) {
	if (element_type >= ElemType::NUM_OF_ELEMENT){
		return static_cast<Element*>(0);
	}
	if (registry_[static_cast<int_t>(element_type)] == static_cast<Element*>(0)) registry_[static_cast<int_t>(element_type)] = newElement(element_type);
	return registry_[static_cast<int_t>(element_type)];
}

Element* ElementFactory::newElement(ElemType element_type) {
	switch (element_type) {
	case ElemType::LINE: return new LineElement();
	case ElemType::TRIS: return new TrisElement();
	case ElemType::QUAD: return new QuadElement();
	case ElemType::TETS: return new TetsElement();
	case ElemType::HEXA: return new HexaElement();
	case ElemType::PRIS: return new PrisElement();
	case ElemType::PYRA: return new PyraElement();
	}
	return static_cast<Element*>(0);
}

// ElementMath functions
real_t Element::ElementMath::Normalization(std::vector<real_t>& vec)
{
	real_t length = Length(vec);
	for (int_t i = 0; i < vec.size(); i++)
		vec[i] /= length;
	return length;
}
real_t Element::ElementMath::Length(const std::vector<real_t>& vec)
{
	return std::sqrt(DotProduct(vec, vec));
}
real_t Element::ElementMath::DotProduct(const std::vector<real_t>& vec1, const std::vector<real_t>& vec2)
{
	real_t result = 0;
	for (int_t i = 0; i < vec1.size(); i++)
		result += vec1[i] * vec2[i];
	return result;
}
const std::vector<real_t> Element::ElementMath::CrossProduct(const std::vector<real_t>& vec1, const std::vector<real_t>& vec2)
{
	std::vector<real_t> result;
	if (vec1.size() == 2){
		result.push_back(vec1[0] * vec2[1] - vec1[1] * vec2[0]);
	}
	else if (vec1.size() == 3){
		result.push_back(vec1[1] * vec2[2] - vec1[2] * vec2[1]);
		result.push_back(vec1[2] * vec2[0] - vec1[0] * vec2[2]);
		result.push_back(vec1[0] * vec2[1] - vec1[1] * vec2[0]);
	}
	return result;
}
real_t Element::ElementMath::TrisFaceArea(const std::vector<real_t>& coords, const int_t dimension)
{
	std::vector<real_t> v12(dimension);
	std::vector<real_t> v13(dimension);
	for (int_t i = 0; i < dimension; i++){
		v12[i] = coords[dimension + i] - coords[i];
		v13[i] = coords[2 * dimension + i] - coords[i];
	}
	return 0.5*Length(CrossProduct(v12, v13));
}
real_t Element::ElementMath::FaceArea(const std::vector<real_t>& coords, const int_t dimension)
{
	real_t area = 0.0;
	std::vector<real_t> local_coords;
	switch (dimension){
	case 1: return 1.0;
	case 2:
		local_coords = { coords[2] - coords[0], coords[3] - coords[1] };
		return Length(local_coords);
	case 3:
		local_coords.resize(9);
		for (int_t idim = 0; idim < dimension; idim++)
			local_coords[idim] = coords[idim];
		for (int_t i = 1; i < coords.size() / dimension - 1; i++){
			for (int_t idim = 0; idim < dimension; idim++){
				local_coords[dimension + idim] = coords[dimension*i + idim];
				local_coords[2 * dimension + idim] = coords[dimension*(i + 1) + idim];
			}
			area += TrisFaceArea(local_coords, dimension);
		}
	}
	return area;
}
const std::vector<real_t> Element::ElementMath::NormalVector(const std::vector<real_t>& coords, const std::vector<real_t>& inside, const int_t dimension)
{
	std::vector<real_t> normal(dimension, 0.0);
	std::vector<real_t> direction(dimension, 0.0);
	for (int_t i = 0; i < dimension; i++)
		direction[i] = coords[i] - inside[i];
	if (dimension == 2){
		normal[0] = coords[3] - coords[1];
		normal[1] = coords[0] - coords[2];
	}
	else{
		std::vector<real_t> vec1(dimension, 0.0);
		std::vector<real_t> vec2(dimension, 0.0);
		for (int_t i = 0; i < dimension; i++){
			vec1[i] = coords[2 * dimension + i] - coords[i];
			vec2[i] = coords[1 * dimension + i] - coords[i];
		}
		normal = CrossProduct(vec1, vec2);
	}
	Normalization(normal);
	if (DotProduct(direction, normal) < 0.0)
	for (int_t i = 0; i < 3; i++)
		normal[i] = -normal[i];
	return normal;
}
const std::vector<std::vector<int_t>> Element::ElementMath::GenerateFacetype(const std::vector<int_t>& node_index)
{
	std::vector<std::vector<int_t>> face_type;
	std::vector<int_t> face_type_temp = node_index;
	// forward direction
	for (int_t i = 0; i < node_index.size(); i++){
		face_type.push_back(face_type_temp);
		int_t temp = face_type_temp[0];
		face_type_temp.erase(face_type_temp.begin());
		face_type_temp.push_back(temp);
	}

	if (node_index.size() == 2) return face_type;

	for (int_t i = 0; i < node_index.size(); i++)
		face_type_temp[i] = node_index[node_index.size() - i - 1];
	//reverse direction
	for (int_t i = 0; i < node_index.size(); i++){
		face_type.push_back(face_type_temp);
		int_t temp = face_type_temp[0];
		face_type_temp.erase(face_type_temp.begin());
		face_type_temp.push_back(temp);
	}
	return face_type;
}
bool Element::ElementMath::isPlane(const std::vector<real_t>& face_coords, const real_t* coord, const int_t dimension)
{
	if (dimension == 2){
		std::vector<real_t> vec1 = { face_coords[0] - coord[0], face_coords[1] - coord[1] };
		std::vector<real_t> vec2 = { face_coords[2] - coord[0], face_coords[3] - coord[1] };
		if (std::abs(CrossProduct(vec1, vec2)[0]) < 1.0E-10) return true;
	}
	else if(dimension == 3){
		std::vector<real_t> vec1 = { face_coords[3] - face_coords[0], face_coords[4] - face_coords[1], face_coords[5] - face_coords[2] };
		std::vector<real_t> vec2 = { face_coords[6] - face_coords[0], face_coords[7] - face_coords[1], face_coords[8] - face_coords[2] };
		std::vector<real_t> vec3 = { coord[0] - face_coords[0], coord[1] - face_coords[1], coord[2] - face_coords[2] };
		if (std::abs(DotProduct(vec3, CrossProduct(vec1, vec2))) < 1.0E-10) return true;
	}
	return false;
}
bool Element::ElementMath::isSamePoint(const real_t* coord1, const real_t* coord2, const int_t dimension)
{
	for (int_t idim = 0; idim < dimension; idim++){
		if (std::abs(coord1[idim] - coord2[idim]) > 1.0E-10) return false;
	}
	return true;
}
const std::vector<real_t> Element::ElementMath::LineTransformation(const std::vector<real_t>& line_coords, const real_t* coord)
{
	std::vector<real_t> result(2);
	result[0] = 0.5*(line_coords[0] + line_coords[2]) + 0.5**coord*(-line_coords[0] + line_coords[2]);
	result[1] = 0.5*(line_coords[1] + line_coords[3]) + 0.5**coord*(-line_coords[1] + line_coords[3]);
	return result;
}
const std::vector<real_t> Element::ElementMath::TrisTransformation(const std::vector<real_t>& tris_coords, const real_t* coord)
{
	arma::mat xyz(3, 3);
	xyz << tris_coords[0] << tris_coords[3] << tris_coords[6] << arma::endr
		<< tris_coords[1] << tris_coords[4] << tris_coords[7] << arma::endr
		<< tris_coords[2] << tris_coords[5] << tris_coords[8] << arma::endr;
	arma::mat rs(3, 3);
	rs << 0.0 << -0.5 << -0.5 << arma::endr
		<< 0.5 << 0.5 << 0.0 << arma::endr
		<< 0.5 << 0.0 << 0.5 << arma::endr;
	arma::mat trans(3, 3);
	trans = xyz*rs;
	arma::mat pt_rs(3, 1);
	arma::mat pt_xyz(3, 1);
	pt_rs << 1.0 << arma::endr << coord[0] << arma::endr << coord[1] << arma::endr;
	pt_xyz = trans*pt_rs;
	std::vector<real_t> result = { pt_xyz(0, 0), pt_xyz(1, 0), pt_xyz(2, 0) };
	return result;
}
const std::vector<real_t> Element::ElementMath::QuadTransformation(const std::vector<real_t>& quad_coords, const real_t* coord)
{
	arma::mat xyz(3, 4);
	xyz << quad_coords[0] << quad_coords[3] << quad_coords[6] << quad_coords[9] << arma::endr
		<< quad_coords[1] << quad_coords[4] << quad_coords[7] << quad_coords[10] << arma::endr
		<< quad_coords[2] << quad_coords[5] << quad_coords[8] << quad_coords[11] << arma::endr;
	arma::mat rs(4, 4);
	rs << 0.25 << -0.25 << -0.25 << 0.25 << arma::endr
		<< 0.25 << 0.25 << -0.25 << -0.25 << arma::endr
		<< 0.25 << 0.25 << 0.25 << 0.25 << arma::endr
		<< 0.25 << -0.25 << 0.25 << -0.25 << arma::endr;
	arma::mat trans(3, 4);
	trans = xyz*rs;
	arma::mat pt_rs(4, 1);
	arma::mat pt_xyz(3, 1);
	pt_rs << 1.0 << arma::endr << coord[0] << arma::endr << coord[1] << arma::endr << coord[0] * coord[1] << arma::endr;
	pt_xyz = trans*pt_rs;
	std::vector<real_t> result = { pt_xyz(0, 0), pt_xyz(1, 0), pt_xyz(2, 0) };
	return result;
}


LineElement::LineElement()
{
	// intrincis data
	dimension_ = 1;
	element_name_ = "LINE";
	element_type_ = ElemType::LINE;
	element_order_supp_ = 6;
	num_faces_ = 2;
	face_nodes_P1_ = { { 0 }, { 1 } };

	//read data
	num_nodes_Pn_.resize(element_order_supp_);
	node_coords_Pn_.resize(element_order_supp_);
	std::ifstream infile;
	for (int_t iorder = 0; iorder < element_order_supp_; iorder++){
		char order_name[5]; sprintf(order_name, "%d", iorder+1);
		const std::string filename = RESOURCE() + "Element/LINE_P" + order_name + ".msh";
		infile.open(filename.c_str());

		std::string text;
		std::vector<real_t> node_coords;
		while (getline(infile, text)){
			if (text.find("$Nodes", 0) != std::string::npos){
				infile >> num_nodes_Pn_[iorder];
				real_t trash;
				node_coords.resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++)
					infile >> trash >> node_coords[inode*dimension_] >> trash >> trash;
			}
			if (text.find("$Elements", 0) != std::string::npos){
				int_t element_type, numbering;
				infile >> element_type;
				node_coords_Pn_[iorder].resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++){
					infile >> numbering;
					for (int_t idim = 0; idim < dimension_; idim++){
						node_coords_Pn_[iorder][inode*dimension_ + idim] = node_coords[(numbering - 1)*dimension_ + idim];
					}
				}
			}
		}
		infile.close();
	}
	
	// derived data
	volume_ = 2.0;
	face_areas_ = { 1.0, 1.0 };
	normals_ = { -1.0, 1.0 };
	num_facetypes_ = 2;
	facetype_to_face_ = { 0, 1 };
	facetype_nodes_Pn_ = { { { 0 }, { 1 } }, { { 0 }, { 1 } }, { { 0 }, { 1 } }, { { 0 }, { 1 } }, { { 0 }, { 1 } } };
}

TrisElement::TrisElement()
{
	// intrincis data
	dimension_ = 2;
	element_name_ = "TRIS";
	element_type_ = ElemType::TRIS;
	element_order_supp_ = 5;
	num_faces_ = 3;
	face_nodes_P1_ = { { 0, 1 }, { 1, 2 }, { 2, 0 } };
	std::vector<real_t> center = { -0.33, -0.33 };

	//read data
	num_nodes_Pn_.resize(element_order_supp_);
	node_coords_Pn_.resize(element_order_supp_);
	std::ifstream infile;
	for (int_t iorder = 0; iorder < element_order_supp_; iorder++){
		char order_name[5]; sprintf(order_name, "%d", iorder + 1);
		const std::string filename = RESOURCE() + "Element/TRIS_P" + order_name + ".msh";
		infile.open(filename.c_str());

		std::string text;
		std::vector<real_t> node_coords;
		while (getline(infile, text)){
			if (text.find("$Nodes", 0) != std::string::npos){
				infile >> num_nodes_Pn_[iorder];
				real_t trash;
				node_coords.resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++)
					infile >> trash >> node_coords[inode*dimension_] >> node_coords[inode*dimension_ + 1] >> trash;
			}
			if (text.find("$Elements", 0) != std::string::npos){
				int_t element_type, numbering;
				infile >> element_type;
				node_coords_Pn_[iorder].resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++){
					infile >> numbering;
					for (int_t idim = 0; idim < dimension_; idim++){
						node_coords_Pn_[iorder][inode*dimension_ + idim] = node_coords[(numbering - 1)*dimension_ + idim];
					}
				}
			}
		}
		infile.close();
	}
	
	// derived data
	volume_ = 2.0;
	face_areas_.resize(num_faces_);
	normals_.resize(dimension_*num_faces_);
	face_element_type_.resize(num_faces_, ElemType::LINE);
	for (int_t iface = 0; iface < num_faces_; iface++){
		std::vector<real_t> coords;
		coords.reserve(dimension_*face_nodes_P1_[iface].size());
		for (auto&& node : face_nodes_P1_[iface])
		for (int_t idim = 0; idim < dimension_; idim++)
			coords.push_back(node_coords_Pn_[0][node*dimension_ + idim]);
		std::vector<real_t> normal = ElementMath::NormalVector(coords, center, dimension_);
		for (int_t idim = 0; idim < dimension_; idim++)
			normals_[dimension_*iface + idim] = normal[idim];
		face_areas_[iface] = ElementMath::FaceArea(coords, dimension_);
	}

	num_facetypes_ = 0;
	std::vector<std::vector<int_t>> facetype_nodes_P1;
	for (int_t iface = 0; iface < num_faces_; iface++){
		std::vector<std::vector<int_t>> nodes_per_facetype = ElementMath::GenerateFacetype(face_nodes_P1_[iface]);
		num_facetypes_ += nodes_per_facetype.size();
		for (int_t i = 0; i < nodes_per_facetype.size(); i++){
			facetype_to_face_.push_back(iface);
			facetype_nodes_P1.push_back(nodes_per_facetype[i]);
		}
	}

	facetype_nodes_Pn_.resize(element_order_supp_);
	facetype_nodes_Pn_[0] = facetype_nodes_P1;
	Element* line = ElementFactory::getInstance().getElement(ElemType::LINE);
	for (int_t iorder = 1; iorder < element_order_supp_; iorder++){
		
		facetype_nodes_Pn_[iorder].resize(num_facetypes_);
		const int_t& num_nodes = line->getNumNodes(iorder + 1);
		const std::vector<real_t>& nodes_coord = line->getNodesCoord(iorder + 1);

		for (int_t itype = 0; itype < num_facetypes_; itype++){
			std::vector<real_t> face_coord_P1;
			for (auto&& inode : facetype_nodes_P1[itype])
			for (int_t idim = 0; idim < dimension_; idim++)
				face_coord_P1.push_back(node_coords_Pn_[0][dimension_*inode + idim]);

			std::vector<real_t> face_coords;
			for (int_t inode = 0; inode < num_nodes; inode++){
				const std::vector<real_t> face_coord = ElementMath::LineTransformation(face_coord_P1, &nodes_coord[inode]);
				for (auto&& coord : face_coord)
					face_coords.push_back(coord);
			}

			std::vector<int_t> node_index(num_nodes);
			for (int_t inode = 0; inode < num_nodes; inode++){
				for (int_t jnode = 0; jnode < num_nodes_Pn_[iorder]; jnode++){
					if (ElementMath::isSamePoint(&face_coords[dimension_*inode], &node_coords_Pn_[iorder][dimension_*jnode], dimension_)){
						node_index[inode] = jnode;
						break;
					}
				}
			}
			facetype_nodes_Pn_[iorder][itype] = node_index;
		}
	}
}

QuadElement::QuadElement()
{
	// intrincis data
	dimension_ = 2;
	element_name_ = "QUAD";
	element_type_ = ElemType::QUAD;
	element_order_supp_ = 6;
	num_faces_ = 4;
	face_nodes_P1_ = { { 0, 1 }, { 1, 2 }, { 2, 3 }, { 3, 0 } };
	std::vector<real_t> center = { 0.0, 0.0 };

	//read data
	num_nodes_Pn_.resize(element_order_supp_);
	node_coords_Pn_.resize(element_order_supp_);
	std::ifstream infile;
	for (int_t iorder = 0; iorder < element_order_supp_; iorder++){
		char order_name[5]; sprintf(order_name, "%d", iorder + 1);
		const std::string filename = RESOURCE() + "Element/QUAD_P" + order_name + ".msh";
		infile.open(filename.c_str());

		std::string text;
		std::vector<real_t> node_coords;
		while (getline(infile, text)){
			if (text.find("$Nodes", 0) != std::string::npos){
				infile >> num_nodes_Pn_[iorder];
				real_t trash;
				node_coords.resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++)
					infile >> trash >> node_coords[inode*dimension_] >> node_coords[inode*dimension_ + 1] >> trash;
			}
			if (text.find("$Elements", 0) != std::string::npos){
				int_t element_type, numbering;
				infile >> element_type;
				node_coords_Pn_[iorder].resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++){
					infile >> numbering;
					for (int_t idim = 0; idim < dimension_; idim++){
						node_coords_Pn_[iorder][inode*dimension_ + idim] = node_coords[(numbering - 1)*dimension_ + idim];
					}
				}
			}
		}
		infile.close();
	}

	// derived data
	volume_ = 4.0;
	face_areas_.resize(num_faces_);
	normals_.resize(dimension_*num_faces_);
	face_element_type_.resize(num_faces_, ElemType::LINE);
	for (int_t iface = 0; iface < num_faces_; iface++){
		std::vector<real_t> coords;
		coords.reserve(dimension_*face_nodes_P1_[iface].size());
		for (auto&& node : face_nodes_P1_[iface])
		for (int_t idim = 0; idim < dimension_; idim++)
			coords.push_back(node_coords_Pn_[0][node*dimension_ + idim]);
		std::vector<real_t> normal = ElementMath::NormalVector(coords, center, dimension_);
		for (int_t idim = 0; idim < dimension_; idim++)
			normals_[dimension_*iface + idim] = normal[idim];
		face_areas_[iface] = ElementMath::FaceArea(coords, dimension_);
	}

	num_facetypes_ = 0;
	std::vector<std::vector<int_t>> facetype_nodes_P1;
	for (int_t iface = 0; iface < num_faces_; iface++){
		std::vector<std::vector<int_t>> nodes_per_facetype = ElementMath::GenerateFacetype(face_nodes_P1_[iface]);
		num_facetypes_ += nodes_per_facetype.size();
		for (int_t i = 0; i < nodes_per_facetype.size(); i++){
			facetype_to_face_.push_back(iface);
			facetype_nodes_P1.push_back(nodes_per_facetype[i]);
		}
	}

	facetype_nodes_Pn_.resize(element_order_supp_);
	facetype_nodes_Pn_[0] = facetype_nodes_P1;
	Element* line = ElementFactory::getInstance().getElement(ElemType::LINE);
	for (int_t iorder = 1; iorder < element_order_supp_; iorder++){

		facetype_nodes_Pn_[iorder].resize(num_facetypes_);
		const int_t& num_nodes = line->getNumNodes(iorder + 1);
		const std::vector<real_t>& nodes_coord = line->getNodesCoord(iorder + 1);

		for (int_t itype = 0; itype < num_facetypes_; itype++){
			std::vector<real_t> face_coord_P1;
			for (auto&& inode : facetype_nodes_P1[itype])
			for (int_t idim = 0; idim < dimension_; idim++)
				face_coord_P1.push_back(node_coords_Pn_[0][dimension_*inode + idim]);

			std::vector<real_t> face_coords;
			for (int_t inode = 0; inode < num_nodes; inode++){
				const std::vector<real_t> face_coord = ElementMath::LineTransformation(face_coord_P1, &nodes_coord[inode]);
				for (auto&& coord : face_coord)
					face_coords.push_back(coord);
			}

			std::vector<int_t> node_index(num_nodes);
			for (int_t inode = 0; inode < num_nodes; inode++){
				for (int_t jnode = 0; jnode < num_nodes_Pn_[iorder]; jnode++){
					if (ElementMath::isSamePoint(&face_coords[dimension_*inode], &node_coords_Pn_[iorder][dimension_*jnode], dimension_)){
						node_index[inode] = jnode;
						break;
					}
				}
			}
			facetype_nodes_Pn_[iorder][itype] = node_index;
		}
	}
	
	std::vector<std::vector<int_t>> simplex_nodes_index(num_nodes_Pn_[0]);
	simplex_nodes_index[0] = { 0, 1 ,3 };
	simplex_nodes_index[1] = { 1, 2, 0 };
	simplex_nodes_index[2] = { 2, 3, 1 };
	simplex_nodes_index[3] = { 3, 0, 2 };
	simplex_nodes_index_ = move(simplex_nodes_index);
}

TetsElement::TetsElement()
{
	// intrincis data
	dimension_ = 3;
	element_name_ = "TETS";
	element_type_ = ElemType::TETS;
	element_order_supp_ = 5;
	num_faces_ = 4;
	face_nodes_P1_ = { { 0, 2, 1 }, { 0, 1, 3 }, { 0, 3, 2 }, { 1, 2, 3 } };
	std::vector<real_t> center = { -0.6, -0.6, -0.6 };

	//read data
	num_nodes_Pn_.resize(element_order_supp_);
	node_coords_Pn_.resize(element_order_supp_);
	std::ifstream infile;
	for (int_t iorder = 0; iorder < element_order_supp_; iorder++){
		char order_name[5]; sprintf(order_name, "%d", iorder + 1);
		const std::string filename = RESOURCE() + "Element/TETS_P" + order_name + ".msh";
		infile.open(filename.c_str());

		std::string text;
		std::vector<real_t> node_coords;
		while (getline(infile, text)){
			if (text.find("$Nodes", 0) != std::string::npos){
				infile >> num_nodes_Pn_[iorder];
				real_t trash;
				node_coords.resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++)
					infile >> trash >> node_coords[inode*dimension_] >> node_coords[inode*dimension_ + 1] >> node_coords[inode*dimension_ + 2];
			}
			if (text.find("$Elements", 0) != std::string::npos){
				int_t element_type, numbering;
				infile >> element_type;
				node_coords_Pn_[iorder].resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++){
					infile >> numbering;
					for (int_t idim = 0; idim < dimension_; idim++){
						node_coords_Pn_[iorder][inode*dimension_ + idim] = node_coords[(numbering - 1)*dimension_ + idim];
					}
				}
			}
		}
		infile.close();
	}

	// derived data
	volume_ = 4.0 / 3.0;
	face_areas_.resize(num_faces_);
	normals_.resize(dimension_*num_faces_);
	face_element_type_.resize(num_faces_, ElemType::TRIS);
	for (int_t iface = 0; iface < num_faces_; iface++){
		std::vector<real_t> coords;
		coords.reserve(dimension_*face_nodes_P1_[iface].size());
		for (auto&& node : face_nodes_P1_[iface])
		for (int_t idim = 0; idim < dimension_; idim++)
			coords.push_back(node_coords_Pn_[0][node*dimension_ + idim]);
		std::vector<real_t> normal = ElementMath::NormalVector(coords, center, dimension_);
		for (int_t idim = 0; idim < dimension_; idim++)
			normals_[dimension_*iface + idim] = normal[idim];
		face_areas_[iface] = ElementMath::FaceArea(coords, dimension_);
	}

	num_facetypes_ = 0;
	std::vector<std::vector<int_t>> facetype_nodes_P1;
	for (int_t iface = 0; iface < num_faces_; iface++){
		std::vector<std::vector<int_t>> nodes_per_facetype = ElementMath::GenerateFacetype(face_nodes_P1_[iface]);
		num_facetypes_ += nodes_per_facetype.size();
		for (int_t i = 0; i < nodes_per_facetype.size(); i++){
			facetype_to_face_.push_back(iface);
			facetype_nodes_P1.push_back(nodes_per_facetype[i]);
		}
	}

	facetype_nodes_Pn_.resize(element_order_supp_);
	facetype_nodes_Pn_[0] = facetype_nodes_P1;
	Element* tris = ElementFactory::getInstance().getElement(ElemType::TRIS);
	for (int_t iorder = 1; iorder < element_order_supp_; iorder++){

		facetype_nodes_Pn_[iorder].resize(num_facetypes_);
		const int_t& num_nodes = tris->getNumNodes(iorder + 1);
		const std::vector<real_t>& nodes_coord = tris->getNodesCoord(iorder + 1);

		for (int_t itype = 0; itype < num_facetypes_; itype++){
			std::vector<real_t> face_coord_P1;
			for (auto&& inode : facetype_nodes_P1[itype])
			for (int_t idim = 0; idim < dimension_; idim++)
				face_coord_P1.push_back(node_coords_Pn_[0][dimension_*inode + idim]);

			std::vector<real_t> face_coords;
			for (int_t inode = 0; inode < num_nodes; inode++){
				const std::vector<real_t> face_coord = ElementMath::TrisTransformation(face_coord_P1, &nodes_coord[(dimension_-1)*inode]);
				for (auto&& coord : face_coord)
					face_coords.push_back(coord);
			}

			std::vector<int_t> node_index(num_nodes);
			for (int_t inode = 0; inode < num_nodes; inode++){
				for (int_t jnode = 0; jnode < num_nodes_Pn_[iorder]; jnode++){
					if (ElementMath::isSamePoint(&face_coords[dimension_*inode], &node_coords_Pn_[iorder][dimension_*jnode], dimension_)){
						node_index[inode] = jnode;
						break;
					}
				}
			}
			facetype_nodes_Pn_[iorder][itype] = node_index;
		}
	}
}

HexaElement::HexaElement()
{
	// intrincis data
	dimension_ = 3;
	element_name_ = "HEXA";
	element_type_ = ElemType::HEXA;
	element_order_supp_ = 5;
	num_faces_ = 6;
	face_nodes_P1_ = { { 0, 1, 2, 3 }, { 0, 1, 5, 4 }, { 1, 2, 6, 5 }, { 2, 3, 7, 6 }, { 3, 0, 4, 7 }, { 4, 5, 6, 7 } };
	std::vector<real_t> center = { 0.0, 0.0, 0.0 };

	//read data
	num_nodes_Pn_.resize(element_order_supp_);
	node_coords_Pn_.resize(element_order_supp_);
	std::ifstream infile;
	for (int_t iorder = 0; iorder < element_order_supp_; iorder++){
		char order_name[5]; sprintf(order_name, "%d", iorder + 1);
		const std::string filename = RESOURCE() + "Element/HEXA_P" + order_name + ".msh";
		infile.open(filename.c_str());

		std::string text;
		std::vector<real_t> node_coords;
		while (getline(infile, text)){
			if (text.find("$Nodes", 0) != std::string::npos){
				infile >> num_nodes_Pn_[iorder];
				real_t trash;
				node_coords.resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++)
					infile >> trash >> node_coords[inode*dimension_] >> node_coords[inode*dimension_ + 1] >> node_coords[inode*dimension_ + 2];
			}
			if (text.find("$Elements", 0) != std::string::npos){
				int_t element_type, numbering;
				infile >> element_type;
				node_coords_Pn_[iorder].resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++){
					infile >> numbering;
					for (int_t idim = 0; idim < dimension_; idim++){
						node_coords_Pn_[iorder][inode*dimension_ + idim] = node_coords[(numbering - 1)*dimension_ + idim];
					}
				}
			}
		}
		infile.close();
	}

	// derived data
	volume_ = 8.0;
	face_areas_.resize(num_faces_);
	normals_.resize(dimension_*num_faces_);
	face_element_type_.resize(num_faces_, ElemType::QUAD);
	for (int_t iface = 0; iface < num_faces_; iface++){
		std::vector<real_t> coords;
		coords.reserve(dimension_*face_nodes_P1_[iface].size());
		for (auto&& node : face_nodes_P1_[iface])
		for (int_t idim = 0; idim < dimension_; idim++)
			coords.push_back(node_coords_Pn_[0][node*dimension_ + idim]);
		std::vector<real_t> normal = ElementMath::NormalVector(coords, center, dimension_);
		for (int_t idim = 0; idim < dimension_; idim++)
			normals_[dimension_*iface + idim] = normal[idim];
		face_areas_[iface] = ElementMath::FaceArea(coords, dimension_);
	}

	num_facetypes_ = 0;
	std::vector<std::vector<int_t>> facetype_nodes_P1;
	for (int_t iface = 0; iface < num_faces_; iface++){
		std::vector<std::vector<int_t>> nodes_per_facetype = ElementMath::GenerateFacetype(face_nodes_P1_[iface]);
		num_facetypes_ += nodes_per_facetype.size();
		for (int_t i = 0; i < nodes_per_facetype.size(); i++){
			facetype_to_face_.push_back(iface);
			facetype_nodes_P1.push_back(nodes_per_facetype[i]);
		}
	}

	facetype_nodes_Pn_.resize(element_order_supp_);
	facetype_nodes_Pn_[0] = facetype_nodes_P1;
	Element* quad = ElementFactory::getInstance().getElement(ElemType::QUAD);
	for (int_t iorder = 1; iorder < element_order_supp_; iorder++){

		facetype_nodes_Pn_[iorder].resize(num_facetypes_);
		const int_t& num_nodes = quad->getNumNodes(iorder + 1);
		const std::vector<real_t>& nodes_coord = quad->getNodesCoord(iorder + 1);

		for (int_t itype = 0; itype < num_facetypes_; itype++){
			std::vector<real_t> face_coord_P1;
			for (auto&& inode : facetype_nodes_P1[itype])
			for (int_t idim = 0; idim < dimension_; idim++)
				face_coord_P1.push_back(node_coords_Pn_[0][dimension_*inode + idim]);

			std::vector<real_t> face_coords;
			for (int_t inode = 0; inode < num_nodes; inode++){
				const std::vector<real_t> face_coord = ElementMath::QuadTransformation(face_coord_P1, &nodes_coord[(dimension_ - 1)*inode]);
				for (auto&& coord : face_coord)
					face_coords.push_back(coord);
			}

			std::vector<int_t> node_index(num_nodes);
			for (int_t inode = 0; inode < num_nodes; inode++){
				for (int_t jnode = 0; jnode < num_nodes_Pn_[iorder]; jnode++){
					if (ElementMath::isSamePoint(&face_coords[dimension_*inode], &node_coords_Pn_[iorder][dimension_*jnode], dimension_)){
						node_index[inode] = jnode;
						break;
					}
				}
			}
			facetype_nodes_Pn_[iorder][itype] = node_index;
		}
	}
}

PrisElement::PrisElement()
{
	// intrincis data
	dimension_ = 3;
	element_name_ = "PRIS";
	element_type_ = ElemType::PRIS;
	element_order_supp_ = 5;
	num_faces_ = 5;
	face_nodes_P1_ = { { 0, 1, 2 },{ 0, 1, 4, 3 },{ 0, 2, 5, 3 },{ 1, 2, 5, 4 },{ 3, 4, 5 } };
	std::vector<real_t> center = { -0.33, -0.33, 0.0 };

	//read data
	num_nodes_Pn_.resize(element_order_supp_);
	node_coords_Pn_.resize(element_order_supp_);
	std::ifstream infile;
	for (int_t iorder = 0; iorder < element_order_supp_; iorder++) {
		char order_name[5]; sprintf(order_name, "%d", iorder + 1);
		const std::string filename = RESOURCE() + "Element/PRIS_P" + order_name + ".msh";
		infile.open(filename.c_str());

		std::string text;
		std::vector<real_t> node_coords;
		while (getline(infile, text)) {
			if (text.find("$Nodes", 0) != std::string::npos) {
				infile >> num_nodes_Pn_[iorder];
				real_t trash;
				node_coords.resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++)
					infile >> trash >> node_coords[inode*dimension_] >> node_coords[inode*dimension_ + 1] >> node_coords[inode*dimension_ + 2];
			}
			if (text.find("$Elements", 0) != std::string::npos) {
				int_t element_type, numbering;
				infile >> element_type;
				node_coords_Pn_[iorder].resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++) {
					infile >> numbering;
					for (int_t idim = 0; idim < dimension_; idim++) {
						node_coords_Pn_[iorder][inode*dimension_ + idim] = node_coords[(numbering - 1)*dimension_ + idim];
					}
				}
			}
		}
		infile.close();
	}

	// derived data
	volume_ = 4.0;
	face_areas_.resize(num_faces_);
	normals_.resize(dimension_*num_faces_);
	face_element_type_.resize(num_faces_);
	for (int_t iface = 0; iface < num_faces_; iface++) {
		if (face_nodes_P1_[iface].size() == 3)
			face_element_type_[iface] = ElemType::TRIS;
		else if (face_nodes_P1_[iface].size() == 4)
			face_element_type_[iface] = ElemType::QUAD;

		std::vector<real_t> coords;
		coords.reserve(dimension_*face_nodes_P1_[iface].size());
		for (auto&& node : face_nodes_P1_[iface])
			for (int_t idim = 0; idim < dimension_; idim++)
				coords.push_back(node_coords_Pn_[0][node*dimension_ + idim]);
		std::vector<real_t> normal = ElementMath::NormalVector(coords, center, dimension_);
		for (int_t idim = 0; idim < dimension_; idim++)
			normals_[dimension_*iface + idim] = normal[idim];
		face_areas_[iface] = ElementMath::FaceArea(coords, dimension_);
	}

	num_facetypes_ = 0;
	std::vector<std::vector<int_t>> facetype_nodes_P1;
	for (int_t iface = 0; iface < num_faces_; iface++) {
		std::vector<std::vector<int_t>> nodes_per_facetype = ElementMath::GenerateFacetype(face_nodes_P1_[iface]);
		num_facetypes_ += nodes_per_facetype.size();
		for (int_t i = 0; i < nodes_per_facetype.size(); i++) {
			facetype_to_face_.push_back(iface);
			facetype_nodes_P1.push_back(nodes_per_facetype[i]);
		}
	}

	facetype_nodes_Pn_.resize(element_order_supp_);
	facetype_nodes_Pn_[0] = facetype_nodes_P1;
	Element* quad = ElementFactory::getInstance().getElement(ElemType::QUAD);
	Element* tris = ElementFactory::getInstance().getElement(ElemType::TRIS);
	for (int_t iorder = 1; iorder < element_order_supp_; iorder++) {

		facetype_nodes_Pn_[iorder].resize(num_facetypes_);
		for (int_t itype = 0; itype < num_facetypes_; itype++) {
			std::vector<real_t> face_coord_P1;
			for (auto&& inode : facetype_nodes_P1[itype])
				for (int_t idim = 0; idim < dimension_; idim++)
					face_coord_P1.push_back(node_coords_Pn_[0][dimension_*inode + idim]);

			Element* elem = tris;
			if (facetype_nodes_P1[itype].size() == 4)
				elem = quad;
			const int_t& num_nodes = elem->getNumNodes(iorder + 1);
			const std::vector<real_t>& nodes_coord = elem->getNodesCoord(iorder + 1);

			std::vector<real_t> face_coords;
			for (int_t inode = 0; inode < num_nodes; inode++) {
				if (elem->getElemType() == ElemType::TRIS) {
					const std::vector<real_t> face_coord = ElementMath::TrisTransformation(face_coord_P1, &nodes_coord[(dimension_ - 1)*inode]);
					for (auto&& coord : face_coord)
						face_coords.push_back(coord);
				}else if (elem->getElemType() == ElemType::QUAD) {
					const std::vector<real_t> face_coord = ElementMath::QuadTransformation(face_coord_P1, &nodes_coord[(dimension_ - 1)*inode]);
					for (auto&& coord : face_coord)
						face_coords.push_back(coord);
				}
			}

			std::vector<int_t> node_index(num_nodes);
			for (int_t inode = 0; inode < num_nodes; inode++) {
				for (int_t jnode = 0; jnode < num_nodes_Pn_[iorder]; jnode++) {
					if (ElementMath::isSamePoint(&face_coords[dimension_*inode], &node_coords_Pn_[iorder][dimension_*jnode], dimension_)) {
						node_index[inode] = jnode;
						break;
					}
				}
			}
			facetype_nodes_Pn_[iorder][itype] = node_index;
		}
	}
}

PyraElement::PyraElement()
{
	// intrincis data
	dimension_ = 3;
	element_name_ = "PYRA";
	element_type_ = ElemType::PYRA;
	element_order_supp_ = 1;
	num_faces_ = 5;
	face_nodes_P1_ = { { 0, 1, 2, 3 },{ 0, 1, 4 },{ 0, 3, 4 },{ 1, 2, 4 },{ 2, 3, 4 } };
	std::vector<real_t> center = { 0.0, 0.0, 0.0 };

	//read data
	num_nodes_Pn_.resize(element_order_supp_);
	node_coords_Pn_.resize(element_order_supp_);
	std::ifstream infile;
	for (int_t iorder = 0; iorder < element_order_supp_; iorder++) {
		char order_name[5]; sprintf(order_name, "%d", iorder + 1);
		const std::string filename = RESOURCE() + "Element/PYRA_P" + order_name + ".msh";
		infile.open(filename.c_str());

		std::string text;
		std::vector<real_t> node_coords;
		while (getline(infile, text)) {
			if (text.find("$Nodes", 0) != std::string::npos) {
				infile >> num_nodes_Pn_[iorder];
				real_t trash;
				node_coords.resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++)
					infile >> trash >> node_coords[inode*dimension_] >> node_coords[inode*dimension_ + 1] >> node_coords[inode*dimension_ + 2];
			}
			if (text.find("$Elements", 0) != std::string::npos) {
				int_t element_type, numbering;
				infile >> element_type;
				node_coords_Pn_[iorder].resize(dimension_*num_nodes_Pn_[iorder]);
				for (int_t inode = 0; inode < num_nodes_Pn_[iorder]; inode++) {
					infile >> numbering;
					for (int_t idim = 0; idim < dimension_; idim++) {
						node_coords_Pn_[iorder][inode*dimension_ + idim] = node_coords[(numbering - 1)*dimension_ + idim];
					}
				}
			}
		}
		infile.close();
	}

	// derived data
	volume_ = 8.0 / 3.0;
	face_areas_.resize(num_faces_);
	normals_.resize(dimension_*num_faces_);
	face_element_type_.resize(num_faces_);
	for (int_t iface = 0; iface < num_faces_; iface++) {
		if (face_nodes_P1_[iface].size() == 3)
			face_element_type_[iface] = ElemType::TRIS;
		else if (face_nodes_P1_[iface].size() == 4)
			face_element_type_[iface] = ElemType::QUAD;

		std::vector<real_t> coords;
		coords.reserve(dimension_*face_nodes_P1_[iface].size());
		for (auto&& node : face_nodes_P1_[iface])
			for (int_t idim = 0; idim < dimension_; idim++)
				coords.push_back(node_coords_Pn_[0][node*dimension_ + idim]);
		std::vector<real_t> normal = ElementMath::NormalVector(coords, center, dimension_);
		for (int_t idim = 0; idim < dimension_; idim++)
			normals_[dimension_*iface + idim] = normal[idim];
		face_areas_[iface] = ElementMath::FaceArea(coords, dimension_);
	}

	num_facetypes_ = 0;
	std::vector<std::vector<int_t>> facetype_nodes_P1;
	for (int_t iface = 0; iface < num_faces_; iface++) {
		std::vector<std::vector<int_t>> nodes_per_facetype = ElementMath::GenerateFacetype(face_nodes_P1_[iface]);
		num_facetypes_ += nodes_per_facetype.size();
		for (int_t i = 0; i < nodes_per_facetype.size(); i++) {
			facetype_to_face_.push_back(iface);
			facetype_nodes_P1.push_back(nodes_per_facetype[i]);
		}
	}

	facetype_nodes_Pn_.resize(element_order_supp_);
	facetype_nodes_Pn_[0] = facetype_nodes_P1;
	Element* quad = ElementFactory::getInstance().getElement(ElemType::QUAD);
	Element* tris = ElementFactory::getInstance().getElement(ElemType::TRIS);
	for (int_t iorder = 1; iorder < element_order_supp_; iorder++) {

		facetype_nodes_Pn_[iorder].resize(num_facetypes_);
		for (int_t itype = 0; itype < num_facetypes_; itype++) {
			std::vector<real_t> face_coord_P1;
			for (auto&& inode : facetype_nodes_P1[itype])
				for (int_t idim = 0; idim < dimension_; idim++)
					face_coord_P1.push_back(node_coords_Pn_[0][dimension_*inode + idim]);

			Element* elem = tris;
			if (facetype_nodes_P1[itype].size() == 4)
				elem = quad;
			const int_t& num_nodes = elem->getNumNodes(iorder + 1);
			const std::vector<real_t>& nodes_coord = elem->getNodesCoord(iorder + 1);

			std::vector<real_t> face_coords;
			for (int_t inode = 0; inode < num_nodes; inode++) {
				if (elem->getElemType() == ElemType::TRIS) {
					const std::vector<real_t> face_coord = ElementMath::TrisTransformation(face_coord_P1, &nodes_coord[(dimension_ - 1)*inode]);
					for (auto&& coord : face_coord)
						face_coords.push_back(coord);
				}
				else if (elem->getElemType() == ElemType::QUAD) {
					const std::vector<real_t> face_coord = ElementMath::QuadTransformation(face_coord_P1, &nodes_coord[(dimension_ - 1)*inode]);
					for (auto&& coord : face_coord)
						face_coords.push_back(coord);
				}
			}

			std::vector<int_t> node_index(num_nodes);
			for (int_t inode = 0; inode < num_nodes; inode++) {
				for (int_t jnode = 0; jnode < num_nodes_Pn_[iorder]; jnode++) {
					if (ElementMath::isSamePoint(&face_coords[dimension_*inode], &node_coords_Pn_[iorder][dimension_*jnode], dimension_)) {
						node_index[inode] = jnode;
						break;
					}
				}
			}
			facetype_nodes_Pn_[iorder][itype] = node_index;
		}
	}
}