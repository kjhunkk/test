#include "../INC/TimeUnstImpBDF2.h"

TimeUnstImpBDF2::TimeUnstImpBDF2()
{
	setName("BDF2");
	TimeScheme::getInstance().addRegistry(getName(), this);
	is_implicit_ = true;
}

TimeUnstImpBDF2::~TimeUnstImpBDF2()
{
	PetscErrorCode ierr;
	ierr = VecDestroy(&delta_);
	ierr = VecDestroy(&intermediate_solution_vector_);
	ierr = VecDestroy(&rhs_);
	ierr = VecDestroy(&base_rhs_);
	for (int_t i = 0; i < solution_array_.size(); i++)
		ierr = VecDestroy(&solution_array_[i]);
}

void TimeUnstImpBDF2::initialize()
{
	const Config& config = Config::getInstance();

	// Parent member variables
	if (config.getIsFixedTimeStep().compare("yes") == 0)
		is_fixed_dt_ = true;
	else
	{
		is_fixed_dt_ = false;
		MASTER_ERROR("Adaptive time step is not supported");
	}

	// Time integration configuration
	diagonal_factor_ = 0.0;
	PetscErrorCode ierr;
	ierr = VecCreate(MPI_COMM_WORLD, &delta_);
	ierr = VecCreate(MPI_COMM_WORLD, &intermediate_solution_vector_);
	ierr = VecCreate(MPI_COMM_WORLD, &rhs_);
	ierr = VecCreate(MPI_COMM_WORLD, &base_rhs_);
	for (int_t i = 0; i < solution_array_.size(); i++)
		ierr = VecCreate(MPI_COMM_WORLD, &solution_array_[i]);

	// Non-linear solver configuration
	max_newton_iteration_ = 1000;
	newton_tolerance_ = 1.0e-8;
	recompute_tol_ = 0.2;

	// Linear system solver initialize
	linear_system_solver_ = LinearSystemSolver::getInstance().getType(config.getLinearSystemSolver());
	linear_system_solver_->Initialize();

	// etc
	is_initialized_ = false;
}

void TimeUnstImpBDF2::integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
	if (is_initialized_ == false)
	{
		InitializeVectors(zone, dt);
		SelfStartingProcedure(zone, dt, current_time, iteration);
		is_initialized_ = true;
	}

	diagonal_factor_ = 1.5 / dt;
	int_t internal_iteration = 0;

	const int_t num_cells = zone->getNumCells();
	const std::vector<int_t> cell_petsc_index = zone->getCellPetscIndex();
	
	// Newton iteration
	VecCopy(solution_array_[1], intermediate_solution_vector_);
	ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);
	system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_, 1.0);
	int_t krylov_iteration = 0;
	real_t newton_current_norm = 0.0;
	real_t newton_previous_norm = 0.0;

	VecCopy(solution_array_[1], base_rhs_);
	VecAXPY(base_rhs_, -0.25, solution_array_[0]);
	VecScale(base_rhs_, 2.0 / dt);

	while (internal_iteration++ < max_newton_iteration_)
	{
		// RHS formulation
		VecCopy(intermediate_solution_vector_, rhs_);
		VecScale(rhs_, -diagonal_factor_);
		VecAXPY(rhs_, 1.0, base_rhs_);
		VecAXPY(rhs_, 1.0, zone->calculateInstantSystemRHS(intermediate_solution_));

		// Krylov solve
		krylov_iteration = linear_system_solver_->Solve(system_matrix_, rhs_, delta_);
		VecNorm(delta_, NORM_2, &newton_current_norm);

		// intermediate solution update
		VecAXPY(delta_, 1.0, intermediate_solution_vector_);
		VecCopy(delta_, intermediate_solution_vector_);
		ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);

		// Check convergence
		if (newton_current_norm < newton_tolerance_) break;
		if ((newton_current_norm / newton_previous_norm) > recompute_tol_)
			system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_, 1.0);
		newton_previous_norm = newton_current_norm;
	}

	zone->UpdateSolutionFromVec(intermediate_solution_vector_);
	zone->communication();

	VecCopy(solution_array_[1], solution_array_[0]);
	VecCopy(intermediate_solution_vector_, solution_array_[1]);

	real_t error = 0;
	VecNorm(rhs_, NORM_2, &error);
	status_string_ = "Iteration=" + TO_STRING(internal_iteration) + " L2=" + TO_STRING((double)error);
}

void TimeUnstImpBDF2::InitializeVectors(std::shared_ptr<Zone> zone, const real_t dt)
{
	const Vec& vec_initializer = zone->getSystemRHS();
	VecDuplicate(vec_initializer, &delta_);
	VecDuplicate(vec_initializer, &intermediate_solution_vector_);
	intermediate_solution_ = zone->getSolution();
	VecDuplicate(vec_initializer, &rhs_);
	VecDuplicate(vec_initializer, &base_rhs_);
	for (int_t i = 0; i < solution_array_.size(); i++)
		VecDuplicate(vec_initializer, &solution_array_[i]);
}

void TimeUnstImpBDF2::ConvertSolutionToPetsc(const std::vector<std::vector<real_t> >& solution, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, Vec& vector)
{
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	PetscScalar *vector_array;
	const PetscInt num_var = solution[0].size();
	VecGetArray(vector, &vector_array);
	for (int_t icell = 0; icell < num_cells; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			vector_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar] = solution[icell][ivar];
	VecRestoreArray(vector, &vector_array);
}

void TimeUnstImpBDF2::ConvertPetscToSolution(std::shared_ptr<Zone> zone, const Vec& vector, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, std::vector<std::vector<real_t> > & solution)
{
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	const PetscScalar *vector_array;
	const PetscInt num_var = solution[0].size();
	VecGetArrayRead(vector, &vector_array);
	for (int_t icell = 0; icell < num_cells; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			solution[icell][ivar] = vector_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar];
	VecRestoreArrayRead(vector, &vector_array);
	zone->RequestCommunication(solution);
}

void TimeUnstImpBDF2::SelfStartingProcedure(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
#ifdef _Order_Test_
	for (int_t i = 0; i < solution_array_.size(); i++)
		ConvertSolutionToPetsc(zone->getExactSolution(dt*(double)(i - 1)), zone->getNumCells(), zone->getCellPetscIndex(), solution_array_[i]);
#else
	const int_t num_cells = zone->getNumCells();
	const std::vector<int_t> cell_petsc_index = zone->getCellPetscIndex();
	std::vector<std::vector<real_t> > temp_solution = zone->getSolution();
	std::vector<Vec> Y(2), F(2);
	for (int_t i = 0; i < solution_array_.size(); i++)
	{
		VecCreate(MPI_COMM_WORLD, &Y[i]); VecCreate(MPI_COMM_WORLD, &F[i]);
		VecDuplicate(solution_array_[0], &Y[i]); VecDuplicate(solution_array_[0], &F[i]);
	}
	ConvertSolutionToPetsc(zone->getSolution(), num_cells, cell_petsc_index, solution_array_[0]);

	// 1st step
	VecCopy(solution_array_[0], Y[0]);
	VecCopy(zone->calculateInstantSystemRHS(zone->getSolution()), F[0]);
	VecCopy(Y[0], Y[1]);
	VecAXPY(Y[1], dt, F[0]);
	ConvertPetscToSolution(zone, Y[1], num_cells, cell_petsc_index, temp_solution);
	VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[1]);

	// Final
	VecCopy(solution_array_[0], solution_array_[1]);
	VecAXPY(solution_array_[1], dt / 2.0, F[0]);
	VecAXPY(solution_array_[1], dt / 2.0, F[1]);
	current_time += dt;
	iteration += 1;
#endif
}