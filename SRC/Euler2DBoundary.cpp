
#include "../INC/Euler2DBoundary.h"

const std::vector<real_t> Euler2DBCExtrapolate::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFlux(owner_u, normal);
}
const std::vector<real_t> Euler2DBCExtrapolate::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFluxJacobian(owner_u, normal);
}
const std::vector<real_t> Euler2DBCExtrapolate::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFlux(owner_u, owner_div_u, normal);
}
const std::vector<real_t> Euler2DBCExtrapolate::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> Euler2DBCExtrapolate::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxGradJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> Euler2DBCExtrapolate::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return owner_u;
}
const std::vector<real_t> Euler2DBCExtrapolate::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return std::vector<real_t>(owner_u.size() * 4, 0.0);
}

const std::vector<real_t> Euler2DBCConstant::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++)
		for (int_t istate = 0; istate < 4; istate++)
			references[ipoint * 4 + istate] = references_[istate];
	return flux_->numConvFlux(owner_u, references, normal);
}
const std::vector<real_t> Euler2DBCConstant::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++)
		for (int_t istate = 0; istate < 4; istate++)
			references[ipoint * 4 + istate] = references_[istate];
	return flux_->numConvFluxOwnerJacobian(owner_u, references, normal);
}
const std::vector<real_t> Euler2DBCConstant::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++)
		for (int_t istate = 0; istate < 4; istate++)
			references[ipoint * 4 + istate] = references_[istate];
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFlux(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> Euler2DBCConstant::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++)
		for (int_t istate = 0; istate < 4; istate++)
			references[ipoint * 4 + istate] = references_[istate];
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> Euler2DBCConstant::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++)
		for (int_t istate = 0; istate < 4; istate++)
			references[ipoint * 4 + istate] = references_[istate];
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFluxOwnerGradJacobian(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> Euler2DBCConstant::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++)
		for (int_t istate = 0; istate < 4; istate++)
			references[ipoint * 4 + istate] = references_[istate];
	return references;
}
const std::vector<real_t> Euler2DBCConstant::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> jacobian(owner_u.size() * 4, 0.0);
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++)
		for (int_t istate = 0; istate < 4; istate++)
			jacobian[ipoint * 16 + istate * 5] = -0.5;
	return jacobian;
}

const std::vector<real_t> Euler2DBCWall::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> Flux(num_points * 4, 0.0);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		Flux[ipoint * 4 + 1] = normal[ipoint * 2] * pressure[ipoint];
		Flux[ipoint * 4 + 2] = normal[ipoint * 2 + 1] * pressure[ipoint];
	}
	return Flux;
}
const std::vector<real_t> Euler2DBCWall::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> FluxJacobi(num_points * 16, 0.0);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& d = owner_u[ipoint*num_states_];
		const real_t& du = owner_u[ipoint*num_states_ + 1];
		const real_t& dv = owner_u[ipoint*num_states_ + 2];
		const real_t& nx = normal[ipoint*2];
		const real_t& ny = normal[ipoint*2 + 1];

		const real_t du_over_d = du / d;
		const real_t dv_over_d = dv / d;

		// row 1
		FluxJacobi[ipoint*num_states_*num_states_ + 4] = 0.2*nx*(du_over_d*du_over_d + dv_over_d * dv_over_d);
		FluxJacobi[ipoint*num_states_*num_states_ + 5] = -0.4*nx*du_over_d;
		FluxJacobi[ipoint*num_states_*num_states_ + 6] = -0.4*nx*dv_over_d;
		FluxJacobi[ipoint*num_states_*num_states_ + 7] = 0.4*nx;

		// row 2
		FluxJacobi[ipoint*num_states_*num_states_ + 8] = 0.2*ny*(du_over_d*du_over_d + dv_over_d * dv_over_d);
		FluxJacobi[ipoint*num_states_*num_states_ + 9] = -0.4*ny*du_over_d;
		FluxJacobi[ipoint*num_states_*num_states_ + 10] = -0.4*ny*dv_over_d;
		FluxJacobi[ipoint*num_states_*num_states_ + 11] = 0.4*ny;
	}
	return FluxJacobi;
}
const std::vector<real_t> Euler2DBCWall::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = flux_->calTemperature(owner_u);
	const std::vector<real_t> viscosity = flux_->calViscosity(temperature);
#endif
	std::vector<real_t> Flux(num_points*num_states_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &owner_div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t d_1 = 1.0 / u[0];
		const real_t ux = t[2] * d_1;
		const real_t uy = t[3] * d_1;
		const real_t vx = t[4] * d_1;
		const real_t vy = t[5] * d_1;
		const real_t txx = 2.0 / 3.0*(2.0*ux - vy);
		const real_t txy = uy + vx;
		const real_t tyy = 2.0 / 3.0 * (2.0*vy - ux);
		const real_t alpha = alpha_ * viscosity[ipoint];

		Flux[ipoint*num_states_ + 1] = alpha * (txx*n[0] + txy * n[1]);
		Flux[ipoint*num_states_ + 2] = alpha * (txy*n[0] + tyy * n[1]);
	}
	return Flux;
}
const std::vector<real_t> Euler2DBCWall::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	const std::vector<real_t> owner_pressure = flux_->calPressure(owner_u);
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = flux_->calTemperature(owner_u);
	const std::vector<real_t> viscosity = flux_->calViscosity(temperature);
#endif
	std::vector<real_t> FluxJacobi(num_points * 16, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &owner_div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t ux = t[2] / u[0];
		const real_t uy = t[3] / u[0];
		const real_t vx = t[4] / u[0];
		const real_t vy = t[5] / u[0];
		const real_t txx = 2.0 / 3.0 * (2.0*ux - vy);
		const real_t txy = uy + vx;
		const real_t tyy = 2.0 / 3.0 * (2.0*vy - ux);

		const real_t Sutherland_coef = flux_->getSutherlandCoeff();
		const real_t alpha = alpha_ * viscosity[ipoint];

		const real_t deno1 = pow(u[0], 2.0);

		const real_t jacobian_txx = (2.0*t[5] - 4.0*t[2]) / (3.0*deno1);
		const real_t jacobian_txy = -(t[3] + t[4]) / deno1;
		const real_t jacobian_tyy = (2.0*t[2] - 4.0*t[5]) / (3.0*deno1);
#ifndef _CONSTANT_VISCOSITY_
		const real_t viscosity_temp = viscosity * (3.0*Sutherland_coef + temperature) / (2.0*temperature * (Sutherland_coef + temperature));
		const std::vector<real_t> jacobian_viscosity = { 0.56*viscosity_temp*((pow(u[1], 2.0) + pow(u[2], 2.0)) / u[0] - u[3]) / deno1, -0.56*viscosity_temp*u[1] / deno1, -0.56*viscosity_temp*u[2] / deno1, 0.56*viscosity_temp / u[0] };
#endif
#ifdef _CONSTANT_VISCOSITY_
		// row 1
		FluxJacobi[ipoint*num_states_*num_states_ + 4] = alpha * (jacobian_txx * n[0] + jacobian_txy * n[1]);

		// row 2
		FluxJacobi[ipoint*num_states_*num_states_ + 8] = alpha * (jacobian_txy * n[0] + jacobian_tyy * n[1]);

#else
		// row 1
		FluxJacobi[ipoint*num_states_*num_states_ + 4] = alpha * (jacobian_txx * n[0] + jacobian_txy * n[1]) + alpha_ * jacobian_viscosity[0] * (txx*n[0] + txy * n[1]);
		FluxJacobi[ipoint*num_states_*num_states_ + 5] = alpha_ * jacobian_viscosity[1] * (txx*n[0] + txy * n[1]);
		FluxJacobi[ipoint*num_states_*num_states_ + 6] = alpha_ * jacobian_viscosity[2] * (txx*n[0] + txy * n[1]);
		FluxJacobi[ipoint*num_states_*num_states_ + 7] = alpha * jacobian_viscosity[3] * (txx*n[0] + txy * n[1]);

		// row 2
		FluxJacobi[ipoint*num_states_*num_states_ + 8] = alpha * (jacobian_txy * n[0] + jacobian_tyy * n[1]) + alpha_ * jacobian_viscosity[0] * (txy*n[0] + tyy * n[1]);
		FluxJacobi[ipoint*num_states_*num_states_ + 9] = alpha * jacobian_viscosity[1] * (txy*n[0] + tyy * n[1]);
		FluxJacobi[ipoint*num_states_*num_states_ + 10] = alpha * jacobian_viscosity[2] * (txy*n[0] + tyy * n[1]);
		FluxJacobi[ipoint*num_states_*num_states_ + 11] = alpha * jacobian_viscosity[3] * (txy*n[0] + tyy * n[1]);
#endif
	}
	return FluxJacobi;
}
const std::vector<real_t> Euler2DBCWall::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = flux_->calTemperature(owner_u);
	const std::vector<real_t> viscosity = flux_->calViscosity(temperature);
#endif
	std::vector<real_t> FluxGradJacobi(num_points * 32, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	{
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t Sutherland_coef = flux_->getSutherlandCoeff();
		const real_t alpha = alpha_ * viscosity[ipoint];

		// (istate, jstate, idim)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 10] = alpha * 4.0*n[0] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 11] = alpha * n[1] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 12] = alpha * n[1] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 13] = alpha * (-2.0)*n[0] / (3.0*u[0]);

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 18] = alpha * (-2.0)*n[1] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 19] = alpha * n[0] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 20] = alpha * n[0] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 21] = alpha * 4.0*n[1] / (3.0*u[0]);
	}
	return FluxGradJacobi;
}
const std::vector<real_t> Euler2DBCWall::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u(owner_u.size());
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++) {
		neighbor_u[ipoint * 4] = owner_u[ipoint * 4];
		neighbor_u[ipoint * 4 + 1] = -owner_u[ipoint * 4 + 1];
		neighbor_u[ipoint * 4 + 2] = -owner_u[ipoint * 4 + 2];
		//neighbor_u[ipoint * 4 + 3] = owner_u[ipoint * 4 + 3];
		neighbor_u[ipoint * 4 + 3] = 5.0*pressure[ipoint] - owner_u[ipoint * 4 + 3];
	}
	return neighbor_u;
}
const std::vector<real_t> Euler2DBCWall::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> jacobian(owner_u.size() * 4, 0.0);
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++) {
		jacobian[ipoint * 16 + 5] = -1.0;
		jacobian[ipoint * 16 + 10] = -1.0;
		jacobian[ipoint * 16 + 12] = 0.5*(owner_u[ipoint * 4 + 1] * owner_u[ipoint * 4 + 1] + owner_u[ipoint * 4 + 2] * owner_u[ipoint * 4 + 2]) / (owner_u[ipoint * 4] * owner_u[ipoint * 4]);
		jacobian[ipoint * 16 + 13] = -owner_u[ipoint * 4 + 1] / owner_u[ipoint * 4];
		jacobian[ipoint * 16 + 14] = -owner_u[ipoint * 4 + 2] / owner_u[ipoint * 4];
	}
	return jacobian;
}

const std::vector<real_t> Euler2DBCSlipWall::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> Flux(num_points * 4, 0.0);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		Flux[ipoint * 4 + 1] = normal[ipoint * 2] * pressure[ipoint];
		Flux[ipoint * 4 + 2] = normal[ipoint * 2 + 1] * pressure[ipoint];
	}
	return Flux;
}
const std::vector<real_t> Euler2DBCSlipWall::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> FluxJacobi(num_points * 16, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& d = owner_u[ipoint*num_states_];
		const real_t& du = owner_u[ipoint*num_states_ + 1];
		const real_t& dv = owner_u[ipoint*num_states_ + 2];
		const real_t& nx = normal[ipoint * 2];
		const real_t& ny = normal[ipoint * 2 + 1];

		const real_t du_over_d = du / d;
		const real_t dv_over_d = dv / d;

		// row 1
		FluxJacobi[ipoint*num_states_*num_states_ + 4] = 0.2*nx*(du_over_d*du_over_d + dv_over_d * dv_over_d);
		FluxJacobi[ipoint*num_states_*num_states_ + 5] = -0.4*nx*du_over_d;
		FluxJacobi[ipoint*num_states_*num_states_ + 6] = -0.4*nx*dv_over_d;
		FluxJacobi[ipoint*num_states_*num_states_ + 7] = 0.4*nx;

		// row 2
		FluxJacobi[ipoint*num_states_*num_states_ + 8] = 0.2*ny*(du_over_d*du_over_d + dv_over_d * dv_over_d);
		FluxJacobi[ipoint*num_states_*num_states_ + 9] = -0.4*ny*du_over_d;
		FluxJacobi[ipoint*num_states_*num_states_ + 10] = -0.4*ny*dv_over_d;
		FluxJacobi[ipoint*num_states_*num_states_ + 11] = 0.4*ny;
	}
	return FluxJacobi;
	//std::vector<real_t> Flux_B_Jacobi(16, 0.0);
	//std::vector<real_t> U_B_Jacobi(16, 0.0);
	//const std::vector<real_t> pressure = flux_->calPressure(owner_u);

	//for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
	//	const real_t& d = owner_u[ipoint * 4];
	//	const real_t& du = owner_u[ipoint * 4 + 1];
	//	const real_t& dv = owner_u[ipoint * 4 + 2];
	//	const real_t& dE = owner_u[ipoint * 4 + 3];
	//	const real_t& p = pressure[ipoint];
	//	const real_t& nx = normal[ipoint * 2];
	//	const real_t& ny = normal[ipoint * 2 + 1];
	//	const real_t dVt = (-du*ny + dv * nx);

	//	const real_t du_wall = -dVt*ny;
	//	const real_t dv_wall = dVt * nx;
	//	const real_t dE_wall = 2.5*p + 0.5*(du_wall*du_wall + dv_wall * dv_wall) / d;

	//	for (int_t i = 0; i < 16; i++)
	//	{
	//		U_B_Jacobi[i] = 0.0;
	//		Flux_B_Jacobi[i] = 0.0;
	//	}

	//	U_B_Jacobi[0] = 1.0; // row 1
	//	U_B_Jacobi[5] = ny * ny; U_B_Jacobi[6] = -nx * ny; // row 1
	//	U_B_Jacobi[9] = -nx * ny; U_B_Jacobi[10] = nx * nx; // row 2
	//	U_B_Jacobi[12] = 0.5*(du*du + dv * dv + du_wall * du_wall + dv_wall * dv_wall) / d; // row 3
	//	U_B_Jacobi[13] = (-du + du_wall * ny*ny - dv_wall * nx*ny) / d;
	//	U_B_Jacobi[14] = (-dv - du_wall * nx*ny + dv_wall * nx*nx) / d;
	//	U_B_Jacobi[15] = 1.0;

	//	// row 0
	//	Flux_B_Jacobi[1] = nx;
	//	Flux_B_Jacobi[2] = ny;

	//	// row 1
	//	Flux_B_Jacobi[4] = 0.2*nx*(du_wall*du_wall + dv_wall * dv_wall) / (d*d);
	//	Flux_B_Jacobi[5] = 0.6*du_wall * nx / d;
	//	Flux_B_Jacobi[6] = du_wall * ny / d - 0.4*dv_wall*nx / d;
	//	Flux_B_Jacobi[7] = 0.4*nx;

	//	// row 2
	//	Flux_B_Jacobi[8] = 0.2*ny*(du_wall*du_wall + dv_wall * dv_wall) / (d*d);
	//	Flux_B_Jacobi[9] = dv_wall * nx / d - 0.4*du_wall*ny / d;
	//	Flux_B_Jacobi[10] = 0.6*dv_wall * ny / d;
	//	Flux_B_Jacobi[11] = 0.4*ny;

	//	// row 3
	//	Flux_B_Jacobi[13] = nx * (dE_wall + p) / d;
	//	Flux_B_Jacobi[14] = ny * (dE_wall + p) / d;

	//	for (int_t irow = 0; irow < 4; irow++)
	//		for (int_t icolumn = 0; icolumn < 4; icolumn++)
	//			for (int_t i = 0; i < 4; i++)
	//				FluxJacobi[ipoint*num_states_*num_states_ + irow * num_states_ + icolumn] += Flux_B_Jacobi[irow*num_states_ + i] * U_B_Jacobi[i*num_states_ + icolumn];
	//}
	//return FluxJacobi;
}
const std::vector<real_t> Euler2DBCSlipWall::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> Flux(num_points * 4, 0.0);
	return Flux;
}
const std::vector<real_t> Euler2DBCSlipWall::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> FluxJacobi(num_points * 16, 0.0);
	return FluxJacobi;
}
const std::vector<real_t> Euler2DBCSlipWall::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> FluxGradJacobi(num_points * 32, 0.0);
	return FluxGradJacobi;
}
const std::vector<real_t> Euler2DBCSlipWall::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	//std::vector<real_t> neighbor_u(owner_u.size());
	//const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	//for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++) {
	//	neighbor_u[ipoint * 4] = owner_u[ipoint * 4];
	//	neighbor_u[ipoint * 4 + 1] = owner_u[ipoint * 4 + 1];
	//	neighbor_u[ipoint * 4 + 2] = owner_u[ipoint * 4 + 2];
	//	neighbor_u[ipoint * 4 + 3] = owner_u[ipoint * 4 + 3];
	//}
	return owner_u;
}
const std::vector<real_t> Euler2DBCSlipWall::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	//std::vector<real_t> jacobian(owner_u.size() * 4, 0.0);
	//for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++) {
	//	const real_t& nx = normal[ipoint * 2];
	//	const real_t& ny = normal[ipoint * 2 + 1];
	//	const real_t du_B = ny * (owner_u[ipoint * 4 + 1] * ny - owner_u[ipoint * 4 + 2] * nx);
	//	const real_t dv_B = nx * (owner_u[ipoint * 4 + 2] * nx - owner_u[ipoint * 4 + 1] * ny);

	//	jacobian[ipoint * 16] = 1.0;
	//	jacobian[ipoint * 16 + 5] = ny * ny;
	//	jacobian[ipoint * 16 + 6] = -nx * ny;
	//	jacobian[ipoint * 16 + 9] = -nx * ny;
	//	jacobian[ipoint * 16 + 10] = nx * nx;
	//	jacobian[ipoint * 16 + 12] = 0.5*(owner_u[ipoint * 4 + 1] * owner_u[ipoint * 4 + 1] + owner_u[ipoint * 4 + 2] * owner_u[ipoint * 4 + 2] - du_B * du_B - dv_B * dv_B) / (owner_u[ipoint * 4] * owner_u[ipoint * 4]);
	//	jacobian[ipoint * 16 + 13] = (-owner_u[ipoint * 4 + 1] + du_B * ny*ny - dv_B * nx*ny) / owner_u[ipoint * 4];
	//	jacobian[ipoint * 16 + 14] = (-owner_u[ipoint * 4 + 2] - du_B * nx * ny + dv_B * nx*nx) / owner_u[ipoint * 4];
	//	jacobian[ipoint * 16 + 15] = 1.0;
	//}
	//return jacobian;
	return std::vector<real_t>(owner_u.size() * 4, 0.0);
}

const std::vector<real_t> Euler2DBCSubOut::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	return flux_->numConvFlux(owner_u, neighbor_u, normal);
}
const std::vector<real_t> Euler2DBCSubOut::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	std::vector<real_t> FluxJacobi = flux_->numConvFluxOwnerJacobian(owner_u, neighbor_u, normal);
	std::vector<real_t> FluxNeighborJacobi = flux_->numConvFluxNeighborJacobian(owner_u, neighbor_u, normal);
	std::vector<real_t> solution_jacobian(num_states_*num_states_, 0.0);
	solution_jacobian[0] = 1.0; solution_jacobian[5] = 1.0; solution_jacobian[10] = 1.0;
	solution_jacobian[12] = -(owner_u[1] * owner_u[1] + owner_u[2] * owner_u[2]) / (owner_u[0] * owner_u[0]);
	solution_jacobian[13] = 2.0*owner_u[1] / owner_u[0]; solution_jacobian[14] = 2.0*owner_u[2] / owner_u[0];
	solution_jacobian[15] = -1.0;

	for (int_t i = 0; i < num_states_; i++)
		for (int_t j = 0; j < num_states_; j++)
			for (int_t k = 0; k < num_states_; k++)
				FluxJacobi[i*num_states_ + j] += FluxNeighborJacobi[i*num_states_ + k] * solution_jacobian[k*num_states_ + j];
	return FluxJacobi;
}
const std::vector<real_t> Euler2DBCSubOut::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFlux(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> Euler2DBCSubOut::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	std::vector<real_t> FluxJacobi = flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
	std::vector<real_t> FluxNeighborJacobi = flux_->numViscFluxNeighborJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
	std::vector<real_t> solution_jacobian(num_states_*num_states_, 0.0);
	solution_jacobian[0] = 1.0; solution_jacobian[5] = 1.0; solution_jacobian[10] = 1.0;
	solution_jacobian[12] = -(owner_u[1] * owner_u[1] + owner_u[2] * owner_u[2]) / (owner_u[0] * owner_u[0]);
	solution_jacobian[13] = 2.0*owner_u[1] / owner_u[0]; solution_jacobian[14] = 2.0*owner_u[2] / owner_u[0];
	solution_jacobian[15] = -1.0;

	for (int_t i = 0; i < num_states_; i++)
		for (int_t j = 0; j < num_states_; j++)
			for (int_t k = 0; k < num_states_; k++)
				FluxJacobi[i*num_states_ + j] += FluxNeighborJacobi[i*num_states_ + k] * solution_jacobian[k*num_states_ + j];
	return FluxJacobi;
}
const std::vector<real_t> Euler2DBCSubOut::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	std::vector<real_t> FluxGradJacobi = flux_->numViscFluxOwnerGradJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
	std::vector<real_t> FluxNeighborGradJacobi = flux_->numViscFluxNeighborGradJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
	for (int_t ivar = 0; ivar < FluxGradJacobi.size(); ivar++)
		FluxGradJacobi[ivar] += FluxNeighborGradJacobi[ivar];
	return FluxGradJacobi;
}
const std::vector<real_t> Euler2DBCSubOut::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u(owner_u.size());
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++) {
		neighbor_u[ipoint * 4 + 0] = owner_u[ipoint * 4 + 0];
		neighbor_u[ipoint * 4 + 1] = owner_u[ipoint * 4 + 1];
		neighbor_u[ipoint * 4 + 2] = owner_u[ipoint * 4 + 2];
		neighbor_u[ipoint * 4 + 3] = owner_u[ipoint * 4 + 3] + 5.0*(1.0 / 1.4 - pressure[ipoint]);
	}
	return neighbor_u;
}
const std::vector<real_t> Euler2DBCSubOut::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> jacobian(owner_u.size() * 4, 0.0);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++) {
		//jacobian[ipoint * 4] = 1.0;
		//jacobian[ipoint * 4 + 5] = 1.0;
		//jacobian[ipoint * 4 + 10] = 1.0;
		jacobian[ipoint * 4 + 12] = -0.5*(pow(owner_u[ipoint * 4 + 1], 2.0) + pow(owner_u[ipoint * 4 + 2], 2.0)) / pow(owner_u[ipoint * 4], 2.0);
		jacobian[ipoint * 4 + 13] = owner_u[ipoint * 4 + 1] / owner_u[ipoint * 4];
		jacobian[ipoint * 4 + 14] = owner_u[ipoint * 4 + 2] / owner_u[ipoint * 4];
		jacobian[ipoint * 4 + 15] = -1.0; // added
	}
	return jacobian;
}

const std::vector<real_t> Euler2DBCSubIn::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	return flux_->numConvFlux(owner_u, neighbor_u, normal);
}
const std::vector<real_t> Euler2DBCSubIn::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	std::vector<real_t> FluxJacobi = flux_->numConvFluxOwnerJacobian(owner_u, neighbor_u, normal);
	std::vector<real_t> FluxNeighborJacobi = flux_->numConvFluxNeighborJacobian(owner_u, neighbor_u, normal);
	std::vector<real_t> solution_jacobian(num_states_*num_states_, 0.0);
	solution_jacobian[0] = -1.0; solution_jacobian[5] = -1.0; solution_jacobian[10] = -1.0;
	solution_jacobian[12] = (owner_u[1] * owner_u[1] + owner_u[2] * owner_u[2]) / (owner_u[0] * owner_u[0]);
	solution_jacobian[13] = -2.0*owner_u[1] / owner_u[0]; solution_jacobian[14] = -2.0*owner_u[2] / owner_u[0];
	solution_jacobian[15] = 1.0;

	for (int_t i = 0; i < num_states_; i++)
		for (int_t j = 0; j < num_states_; j++)
			for (int_t k = 0; k < num_states_; k++)
				FluxJacobi[i*num_states_ + j] += FluxNeighborJacobi[i*num_states_ + k] * solution_jacobian[k*num_states_ + j];
	return FluxJacobi;
}
const std::vector<real_t> Euler2DBCSubIn::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFlux(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> Euler2DBCSubIn::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	std::vector<real_t> FluxJacobi = flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
	std::vector<real_t> FluxNeighborJacobi = flux_->numViscFluxNeighborJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
	std::vector<real_t> solution_jacobian(num_states_*num_states_, 0.0);
	solution_jacobian[0] = -1.0; solution_jacobian[5] = -1.0; solution_jacobian[10] = -1.0;
	solution_jacobian[12] = (owner_u[1] * owner_u[1] + owner_u[2] * owner_u[2]) / (owner_u[0] * owner_u[0]);
	solution_jacobian[13] = -2.0*owner_u[1] / owner_u[0]; solution_jacobian[14] = -2.0*owner_u[2] / owner_u[0];
	solution_jacobian[15] = 1.0;

	for (int_t i = 0; i < num_states_; i++)
		for (int_t j = 0; j < num_states_; j++)
			for (int_t k = 0; k < num_states_; k++)
				FluxJacobi[i*num_states_ + j] += FluxNeighborJacobi[i*num_states_ + k] * solution_jacobian[k*num_states_ + j];
	return FluxJacobi;
}
const std::vector<real_t> Euler2DBCSubIn::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	std::vector<real_t> FluxGradJacobi = flux_->numViscFluxOwnerGradJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
	std::vector<real_t> FluxNeighborGradJacobi = flux_->numViscFluxNeighborGradJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
	for (int_t ivar = 0; ivar < FluxGradJacobi.size(); ivar++)
		FluxGradJacobi[ivar] += FluxNeighborGradJacobi[ivar];
	return FluxGradJacobi;
}
const std::vector<real_t> Euler2DBCSubIn::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u(owner_u.size());
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++) {
		/*neighbor_u[ipoint * 4 + 0] = 1.0;
		neighbor_u[ipoint * 4 + 1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
		neighbor_u[ipoint * 4 + 2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
		neighbor_u[ipoint * 4 + 3] = 2.5*pressure[ipoint] + 0.5*(neighbor_u[ipoint * 4 + 1] * neighbor_u[ipoint * 4 + 1] + neighbor_u[ipoint * 4 + 2] * neighbor_u[ipoint * 4 + 2]);*/
		neighbor_u[ipoint * 4 + 0] = 2.0 - owner_u[ipoint * 4];
		neighbor_u[ipoint * 4 + 1] = 2.0 * references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - owner_u[ipoint * 4 + 1];
		neighbor_u[ipoint * 4 + 2] = 2.0 * references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - owner_u[ipoint * 4 + 2];
		neighbor_u[ipoint * 4 + 3] = 5.0*pressure[ipoint] + (pow(references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0), 2.0)
			+ pow(references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0), 2.0)) - owner_u[ipoint * 4 + 3];
	}
	return neighbor_u;
}
const std::vector<real_t> Euler2DBCSubIn::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> jacobian(owner_u.size() * 4, 0.0);
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++) {
		jacobian[ipoint * 16] = -1.0;
		jacobian[ipoint * 16 + 5] = -1.0;
		jacobian[ipoint * 16 + 10] = -1.0;
		jacobian[ipoint * 16 + 12] = (pow(owner_u[ipoint * 4 + 1], 2.0) + pow(owner_u[ipoint * 4 + 2], 2.0)) / pow(owner_u[ipoint * 4], 2.0);
		jacobian[ipoint * 16 + 13] = -2.0*owner_u[ipoint * 4 + 1] / owner_u[ipoint * 4];
		jacobian[ipoint * 16 + 14] = -2.0*owner_u[ipoint * 4 + 2] / owner_u[ipoint * 4];
		//jacobian[ipoint * 16 + 15] = 1.0;
	}
	return jacobian;
}

const std::vector<real_t> Euler2DBCSupOut::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFlux(owner_u, normal);
}
const std::vector<real_t> Euler2DBCSupOut::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFluxJacobian(owner_u, normal);
}
const std::vector<real_t> Euler2DBCSupOut::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFlux(owner_u, owner_div_u, normal);
}
const std::vector<real_t> Euler2DBCSupOut::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> Euler2DBCSupOut::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxGradJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> Euler2DBCSupOut::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return owner_u;
}
const std::vector<real_t> Euler2DBCSupOut::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	//std::vector<real_t> jacobian(owner_u.size() * 4, 0.0);
	//for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++)
	//	for (int_t istate = 0; istate < 4; istate++)
	//		jacobian[ipoint * 16 + istate * 5] = 1.0;
	//return jacobian;
	return std::vector<real_t>(owner_u.size() * 4, 0.0);
}

const std::vector<real_t> Euler2DBCSupIn::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	return flux_->commonConvFlux(neighbor_u, normal);
}
const std::vector<real_t> Euler2DBCSupIn::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	std::vector<real_t> FluxJacobi = flux_->commonConvFluxJacobian(neighbor_u, normal);
	for (int_t ivar = 0; ivar < FluxJacobi.size(); ivar++)
		FluxJacobi[ivar] *= -1.0;
	return FluxJacobi;
}
const std::vector<real_t> Euler2DBCSupIn::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFlux(neighbor_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> Euler2DBCSupIn::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	std::vector<real_t> FluxJacobi = flux_->numViscFluxOwnerJacobian(neighbor_u, owner_div_u, neighbor_u, owner_div_u, normal);
	for (int_t ivar = 0; ivar < FluxJacobi.size(); ivar++)
		FluxJacobi[ivar] *= -2.0;
	return FluxJacobi;
}
const std::vector<real_t> Euler2DBCSupIn::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	std::vector<real_t> FluxGradJacobi = flux_->numViscFluxOwnerGradJacobian(neighbor_u, owner_div_u, neighbor_u, owner_div_u, normal);
	for (int_t ivar = 0; ivar < FluxGradJacobi.size(); ivar++)
		FluxGradJacobi[ivar] *= 2.0;
	return FluxGradJacobi;
}
const std::vector<real_t> Euler2DBCSupIn::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> neighbor_u(owner_u.size());
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		/*neighbor_u[ipoint * 4 + 0] = 1.0;
		neighbor_u[ipoint * 4 + 1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
		neighbor_u[ipoint * 4 + 2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
		neighbor_u[ipoint * 4 + 3] = 2.5 / 1.4 + 0.5*(neighbor_u[ipoint * 4 + 1] * neighbor_u[ipoint * 4 + 1] + neighbor_u[ipoint * 4 + 2] * neighbor_u[ipoint * 4 + 2]);*/
		neighbor_u[ipoint * 4 + 0] = 2.0 - owner_u[ipoint * 4];
		neighbor_u[ipoint * 4 + 1] = 2.0*references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - owner_u[ipoint * 4 + 1];
		neighbor_u[ipoint * 4 + 2] = 2.0*references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - owner_u[ipoint * 4 + 2];
		neighbor_u[ipoint * 4 + 3] = 5.0 / 1.4 + (pow(references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0), 2.0)
			+ pow(references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0), 2.0)) - owner_u[ipoint * 4 + 3];
	}
	return neighbor_u;
}
const std::vector<real_t> Euler2DBCSupIn::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> jacobian(owner_u.size() * 4, 0.0);
	for (int_t ipoint = 0; ipoint < owner_u.size() / 4; ipoint++)
		for (int_t istate = 0; istate < 4; istate++)
			jacobian[ipoint * 16 + istate * 5] = -1.0;
	return jacobian;
}

const std::vector<real_t> Euler2DBCFarField::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> Flux(num_points * 4);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		std::vector<real_t> neighbor_u(4);
		std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 4, owner_u.begin() + (ipoint + 1) * 4);
		std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
		std::vector<real_t> local_flux;

		const real_t aL = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 4]);
		const real_t VL = (owner_u[ipoint * 4 + 1] * normal[ipoint * 2] + owner_u[ipoint * 4 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 4];
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2] + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2 + 1]);
		real_t RL, Rf;
		if (std::abs(Vf) >= 1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
			}
			else {
				RL = VL + 5.0*aL;
			}
			if (VL < 0.0) {
				Rf = VL - 5.0*aL;
			}
			else {
				Rf = Vf - 5.0;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
		}
		real_t rho, U, V;
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*(RL - Rf)*0.01, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2] * Vf;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2 + 1] * Vf;
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 4], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + owner_u[ipoint * 4 + 1] / owner_u[ipoint * 4] - normal[ipoint * 2] * VL;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + owner_u[ipoint * 4 + 2] / owner_u[ipoint * 4] - normal[ipoint * 2 + 1] * VL;
		}
		real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		neighbor_u[0] = rho;
		neighbor_u[1] = rho*U;
		neighbor_u[2] = rho*V;
		neighbor_u[3] = 2.5*p + 0.5*rho*(U*U + V*V);

		local_flux = flux_->numConvFlux(local_owner_u, neighbor_u, local_normal);

		Flux[ipoint * 4 + 0] = local_flux[0];
		Flux[ipoint * 4 + 1] = local_flux[1];
		Flux[ipoint * 4 + 2] = local_flux[2];
		Flux[ipoint * 4 + 3] = local_flux[3];
	}
	return Flux;
}
const std::vector<real_t> Euler2DBCFarField::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> FluxJacobi(num_points * 16);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	std::vector<real_t> neighbor_u(num_states_);
	std::vector<real_t> local_flux_jacobi;
	std::vector<real_t> local_flux_neighbor_jacobi;
	std::vector<real_t> solution_jacobian(num_states_*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 4, owner_u.begin() + (ipoint + 1) * 4);
		std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
		const real_t& dL = owner_u[ipoint * 4];
		const real_t& duL = owner_u[ipoint * 4 + 1];
		const real_t& dvL = owner_u[ipoint * 4 + 2];
		const real_t& dEL = owner_u[ipoint * 4 + 3];
		const real_t& nx = normal[ipoint * 2];
		const real_t& ny = normal[ipoint * 2 + 1];
		const real_t aL = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 4]);
		const real_t VL = (owner_u[ipoint * 4 + 1] * normal[ipoint * 2] + owner_u[ipoint * 4 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 4];
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2] + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2 + 1]);
		real_t RL, Rf;
		std::vector<real_t> jacobian_RL(4, 0.0), jacobian_Rf(4, 0.0);
		if (std::abs(Vf) >= 1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
				Rf = Vf - 5.0;
			}
			else {
				RL = VL + 5.0*aL;
				Rf = VL - 5.0*aL;
				jacobian_RL[0] = -VL / dL + 2.0*(pow(duL, 2.0) + pow(dvL, 2.0) - dL * dEL) / pow(dL, 3.0);
				jacobian_RL[1] = (dL * nx - 2.0*duL) / pow(dL, 2.0);
				jacobian_RL[2] = (dL * ny - 2.0*dvL) / pow(dL, 2.0);
				jacobian_RL[3] = 2.0 / dL;
				jacobian_Rf[0] = -VL / dL - 2.0*(pow(duL, 2.0) + pow(dvL, 2.0) - dL * dEL) / pow(dL, 3.0);
				jacobian_Rf[1] = (dL * nx + 2.0*duL) / pow(dL, 2.0);
				jacobian_Rf[2] = (dL * ny + 2.0*dvL) / pow(dL, 2.0);
				jacobian_Rf[3] = -2.0 / dL;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
			jacobian_RL[0] = -VL / dL + 2.0*(pow(duL, 2.0) + pow(dvL, 2.0) - dL * dEL) / pow(dL, 3.0);
			jacobian_RL[1] = (dL * nx - 2.0*duL) / pow(dL, 2.0);
			jacobian_RL[2] = (dL * ny - 2.0*dvL) / pow(dL, 2.0);
			jacobian_RL[3] = 2.0 / dL;
		}
		real_t rho, U, V;
		std::vector<real_t> jacobian_rho(4, 0.0), jacobian_U(4, 0.0), jacobian_V(4, 0.0);
		const std::vector<real_t> jacobian_P = { 0.2*(pow(duL, 2.0) + pow(dvL, 2.0)) / pow(dL, 2.0), -0.4*duL / dL, -0.4*dvL / dL, 0.4 };
		const std::vector<real_t> jacobian_d = { 1.0, 0, 0, 0 };
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*0.1, 5.0);
			U = 0.5*nx * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - nx * Vf;
			V = 0.5*ny * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - ny * Vf;
			for (int_t istate = 0; istate < 4; istate++)
			{
				jacobian_rho[istate] = 0.00005*pow(RL - Rf, 4.0)*(jacobian_RL[istate] - jacobian_Rf[istate]);
				jacobian_U[istate] = 0.5*nx*(jacobian_RL[istate] + jacobian_Rf[istate]);
				jacobian_V[istate] = 0.5*ny*(jacobian_RL[istate] + jacobian_Rf[istate]);
			}
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 4], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*nx * (RL + Rf) + duL / dL - nx * VL;
			V = 0.5*ny * (RL + Rf) + dvL / dL - ny * VL;
			for (int_t istate = 0; istate < 4; istate++)
			{
				jacobian_rho[istate] = 2.5*rho*(2.0*(jacobian_RL[istate] - jacobian_Rf[istate]) / (RL - Rf) + 1.4 / duL * jacobian_d[istate] - jacobian_P[istate] / pressure[ipoint]);
				jacobian_U[istate] = 0.5*nx*(jacobian_RL[istate] + jacobian_Rf[istate]);
				jacobian_V[istate] = 0.5*ny*(jacobian_RL[istate] + jacobian_Rf[istate]);
			}
			jacobian_U[0] -= ((1.0 - nx * nx)*duL - dvL * nx*ny) / pow(dL, 2.0);
			jacobian_U[1] += (1.0 - nx * nx) / dL;
			jacobian_U[2] -= nx * ny / dL;
			jacobian_V[0] -= ((1.0 - ny * ny)*dvL - duL * nx*ny) / pow(dL, 2.0);
			jacobian_V[1] -= nx * ny / dL;
			jacobian_V[2] += (1.0 - ny * ny) / dL;
		}
		const real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		std::vector<real_t> jacobian_p(4, 0.0);
		for (int_t istate = 0; istate < 4; istate++)
			jacobian_p[istate] = (2.0*(RL - Rf)*rho*(jacobian_RL[istate] - jacobian_Rf[istate]) + pow(RL - Rf, 2.0)*jacobian_rho[istate]) / 140.0;

		neighbor_u[0] = rho;
		neighbor_u[1] = rho * U;
		neighbor_u[2] = rho * V;
		neighbor_u[3] = 2.5*p + 0.5*rho*(U*U + V * V);

		for (int_t istate = 0; istate < 4; istate++)
		{
			solution_jacobian[istate] = jacobian_rho[istate];
			solution_jacobian[4 + istate] = jacobian_rho[istate] * U + rho * jacobian_U[istate];
			solution_jacobian[8 + istate] = jacobian_rho[istate] * V + rho * jacobian_V[istate];
			solution_jacobian[12 + istate] = 2.5*jacobian_p[istate] + 0.5*jacobian_rho[istate] * (U*U + V * V) + rho * (U*jacobian_U[istate] + V * jacobian_V[istate]);
		}

		local_flux_jacobi = flux_->numConvFluxOwnerJacobian(local_owner_u, neighbor_u, local_normal);
		local_flux_neighbor_jacobi = flux_->numConvFluxNeighborJacobian(local_owner_u, neighbor_u, local_normal);

		for (int_t i = 0; i < num_states_; i++)
			for (int_t j = 0; j < num_states_; j++)
			{
				FluxJacobi[ipoint * 16 + i * num_states_ + j] = local_flux_jacobi[i*num_states_ + j];
				for (int_t k = 0; k < num_states_; k++)
					FluxJacobi[ipoint * 16 + i * num_states_ + j] += local_flux_neighbor_jacobi[i*num_states_ + k] * solution_jacobian[k*num_states_ + j];
			}
	}
	return FluxJacobi;
}
const std::vector<real_t> Euler2DBCFarField::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> ViscFlux(num_points * 4);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		std::vector<real_t> neighbor_u(4);
		std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 4, owner_u.begin() + (ipoint + 1) * 4);
		std::vector<real_t> local_owner_div_u(owner_div_u.begin() + ipoint * 8, owner_div_u.begin() + (ipoint + 1) * 8);
		std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
		std::vector<real_t> local_viscflux;

		const real_t aL = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 4]);
		const real_t VL = (owner_u[ipoint * 4 + 1] * normal[ipoint * 2] + owner_u[ipoint * 4 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 4];
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2] + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2 + 1]);
		real_t RL, Rf;
		if (std::abs(Vf) >= 1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
			}
			else {
				RL = VL + 5.0*aL;
			}
			if (VL < 0.0) {
				Rf = VL - 5.0*aL;
			}
			else {
				Rf = Vf - 5.0;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
		}
		real_t rho, U, V;
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*(RL - Rf)*0.01, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2] * Vf;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2 + 1] * Vf;
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 4], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + owner_u[ipoint * 4 + 1] / owner_u[ipoint * 4] - normal[ipoint * 2] * VL;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + owner_u[ipoint * 4 + 2] / owner_u[ipoint * 4] - normal[ipoint * 2 + 1] * VL;
		}
		real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		neighbor_u[0] = rho;
		neighbor_u[1] = rho*U;
		neighbor_u[2] = rho*V;
		neighbor_u[3] = 2.5*p + 0.5*rho*(U*U + V*V);

		local_viscflux = flux_->numViscFlux(local_owner_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);

		ViscFlux[ipoint * 4 + 0] = local_viscflux[0];
		ViscFlux[ipoint * 4 + 1] = local_viscflux[1];
		ViscFlux[ipoint * 4 + 2] = local_viscflux[2];
		ViscFlux[ipoint * 4 + 3] = local_viscflux[3];
	}
	return ViscFlux;
}
const std::vector<real_t> Euler2DBCFarField::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> ViscFluxJacobi(num_points * 16);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	std::vector<real_t> neighbor_u(num_states_);
	std::vector<real_t> local_flux_jacobi;
	std::vector<real_t> local_flux_neighbor_jacobi;
	std::vector<real_t> solution_jacobian(num_states_*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 4, owner_u.begin() + (ipoint + 1) * 4);
		std::vector<real_t> local_owner_div_u(owner_div_u.begin() + ipoint * 8, owner_div_u.begin() + (ipoint + 1) * 8);
		std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
		const real_t& dL = owner_u[ipoint * 4];
		const real_t& duL = owner_u[ipoint * 4 + 1];
		const real_t& dvL = owner_u[ipoint * 4 + 2];
		const real_t& dEL = owner_u[ipoint * 4 + 3];
		const real_t& nx = normal[ipoint * 2];
		const real_t& ny = normal[ipoint * 2 + 1];
		const real_t aL = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 4]);
		const real_t VL = (owner_u[ipoint * 4 + 1] * normal[ipoint * 2] + owner_u[ipoint * 4 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 4];
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2] + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2 + 1]);
		real_t RL, Rf;
		std::vector<real_t> jacobian_RL(4, 0.0), jacobian_Rf(4, 0.0);
		if (std::abs(Vf) >= 1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
				Rf = Vf - 5.0;
			}
			else {
				RL = VL + 5.0*aL;
				Rf = VL - 5.0*aL;
				jacobian_RL[0] = -VL / dL + 2.0*(pow(duL, 2.0) + pow(dvL, 2.0) - dL * dEL) / pow(dL, 3.0);
				jacobian_RL[1] = (dL * nx - 2.0*duL) / pow(dL, 2.0);
				jacobian_RL[2] = (dL * ny - 2.0*dvL) / pow(dL, 2.0);
				jacobian_RL[3] = 2.0 / dL;
				jacobian_Rf[0] = -VL / dL - 2.0*(pow(duL, 2.0) + pow(dvL, 2.0) - dL * dEL) / pow(dL, 3.0);
				jacobian_Rf[1] = (dL * nx + 2.0*duL) / pow(dL, 2.0);
				jacobian_Rf[2] = (dL * ny + 2.0*dvL) / pow(dL, 2.0);
				jacobian_Rf[3] = -2.0 / dL;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
			jacobian_RL[0] = -VL / dL + 2.0*(pow(duL, 2.0) + pow(dvL, 2.0) - dL * dEL) / pow(dL, 3.0);
			jacobian_RL[1] = (dL * nx - 2.0*duL) / pow(dL, 2.0);
			jacobian_RL[2] = (dL * ny - 2.0*dvL) / pow(dL, 2.0);
			jacobian_RL[3] = 2.0 / dL;
		}
		real_t rho, U, V;
		std::vector<real_t> jacobian_rho(4, 0.0), jacobian_U(4, 0.0), jacobian_V(4, 0.0);
		const std::vector<real_t> jacobian_P = { 0.2*(pow(duL, 2.0) + pow(dvL, 2.0)) / pow(dL, 2.0), -0.4*duL / dL, -0.4*dvL / dL, 0.4 };
		const std::vector<real_t> jacobian_d = { 1.0, 0, 0, 0 };
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*0.1, 5.0);
			U = 0.5*nx * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - nx * Vf;
			V = 0.5*ny * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - ny * Vf;
			for (int_t istate = 0; istate < 4; istate++)
			{
				jacobian_rho[istate] = 0.00005*pow(RL - Rf, 4.0)*(jacobian_RL[istate] - jacobian_Rf[istate]);
				jacobian_U[istate] = 0.5*nx*(jacobian_RL[istate] + jacobian_Rf[istate]);
				jacobian_V[istate] = 0.5*ny*(jacobian_RL[istate] + jacobian_Rf[istate]);
			}
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 4], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*nx * (RL + Rf) + duL / dL - nx * VL;
			V = 0.5*ny * (RL + Rf) + dvL / dL - ny * VL;
			for (int_t istate = 0; istate < 4; istate++)
			{
				jacobian_rho[istate] = 2.5*rho*(2.0*(jacobian_RL[istate] - jacobian_Rf[istate]) / (RL - Rf) + 1.4 / duL * jacobian_d[istate] - jacobian_P[istate] / pressure[ipoint]);
				jacobian_U[istate] = 0.5*nx*(jacobian_RL[istate] + jacobian_Rf[istate]);
				jacobian_V[istate] = 0.5*ny*(jacobian_RL[istate] + jacobian_Rf[istate]);
			}
			jacobian_U[0] -= ((1.0 - nx * nx)*duL - dvL * nx*ny) / pow(dL, 2.0);
			jacobian_U[1] += (1.0 - nx * nx) / dL;
			jacobian_U[2] -= nx * ny / dL;
			jacobian_V[0] -= ((1.0 - ny * ny)*dvL - duL * nx*ny) / pow(dL, 2.0);
			jacobian_V[1] -= nx * ny / dL;
			jacobian_V[2] += (1.0 - ny * ny) / dL;
		}
		const real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		std::vector<real_t> jacobian_p(4, 0.0);
		for (int_t istate = 0; istate < 4; istate++)
			jacobian_p[istate] = (2.0*(RL - Rf)*rho*(jacobian_RL[istate] - jacobian_Rf[istate]) + pow(RL - Rf, 2.0)*jacobian_rho[istate]) / 140.0;

		neighbor_u[0] = rho;
		neighbor_u[1] = rho * U;
		neighbor_u[2] = rho * V;
		neighbor_u[3] = 2.5*p + 0.5*rho*(U*U + V * V);

		for (int_t istate = 0; istate < 4; istate++)
		{
			solution_jacobian[istate] = jacobian_rho[istate];
			solution_jacobian[4 + istate] = jacobian_rho[istate] * U + rho * jacobian_U[istate];
			solution_jacobian[8 + istate] = jacobian_rho[istate] * V + rho * jacobian_V[istate];
			solution_jacobian[12 + istate] = 2.5*jacobian_p[istate] + 0.5*jacobian_rho[istate] * (U*U + V * V) + rho * (U*jacobian_U[istate] + V * jacobian_V[istate]);
		}

		local_flux_jacobi = flux_->numViscFluxOwnerJacobian(local_owner_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);
		local_flux_neighbor_jacobi = flux_->numViscFluxNeighborJacobian(local_owner_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);

		for (int_t i = 0; i < num_states_; i++)
			for (int_t j = 0; j < num_states_; j++)
			{
				ViscFluxJacobi[ipoint * 16 + i * num_states_ + j] = local_flux_jacobi[i*num_states_ + j];
				for (int_t k = 0; k < num_states_; k++)
					ViscFluxJacobi[ipoint * 16 + i * num_states_ + j] += local_flux_neighbor_jacobi[i*num_states_ + k] * solution_jacobian[k*num_states_ + j];
			}
	}
	return ViscFluxJacobi;
}

const std::vector<real_t> Euler2DBCFarField::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> ViscFluxGradJacobi(num_points * 32);
	std::vector<real_t> local_viscflux_grad_jacobi;
	std::vector<real_t> local_viscflux_neighbor_grad_jacobi;
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		std::vector<real_t> neighbor_u(4);
		std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 4, owner_u.begin() + (ipoint + 1) * 4);
		std::vector<real_t> local_owner_div_u(owner_div_u.begin() + ipoint * 8, owner_div_u.begin() + (ipoint + 1) * 8);
		std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);

		const real_t aL = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 4]);
		const real_t VL = (owner_u[ipoint * 4 + 1] * normal[ipoint * 2] + owner_u[ipoint * 4 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 4];
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2] + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2 + 1]);
		real_t RL, Rf;
		if (std::abs(Vf) >= 1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
			}
			else {
				RL = VL + 5.0*aL;
			}
			if (VL < 0.0) {
				Rf = VL - 5.0*aL;
			}
			else {
				Rf = Vf - 5.0;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
		}
		real_t rho, U, V;
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*(RL - Rf)*0.01, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2] * Vf;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2 + 1] * Vf;
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 4], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + owner_u[ipoint * 4 + 1] / owner_u[ipoint * 4] - normal[ipoint * 2] * VL;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + owner_u[ipoint * 4 + 2] / owner_u[ipoint * 4] - normal[ipoint * 2 + 1] * VL;
		}
		real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		neighbor_u[0] = rho;
		neighbor_u[1] = rho * U;
		neighbor_u[2] = rho * V;
		neighbor_u[3] = 2.5*p + 0.5*rho*(U*U + V * V);

		local_viscflux_grad_jacobi = flux_->numViscFluxOwnerGradJacobian(local_owner_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);
		local_viscflux_neighbor_grad_jacobi = flux_->numViscFluxNeighborGradJacobian(local_owner_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);

		for (int_t ivar = 0; ivar < 32; ivar++)
			ViscFluxGradJacobi[ipoint * 32 + ivar] = local_viscflux_grad_jacobi[ivar] + local_viscflux_neighbor_grad_jacobi[ivar];
	}
	return ViscFluxGradJacobi;
}

const std::vector<real_t> Euler2DBCFarField::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> neighbor_u(owner_u.size());
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t aL = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 4]);
		const real_t VL = (owner_u[ipoint * 4 + 1] * normal[ipoint * 2] + owner_u[ipoint * 4 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 4];
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2] + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2 + 1]);
		real_t RL, Rf;
		if (std::abs(Vf)>=1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
			}
			else {
				RL = VL + 5.0*aL;
			}
			if (VL < 0.0) {
				Rf = VL - 5.0*aL;
			}
			else {
				Rf = Vf - 5.0;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
		}
		real_t rho, U, V;
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*(RL - Rf)*0.01, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2] * Vf;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2 + 1] * Vf;
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 4], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + owner_u[ipoint * 4 + 1] / owner_u[ipoint * 4] - normal[ipoint * 2] * VL;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + owner_u[ipoint * 4 + 2] / owner_u[ipoint * 4] - normal[ipoint * 2 + 1] * VL;
		}
		real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		neighbor_u[ipoint * 4 + 0] = rho;
		neighbor_u[ipoint * 4 + 1] = rho*U;
		neighbor_u[ipoint * 4 + 2] = rho*V;
		neighbor_u[ipoint * 4 + 3] = 2.5*p + 0.5*rho*(U*U + V*V);
	}
	return neighbor_u;
}
const std::vector<real_t> Euler2DBCFarField::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 4;
	std::vector<real_t> solution_jacobian(owner_u.size() * 4);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& dL = owner_u[ipoint * 4];
		const real_t& duL = owner_u[ipoint * 4 + 1];
		const real_t& dvL = owner_u[ipoint * 4 + 2];
		const real_t& dEL = owner_u[ipoint * 4 + 3];
		const real_t& nx = normal[ipoint * 2];
		const real_t& ny = normal[ipoint * 2 + 1];
		const real_t aL = std::sqrt(1.4*pressure[ipoint] / dL);
		const real_t VL = (duL * nx + dvL * ny) / dL;
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*nx + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*ny);
		real_t RL, Rf;
		std::vector<real_t> jacobian_RL(4, 0.0), jacobian_Rf(4, 0.0);
		if (std::abs(Vf) >= 1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
				Rf = Vf - 5.0;
			}
			else {
				RL = VL + 5.0*aL;
				Rf = VL - 5.0*aL;
				jacobian_RL[0] = -VL / dL + 2.0*(pow(duL, 2.0) + pow(dvL, 2.0) - dL * dEL) / pow(dL, 3.0);
				jacobian_RL[1] = (dL * nx - 2.0*duL) / pow(dL, 2.0);
				jacobian_RL[2] = (dL * ny - 2.0*dvL) / pow(dL, 2.0);
				jacobian_RL[3] = 2.0 / dL;
				jacobian_Rf[0] = -VL / dL - 2.0*(pow(duL, 2.0) + pow(dvL, 2.0) - dL * dEL) / pow(dL, 3.0);
				jacobian_Rf[1] = (dL * nx + 2.0*duL) / pow(dL, 2.0);
				jacobian_Rf[2] = (dL * ny + 2.0*dvL) / pow(dL, 2.0);
				jacobian_Rf[3] = -2.0 / dL;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
			jacobian_RL[0] = -VL / dL + 2.0*(pow(duL, 2.0) + pow(dvL, 2.0) - dL * dEL) / pow(dL, 3.0);
			jacobian_RL[1] = (dL * nx - 2.0*duL) / pow(dL, 2.0);
			jacobian_RL[2] = (dL * ny - 2.0*dvL) / pow(dL, 2.0);
			jacobian_RL[3] = 2.0 / dL;
		}
		real_t rho, U, V;
		std::vector<real_t> jacobian_rho(4, 0.0), jacobian_U(4, 0.0), jacobian_V(4, 0.0);
		const std::vector<real_t> jacobian_P = { 0.2*(pow(duL, 2.0) + pow(dvL, 2.0)) / pow(dL, 2.0), -0.4*duL / dL, -0.4*dvL / dL, 0.4 };
		const std::vector<real_t> jacobian_d = { 1.0, 0, 0, 0 };
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*0.1, 5.0);
			U = 0.5*nx * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - nx * Vf;
			V = 0.5*ny * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - ny * Vf;
			for (int_t istate = 0; istate < 4; istate++)
			{
				jacobian_rho[istate] = 0.00005*pow(RL - Rf, 4.0)*(jacobian_RL[istate] - jacobian_Rf[istate]);
				jacobian_U[istate] = 0.5*nx*(jacobian_RL[istate] + jacobian_Rf[istate]);
				jacobian_V[istate] = 0.5*ny*(jacobian_RL[istate] + jacobian_Rf[istate]);
			}
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 4], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*nx * (RL + Rf) + duL / dL - nx * VL;
			V = 0.5*ny * (RL + Rf) + dvL / dL - ny * VL;
			for (int_t istate = 0; istate < 4; istate++)
			{
				jacobian_rho[istate] = 2.5*rho*(2.0*(jacobian_RL[istate] - jacobian_Rf[istate]) / (RL - Rf) + 1.4 / duL * jacobian_d[istate] - jacobian_P[istate] / pressure[ipoint]);
				jacobian_U[istate] = 0.5*nx*(jacobian_RL[istate] + jacobian_Rf[istate]);
				jacobian_V[istate] = 0.5*ny*(jacobian_RL[istate] + jacobian_Rf[istate]);
			}
			jacobian_U[0] -= ((1.0 - nx * nx)*duL - dvL * nx*ny) / pow(dL, 2.0);
			jacobian_U[1] += (1.0 - nx * nx) / dL;
			jacobian_U[2] -= nx * ny / dL;
			jacobian_V[0] -= ((1.0 - ny * ny)*dvL - duL * nx*ny) / pow(dL, 2.0);
			jacobian_V[1] -= nx * ny / dL;
			jacobian_V[2] += (1.0 - ny * ny) / dL;
		}
		const real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		std::vector<real_t> jacobian_p(4, 0.0);
		for (int_t istate = 0; istate < 4; istate++)
			jacobian_p[istate] = (2.0*(RL - Rf)*rho*(jacobian_RL[istate] - jacobian_Rf[istate]) + pow(RL - Rf, 2.0)*jacobian_rho[istate]) / 140.0;
		
		for (int_t istate = 0; istate < 4; istate++)
		{
			solution_jacobian[ipoint * 16 + istate] = 0.5*jacobian_rho[istate];
			solution_jacobian[ipoint * 16 + 4 + istate] = 0.5*(jacobian_rho[istate] * U + rho * jacobian_U[istate]);
			solution_jacobian[ipoint * 16 + 8 + istate] = 0.5*(jacobian_rho[istate] * V + rho * jacobian_V[istate]);
			solution_jacobian[ipoint * 16 + 12 + istate] = 1.25*jacobian_p[istate] + 0.25*jacobian_rho[istate] * (U*U + V * V) + rho * (U*jacobian_U[istate] + V * jacobian_V[istate]);
		}
		solution_jacobian[ipoint * 16] -= 0.5;
		solution_jacobian[ipoint * 16 + 5] -= 0.5;
		solution_jacobian[ipoint * 16 + 10] -= 0.5;
		solution_jacobian[ipoint * 16 + 15] -= 0.5;
	}
	return solution_jacobian;
}