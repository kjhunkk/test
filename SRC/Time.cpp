
#include "../INC/Time.h"

Time::Time()
{
	const Config& config = Config::getInstance();

	print_option_ = config.getPrintOption();
	save_option_ = config.getSaveOption();
	timescheme_ = TimeScheme::getInstance().getType(config.getTimeSchemeName());
	timescheme_->initialize(); // for implicit
	itermax_ = config.getIterMax();
	start_time_ = config.getStartTime();
	end_time_ = config.getEndTime();
	CFL_ = config.getCFL();
	pseudo_CFL_min_ = config.getPseudoCFLmin(); // for implicit
	pseudo_CFL_max_ = config.getPseudoCFLmax(); // for implicit
	SER_up_factor_ = config.getSERupfactor(); // for implciit
	SER_start_error_ = config.getSERstarterror(); // for implicit
	if (pseudo_CFL_max_ - pseudo_CFL_min_ < 10e-6) // for implicit
		is_variable_cfl_ = false; // for implicit
	else is_variable_cfl_ = true; // for implicit

	current_time_ = 0.0;
	iteration_ = 0;
	dt_ = 0.0;
	complete_ = false;
	strandid_ = 1;

	std::string hs = TO_UPPER(config.getHistory());
	if (!hs.compare("ON")) history_ = true;
	else history_ = false;
}

void Time::integrate(std::shared_ptr<Zone> zone)
{
	const Config& config = Config::getInstance();
	std::string rs = TO_UPPER(config.getRestart());
	const bool is_fixed_dt = timescheme_->getIsFixedDt();
	
	// for implicit
	if (timescheme_->getIsImplicit())
	{
		zone->initializeRHSImplicit();
		zone->initializeImpOperator();
	}
	else zone->initializeRHS();
	// for implicit

	if (!rs.compare("ON")) {
		zone->read(current_time_, iteration_, strandid_);
		post_->updatePreSolution();
		//iteration_ = 1; // for test
	}
	else {
		// zero iteration / for implicit
		printMessage(timescheme_->getStatusString()); // for implicit
		if (printCheck())
			post_->postDuringSolution(strandid_, current_time_, iteration_);
		if (saveCheck())
			zone->save(current_time_, time_gap_.count(), iteration_, strandid_);
		if (history_)
			post_->postHistory(iteration_);
		iteration_++;
		// for implicit
	}

	start_clock_ = std::chrono::system_clock::now();
	for (iteration_; iteration_ <= itermax_; iteration_++){ // for implicit iteration <= itermax_
		if (history_)
			post_->updatePreSolution();
		
		if (is_variable_cfl_) zone->setCFL(getUpdatedCFL(timescheme_->getRelError())); // for implicit
		if (is_fixed_dt) setTimeStep(config.getPhysicalTimeStep()); // for implicit
		//if (is_fixed_dt)
		//{
		//	if (iteration_ < 4) setTimeStep((1.0e-2)*config.getPhysicalTimeStep());
		//	else if (iteration_ == 4) setTimeStep((1.0 - 3.0e-2)*config.getPhysicalTimeStep());
		//	else setTimeStep(config.getPhysicalTimeStep()); // for implicit
		//}
		else setTimeStep(zone->getTimeStep()); // for implicit
		timescheme_->integrate(zone, dt_, current_time_, iteration_);
		
		printMessage(timescheme_->getStatusString()); // for implicit
		if (printCheck())
			post_->postDuringSolution(strandid_, current_time_, iteration_);
		if (saveCheck())
			zone->save(current_time_, time_gap_.count(), iteration_, strandid_);
		if (history_)
			post_->postHistory(iteration_);
		zone->saveApprox(current_time_, time_gap_.count(), iteration_, strandid_); // for implicit (order test)

#ifdef _Pressure_Fix_Numoff_
		if(iteration_ == _Pressure_Fix_Numoff_)
			zone->setPressureFix(false);
#endif
		if (complete_ == true) break;
	}
	now_clock_ = std::chrono::system_clock::now();
	std::chrono::duration<real_t> finish = now_clock_ - start_clock_; // total time cost
	zone->communication();
	zone->limiting();
	zone->save(current_time_, finish.count(), iteration_, strandid_);
}

void Time::setTimeStep(real_t dt)
{
	if (current_time_ + dt >= end_time_){
		dt_ = end_time_ - current_time_;
		current_time_ = end_time_;
		complete_ = true;
	}
	else{
		dt_ = dt;
		current_time_ += dt_;
	}
}

void Time::printMessage(const std::string& input_message) // for implicit
{
	now_clock_ = std::chrono::system_clock::now();
	time_gap_ = now_clock_ - start_clock_;
	char sentence[100];
	//sprintf(sentence, "Iter=%-d  CalT=%.3lf (%.3lf%%) PhyT=%.3lf %-.1lfs remain", iteration_, current_time_, 100.0*current_time_ / end_time_, time_gap.count(), time_gap.count()*(end_time_ / current_time_ - 1.0));
	if (!timescheme_->getIsFixedDt())
	{
		if (is_variable_cfl_)
			sprintf(sentence, "Iter=%d CFL=%f CalT=%lf (%lf%%) PhyT=%lf %lfs remain ", iteration_, CFL_, current_time_, 100.0*current_time_ / end_time_, time_gap_.count(), time_gap_.count()*(end_time_ / current_time_ - 1.0)); // for implicit
		else sprintf(sentence, "Iter=%d  CalT=%lf (%lf%%) PhyT=%lf %lfs remain ", iteration_, current_time_, 100.0*current_time_ / end_time_, time_gap_.count(), time_gap_.count()*(end_time_ / current_time_ - 1.0)); // for implicit
	}
	else sprintf(sentence, "Iter=%d (%lf%%) CalT=%lf PhyT=%lf %lfs remain ", iteration_, 100.0*(double)iteration_ / (double)itermax_, current_time_, time_gap_.count(), time_gap_.count()*((double)itermax_ / (double)iteration_ - 1.0)); // for implicit
	MASTER_MESSAGE(std::string(sentence) + input_message);
}

real_t Time::getUpdatedCFL(const real_t relative_error) // for implicit
{
	CFL_ = pseudo_CFL_min_;
	CFL_ += SER_up_factor_ * log10(SER_start_error_ / relative_error);
	CFL_ = std::min(std::max(CFL_, pseudo_CFL_min_), pseudo_CFL_max_);
	return CFL_;
}

bool Time::printCheck()
{
	if (print_option_[1] == -1){
		if ((print_option_[0] <= iteration_)){
			if (iteration_%print_option_[2] == 0) return true;
			return false;
		}
	}
	if ((print_option_[0] <= iteration_) && (iteration_ <= print_option_[1])){
		if (iteration_%print_option_[2] == 0) return true;
		return false;
	}
	return false;
}

bool Time::saveCheck()
{
	if (save_option_[1] == -1){
		if ((save_option_[0] <= iteration_)){
			if (iteration_%save_option_[2] == 0) return true;
			return false;
		}
	}
	if ((save_option_[0] <= iteration_) && (iteration_ <= save_option_[1])){
		if (iteration_%save_option_[2] == 0) return true;
		return false;
	}
	return false;
}