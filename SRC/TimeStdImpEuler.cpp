
#include "../INC/TimeStdImpEuler.h"

TimeStdImpEuler::TimeStdImpEuler()
{
	setName("SteadyImplicitEuler");
	TimeScheme::getInstance().addRegistry(getName(), this);
	is_implicit_ = true;
}

TimeStdImpEuler::~TimeStdImpEuler()
{
	ierr_ = VecDestroy(&delta_);
}

void TimeStdImpEuler::initialize()
{
	const Config& config = Config::getInstance();
	is_fixed_dt_ = false;
	is_initialized_ = false;
	linear_solver_iteration_ = 0;
	pseudo_iteration_ = 0;
	relative_error_ = 1.0;
	for (int_t ires = 0; ires < residual_norms_.size(); ires++)
		residual_norms_[ires] = 100.0;

	// RHS vector
	ierr_ = VecCreate(MPI_COMM_WORLD, &delta_);

	// Linear system solver initialize
	linear_system_solver_ = LinearSystemSolver::getInstance().getType(config.getLinearSystemSolver());
	linear_system_solver_->Initialize();
}

void TimeStdImpEuler::integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
	const int_t num_cell = zone->getNumCells();

	// Initializing delta
	if (is_initialized_ == false)
	{
		ierr_ = VecDuplicate(zone->getSystemRHS(), &delta_);
		is_initialized_ = true;
	}

	zone->communication();
	const Vec& rhs = zone->calculateSystemRHS();
	const real_t diagonal_factor = 1.0 / dt;
	const Mat& system_matrix = zone->calculateSystemMatrix(diagonal_factor, 1.0);

	// Calculate residual norms
	//residual_norms_[0] = residual_norms_[1];
	//ierr_ = VecNorm(rhs, NORM_2, &residual_norms_[1]);
	
	// Krylov solve
	ierr_ = linear_system_solver_->Solve(system_matrix, rhs, delta_);
	linear_solver_iteration_ = linear_system_solver_->getIterationNumber();
	computeError(rhs, pseudo_iteration_++);
	status_string_ = "KrylovIteration=" + TO_STRING(linear_solver_iteration_) + " L2=" + TO_STRING((double)relative_error_);
	//if (iteration % 5 == 1)
	//{
		//linear_system_solver_->PostMatrix(system_matrix, "Matrix" + TO_STRING(iteration));
		//MatView(system_matrix, PETSC_VIEWER_STDOUT_WORLD);
		//linear_system_solver_->PostVector(rhs, "bVector" + TO_STRING(iteration));
		//linear_system_solver_->PostVector(delta_, "xVector" + TO_STRING(iteration));
	//}

	// Solution update
	zone->UpdateSolutionFromDelta(delta_);

	//zone->UpdatePseudoCFL(residual_norms_);

	zone->communication();
	zone->limiting();
	zone->pressurefix(current_time, iteration);
}

void TimeStdImpEuler::computeError(const Vec& input_vector, const int_t iteration)
{
	PetscReal error_sum = 0.0;
	//VecNorm(delta_, NORM_2, &error_sum);
	VecNorm(input_vector, NORM_2, &error_sum);
	if (iteration == 0)
	{
		reference_error_ = error_sum;
	}
	else if (iteration < 5)
	{
		if (error_sum > reference_error_)
		{
			reference_error_ = error_sum;
		}
	}
	relative_error_ = error_sum / reference_error_;
}