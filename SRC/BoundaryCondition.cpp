
#include "../INC/BoundaryCondition.h"

void BoundaryCondition::readBC(const std::string& file_name)
{
	std::string bc_file_name = INPUT() + file_name;
	std::ifstream infile(bc_file_name.c_str());

	std::string text;
	char* str;

	MASTER_MESSAGE("----------------Read Boundary Data----------------");
	if (infile.is_open()) MASTER_MESSAGE("Open boundary data file : input/boundary.dat");
	else MASTER_ERROR("Fail to open configuration file : input/boundary.dat");

	while (getline(infile, text))
	{
		str = (char*)(text.c_str());

		char* pch = strtok(str, " \t");
		if (!strcmp(pch, "//")){}
		else if (!strcmp(pch, "references")){
			std::vector<real_t> references;
			pch = strtok(NULL, " \t");
			while (pch != NULL){
				references.push_back(std::atof(pch));
				pch = strtok(NULL, " \t");
			}
			references_.push_back(references);
			references_index_[name_.size() - 1] = references_.size() - 1;
			MASTER_LISTPRINT("References : ", references);
		}
		else{
			std::string name(pch);
			std::string type(strtok(NULL, " \t"));
			int_t tag_number = std::atoi(strtok(NULL, " \t"));
			name_.push_back(name);
			type_.push_back(TO_UPPER(type));
			tag_number_.push_back(tag_number);
			references_index_.push_back(-1);
			MASTER_MESSAGE("Read boundary(name: " + name + ", type: " + type + ", tag_number: " + TO_STRING(tag_number) + ")");
		}
	}
	MASTER_MESSAGE("--------------------------------------------------");
	infile.close();
}
const std::string& BoundaryCondition::getBCType(int_t tag_number)
{
	for (int_t ibc = 0; ibc < tag_number_.size(); ibc++){
		if (tag_number_[ibc] == tag_number)
			return type_[ibc];
	}
	MEMBER_ERROR("Tag number (" + TO_STRING(tag_number) + ") is not registered");
}
const std::string& BoundaryCondition::getBCName(int_t tag_number)
{
	for (int_t ibc = 0; ibc < tag_number_.size(); ibc++){
		if (tag_number_[ibc] == tag_number)
			return name_[ibc];
	}
	MEMBER_ERROR("Tag number (" + TO_STRING(tag_number) + ") is not registered");
}
int_t BoundaryCondition::getTagNumber(const std::string& type)
{
	std::string tmp = TO_UPPER(type);
	for (int_t ibc = 0; ibc < type_.size(); ibc++){
		if (!tmp.compare(type_[ibc]))
			return tag_number_[ibc];
	}
	return -1;
}
const std::vector<real_t> BoundaryCondition::getBCReferences(int_t tag_number)
{
	for (int_t ibc = 0; ibc < tag_number_.size(); ibc++){
		if (tag_number_[ibc] == tag_number){
			if (references_index_[ibc] == -1) break;

			return references_[references_index_[ibc]];
		}
	}
	return {0.0};
}