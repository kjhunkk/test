
#include <memory>

#include "../INC/DataType.h"
#include "../INC/MPIHeader.h"
#include "../INC/BoundaryCondition.h"
#include "../INC/GridReader.h"
#include "../INC/Config.h"
#include "../INC/Grid.h"
#include "../INC/Zone.h"
#include "../INC/SAEquation.h"
#include "../INC/SAFlux.h"
#include "../INC/SABoundary.h"
#include "../INC/BGEquation.h"
#include "../INC/BGFlux.h"
#include "../INC/BGBoundary.h"
#include "../INC/Euler2DEquation.h"
#include "../INC/Euler2DFlux.h"
#include "../INC/Euler2DBoundary.h"
#include "../INC/TurbSA2DEquation.h"
#include "../INC/TurbSA2DFlux.h"
#include "../INC/TurbSA2DBoundary.h"
#include "../INC/Time.h"
#include "../INC/TimeScheme.h"
#include "../INC/TimeStdImpEuler.h" // for implicit
#include "../INC/TimeUnstImpBDF2.h" // for implicit
#include "../INC/TimeUnstMEBDF.h" // for implicit
#include "../INC/TimeUnstLMEBDF.h" // for implicit
#include "../INC/TimeUnstRosenbrock.h" // for implicit
#include "../INC/TimeUnstTIAS.h" // for implicit
#include "../INC/LinearSystemGMRES.h" // for implicit
#include "../INC/Limiter.h"
#include "../INC/Post.h"

std::string MPIHeader::code_name_ = "Deneb";
MPIHeader MPIHeader::MPI_header_;
BoundaryCondition BoundaryCondition::boundary_condition_;
Config Config::config_;
// Grid reader
GmshGridReader GmshGridReader::gmsh_grid_reader_;
Gmsh2DTrisGridReader Gmsh2DTrisGridReader::gmsh2dtris_grid_reader_;
Gmsh2DQuadGridReader Gmsh2DQuadGridReader::gmsh2dquad_grid_reader_;
Gmsh3DTetsGridReader Gmsh3DTetsGridReader::gmsh3dtets_grid_reader_;
Gmsh3DHexaGridReader Gmsh3DHexaGridReader::gmsh3dhexa_grid_reader_;
Gmsh3DPrisGridReader Gmsh3DPrisGridReader::gmsh3dpris_grid_reader_;
// Scalar advection
SAEquation SAEquation::equation_;
SAConstant SAConstant::SA_Constant_;
SASquare SASquare::SA_Square_;
SADoubleSine SADoubleSine::SA_double_sine_;
SAUserDefine SAUserDefine::SA_UserDefine_;
SAUpwind SAUpwind::SA_upwind_;
SABCExtrapolate SABCExtrapolate::SA_extrapolate_;
SABCConstant SABCConstant::SA_constant_;
// Burgers' equation
BGEquation BGEquation::equation_;
BGConstant BGConstant::BG_Constant_;
BGDoubleSine BGDoubleSine::BG_double_sine_;
BGUserDefine BGUserDefine::BG_UserDefine_;
BGUpwind BGUpwind::BG_upwind_;
BGBCExtrapolate BGBCExtrapolate::BG_extrapolate_;
BGBCConstant BGBCConstant::BG_constant_;
// Euler 2D equations
Euler2DEquation Euler2DEquation::equation_;
Euler2DSodTest Euler2DSodTest::Euler_2d_sodtest_;
Euler2DShuOsher Euler2DShuOsher::Euler_2d_shuosher_;
Euler2DExplosion Euler2DExplosion::Euler_2d_explosion_;
Euler2DShockVortex Euler2DShockVortex::Euler_2d_shockvortex_;
Euler2DRiemann Euler2DRiemann::Euler_2d_riemann_;
Euler2DFreeStream Euler2DFreeStream::Euler_2d_freestream_;
Euler2DUserShockTube Euler2DUserShockTube::Euler_2d_user_shocktube_;
Euler2DDoubleMach Euler2DDoubleMach::Euler_2d_double_mach_;
Euler2DShockWedge Euler2DShockWedge::Euler_2d_shock_wedge_;
Euler2DVortexTransport Euler2DVortexTransport::Euler_2d_vortex_transport_;
Euler2DDoubleSine Euler2DDoubleSine::Euler_2d_double_sine_;
Euler2DTravellingWaves Euler2DTravellingWaves::Euler_2d_travelling_waves_;
Euler2DUserDefine Euler2DUserDefine::Euler_2d_user_define_;
Euler2DLLF Euler2DLLF::Euler_2d_LLF_;
Euler2DRoe Euler2DRoe::Euler_2d_Roe_;
Euler2DAUSM Euler2DAUSM::Euler_2d_ausm_;
Euler2DBCExtrapolate Euler2DBCExtrapolate::Euler_2d_extrapolate_;
Euler2DBCConstant Euler2DBCConstant::Euler_2d_constant_;
Euler2DBCWall Euler2DBCWall::Euler_2d_wall_;
Euler2DBCSlipWall Euler2DBCSlipWall::Euler_2d_slipwall_;
Euler2DBCSubOut Euler2DBCSubOut::Euler_2d_subout_;
Euler2DBCSubIn Euler2DBCSubIn::Euler_2d_subin_;
Euler2DBCSupOut Euler2DBCSupOut::Euler_2d_supout_;
Euler2DBCSupIn Euler2DBCSupIn::Euler_2d_supin_;
Euler2DBCFarField Euler2DBCFarField::Euler_2d_farfield_;
// Turbulence S-A model 2D equations
TurbSA2DEquation TurbSA2DEquation::equation_;
TurbSA2DFreeStream TurbSA2DFreeStream::TurbSA_2d_freestream_;
TurbSA2DUserDefine TurbSA2DUserDefine::TurbSA_2d_user_define_;
TurbSA2DLLF TurbSA2DLLF::TurbSA_2d_LLF_;
TurbSA2DRoe TurbSA2DRoe::TurbSA_2d_Roe_;
TurbSA2DBCExtrapolate TurbSA2DBCExtrapolate::TurbSA_2d_extrapolate_;
TurbSA2DBCConstant TurbSA2DBCConstant::TurbSA_2d_constant_;
TurbSA2DBCWall TurbSA2DBCWall::TurbSA_2d_wall_;
TurbSA2DBCSlipWall TurbSA2DBCSlipWall::TurbSA_2d_slipwall_;
TurbSA2DBCSubOut TurbSA2DBCSubOut::TurbSA_2d_subout_;
TurbSA2DBCSubIn TurbSA2DBCSubIn::TurbSA_2d_subin_;
TurbSA2DBCSupOut TurbSA2DBCSupOut::TurbSA_2d_supout_;
TurbSA2DBCSupIn TurbSA2DBCSupIn::TurbSA_2d_supin_;
TurbSA2DBCFarField TurbSA2DBCFarField::TurbSA_2d_farfield_;
// Time scheme
ExplicitEuler ExplicitEuler::explicit_euler_;
TVDRK TVDRK::tvdrk_;
SSPRK SSPRK::ssprk_;
TimeStdImpEuler TimeStdImpEuler::std_imp_euler_; // for implicit
TimeUnstImpBDF2 TimeUnstImpBDF2::ustd_imp_bdf2_; // for implicit
TimeUnstMEBDF TimeUnstMEBDF::unst_imp_mebdf_; // for implicit
TimeUnstLMEBDF TimeUnstLMEBDF::unst_imp_lmebdf_; // for implicit
RosenbrockROS3P RosenbrockROS3P::rosenbrock_ros3p_; // for implicit
RosenbrockRODAS3 RosenbrockRODAS3::rosenbrock_rodas3_; // for implicit
RosenbrockROS4 RosenbrockROS4::rosenbrock_ros4_; // for implicit
RosenbrockRODASP RosenbrockRODASP::rosenbrock_rodasp_; // for implicit
RosenbrockRODASS RosenbrockRODASS::rosenbrock_rodass_; // for implicit
RosenbrockROW6A RosenbrockROW6A::rosenbrock_row6a_; // for implicit
TimeUnstTIAS TimeUnstTIAS::unst_imp_tias_; // for implicit
LinearSystemGMRES LinearSystemGMRES::gmres_; // for implicit
// Limiting scheme
NoneLimiter NoneLimiter::none_limiter_;
hMLP hMLP::hmlp_;
hMLP_SIM hMLP_SIM::hmlp_sim_;
hMLPBD hMLPBD::hmlp_bd_;
hMLPBD_SIM hMLPBD_SIM::hmlp_bd_sim_;

void problem() {
	std::shared_ptr<Grid> grid = std::make_shared<Grid>();
	grid->initialize();
	
	std::shared_ptr<Zone> zone = std::make_shared<Zone>();
	zone->setGrid(grid);
	zone->initialize();
	
	std::shared_ptr<Post> post = std::make_shared<Post>();
	post->initialize(grid, zone);
	//post->postSolution("initial", 0.0); // for debug
	
	std::shared_ptr<Time> time = std::make_shared<Time>();
	time->setPost(post);
	time->integrate(zone);
	
	zone->orderTest();
	post->postSolution("post", 0.0);
	//post->postSolution("postavg", 0.0, true);
}

int main(int argc, char* argv[])
{
	BoundaryCondition::getInstance().readBC("boundary.dat");
	Config &config = Config::getInstance();
	config.readConfigFile("config.dat");

	if(argc == 1) problem();
	else
	{
		std::string file_name;
		file_name.append(argv[1]);
		std::string file_name_dir = INPUT() + file_name;
		std::ifstream infile(file_name_dir.c_str());

		MASTER_MESSAGE("-----------------Read Input Array------------------");
		if (infile.is_open()) MASTER_MESSAGE("Open configuration file : " + file_name);
		else MASTER_ERROR("Fail to open configuration file : " + file_name);

		std::string text;
		char* str;
		int_t number = -1;
		while (getline(infile, text))
		{
			str = (char*)(text.c_str());

			char* pch = strtok(str, " \t");
			if (!strcmp(pch, "//")) {}
			else
			{
				number = atoi(pch);
				pch = strtok(NULL, " \t"); config.setIterMax(std::atoi(pch));
				pch = strtok(NULL, " \t"); config.setPhysicalTimeStep(std::atof(pch));
				pch = strtok(NULL, " \t"); config.setTimeSchemeName(pch);
				pch = strtok(NULL, " \t"); config.setTimeOrder(std::atoi(pch));
				pch = strtok(NULL, " \t"); config.setReternAddress(pch);

				config.renewal();
				problem();
			}
		}
	}
	
	return 0;
}