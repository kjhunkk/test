
#include "../INC/Jacobian.h"

std::shared_ptr<Jacobian> JacobianFactory::getJacobian(ElemType element_type, const int_t order)
{
	switch (element_type)
	{
	case ElemType::TRIS: return std::make_shared<TrisJacobian>(order);
	case ElemType::QUAD: return std::make_shared<QuadJacobian>(order);
	case ElemType::TETS: return std::make_shared<TetsJacobian>(order);
	case ElemType::HEXA: return std::make_shared<HexaJacobian>(order);
	case ElemType::PRIS: return std::make_shared<PrisJacobian>(order);
	}
	return std::shared_ptr<Jacobian>();
}

std::shared_ptr<Jacobian> JacobianFactory::getFaceJacobian(ElemType face_element_type, const int_t order)
{
	switch (face_element_type)
	{
	case ElemType::LINE: return std::make_shared<LineFaceJacobian>(order);
	case ElemType::TRIS: return std::make_shared<TrisFaceJacobian>(order);
	case ElemType::QUAD: return std::make_shared<QuadFaceJacobian>(order);
	}
	return std::shared_ptr<Jacobian>();
}

// jacobian math
const std::vector<real_t> Jacobian::JacobianMath::monomialLine(const int_t order, const real_t r)
{
	std::vector<real_t> results(order + 1);
	results[0] = 1.0;
	for (int_t i = 1; i < order + 1; i++)
		results[i] = results[i - 1] * r;
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialTris(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results((order + 1)*(order + 2) / 2);
	results[0] = 1.0;
	int_t index = 1;
	for (int_t i = 1; i <= order; i++){
		results[index] = results[index - i] * r;
		index++;
		for (int_t j = 0; j < i; j++){
			results[index] = results[index - i - 1] * s;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialQuad(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results((order + 1)*(order + 1));
	results[0] = 1.0;
	int_t index = 1;
	for (int_t i = 1; i <= order; i++){
		results[index] = results[index - 2 * i + 1] * r;
		index++;
		for (int_t j = 0; j < i; j++){
			results[index] = results[index - 1] * s;
			index++;
		}
		for (int_t j = 0; j < i; j++){
			results[index] = results[index - 2 * i - 1] * s;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialTets(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 2)*(order + 3) / 6);
	results[0] = 1.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++){
		results[index] = results[bef_index] * r;
		index++;
		def = index - bef_index;
		bef_index = index - 1;
		for (int_t j = 1; j <= i; j++){
			results[index] = results[index - def] * s;
			index++; def++;
			for (int_t k = 1; k <= j; k++){
				results[index] = results[index - def] * t;
				index++;
			}
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialHexa(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 1)*(order + 1));
	results[0] = 1.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++){
		def = index - bef_index;
		results[index] = results[bef_index] * r;
		bef_index = index;
		index++;
		for (int_t j = 1; j <= i; j++){ // bottom
			results[index] = results[index - 1] * s;
			index++;
		}
		def += 2;
		for (int_t j = 1; j <= i; j++){
			results[index] = results[index - def] * s;
			index++;
		}
		def = 2 * i + 1;
		for (int_t j = 1; j < i; j++){ // wall
			for (int_t k = 0; k < def; k++){
				results[index] = results[index - def] * t;
				index++;
			}
		}
		// top
		def = i*(3 * i + 1);
		for (int_t j = 0; j < i; j++){
			for (int_t k = 0; k < 2 * j + 1; k++){
				results[index] = results[index - def] * t;
				index++;
			}
		}
		def = (i + 1)*(i + 1);
		for (int_t j = 0; j < 2 * i + 1; j++){
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialPris(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 1)*(order + 2) / 2);
	results[0] = 1.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		results[index] = results[bef_index] * r;
		index++;
		def = index - bef_index;
		bef_index = index - 1;
		for (int_t j = 1; j <= i; j++) { // bottom
			results[index] = results[index - def] * s;
			index++;
		}
		def = i + 1;
		for (int_t j = 1; j < i; j++) { // wall
			for (int_t k = 0; k < def; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		// top
		def = i*(i + 1) * 3 / 2;
		for (int_t j = 1; j <= i; j++) {
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		def = (i + 1)*(i + 2) / 2;
		for (int_t j = 0; j < i + 1; j++) {
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDrLine(const int_t order, const real_t r)
{
	std::vector<real_t> results(order + 1);
	results[0] = 0.0;
	results[1] = 1.0;
	for (int_t i = 2; i < order + 1; i++)
		results[i] = real_t(i) / real_t(i - 1)*results[i - 1] * r;
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDrTris(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results((order + 1)*(order + 2) / 2);
	const std::vector<real_t> monomials = monomialDrLine(order, r);
	int_t index = 0;
	for (int_t i = 0; i <= order; i++){
		results[index] = monomials[i];
		index++;
		for (int_t j = 0; j < i; j++){
			results[index] = results[index - i - 1] * s;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDsTris(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results((order + 1)*(order + 2) / 2);
	const std::vector<real_t> monomials = monomialDrLine(order, s);
	int_t index = 0;
	for (int_t i = 0; i <= order; i++){
		for (int_t j = 0; j < i; j++){
			results[index] = results[index - i] * r;
			index++;
		}
		results[index] = monomials[i];
		index++;
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDrQuad(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results((order + 1)*(order + 1));
	const std::vector<real_t> monomials = monomialDrLine(order, r);

	int_t index = 0;
	for (int_t i = 0; i <= order; i++){
		results[index] = monomials[i];
		index++;
		for (int_t j = 0; j < i; j++){
			results[index] = results[index - 1] * s;
			index++;
		}
		for (int_t j = 0; j < i; j++){
			results[index] = results[index - 2 * i - 1] * s;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDsQuad(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results((order + 1)*(order + 1));
	const std::vector<real_t> monomials = monomialDrLine(order, s);

	results[0] = 0.0;
	int_t index = 1;
	for (int_t i = 1; i <= order; i++){
		for (int_t j = 0; j < i; j++){
			results[index] = results[index - 2 * i + 1] * r;
			index++;
		}
		index = (i + 1)*(i + 1) - 1;
		results[index] = monomials[i];
		index--;
		for (int_t j = 0; j < i; j++){
			results[index] = results[index + 1] * r;
			index--;
		}
		index = (i + 1)*(i + 1);
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDrTets(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 2)*(order + 3) / 6);
	const std::vector<real_t> monomials = monomialDrLine(order, r);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++){
		results[index] = monomials[i];
		index++;
		def = index - bef_index;
		bef_index = index - 1;
		for (int_t j = 1; j <= i; j++){
			results[index] = results[index - def] * s;
			index++; def++;
			for (int_t k = 1; k <= j; k++){
				results[index] = results[index - def] * t;
				index++;
			}
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDsTets(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 2)*(order + 3) / 6);
	const std::vector<real_t> monomials = monomialDrLine(order, s);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++){
		def = index - bef_index;
		bef_index = index;
		for (int_t j = 0; j < i; j++){
			for (int_t k = 0; k <= j; k++){
				results[index] = results[index - def] * r;
				index++;
			}
		}
		results[index] = monomials[i];
		index++;
		def += (i + 1);
		for (int_t k = 1; k <= i; k++){
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDtTets(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 2)*(order + 3) / 6);
	const std::vector<real_t> monomials = monomialDrLine(order, t);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++){
		def = index - bef_index;
		bef_index = index;
		for (int_t j = 0; j < i; j++){
			for (int_t k = 0; k <= j; k++){
				results[index] = results[index - def] * r;
				index++;
			}
		}
		def += i;
		for (int_t k = 0; k < i; k++){
			results[index] = results[index - def] * s;
			index++;
		}
		results[index] = monomials[i];
		index++;
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDrHexa(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 1)*(order + 1));
	const std::vector<real_t> monomials = monomialDrLine(order, r);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++){
		results[index] = monomials[i];
		def = index - bef_index;
		bef_index = index;
		index++;
		for (int_t j = 1; j <= i; j++){ // bottom
			results[index] = results[index - 1] * s;
			index++;
		}
		def += 2;
		for (int_t j = 1; j < i; j++){
			results[index] = results[index - def] * s;
			index++;
		}
		results[index++] = 0.0;
		def = 2 * i + 1;
		for (int_t j = 1; j < i; j++){ // wall
			for (int_t k = 1; k < def; k++){
				results[index] = results[index - def] * t;
				index++;
			}
			results[index++] = 0.0;
		}
		// top
		def = i*(3 * i + 1);
		for (int_t j = 0; j < i; j++){
			for (int_t k = 1; k < 2 * j + 1; k++){
				results[index] = results[index - def] * t;
				index++;
			}
			results[index++] = 0.0;
		}
		def = (i + 1)*(i + 1);
		for (int_t j = 1; j < 2 * i + 1; j++){
			results[index] = results[index - def] * t;
			index++;
		}
		results[index++] = 0.0;
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDsHexa(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 1)*(order + 1));
	const std::vector<real_t> monomials = monomialDrLine(order, s);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++){
		def = index - bef_index;
		bef_index = index;
		results[index++] = 0.0;
		for (int_t j = 1; j < i; j++){// bottom
			results[index] = results[index - def] * r;
			index++;
		}
		index += i;
		results[index] = monomials[i];
		for (int_t j = 1; j <= i; j++){
			index--;
			results[index] = results[index + 1] * r;
		}
		index += (i + 1);
		def = 2 * i + 1;
		for (int_t j = 1; j < i; j++){ // wall
			results[index++] = 0.0;
			for (int_t k = 1; k < def; k++){
				results[index] = results[index - def] * t;
				index++;
			}
		}
		// top
		def = i*(3 * i + 1);
		for (int_t j = 0; j < i; j++){
			results[index++] = 0.0;
			for (int_t k = 1; k < 2 * j + 1; k++){
				results[index] = results[index - def] * t;
				index++;
			}
		}
		def = (i + 1)*(i + 1);
		results[index++] = 0.0;
		for (int_t j = 1; j < 2 * i + 1; j++){
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDtHexa(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 1)*(order + 1));
	const std::vector<real_t> monomials = monomialDrLine(order, t);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++){
		def = index - bef_index + 2;
		bef_index = index;
		for (int_t j = 0; j < 2*i + 1; j++){ // bottom
			results[index++] = 0.0;
		}
		for (int_t j = 1; j < i; j++){ // wall
			if (j == i - 1){
				def -= (i - 1)*(i - 1);
			}
			results[index] = results[index - def] * r;
			index++;
			for (int_t k = 1; k <= i; k++){
				results[index] = results[index - 1] * s;
				index++;
			}
			def += 2;
			for (int_t k = 1; k <= i; k++){
				results[index] = results[index - def] * s;
				index++;
			}
		}
		// top
		results[index++] = monomials[i];
		def = 1;
		for (int_t j = 1; j <= i; j++){
			results[index] = results[index - def] * r;
			index++;
			for (int_t k = 1; k <= j; k++){
				results[index] = results[index - 1] * s;
				index++;
			}
			def += 2;
			for (int_t k = 1; k <= j; k++){
				results[index] = results[index - def] * s;
				index++;
			}
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDrPris(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 1)*(order + 2) / 2);
	const std::vector<real_t> monomials = monomialDrLine(order, r);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		results[index] = monomials[i];
		index++;
		def = index - bef_index;
		bef_index = index - 1;
		for (int_t j = 1; j <= i; j++) { // bottom
			results[index] = results[index - def] * s;
			index++;
		}
		def = i + 1;
		for (int_t j = 1; j < i; j++) { // wall
			for (int_t k = 0; k < def; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		// top
		def = i*(i + 1) * 3 / 2;
		for (int_t j = 1; j <= i; j++) {
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		def = (i + 1)*(i + 2) / 2;
		for (int_t j = 0; j < i + 1; j++) {
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDsPris(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 1)*(order + 2) / 2);
	const std::vector<real_t> monomials = monomialDrLine(order, s);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		def = index - bef_index;
		bef_index = index;
		results[index++] = 0.0;
		for (int_t j = 1; j < i; j++) { // bottom
			results[index] = results[index - def] * r;
			index++;
		}
		results[index++] = monomials[i];
		def = i + 1;
		for (int_t j = 1; j < i; j++) { // wall
			for (int_t k = 0; k < def; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		// top
		def = i*(i + 1) * 3 / 2;
		for (int_t j = 1; j <= i; j++) {
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		def = (i + 1)*(i + 2) / 2;
		for (int_t j = 0; j < i + 1; j++) {
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Jacobian::JacobianMath::monomialDtPris(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results((order + 1)*(order + 1)*(order + 2) / 2);
	const std::vector<real_t> monomials = monomialDrLine(order, t);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		def = index - bef_index;
		bef_index = index;

		for (int_t j = 1; j <= i + 1; j++) { // bottom
			results[index++] = 0.0;
		}

		def++;
		for (int_t j = 1; j < i; j++) { // wall
			if (j == i - 1) {
				def = i*(i + 1) - 1;
			}
			results[index] = results[index - def] * r;
			index++;
			def++;
			for (int_t k = 1; k <= i; k++) {
				results[index] = results[index - def] * s;
				index++;
			}
		}

		// top
		results[index++] = monomials[i];
		def = 1;
		for (int_t j = 1; j <= i; j++) {
			results[index] = results[index - def] * r;
			index++;
			def++;
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - def] * s;
				index++;
			}
		}
	}
	return results;
}

// triangle
const std::vector<std::vector<real_t>> TrisJacobian::calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords)
{
	int_t nbasis = (element_order + 1)*(element_order + 2) / 2;
	int_t npoint = coords.size() / 2;

	arma::mat rs(nbasis, nbasis);

	Element* tris = ElementFactory::getInstance().getElement(element_type_);
	const std::vector<real_t>& node_numbering = tris->getNodesCoord(element_order);
	for (int_t i = 0; i < nbasis; i++){
		const real_t r = node_numbering[2 * i];
		const real_t s = node_numbering[2 * i + 1];
		const std::vector<real_t> rs_row = JacobianMath::monomialTris(element_order, r, s);
		for (int_t j = 0; j < nbasis; j++){
			rs(i, j) = rs_row[j];
		}
	}
	rs = arma::inv(rs);
	std::vector<std::vector<real_t>> comtophy(2);
	for (int_t i = 0; i < comtophy.size(); i++){
		arma::mat xy(nbasis, 1);
		for (int_t j = 0; j < nbasis; j++){
			xy(j, 0) = coords[2 * j + i];
		}
		xy = rs*xy;
		comtophy[i].resize(nbasis);
		for (int_t j = 0; j < nbasis; j++){
			comtophy[i][j] = xy(j, 0);
		}
	}
	return comtophy;
}
const std::vector<real_t> TrisJacobian::calComToPhyCoord(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 2;
	std::vector<real_t> phy(num_points * 2);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		for (int_t i = 0; i < comtophy_coefficients_.size(); i++){
			const real_t r = coord[2 * ipoint];
			const real_t s = coord[2 * ipoint + 1];
			phy[2 * ipoint + i] = Poly(comtophy_coefficients_[i], JacobianMath::monomialTris(element_order_, r, s));
		}
	}
	return phy;
}
const std::vector<real_t> TrisJacobian::calJacobian(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 2;
	std::vector<real_t> jacobi(num_points);
	arma::mat jac(2, 2);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r = coord[2 * ipoint];
		const real_t s = coord[2 * ipoint + 1];
		for (int_t i = 0; i < 2; i++){
			jac(0, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDrTris(element_order_, r, s));
			jac(1, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDsTris(element_order_, r, s));
		}
		jacobi[ipoint] = std::abs(arma::det(jac));
	}
	return jacobi;
}
const std::vector<real_t> TrisJacobian::calJacobianAdjMat(const std::vector<real_t>& coord) const // adj = det(J)inv(J)^T
{
	int_t num_points = coord.size() / 2;
	std::vector<real_t> jacobi(4 * num_points);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r = coord[2 * ipoint];
		const real_t s = coord[2 * ipoint + 1];
		jacobi[4 * ipoint] = Poly(comtophy_coefficients_[1], JacobianMath::monomialDsTris(element_order_, r, s));
		jacobi[4 * ipoint + 1] = -Poly(comtophy_coefficients_[1], JacobianMath::monomialDrTris(element_order_, r, s));
		jacobi[4 * ipoint + 2] = -Poly(comtophy_coefficients_[0], JacobianMath::monomialDsTris(element_order_, r, s));
		jacobi[4 * ipoint + 3] = Poly(comtophy_coefficients_[0], JacobianMath::monomialDrTris(element_order_, r, s));
	}
	return jacobi;
}

// quadrilateral
const std::vector<std::vector<real_t>> QuadJacobian::calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords)
{
	int_t nbasis = (element_order + 1)*(element_order + 1);
	int_t npoint = coords.size() / 2;

	arma::mat rs(nbasis, nbasis);
	arma::mat rs_row(1, nbasis);

	Element* quad = ElementFactory::getInstance().getElement(element_type_);
	const std::vector<real_t>& node_numbering = quad->getNodesCoord(element_order);
	for (int_t i = 0; i < nbasis; i++){
		const real_t r = node_numbering[2 * i];
		const real_t s = node_numbering[2 * i + 1];
		const std::vector<real_t> rs_row = JacobianMath::monomialQuad(element_order, r, s);
		for (int_t j = 0; j < nbasis; j++){
			rs(i, j) = rs_row[j];
		}
	}
	rs = arma::inv(rs);
	std::vector<std::vector<real_t>> comtophy(2);
	for (int_t i = 0; i < comtophy.size(); i++){
		arma::mat xy(nbasis, 1);
		for (int_t j = 0; j < nbasis; j++){
			xy(j, 0) = coords[2 * j + i];
		}
		xy = rs*xy;
		comtophy[i].resize(nbasis);
		for (int_t j = 0; j < nbasis; j++){
			comtophy[i][j] = xy(j, 0);
		}
	}
	return comtophy;
}
const std::vector<real_t> QuadJacobian::calComToPhyCoord(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 2;
	std::vector<real_t> phy(num_points * 2);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		for (int_t i = 0; i < comtophy_coefficients_.size(); i++){
			const real_t r = coord[2 * ipoint];
			const real_t s = coord[2 * ipoint + 1];
			phy[2 * ipoint + i] = Poly(comtophy_coefficients_[i], JacobianMath::monomialQuad(element_order_, r, s));
		}
	}
	return phy;
}
const std::vector<real_t> QuadJacobian::calJacobian(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 2;
	std::vector<real_t> jacobi(num_points);
	arma::mat jac(2, 2);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r = coord[2 * ipoint];
		const real_t s = coord[2 * ipoint + 1];
		for (int_t i = 0; i < 2; i++){
			jac(0, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDrQuad(element_order_, r, s));
			jac(1, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDsQuad(element_order_, r, s));
		}
		jacobi[ipoint] = std::abs(arma::det(jac));
	}
	return jacobi;
}
const std::vector<real_t> QuadJacobian::calJacobianAdjMat(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 2;
	std::vector<real_t> jacobi(4 * num_points);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r = coord[2 * ipoint];
		const real_t s = coord[2 * ipoint + 1];
		jacobi[4 * ipoint] = Poly(comtophy_coefficients_[1], JacobianMath::monomialDsQuad(element_order_, r, s));
		jacobi[4 * ipoint + 1] = -Poly(comtophy_coefficients_[1], JacobianMath::monomialDrQuad(element_order_, r, s));
		jacobi[4 * ipoint + 2] = -Poly(comtophy_coefficients_[0], JacobianMath::monomialDsQuad(element_order_, r, s));
		jacobi[4 * ipoint + 3] = Poly(comtophy_coefficients_[0], JacobianMath::monomialDrQuad(element_order_, r, s));
	}
	return jacobi;
}

// tetrahedral
const std::vector<std::vector<real_t>> TetsJacobian::calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords)
{
	int_t nbasis = (element_order + 1)*(element_order + 2)*(element_order + 3) / 6;
	int_t npoint = coords.size() / 3;

	arma::mat rst(nbasis, nbasis);
	arma::mat rst_row(1, nbasis);

	Element* tets = ElementFactory::getInstance().getElement(element_type_);
	const std::vector<real_t>& node_numbering = tets->getNodesCoord(element_order);
	for (int_t i = 0; i < nbasis; i++){
		const real_t r = node_numbering[3 * i];
		const real_t s = node_numbering[3 * i + 1];
		const real_t t = node_numbering[3 * i + 2];
		const std::vector<real_t> rst_row = JacobianMath::monomialTets(element_order, r, s, t);
		for (int_t j = 0; j < nbasis; j++){
			rst(i, j) = rst_row[j];
		}
	}
	rst = arma::inv(rst);
	std::vector<std::vector<real_t>> comtophy(3);
	for (int_t i = 0; i < comtophy.size(); i++){
		arma::mat xyz(nbasis, 1);
		for (int_t j = 0; j < nbasis; j++){
			xyz(j, 0) = coords[3 * j + i];
		}
		xyz = rst*xyz;
		comtophy[i].resize(nbasis);
		for (int_t j = 0; j < nbasis; j++){
			comtophy[i][j] = xyz(j, 0);
		}
	}
	return comtophy;
}
const std::vector<real_t> TetsJacobian::calComToPhyCoord(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 3;
	std::vector<real_t> phy(num_points * 3);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		for (int_t i = 0; i < comtophy_coefficients_.size(); i++){
			const real_t r = coord[3 * ipoint];
			const real_t s = coord[3 * ipoint + 1];
			const real_t t = coord[3 * ipoint + 2];
			phy[3 * ipoint + i] = Poly(comtophy_coefficients_[i], JacobianMath::monomialTets(element_order_, r, s, t));
		}
	}
	return phy;
}
const std::vector<real_t> TetsJacobian::calJacobian(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 3;
	std::vector<real_t> jacobi(num_points);
	arma::mat jac(3, 3);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r = coord[3 * ipoint];
		const real_t s = coord[3 * ipoint + 1];
		const real_t t = coord[3 * ipoint + 2];
		for (int_t i = 0; i < 3; i++){
			jac(0, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDrTets(element_order_, r, s, t));
			jac(1, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDsTets(element_order_, r, s, t));
			jac(2, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDtTets(element_order_, r, s, t));
		}
		jacobi[ipoint] = std::abs(arma::det(jac));
	}
	return jacobi;
}
const std::vector<real_t> TetsJacobian::calJacobianAdjMat(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 3;
	std::vector<real_t> jacobi(9 * num_points);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r = coord[3 * ipoint];
		const real_t s = coord[3 * ipoint + 1];
		const real_t t = coord[3 * ipoint + 2];
		const real_t xr = Poly(comtophy_coefficients_[0], JacobianMath::monomialDrTets(element_order_, r, s, t));
		const real_t xs = Poly(comtophy_coefficients_[0], JacobianMath::monomialDsTets(element_order_, r, s, t));
		const real_t xt = Poly(comtophy_coefficients_[0], JacobianMath::monomialDtTets(element_order_, r, s, t));
		const real_t yr = Poly(comtophy_coefficients_[1], JacobianMath::monomialDrTets(element_order_, r, s, t));
		const real_t ys = Poly(comtophy_coefficients_[1], JacobianMath::monomialDsTets(element_order_, r, s, t));
		const real_t yt = Poly(comtophy_coefficients_[1], JacobianMath::monomialDtTets(element_order_, r, s, t));
		const real_t zr = Poly(comtophy_coefficients_[2], JacobianMath::monomialDrTets(element_order_, r, s, t));
		const real_t zs = Poly(comtophy_coefficients_[2], JacobianMath::monomialDsTets(element_order_, r, s, t));
		const real_t zt = Poly(comtophy_coefficients_[2], JacobianMath::monomialDtTets(element_order_, r, s, t));

		jacobi[9 * ipoint] = ys*zt - yt*zs;
		jacobi[9 * ipoint + 1] = yt*zr - yr*zt;
		jacobi[9 * ipoint + 2] = yr*zs - ys*zr;
		jacobi[9 * ipoint + 3] = xt*zs - xs*zt;
		jacobi[9 * ipoint + 4] = xr*zt - xt*zr;
		jacobi[9 * ipoint + 5] = xs*zr - xr*zs;
		jacobi[9 * ipoint + 6] = xs*yt - xt*ys;
		jacobi[9 * ipoint + 7] = xt*yr - xr*yt;
		jacobi[9 * ipoint + 8] = xr*ys - xs*yr;
	}
	return jacobi;
}

// hexahedral
const std::vector<std::vector<real_t>> HexaJacobian::calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords)
{
	int_t nbasis = (element_order + 1)*(element_order + 1)*(element_order + 1);
	int_t npoint = coords.size() / 3;

	arma::mat rst(nbasis, nbasis);
	arma::mat rst_row(1, nbasis);

	Element* tets = ElementFactory::getInstance().getElement(element_type_);
	const std::vector<real_t>& node_numbering = tets->getNodesCoord(element_order);
	for (int_t i = 0; i < nbasis; i++){
		const real_t r = node_numbering[3 * i];
		const real_t s = node_numbering[3 * i + 1];
		const real_t t = node_numbering[3 * i + 2];
		const std::vector<real_t> rst_row = JacobianMath::monomialHexa(element_order, r, s, t);
		for (int_t j = 0; j < nbasis; j++){
			rst(i, j) = rst_row[j];
		}
	}
	rst = arma::inv(rst);
	std::vector<std::vector<real_t>> comtophy(3);
	for (int_t i = 0; i < comtophy.size(); i++){
		arma::mat xyz(nbasis, 1);
		for (int_t j = 0; j < nbasis; j++){
			xyz(j, 0) = coords[3 * j + i];
		}
		xyz = rst*xyz;
		comtophy[i].resize(nbasis);
		for (int_t j = 0; j < nbasis; j++){
			comtophy[i][j] = xyz(j, 0);
		}
	}
	return comtophy;
}
const std::vector<real_t> HexaJacobian::calComToPhyCoord(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 3;
	std::vector<real_t> phy(num_points * 3);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		for (int_t i = 0; i < comtophy_coefficients_.size(); i++){
			const real_t r = coord[3 * ipoint];
			const real_t s = coord[3 * ipoint + 1];
			const real_t t = coord[3 * ipoint + 2];
			phy[3 * ipoint + i] = Poly(comtophy_coefficients_[i], JacobianMath::monomialHexa(element_order_, r, s, t));
		}
	}
	return phy;
}
const std::vector<real_t> HexaJacobian::calJacobian(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 3;
	std::vector<real_t> jacobi(num_points);
	arma::mat jac(3, 3);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r = coord[3 * ipoint];
		const real_t s = coord[3 * ipoint + 1];
		const real_t t = coord[3 * ipoint + 2];
		for (int_t i = 0; i < 3; i++){
			jac(0, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDrHexa(element_order_, r, s, t));
			jac(1, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDsHexa(element_order_, r, s, t));
			jac(2, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDtHexa(element_order_, r, s, t));
		}
		jacobi[ipoint] = std::abs(arma::det(jac));
	}
	return jacobi;
}
const std::vector<real_t> HexaJacobian::calJacobianAdjMat(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 3;
	std::vector<real_t> jacobi(9 * num_points);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r = coord[3 * ipoint];
		const real_t s = coord[3 * ipoint + 1];
		const real_t t = coord[3 * ipoint + 2];
		const real_t xr = Poly(comtophy_coefficients_[0], JacobianMath::monomialDrHexa(element_order_, r, s, t));
		const real_t xs = Poly(comtophy_coefficients_[0], JacobianMath::monomialDsHexa(element_order_, r, s, t));
		const real_t xt = Poly(comtophy_coefficients_[0], JacobianMath::monomialDtHexa(element_order_, r, s, t));
		const real_t yr = Poly(comtophy_coefficients_[1], JacobianMath::monomialDrHexa(element_order_, r, s, t));
		const real_t ys = Poly(comtophy_coefficients_[1], JacobianMath::monomialDsHexa(element_order_, r, s, t));
		const real_t yt = Poly(comtophy_coefficients_[1], JacobianMath::monomialDtHexa(element_order_, r, s, t));
		const real_t zr = Poly(comtophy_coefficients_[2], JacobianMath::monomialDrHexa(element_order_, r, s, t));
		const real_t zs = Poly(comtophy_coefficients_[2], JacobianMath::monomialDsHexa(element_order_, r, s, t));
		const real_t zt = Poly(comtophy_coefficients_[2], JacobianMath::monomialDtHexa(element_order_, r, s, t));

		jacobi[9 * ipoint] = ys*zt - yt*zs;
		jacobi[9 * ipoint + 1] = yt*zr - yr*zt;
		jacobi[9 * ipoint + 2] = yr*zs - ys*zr;
		jacobi[9 * ipoint + 3] = xt*zs - xs*zt;
		jacobi[9 * ipoint + 4] = xr*zt - xt*zr;
		jacobi[9 * ipoint + 5] = xs*zr - xr*zs;
		jacobi[9 * ipoint + 6] = xs*yt - xt*ys;
		jacobi[9 * ipoint + 7] = xt*yr - xr*yt;
		jacobi[9 * ipoint + 8] = xr*ys - xs*yr;
	}
	return jacobi;
}

// prism
const std::vector<std::vector<real_t>> PrisJacobian::calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords)
{
	int_t nbasis = (element_order + 1)*(element_order + 1)*(element_order + 2) / 2;
	int_t npoint = coords.size() / 3;

	arma::mat rst(nbasis, nbasis);
	arma::mat rst_row(1, nbasis);

	Element* pris = ElementFactory::getInstance().getElement(element_type_);
	const std::vector<real_t>& node_numbering = pris->getNodesCoord(element_order);
	for (int_t i = 0; i < nbasis; i++) {
		const real_t r = node_numbering[3 * i];
		const real_t s = node_numbering[3 * i + 1];
		const real_t t = node_numbering[3 * i + 2];
		const std::vector<real_t> rst_row = JacobianMath::monomialPris(element_order, r, s, t);
		for (int_t j = 0; j < nbasis; j++) {
			rst(i, j) = rst_row[j];
		}
	}
	rst = arma::inv(rst);
	std::vector<std::vector<real_t>> comtophy(3);
	for (int_t i = 0; i < comtophy.size(); i++) {
		arma::mat xyz(nbasis, 1);
		for (int_t j = 0; j < nbasis; j++) {
			xyz(j, 0) = coords[3 * j + i];
		}
		xyz = rst*xyz;
		comtophy[i].resize(nbasis);
		for (int_t j = 0; j < nbasis; j++) {
			comtophy[i][j] = xyz(j, 0);
		}
	}
	return comtophy;
}
const std::vector<real_t> PrisJacobian::calComToPhyCoord(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 3;
	std::vector<real_t> phy(num_points * 3);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		for (int_t i = 0; i < comtophy_coefficients_.size(); i++) {
			const real_t r = coord[3 * ipoint];
			const real_t s = coord[3 * ipoint + 1];
			const real_t t = coord[3 * ipoint + 2];
			phy[3 * ipoint + i] = Poly(comtophy_coefficients_[i], JacobianMath::monomialPris(element_order_, r, s, t));
		}
	}
	return phy;
}
const std::vector<real_t> PrisJacobian::calJacobian(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 3;
	std::vector<real_t> jacobi(num_points);
	arma::mat jac(3, 3);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t r = coord[3 * ipoint];
		const real_t s = coord[3 * ipoint + 1];
		const real_t t = coord[3 * ipoint + 2];
		for (int_t i = 0; i < 3; i++) {
			jac(0, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDrPris(element_order_, r, s, t));
			jac(1, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDsPris(element_order_, r, s, t));
			jac(2, i) = Poly(comtophy_coefficients_[i], JacobianMath::monomialDtPris(element_order_, r, s, t));
		}
		jacobi[ipoint] = std::abs(arma::det(jac));
	}
	return jacobi;
}
const std::vector<real_t> PrisJacobian::calJacobianAdjMat(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 3;
	std::vector<real_t> jacobi(9 * num_points);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t r = coord[3 * ipoint];
		const real_t s = coord[3 * ipoint + 1];
		const real_t t = coord[3 * ipoint + 2];
		const real_t xr = Poly(comtophy_coefficients_[0], JacobianMath::monomialDrPris(element_order_, r, s, t));
		const real_t xs = Poly(comtophy_coefficients_[0], JacobianMath::monomialDsPris(element_order_, r, s, t));
		const real_t xt = Poly(comtophy_coefficients_[0], JacobianMath::monomialDtPris(element_order_, r, s, t));
		const real_t yr = Poly(comtophy_coefficients_[1], JacobianMath::monomialDrPris(element_order_, r, s, t));
		const real_t ys = Poly(comtophy_coefficients_[1], JacobianMath::monomialDsPris(element_order_, r, s, t));
		const real_t yt = Poly(comtophy_coefficients_[1], JacobianMath::monomialDtPris(element_order_, r, s, t));
		const real_t zr = Poly(comtophy_coefficients_[2], JacobianMath::monomialDrPris(element_order_, r, s, t));
		const real_t zs = Poly(comtophy_coefficients_[2], JacobianMath::monomialDsPris(element_order_, r, s, t));
		const real_t zt = Poly(comtophy_coefficients_[2], JacobianMath::monomialDtPris(element_order_, r, s, t));

		jacobi[9 * ipoint] = ys*zt - yt*zs;
		jacobi[9 * ipoint + 1] = yt*zr - yr*zt;
		jacobi[9 * ipoint + 2] = yr*zs - ys*zr;
		jacobi[9 * ipoint + 3] = xt*zs - xs*zt;
		jacobi[9 * ipoint + 4] = xr*zt - xt*zr;
		jacobi[9 * ipoint + 5] = xs*zr - xr*zs;
		jacobi[9 * ipoint + 6] = xs*yt - xt*ys;
		jacobi[9 * ipoint + 7] = xt*yr - xr*yt;
		jacobi[9 * ipoint + 8] = xr*ys - xs*yr;
	}
	return jacobi;
}

// line face
const std::vector<std::vector<real_t>> LineFaceJacobian::calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords)
{
	int_t nbasis = element_order + 1;
	int_t npoint = coords.size() / 2;

	arma::mat rs(nbasis, nbasis);
	arma::mat rs_row(1, nbasis);

	Element* line = ElementFactory::getInstance().getElement(element_type_);
	const std::vector<real_t>& node_numbering = line->getNodesCoord(element_order);
	for (int_t i = 0; i < nbasis; i++){
		const real_t r = node_numbering[i];
		const std::vector<real_t> rs_row = JacobianMath::monomialLine(element_order, r);
		for (int_t j = 0; j < nbasis; j++){
			rs(i, j) = rs_row[j];
		}
	}
	rs = arma::inv(rs);
	std::vector<std::vector<real_t>> comtophy(2);
	for (int_t i = 0; i < comtophy.size(); i++){
		arma::mat xy(nbasis, 1);
		for (int_t j = 0; j < nbasis; j++){
			xy(j, 0) = coords[2 * j + i];
		}
		xy = rs*xy;
		comtophy[i].resize(nbasis);
		for (int_t j = 0; j < nbasis; j++){
			comtophy[i][j] = xy(j, 0);
		}
	}
	return comtophy;
}
const std::vector<real_t> LineFaceJacobian::calComToPhyCoord(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size();
	std::vector<real_t> phy(2 * num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		for (int_t i = 0; i < comtophy_coefficients_.size(); i++){
			const real_t r = coord[ipoint];
			phy[2 * ipoint + i] = Poly(comtophy_coefficients_[i], JacobianMath::monomialLine(element_order_, r));
		}
	}
	return phy;
}
const std::vector<real_t> LineFaceJacobian::calJacobian(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size();
	std::vector<real_t> results(num_points, 0.0);
	return results;
}
const std::vector<real_t> LineFaceJacobian::calJacobianAdjMat(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size();
	std::vector<real_t> jacobi(2 * num_points);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r = coord[ipoint];
		jacobi[2 * ipoint] = -Poly(comtophy_coefficients_[1], JacobianMath::monomialDrLine(element_order_, r));
		jacobi[2 * ipoint + 1] = Poly(comtophy_coefficients_[0], JacobianMath::monomialDrLine(element_order_, r));
	}
	return jacobi;
}

// tris face
const std::vector<std::vector<real_t>> TrisFaceJacobian::calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords)
{
	int_t nbasis = (element_order + 1)*(element_order + 2) / 2;
	int_t npoint = coords.size() / 3;

	arma::mat rs(nbasis, nbasis);
	arma::mat rs_row(1, nbasis);

	Element* tris = ElementFactory::getInstance().getElement(element_type_);
	const std::vector<real_t>& node_numbering = tris->getNodesCoord(element_order);
	for (int_t i = 0; i < nbasis; i++){
		const real_t r = node_numbering[2 * i];
		const real_t s = node_numbering[2 * i + 1];
		const std::vector<real_t> rs_row = JacobianMath::monomialTris(element_order, r, s);
		for (int_t j = 0; j < nbasis; j++){
			rs(i, j) = rs_row[j];
		}
	}
	rs = arma::inv(rs);
	std::vector<std::vector<real_t>> comtophy(3);
	for (int_t i = 0; i < comtophy.size(); i++){
		arma::mat xy(nbasis, 1);
		for (int_t j = 0; j < nbasis; j++){
			xy(j, 0) = coords[3 * j + i];
		}
		xy = rs*xy;
		comtophy[i].resize(nbasis);
		for (int_t j = 0; j < nbasis; j++){
			comtophy[i][j] = xy(j, 0);
		}
	}
	return comtophy;
}
const std::vector<real_t> TrisFaceJacobian::calComToPhyCoord(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 2;
	std::vector<real_t> phy(3 * num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		for (int_t i = 0; i < comtophy_coefficients_.size(); i++){
			const real_t r = coord[2 * ipoint];
			const real_t s = coord[2 * ipoint + 1];
			phy[3 * ipoint + i] = Poly(comtophy_coefficients_[i], JacobianMath::monomialTris(element_order_, r, s));
		}
	}
	return phy;
}
const std::vector<real_t> TrisFaceJacobian::calJacobian(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / 2;
	std::vector<real_t> results(num_points, 0.0);
	return results;
}
const std::vector<real_t> TrisFaceJacobian::calJacobianAdjMat(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 2;
	std::vector<real_t> jacobi(3 * num_points);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r = coord[2 * ipoint];
		const real_t s = coord[2 * ipoint + 1];
		const real_t xr = Poly(comtophy_coefficients_[0], JacobianMath::monomialDrTris(element_order_, r, s));
		const real_t xs = Poly(comtophy_coefficients_[0], JacobianMath::monomialDsTris(element_order_, r, s));
		const real_t yr = Poly(comtophy_coefficients_[1], JacobianMath::monomialDrTris(element_order_, r, s));
		const real_t ys = Poly(comtophy_coefficients_[1], JacobianMath::monomialDsTris(element_order_, r, s));
		const real_t zr = Poly(comtophy_coefficients_[2], JacobianMath::monomialDrTris(element_order_, r, s));
		const real_t zs = Poly(comtophy_coefficients_[2], JacobianMath::monomialDsTris(element_order_, r, s));

		jacobi[3 * ipoint] = yr*zs - ys*zr;
		jacobi[3 * ipoint + 1] = xs*zr - xr*zs;
		jacobi[3 * ipoint + 2] = xr*ys - xs*yr;
	}
	return jacobi;
}

// quad face
const std::vector<std::vector<real_t>> QuadFaceJacobian::calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords)
{
	int_t nbasis = (element_order + 1)*(element_order + 1);
	int_t npoint = coords.size() / 3;

	arma::mat rs(nbasis, nbasis);
	arma::mat rs_row(1, nbasis);

	Element* quad = ElementFactory::getInstance().getElement(element_type_);
	const std::vector<real_t>& node_numbering = quad->getNodesCoord(element_order);
	for (int_t i = 0; i < nbasis; i++){
		const real_t r = node_numbering[2 * i];
		const real_t s = node_numbering[2 * i + 1];
		const std::vector<real_t> rs_row = JacobianMath::monomialQuad(element_order, r, s);
		for (int_t j = 0; j < nbasis; j++){
			rs(i, j) = rs_row[j];
		}
	}
	rs = arma::inv(rs);
	std::vector<std::vector<real_t>> comtophy(3);
	for (int_t i = 0; i < comtophy.size(); i++){
		arma::mat xy(nbasis, 1);
		for (int_t j = 0; j < nbasis; j++){
			xy(j, 0) = coords[3 * j + i];
		}
		xy = rs*xy;
		comtophy[i].resize(nbasis);
		for (int_t j = 0; j < nbasis; j++){
			comtophy[i][j] = xy(j, 0);
		}
	}
	return comtophy;
}
const std::vector<real_t> QuadFaceJacobian::calComToPhyCoord(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 2;
	std::vector<real_t> phy(3 * num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		for (int_t i = 0; i < comtophy_coefficients_.size(); i++){
			const real_t r = coord[2 * ipoint];
			const real_t s = coord[2 * ipoint + 1];
			phy[3 * ipoint + i] = Poly(comtophy_coefficients_[i], JacobianMath::monomialQuad(element_order_, r, s));
		}
	}
	return phy;
}
const std::vector<real_t> QuadFaceJacobian::calJacobian(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / 2;
	std::vector<real_t> results(num_points, 0.0);
	return results;
}
const std::vector<real_t> QuadFaceJacobian::calJacobianAdjMat(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / 2;
	std::vector<real_t> jacobi(3 * num_points);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r = coord[2 * ipoint];
		const real_t s = coord[2 * ipoint + 1];
		const real_t xr = Poly(comtophy_coefficients_[0], JacobianMath::monomialDrQuad(element_order_, r, s));
		const real_t xs = Poly(comtophy_coefficients_[0], JacobianMath::monomialDsQuad(element_order_, r, s));
		const real_t yr = Poly(comtophy_coefficients_[1], JacobianMath::monomialDrQuad(element_order_, r, s));
		const real_t ys = Poly(comtophy_coefficients_[1], JacobianMath::monomialDsQuad(element_order_, r, s));
		const real_t zr = Poly(comtophy_coefficients_[2], JacobianMath::monomialDrQuad(element_order_, r, s));
		const real_t zs = Poly(comtophy_coefficients_[2], JacobianMath::monomialDsQuad(element_order_, r, s));

		jacobi[3 * ipoint] = yr*zs - ys*zr;
		jacobi[3 * ipoint + 1] = xs*zr - xr*zs;
		jacobi[3 * ipoint + 2] = xr*ys - xs*yr;
	}
	return jacobi;
}