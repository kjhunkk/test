#include "../INC/TecWrite.h"

TecWrite::TecWrite()
{
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank_);
	MPI_Comm_size(MPI_COMM_WORLD, &ndomain_);
}

void TecWrite::resetBuffer()
{
	buffer_.clear();
	int ver1 = 0;
	int ver2 = 0;
	ver1 = ver1 | (*(char*)&version_[3] << 24);
	ver1 = ver1 | (*(char*)&version_[2] << 16);
	ver1 = ver1 | (*(char*)&version_[1] << 8);
	ver1 = ver1 | *(char*)&version_[0];
	ver2 = ver2 | (*(char*)&version_[7] << 24);
	ver2 = ver2 | (*(char*)&version_[6] << 16);
	ver2 = ver2 | (*(char*)&version_[5] << 8);
	ver2 = ver2 | *(char*)&version_[4];
	buffer_.push_back(ver1);
	buffer_.push_back(ver2);
}

void TecWrite::writeTecSolutionFile(const std::string& filename, const std::vector<std::vector<float>>& solutions)
{
	if (ndomain_ == 1) {
		std::ofstream outfile(filename, std::ios::binary);
		outfile.write((char*)&buffer_[0], sizeof(int)*buffer_.size());
		std::vector<double> minmax(solutions.size() * 2);
		for (int ivar = 0; ivar < solutions.size(); ivar++) {
			minmax[ivar * 2 + 0] = *std::min_element(solutions[ivar].begin(), solutions[ivar].end());
			minmax[ivar * 2 + 1] = *std::max_element(solutions[ivar].begin(), solutions[ivar].end());
		}
		outfile.write((char*)&minmax[0], sizeof(double)*minmax.size());
		for (int ivar = 0; ivar < solutions.size(); ivar++)
			outfile.write((char*)&solutions[ivar][0], sizeof(float)*solutions[ivar].size());
		outfile.close();
	}
	else {
		const int nvar = solutions.size();
		std::vector<double> min_local(nvar);
		std::vector<double> max_local(nvar);
		std::vector<double> min_global(nvar);
		std::vector<double> max_global(nvar);
		std::vector<double> minmax(nvar * 2);
		for (int ivar = 0; ivar < nvar; ivar++) {
			min_local[ivar] = *std::min_element(solutions[ivar].begin(), solutions[ivar].end());
			max_local[ivar] = *std::max_element(solutions[ivar].begin(), solutions[ivar].end());
		}
		MPI_Allreduce(&min_local[0], &min_global[0], nvar, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
		MPI_Allreduce(&max_local[0], &max_global[0], nvar, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
		for (int ivar = 0; ivar < nvar; ivar++) {
			minmax[ivar * 2 + 0] = min_global[ivar];
			minmax[ivar * 2 + 1] = max_global[ivar];
		}

		MPI_File outfile;
		int displacement = first_var_displacement_;
		MPI_File_open(MPI_COMM_WORLD, filename.c_str(), MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &outfile);
		MPI_File_set_view(outfile, displacement, MPI_INT, MPI_INT, "native", MPI_INFO_NULL);
		if (myrank_ == MASTER_NODE) {
			MPI_File_write(outfile, &buffer_[0], buffer_.size(), MPI_INT, MPI_STATUS_IGNORE);
			MPI_File_write(outfile, &minmax[0], nvar * 2, MPI_DOUBLE, MPI_STATUS_IGNORE);
			displacement += header_size_;
		}
		for (int ivar = 0; ivar < nvar; ivar++) {
			MPI_File_write(outfile, &solutions[ivar][0], solutions[ivar].size(), MPI_FLOAT, MPI_STATUS_IGNORE);
			if (ivar == nvar - 1) break;
			displacement += next_var_displacement_;
			MPI_File_set_view(outfile, displacement, MPI_INT, MPI_INT, "native", MPI_INFO_NULL);
		}
		MPI_File_close(&outfile);
	}
}

void TecWrite::writeTecGridFile(const std::string& filename, const std::vector<std::vector<float>>& coords, const std::vector<int>& connectivity)
{
	if (ndomain_ == 1) {
		std::ofstream outfile(filename, std::ios::binary);
		outfile.write((char*)&buffer_[0], sizeof(int)*buffer_.size());
		std::vector<double> minmax(coords.size() * 2);
		for (int idim = 0; idim < coords.size(); idim++) {
			minmax[idim * 2 + 0] = *std::min_element(coords[idim].begin(), coords[idim].end());
			minmax[idim * 2 + 1] = *std::max_element(coords[idim].begin(), coords[idim].end());
		}
		outfile.write((char*)&minmax[0], sizeof(double)*minmax.size());
		for (int idim = 0; idim < coords.size(); idim++)
			outfile.write((char*)&coords[idim][0], sizeof(float)*coords[idim].size());
		outfile.write((char*)&connectivity[0], sizeof(int)*connectivity.size());
		outfile.close();
	}
	else {
		const int dimension = coords.size();
		std::vector<double> min_local(dimension);
		std::vector<double> max_local(dimension);
		std::vector<double> min_global(dimension);
		std::vector<double> max_global(dimension);
		std::vector<double> minmax(dimension * 2);
		for (int idim = 0; idim < dimension; idim++) {
			min_local[idim] = *std::min_element(coords[idim].begin(), coords[idim].end());
			max_local[idim] = *std::max_element(coords[idim].begin(), coords[idim].end());
		}
		MPI_Allreduce(&min_local[0], &min_global[0], dimension, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
		MPI_Allreduce(&max_local[0], &max_global[0], dimension, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
		for (int idim = 0; idim < dimension; idim++) {
			minmax[idim * 2 + 0] = min_global[idim];
			minmax[idim * 2 + 1] = max_global[idim];
		}

		MPI_File outfile;
		int displacement = first_var_displacement_;
		MPI_File_open(MPI_COMM_WORLD, filename.c_str(), MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &outfile);
		MPI_File_set_view(outfile, displacement, MPI_INT, MPI_INT, "native", MPI_INFO_NULL);
		std::vector<int> connectivity_change = connectivity;
		if (myrank_ == MASTER_NODE) {
			MPI_File_write(outfile, &buffer_[0], buffer_.size(), MPI_INT, MPI_STATUS_IGNORE);
			MPI_File_write(outfile, &minmax[0], dimension * 2, MPI_DOUBLE, MPI_STATUS_IGNORE);
			displacement += header_size_;
		}
		else {
			for (auto&& connect : connectivity_change)
				connect += first_node_index_;
		}
		for (int idim = 0; idim<dimension; idim++) {
			MPI_File_write(outfile, &coords[idim][0], coords[idim].size(), MPI_FLOAT, MPI_STATUS_IGNORE);
			if (idim == dimension - 1) break;
			displacement += next_var_displacement_;
			MPI_File_set_view(outfile, displacement, MPI_INT, MPI_INT, "native", MPI_INFO_NULL);
		}
		displacement += next_connect_displacement_;
		MPI_File_set_view(outfile, displacement, MPI_INT, MPI_INT, "native", MPI_INFO_NULL);
		MPI_File_write(outfile, &connectivity_change[0], connectivity_change.size(), MPI_INT, MPI_STATUS_IGNORE);
		MPI_File_close(&outfile);
	}
}

void TecWrite::setStrandID(const int strandid)
{
	const int strandid_cast = strandid;
	buffer_[strandid_index_] = strandid_cast;
}

void TecWrite::setSolutionTime(const double solution_time)
{
	const double solution_time_cast = solution_time;
	__int64 left = (*(__int64*)&solution_time_cast) >> 32;
	const __int64 mask = 0xffffffff00000000;
	__int64 right = ((*(__int64*)&solution_time_cast) & mask) >> 32;
	buffer_[solution_time_index_ + 0] = (int)right;
	buffer_[solution_time_index_ + 1] = (int)left;
}

void TecWrite::setHeader(const FileType filetype, const std::string& title, const int num_variables, const std::vector<std::string>& variable_names, const ZoneType zonetype, const int num_nodes, const int num_cells)
{
	writeIntDataBuffer(1); // default
	writeIntDataBuffer(static_cast<int>(filetype)); // file type
	writeStringDataBuffer(title); // title
	writeIntDataBuffer(num_variables); // num var
	for (int ivar = 0; ivar<num_variables; ivar++)
		writeStringDataBuffer(variable_names[ivar]); // var names

	writeReal32DataBuffer(299.0); // zone marker
	writeStringDataBuffer("ZONE001"); // zone name
	writeIntDataBuffer(-1); // parent zone
	strandid_index_ = buffer_.size();
	writeIntDataBuffer(-1); // strand ID
	solution_time_index_ = buffer_.size();
	writeReal64DataBuffer(0.0); // solution time
	writeIntDataBuffer(-1); // default = -1
	writeIntDataBuffer(static_cast<int>(zonetype)); // zone type
	writeIntDataBuffer(0); // specify var location
	writeIntDataBuffer(0); // Are raw local 1-to-1 face neighbors supplied?
	writeIntDataBuffer(0); // number of miscellaneous user-defined face neighbor connections
	int num_total_nodes;
	MPI_Allreduce(&num_nodes, &num_total_nodes, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	writeIntDataBuffer(num_total_nodes); // number of nodes
	int num_total_cells;
	MPI_Allreduce(&num_cells, &num_total_cells, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	writeIntDataBuffer(num_total_cells); // number of cells
	writeIntDataBuffer(0); // ICellDim
	writeIntDataBuffer(0); // JCellDim
	writeIntDataBuffer(0); // KCellDim
	writeIntDataBuffer(0); // auxiliary name

	writeReal32DataBuffer(357.0); // EOH marker

	writeReal32DataBuffer(299.0); // zone marker
	for (int ivar = 0; ivar<num_variables; ivar++)
		writeIntDataBuffer(1); // variable data format
	writeIntDataBuffer(0); // has passive variables
	writeIntDataBuffer(0); // has variable sharing
	writeIntDataBuffer(-1); // zeros based zone number to share connectivity list with (-1=no sharing)

	if (ndomain_ == 1) return;
	const int local_disp = sizeof(float)*num_nodes;
	header_size_ = buffer_.size() * sizeof(int) + sizeof(double) * 2 * num_variables;

	std::vector<int> disp_gather(ndomain_);
	MPI_Allgather(&local_disp, 1, MPI_INT, &disp_gather[0], 1, MPI_INT, MPI_COMM_WORLD);
	first_var_displacement_ = 0;
	for (int idomain = 0; idomain < myrank_; idomain++)
		first_var_displacement_ += disp_gather[idomain];
	next_var_displacement_ = 0;
	for (int idomain = 0; idomain < ndomain_; idomain++)
		next_var_displacement_ += disp_gather[idomain];
	if (myrank_ != MASTER_NODE)
		first_var_displacement_ += header_size_;

	int csize = 0;
	switch (zonetype) {
	case ZoneType::FETRIS: csize = 3; break;
	case ZoneType::FETETS: csize = 4; break;
	}

	const int local_connect_disp = sizeof(int)*num_cells*csize;
	std::vector<int> connect_disp_gather(ndomain_);
	MPI_Allgather(&local_connect_disp, 1, MPI_INT, &connect_disp_gather[0], 1, MPI_INT, MPI_COMM_WORLD);
	next_connect_displacement_ = 0;
	for (int idomain = 0; idomain < myrank_; idomain++)
		next_connect_displacement_ += connect_disp_gather[idomain];
	for (int idomain = myrank_; idomain < ndomain_; idomain++)
		next_connect_displacement_ += disp_gather[idomain];

	std::vector<int> num_nodes_gather(ndomain_);
	MPI_Allgather(&num_nodes, 1, MPI_INT, &num_nodes_gather[0], 1, MPI_INT, MPI_COMM_WORLD);
	first_node_index_ = 0;
	for (int idomain = 0; idomain < myrank_; idomain++)
		first_node_index_ += num_nodes_gather[idomain];
}

void TecWrite::writeIntDataBuffer(const int data)
{
	buffer_.push_back(data);
}

void TecWrite::writeReal32DataBuffer(const float data)
{
	buffer_.push_back(*(int*)&data);
}

void TecWrite::writeReal64DataBuffer(const double data)
{
	__int64 left = (*(__int64*)&data) >> 32;
	const __int64 mask = 0xffffffff00000000;
	__int64 right = ((*(__int64*)&data) & mask) >> 32;
	buffer_.push_back((int)right);
	buffer_.push_back((int)left);
}

void TecWrite::writeStringDataBuffer(const std::string data)
{
	const char* str = data.c_str();
	int ch;
	for (int i = 0; i < data.length(); i++) {
		ch = (int)str[i];
		buffer_.push_back(ch);
	}
	buffer_.push_back(0);
}