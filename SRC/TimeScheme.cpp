
#include "../INC/TimeScheme.h"

ExplicitEuler::ExplicitEuler()
{
	setName("ExplicitEuler");
	TimeScheme::getInstance().addRegistry(getName(), this);
	is_implicit_ = false; // for implicit
}
void ExplicitEuler::initialize() // for implicit
{
	is_fixed_dt_ = false;
}
void ExplicitEuler::integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
	const int_t num_cell = zone->getNumCells();

	zone->communication();
	const std::vector<std::vector<real_t>>& rhs = zone->calculateRHS();
	std::vector<std::vector<real_t>>& solutions = zone->getSolution();

	
	for (int_t icell = 0; icell < num_cell; icell++) {
		for (int_t ivar = 0; ivar < rhs[icell].size(); ivar++) {
			solutions[icell][ivar] -= dt*rhs[icell][ivar];
		}
	}
	zone->communication();
	zone->limiting();
	zone->pressurefix(current_time, iteration);
}

TVDRK::TVDRK()
{
	setName("TVDRK");
	TimeScheme::getInstance().addRegistry(getName(), this);
	is_implicit_ = false; // for implicit
}
void TVDRK::initialize() // for implicit
{
	is_fixed_dt_ = false;
}
void TVDRK::integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
	const int_t num_cell = zone->getNumCells();

	std::vector<std::vector<real_t>> U1;

	for (int_t istep = 0; istep < 3; istep++) {
		zone->communication();
		const std::vector<std::vector<real_t>>& rhs = zone->calculateRHS();
		std::vector<std::vector<real_t>>& solutions = zone->getSolution();
		switch (istep) {
		case 0:
			U1 = solutions;
			for (int_t icell = 0; icell < num_cell; icell++) {
				for (int_t ivar = 0; ivar < rhs[icell].size(); ivar++) {
					solutions[icell][ivar] -= dt*rhs[icell][ivar];
				}
			}
			break;
		case 1:
			for (int_t icell = 0; icell < num_cell; icell++) {
				for (int_t ivar = 0; ivar < rhs[icell].size(); ivar++) {
					solutions[icell][ivar] = 0.25*(3.0*U1[icell][ivar] + solutions[icell][ivar] - dt*rhs[icell][ivar]);
				}
			}
			break;
		case 2:
			for (int_t icell = 0; icell < num_cell; icell++) {
				for (int_t ivar = 0; ivar < rhs[icell].size(); ivar++) {
					solutions[icell][ivar] = (U1[icell][ivar] + 2.0*solutions[icell][ivar] - 2.0*dt*rhs[icell][ivar]) / 3.0;
				}
			}
			break;
		}
		zone->communication();
		zone->limiting();
		zone->pressurefix(current_time, iteration);
	}
}

SSPRK::SSPRK()
{
	setName("SSPRK");
	TimeScheme::getInstance().addRegistry(getName(), this);
	is_implicit_ = false; // for implicit
}
void SSPRK::initialize() // for implicit
{
	is_fixed_dt_ = false;
}
void SSPRK::integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
	const int_t num_cell = zone->getNumCells();
	
	std::vector<std::vector<real_t>> U1;
	std::vector<std::vector<real_t>> U2;
	std::vector<std::vector<real_t>> U3;
	std::vector<std::vector<real_t>> rhs3;
	
	for (int_t istep = 0; istep < 5; istep++) {
		zone->communication();
		const std::vector<std::vector<real_t>>& rhs = zone->calculateRHS();
		std::vector<std::vector<real_t>>& solutions = zone->getSolution();
		switch (istep) {
		case 0:
			U1 = solutions;
			for (int_t icell = 0; icell < num_cell; icell++) {
				for (int_t ivar = 0; ivar < rhs[icell].size(); ivar++) {
					solutions[icell][ivar] -= 0.391752226571890*dt *rhs[icell][ivar];
				}
			}
			break;
		case 1:
			for (int_t icell = 0; icell < num_cell; icell++) {
				for (int_t ivar = 0; ivar < rhs[icell].size(); ivar++) {
					solutions[icell][ivar] = 0.444370493651235*U1[icell][ivar] + 0.555629506348765*solutions[icell][ivar] - 0.368410593050371*dt *rhs[icell][ivar];
				}
			}
			U2 = solutions;
			break;
		case 2:
			for (int_t icell = 0; icell < num_cell; icell++) {
				for (int_t ivar = 0; ivar < rhs[icell].size(); ivar++) {
					solutions[icell][ivar] = 0.620101851488403*U1[icell][ivar] + 0.379898148511597*solutions[icell][ivar] - 0.251891774271694*dt *rhs[icell][ivar];
				}
			}
			U3 = solutions;
			break;
		case 3:
			rhs3 = rhs;
			for (int_t icell = 0; icell < num_cell; icell++) {
				for (int_t ivar = 0; ivar < rhs[icell].size(); ivar++) {
					solutions[icell][ivar] = 0.178079954393132*U1[icell][ivar] + 0.821920045606868*solutions[icell][ivar] - 0.544974750228521*dt *rhs[icell][ivar];
				}
			}
			U1.clear();
			break;
		case 4:
			for (int_t icell = 0; icell < num_cell; icell++) {
				for (int_t ivar = 0; ivar < rhs[icell].size(); ivar++) {
					solutions[icell][ivar] = 0.517231671970585*U2[icell][ivar] + 0.096059710526147*U3[icell][ivar] - 0.063692468666290*dt *rhs3[icell][ivar]
						+ 0.386708617503269*solutions[icell][ivar] - 0.226007483236906*dt*rhs[icell][ivar];
				}
			}
			break;
		}
		
		zone->communication();
		zone->limiting();
		zone->pressurefix(current_time, iteration);
	}
}