#include "../INC/Aerodynamics.h"

void Aerodynamics::initialize(std::shared_ptr<Grid> grid, std::vector<std::vector<real_t>>* solution, Equation* equation, const int_t post_order)
{
	grid_ = grid;
	equation_ = equation;
	solution_ = solution;
	num_states_ = equation_->getNumStates();
	post_order_ = post_order;
	const Config& config = Config::getInstance();
	return_address_ = config.getReturnAddress();
	dimension_ = config.getDimension();
	order_ = config.getOrder();

	const std::string aero("Euler");
	if (equation_->getName().find(aero) == std::string::npos) {
		aerodynamics_ = false; 
		return;
	}
	aerodynamics_ = true;

	const int_t wall_tag = BoundaryCondition::getInstance().getTagNumber("Wall");

	num_basis_ = grid_->getNumBasis();
	const int_t num_bdries = grid_->getNumBdries();
	const std::vector<int_t>& bdry_type = grid_->getBdryType();
	const std::vector<int_t>& bdry_owner_type = grid_->getBdryOwnerType();
	const std::vector<int_t>& bdry_owner_cell = grid_->getBdryOwnerCell();
	const std::vector<int_t>& cell_to_subnode_ptr = grid_->getCellToSubnodePtr();
	const std::vector<int_t>& cell_to_subnode_ind = grid_->getCellToSubnodeInd();
	const std::vector<int_t>& cell_order = grid_->getCellElemOrder();
	const std::vector<real_t>& node_coords = grid_->getNodeCoords();
	const std::vector<Element*>& cell_element = grid_->getCellElement();

	num_wall_bdries_ = 0;
	for (int_t ibdry = 0; ibdry<num_bdries; ibdry++) {
		if (bdry_type[ibdry] != wall_tag) continue;
		num_wall_bdries_++;
		wall_bdry_list_.push_back(ibdry);
	}

	std::vector<int_t> num_bdry_quad_points(num_wall_bdries_);
	std::vector<std::vector<real_t>> bdry_owner_basis_value(num_wall_bdries_);
	std::vector<std::vector<real_t>> bdry_owner_coefficients(num_wall_bdries_);
	for (int_t index = 0; index<num_wall_bdries_; index++) {

		const int_t& ibdry = wall_bdry_list_[index];
		const int_t& owner_cell = bdry_owner_cell[ibdry];

		std::vector<real_t> face_sub_coords;
		const int_t& start_index = cell_to_subnode_ptr[owner_cell];
		const std::vector<int_t>& face_nodes = cell_element[owner_cell]->getFacetypeNodes(cell_order[owner_cell])[bdry_owner_type[ibdry]];
		face_sub_coords.reserve(dimension_*face_nodes.size());
		for (auto&& inode : face_nodes) {
			for (int_t idim = 0; idim < dimension_; idim++)
				face_sub_coords.push_back(node_coords[dimension_*cell_to_subnode_ind[start_index + inode] + idim]);
		}

		ElemType face_elemtype = cell_element[owner_cell]->getFaceElemType(bdry_owner_type[ibdry]);
		std::shared_ptr<Jacobian> face_jacobian = JacobianFactory::getFaceJacobian(face_elemtype, cell_order[owner_cell]);
		face_jacobian->setCoords(face_sub_coords);
		face_jacobian->setTopology();

		std::vector<real_t> quad_points;
		std::vector<real_t> quad_weights;
		std::vector<real_t> adjmat;
		Quadrature::Surface::getSurfaceQdrt(face_elemtype, face_jacobian, order_, quad_points, quad_weights, adjmat);
		for (auto&& weight : quad_weights)
			weight = std::abs(weight);

		num_bdry_quad_points[index] = quad_weights.size();
		bdry_owner_basis_value[index] = grid_->calBasisValue(owner_cell, quad_points);
		bdry_owner_coefficients[index].resize(num_bdry_quad_points[index] * dimension_);
		for (int_t ipoint = 0; ipoint < num_bdry_quad_points[index]; ipoint++)
			for (int_t idim = 0; idim < dimension_; idim++)
				bdry_owner_coefficients[index][ipoint*dimension_ + idim] = adjmat[ipoint*dimension_ + idim] * quad_weights[ipoint];
	}
	num_bdry_quad_points_ = move(num_bdry_quad_points);
	bdry_owner_basis_value_ = move(bdry_owner_basis_value);
	bdry_owner_coefficients_ = move(bdry_owner_coefficients);

	// for Cp curve
	post_elements_.resize(num_wall_bdries_);
	for (int_t index = 0; index < num_wall_bdries_; index++) {

		const int_t& ibdry = wall_bdry_list_[index];
		const int_t& owner_cell = bdry_owner_cell[ibdry];
		const int_t& owner_type = bdry_owner_type[ibdry];

		const ElemType bdry_elem_type = cell_element[owner_cell]->getFaceElemType(owner_type);
		post_elements_[index] = PostElementFactory::getInstance().getPostElement(bdry_elem_type);
	}

	coords_.resize(num_wall_bdries_);
	basis_.resize(num_wall_bdries_);
	num_subcell_nodes_ = 0;
	num_subcell_cells_ = 0;

	for (int_t index = 0; index < num_wall_bdries_; index++) {

		const int_t& ibdry = wall_bdry_list_[index];
		const int_t& owner_cell = bdry_owner_cell[ibdry];
		const int_t& owner_type = bdry_owner_type[ibdry];

		std::vector<real_t> cell_coords;
		const int_t& start_index = cell_to_subnode_ptr[owner_cell];
		const int_t& end_index = cell_to_subnode_ptr[owner_cell + 1];
		cell_coords.reserve(dimension_*(end_index - start_index));
		for (int_t inode = start_index; inode < end_index; inode++) {
			for (int_t idim = 0; idim < dimension_; idim++)
				cell_coords.push_back(node_coords[cell_to_subnode_ind[inode] * dimension_ + idim]);
		}

		std::vector<real_t> face_coords;
		const std::vector<int_t>& face_nodes = cell_element[owner_cell]->getFacetypeNodes(cell_order[owner_cell])[owner_type];
		face_coords.reserve(dimension_*face_nodes.size());
		for (auto&& inode : face_nodes)
			for (int_t idim = 0; idim < dimension_; idim++)
				face_coords.push_back(cell_coords[dimension_*inode + idim]);

		const ElemType face_elem_type = cell_element[owner_cell]->getFaceElemType(owner_type);
		coords_[index] = post_elements_[index]->getFacePostCoords(static_cast<int_t>(face_elem_type), cell_order[owner_cell], post_order_, face_coords);
		basis_[index] = grid->calBasisValue(owner_cell, coords_[index]);
		const std::vector<int_t> connects = post_elements_[index]->getPostConnects(post_order_);

		num_subcell_nodes_ += coords_[index].size() / dimension_;
		num_subcell_cells_ += connects.size() / dimension_;
	}
}

const std::vector<real_t> Aerodynamics::calForceCoefficients()
{
	std::vector<real_t> Cf(dimension_, 0.0);

	if (aerodynamics_ == false) return Cf;

	const std::vector<int_t>& bdry_owner_cell = grid_->getBdryOwnerCell();
	for (int_t index = 0; index<num_wall_bdries_; index++) {

		const int_t& num_quad_points = num_bdry_quad_points_[index];
		const int_t& ibdry = wall_bdry_list_[index];
		const int_t& owner_cell = bdry_owner_cell[ibdry];

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += bdry_owner_basis_value_[index][ipoint*num_basis_ + ibasis] * (*solution_)[owner_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> Cp = equation_->calPressureCoefficients(owner_solution);
		for (int_t ipoint = 0; ipoint<num_quad_points; ipoint++)
			for (int_t idim = 0; idim<dimension_; idim++)
				Cf[idim] -= Cp[ipoint] * bdry_owner_coefficients_[index][ipoint*dimension_ + idim];
	}

	std::vector<real_t> Cf_sum(dimension_, 0.0);
	for (int_t idim = 0; idim < dimension_; idim++)
		MPI_Allreduce(&Cf[idim], &Cf_sum[idim], 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

	return Cf_sum;
}

void Aerodynamics::postPressureCoefficients(const std::string& title, const real_t time)
{
	if (aerodynamics_ == false) return;
	if (num_wall_bdries_ == 0) return;

	char tag_name[10]; sprintf(tag_name, "%d", MYRANK());
	std::string filename = return_address_ + "/" + title + tag_name + ".plt";
	std::ofstream outfile(filename);
	//outfile << std::scientific;
	//outfile.precision(16);

	MASTER_MESSAGE("Post: File output(" + filename + ")");

	if (dimension_ == 2) {
		outfile << "TITLE = \"field \"" << std::endl;
		outfile << "VARIABLES = \"x\",\"y\",\"Cp\"" << std::endl;
		outfile << "ZONE T=\"ZONE " << MYRANK() << "\" SOLUTIONTIME=" << time << std::endl;
		outfile << "n=" << num_subcell_nodes_ << " ,e=" << num_subcell_cells_ << " ,f=fepoint, et=triangle" << std::endl;
	}
	else if (dimension_ == 3) {
		outfile << "TITLE = \"field \"" << std::endl;
		outfile << "VARIABLES = \"x\",\"y\",\"z\",\"Cp\"" << std::endl;
		outfile << "ZONE T=\"ZONE " << MYRANK() << "\" SOLUTIONTIME=" << time << std::endl;
		outfile << "n=" << num_subcell_nodes_ << " ,e=" << num_subcell_cells_ << " ,f=fepoint, et=triangle" << std::endl;
	}

	const std::vector<int_t>& bdry_owner_cell = grid_->getBdryOwnerCell();
	for (int_t index = 0; index < num_wall_bdries_; index++) {

		const int_t& ibdry = wall_bdry_list_[index];
		const int_t& owner_cell = bdry_owner_cell[ibdry];
		const int_t num_points = coords_[index].size() / dimension_;
		std::vector<real_t> solution(num_points*num_states_, 0.0);

		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					solution[ipoint*num_states_ + istate] += (*solution_)[owner_cell][istate*num_basis_ + ibasis] * basis_[index][ipoint*num_basis_ + ibasis];

		std::vector<real_t> Cp = equation_->calPressureCoefficients(solution);

		for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
			for (int_t idim = 0; idim < dimension_; idim++)
				outfile << coords_[index][ipoint*dimension_ + idim] << "\t";
			outfile << Cp[ipoint] << std::endl;
		}
	}

	if (dimension_ == 2) {
		int_t inode = 1;
		for (int_t index = 0; index < num_wall_bdries_; index++) {
			std::vector<int_t> connects = post_elements_[index]->getPostConnects(post_order_);
			for (int_t isubcell = 0; isubcell < connects.size() / dimension_; isubcell++) {
				for (int_t idim = 0; idim < dimension_; idim++)
					outfile << inode + connects[isubcell*dimension_ + idim] << "\t";
				outfile << inode + connects[isubcell*dimension_ + dimension_ - 1] << std::endl;
			}
			inode += coords_[index].size() / dimension_;
		}
	}
	else if (dimension_ == 3) {
		int_t inode = 1;
		for (int_t index = 0; index < num_wall_bdries_; index++) {
			std::vector<int_t> connects = post_elements_[index]->getPostConnects(post_order_);
			for (int_t isubcell = 0; isubcell < connects.size() / dimension_; isubcell++) {
				for (int_t idim = 0; idim < dimension_; idim++)
					outfile << inode + connects[isubcell*dimension_ + idim] << "\t";
				outfile << std::endl;
			}
			inode += coords_[index].size() / dimension_;
		}
	}
	outfile.close();
}

void Aerodynamics::postDuringPressureCoefficients(int_t& strandid, const real_t time)
{
	if (aerodynamics_ == false) return;
	if (num_wall_bdries_ == 0) return;

	char tag_name[10]; sprintf(tag_name, "%d", MYRANK());
	std::string filename = return_address_ + "/during/RANK_Cp" + tag_name + ".plt";
	std::ofstream outfile;
	if (strandid == 1) outfile.open(filename, std::ios::trunc);
	else outfile.open(filename, std::ios::app);
	//outfile << std::scientific;
	//outfile.precision(16);

	MASTER_MESSAGE("Post: File output(" + filename + ")");

	if (dimension_ == 2) {
		outfile << "TITLE = \"field \"" << std::endl;
		outfile << "VARIABLES = \"x\",\"y\",\"Cp\"" << std::endl;
		outfile << "ZONE T=\"ZONE " << MYRANK() << "\" SOLUTIONTIME=" << time << std::endl;
		outfile << "n=" << num_subcell_nodes_ << " ,e=" << num_subcell_cells_ << " ,f=fepoint, et=triangle" << std::endl;
	}
	else if (dimension_ == 3) {
		outfile << "TITLE = \"field \"" << std::endl;
		outfile << "VARIABLES = \"x\",\"y\",\"z\",\"Cp\"" << std::endl;
		outfile << "ZONE T=\"ZONE " << MYRANK() << "\" SOLUTIONTIME=" << time << std::endl;
		outfile << "n=" << num_subcell_nodes_ << " ,e=" << num_subcell_cells_ << " ,f=fepoint, et=triangle" << std::endl;
	}

	const std::vector<int_t>& bdry_owner_cell = grid_->getBdryOwnerCell();
	for (int_t index = 0; index < num_wall_bdries_; index++) {

		const int_t& ibdry = wall_bdry_list_[index];
		const int_t& owner_cell = bdry_owner_cell[ibdry];
		const int_t num_points = coords_[index].size() / dimension_;
		std::vector<real_t> solution(num_points*num_states_, 0.0);

		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					solution[ipoint*num_states_ + istate] += (*solution_)[owner_cell][istate*num_basis_ + ibasis] * basis_[index][ipoint*num_basis_ + ibasis];

		std::vector<real_t> Cp = equation_->calPressureCoefficients(solution);

		for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
			for (int_t idim = 0; idim < dimension_; idim++)
				outfile << coords_[index][ipoint*dimension_ + idim] << "\t";
			outfile << Cp[ipoint] << std::endl;
		}
	}

	if (dimension_ == 2) {
		int_t inode = 1;
		for (int_t index = 0; index < num_wall_bdries_; index++) {
			std::vector<int_t> connects = post_elements_[index]->getPostConnects(post_order_);
			for (int_t isubcell = 0; isubcell < connects.size() / dimension_; isubcell++) {
				for (int_t idim = 0; idim < dimension_; idim++)
					outfile << inode + connects[isubcell*dimension_ + idim] << "\t";
				outfile << inode + connects[isubcell*dimension_ + dimension_ - 1] << std::endl;
			}
			inode += coords_[index].size() / dimension_;
		}
	}
	else if (dimension_ == 3) {
		int_t inode = 1;
		for (int_t index = 0; index < num_wall_bdries_; index++) {
			std::vector<int_t> connects = post_elements_[index]->getPostConnects(post_order_);
			for (int_t isubcell = 0; isubcell < connects.size() / dimension_; isubcell++) {
				for (int_t idim = 0; idim < dimension_; idim++)
					outfile << inode + connects[isubcell*dimension_ + idim] << "\t";
				outfile << std::endl;
			}
			inode += coords_[index].size() / dimension_;
		}
	}
	outfile.close();
}
