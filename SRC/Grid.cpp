#include "../INC/Grid.h"

void Grid::initialize()
{
	const Config& config = Config::getInstance();

	dimension_ = config.getDimension();
	order_ = config.getOrder();

	if (dimension_ == 1)
		num_basis_ = order_ + 1;
	else if (dimension_ == 2)
		num_basis_ = (order_ + 1)*(order_ + 2) / 2;
	else if (dimension_ == 3)
		num_basis_ = (order_ + 1)*(order_ + 2)*(order_ + 3) / 6;
	
	GridBuilder gridbuilder;
	const std::string& file_format = config.getGridFormat();
	const std::string& file_name = config.getGridFileName();
	gridbuilder.MakeGrid(file_format, file_name, dimension_);
	
	// nodes
	START();
	num_global_nodes_ = gridbuilder.num_global_nodes_;
	num_total_nodes_ = gridbuilder.num_total_nodes_;
	num_nodes_ = gridbuilder.num_nodes_;
	node_coords_ = move(gridbuilder.node_coords_);
	node_global_index_ = move(gridbuilder.node_global_index_);
	
	node_to_cells_ = move(gridbuilder.node_to_cells_);
	node_to_vertex_ = move(gridbuilder.node_to_vertex_);
	
	gridbuilder.node_dist_.clear();
	gridbuilder.peribdry_node_.clear();
	gridbuilder.peribdry_node_to_matching_.clear();
	MASTER_MESSAGE("Message(Grid): nodes data are reconstructed(Time: " + TO_STRING(STOP()) + "s)");
	SYNCRO();
	
	// cells
	START();
	num_global_cells_ = gridbuilder.num_global_cells_;
	num_total_cells_ = gridbuilder.num_total_cells_;
	num_cells_ = gridbuilder.num_cells_;
	cell_to_node_ptr_ = move(gridbuilder.cell_to_node_ptr_);
	cell_to_node_ind_ = move(gridbuilder.cell_to_node_ind_);
	cell_elem_type_ = move(gridbuilder.cell_elem_type_);
	cell_order_ = move(gridbuilder.cell_order_);
	cell_to_subnode_ptr_ = move(gridbuilder.cell_to_subnode_ptr_);
	cell_to_subnode_ind_ = move(gridbuilder.cell_to_subnode_ind_);
	cell_global_index_ = move(gridbuilder.cell_global_index_);
	cell_to_cells_ = move(gridbuilder.cell_to_cells_);
	cell_to_faces_ = move(gridbuilder.cell_to_faces_);
	
	gridbuilder.cell_dist_.clear();
	gridbuilder.cell_color_.clear();
	
	std::vector<Element*> cell_element(num_total_cells_);
	std::vector<int_t> num_cell_quad_points(num_total_cells_);
	std::vector<std::vector<real_t>> cell_quad_points(num_total_cells_);
	std::vector<std::vector<real_t>> cell_quad_weights(num_total_cells_);
	std::vector<std::shared_ptr<Basis>> cell_basis(num_total_cells_);
	std::vector<std::vector<real_t>> cell_basis_value(num_total_cells_);
	std::vector<std::vector<real_t>> cell_div_basis_value(num_total_cells_);
	std::vector<real_t> cell_volumes(num_total_cells_, 0.0);
	std::vector<real_t> cell_proj_volumes(num_total_cells_*dimension_, 0.0);
	std::vector<std::vector<real_t>> cell_vertex_basis_value(num_total_cells_);
	std::vector<std::vector<real_t>> cell_coefficients(num_cells_);
	std::vector<std::vector<real_t>> cell_source_coefficients(num_cells_);
	std::vector<std::vector<real_t>> cell_owner_to_owner_auxiliary_jacobian(num_total_cells_); // for implicit
	std::vector<std::vector<real_t>> cell_owner_to_neighbor_auxiliary_jacobian(num_total_cells_); // for implicit
	
	for (int_t icell = 0; icell < num_total_cells_; icell++){
		cell_element[icell] = ElementFactory::getInstance().getElement(static_cast<ElemType>(cell_elem_type_[icell]));
		
		std::vector<real_t> cell_sub_coords;
		int_t start_index = cell_to_subnode_ptr_[icell];
		int_t end_index = cell_to_subnode_ptr_[icell + 1];
		cell_sub_coords.reserve(dimension_*(end_index - start_index));
		for (int_t inode = start_index; inode < end_index; inode++) {
			for (int_t idim = 0; idim < dimension_; idim++)
				cell_sub_coords.push_back(node_coords_[cell_to_subnode_ind_[inode] * dimension_ + idim]);
		}
		std::vector<real_t> center(dimension_, 0.0);
		const int_t num_subnodes = end_index - start_index;
		for (int_t inode = 0; inode < num_subnodes; inode++)
			for (int_t idim = 0; idim < dimension_; idim++)
				center[idim] += cell_sub_coords[inode*dimension_ + idim];
		for (auto&& var : center)
			var /= real_t(num_subnodes);

		std::shared_ptr<Jacobian> jacobian = JacobianFactory::getJacobian(static_cast<ElemType>(cell_elem_type_[icell]), cell_order_[icell]);
		jacobian->setCoords(cell_sub_coords);
		jacobian->setTopology();

		std::vector<real_t> quad_points;
		std::vector<real_t> quad_weights;
		Quadrature::Volume::getVolumeQdrt(static_cast<ElemType>(cell_elem_type_[icell]), jacobian, 2 * order_, quad_points, quad_weights);
		for (auto&& weight : quad_weights)
			weight = std::abs(weight);
		num_cell_quad_points[icell] = quad_weights.size();
		cell_quad_points[icell] = quad_points;
		cell_quad_weights[icell] = quad_weights;


		std::vector<real_t> cell_coords;
		start_index = cell_to_node_ptr_[icell];
		end_index = cell_to_node_ptr_[icell + 1];
		cell_coords.reserve(dimension_*(end_index - start_index));
		for (int_t inode = start_index; inode < end_index; inode++) {
			for (int_t idim = 0; idim < dimension_; idim++)
				cell_coords.push_back(node_coords_[cell_to_node_ind_[inode] * dimension_ + idim]);
		}

		cell_basis[icell] = BasisFactory::getBasis(dimension_, ElemType::SPLX);
		cell_basis[icell]->setRotation(cell_coords, static_cast<ElemType>(cell_elem_type_[icell]));
		cell_basis[icell]->calculateBasis(order_, center, quad_points, quad_weights);
		
		cell_basis_value[icell] = cell_basis[icell]->getBasis(0, order_, quad_points);
		cell_div_basis_value[icell] = cell_basis[icell]->getDivBasis(0, order_, quad_points);

		for (auto&& weights : quad_weights)
			cell_volumes[icell] += weights;

		cell_vertex_basis_value[icell] = cell_basis[icell]->getBasis(0, order_, cell_coords);

		for (auto&& nodes_per_face : cell_element[icell]->getFaceNodes()) {
			std::vector<real_t> coords;
			for (auto&& inode : nodes_per_face)
				for (int_t idim = 0; idim < dimension_; idim++)
					coords.push_back(cell_coords[inode*dimension_ + idim]);
			for (int_t idim = 0; idim < dimension_; idim++)
				cell_proj_volumes[icell*dimension_ + idim] += 0.5*calProjVolume(coords, idim);
		}

		if (icell >= num_cells_) continue;

		cell_coefficients[icell].resize(num_cell_quad_points[icell] * num_basis_*dimension_);
		for (int_t ipoint = 0; ipoint < num_cell_quad_points[icell]; ipoint++){
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++){
				for (int_t idim = 0; idim < dimension_; idim++){
					cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis*dimension_ + idim] = quad_weights[ipoint] * cell_div_basis_value[icell][ipoint*num_basis_*dimension_ + ibasis*dimension_ + idim];
				}
			}
		}
		
		cell_source_coefficients[icell].resize(num_cell_quad_points[icell] * num_basis_);
		for (int_t ipoint = 0; ipoint < num_cell_quad_points[icell]; ipoint++) {
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++) {
				cell_source_coefficients[icell][ipoint*num_basis_ + ibasis] = quad_weights[ipoint] * cell_basis_value[icell][ipoint*num_basis_ + ibasis];
			}
		}
	}

	for (int_t icell = 0; icell < num_total_cells_; icell++)
	{
		cell_owner_to_owner_auxiliary_jacobian[icell].resize(num_cell_quad_points[icell] * num_basis_ * dimension_); // for implicit
		cell_owner_to_neighbor_auxiliary_jacobian[icell].resize(cell_to_cells_[icell].size() * num_cell_quad_points[icell] * num_basis_ * dimension_); // for implicit
	}		
	
	cell_element_ = move(cell_element);
	num_cell_quad_points_ = move(num_cell_quad_points);
	cell_quad_points_ = move(cell_quad_points);
	cell_quad_weights_ = move(cell_quad_weights);
	cell_basis_ = move(cell_basis);
	cell_basis_value_ = move(cell_basis_value);
	cell_div_basis_value_ = move(cell_div_basis_value);
	cell_volumes_ = move(cell_volumes);
	cell_proj_volumes_ = move(cell_proj_volumes);
	cell_vertex_basis_value_ = move(cell_vertex_basis_value);
	cell_coefficients_ = move(cell_coefficients);
	cell_source_coefficients_ = move(cell_source_coefficients);

	MASTER_MESSAGE("Message(Grid): cells data are reconstructed(Time: " + TO_STRING(STOP()) + "s)");
	SYNCRO();

	// faces
	START();
	num_faces_ = gridbuilder.num_faces_;
	num_total_faces_ = gridbuilder.num_total_faces_;
	face_owner_cell_ = move(gridbuilder.face_owner_cell_);
	face_neighbor_cell_ = move(gridbuilder.face_neighbor_cell_);
	face_owner_type_ = move(gridbuilder.face_owner_type_);
	face_neighbor_type_ = move(gridbuilder.face_neighbor_type_);

	gridbuilder.face_to_node_ptr_.clear();
	gridbuilder.face_to_node_ind_.clear();

	std::vector<int_t> num_face_quad_points(num_faces_);
	std::vector<std::vector<real_t>> face_owner_basis_value(num_faces_);
	std::vector<std::vector<real_t>> face_neighbor_basis_value(num_faces_);
	std::vector<std::vector<real_t>> face_owner_div_basis_value(num_faces_);
	std::vector<std::vector<real_t>> face_neighbor_div_basis_value(num_faces_);
	std::vector<std::vector<real_t>> face_normals(num_faces_);
	std::vector<std::vector<real_t>> face_owner_coefficients(num_faces_);
	std::vector<std::vector<real_t>> face_neighbor_coefficients(num_faces_);
	std::vector<std::vector<real_t>> face_owner_auxiliary_coefficients(num_faces_);
	std::vector<std::vector<real_t>> face_neighbor_auxiliary_coefficients(num_faces_);
	std::vector<std::vector<real_t>> face_owner_to_owner_auxiliary_jacobian(num_faces_); // for implicit
	std::vector<std::vector<real_t>> face_owner_to_neighbor_auxiliary_jacobian(num_faces_); // for implicit
	std::vector<std::vector<real_t>> face_neighbor_to_owner_auxiliary_jacobian(num_faces_); // for implicit
	std::vector<std::vector<real_t>> face_neighbor_to_neighbor_auxiliary_jacobian(num_faces_); // for implicit

	std::vector<int_t> num_face_quad_points_hMLPBD(num_faces_);
	std::vector<real_t> face_areas(num_faces_);
	std::vector<real_t> face_characteristic_length(num_faces_);
	std::vector<std::vector<real_t>> face_owner_basis_value_hMLPBD(num_faces_);
	std::vector<std::vector<real_t>> face_neighbor_basis_value_hMLPBD(num_faces_);
	std::vector<std::vector<real_t>> face_weights_hMLPBD(num_faces_);

	for (int_t iface = 0; iface < num_faces_;iface++){
		const int_t& owner_index = face_owner_cell_[iface];
		const int_t& owner_type = face_owner_type_[iface];
		const int_t& neighbor_index = face_neighbor_cell_[iface];

		std::vector<real_t> face_sub_coords;
		const int_t& start_index = cell_to_subnode_ptr_[owner_index];
		const std::vector<int_t>& face_nodes = cell_element_[owner_index]->getFacetypeNodes(cell_order_[owner_index])[owner_type];
		face_sub_coords.reserve(dimension_*face_nodes.size());
		for (auto&& inode : face_nodes) {
			for (int_t idim = 0; idim < dimension_; idim++)
				face_sub_coords.push_back(node_coords_[dimension_*cell_to_subnode_ind_[start_index + inode] + idim]);
		}

		ElemType face_elemtype = cell_element_[owner_index]->getFaceElemType(owner_type);
		std::shared_ptr<Jacobian> face_jacobian = JacobianFactory::getFaceJacobian(face_elemtype, cell_order_[owner_index]);
		face_jacobian->setCoords(face_sub_coords);
		face_jacobian->setTopology();

		std::vector<real_t> quad_points;
		std::vector<real_t> quad_weights;
		std::vector<real_t> adjmat;
		Quadrature::Surface::getSurfaceQdrt(face_elemtype, face_jacobian, 2 * order_ + 1, quad_points, quad_weights, adjmat);
		for (auto&& weight : quad_weights)
			weight = std::abs(weight);
		num_face_quad_points[iface] = quad_weights.size();

		int_t orientation;
		std::vector<real_t> owner_cell_basis_value = cell_basis_[owner_index]->getBasis(0, order_, quad_points);
		std::vector<real_t> neighbor_cell_basis_value = cell_basis_[neighbor_index]->getBasis(0, order_, quad_points);
		const std::vector<real_t> owner_cell_div_basis_value = cell_basis_[owner_index]->getDivBasis(0, order_, quad_points);
		const std::vector<real_t> neighbor_cell_div_basis_value = cell_basis_[neighbor_index]->getDivBasis(0, order_, quad_points);
		face_owner_basis_value[iface] = owner_cell_basis_value;
		face_neighbor_basis_value[iface] = neighbor_cell_basis_value;
		face_owner_div_basis_value[iface] = owner_cell_div_basis_value;
		face_neighbor_div_basis_value[iface] = neighbor_cell_div_basis_value;
		face_normals[iface].resize(num_face_quad_points[iface] * dimension_);
		face_owner_coefficients[iface].resize(num_face_quad_points[iface] * num_basis_);
		face_neighbor_coefficients[iface].resize(num_face_quad_points[iface] * num_basis_);
		for (int_t ipoint = 0; ipoint < num_face_quad_points[iface]; ipoint++){
			if (ipoint == 0){
				std::vector<real_t> normal(dimension_);
				for (int_t idim = 0; idim < dimension_; idim++)
					normal[idim] = adjmat[idim];
				orientation = normalOrientation(normal, &cell_to_node_ind_[cell_to_node_ptr_[owner_index]], cell_element_[owner_index]->getFacetypeNodes(1)[face_owner_type_[iface]]);
			}

			real_t sum = 0.0;
			for (int_t idim = 0; idim < dimension_; idim++)
				sum += adjmat[ipoint*dimension_ + idim] * adjmat[ipoint*dimension_ + idim];
			sum = std::sqrt(sum);
			for (int_t idim = 0; idim < dimension_; idim++)
				face_normals[iface][ipoint*dimension_ + idim] = real_t(orientation)*adjmat[ipoint*dimension_ + idim] / sum;
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				face_owner_coefficients[iface][ipoint*num_basis_ + ibasis] = owner_cell_basis_value[ipoint*num_basis_ + ibasis] * quad_weights[ipoint] * sum;
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis] = neighbor_cell_basis_value[ipoint*num_basis_ + ibasis] * quad_weights[ipoint] * sum;
		}

		face_owner_auxiliary_coefficients[iface].resize(num_face_quad_points[iface] * dimension_*num_basis_);
		face_neighbor_auxiliary_coefficients[iface].resize(num_face_quad_points[iface] * dimension_*num_basis_);
		for (int_t ipoint = 0; ipoint < num_face_quad_points[iface]; ipoint++){
			for (int_t idim = 0; idim < dimension_; idim++){
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++){
					face_owner_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim*num_basis_ + ibasis] = 0.5*real_t(orientation)*adjmat[ipoint*dimension_ + idim] * quad_weights[ipoint] * owner_cell_basis_value[ipoint*num_basis_ + ibasis];
					face_neighbor_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim*num_basis_ + ibasis] = -0.5*real_t(orientation)*adjmat[ipoint*dimension_ + idim] * quad_weights[ipoint] * neighbor_cell_basis_value[ipoint*num_basis_ + ibasis];
				}
			}
		}

		face_owner_to_owner_auxiliary_jacobian[iface].resize(num_face_quad_points[iface] * num_basis_ * dimension_); // for implicit
		face_owner_to_neighbor_auxiliary_jacobian[iface].resize(num_face_quad_points[iface] * num_basis_ * dimension_); // for implicit
		face_neighbor_to_owner_auxiliary_jacobian[iface].resize(num_face_quad_points[iface] * num_basis_ * dimension_); // for implicit
		face_neighbor_to_neighbor_auxiliary_jacobian[iface].resize(num_face_quad_points[iface] * num_basis_ * dimension_); // for implicit
		for (int_t ipoint = 0; ipoint < num_face_quad_points[iface]; ipoint++) // for implicit
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				for(int_t idim = 0; idim < dimension_; idim++)
					for(int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						for (int_t jpoint = 0; jpoint < num_face_quad_points[iface]; jpoint++)
						{
							face_owner_to_owner_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + ibasis*dimension_ + idim]
								-= face_owner_basis_value[iface][ipoint*num_basis_ + jbasis]
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * owner_cell_basis_value[jpoint*num_basis_ + ibasis] * owner_cell_basis_value[jpoint*num_basis_ + jbasis];
							face_owner_to_neighbor_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
								+= face_owner_basis_value[iface][ipoint*num_basis_ + jbasis] //+
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * neighbor_cell_basis_value[jpoint*num_basis_ + ibasis] * owner_cell_basis_value[jpoint*num_basis_ + jbasis];
							face_neighbor_to_owner_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
								-= face_neighbor_basis_value[iface][ipoint*num_basis_ + jbasis] //-
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * owner_cell_basis_value[jpoint*num_basis_ + ibasis] * neighbor_cell_basis_value[jpoint*num_basis_ + jbasis];
							face_neighbor_to_neighbor_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
								+= face_neighbor_basis_value[iface][ipoint*num_basis_ + jbasis]
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * neighbor_cell_basis_value[jpoint*num_basis_ + ibasis] * neighbor_cell_basis_value[jpoint*num_basis_ + jbasis];
						}
		for (int_t ipoint = 0; ipoint < num_cell_quad_points_[owner_index]; ipoint++) // for implicit
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						for (int_t jpoint = 0; jpoint < num_face_quad_points[iface]; jpoint++)
						{
							cell_owner_to_owner_auxiliary_jacobian[owner_index][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
								-= cell_basis_value_[owner_index][ipoint*num_basis_ + jbasis]
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * owner_cell_basis_value[jpoint*num_basis_ + ibasis] * owner_cell_basis_value[jpoint*num_basis_ + jbasis];

							int_t jneighbor;
							for (jneighbor = 0; jneighbor < cell_to_cells_[owner_index].size(); jneighbor++)
								if (cell_to_cells_[owner_index][jneighbor] == neighbor_index) break;
							cell_owner_to_neighbor_auxiliary_jacobian[owner_index][jneighbor*num_cell_quad_points_[owner_index]*num_basis_*dimension_ + ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
								+= cell_basis_value_[owner_index][ipoint*num_basis_ + jbasis]
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * neighbor_cell_basis_value[jpoint*num_basis_ + ibasis] * owner_cell_basis_value[jpoint*num_basis_ + jbasis];
						}
		for (int_t ipoint = 0; ipoint < num_cell_quad_points_[neighbor_index]; ipoint++) // for implicit
			for(int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						for (int_t jpoint = 0; jpoint < num_face_quad_points[iface]; jpoint++)
						{
							cell_owner_to_owner_auxiliary_jacobian[neighbor_index][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
								+= cell_basis_value_[neighbor_index][ipoint*num_basis_ + jbasis]
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * neighbor_cell_basis_value[jpoint*num_basis_ + ibasis] * neighbor_cell_basis_value[jpoint*num_basis_ + jbasis];						

							int_t jowner;
							for (jowner = 0; jowner < cell_to_cells_[neighbor_index].size(); jowner++)
								if (cell_to_cells_[neighbor_index][jowner] == owner_index) break;
							cell_owner_to_neighbor_auxiliary_jacobian[neighbor_index][jowner*num_cell_quad_points_[neighbor_index] * num_basis_*dimension_ + ipoint * num_basis_*dimension_ + ibasis * dimension_ + idim]
								-= cell_basis_value_[neighbor_index][ipoint*num_basis_ + jbasis]
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * owner_cell_basis_value[jpoint*num_basis_ + ibasis] * neighbor_cell_basis_value[jpoint*num_basis_ + jbasis];
						}
					
		// for hMLP_BD
		Quadrature::Surface::getSurfaceQdrt(face_elemtype, face_jacobian, order_, quad_points, quad_weights, adjmat);
		for (auto&& weight : quad_weights)
			weight = std::abs(weight);
		num_face_quad_points_hMLPBD[iface] = quad_weights.size();

		owner_cell_basis_value = cell_basis_[owner_index]->getBasis(0, order_, quad_points);
		neighbor_cell_basis_value = cell_basis_[neighbor_index]->getBasis(0, order_, quad_points);
		face_owner_basis_value_hMLPBD[iface] = owner_cell_basis_value;
		face_neighbor_basis_value_hMLPBD[iface] = neighbor_cell_basis_value;
		face_weights_hMLPBD[iface].resize(num_face_quad_points_hMLPBD[iface]);
		for (int_t ipoint = 0; ipoint < num_face_quad_points_hMLPBD[iface]; ipoint++) {
			real_t sum = 0.0;
			for (int_t idim = 0; idim < dimension_; idim++)
				sum += adjmat[ipoint*dimension_ + idim] * adjmat[ipoint*dimension_ + idim];
			sum = std::sqrt(sum);
			face_weights_hMLPBD[iface][ipoint] = quad_weights[ipoint] * sum;
		}

		face_areas[iface] = 0.0;
		for (auto&& weight : face_weights_hMLPBD[iface])
			face_areas[iface] += weight;
		if (dimension_ == 2)
			face_characteristic_length[iface] = face_areas[iface];
		else if (dimension_ == 3)
			face_characteristic_length[iface] = std::sqrt(face_areas[iface]);
	}

	num_face_quad_points_ = move(num_face_quad_points);
	face_owner_basis_value_ = move(face_owner_basis_value);
	face_neighbor_basis_value_ = move(face_neighbor_basis_value);
	face_owner_div_basis_value_ = move(face_owner_div_basis_value);
	face_neighbor_div_basis_value_ = move(face_neighbor_div_basis_value);
	face_normals_ = move(face_normals);
	face_owner_coefficients_ = move(face_owner_coefficients);
	face_neighbor_coefficients_ = move(face_neighbor_coefficients);
	face_owner_auxiliary_coefficients_ = move(face_owner_auxiliary_coefficients);
	face_neighbor_auxiliary_coefficients_ = move(face_neighbor_auxiliary_coefficients);
	face_owner_to_owner_auxiliary_jacobian_ = move(face_owner_to_owner_auxiliary_jacobian); // for implicit
	face_owner_to_neighbor_auxiliary_jacobian_ = move(face_owner_to_neighbor_auxiliary_jacobian); // for implicit
	face_neighbor_to_owner_auxiliary_jacobian_ = move(face_neighbor_to_owner_auxiliary_jacobian); // for implicit
	face_neighbor_to_neighbor_auxiliary_jacobian_ = move(face_neighbor_to_neighbor_auxiliary_jacobian); // for implicit

	num_face_quad_points_hMLPBD_ = move(num_face_quad_points_hMLPBD);
	face_areas_ = move(face_areas);
	face_characteristic_length_ = move(face_characteristic_length);
	face_owner_basis_value_hMLPBD_ = move(face_owner_basis_value_hMLPBD);
	face_neighbor_basis_value_hMLPBD_ = move(face_neighbor_basis_value_hMLPBD);
	face_weights_hMLPBD_ = move(face_weights_hMLPBD);

	MASTER_MESSAGE("Message(Grid): faces data are reconstructed(Time: " + TO_STRING(STOP()) + "s)");
	SYNCRO();

	// boundaries
	START();
	num_global_bdries_ = gridbuilder.num_global_bdries_;
	num_total_bdries_ = gridbuilder.num_total_bdries_;
	num_bdries_ = gridbuilder.num_bdries_;
	bdry_type_ = move(gridbuilder.bdry_type_);
	bdry_owner_cell_ = move(gridbuilder.bdry_owner_cell_);
	bdry_owner_type_ = move(gridbuilder.bdry_owner_type_);

	gridbuilder.bdry_dist_.clear();
	gridbuilder.bdry_to_node_ptr_.clear();
	gridbuilder.bdry_to_node_ind_.clear();
	gridbuilder.bdry_color_.clear();

	std::vector<int_t> num_bdry_quad_points(num_bdries_);
	std::vector<std::vector<real_t>> bdry_owner_basis_value(num_bdries_);
	std::vector<std::vector<real_t>> bdry_owner_div_basis_value(num_bdries_);
	std::vector<std::vector<real_t>> bdry_normals(num_bdries_);
	std::vector<std::vector<real_t>> bdry_owner_coefficients(num_bdries_);
	std::vector<std::vector<real_t>> bdry_owner_auxiliary_coefficients(num_bdries_);
	std::vector<std::vector<real_t>> bdry_auxiliary_jacobian_coefficients(num_bdries_); // for implicit
	std::vector<std::vector<real_t>> bdry_cell_auxiliary_jacobian_coefficients(num_bdries_); // for implicit

	for (int_t ibdry = 0; ibdry < num_bdries_; ibdry++){
		const int_t& owner_index = bdry_owner_cell_[ibdry];
		const int_t& owner_type = bdry_owner_type_[ibdry];
		
		std::vector<real_t> bdry_sub_coords;
		const int_t& start_index = cell_to_subnode_ptr_[owner_index];
		const std::vector<int_t>& bdry_nodes = cell_element_[owner_index]->getFacetypeNodes(cell_order_[owner_index])[owner_type];
		bdry_sub_coords.reserve(dimension_*bdry_nodes.size());
		for (auto&& inode : bdry_nodes) {
			for (int_t idim = 0; idim < dimension_; idim++)
				bdry_sub_coords.push_back(node_coords_[dimension_*cell_to_subnode_ind_[start_index + inode] + idim]);
		}

		ElemType bdry_elemtype = cell_element_[owner_index]->getFaceElemType(owner_type);
		std::shared_ptr<Jacobian> bdry_jacobian = JacobianFactory::getFaceJacobian(bdry_elemtype, cell_order_[owner_index]);
		bdry_jacobian->setCoords(bdry_sub_coords);
		bdry_jacobian->setTopology();

		std::vector<real_t> quad_points;
		std::vector<real_t> quad_weights;
		std::vector<real_t> adjmat;
		Quadrature::Surface::getSurfaceQdrt(bdry_elemtype, bdry_jacobian, 2 * order_ + 1, quad_points, quad_weights, adjmat);
		for (auto&& weight : quad_weights)
			weight = std::abs(weight);
		num_bdry_quad_points[ibdry] = quad_weights.size();

		int_t orientation;
		const std::vector<real_t> owner_cell_basis_value = cell_basis_[owner_index]->getBasis(0, order_, quad_points);
		const std::vector<real_t> owner_cell_div_basis_value = cell_basis_[owner_index]->getDivBasis(0, order_, quad_points);
		bdry_owner_basis_value[ibdry] = owner_cell_basis_value;
		bdry_owner_div_basis_value[ibdry] = owner_cell_div_basis_value;
		bdry_normals[ibdry].resize(num_bdry_quad_points[ibdry] * dimension_);
		bdry_owner_coefficients[ibdry].resize(num_bdry_quad_points[ibdry] * num_basis_);
		for (int_t ipoint = 0; ipoint < num_bdry_quad_points[ibdry]; ipoint++){
			if (ipoint == 0){
				std::vector<real_t> normal(dimension_);
				for (int_t idim = 0; idim < dimension_; idim++)
					normal[idim] = adjmat[idim];
				orientation = normalOrientation(normal, &cell_to_node_ind_[cell_to_node_ptr_[owner_index]], cell_element_[owner_index]->getFacetypeNodes(1)[bdry_owner_type_[ibdry]]);
			}

			real_t sum = 0.0;
			for (int_t idim = 0; idim < dimension_; idim++)
				sum += adjmat[ipoint*dimension_ + idim] * adjmat[ipoint*dimension_ + idim];
			sum = std::sqrt(sum);
			for (int_t idim = 0; idim < dimension_; idim++)
				bdry_normals[ibdry][ipoint*dimension_ + idim] = real_t(orientation)*adjmat[ipoint*dimension_ + idim] / sum;
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] = owner_cell_basis_value[ipoint*num_basis_ + ibasis] * quad_weights[ipoint] * sum;
		}

		bdry_owner_auxiliary_coefficients[ibdry].resize(num_bdry_quad_points[ibdry] * dimension_*num_basis_);
		for (int_t ipoint = 0; ipoint < num_bdry_quad_points[ibdry]; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			bdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim*num_basis_ + ibasis] = 0.5*real_t(orientation)*adjmat[ipoint*dimension_ + idim] * quad_weights[ipoint] * owner_cell_basis_value[ipoint*num_basis_ + ibasis];
		
		bdry_auxiliary_jacobian_coefficients[ibdry].resize(num_bdry_quad_points[ibdry] * num_basis_ *dimension_*num_bdry_quad_points[ibdry]); // for implicit
		for (int_t ipoint = 0; ipoint < num_bdry_quad_points[ibdry]; ipoint++) // for implicit
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						for (int_t jpoint = 0; jpoint < num_bdry_quad_points[ibdry]; jpoint++)
						{
							bdry_auxiliary_jacobian_coefficients[ibdry][ipoint*num_basis_*dimension_*num_bdry_quad_points[ibdry] + ibasis * dimension_*num_bdry_quad_points[ibdry] + idim * num_bdry_quad_points[ibdry] + jpoint]
								+= bdry_owner_basis_value[ibdry][ipoint*num_basis_ + jbasis]
								* real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * owner_cell_basis_value[jpoint*num_basis_ + ibasis] * owner_cell_basis_value[jpoint*num_basis_ + jbasis];
						}

		bdry_cell_auxiliary_jacobian_coefficients[ibdry].resize(num_cell_quad_points_[owner_index] * num_basis_ *dimension_*num_bdry_quad_points[ibdry]); // for implicit
		for (int_t ipoint = 0; ipoint < num_cell_quad_points_[owner_index]; ipoint++) // for implicit
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						for (int_t jpoint = 0; jpoint < num_bdry_quad_points[ibdry]; jpoint++)
						{
							bdry_cell_auxiliary_jacobian_coefficients[ibdry][ipoint*num_basis_*dimension_*num_bdry_quad_points[ibdry] + ibasis * dimension_*num_bdry_quad_points[ibdry] + idim * num_bdry_quad_points[ibdry] + jpoint]
								+= cell_basis_value_[owner_index][ipoint*num_basis_ + jbasis]
								* real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * owner_cell_basis_value[jpoint*num_basis_ + ibasis] * owner_cell_basis_value[jpoint*num_basis_ + jbasis];
						}
		//for (int_t ipoint = 0; ipoint < num_cell_quad_points_[owner_index]; ipoint++) // for implicit
		//	for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		//		for (int_t idim = 0; idim < dimension_; idim++)
		//			for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
		//				for (int_t jpoint = 0; jpoint < num_bdry_quad_points[ibdry]; jpoint++)
		//				{
		//					cell_owner_to_owner_auxiliary_jacobian[owner_index][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
		//						-= cell_basis_value_[owner_index][ipoint*num_basis_ + jbasis]
		//						* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * owner_cell_basis_value[jpoint*num_basis_ + ibasis] * owner_cell_basis_value[jpoint*num_basis_ + jbasis];
		//				}
	}

	num_bdry_quad_points_ = move(num_bdry_quad_points);
	bdry_owner_basis_value_ = move(bdry_owner_basis_value);
	bdry_owner_div_basis_value_ = move(bdry_owner_div_basis_value);
	bdry_normals_ = move(bdry_normals);
	bdry_owner_coefficients_ = move(bdry_owner_coefficients);
	bdry_owner_auxiliary_coefficients_ = move(bdry_owner_auxiliary_coefficients);
	bdry_auxiliary_jacobian_coefficients_ = move(bdry_auxiliary_jacobian_coefficients); // for implicit
	bdry_cell_auxiliary_jacobian_coefficients_ = move(bdry_cell_auxiliary_jacobian_coefficients); // for implicit

	MASTER_MESSAGE("Message(Grid): bdries data are reconstructed(Time: " + TO_STRING(STOP()) + "s)");
	SYNCRO();

	// periodic boundaries
	START();
	num_global_peribdries_ = gridbuilder.num_global_peribdries_;
	num_total_peribdries_ = gridbuilder.num_total_peribdries_;
	num_peribdries_ = gridbuilder.num_peribdries_;
	peribdry_owner_cell_ = move(gridbuilder.peribdry_owner_cell_);
	peribdry_neighbor_cell_ = move(gridbuilder.peribdry_neighbor_cell_);
	peribdry_owner_type_ = move(gridbuilder.peribdry_owner_type_);
	peribdry_neighbor_type_ = move(gridbuilder.peribdry_neighbor_type_);

	gridbuilder.peribdry_dist_.clear();
	gridbuilder.peribdry_to_node_ptr_.clear();
	gridbuilder.peribdry_to_node_ind_.clear();
	gridbuilder.peribdry_direction_.clear();
	gridbuilder.peribdry_color_.clear();
	gridbuilder.peribdry_to_matching_node_ind_.clear();

	std::vector<int_t> num_peribdry_quad_points(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_owner_basis_value(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_neighbor_basis_value(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_owner_div_basis_value(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_neighbor_div_basis_value(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_normals(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_owner_coefficients(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_neighbor_coefficients(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_owner_auxiliary_coefficients(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_neighbor_auxiliary_coefficients(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_owner_to_owner_auxiliary_jacobian(num_peribdries_); // for implicit
	std::vector<std::vector<real_t>> peribdry_owner_to_neighbor_auxiliary_jacobian(num_peribdries_); // for implicit
	std::vector<std::vector<real_t>> peribdry_neighbor_to_owner_auxiliary_jacobian(num_peribdries_); // for implicit
	std::vector<std::vector<real_t>> peribdry_neighbor_to_neighbor_auxiliary_jacobian(num_peribdries_); // for implicit

	std::vector<int_t> num_peribdry_quad_points_hMLPBD(num_peribdries_);
	std::vector<real_t> peribdry_areas(num_peribdries_);
	std::vector<real_t> peribdry_characteristic_length(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_owner_basis_value_hMLPBD(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_neighbor_basis_value_hMLPBD(num_peribdries_);
	std::vector<std::vector<real_t>> peribdry_weights_hMLPBD(num_peribdries_);

	for (int_t ibdry = 0; ibdry < num_peribdries_; ibdry++){
		const int_t& owner_index = peribdry_owner_cell_[ibdry];
		const int_t& owner_type = peribdry_owner_type_[ibdry];
		const int_t& neighbor_index = peribdry_neighbor_cell_[ibdry];
		const int_t& neighbor_type = peribdry_neighbor_type_[ibdry];

		std::vector<real_t> bdry_owner_sub_coords;
		int_t start_index = cell_to_subnode_ptr_[owner_index];
		const std::vector<int_t>& bdry_owner_nodes = cell_element_[owner_index]->getFacetypeNodes(cell_order_[owner_index])[owner_type];
		bdry_owner_sub_coords.reserve(dimension_*bdry_owner_nodes.size());
		for (auto&& inode : bdry_owner_nodes) {
			for (int_t idim = 0; idim < dimension_; idim++)
				bdry_owner_sub_coords.push_back(node_coords_[dimension_*cell_to_subnode_ind_[start_index + inode] + idim]);
		}

		std::vector<real_t> bdry_neighbor_sub_coords;
		start_index = cell_to_subnode_ptr_[neighbor_index];
		const std::vector<int_t>& bdry_neighbor_nodes = cell_element_[neighbor_index]->getFacetypeNodes(cell_order_[neighbor_index])[neighbor_type];
		bdry_neighbor_sub_coords.reserve(dimension_*bdry_neighbor_nodes.size());
		for (auto&& inode : bdry_neighbor_nodes) {
			for (int_t idim = 0; idim < dimension_; idim++)
				bdry_neighbor_sub_coords.push_back(node_coords_[dimension_*cell_to_subnode_ind_[start_index + inode] + idim]);
		}

		ElemType bdry_owner_elemtype = cell_element_[owner_index]->getFaceElemType(owner_type);
		std::shared_ptr<Jacobian> bdry_owner_jacobian = JacobianFactory::getFaceJacobian(bdry_owner_elemtype, cell_order_[owner_index]);
		bdry_owner_jacobian->setCoords(bdry_owner_sub_coords);
		bdry_owner_jacobian->setTopology();

		ElemType bdry_neighbor_elemtype = cell_element_[neighbor_index]->getFaceElemType(neighbor_type);
		std::shared_ptr<Jacobian> bdry_neighbor_jacobian = JacobianFactory::getFaceJacobian(bdry_neighbor_elemtype, cell_order_[neighbor_index]);
		bdry_neighbor_jacobian->setCoords(bdry_neighbor_sub_coords);
		bdry_neighbor_jacobian->setTopology();

		std::vector<real_t> quad_points;
		std::vector<real_t> quad_weights;
		std::vector<real_t> adjmat;
		std::vector<real_t> neighbor_quad_points;
		Quadrature::Surface::getSurfaceQdrt(bdry_neighbor_elemtype, bdry_neighbor_jacobian, 2 * order_ + 1, neighbor_quad_points, quad_weights, adjmat);
		Quadrature::Surface::getSurfaceQdrt(bdry_owner_elemtype, bdry_owner_jacobian, 2 * order_ + 1, quad_points, quad_weights, adjmat);
		for (auto&& weight : quad_weights)
			weight = std::abs(weight);
		num_peribdry_quad_points[ibdry] = quad_weights.size();

		int_t orientation;
		std::vector<real_t> owner_cell_basis_value = cell_basis_[owner_index]->getBasis(0, order_, quad_points);
		std::vector<real_t> neighbor_cell_basis_value = cell_basis_[neighbor_index]->getBasis(0, order_, neighbor_quad_points);
		const std::vector<real_t> owner_cell_div_basis_value = cell_basis_[owner_index]->getDivBasis(0, order_, quad_points);
		const std::vector<real_t> neighbor_cell_div_basis_value = cell_basis_[neighbor_index]->getDivBasis(0, order_, neighbor_quad_points);
		peribdry_owner_basis_value[ibdry] = owner_cell_basis_value;
		peribdry_neighbor_basis_value[ibdry] = neighbor_cell_basis_value;
		peribdry_owner_div_basis_value[ibdry] = owner_cell_div_basis_value;
		peribdry_neighbor_div_basis_value[ibdry] = neighbor_cell_div_basis_value;
		peribdry_normals[ibdry].resize(num_peribdry_quad_points[ibdry] * dimension_);
		peribdry_owner_coefficients[ibdry].resize(num_peribdry_quad_points[ibdry] * num_basis_);
		peribdry_neighbor_coefficients[ibdry].resize(num_peribdry_quad_points[ibdry] * num_basis_);
		for (int_t ipoint = 0; ipoint < num_peribdry_quad_points[ibdry]; ipoint++){
			if (ipoint == 0){
				std::vector<real_t> normal(dimension_);
				for (int_t idim = 0; idim < dimension_; idim++)
					normal[idim] = adjmat[idim];
				orientation = normalOrientation(normal, &cell_to_node_ind_[cell_to_node_ptr_[owner_index]], cell_element_[owner_index]->getFacetypeNodes(1)[peribdry_owner_type_[ibdry]]);
			}

			real_t sum = 0.0;
			for (int_t idim = 0; idim < dimension_; idim++)
				sum += adjmat[ipoint*dimension_ + idim] * adjmat[ipoint*dimension_ + idim];
			sum = std::sqrt(sum);
			for (int_t idim = 0; idim < dimension_; idim++)
				peribdry_normals[ibdry][ipoint*dimension_ + idim] = real_t(orientation)*adjmat[ipoint*dimension_ + idim] / sum;
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] = owner_cell_basis_value[ipoint*num_basis_ + ibasis] * quad_weights[ipoint] * sum;
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				peribdry_neighbor_coefficients[ibdry][ipoint*num_basis_ + ibasis] = neighbor_cell_basis_value[ipoint*num_basis_ + ibasis] * quad_weights[ipoint] * sum;
		}

		peribdry_owner_auxiliary_coefficients[ibdry].resize(num_peribdry_quad_points[ibdry] * dimension_*num_basis_);
		peribdry_neighbor_auxiliary_coefficients[ibdry].resize(num_peribdry_quad_points[ibdry] * dimension_*num_basis_);
		for (int_t ipoint = 0; ipoint < num_peribdry_quad_points[ibdry]; ipoint++){
			for (int_t idim = 0; idim < dimension_; idim++){
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++){
					peribdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim*num_basis_ + ibasis] = 0.5*real_t(orientation)*adjmat[ipoint*dimension_ + idim] * quad_weights[ipoint] * owner_cell_basis_value[ipoint*num_basis_ + ibasis];
					peribdry_neighbor_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim*num_basis_ + ibasis] = -0.5*real_t(orientation)*adjmat[ipoint*dimension_ + idim] * quad_weights[ipoint] * neighbor_cell_basis_value[ipoint*num_basis_ + ibasis];
				}
			}
		}

		peribdry_owner_to_owner_auxiliary_jacobian[ibdry].resize(num_peribdry_quad_points[ibdry] * num_basis_ * dimension_); // for implicit
		peribdry_owner_to_neighbor_auxiliary_jacobian[ibdry].resize(num_peribdry_quad_points[ibdry] * num_basis_ * dimension_); // for implicit
		peribdry_neighbor_to_owner_auxiliary_jacobian[ibdry].resize(num_peribdry_quad_points[ibdry] * num_basis_ * dimension_); // for implicit
		peribdry_neighbor_to_neighbor_auxiliary_jacobian[ibdry].resize(num_peribdry_quad_points[ibdry] * num_basis_ * dimension_); // for implicit
		for (int_t ipoint = 0; ipoint < num_peribdry_quad_points[ibdry]; ipoint++) // for implicit
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						for (int_t jpoint = 0; jpoint < num_peribdry_quad_points[ibdry]; jpoint++)
						{
							peribdry_owner_to_owner_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + ibasis*dimension_ + idim]
								-= peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + jbasis]
								*0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * owner_cell_basis_value[jpoint*num_basis_ + ibasis] * owner_cell_basis_value[jpoint*num_basis_ + jbasis];
							peribdry_owner_to_neighbor_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
								+= peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + jbasis] // +
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * neighbor_cell_basis_value[jpoint*num_basis_ + ibasis] * owner_cell_basis_value[jpoint*num_basis_ + jbasis];
							peribdry_neighbor_to_owner_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
								-= peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + jbasis] // -
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * owner_cell_basis_value[jpoint*num_basis_ + ibasis] * neighbor_cell_basis_value[jpoint*num_basis_ + jbasis];
							peribdry_neighbor_to_neighbor_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
								+= peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + jbasis]
								*0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * neighbor_cell_basis_value[jpoint*num_basis_ + ibasis] * neighbor_cell_basis_value[jpoint*num_basis_ + jbasis];
						}
		for (int_t ipoint = 0; ipoint < num_cell_quad_points_[owner_index]; ipoint++) // for implicit
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						for (int_t jpoint = 0; jpoint < num_peribdry_quad_points[ibdry]; jpoint++)
						{
							cell_owner_to_owner_auxiliary_jacobian[owner_index][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
								-= cell_basis_value_[owner_index][ipoint*num_basis_ + jbasis]
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * owner_cell_basis_value[jpoint*num_basis_ + ibasis] * owner_cell_basis_value[jpoint*num_basis_ + jbasis];

							int_t jneighbor;
							for (jneighbor = 0; jneighbor < cell_to_cells_[owner_index].size(); jneighbor++)
								if (cell_to_cells_[owner_index][jneighbor] == neighbor_index) break;
							cell_owner_to_neighbor_auxiliary_jacobian[owner_index][jneighbor*num_cell_quad_points_[owner_index]*num_basis_*dimension_ + ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
								+= cell_basis_value_[owner_index][ipoint*num_basis_ + jbasis]
								* 0.5*real_t(orientation)*adjmat[jpoint*dimension_ + idim] * quad_weights[jpoint] * neighbor_cell_basis_value[jpoint*num_basis_ + ibasis] * owner_cell_basis_value[jpoint*num_basis_ + jbasis];
						}

		// for hMLP_BD
		Quadrature::Surface::getSurfaceQdrt(bdry_neighbor_elemtype, bdry_neighbor_jacobian, order_, neighbor_quad_points, quad_weights, adjmat);
		Quadrature::Surface::getSurfaceQdrt(bdry_owner_elemtype, bdry_owner_jacobian, order_, quad_points, quad_weights, adjmat);
		for (auto&& weight : quad_weights)
			weight = std::abs(weight);
		num_peribdry_quad_points_hMLPBD[ibdry] = quad_weights.size();

		owner_cell_basis_value = cell_basis_[owner_index]->getBasis(0, order_, quad_points);
		neighbor_cell_basis_value = cell_basis_[neighbor_index]->getBasis(0, order_, neighbor_quad_points);
		peribdry_owner_basis_value_hMLPBD[ibdry] = owner_cell_basis_value;
		peribdry_neighbor_basis_value_hMLPBD[ibdry] = neighbor_cell_basis_value;
		peribdry_weights_hMLPBD[ibdry].resize(num_peribdry_quad_points_hMLPBD[ibdry]);
		for (int_t ipoint = 0; ipoint < num_peribdry_quad_points_hMLPBD[ibdry]; ipoint++) {
			real_t sum = 0.0;
			for (int_t idim = 0; idim < dimension_; idim++)
				sum += adjmat[ipoint*dimension_ + idim] * adjmat[ipoint*dimension_ + idim];
			sum = std::sqrt(sum);
			peribdry_weights_hMLPBD[ibdry][ipoint] = quad_weights[ipoint] * sum;
		}

		peribdry_areas[ibdry] = 0.0;
		for (auto&& weight : peribdry_weights_hMLPBD[ibdry])
			peribdry_areas[ibdry] += weight;
		if (dimension_ == 2)
			peribdry_characteristic_length[ibdry] = peribdry_areas[ibdry];
		else if (dimension_ == 3)
			peribdry_characteristic_length[ibdry] = std::sqrt(peribdry_areas[ibdry]);
	}

	num_peribdry_quad_points_ = move(num_peribdry_quad_points);
	peribdry_owner_basis_value_ = move(peribdry_owner_basis_value);
	peribdry_neighbor_basis_value_ = move(peribdry_neighbor_basis_value);
	peribdry_owner_div_basis_value_ = move(peribdry_owner_div_basis_value);
	peribdry_neighbor_div_basis_value_ = move(peribdry_neighbor_div_basis_value);
	peribdry_normals_ = move(peribdry_normals);
	peribdry_owner_coefficients_ = move(peribdry_owner_coefficients);
	peribdry_neighbor_coefficients_ = move(peribdry_neighbor_coefficients);
	peribdry_owner_auxiliary_coefficients_ = move(peribdry_owner_auxiliary_coefficients);
	peribdry_neighbor_auxiliary_coefficients_ = move(peribdry_neighbor_auxiliary_coefficients);
	peribdry_owner_to_owner_auxiliary_jacobian_ = move(peribdry_owner_to_owner_auxiliary_jacobian); // for implicit
	peribdry_owner_to_neighbor_auxiliary_jacobian_ = move(peribdry_owner_to_neighbor_auxiliary_jacobian); // for implicit
	peribdry_neighbor_to_owner_auxiliary_jacobian_ = move(peribdry_neighbor_to_owner_auxiliary_jacobian); // for implicit
	peribdry_neighbor_to_neighbor_auxiliary_jacobian_ = move(peribdry_neighbor_to_neighbor_auxiliary_jacobian); // for implicit

	num_peribdry_quad_points_hMLPBD_ = move(num_peribdry_quad_points_hMLPBD);
	peribdry_areas_ = move(peribdry_areas);
	peribdry_characteristic_length_ = move(peribdry_characteristic_length);
	peribdry_owner_basis_value_hMLPBD_ = move(peribdry_owner_basis_value_hMLPBD);
	peribdry_neighbor_basis_value_hMLPBD_ = move(peribdry_neighbor_basis_value_hMLPBD);
	peribdry_weights_hMLPBD_ = move(peribdry_weights_hMLPBD);

	MASTER_MESSAGE("Message(Grid): periodic bdries data are reconstructed(Time: " + TO_STRING(STOP()) + "s)");
	SYNCRO();

	num_send_cells_ = move(gridbuilder.num_send_cells_);
	num_recv_cells_ = move(gridbuilder.num_recv_cells_);
	send_cell_list_ = move(gridbuilder.send_cell_list_);
	recv_cell_list_ = move(gridbuilder.recv_cell_list_);

	// for implicit
	cell_owner_to_owner_auxiliary_jacobian_ = move(cell_owner_to_owner_auxiliary_jacobian);
	cell_owner_to_neighbor_auxiliary_jacobian_ = move(cell_owner_to_neighbor_auxiliary_jacobian);
	cell_petsc_index_ = gridbuilder.cell_petsc_index_;
	domain_label_ = gridbuilder.domain_label_;
}

real_t Grid::calProjVolume(const std::vector<real_t>& coords, const int idim) const
{
	int_t num_points = coords.size() / dimension_;
	if (num_points == 2){
		switch (idim){
		case 0: return std::abs(coords[dimension_ + 1] - coords[1]);
		case 1: return std::abs(coords[dimension_] - coords[0]);
		default: return 0.0;
		}
	}
	real_t sum = 0.0;
	switch (idim){
	case 0:
		sum += (coords[dimension_*(num_points - 1) + 1] - coords[1])*(coords[dimension_*(num_points - 1) + 2] + coords[2]);
		for (int_t i = 0; i < num_points - 1; i++)
			sum += (coords[dimension_*i + 1] - coords[dimension_*(i + 1) + 1])*(coords[dimension_*i + 2] + coords[dimension_*(i + 1) + 2]);
		return 0.5*std::abs(sum);
	case 1:
		sum += (coords[dimension_*(num_points - 1)] - coords[0])*(coords[dimension_*(num_points - 1) + 2] + coords[2]);
		for (int_t i = 0; i < num_points - 1; i++)
			sum += (coords[dimension_*i] - coords[dimension_*(i + 1)])*(coords[dimension_*i + 2] + coords[dimension_*(i + 1) + 2]);
		return 0.5*std::abs(sum);
	case 2:
		sum += (coords[dimension_*(num_points - 1)] - coords[0])*(coords[dimension_*(num_points - 1) + 1] + coords[1]);
		for (int_t i = 0; i < num_points - 1; i++)
			sum += (coords[dimension_*i] - coords[dimension_*(i + 1)])*(coords[dimension_*i + 1] + coords[dimension_*(i + 1) + 1]);
		return 0.5*std::abs(sum);
	}
	return 0.0;
}

int_t Grid::normalOrientation(const std::vector<real_t>& normal, const int_t* cell_to_node_ind, const std::vector<int_t>& face_to_node_ind) const
{
	int_t target_node = face_to_node_ind[0];
	int_t other_node = 0;
	
	for (;; other_node++){
		bool flag = true;
		for (auto&& face_node : face_to_node_ind){
			if (face_node == other_node){
				flag = false;
				break;
			}
		}
		if (flag == true) break;
	}

	std::vector<real_t> inward(dimension_);
	for (int_t idim = 0; idim < dimension_; idim++)
		inward[idim] = node_coords_[cell_to_node_ind[other_node] * dimension_ + idim] - node_coords_[cell_to_node_ind[target_node] * dimension_ + idim];

	std::vector<real_t> outward(dimension_);
	if (dimension_ == 2){
		outward[0] = node_coords_[cell_to_node_ind[face_to_node_ind[1]] * dimension_ + 1] - node_coords_[cell_to_node_ind[face_to_node_ind[0]] * dimension_ + 1];
		outward[1] = node_coords_[cell_to_node_ind[face_to_node_ind[0]] * dimension_] - node_coords_[cell_to_node_ind[face_to_node_ind[1]] * dimension_];
		if (outward[0] * inward[0] + outward[1] * inward[1] > 0.0){
			outward[0] *= (-1.0);
			outward[1] *= (-1.0);
		}
	}
	else{
		std::vector<real_t> vec1(dimension_);
		for (int_t idim = 0; idim < dimension_; idim++)
			vec1[idim] = node_coords_[cell_to_node_ind[face_to_node_ind[1]] * dimension_ + idim] - node_coords_[cell_to_node_ind[face_to_node_ind[0]] * dimension_ + idim];
		std::vector<real_t> vec2(dimension_);
		for (int_t idim = 0; idim < dimension_; idim++)
			vec2[idim] = node_coords_[cell_to_node_ind[face_to_node_ind[2]] * dimension_ + idim] - node_coords_[cell_to_node_ind[face_to_node_ind[1]] * dimension_ + idim];
		outward[0] = vec1[1] * vec2[2] - vec1[2] * vec2[1];
		outward[1] = vec1[2] * vec2[0] - vec1[0] * vec2[2];
		outward[2] = vec1[0] * vec2[1] - vec1[1] * vec2[0];
		if (outward[0] * inward[0] + outward[1] * inward[1] + outward[2] * inward[2] > 0.0)
		for (int_t idim = 0; idim < dimension_; idim++)
			outward[idim] *= (-1.0);
	}

	real_t sum = 0.0;
	for (int_t idim = 0; idim < dimension_; idim++)
		sum += outward[idim] * normal[idim];

	if (sum > 0.0)
		return 1;

	return -1;
}

void Grid::prpcForHmlpBdSim()
{
	ElemType simplex_type;
	if (dimension_ == 2) simplex_type = ElemType::TRIS;
	else if (dimension_ == 3) simplex_type = ElemType::TETS;
	Element* simplex_element = ElementFactory::getInstance().getElement(simplex_type);
	const int_t num_basis_p1 = dimension_ + 1;

	std::vector<std::vector<real_t>> simplex_average_coefficients(num_total_cells_);
	std::vector<std::vector<real_t>> simplex_p1_coefficients(num_total_cells_);
	std::vector<std::vector<real_t>> simplex_volume(num_total_cells_);
	
	for (int_t icell = 0; icell < num_total_cells_; icell++){

		const int_t& num_vertex = cell_element_[icell]->getNumNodes(1);
		simplex_average_coefficients[icell].resize(num_vertex*num_basis_, 0.0);
		simplex_p1_coefficients[icell].resize(num_vertex*num_basis_, 0.0);
		simplex_volume[icell].resize(num_vertex, 0.0);

		if (static_cast<ElemType>(cell_elem_type_[icell]) == ElemType::TRIS || static_cast<ElemType>(cell_elem_type_[icell]) == ElemType::TETS){
			for (int_t ivertex = 0; ivertex < num_vertex; ivertex++)
				simplex_volume[icell][ivertex] = cell_volumes_[icell];
			for (int_t ivertex = 0; ivertex < num_vertex; ivertex++){
				simplex_average_coefficients[icell][ivertex*num_basis_] = cell_vertex_basis_value_[icell][ivertex*num_basis_];
				for (int_t ibasis = 0; ibasis < num_basis_p1; ibasis++)
					simplex_p1_coefficients[icell][ivertex*num_basis_ + ibasis] = cell_vertex_basis_value_[icell][ivertex*num_basis_ + ibasis];
			}

			continue;
		}

		const std::vector<std::vector<int_t>> simplex_nodes_index = cell_element_[icell]->getSimplexNodesIndex();
		for (int_t ivertex = 0; ivertex < num_vertex; ivertex++){
			const int_t num_nodes = simplex_nodes_index[ivertex].size();

			std::vector<real_t> cell_coords;
			const int_t& start_index = cell_to_subnode_ptr_[icell];
			cell_coords.reserve(dimension_*num_nodes);
			for (int_t inode = 0; inode < num_nodes; inode++)
			for (int_t idim = 0; idim < dimension_; idim++)
				cell_coords.push_back(node_coords_[cell_to_subnode_ind_[start_index + simplex_nodes_index[ivertex][inode]] * dimension_ + idim]);

			std::shared_ptr<Jacobian> jacobian = JacobianFactory::getJacobian(simplex_type, 1);
			jacobian->setCoords(cell_coords);
			jacobian->setTopology();

			std::vector<real_t> quad_points;
			std::vector<real_t> quad_weights;
			Quadrature::Volume::getVolumeQdrt(simplex_type, jacobian, 2 * order_, quad_points, quad_weights);
			for (auto&& weight : quad_weights)
				simplex_volume[icell][ivertex] += weight;

			std::vector<real_t> center(dimension_, 0.0);
			for (int_t inode = 0; inode < num_nodes; inode++)
			for (int_t idim = 0; idim < dimension_; idim++)
				center[idim] += cell_coords[inode*dimension_ + idim];
			for (auto&& var : center)
				var /= real_t(num_nodes);
			std::shared_ptr<Basis> basis = BasisFactory::getBasis(dimension_, ElemType::SPLX);
			basis->setRotation(cell_coords, ElemType::SPLX);
			basis->calculateBasis(1, center, quad_points, quad_weights);

			const std::vector<real_t> cell_basis_value = cell_basis_[icell]->getBasis(0, order_, quad_points);
			const std::vector<real_t> simplex_basis_value = basis->getBasis(0, 1, quad_points);

			std::vector<real_t> transition_matrix(num_basis_*num_basis_p1, 0);
			for (int_t jbasis = 0; jbasis < num_basis_p1; jbasis++)
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			for (int_t ipoint = 0; ipoint < quad_weights.size(); ipoint++)
				transition_matrix[jbasis*num_basis_ + ibasis] += cell_basis_value[ipoint*num_basis_ + ibasis] * simplex_basis_value[ipoint*num_basis_p1 + jbasis] * quad_weights[ipoint];

			std::vector<real_t> com_coord(simplex_element->getNodesCoord(1).begin(), simplex_element->getNodesCoord(1).begin() + dimension_);
			std::vector<real_t> phy_coord = jacobian->calComToPhyCoord(com_coord);
			std::vector<real_t> basis_vertex_value = basis->getBasis(0, 1, phy_coord);

			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				simplex_average_coefficients[icell][ivertex*num_basis_ + ibasis] = transition_matrix[ibasis] * basis_vertex_value[0];
			for (int_t jbasis = 0; jbasis < num_basis_p1; jbasis++)
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				simplex_p1_coefficients[icell][ivertex*num_basis_ + ibasis] += transition_matrix[jbasis*num_basis_ + ibasis] * basis_vertex_value[jbasis];
		}
	}
	
	simplex_average_coefficients_ = move(simplex_average_coefficients);
	simplex_p1_coefficients_ = move(simplex_p1_coefficients);
	simplex_volume_ = move(simplex_volume);
}

void Grid::calWallDistance()
{
	START();
	const int_t ndomain = NDOMAIN();
	const int_t myrank = MYRANK();

	// Wall check
	const int_t wall_tag = BoundaryCondition::getInstance().getTagNumber("Wall");
	std::vector<real_t> wall_bdry_list;
	for (int_t ibdry = 0; ibdry<num_total_bdries_; ibdry++)
		if (bdry_type_[ibdry] == wall_tag)
			wall_bdry_list.push_back(ibdry);

	int_t num_wall = wall_bdry_list.size();
	std::vector<int_t> gather_temp(ndomain);
	MPI_Allgather(&num_wall, 1, MPI_INT_T, &gather_temp[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	std::vector<int_t> wall_process;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		if (gather_temp[idomain] > 0)
			wall_process.push_back(idomain);
	gather_temp.clear();

	// Bounding box
	real_t xmin, ymin, zmin;
	real_t xmax, ymax, zmax;
	xmin = ymin = zmin = 1.0E+10;
	xmax = ymax = zmax = -1.0E+10;
	for (int_t inode = 0; inode<num_total_nodes_; inode++) {
		if (xmin>node_coords_[dimension_*inode]) xmin = node_coords_[dimension_*inode];
		if (xmax<node_coords_[dimension_*inode]) xmax = node_coords_[dimension_*inode];
		if (ymin>node_coords_[dimension_*inode + 1]) ymin = node_coords_[dimension_*inode + 1];
		if (ymax<node_coords_[dimension_*inode + 1]) ymax = node_coords_[dimension_*inode + 1];
		if (dimension_ == 2) continue;
		if (zmin>node_coords_[dimension_*inode + 2]) zmin = node_coords_[dimension_*inode + 2];
		if (zmax<node_coords_[dimension_*inode + 2]) zmax = node_coords_[dimension_*inode + 2];
	}

	real_t xmin_wall, ymin_wall, zmin_wall;
	real_t xmax_wall, ymax_wall, zmax_wall;
	xmin_wall = ymin_wall = zmin_wall = 1.0E+10;
	xmax_wall = ymax_wall = zmax_wall = -1.0E+10;
	for (auto&& inode : wall_bdry_list) {
		if (xmin_wall>node_coords_[dimension_*inode]) xmin_wall = node_coords_[dimension_*inode];
		if (xmax_wall<node_coords_[dimension_*inode]) xmax_wall = node_coords_[dimension_*inode];
		if (ymin_wall>node_coords_[dimension_*inode + 1]) ymin_wall = node_coords_[dimension_*inode + 1];
		if (ymax_wall<node_coords_[dimension_*inode + 1]) ymax_wall = node_coords_[dimension_*inode + 1];
		if (dimension_ == 2) continue;
		if (zmin_wall>node_coords_[dimension_*inode + 2]) zmin_wall = node_coords_[dimension_*inode + 2];
		if (zmax_wall<node_coords_[dimension_*inode + 2]) zmax_wall = node_coords_[dimension_*inode + 2];
	}

	const int_t dim2 = dimension_ * 2;
	std::vector<real_t> boundingbox(dim2*ndomain, 0.0);
	boundingbox[dim2*myrank + 0] = xmin;
	boundingbox[dim2*myrank + 1] = xmax;
	boundingbox[dim2*myrank + 2] = ymin;
	boundingbox[dim2*myrank + 3] = ymax;
	if (dimension_ == 3) {
		boundingbox[dim2*myrank + 4] = zmin;
		boundingbox[dim2*myrank + 5] = zmax;
	}

	std::vector<real_t> boundingbox_wall(dim2*ndomain, 0.0);
	boundingbox_wall[dim2*myrank + 0] = xmin_wall;
	boundingbox_wall[dim2*myrank + 1] = xmax_wall;
	boundingbox_wall[dim2*myrank + 2] = ymin_wall;
	boundingbox_wall[dim2*myrank + 3] = ymax_wall;
	if (dimension_ == 3) {
		boundingbox_wall[dim2*myrank + 4] = zmin_wall;
		boundingbox_wall[dim2*myrank + 5] = zmax_wall;
	}

	std::vector<real_t> reduce_sum(dim2*ndomain, 0.0);
	MPI_Allreduce(&boundingbox[0], &reduce_sum[0], dim2*ndomain, MPI_REAL_T, MPI_SUM, MPI_COMM_WORLD);
	boundingbox = reduce_sum;
	MPI_Allreduce(&boundingbox_wall[0], &reduce_sum[0], dim2*ndomain, MPI_REAL_T, MPI_SUM, MPI_COMM_WORLD);
	boundingbox_wall = reduce_sum;
	reduce_sum.clear();

	// filter processes
	std::vector<real_t> min_distance;
	std::vector<real_t> max_distance;
	for (auto&& iprocess : wall_process) {
		std::vector<real_t> distance_candi;

		if (dimension_ == 2) {
			distance_candi.resize(8);

			const real_t& wall_xmin = boundingbox_wall[dim2*iprocess];
			const real_t& wall_xmax = boundingbox_wall[dim2*iprocess + 1];
			const real_t& wall_ymin = boundingbox_wall[dim2*iprocess + 2];
			const real_t& wall_ymax = boundingbox_wall[dim2*iprocess + 3];

			distance_candi[0] = calDist(xmin, ymin, wall_xmax, wall_ymax);
			distance_candi[1] = calDist(xmax, ymin, wall_xmin, wall_ymax);
			distance_candi[2] = calDist(xmin, ymax, wall_xmax, wall_ymin);
			distance_candi[3] = calDist(xmax, ymax, wall_xmin, wall_ymin);

			distance_candi[4] = calDist(xmin, ymin, wall_xmin, wall_ymin);
			distance_candi[5] = calDist(xmax, ymin, wall_xmax, wall_ymin);
			distance_candi[6] = calDist(xmin, ymax, wall_xmin, wall_ymax);
			distance_candi[7] = calDist(xmax, ymax, wall_xmax, wall_ymax);
		}
		else if (dimension_ == 3) {
			distance_candi.resize(16);

			const real_t& wall_xmin = boundingbox_wall[dim2*iprocess];
			const real_t& wall_xmax = boundingbox_wall[dim2*iprocess + 1];
			const real_t& wall_ymin = boundingbox_wall[dim2*iprocess + 2];
			const real_t& wall_ymax = boundingbox_wall[dim2*iprocess + 3];
			const real_t& wall_zmin = boundingbox_wall[dim2*iprocess + 4];
			const real_t& wall_zmax = boundingbox_wall[dim2*iprocess + 5];

			distance_candi[0] = calDist(xmin, ymin, zmin, wall_xmax, wall_ymax, wall_zmax);
			distance_candi[1] = calDist(xmax, ymin, zmin, wall_xmin, wall_ymax, wall_zmax);
			distance_candi[2] = calDist(xmin, ymax, zmin, wall_xmax, wall_ymin, wall_zmax);
			distance_candi[3] = calDist(xmax, ymax, zmin, wall_xmin, wall_ymin, wall_zmax);
			distance_candi[4] = calDist(xmin, ymin, zmax, wall_xmax, wall_ymax, wall_zmin);
			distance_candi[5] = calDist(xmax, ymin, zmax, wall_xmin, wall_ymax, wall_zmin);
			distance_candi[6] = calDist(xmin, ymax, zmax, wall_xmax, wall_ymin, wall_zmin);
			distance_candi[7] = calDist(xmax, ymax, zmax, wall_xmin, wall_ymin, wall_zmin);

			distance_candi[8] = calDist(xmin, ymin, zmin, wall_xmin, wall_ymin, wall_zmin);
			distance_candi[9] = calDist(xmax, ymin, zmin, wall_xmax, wall_ymin, wall_zmin);
			distance_candi[10] = calDist(xmin, ymax, zmin, wall_xmin, wall_ymax, wall_zmin);
			distance_candi[11] = calDist(xmax, ymax, zmin, wall_xmax, wall_ymax, wall_zmin);
			distance_candi[12] = calDist(xmin, ymin, zmax, wall_xmin, wall_ymin, wall_zmax);
			distance_candi[13] = calDist(xmax, ymin, zmax, wall_xmax, wall_ymin, wall_zmax);
			distance_candi[14] = calDist(xmin, ymax, zmax, wall_xmin, wall_ymax, wall_zmax);
			distance_candi[15] = calDist(xmax, ymax, zmax, wall_xmax, wall_ymax, wall_zmax);
		}
		min_distance.push_back(*std::min_element(distance_candi.begin(), distance_candi.end()));
		max_distance.push_back(*std::max_element(distance_candi.begin(), distance_candi.end()));
	}

	std::vector<int_t> wall_process_new;
	const real_t& allowable_distance = *std::min_element(max_distance.begin(), max_distance.end());
	for (int_t iprocess = 0; iprocess<wall_process.size(); iprocess++)
		if (min_distance[iprocess] <= allowable_distance)
			wall_process_new.push_back(wall_process[iprocess]);
	wall_process = move(wall_process_new);

	// communicate wall boundary data (sub-nodes & coordinates etc.)
	std::vector<int_t> wall_recv_proc(ndomain, 0);
	for (auto&& iprocess : wall_process)
		wall_recv_proc[iprocess] = 1;
	std::vector<int_t> wall_send_proc(ndomain, 0);
	MPI_Alltoall(&wall_recv_proc[0], 1, MPI_INT_T, &wall_send_proc[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	std::vector<int_t> send_num_wall(ndomain, 0);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		if (wall_send_proc[idomain] == 1)
			send_num_wall[idomain] = num_wall;
	std::vector<int_t> recv_num_wall(ndomain, 0);
	MPI_Alltoall(&send_num_wall[0], 1, MPI_INT_T, &recv_num_wall[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	std::vector<int_t> wbdry_elem_type;
	std::vector<int_t> wbdry_order;
	std::vector<int_t> num_wbdry_subnodes;
	std::vector<real_t> wbdry_to_subnode_coords;
	for (auto&& ibdry : wall_bdry_list) {
		const int_t& owner_cell = bdry_owner_cell_[ibdry];
		const int_t& owner_type = bdry_owner_type_[ibdry];
		const int_t& bdry_order = cell_order_[owner_cell];

		wbdry_elem_type.push_back(static_cast<int_t>(cell_element_[owner_cell]->getFaceElemType(owner_type)));
		wbdry_order.push_back(bdry_order);

		std::vector<real_t> cell_coords;
		const int_t& start_index = cell_to_subnode_ptr_[owner_cell];
		const int_t& end_index = cell_to_subnode_ptr_[owner_cell + 1];
		cell_coords.reserve(dimension_*(end_index - start_index));
		for (int_t inode = start_index; inode < end_index; inode++)
			for (int_t idim = 0; idim < dimension_; idim++)
				cell_coords.push_back(node_coords_[cell_to_subnode_ind_[inode] * dimension_ + idim]);

		const std::vector<int_t>& face_nodes = cell_element_[owner_cell]->getFacetypeNodes(bdry_order)[owner_type];
		num_wbdry_subnodes.push_back(face_nodes.size());
		for (auto&& inode : face_nodes) 
			for (int_t idim = 0; idim < dimension_; idim++)
				wbdry_to_subnode_coords.push_back(cell_coords[dimension_*inode + idim]);
	}

	std::vector<int_t*> send_data_int(ndomain);
	std::vector<std::vector<int_t>> recv_data_int_sum(ndomain);
	std::vector<int_t*> recv_data_int(ndomain);
	for (int_t idomain = 0; idomain < ndomain; idomain++) {
		recv_data_int_sum[idomain].resize(recv_num_wall[idomain]);
		recv_data_int[idomain] = &recv_data_int_sum[idomain][0];
	}
	
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		if (wall_send_proc[idomain] == 1)
			send_data_int[idomain] = &wbdry_elem_type[0];
	MPIHeader::dataCommunication(send_data_int, send_num_wall, recv_data_int, recv_num_wall);
	int_t total_size = 0;
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		total_size += recv_num_wall[idomain];
	wbdry_elem_type.clear();
	wbdry_elem_type.reserve(total_size);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		if (wall_recv_proc[idomain] == 1)
			for (auto&& data : recv_data_int_sum[idomain])
				wbdry_elem_type.push_back(data);

	for (int_t idomain = 0; idomain < ndomain; idomain++)
		if (wall_send_proc[idomain] == 1)
			send_data_int[idomain] = &wbdry_order[0];
	MPIHeader::dataCommunication(send_data_int, send_num_wall, recv_data_int, recv_num_wall);
	total_size = 0;
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		total_size += recv_num_wall[idomain];
	wbdry_order.clear();
	wbdry_order.reserve(total_size);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		if (wall_recv_proc[idomain] == 1)
			for (auto&& data : recv_data_int_sum[idomain])
				wbdry_order.push_back(data);

	std::vector<int_t> num_send_coords(ndomain, 0);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		if(wall_send_proc[idomain] == 1)
			for (auto&& num_subnodes : num_wbdry_subnodes)
				num_send_coords[idomain] += (dimension_*num_subnodes);
	std::vector<int_t> num_recv_coords(ndomain, 0);
	MPI_Alltoall(&num_send_coords[0], 1, MPI_INT_T, &num_recv_coords[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain < ndomain; idomain++)
		if (wall_send_proc[idomain] == 1)
			send_data_int[idomain] = &num_wbdry_subnodes[0];
	MPIHeader::dataCommunication(send_data_int, send_num_wall, recv_data_int, recv_num_wall);
	total_size = 0;
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		total_size += recv_num_wall[idomain];
	num_wbdry_subnodes.clear();
	num_wbdry_subnodes.reserve(total_size);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		if (wall_recv_proc[idomain] == 1)
			for (auto&& data : recv_data_int_sum[idomain])
				num_wbdry_subnodes.push_back(data);

	send_data_int.clear();
	recv_data_int_sum.clear();
	recv_data_int.clear();

	std::vector<real_t*> send_data_real(ndomain);
	std::vector<std::vector<real_t>> recv_data_real_sum(ndomain);
	std::vector<real_t*> recv_data_real(ndomain);
	for (int_t idomain = 0; idomain < ndomain; idomain++) {
		recv_data_real_sum[idomain].resize(num_recv_coords[idomain]);
		recv_data_real[idomain] = &recv_data_real_sum[idomain][0];
	}

	for (int_t idomain = 0; idomain < ndomain; idomain++)
		if (wall_send_proc[idomain] == 1)
			send_data_real[idomain] = &wbdry_to_subnode_coords[0];
	MPIHeader::dataCommunication(send_data_real, num_send_coords, recv_data_real, num_recv_coords);
	total_size = 0;
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		total_size += num_recv_coords[idomain];
	wbdry_to_subnode_coords.clear();
	wbdry_to_subnode_coords.reserve(total_size);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		if (wall_recv_proc[idomain] == 1)
			for (auto&& data : recv_data_real_sum[idomain])
				wbdry_to_subnode_coords.push_back(data);

	send_data_real.clear();
	recv_data_real_sum.clear();
	recv_data_real.clear();

	num_wall = 0;
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		num_wall += recv_num_wall[idomain];
	std::vector<int_t> wbdry_to_subnode_ptr(1, 0);
	wbdry_to_subnode_ptr.reserve(1 + num_wall);
	for (int_t iwall = 0; iwall < num_wall; iwall++)
		wbdry_to_subnode_ptr.push_back(wbdry_to_subnode_ptr.back() + num_wbdry_subnodes[iwall]);

	// calculate wall distance
	std::vector<std::shared_ptr<Jacobian>> wall_jacobians;
	std::vector<int_t> wnode_to_wall(wbdry_to_subnode_ptr.back());
	wall_jacobians.reserve(num_wall);
	for (int_t iwall = 0; iwall < num_wall; iwall++) {

		const int_t& start_index = wbdry_to_subnode_ptr[iwall];
		const int_t& end_index = wbdry_to_subnode_ptr[iwall + 1];
		std::vector<real_t> wall_coords(dimension_*(end_index - start_index));
		for (int_t index = start_index; index < end_index; index++)
			for (int_t idim = 0; idim < dimension_; idim++)
				wall_coords[dimension_*(index - start_index) + idim] = wbdry_to_subnode_coords[dimension_*index + idim];
		for (int_t index = start_index; index < end_index; index++)
			wnode_to_wall[index] = iwall;

		std::shared_ptr<Jacobian> wall_jacobian = JacobianFactory::getFaceJacobian(static_cast<ElemType>(wbdry_elem_type[iwall]), wbdry_order[iwall]);
		wall_jacobian->setCoords(wall_coords);
		wall_jacobian->setTopology();
		wall_jacobians.push_back(wall_jacobian);
	}
	
	std::vector<std::vector<real_t>> wall_distance(num_cells_);
	std::vector<std::vector<std::vector<int_t>>> nearest_wnodes(num_cells_);

	for (int_t icell = 0; icell < num_cells_; icell++) {
		wall_distance[icell].resize(num_cell_quad_points_[icell]);
		nearest_wnodes[icell].resize(num_cell_quad_points_[icell]);
	}

	for (int_t icell = 0; icell < num_cells_; icell++) {
		const int_t num_points = num_cell_quad_points_[icell];

		for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
			real_t min_dist = 1.0E+10;
			real_t dist;
			for (int iwnode = 0; iwnode < wbdry_to_subnode_ptr.back(); iwnode++) {
				dist = calDist(&cell_quad_points_[icell][dimension_*ipoint], &wbdry_to_subnode_coords[dimension_*iwnode], dimension_);
				if (std::abs(dist - min_dist) < 1.0E-10) {
					min_dist = dist;
					wall_distance[icell][ipoint] = min_dist;
					nearest_wnodes[icell][ipoint].push_back(iwnode);
				}
				else if (dist < min_dist) {
					min_dist = dist;
					wall_distance[icell][ipoint] = min_dist;
					nearest_wnodes[icell][ipoint] = { iwnode };
				}
			}
		}
	}

	for (int_t icell = 0; icell < num_cells_; icell++) {
		const int_t num_points = num_cell_quad_points_[icell];

		for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
			real_t min_dist = wall_distance[icell][ipoint];
			real_t dist;
			for (auto&& iwnode : nearest_wnodes[icell][ipoint]) {
				const int_t& iwall = wnode_to_wall[iwnode];
				dist = calMinimumDist(wall_jacobians[iwall], &cell_quad_points_[icell][dimension_*ipoint], dimension_);
				min_dist = std::min(min_dist, dist);
			}
			//const real_t* x = &cell_quad_points_[icell][dimension_*ipoint];
			//real_t error = std::abs(std::sqrt(x[0] * x[0] + x[1] * x[1]) - 1.0 - std::sqrt(min_dist));
			//MASTER_MESSAGE(TO_STRING(icell)+"/"+TO_STRING(ipoint)+": "+TO_STRING(error));
			wall_distance[icell][ipoint] = min_dist;
		}
	}

	for (int_t icell = 0; icell < num_cells_; icell++) {
		const int_t num_points = num_cell_quad_points_[icell];

		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
			wall_distance[icell][ipoint] = std::sqrt(wall_distance[icell][ipoint]);
	}
	wall_distance_ = move(wall_distance);
	MASTER_MESSAGE("Calculate wall distance(Time: " + TO_STRING(STOP()) + "s)");
}

real_t Grid::calMinimumDist(std::shared_ptr<Jacobian> wbdry_jacobian, const real_t* x, const int_t dimension)
{
	if (dimension == 2) {
		real_t a = 0.2;
		int_t max_iteration = 20;
		std::vector<real_t> coord = { -1.0, 1.0 };
		real_t min_dist;

		for (int_t iter = 0; iter < max_iteration; iter++) {
			if (iter == 8)
				a = 0.5;
			else if (iter == 14)
				a = 0.8;
			const std::vector<real_t> phy = wbdry_jacobian->calComToPhyCoord(coord);
			const real_t dist1 = calDist(x, &phy[0], dimension);
			const real_t dist2 = calDist(x, &phy[dimension], dimension);
			min_dist = std::min(dist1, dist2);
			if (dist1 > dist2)
				coord[0] = (1.0 - a)*coord[0] + a*coord[1];
			else
				coord[1] = (1.0 - a)*coord[1] + a*coord[0];
		}
		return min_dist;
	}
}