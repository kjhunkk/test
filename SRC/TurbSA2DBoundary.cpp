
#include "../INC/TurbSA2DBoundary.h"

const std::vector<real_t> TurbSA2DBCExtrapolate::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFlux(owner_u, normal);
}
const std::vector<real_t> TurbSA2DBCExtrapolate::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFluxJacobian(owner_u, normal);
}
const std::vector<real_t> TurbSA2DBCExtrapolate::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFlux(owner_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCExtrapolate::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCExtrapolate::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxGradJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCExtrapolate::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return owner_u;
}
const std::vector<real_t> TurbSA2DBCExtrapolate::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return std::vector<real_t>(owner_u.size(), 1.0);
}

const std::vector<real_t> TurbSA2DBCConstant::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 5; ipoint++)
		for (int_t istate = 0; istate < 5; istate++)
			references[ipoint * 5 + istate] = references_[istate];
	return flux_->numConvFlux(owner_u, references, normal);
}
const std::vector<real_t> TurbSA2DBCConstant::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 5; ipoint++)
		for (int_t istate = 0; istate < 5; istate++)
			references[ipoint * 5 + istate] = references_[istate];
	return flux_->numConvFluxOwnerJacobian(owner_u, references, normal);
}
const std::vector<real_t> TurbSA2DBCConstant::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 5; ipoint++)
		for (int_t istate = 0; istate < 5; istate++)
			references[ipoint * 5 + istate] = references_[istate];
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFlux(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> TurbSA2DBCConstant::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 5; ipoint++)
		for (int_t istate = 0; istate < 5; istate++)
			references[ipoint * 5 + istate] = references_[istate];
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> TurbSA2DBCConstant::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 5; ipoint++)
		for (int_t istate = 0; istate < 5; istate++)
			references[ipoint * 5 + istate] = references_[istate];
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFluxOwnerGradJacobian(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> TurbSA2DBCConstant::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 5; ipoint++)
		for (int_t istate = 0; istate < 5; istate++)
			references[ipoint * 5 + istate] = references_[istate];
	return references;
}
const std::vector<real_t> TurbSA2DBCConstant::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return std::vector<real_t>(owner_u.size(), 0.0);
}

const std::vector<real_t> TurbSA2DBCWall::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> Flux(num_points * 5, 0.0);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		Flux[ipoint * 5 + 1] = normal[ipoint * 2] * pressure[ipoint];
		Flux[ipoint * 5 + 2] = normal[ipoint * 2 + 1] * pressure[ipoint];
	}
	return Flux;
}
const std::vector<real_t> TurbSA2DBCWall::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25, 0.0);
	return FluxJacobi;
}
const std::vector<real_t> TurbSA2DBCWall::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFlux(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCWall::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCWall::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFluxOwnerGradJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCWall::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u(owner_u.size());
	for (int_t ipoint = 0; ipoint < owner_u.size() / 5; ipoint++) {
		neighbor_u[ipoint * 5] = owner_u[ipoint * 5];
		neighbor_u[ipoint * 5 + 1] = -owner_u[ipoint * 5 + 1];
		neighbor_u[ipoint * 5 + 2] = -owner_u[ipoint * 5 + 2];
		neighbor_u[ipoint * 5 + 3] = owner_u[ipoint * 5 + 3];
		neighbor_u[ipoint * 5 + 4] = owner_u[ipoint * 5 + 4];
	}
	return neighbor_u;
}
const std::vector<real_t> TurbSA2DBCWall::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DBCSlipWall::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> Flux(num_points * 5, 0.0);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		Flux[ipoint * 5 + 1] = normal[ipoint * 2] * pressure[ipoint];
		Flux[ipoint * 5 + 2] = normal[ipoint * 2 + 1] * pressure[ipoint];
	}
	return Flux;
}
const std::vector<real_t> TurbSA2DBCSlipWall::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25, 0.0);
	return FluxJacobi;
}
const std::vector<real_t> TurbSA2DBCSlipWall::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> Flux(num_points * 5, 0.0);
	return Flux;
}
const std::vector<real_t> TurbSA2DBCSlipWall::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25, 0.0);
	return FluxJacobi;
}
const std::vector<real_t> TurbSA2DBCSlipWall::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25, 0.0);
	return FluxJacobi;
}
const std::vector<real_t> TurbSA2DBCSlipWall::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u(owner_u.size());
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < owner_u.size() / 5; ipoint++) {
		neighbor_u[ipoint * 5] = owner_u[ipoint * 5];
		neighbor_u[ipoint * 5 + 1] = owner_u[ipoint * 5 + 1];
		neighbor_u[ipoint * 5 + 2] = owner_u[ipoint * 5 + 2];
		neighbor_u[ipoint * 5 + 3] = owner_u[ipoint * 5 + 3];
		neighbor_u[ipoint * 5 + 4] = owner_u[ipoint * 5 + 4];
	}
	return neighbor_u;
}
const std::vector<real_t> TurbSA2DBCSlipWall::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DBCSubOut::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	return flux_->numConvFlux(owner_u, neighbor_u, normal);
}
const std::vector<real_t> TurbSA2DBCSubOut::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	return flux_->numConvFluxOwnerJacobian(owner_u, neighbor_u, normal);
}
const std::vector<real_t> TurbSA2DBCSubOut::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFlux(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSubOut::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSubOut::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSubOut::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u(owner_u.size());
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < owner_u.size() / 5; ipoint++) {
		neighbor_u[ipoint * 5 + 0] = owner_u[ipoint * 5 + 0];
		neighbor_u[ipoint * 5 + 1] = owner_u[ipoint * 5 + 1];
		neighbor_u[ipoint * 5 + 2] = owner_u[ipoint * 5 + 2];
		neighbor_u[ipoint * 5 + 3] = owner_u[ipoint * 5 + 3] + 2.5*(1.0 / 1.4 - pressure[ipoint]);
		neighbor_u[ipoint * 5 + 4] = owner_u[ipoint * 5 + 4];
	}
	return neighbor_u;
}
const std::vector<real_t> TurbSA2DBCSubOut::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DBCSubIn::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	return flux_->numConvFlux(owner_u, neighbor_u, normal);
}
const std::vector<real_t> TurbSA2DBCSubIn::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	return flux_->numConvFluxOwnerJacobian(owner_u, neighbor_u, normal);
}
const std::vector<real_t> TurbSA2DBCSubIn::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFlux(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSubIn::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSubIn::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFluxOwnerGradJacobian(owner_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSubIn::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u(owner_u.size());
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	for (int_t ipoint = 0; ipoint < owner_u.size() / 5; ipoint++) {
		neighbor_u[ipoint * 5 + 0] = 1.0;
		neighbor_u[ipoint * 5 + 1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
		neighbor_u[ipoint * 5 + 2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
		neighbor_u[ipoint * 5 + 3] = 2.5*pressure[ipoint] + 0.5*(neighbor_u[ipoint * 5 + 1] * neighbor_u[ipoint * 5 + 1] + neighbor_u[ipoint * 5 + 2] * neighbor_u[ipoint * 5 + 2]);
		neighbor_u[ipoint * 5 + 4] = free_dnut_;
	}
	return neighbor_u;
}
const std::vector<real_t> TurbSA2DBCSubIn::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DBCSupOut::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFlux(owner_u, normal);
}
const std::vector<real_t> TurbSA2DBCSupOut::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFluxJacobian(owner_u, normal);
}
const std::vector<real_t> TurbSA2DBCSupOut::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFlux(owner_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSupOut::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSupOut::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSupOut::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return owner_u;
}
const std::vector<real_t> TurbSA2DBCSupOut::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DBCSupIn::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	return flux_->commonConvFlux(neighbor_u, normal);
}
const std::vector<real_t> TurbSA2DBCSupIn::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_u, normal);
	return flux_->commonConvFluxJacobian(neighbor_u, normal);
}
const std::vector<real_t> TurbSA2DBCSupIn::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFlux(neighbor_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSupIn::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFluxOwnerJacobian(neighbor_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSupIn::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> neighbor_u = calBoundarySolution(owner_u, owner_div_u, normal);
	return flux_->numViscFluxOwnerJacobian(neighbor_u, owner_div_u, neighbor_u, owner_div_u, normal);
}
const std::vector<real_t> TurbSA2DBCSupIn::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> neighbor_u(owner_u.size());
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		neighbor_u[ipoint * 5 + 0] = 1.0;
		neighbor_u[ipoint * 5 + 1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
		neighbor_u[ipoint * 5 + 2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
		neighbor_u[ipoint * 5 + 3] = 2.5 / 1.4 + 0.5*(neighbor_u[ipoint * 5 + 1] * neighbor_u[ipoint * 5 + 1] + neighbor_u[ipoint * 5 + 2] * neighbor_u[ipoint * 5 + 2]);
		neighbor_u[ipoint * 5 + 4] = free_dnut_;
	}
	return neighbor_u;
}
const std::vector<real_t> TurbSA2DBCSupIn::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DBCFarField::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> Flux(num_points * 5);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	/*for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
	std::vector<real_t> neighbor_u(4);
	std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 4, owner_u.begin() + (ipoint + 1) * 4);
	std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
	std::vector<real_t> local_flux;

	const real_t a = std::sqrt(1.4*pressure[ipoint] / local_owner_u[0]);
	const real_t V = (local_owner_u[1] * local_normal[0] + local_owner_u[2] * local_normal[1]) / local_owner_u[0];
	const real_t Ma = V / a;

	if (Ma <= -1.0) {
	neighbor_u[0] = 1.0;
	neighbor_u[1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[3] = 2.5 / 1.4 + 0.5*(neighbor_u[1] * neighbor_u[1] + neighbor_u[2] * neighbor_u[2]);
	local_flux = flux_->commonConvFlux(neighbor_u, local_normal);
	}
	else if (Ma <= 0.0) {
	neighbor_u[0] = 1.0;
	neighbor_u[1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[3] = 2.5*pressure[ipoint] + 0.5*(neighbor_u[1] * neighbor_u[1] + neighbor_u[2] * neighbor_u[2]);
	local_flux = flux_->numConvFlux(local_owner_u, neighbor_u, local_normal);
	}
	else if (Ma <= 1.0) {
	neighbor_u[0] = local_owner_u[0];
	neighbor_u[1] = local_owner_u[1];
	neighbor_u[2] = local_owner_u[2];
	neighbor_u[3] = local_owner_u[3] + 2.5*(1.0 / 1.4 - pressure[ipoint]);
	local_flux = flux_->numConvFlux(local_owner_u, neighbor_u, local_normal);
	}
	else {
	local_flux = flux_->commonConvFlux(local_owner_u, local_normal);
	}

	Flux[ipoint * 4 + 0] = local_flux[0];
	Flux[ipoint * 4 + 1] = local_flux[1];
	Flux[ipoint * 4 + 2] = local_flux[2];
	Flux[ipoint * 4 + 3] = local_flux[3];
	}*/
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		std::vector<real_t> neighbor_u(5);
		std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 5, owner_u.begin() + (ipoint + 1) * 5);
		std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
		std::vector<real_t> local_flux;

		const real_t aL = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 5]);
		const real_t VL = (owner_u[ipoint * 5 + 1] * normal[ipoint * 2] + owner_u[ipoint * 5 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 5];
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2] + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2 + 1]);
		real_t RL, Rf;
		if (std::abs(Vf) >= 1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
			}
			else {
				RL = VL + 5.0*aL;
			}
			if (VL < 0.0) {
				Rf = VL - 5.0*aL;
			}
			else {
				Rf = Vf - 5.0;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
		}
		real_t rho, U, V;
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*(RL - Rf)*0.01, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2] * Vf;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2 + 1] * Vf;
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 5], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + owner_u[ipoint * 5 + 1] / owner_u[ipoint * 5] - normal[ipoint * 2] * VL;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + owner_u[ipoint * 5 + 2] / owner_u[ipoint * 5] - normal[ipoint * 2 + 1] * VL;
		}
		real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		neighbor_u[0] = rho;
		neighbor_u[1] = rho*U;
		neighbor_u[2] = rho*V;
		neighbor_u[3] = 2.5*p + 0.5*rho*(U*U + V*V);
		neighbor_u[4] = free_dnut_;

		local_flux = flux_->numConvFlux(local_owner_u, neighbor_u, local_normal);

		Flux[ipoint * 5 + 0] = local_flux[0];
		Flux[ipoint * 5 + 1] = local_flux[1];
		Flux[ipoint * 5 + 2] = local_flux[2];
		Flux[ipoint * 5 + 3] = local_flux[3];
		Flux[ipoint * 5 + 4] = local_flux[4];
	}
	return Flux;
}
const std::vector<real_t> TurbSA2DBCFarField::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	/*for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
	std::vector<real_t> neighbor_u(4);
	std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 4, owner_u.begin() + (ipoint + 1) * 4);
	std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
	std::vector<real_t> local_flux;

	const real_t a = std::sqrt(1.4*pressure[ipoint] / local_owner_u[0]);
	const real_t V = (local_owner_u[1] * local_normal[0] + local_owner_u[2] * local_normal[1]) / local_owner_u[0];
	const real_t Ma = V / a;

	if (Ma <= -1.0) {
	neighbor_u[0] = 1.0;
	neighbor_u[1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[3] = 2.5 / 1.4 + 0.5*(neighbor_u[1] * neighbor_u[1] + neighbor_u[2] * neighbor_u[2]);
	local_flux = flux_->commonConvFlux(neighbor_u, local_normal);
	}
	else if (Ma <= 0.0) {
	neighbor_u[0] = 1.0;
	neighbor_u[1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[3] = 2.5*pressure[ipoint] + 0.5*(neighbor_u[1] * neighbor_u[1] + neighbor_u[2] * neighbor_u[2]);
	local_flux = flux_->numConvFlux(local_owner_u, neighbor_u, local_normal);
	}
	else if (Ma <= 1.0) {
	neighbor_u[0] = local_owner_u[0];
	neighbor_u[1] = local_owner_u[1];
	neighbor_u[2] = local_owner_u[2];
	neighbor_u[3] = local_owner_u[3] + 2.5*(1.0 / 1.4 - pressure[ipoint]);
	local_flux = flux_->numConvFlux(local_owner_u, neighbor_u, local_normal);
	}
	else {
	local_flux = flux_->commonConvFlux(local_owner_u, local_normal);
	}

	Flux[ipoint * 4 + 0] = local_flux[0];
	Flux[ipoint * 4 + 1] = local_flux[1];
	Flux[ipoint * 4 + 2] = local_flux[2];
	Flux[ipoint * 4 + 3] = local_flux[3];
	}*/
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		std::vector<real_t> neighbor_u(5);
		std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 5, owner_u.begin() + (ipoint + 1) * 5);
		std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
		std::vector<real_t> local_flux_jacobi;

		const real_t aL = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 5]);
		const real_t VL = (owner_u[ipoint * 5 + 1] * normal[ipoint * 2] + owner_u[ipoint * 5 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 5];
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2] + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2 + 1]);
		real_t RL, Rf;
		if (std::abs(Vf) >= 1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
			}
			else {
				RL = VL + 5.0*aL;
			}
			if (VL < 0.0) {
				Rf = VL - 5.0*aL;
			}
			else {
				Rf = Vf - 5.0;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
		}
		real_t rho, U, V;
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*(RL - Rf)*0.01, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2] * Vf;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2 + 1] * Vf;
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 5], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + owner_u[ipoint * 5 + 1] / owner_u[ipoint * 5] - normal[ipoint * 2] * VL;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + owner_u[ipoint * 5 + 2] / owner_u[ipoint * 5] - normal[ipoint * 2 + 1] * VL;
		}
		real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		neighbor_u[0] = rho;
		neighbor_u[1] = rho * U;
		neighbor_u[2] = rho * V;
		neighbor_u[3] = 2.5*p + 0.5*rho*(U*U + V * V);
		neighbor_u[4] = free_dnut_;

		local_flux_jacobi = flux_->numConvFluxOwnerJacobian(local_owner_u, neighbor_u, local_normal);

		for (int_t ivar = 0; ivar < 25; ivar++)
			FluxJacobi[ipoint * 5 + ivar] = local_flux_jacobi[ivar];
	}
	return FluxJacobi;
}
const std::vector<real_t> TurbSA2DBCFarField::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> ViscFlux(num_points * 5);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	/*for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
	std::vector<real_t> neighbor_u(4);
	std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 4, owner_u.begin() + (ipoint + 1) * 4);
	std::vector<real_t> local_owner_div_u(owner_div_u.begin() + ipoint * 8, owner_div_u.begin() + (ipoint + 1) * 8);
	std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
	std::vector<real_t> local_viscflux;

	const real_t a = std::sqrt(1.4*pressure[ipoint] / local_owner_u[0]);
	const real_t V = (local_owner_u[1] * local_normal[0] + local_owner_u[2] * local_normal[1]) / local_owner_u[0];
	const real_t Ma = V / a;

	if (Ma <= -1.0) {
	neighbor_u[0] = 1.0;
	neighbor_u[1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[3] = 2.5 / 1.4 + 0.5*(neighbor_u[1] * neighbor_u[1] + neighbor_u[2] * neighbor_u[2]);
	local_viscflux = flux_->numViscFlux(neighbor_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);
	}
	else if (Ma <= 0.0) {
	neighbor_u[0] = 1.0;
	neighbor_u[1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[3] = 2.5*pressure[ipoint] + 0.5*(neighbor_u[1] * neighbor_u[1] + neighbor_u[2] * neighbor_u[2]);
	local_viscflux = flux_->numViscFlux(local_owner_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);
	}
	else if (Ma <= 1.0) {
	neighbor_u[0] = local_owner_u[0];
	neighbor_u[1] = local_owner_u[1];
	neighbor_u[2] = local_owner_u[2];
	neighbor_u[3] = local_owner_u[3] + 2.5*(1.0 / 1.4 - pressure[ipoint]);
	local_viscflux = flux_->numViscFlux(local_owner_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);
	}
	else {
	local_viscflux = flux_->numViscFlux(local_owner_u, local_owner_div_u, local_owner_u, local_owner_div_u, local_normal);
	}

	ViscFlux[ipoint * 4 + 0] = local_viscflux[0];
	ViscFlux[ipoint * 4 + 1] = local_viscflux[1];
	ViscFlux[ipoint * 4 + 2] = local_viscflux[2];
	ViscFlux[ipoint * 4 + 3] = local_viscflux[3];
	}*/
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		std::vector<real_t> neighbor_u(5);
		std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 5, owner_u.begin() + (ipoint + 1) * 5);
		std::vector<real_t> local_owner_div_u(owner_div_u.begin() + ipoint * 10, owner_div_u.begin() + (ipoint + 1) * 10);
		std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
		std::vector<real_t> local_viscflux;

		const real_t aL = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 5]);
		const real_t VL = (owner_u[ipoint * 5 + 1] * normal[ipoint * 2] + owner_u[ipoint * 5 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 5];
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2] + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2 + 1]);
		real_t RL, Rf;
		if (std::abs(Vf) >= 1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
			}
			else {
				RL = VL + 5.0*aL;
			}
			if (VL < 0.0) {
				Rf = VL - 5.0*aL;
			}
			else {
				Rf = Vf - 5.0;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
		}
		real_t rho, U, V;
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*(RL - Rf)*0.01, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2] * Vf;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2 + 1] * Vf;
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 5], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + owner_u[ipoint * 5 + 1] / owner_u[ipoint * 5] - normal[ipoint * 2] * VL;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + owner_u[ipoint * 5 + 2] / owner_u[ipoint * 5] - normal[ipoint * 2 + 1] * VL;
		}
		real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		neighbor_u[0] = rho;
		neighbor_u[1] = rho*U;
		neighbor_u[2] = rho*V;
		neighbor_u[3] = 2.5*p + 0.5*rho*(U*U + V*V);
		neighbor_u[4] = free_dnut_;

		local_viscflux = flux_->numViscFlux(local_owner_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);

		ViscFlux[ipoint * 5 + 0] = local_viscflux[0];
		ViscFlux[ipoint * 5 + 1] = local_viscflux[1];
		ViscFlux[ipoint * 5 + 2] = local_viscflux[2];
		ViscFlux[ipoint * 5 + 3] = local_viscflux[3];
		ViscFlux[ipoint * 5 + 4] = local_viscflux[4];
	}
	return ViscFlux;
}
const std::vector<real_t> TurbSA2DBCFarField::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> ViscFluxJacobi(num_points * 25);
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	/*for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
	std::vector<real_t> neighbor_u(4);
	std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 4, owner_u.begin() + (ipoint + 1) * 4);
	std::vector<real_t> local_owner_div_u(owner_div_u.begin() + ipoint * 8, owner_div_u.begin() + (ipoint + 1) * 8);
	std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
	std::vector<real_t> local_viscflux;

	const real_t a = std::sqrt(1.4*pressure[ipoint] / local_owner_u[0]);
	const real_t V = (local_owner_u[1] * local_normal[0] + local_owner_u[2] * local_normal[1]) / local_owner_u[0];
	const real_t Ma = V / a;

	if (Ma <= -1.0) {
	neighbor_u[0] = 1.0;
	neighbor_u[1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[3] = 2.5 / 1.4 + 0.5*(neighbor_u[1] * neighbor_u[1] + neighbor_u[2] * neighbor_u[2]);
	local_viscflux = flux_->numViscFlux(neighbor_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);
	}
	else if (Ma <= 0.0) {
	neighbor_u[0] = 1.0;
	neighbor_u[1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[3] = 2.5*pressure[ipoint] + 0.5*(neighbor_u[1] * neighbor_u[1] + neighbor_u[2] * neighbor_u[2]);
	local_viscflux = flux_->numViscFlux(local_owner_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);
	}
	else if (Ma <= 1.0) {
	neighbor_u[0] = local_owner_u[0];
	neighbor_u[1] = local_owner_u[1];
	neighbor_u[2] = local_owner_u[2];
	neighbor_u[3] = local_owner_u[3] + 2.5*(1.0 / 1.4 - pressure[ipoint]);
	local_viscflux = flux_->numViscFlux(local_owner_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);
	}
	else {
	local_viscflux = flux_->numViscFlux(local_owner_u, local_owner_div_u, local_owner_u, local_owner_div_u, local_normal);
	}

	ViscFlux[ipoint * 4 + 0] = local_viscflux[0];
	ViscFlux[ipoint * 4 + 1] = local_viscflux[1];
	ViscFlux[ipoint * 4 + 2] = local_viscflux[2];
	ViscFlux[ipoint * 4 + 3] = local_viscflux[3];
	}*/
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		std::vector<real_t> neighbor_u(5);
		std::vector<real_t> local_owner_u(owner_u.begin() + ipoint * 5, owner_u.begin() + (ipoint + 1) * 5);
		std::vector<real_t> local_owner_div_u(owner_div_u.begin() + ipoint * 10, owner_div_u.begin() + (ipoint + 1) * 10);
		std::vector<real_t> local_normal(normal.begin() + ipoint * 2, normal.begin() + (ipoint + 1) * 2);
		std::vector<real_t> local_viscflux_jacobi;

		const real_t aL = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 5]);
		const real_t VL = (owner_u[ipoint * 5 + 1] * normal[ipoint * 2] + owner_u[ipoint * 5 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 5];
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2] + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2 + 1]);
		real_t RL, Rf;
		if (std::abs(Vf) >= 1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
			}
			else {
				RL = VL + 5.0*aL;
			}
			if (VL < 0.0) {
				Rf = VL - 5.0*aL;
			}
			else {
				Rf = Vf - 5.0;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
		}
		real_t rho, U, V;
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*(RL - Rf)*0.01, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2] * Vf;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2 + 1] * Vf;
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 5], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + owner_u[ipoint * 5 + 1] / owner_u[ipoint * 5] - normal[ipoint * 2] * VL;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + owner_u[ipoint * 5 + 2] / owner_u[ipoint * 5] - normal[ipoint * 2 + 1] * VL;
		}
		real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		neighbor_u[0] = rho;
		neighbor_u[1] = rho * U;
		neighbor_u[2] = rho * V;
		neighbor_u[3] = 2.5*p + 0.5*rho*(U*U + V * V);
		neighbor_u[4] = free_dnut_;

		local_viscflux_jacobi = flux_->numViscFluxOwnerJacobian(local_owner_u, local_owner_div_u, neighbor_u, local_owner_div_u, local_normal);

		for (int_t ivar = 0; ivar < 25; ivar++)
			ViscFluxJacobi[ipoint * 5 + ivar] = local_viscflux_jacobi[ivar];
	}
	return ViscFluxJacobi;
}
const std::vector<real_t> TurbSA2DBCFarField::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> ViscFluxJacobi(num_points * 25);
	
	return ViscFluxJacobi;
}
const std::vector<real_t> TurbSA2DBCFarField::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> neighbor_u(owner_u.size());
	const std::vector<real_t> pressure = flux_->calPressure(owner_u);
	/*for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
	const real_t a = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 4]);
	const real_t V = (owner_u[ipoint * 4 + 1] * normal[ipoint * 2] + owner_u[ipoint * 4 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 4];
	const real_t Ma = V / a;
	if (Ma <= -1.0) {
	neighbor_u[ipoint * 4 + 0] = 1.0;
	neighbor_u[ipoint * 4 + 1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[ipoint * 4 + 2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[ipoint * 4 + 3] = 2.5 / 1.4 + 0.5*(neighbor_u[ipoint * 4 + 1] * neighbor_u[ipoint * 4 + 1] + neighbor_u[ipoint * 4 + 2] * neighbor_u[ipoint * 4 + 2]);
	}
	else if (Ma <= 0.0) {
	neighbor_u[ipoint * 4 + 0] = 1.0;
	neighbor_u[ipoint * 4 + 1] = references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[ipoint * 4 + 2] = references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0);
	neighbor_u[ipoint * 4 + 3] = 2.5*pressure[ipoint] + 0.5*(neighbor_u[ipoint * 4 + 1] * neighbor_u[ipoint * 4 + 1] + neighbor_u[ipoint * 4 + 2] * neighbor_u[ipoint * 4 + 2]);
	}
	else if (Ma <= 1.0) {
	neighbor_u[ipoint * 4 + 0] = owner_u[ipoint * 4 + 0];
	neighbor_u[ipoint * 4 + 1] = owner_u[ipoint * 4 + 1];
	neighbor_u[ipoint * 4 + 2] = owner_u[ipoint * 4 + 2];
	neighbor_u[ipoint * 4 + 3] = owner_u[ipoint * 4 + 3] + 2.5*(1.0 / 1.4 - pressure[ipoint]);
	}
	else {
	neighbor_u[ipoint * 4 + 0] = owner_u[ipoint * 4 + 0];
	neighbor_u[ipoint * 4 + 1] = owner_u[ipoint * 4 + 1];
	neighbor_u[ipoint * 4 + 2] = owner_u[ipoint * 4 + 2];
	neighbor_u[ipoint * 4 + 3] = owner_u[ipoint * 4 + 3];
	}
	}*/
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t aL = std::sqrt(1.4*pressure[ipoint] / owner_u[ipoint * 5]);
		const real_t VL = (owner_u[ipoint * 5 + 1] * normal[ipoint * 2] + owner_u[ipoint * 5 + 2] * normal[ipoint * 2 + 1]) / owner_u[ipoint * 5];
		const real_t Vf = references_[0] * (std::cos(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2] + std::sin(references_[1] * 3.14159265358979323846264338 / 180.0)*normal[ipoint * 2 + 1]);
		real_t RL, Rf;
		if (std::abs(Vf) >= 1.0) {
			if (VL >= 0.0) {
				RL = Vf + 5.0;
			}
			else {
				RL = VL + 5.0*aL;
			}
			if (VL < 0.0) {
				Rf = VL - 5.0*aL;
			}
			else {
				Rf = Vf - 5.0;
			}
		}
		else {
			RL = VL + 5.0*aL;
			Rf = Vf - 5.0;
		}
		real_t rho, U, V;
		if (VL < 0.0) {
			rho = std::pow((RL - Rf)*(RL - Rf)*0.01, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + references_[0] * std::cos(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2] * Vf;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + references_[0] * std::sin(references_[1] * 3.14159265358979323846264338 / 180.0) - normal[ipoint * 2 + 1] * Vf;
		}
		else {
			rho = std::pow((RL - Rf)*(RL - Rf)*std::pow(owner_u[ipoint * 5], 1.4) / pressure[ipoint] / 140.0, 2.5);
			U = 0.5*normal[ipoint * 2] * (RL + Rf) + owner_u[ipoint * 5 + 1] / owner_u[ipoint * 5] - normal[ipoint * 2] * VL;
			V = 0.5*normal[ipoint * 2 + 1] * (RL + Rf) + owner_u[ipoint * 5 + 2] / owner_u[ipoint * 5] - normal[ipoint * 2 + 1] * VL;
		}
		real_t p = (RL - Rf)*(RL - Rf)*rho / 140.0;
		neighbor_u[ipoint * 5 + 0] = rho;
		neighbor_u[ipoint * 5 + 1] = rho*U;
		neighbor_u[ipoint * 5 + 2] = rho*V;
		neighbor_u[ipoint * 5 + 3] = 2.5*p + 0.5*rho*(U*U + V*V);
		neighbor_u[ipoint * 5 + 4] = free_dnut_;
	}
	return neighbor_u;
}
const std::vector<real_t> TurbSA2DBCFarField::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / 5;
	std::vector<real_t> FluxJacobi(num_points * 25, 0.0);
	return FluxJacobi;
}