#include "../INC/GridBuilder.h"

void GridBuilder::MakeGrid(const std::string& file_format, const std::string& file_name, const int_t dimension)
{
	dimension_ = dimension;

	readGridFileParallel(file_format, file_name);

	setCellColor();

	redistributeCellData();

	periodicMatching();
	
	constructVirtualCellData();

	redistributeBdryData();

	redistributeNodeData();

	constructFaceData();

	constructNodeToCellsData();

	petscReordering(); // for implicit
	
	//test();

	/*int_t myrank = MYRANK();
	if (myrank == MASTER_NODE){
		MEMBER_MESSAGE("num_global_nodes_:" + TO_STRING(num_global_nodes_));
		MEMBER_MESSAGE("num_total_nodes_:" + TO_STRING(num_total_nodes_));
		MEMBER_MESSAGE("num_nodes_:" + TO_STRING(num_nodes_));
		MEMBER_LISTPRINT(myrank, "(node_coords)", node_coords_);
		MEMBER_LISTPRINT(myrank, "(peribdry_node_x)", peribdry_node_[0]);
		MEMBER_LISTPRINT(myrank, "(peribdry_node_to_matching_x)", peribdry_node_to_matching_[0]);
		MEMBER_LISTPRINT(myrank, "(node_global_index)", node_global_index_);
		MEMBER_MESSAGE("");
		MEMBER_MESSAGE("");
		MEMBER_MESSAGE("");
		MEMBER_MESSAGE("num_global_cells_:" + TO_STRING(num_global_cells_));
		MEMBER_MESSAGE("num_total_cells_:" + TO_STRING(num_total_cells_));
		MEMBER_MESSAGE("num_cells_:" + TO_STRING(num_cells_));
		MEMBER_LISTPRINT(myrank, "(cell_to_node_ptr)", cell_to_node_ptr_);
		MEMBER_LISTPRINT(myrank, "(cell_to_node_ind)", cell_to_node_ind_);
		MEMBER_LISTPRINT(myrank, "(cell_elem_type)", cell_elem_type_);
		MEMBER_LISTPRINT(myrank, "(cell_order)", cell_order_);
		MEMBER_LISTPRINT(myrank, "(cell_to_subnode_ptr)", cell_to_subnode_ptr_);
		MEMBER_LISTPRINT(myrank, "(cell_to_subnode_ind)", cell_to_subnode_ind_);
		MEMBER_LISTPRINT(myrank, "(cell_color)", cell_color_);
		MEMBER_LISTPRINT(myrank, "(cell_global_index)", cell_global_index_);
		MEMBER_LISTPRINT(myrank, "(send_cell_index)", send_cell_list_[1]);
		MEMBER_LISTPRINT(myrank, "(recv_cell_index)", recv_cell_list_[1]);
		MEMBER_MESSAGE("");
		MEMBER_MESSAGE("");
		MEMBER_MESSAGE("");
		MEMBER_MESSAGE("num_global_bdries_:" + TO_STRING(num_global_bdries_));
		MEMBER_MESSAGE("num_total_bdries_:" + TO_STRING(num_total_bdries_));
		MEMBER_MESSAGE("num_bdries_:" + TO_STRING(num_bdries_));
		MEMBER_LISTPRINT(myrank, "(bdry_to_node_ptr)", bdry_to_node_ptr_);
		MEMBER_LISTPRINT(myrank, "(bdry_to_node_ind)", bdry_to_node_ind_);
		MEMBER_LISTPRINT(myrank, "(bdry_type)", bdry_type_);
		MEMBER_LISTPRINT(myrank, "(bdry_color)", bdry_color_);
		MEMBER_LISTPRINT(myrank, "(bdry_owner_cell)", bdry_owner_cell_);
		MEMBER_MESSAGE("");
		MEMBER_MESSAGE("");
		MEMBER_MESSAGE("");
		MEMBER_MESSAGE("num_global_peribdries_:" + TO_STRING(num_global_peribdries_));
		MEMBER_MESSAGE("num_total_peribdries_:" + TO_STRING(num_total_peribdries_));
		MEMBER_MESSAGE("num_peribdries_:" + TO_STRING(num_peribdries_));
		MEMBER_LISTPRINT(myrank, "(peribdry_to_node_ptr)", peribdry_to_node_ptr_);
		MEMBER_LISTPRINT(myrank, "(peribdry_to_node_ind)", peribdry_to_node_ind_);
		MEMBER_LISTPRINT(myrank, "(peribdry_direction)", peribdry_direction_);
		MEMBER_LISTPRINT(myrank, "(peribdry_color)", peribdry_color_);
		MEMBER_LISTPRINT(myrank, "(peribdry_owner_cell)", peribdry_owner_cell_);
		MEMBER_LISTPRINT(myrank, "(peribdry_neighbor_cell)", peribdry_neighbor_cell_);
		MEMBER_LISTPRINT(myrank, "(peribdry_to_matching_node_ind)", peribdry_to_matching_node_ind_);
		MEMBER_MESSAGE("");
		MEMBER_MESSAGE("");
		MEMBER_MESSAGE("");
	}*/
}

void GridBuilder::readGridFileParallel(const std::string& file_format, const std::string& file_name)
{
	START();
	GridReader* grid_reader = GridReader::getInstance().getType(file_format);
	grid_reader->open(file_name, dimension_);

	grid_reader->readGridInfo(num_global_nodes_, num_global_cells_, num_global_bdries_, num_global_peribdries_);

	int_t ndomain = NDOMAIN();
	int_t myrank = MYRANK();

	num_nodes_ = int_t(std::ceil(real_t(num_global_nodes_) / real_t(ndomain)));
	std::vector<int_t> node_dist(ndomain + 1, 0);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		node_dist[idomain + 1] = node_dist[idomain] + num_nodes_;
	for (int_t idomain = ndomain; idomain > 0; idomain--) {
		if (node_dist[idomain] > num_global_nodes_)
			node_dist[idomain] = num_global_nodes_;
		else
			break;
	}
	num_nodes_ = node_dist[myrank + 1] - node_dist[myrank];
	node_dist_ = move(node_dist);

	// read node data in parallel
	grid_reader->readNodeData(node_dist_[myrank], node_dist_[myrank + 1], node_coords_);

	num_cells_ = int_t(std::ceil(real_t(num_global_cells_) / real_t(ndomain)));
	std::vector<int_t> cell_dist(ndomain + 1, 0);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		cell_dist[idomain + 1] = cell_dist[idomain] + num_cells_;
	for (int_t idomain = ndomain; idomain > 0; idomain--) {
		if (cell_dist[idomain] > num_global_cells_)
			cell_dist[idomain] = num_global_cells_;
		else
			break;
	}
	num_cells_ = cell_dist[myrank + 1] - cell_dist[myrank];
	cell_dist_ = move(cell_dist);
	
	// read cell data in parallel
	grid_reader->readCellData(cell_dist_[myrank], cell_dist_[myrank + 1], cell_to_node_ind_, cell_to_node_ptr_, cell_elem_type_, cell_order_, cell_to_subnode_ptr_, cell_to_subnode_ind_);

	num_bdries_ = int_t(std::ceil(real_t(num_global_bdries_) / real_t(ndomain)));
	std::vector<int_t> bdry_dist(ndomain + 1, 0);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		bdry_dist[idomain + 1] = bdry_dist[idomain] + num_bdries_;
	for (int_t idomain = ndomain; idomain > 0; idomain--) {
		if (bdry_dist[idomain] > num_global_bdries_)
			bdry_dist[idomain] = num_global_bdries_;
		else
			break;
	}
	num_bdries_ = bdry_dist[myrank + 1] - bdry_dist[myrank];
	bdry_dist_ = move(bdry_dist);

	// read boundary data in parallel
	grid_reader->readBdryData(bdry_dist_[myrank], bdry_dist_[myrank + 1], bdry_to_node_ptr_, bdry_to_node_ind_, bdry_type_);
	
	num_peribdries_ = int_t(std::ceil(real_t(num_global_peribdries_) / real_t(ndomain)));
	std::vector<int_t> peribdry_dist(ndomain + 1, 0);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		peribdry_dist[idomain + 1] = peribdry_dist[idomain] + num_peribdries_;
	for (int_t idomain = ndomain; idomain > 0; idomain--) {
		if (peribdry_dist[idomain] > num_global_peribdries_)
			peribdry_dist[idomain] = num_global_peribdries_;
		else
			break;
	}
	num_peribdries_ = peribdry_dist[myrank + 1] - peribdry_dist[myrank];
	peribdry_dist_ = move(peribdry_dist);

	// read periodic boundary data in parallel
	grid_reader->readPeriBdryData(peribdry_dist_[myrank], peribdry_dist_[myrank + 1], peribdry_to_node_ptr_, peribdry_to_node_ind_, peribdry_direction_);

	grid_reader->close();

	MASTER_MESSAGE("Read grid file(Time: " + TO_STRING(STOP())+"s)");
}

void GridBuilder::setCellColor(void)
{
	START();
	int_t ndomain = NDOMAIN();
	int_t myrank = MYRANK();
	
	idx_t wgtflag = 0;
	idx_t numflag = 0;
	idx_t ncon = 1;
	idx_t ncommonnodes = dimension_;
	idx_t npart = ndomain;
	std::vector<real_t> tpwgts;
	tpwgts.resize(npart);
	for (idx_t ipart = 0; ipart < npart; ++ipart)
		tpwgts[ipart] = 1.0 / real_t(npart);
	real_t ubvec = 1.05;
	
	std::vector<idx_t> options;
	options.resize(METIS_NOPTIONS);
	METIS_SetDefaultOptions(&options[0]);
	options[0] = 0;
	
	idx_t edgecut;
	std::vector<idx_t> part(num_cells_);
	std::vector<idx_t> eptr;
	std::vector<idx_t> eind;
	std::vector<idx_t> elmdist;

	eptr.reserve(cell_to_node_ptr_.size());
	for (auto&& iterator : cell_to_node_ptr_)
		eptr.push_back(iterator);

	eind.reserve(cell_to_node_ind_.size());
	for (auto&& iterator : cell_to_node_ind_)
		eind.push_back(iterator);

	elmdist.reserve(cell_dist_.size());
	for (auto&& iterator : cell_dist_)
		elmdist.push_back(iterator);
	
	MPI_Comm comm = MPI_COMM_WORLD;

	ParMETIS_V3_PartMeshKway(&elmdist[0], &eptr[0], &eind[0], NULL, &wgtflag, &numflag, &ncon, &ncommonnodes, &npart, &tpwgts[0], &ubvec, &options[0], &edgecut, &part[0], &comm);

	std::vector<int_t> cell_color;
	cell_color.reserve(num_cells_);
	for (auto&& it : part)
		cell_color.push_back(it);
	cell_color_ = move(cell_color);

	MASTER_MESSAGE("Set cell color(Time: " + TO_STRING(STOP()) + "s)");
}

void GridBuilder::redistributeCellData(void)
{
	START();
	int_t myrank = MYRANK();
	int_t ndomain = NDOMAIN();

	// data frame
	std::vector<int_t> num_senddata;
	std::vector<std::vector<int_t>> senddata;
	std::vector<int_t> num_recvdata;
	std::vector<std::vector<int_t>> recvdata;
	int_t num_total_recvdata;
	std::vector<MPI_Request> recv_req;
	std::vector<MPI_Status> recv_sta;
	std::vector<MPI_Request> send_req;
	std::vector<MPI_Status> send_sta;

	// generate cell global index
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	std::vector<int_t> num_cells_send(ndomain, 0);
	std::vector<std::vector<int_t>> cell_list_send(ndomain);

	for (int_t icell = 0; icell<num_cells_; icell++)
		num_cells_send[cell_color_[icell]]++;

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		cell_list_send[idomain].reserve(num_cells_send[idomain]);
	for (int_t icell = 0; icell<num_cells_; icell++)
		cell_list_send[cell_color_[icell]].push_back(icell);

	std::vector<int_t> num_cells_recv(ndomain, 0);
	std::vector<std::vector<int_t>> cell_list_recv(ndomain);

	MPI_Alltoall(&num_cells_send[0], 1, MPI_INT_T, &num_cells_recv[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	std::vector<int_t> trash_send(ndomain, 0);
	std::vector<int_t> trash_recv(ndomain, 0);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		cell_list_recv[idomain].resize(num_cells_recv[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++){
		if (num_cells_recv[idomain] > 0)
			MPI_Irecv(&cell_list_recv[idomain][0], num_cells_recv[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_cells_send[idomain]>0)
			MPI_Isend(&cell_list_send[idomain][0], num_cells_send[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	std::vector<int_t> cell_global_index;
	num_cells_ = 0;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_cells_ += num_cells_recv[idomain];
	cell_global_index.reserve(num_cells_);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_cells_recv[idomain]; idata++)
		cell_global_index.push_back(cell_list_recv[idomain][idata] + cell_dist_[idomain]);

	cell_global_index_ = move(cell_global_index);

	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell to node index data
	num_senddata.resize(ndomain, 0);
	senddata.resize(ndomain);
	num_recvdata.resize(ndomain, 0);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (auto&& icell : cell_list_send[idomain])
		num_senddata[idomain] += (cell_to_node_ptr_[icell + 1] - cell_to_node_ptr_[icell]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++){
		for (auto&& icell : cell_list_send[idomain]){
			const int_t& start_index = cell_to_node_ptr_[icell];
			const int_t& end_index = cell_to_node_ptr_[icell + 1];
			for (int_t inode = start_index; inode<end_index; inode++)
				senddata[idomain].push_back(cell_to_node_ind_[inode]);
		}
	}

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain < ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_recvdata[idomain]>0)
			MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_senddata[idomain]>0)
			MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	cell_to_node_ind_.clear();
	num_total_recvdata = 0;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_to_node_ind_.reserve(num_total_recvdata);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain]; idata++)
		cell_to_node_ind_.push_back(recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell to node pointer data
	num_senddata.resize(ndomain, 0);
	senddata.resize(ndomain);
	num_recvdata.resize(ndomain, 0);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_senddata[idomain] = num_cells_send[idomain] + 1;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++){
		senddata[idomain].push_back(0);
		for (auto&& icell : cell_list_send[idomain])
			senddata[idomain].push_back(senddata[idomain].back() + cell_to_node_ptr_[icell + 1] - cell_to_node_ptr_[icell]);
	}

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	cell_to_node_ptr_.clear();
	num_total_recvdata = 0;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_to_node_ptr_.reserve(num_total_recvdata - ndomain + 1);
	cell_to_node_ptr_.push_back(0);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain] - 1; idata++)
		cell_to_node_ptr_.push_back(cell_to_node_ptr_.back() + recvdata[idomain][idata + 1] - recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell to subcell node index data
	num_senddata.resize(ndomain, 0);
	senddata.resize(ndomain);
	num_recvdata.resize(ndomain, 0);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (auto&& icell : cell_list_send[idomain])
		num_senddata[idomain] += (cell_to_subnode_ptr_[icell + 1] - cell_to_subnode_ptr_[icell]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++){
		for (auto&& icell : cell_list_send[idomain]){
			const int_t& start_index = cell_to_subnode_ptr_[icell];
			const int_t& end_index = cell_to_subnode_ptr_[icell + 1];
			for (int_t inode = start_index; inode<end_index; inode++)
				senddata[idomain].push_back(cell_to_subnode_ind_[inode]);
		}
	}

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_recvdata[idomain]>0)
			MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_senddata[idomain]>0)
			MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	cell_to_subnode_ind_.clear();
	num_total_recvdata = 0;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_to_subnode_ind_.reserve(num_total_recvdata);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain]; idata++)
		cell_to_subnode_ind_.push_back(recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell to subcell node pointer data
	num_senddata.resize(ndomain, 0);
	senddata.resize(ndomain);
	num_recvdata.resize(ndomain, 0);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_senddata[idomain] = num_cells_send[idomain] + 1;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++){
		senddata[idomain].push_back(0);
		for (auto&& icell : cell_list_send[idomain])
			senddata[idomain].push_back(senddata[idomain].back() + cell_to_subnode_ptr_[icell + 1] - cell_to_subnode_ptr_[icell]);
	}

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	cell_to_subnode_ptr_.clear();
	num_total_recvdata = 0;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_to_subnode_ptr_.reserve(num_total_recvdata - ndomain + 1);
	cell_to_subnode_ptr_.push_back(0);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain] - 1; idata++)
		cell_to_subnode_ptr_.push_back(cell_to_subnode_ptr_.back() + recvdata[idomain][idata + 1] - recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell element type data
	num_senddata.resize(ndomain, 0);
	senddata.resize(ndomain);
	num_recvdata.resize(ndomain, 0);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_senddata[idomain] = num_cells_send[idomain];
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (auto&& icell : cell_list_send[idomain])
		senddata[idomain].push_back(cell_elem_type_[icell]);

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_recvdata[idomain]>0)
			MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_senddata[idomain]>0)
			MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	cell_elem_type_.clear();
	num_total_recvdata = 0;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_elem_type_.reserve(num_total_recvdata);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain]; idata++)
		cell_elem_type_.push_back(recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell order data
	num_senddata.resize(ndomain, 0);
	senddata.resize(ndomain);
	num_recvdata.resize(ndomain, 0);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_senddata[idomain] = num_cells_send[idomain];
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (auto&& icell : cell_list_send[idomain])
		senddata[idomain].push_back(cell_order_[icell]);

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_recvdata[idomain]>0)
			MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_senddata[idomain]>0)
			MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	cell_order_.clear();
	num_total_recvdata = 0;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_order_.reserve(num_total_recvdata);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain]; idata++)
		cell_order_.push_back(recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// renew cell color
	cell_color_.clear();
	cell_color_.resize(num_cells_, myrank);

	MASTER_MESSAGE("Redistribute cell data(Time: " + TO_STRING(STOP()) + "s)");
}

void GridBuilder::periodicMatching(void)
{
	// idirection = 0 : x-periodic
	// idirection = 1 : y-periodic
	// idirection = 2 : z-periodic
	// idirection = 3 : exact matching

	START();

	int_t myrank = MYRANK();
	int_t ndomain = NDOMAIN();

	peribdry_node_.resize(4);
	peribdry_node_to_matching_.resize(4);

	for (int_t idirection = 0; idirection < 4; idirection++){

		int_t num_alloc_nodes = node_dist_[1] - node_dist_[0];
		std::unordered_map<int_t, int_t> node_to_processor;
		for (int_t ibdry = 0; ibdry < num_peribdries_; ibdry++){
			if (peribdry_direction_[ibdry] == idirection){
				const int_t& start_index = peribdry_to_node_ptr_[ibdry];
				const int_t& end_index = peribdry_to_node_ptr_[ibdry + 1];
				for (int_t node_index = start_index; node_index < end_index; node_index++){
					const int_t& inode = peribdry_to_node_ind_[node_index];
					int_t idomain = inode / num_alloc_nodes;
					node_to_processor[inode] = idomain;
				}
			}
		}

		// get coordinates of periodic boundary nodes from allocated processor
		std::vector<int_t> num_nodes_send(ndomain, 0);
		std::vector<std::vector<int_t>> node_list_send(ndomain);

		for (auto&& iterator : node_to_processor)
			num_nodes_send[iterator.second]++;

		for (int_t idomain = 0; idomain < ndomain; idomain++)
			node_list_send[idomain].reserve(num_nodes_send[idomain]);
		for (auto&& iterator : node_to_processor)
			node_list_send[iterator.second].push_back(iterator.first);

		std::vector<MPI_Request> recv_req(ndomain);
		std::vector<MPI_Status> recv_sta(ndomain);
		std::vector<MPI_Request> send_req(ndomain);
		std::vector<MPI_Status> send_sta(ndomain);

		std::vector<int_t> num_nodes_recv(ndomain, 0);
		std::vector<std::vector<int_t>> node_list_recv(ndomain);

		MPI_Alltoall(&num_nodes_send[0], 1, MPI_INT_T, &num_nodes_recv[0], 1, MPI_INT_T, MPI_COMM_WORLD);

		std::vector<int_t> trash_send(ndomain, 0);
		std::vector<int_t> trash_recv(ndomain, 0);
		for (int_t idomain = 0; idomain < ndomain; idomain++)
			node_list_recv[idomain].resize(num_nodes_recv[idomain]);
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			if (num_nodes_recv[idomain]>0)
				MPI_Irecv(&node_list_recv[idomain][0], num_nodes_recv[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
			else
				MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		}
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			if (num_nodes_send[idomain]>0)
				MPI_Isend(&node_list_send[idomain][0], num_nodes_send[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
			else
				MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		}
		MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
		MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

		std::vector<std::vector<real_t>> node_coords_send(ndomain);
		for (int_t idomain = 0; idomain < ndomain; idomain++)
			node_coords_send[idomain].reserve(dimension_*num_nodes_recv[idomain]);
		for (int_t idomain = 0; idomain < ndomain; idomain++)
		for (auto&& inode : node_list_recv[idomain])
		for (int_t idimension = 0; idimension < dimension_; idimension++)
			node_coords_send[idomain].push_back(node_coords_[dimension_*(inode - node_dist_[myrank]) + idimension]);

		std::vector<std::vector<real_t>> node_coords_recv(ndomain);
		for (int_t idomain = 0; idomain < ndomain; idomain++)
			node_coords_recv[idomain].resize(dimension_*num_nodes_send[idomain]);
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			if (num_nodes_send[idomain]>0)
				MPI_Irecv(&node_coords_recv[idomain][0], dimension_*num_nodes_send[idomain], MPI_REAL_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
			else
				MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		}
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			if (num_nodes_recv[idomain]>0)
				MPI_Isend(&node_coords_send[idomain][0], dimension_*num_nodes_recv[idomain], MPI_REAL_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
			else
				MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		}
		MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
		MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

		int_t num_peribdry_nodes;
		std::vector<int_t> peribdry_node;
		std::vector<real_t> peribdry_node_coords;
		std::vector<int_t> peribdry_node_match;

		num_peribdry_nodes = node_to_processor.size();
		peribdry_node.reserve(num_peribdry_nodes);
		for (int_t idomain = 0; idomain < ndomain; idomain++)
		for (auto&& inode : node_list_send[idomain])
			peribdry_node.push_back(inode);
		peribdry_node_coords.reserve(num_peribdry_nodes*dimension_);
		for (int_t idomain = 0; idomain < ndomain; idomain++)
		for (auto&& icoords : node_coords_recv[idomain])
			peribdry_node_coords.push_back(icoords);
		peribdry_node_match.resize(num_peribdry_nodes, -1);

		// find matching node by circulating each processor circularly
		int_t before_domain = myrank - 1;
		int_t next_domain = myrank + 1;
		if (myrank == MASTER_NODE) before_domain = ndomain - 1;
		if (myrank == ndomain - 1) next_domain = MASTER_NODE;

		std::vector<int_t> num_peribdry_nodes_recv(ndomain, 0);
		std::vector<int_t> peribdry_node_recv;
		std::vector<real_t> peribdry_node_coords_recv;
		std::vector<int_t> peribdry_node_match_recv;
		std::vector<int_t> peribdry_node_send;
		std::vector<real_t> peribdry_node_coords_send;
		std::vector<int_t> peribdry_node_match_send;

		MPI_Request recv_req_local;
		MPI_Status recv_sta_local;
		MPI_Request send_req_local;
		MPI_Status send_sta_local;

		MPI_Allgather(&num_peribdry_nodes, 1, MPI_INT_T, &num_peribdry_nodes_recv[0], 1, MPI_INT_T, MPI_COMM_WORLD);

		peribdry_node_recv = peribdry_node;
		peribdry_node_coords_recv = peribdry_node_coords;
		peribdry_node_match_recv.resize(num_peribdry_nodes, -1);
		int_t recv_domain;
		int_t send_domain;
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			recv_domain = (myrank - idomain + ndomain) % ndomain;
			for (int_t ibdry_recv = 0; ibdry_recv < num_peribdry_nodes_recv[recv_domain]; ibdry_recv++){
				int_t check_flag = -1;
				if (peribdry_node_match_recv[ibdry_recv] != -1) continue;
				for (int_t ibdry = 0; ibdry < num_peribdry_nodes; ibdry++){
					if (peribdry_node_recv[ibdry_recv] == peribdry_node[ibdry]){
						if (peribdry_node_match[ibdry] == -1){
							check_flag = ibdry; continue;
						}
						else{
							peribdry_node_match_recv[ibdry_recv] = peribdry_node_match[ibdry]; continue;
						}
					}
					if (peribdry_node_recv[ibdry_recv] == peribdry_node_match[ibdry] || isMatch(&peribdry_node_coords_recv[ibdry_recv*dimension_], &peribdry_node_coords[ibdry*dimension_], idirection)){
						peribdry_node_match_recv[ibdry_recv] = peribdry_node[ibdry];
						if (check_flag != -1) peribdry_node_match[check_flag] = peribdry_node[ibdry];
						continue;
					}
				}
			}

			send_domain = recv_domain;
			peribdry_node_send = move(peribdry_node_recv);
			peribdry_node_coords_send = move(peribdry_node_coords_recv);
			peribdry_node_match_send = move(peribdry_node_match_recv);

			recv_domain = (recv_domain - 1 + ndomain) % ndomain;
			peribdry_node_recv.resize(num_peribdry_nodes_recv[recv_domain]);
			peribdry_node_coords_recv.resize(dimension_*num_peribdry_nodes_recv[recv_domain]);
			peribdry_node_match_recv.resize(num_peribdry_nodes_recv[recv_domain]);

			if (num_peribdry_nodes_recv[recv_domain]>0)
				MPI_Irecv(&peribdry_node_recv[0], num_peribdry_nodes_recv[recv_domain], MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
			else
				MPI_Irecv(&trash_recv[0], 1, MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
			if (num_peribdry_nodes_recv[send_domain]>0)
				MPI_Isend(&peribdry_node_send[0], num_peribdry_nodes_recv[send_domain], MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
			else
				MPI_Isend(&trash_send[0], 1, MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
			MPI_Wait(&recv_req_local, &recv_sta_local);
			MPI_Wait(&send_req_local, &send_sta_local);

			if (num_peribdry_nodes_recv[recv_domain]>0)
				MPI_Irecv(&peribdry_node_coords_recv[0], dimension_*num_peribdry_nodes_recv[recv_domain], MPI_REAL_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
			else
				MPI_Irecv(&trash_recv[0], 1, MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
			if (num_peribdry_nodes_recv[send_domain]>0)
				MPI_Isend(&peribdry_node_coords_send[0], dimension_*num_peribdry_nodes_recv[send_domain], MPI_REAL_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
			else
				MPI_Isend(&trash_send[0], 1, MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
			MPI_Wait(&recv_req_local, &recv_sta_local);
			MPI_Wait(&send_req_local, &send_sta_local);

			if (num_peribdry_nodes_recv[recv_domain]>0)
				MPI_Irecv(&peribdry_node_match_recv[0], num_peribdry_nodes_recv[recv_domain], MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
			else
				MPI_Irecv(&trash_recv[0], 1, MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
			if (num_peribdry_nodes_recv[send_domain]>0)
				MPI_Isend(&peribdry_node_match_send[0], num_peribdry_nodes_recv[send_domain], MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
			else
				MPI_Isend(&trash_send[0], 1, MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
			MPI_Wait(&recv_req_local, &recv_sta_local);
			MPI_Wait(&send_req_local, &send_sta_local);
		}

		// generate matching node index
		std::unordered_map<int_t, int_t> node_to_matching;
		for (int_t inode = 0; inode < num_peribdry_nodes; inode++)
			node_to_matching[peribdry_node[inode]] = peribdry_node_match_recv[inode];

		std::vector<int_t> peribdry_node_to_matching(num_peribdry_nodes);
		for (int_t inode = 0; inode < num_peribdry_nodes; inode++)
			peribdry_node_to_matching[inode] = node_to_matching[peribdry_node[inode]];

		// reallocate matching node index to processor
		std::vector<std::vector<int_t>> matching_node_list_send(ndomain);
		for (int_t idomain = 0; idomain < ndomain; idomain++)
			matching_node_list_send[idomain].reserve(num_nodes_send[idomain]);
		int_t index = 0;
		for (int_t idomain = 0; idomain < ndomain; idomain++)
		for (int_t inode = 0; inode < num_nodes_send[idomain]; inode++)
			matching_node_list_send[idomain].push_back(peribdry_node_to_matching[index++]);

		std::vector<std::vector<int_t>> matching_node_list_recv(ndomain);
		for (int_t idomain = 0; idomain < ndomain; idomain++)
			matching_node_list_recv[idomain].resize(num_nodes_recv[idomain]);
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			if (num_nodes_recv[idomain]>0)
				MPI_Irecv(&matching_node_list_recv[idomain][0], num_nodes_recv[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
			else
				MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		}
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			if (num_nodes_send[idomain]>0)
				MPI_Isend(&matching_node_list_send[idomain][0], num_nodes_send[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
			else
				MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		}
		MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
		MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

		// generate matching node index after reallocation
		node_to_matching.clear();
		for (int_t idomain = 0; idomain < ndomain; idomain++)
		for (int_t inode = 0; inode < num_nodes_recv[idomain];inode++)
			node_to_matching[node_list_recv[idomain][inode]] = matching_node_list_recv[idomain][inode];

		peribdry_node.clear();
		peribdry_node_to_matching.clear();
		peribdry_node.reserve(node_to_matching.size());
		peribdry_node_to_matching.reserve(node_to_matching.size());
		for (auto&& iterator : node_to_matching){
			peribdry_node.push_back(iterator.first);
			peribdry_node_to_matching.push_back(iterator.second);
		}

		peribdry_node_[idirection] = move(peribdry_node);
		peribdry_node_to_matching_[idirection] = move(peribdry_node_to_matching);
	}

	MASTER_MESSAGE("Match periodic nodes(Time: " + TO_STRING(STOP()) + "s)");
}

void GridBuilder::constructVirtualCellData(void)
{
	START();

	int_t myrank = MYRANK();
	int_t ndomain = NDOMAIN();

	// identify periodic bounadry nodes
	std::vector<std::vector<int_t>> peribdry_node;
	std::vector<std::vector<int_t>> peribdry_node_to_matching;
	communicatePeribdryNode(peribdry_node, peribdry_node_to_matching);

	// generate node to cell pointer
	std::unordered_map<int_t, std::vector<int_t>> node_to_cell;
	for (int_t icell = 0; icell < cell_to_node_ptr_.size() - 1; icell++){
		const int_t& start_index = cell_to_node_ptr_[icell];
		const int_t& end_index = cell_to_node_ptr_[icell+1];
		for (int_t inode = start_index; inode < end_index; inode++){
			node_to_cell[cell_to_node_ind_[inode]].push_back(icell);
		}
	}

	// generate node index for communication
	std::unordered_map<int_t, bool> total_node_to_cell;
	for (auto&& iterator : node_to_cell)
		total_node_to_cell[iterator.first] = true;
	for (int_t idirection = 0; idirection < 4; idirection++){
		for (auto&& inode : peribdry_node_to_matching[idirection]){
			if (total_node_to_cell.find(inode) == total_node_to_cell.end()){
				total_node_to_cell[inode] = false;
			}
		}
	}

	std::vector<int_t> node_list_send;
	std::vector<int_t> node_list_recv;
	std::vector<int_t> num_nodes_send(ndomain);
	node_list_send.reserve(total_node_to_cell.size());
	for (auto&& iterator : total_node_to_cell)
		node_list_send.push_back(iterator.first);
	int_t num_nodes = node_list_send.size();

	MPI_Allgather(&num_nodes, 1, MPI_INT_T, &num_nodes_send[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	MPI_Request recv_req_local;
	MPI_Status recv_sta_local;
	MPI_Request send_req_local;
	MPI_Status send_sta_local;

	int_t before_domain = myrank - 1;
	int_t next_domain = myrank + 1;
	if (myrank == MASTER_NODE) before_domain = ndomain - 1;
	if (myrank == ndomain - 1) next_domain = MASTER_NODE;
	int_t recv_domain;
	int_t send_domain;
	std::vector<std::vector<int_t>> cell_list_send(ndomain);

	std::vector<int_t> trash_send(ndomain, 0);
	std::vector<int_t> trash_recv(ndomain, 0);

	for (int_t idomain = 0; idomain < ndomain - 1; idomain++){
		recv_domain = (myrank - 1 - idomain + ndomain) % ndomain;
		send_domain = (myrank - idomain + ndomain) % ndomain;
		node_list_recv.clear();
		node_list_recv.resize(num_nodes_send[recv_domain]);

		if (num_nodes_send[recv_domain]>0)
			MPI_Irecv(&node_list_recv[0], num_nodes_send[recv_domain], MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		else
			MPI_Irecv(&trash_recv[0], 1, MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		if (num_nodes_send[send_domain]>0)
			MPI_Isend(&node_list_send[0], num_nodes_send[send_domain], MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		else
			MPI_Isend(&trash_send[0], 1, MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		MPI_Wait(&recv_req_local, &recv_sta_local);
		MPI_Wait(&send_req_local, &send_sta_local);

		node_list_send = move(node_list_recv);

		std::vector<bool> cell_flag(num_cells_, false);
		for (int_t inode = 0; inode < num_nodes_send[recv_domain]; inode++){
			auto&& iterator = node_to_cell.find(node_list_send[inode]);
			if (iterator == node_to_cell.end()) continue;
			for (auto&& icell : iterator->second)
				cell_flag[icell] = true;
		}
		for (int_t idirection = 0; idirection < 4; idirection++){
			for (int_t inode = 0; inode < peribdry_node[idirection].size();inode++){
				for (int_t jnode = 0; jnode < num_nodes_send[recv_domain]; jnode++){
					if (peribdry_node_to_matching[idirection][inode] == node_list_send[jnode]){
						for (auto&& icell : node_to_cell[peribdry_node[idirection][inode]])
							cell_flag[icell] = true;
					}
				}
			}
		}

		std::vector<int_t> cell_list_send_local;
		for (int_t icell = 0; icell < num_cells_;icell++)
			if (cell_flag[icell] == true)
				cell_list_send_local.push_back(icell);
		cell_list_send[recv_domain] = move(cell_list_send_local);
	}

	// data frame for communication
	std::vector<MPI_Request> recv_req(ndomain);
	std::vector<MPI_Status> recv_sta(ndomain);
	std::vector<MPI_Request> send_req(ndomain);
	std::vector<MPI_Status> send_sta(ndomain);

	std::vector<int_t> num_cells_send(ndomain, 0);
	std::vector<int_t> num_cells_recv(ndomain, 0);

	int_t num_total_recvdata = 0;
	std::vector<int_t> num_senddata(ndomain, 0);
	std::vector<int_t> num_recvdata(ndomain, 0);
	std::vector<std::vector<int_t>> senddata(ndomain);
	std::vector<std::vector<int_t>> recvdata(ndomain);

	// communicate number of sending & receiving cells
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		num_cells_send[idomain] = cell_list_send[idomain].size();

	MPI_Alltoall(&num_cells_send[0], 1, MPI_INT_T, &num_cells_recv[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	// communicate cell global index
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_cells_send[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (auto&& icell : cell_list_send[idomain])
		senddata[idomain].push_back(cell_global_index_[icell]);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_cells_recv[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_cells_recv[idomain]>0)
			MPI_Irecv(&recvdata[idomain][0], num_cells_recv[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_cells_send[idomain]>0)
			MPI_Isend(&senddata[idomain][0], num_cells_send[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	num_total_cells_ = num_cells_;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_cells_ += num_cells_recv[idomain];
	cell_global_index_.reserve(num_total_cells_);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_cells_recv[idomain]; idata++)
		cell_global_index_.push_back(recvdata[idomain][idata]);

	senddata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell to node index data
	senddata.resize(ndomain);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (auto&& icell : cell_list_send[idomain])
		num_senddata[idomain] += (cell_to_node_ptr_[icell + 1] - cell_to_node_ptr_[icell]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++){
		for (auto&& icell : cell_list_send[idomain]){
			const int_t& start_index = cell_to_node_ptr_[icell];
			const int_t& end_index = cell_to_node_ptr_[icell + 1];
			for (int_t inode = start_index; inode<end_index; inode++)
				senddata[idomain].push_back(cell_to_node_ind_[inode]);
		}
	}

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_recvdata[idomain]>0)
			MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_senddata[idomain]>0)
			MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	num_total_recvdata = cell_to_node_ind_.size();
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_to_node_ind_.reserve(num_total_recvdata);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain]; idata++)
		cell_to_node_ind_.push_back(recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell to node pointer data
	num_senddata.resize(ndomain, 0);
	senddata.resize(ndomain);
	num_recvdata.resize(ndomain, 0);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_senddata[idomain] = num_cells_send[idomain] + 1;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++){
		senddata[idomain].push_back(0);
		for (auto&& icell : cell_list_send[idomain])
			senddata[idomain].push_back(senddata[idomain].back() + cell_to_node_ptr_[icell + 1] - cell_to_node_ptr_[icell]);
	}

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	num_total_recvdata = cell_to_node_ptr_.size();
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_to_node_ptr_.reserve(num_total_recvdata - ndomain);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain] - 1; idata++)
		cell_to_node_ptr_.push_back(cell_to_node_ptr_.back() + recvdata[idomain][idata + 1] - recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell to subcell node index data
	num_senddata.resize(ndomain, 0);
	senddata.resize(ndomain);
	num_recvdata.resize(ndomain, 0);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (auto&& icell : cell_list_send[idomain])
		num_senddata[idomain] += (cell_to_subnode_ptr_[icell + 1] - cell_to_subnode_ptr_[icell]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++){
		for (auto&& icell : cell_list_send[idomain]){
			const int_t& start_index = cell_to_subnode_ptr_[icell];
			const int_t& end_index = cell_to_subnode_ptr_[icell + 1];
			for (int_t inode = start_index; inode<end_index; inode++)
				senddata[idomain].push_back(cell_to_subnode_ind_[inode]);
		}
	}

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_recvdata[idomain]>0)
			MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_senddata[idomain]>0)
			MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	num_total_recvdata = cell_to_subnode_ind_.size();
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_to_subnode_ind_.reserve(num_total_recvdata);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain]; idata++)
		cell_to_subnode_ind_.push_back(recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell to subcell node pointer data
	num_senddata.resize(ndomain, 0);
	senddata.resize(ndomain);
	num_recvdata.resize(ndomain, 0);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_senddata[idomain] = num_cells_send[idomain] + 1;
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++){
		senddata[idomain].push_back(0);
		for (auto&& icell : cell_list_send[idomain])
			senddata[idomain].push_back(senddata[idomain].back() + cell_to_subnode_ptr_[icell + 1] - cell_to_subnode_ptr_[icell]);
	}

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	num_total_recvdata = cell_to_subnode_ptr_.size();
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_to_subnode_ptr_.reserve(num_total_recvdata - ndomain);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain] - 1; idata++)
		cell_to_subnode_ptr_.push_back(cell_to_subnode_ptr_.back() + recvdata[idomain][idata + 1] - recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell element type data
	num_senddata.resize(ndomain, 0);
	senddata.resize(ndomain);
	num_recvdata.resize(ndomain, 0);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_senddata[idomain] = num_cells_send[idomain];
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (auto&& icell : cell_list_send[idomain])
		senddata[idomain].push_back(cell_elem_type_[icell]);

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_recvdata[idomain]>0)
			MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_senddata[idomain]>0)
			MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	num_total_recvdata = cell_elem_type_.size();
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_elem_type_.reserve(num_total_recvdata);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain]; idata++)
		cell_elem_type_.push_back(recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// communicate cell order data
	num_senddata.resize(ndomain, 0);
	senddata.resize(ndomain);
	num_recvdata.resize(ndomain, 0);
	recvdata.resize(ndomain);
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_senddata[idomain] = num_cells_send[idomain];
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		senddata[idomain].reserve(num_senddata[idomain]);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (auto&& icell : cell_list_send[idomain])
		senddata[idomain].push_back(cell_order_[icell]);

	MPI_Alltoall(&num_senddata[0], 1, MPI_INT_T, &num_recvdata[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	for (int_t idomain = 0; idomain<ndomain; idomain++)
		recvdata[idomain].resize(num_recvdata[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_recvdata[idomain]>0)
			MPI_Irecv(&recvdata[idomain][0], num_recvdata[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_senddata[idomain]>0)
			MPI_Isend(&senddata[idomain][0], num_senddata[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	num_total_recvdata = cell_order_.size();
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		num_total_recvdata += num_recvdata[idomain];
	cell_order_.reserve(num_total_recvdata);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
	for (int_t idata = 0; idata<num_recvdata[idomain]; idata++)
		cell_order_.push_back(recvdata[idomain][idata]);

	num_senddata.clear();
	senddata.clear();
	num_recvdata.clear();
	recvdata.clear();
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();

	// add cell color
	cell_color_.reserve(num_total_cells_);
	for (int_t idomain = 0; idomain < ndomain;idomain++)
	for (int_t icell = 0; icell < num_cells_recv[idomain]; icell++)
		cell_color_.push_back(idomain);

	// reconstruct communication data
	recv_req.clear();
	recv_sta.clear();
	send_req.clear();
	send_sta.clear();
	recv_req.resize(ndomain);
	recv_sta.resize(ndomain);
	send_req.resize(ndomain);
	send_sta.resize(ndomain);

	num_recv_cells_.resize(ndomain, 0);
	for (int_t icell = num_cells_; icell < num_total_cells_; icell++)
		num_recv_cells_[cell_color_[icell]]++;

	num_send_cells_.resize(ndomain, 0);
	MPI_Alltoall(&num_recv_cells_[0], 1, MPI_INT_T, &num_send_cells_[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	recv_cell_list_.resize(ndomain);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		recv_cell_list_[idomain].reserve(num_recv_cells_[idomain]);
	for (int_t icell = num_cells_; icell < num_total_cells_; icell++)
		recv_cell_list_[cell_color_[icell]].push_back(icell);

	std::vector<std::vector<int_t>> recv_cell_global_list(ndomain);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		recv_cell_global_list[idomain].resize(num_recv_cells_[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
	for (int_t icell = 0; icell < num_recv_cells_[idomain]; icell++)
		recv_cell_global_list[idomain][icell] = cell_global_index_[recv_cell_list_[idomain][icell]];

	std::vector<std::vector<int_t>> send_cell_global_list(ndomain);
	for (int_t idomain = 0; idomain<ndomain; idomain++)
		send_cell_global_list[idomain].resize(num_send_cells_[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_send_cells_[idomain]>0)
			MPI_Irecv(&send_cell_global_list[idomain][0], num_send_cells_[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_recv_cells_[idomain]>0)
			MPI_Isend(&recv_cell_global_list[idomain][0], num_recv_cells_[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	std::unordered_map<int_t, int_t> global_to_local_cell_index;
	for (int_t icell = 0; icell < num_cells_; icell++){
		global_to_local_cell_index[cell_global_index_[icell]] = icell;
	}

	send_cell_list_.resize(ndomain);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		send_cell_list_[idomain].resize(num_send_cells_[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
	for (int_t icell = 0; icell < num_send_cells_[idomain]; icell++)
		send_cell_list_[idomain][icell] = global_to_local_cell_index[send_cell_global_list[idomain][icell]];

	MASTER_MESSAGE("Construct virtual cell data(Time: " + TO_STRING(STOP()) + "s)");
}

void GridBuilder::redistributeBdryData(void)
{
	START();

	int_t myrank = MYRANK();
	int_t ndomain = NDOMAIN();

	// generate node to cell pointer
	std::unordered_map<int_t, std::vector<int_t>> node_to_cell;
	for (int_t icell = 0; icell < num_total_cells_; icell++){
		const int_t& start_index = cell_to_node_ptr_[icell];
		const int_t& end_index = cell_to_node_ptr_[icell + 1];
		for (int_t inode = start_index; inode < end_index; inode++){
			node_to_cell[cell_to_node_ind_[inode]].push_back(icell);
		}
	}

	// data frame
	std::vector<int_t> num_ind_send(ndomain, 0);
	std::vector<int_t> num_ptr_send(ndomain, 0);
	std::vector<int_t> num_type_send(ndomain, 0);

	int_t num_ind_send_local;
	int_t num_ptr_send_local;
	int_t num_type_send_local;

	MPI_Request recv_req_local;
	MPI_Status recv_sta_local;
	MPI_Request send_req_local;
	MPI_Status send_sta_local;

	std::vector<int_t> bdry_to_node_ind_send;
	std::vector<int_t> bdry_to_node_ptr_send;
	std::vector<int_t> bdry_type_send;
	std::vector<int_t> bdry_to_node_ind_recv;
	std::vector<int_t> bdry_to_node_ptr_recv;
	std::vector<int_t> bdry_type_recv;

	int_t trash_send = 0;
	int_t trash_recv = 0;

	int_t before_domain = myrank - 1;
	int_t next_domain = myrank + 1;
	if (myrank == MASTER_NODE) before_domain = ndomain - 1;
	if (myrank == ndomain - 1) next_domain = MASTER_NODE;
	int_t recv_domain;
	int_t send_domain;

	// redistribute boundary data
	num_ind_send_local = bdry_to_node_ind_.size();
	num_ptr_send_local = bdry_to_node_ptr_.size();
	num_type_send_local = bdry_type_.size();
	MPI_Allgather(&num_ind_send_local, 1, MPI_INT_T, &num_ind_send[0], 1, MPI_INT_T, MPI_COMM_WORLD);
	MPI_Allgather(&num_ptr_send_local, 1, MPI_INT_T, &num_ptr_send[0], 1, MPI_INT_T, MPI_COMM_WORLD);
	MPI_Allgather(&num_type_send_local, 1, MPI_INT_T, &num_type_send[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	bdry_to_node_ind_send = move(bdry_to_node_ind_);
	bdry_to_node_ptr_send = move(bdry_to_node_ptr_);
	bdry_type_send = move(bdry_type_);
	bdry_to_node_ptr_.push_back(0);

	for (int_t idomain = 0; idomain < ndomain; idomain++){
		for (int_t ibdry = 0; ibdry < bdry_to_node_ptr_send.size() - 1; ibdry++){
			const int_t& start_index = bdry_to_node_ptr_send[ibdry];
			const int_t& end_index = bdry_to_node_ptr_send[ibdry + 1];
			auto&& iterator = node_to_cell.find(bdry_to_node_ind_send[start_index]);
			if (iterator == node_to_cell.end()) continue;

			for (auto&& icell : iterator->second){
				if (isParent(&cell_to_node_ind_[cell_to_node_ptr_[icell]], cell_to_node_ptr_[icell + 1] - cell_to_node_ptr_[icell], &bdry_to_node_ind_send[start_index], end_index - start_index)){
					for (int_t inode = start_index; inode < end_index; inode++)
						bdry_to_node_ind_.push_back(bdry_to_node_ind_send[inode]);
					bdry_to_node_ptr_.push_back(bdry_to_node_ind_.size());
					bdry_type_.push_back(bdry_type_send[ibdry]);
					bdry_color_.push_back(cell_color_[icell]);
					bdry_owner_cell_.push_back(icell);
					break;
				}
			}
		}

		recv_domain = (myrank - 1 - idomain + ndomain) % ndomain;
		send_domain = (myrank - idomain + ndomain) % ndomain;
		bdry_to_node_ind_recv.clear();
		bdry_to_node_ptr_recv.clear();
		bdry_type_recv.clear();
		bdry_to_node_ind_recv.resize(num_ind_send[recv_domain]);
		bdry_to_node_ptr_recv.resize(num_ptr_send[recv_domain]);
		bdry_type_recv.resize(num_type_send[recv_domain]);

		if (num_ind_send[recv_domain]>0)
			MPI_Irecv(&bdry_to_node_ind_recv[0], num_ind_send[recv_domain], MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		else
			MPI_Irecv(&trash_recv, 1, MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		if (num_ind_send[send_domain]>0)
			MPI_Isend(&bdry_to_node_ind_send[0], num_ind_send[send_domain], MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		else
			MPI_Isend(&trash_send, 1, MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		MPI_Wait(&recv_req_local, &recv_sta_local);
		MPI_Wait(&send_req_local, &send_sta_local);

		if (num_ptr_send[recv_domain]>0)
			MPI_Irecv(&bdry_to_node_ptr_recv[0], num_ptr_send[recv_domain], MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		else
			MPI_Irecv(&trash_recv, 1, MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		if (num_ptr_send[send_domain]>0)
			MPI_Isend(&bdry_to_node_ptr_send[0], num_ptr_send[send_domain], MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		else
			MPI_Isend(&trash_send, 1, MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		MPI_Wait(&recv_req_local, &recv_sta_local);
		MPI_Wait(&send_req_local, &send_sta_local);

		if (num_type_send[recv_domain]>0)
			MPI_Irecv(&bdry_type_recv[0], num_type_send[recv_domain], MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		else
			MPI_Irecv(&trash_recv, 1, MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		if (num_type_send[send_domain]>0)
			MPI_Isend(&bdry_type_send[0], num_type_send[send_domain], MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		else
			MPI_Isend(&trash_send, 1, MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		MPI_Wait(&recv_req_local, &recv_sta_local);
		MPI_Wait(&send_req_local, &send_sta_local);

		bdry_to_node_ind_send = move(bdry_to_node_ind_recv);
		bdry_to_node_ptr_send = move(bdry_to_node_ptr_recv);
		bdry_type_send = move(bdry_type_recv);
	}

	// renumbering boundary data by color
	num_total_bdries_ = bdry_type_.size();
	std::vector<int_t> new_to_old_index(num_total_bdries_);
	int_t index = 0;
	for (int_t ibdry = 0; ibdry < num_total_bdries_; ibdry++)
		if (bdry_color_[ibdry] == myrank)
			new_to_old_index[index++] = ibdry;
	num_bdries_ = index;
	for (int_t ibdry = 0; ibdry < num_total_bdries_; ibdry++)
		if (bdry_color_[ibdry] != myrank)
			new_to_old_index[index++] = ibdry;

	std::vector<int_t> bdry_color(num_total_bdries_);
	for (int_t ibdry = 0; ibdry < num_total_bdries_; ibdry++)
		bdry_color[ibdry] = bdry_color_[new_to_old_index[ibdry]];
	bdry_color_ = move(bdry_color);

	std::vector<int_t> bdry_type(num_total_bdries_);
	for (int_t ibdry = 0; ibdry < num_total_bdries_; ibdry++)
		bdry_type[ibdry] = bdry_type_[new_to_old_index[ibdry]];
	bdry_type_ = move(bdry_type);

	std::vector<int_t> bdry_owner_cell(num_total_bdries_);
	for (int_t ibdry = 0; ibdry < num_total_bdries_; ibdry++)
		bdry_owner_cell[ibdry] = bdry_owner_cell_[new_to_old_index[ibdry]];
	bdry_owner_cell_ = move(bdry_owner_cell);

	std::vector<int_t> bdry_to_node_ind(bdry_to_node_ind_.size());
	index = 0;
	for (int_t ibdry = 0; ibdry < num_total_bdries_; ibdry++){
		const int_t& start_index = bdry_to_node_ptr_[new_to_old_index[ibdry]];
		const int_t& end_index = bdry_to_node_ptr_[new_to_old_index[ibdry]+1];
		for (int_t inode = start_index; inode < end_index; inode++)
			bdry_to_node_ind[index++] = bdry_to_node_ind_[inode];
	}
	bdry_to_node_ind_ = move(bdry_to_node_ind);

	std::vector<int_t> bdry_to_node_ptr(bdry_to_node_ptr_.size());
	bdry_to_node_ptr[0] = 0;
	for (int_t ibdry = 0; ibdry < num_total_bdries_; ibdry++)
		bdry_to_node_ptr[ibdry + 1] = bdry_to_node_ptr[ibdry] + bdry_to_node_ptr_[new_to_old_index[ibdry] + 1] - bdry_to_node_ptr_[new_to_old_index[ibdry]];
	bdry_to_node_ptr_ = move(bdry_to_node_ptr);

	// redistribute periodic boundary data
	num_ind_send_local = peribdry_to_node_ind_.size();
	num_ptr_send_local = peribdry_to_node_ptr_.size();
	num_type_send_local = peribdry_direction_.size();
	MPI_Allgather(&num_ind_send_local, 1, MPI_INT_T, &num_ind_send[0], 1, MPI_INT_T, MPI_COMM_WORLD);
	MPI_Allgather(&num_ptr_send_local, 1, MPI_INT_T, &num_ptr_send[0], 1, MPI_INT_T, MPI_COMM_WORLD);
	MPI_Allgather(&num_type_send_local, 1, MPI_INT_T, &num_type_send[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	bdry_to_node_ind_send = move(peribdry_to_node_ind_);
	bdry_to_node_ptr_send = move(peribdry_to_node_ptr_);
	bdry_type_send = move(peribdry_direction_);
	peribdry_to_node_ptr_.push_back(0);

	for (int_t idomain = 0; idomain < ndomain; idomain++){
		for (int_t ibdry = 0; ibdry < bdry_to_node_ptr_send.size() - 1; ibdry++){
			const int_t& start_index = bdry_to_node_ptr_send[ibdry];
			const int_t& end_index = bdry_to_node_ptr_send[ibdry + 1];
			auto&& iterator = node_to_cell.find(bdry_to_node_ind_send[start_index]);
			if (iterator == node_to_cell.end()) continue;

			for (auto&& icell : iterator->second){
				if (isParent(&cell_to_node_ind_[cell_to_node_ptr_[icell]], cell_to_node_ptr_[icell + 1] - cell_to_node_ptr_[icell], &bdry_to_node_ind_send[start_index], end_index - start_index)){
					for (int_t inode = start_index; inode < end_index; inode++)
						peribdry_to_node_ind_.push_back(bdry_to_node_ind_send[inode]);
					peribdry_to_node_ptr_.push_back(peribdry_to_node_ind_.size());
					peribdry_direction_.push_back(bdry_type_send[ibdry]);
					peribdry_color_.push_back(cell_color_[icell]);
					peribdry_owner_cell_.push_back(icell);
					break;
				}
			}
		}

		recv_domain = (myrank - 1 - idomain + ndomain) % ndomain;
		send_domain = (myrank - idomain + ndomain) % ndomain;
		bdry_to_node_ind_recv.clear();
		bdry_to_node_ptr_recv.clear();
		bdry_type_recv.clear();
		bdry_to_node_ind_recv.resize(num_ind_send[recv_domain]);
		bdry_to_node_ptr_recv.resize(num_ptr_send[recv_domain]);
		bdry_type_recv.resize(num_type_send[recv_domain]);

		if (num_ind_send[recv_domain]>0)
			MPI_Irecv(&bdry_to_node_ind_recv[0], num_ind_send[recv_domain], MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		else
			MPI_Irecv(&trash_recv, 1, MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		if (num_ind_send[send_domain]>0)
			MPI_Isend(&bdry_to_node_ind_send[0], num_ind_send[send_domain], MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		else
			MPI_Isend(&trash_send, 1, MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		MPI_Wait(&recv_req_local, &recv_sta_local);
		MPI_Wait(&send_req_local, &send_sta_local);

		if (num_ptr_send[recv_domain]>0)
			MPI_Irecv(&bdry_to_node_ptr_recv[0], num_ptr_send[recv_domain], MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		else
			MPI_Irecv(&trash_recv, 1, MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		if (num_ptr_send[send_domain]>0)
			MPI_Isend(&bdry_to_node_ptr_send[0], num_ptr_send[send_domain], MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		else
			MPI_Isend(&trash_send, 1, MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		MPI_Wait(&recv_req_local, &recv_sta_local);
		MPI_Wait(&send_req_local, &send_sta_local);

		if (num_type_send[recv_domain]>0)
			MPI_Irecv(&bdry_type_recv[0], num_type_send[recv_domain], MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		else
			MPI_Irecv(&trash_recv, 1, MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
		if (num_type_send[send_domain]>0)
			MPI_Isend(&bdry_type_send[0], num_type_send[send_domain], MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		else
			MPI_Isend(&trash_send, 1, MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
		MPI_Wait(&recv_req_local, &recv_sta_local);
		MPI_Wait(&send_req_local, &send_sta_local);

		bdry_to_node_ind_send = move(bdry_to_node_ind_recv);
		bdry_to_node_ptr_send = move(bdry_to_node_ptr_recv);
		bdry_type_send = move(bdry_type_recv);
	}

	// renumbering periodic boundary data by color
	num_total_peribdries_ = peribdry_direction_.size();
	new_to_old_index.resize(num_total_peribdries_);
	index = 0;
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++)
	if (peribdry_color_[ibdry] == myrank)
		new_to_old_index[index++] = ibdry;
	num_peribdries_ = index;
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++)
	if (peribdry_color_[ibdry] != myrank)
		new_to_old_index[index++] = ibdry;

	std::vector<int_t> peribdry_color(num_total_peribdries_);
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++)
		peribdry_color[ibdry] = peribdry_color_[new_to_old_index[ibdry]];
	peribdry_color_ = move(peribdry_color);

	std::vector<int_t> peribdry_direction(num_total_peribdries_);
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++)
		peribdry_direction[ibdry] = peribdry_direction_[new_to_old_index[ibdry]];
	peribdry_direction_ = move(peribdry_direction);

	std::vector<int_t> peribdry_owner_cell(num_total_peribdries_);
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++)
		peribdry_owner_cell[ibdry] = peribdry_owner_cell_[new_to_old_index[ibdry]];
	peribdry_owner_cell_ = move(peribdry_owner_cell);

	std::vector<int_t> peribdry_to_node_ind(peribdry_to_node_ind_.size());
	index = 0;
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++){
		const int_t& start_index = peribdry_to_node_ptr_[new_to_old_index[ibdry]];
		const int_t& end_index = peribdry_to_node_ptr_[new_to_old_index[ibdry] + 1];
		for (int_t inode = start_index; inode < end_index; inode++)
			peribdry_to_node_ind[index++] = peribdry_to_node_ind_[inode];
	}
	peribdry_to_node_ind_ = move(peribdry_to_node_ind);

	std::vector<int_t> peribdry_to_node_ptr(peribdry_to_node_ptr_.size());
	peribdry_to_node_ptr[0] = 0;
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++)
		peribdry_to_node_ptr[ibdry + 1] = peribdry_to_node_ptr[ibdry] + peribdry_to_node_ptr_[new_to_old_index[ibdry] + 1] - peribdry_to_node_ptr_[new_to_old_index[ibdry]];
	peribdry_to_node_ptr_ = move(peribdry_to_node_ptr);

	// periodic boundary matching
	std::vector<std::vector<int_t>> peribdry_node;
	std::vector<std::vector<int_t>> peribdry_node_to_matching;
	communicatePeribdryNode(peribdry_node, peribdry_node_to_matching);
	peribdry_node_ = move(peribdry_node);
	peribdry_node_to_matching_ = move(peribdry_node_to_matching);

	peribdry_to_matching_node_ind_.resize(peribdry_to_node_ind_.size());
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++){
		const int_t& start_index = peribdry_to_node_ptr_[ibdry];
		const int_t& end_index = peribdry_to_node_ptr_[ibdry + 1];
		const int_t& direction = peribdry_direction_[ibdry];
		for (int_t inode = start_index; inode < end_index; inode++)
			for (int_t jnode = 0; jnode < peribdry_node_[direction].size(); jnode++)
				if (peribdry_node_[direction][jnode] == peribdry_to_node_ind_[inode])
					peribdry_to_matching_node_ind_[inode] = peribdry_node_to_matching_[direction][jnode];
	}

	std::vector<bool> peribdry_neighbor_flag(num_total_peribdries_, true);
	peribdry_neighbor_cell_.reserve(num_total_peribdries_);
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++){
		const int_t& start_index = peribdry_to_node_ptr_[ibdry];
		const int_t& end_index = peribdry_to_node_ptr_[ibdry + 1];
		auto&& iterator = node_to_cell.find(peribdry_to_matching_node_ind_[start_index]);

		if(iterator == node_to_cell.end()){
			peribdry_neighbor_flag[ibdry] = false;
			continue;
		}
		
		bool is_exist = false;
		for (auto&& icell : iterator->second){
			if (isParent(&cell_to_node_ind_[cell_to_node_ptr_[icell]], cell_to_node_ptr_[icell + 1] - cell_to_node_ptr_[icell], &peribdry_to_matching_node_ind_[start_index], end_index - start_index)){
				peribdry_neighbor_cell_.push_back(icell);
				is_exist = true;
				break;
			}
		}
		if (is_exist == false)
			peribdry_neighbor_flag[ibdry] = false;
	}
	
	// removing periodic boundaries that do not have neighboring cell
	const int_t old_num_total_peribdries = num_total_peribdries_;
	const int_t old_num_peribdries = num_peribdries_;
	peribdry_to_node_ptr.clear();
	peribdry_to_node_ind.clear();
	peribdry_direction.clear();
	peribdry_color.clear();
	peribdry_owner_cell.clear();

	peribdry_to_node_ptr.reserve(peribdry_to_node_ptr_.size());
	peribdry_to_node_ind.reserve(peribdry_to_node_ind_.size());
	peribdry_direction.reserve(peribdry_direction_.size());
	peribdry_color.reserve(peribdry_color_.size());
	peribdry_owner_cell.reserve(peribdry_owner_cell_.size());

	peribdry_to_node_ptr.push_back(0);
	for (int_t ibdry = 0; ibdry < old_num_total_peribdries; ibdry++){
		if (peribdry_neighbor_flag[ibdry] == true){
			peribdry_to_node_ptr.push_back(peribdry_to_node_ptr.back() + peribdry_to_node_ptr_[ibdry + 1] - peribdry_to_node_ptr_[ibdry]);
			for (int_t inode = peribdry_to_node_ptr_[ibdry]; inode < peribdry_to_node_ptr_[ibdry + 1]; inode++)
				peribdry_to_node_ind.push_back(peribdry_to_node_ind_[inode]);
			peribdry_direction.push_back(peribdry_direction_[ibdry]);
			peribdry_color.push_back(peribdry_color_[ibdry]);
			peribdry_owner_cell.push_back(peribdry_owner_cell_[ibdry]);
		}
		else{
			if (ibdry < old_num_peribdries) num_peribdries_--;
			num_total_peribdries_--;
		}
	}

	peribdry_to_node_ptr_ = move(peribdry_to_node_ptr);
	peribdry_to_node_ind_ = move(peribdry_to_node_ind);
	peribdry_direction_ = move(peribdry_direction);
	peribdry_color_ = move(peribdry_color);
	peribdry_owner_cell_ = move(peribdry_owner_cell);
	MASTER_MESSAGE("Redistribute boundary data(Time: " + TO_STRING(STOP()) + "s)");
}

void GridBuilder::redistributeNodeData(void)
{
	START();

	int_t myrank = MYRANK();
	int_t ndomain = NDOMAIN();

	int_t num_alloc_nodes = node_dist_[1] - node_dist_[0];
	std::unordered_map<int_t, int_t> node_to_processor;
	for (auto&& inode : cell_to_subnode_ind_){
		int_t idomain = inode / num_alloc_nodes;
		node_to_processor[inode] = idomain;
	}

	std::vector<int_t> num_nodes_send(ndomain, 0);
	std::vector<std::vector<int_t>> node_list_send(ndomain);

	for (auto&& iterator : node_to_processor)
		num_nodes_send[iterator.second]++;

	for (int_t idomain = 0; idomain < ndomain; idomain++)
		node_list_send[idomain].reserve(num_nodes_send[idomain]);
	for (auto&& iterator : node_to_processor)
		node_list_send[iterator.second].push_back(iterator.first);

	std::vector<MPI_Request> recv_req(ndomain);
	std::vector<MPI_Status> recv_sta(ndomain);
	std::vector<MPI_Request> send_req(ndomain);
	std::vector<MPI_Status> send_sta(ndomain);

	std::vector<int_t> num_nodes_recv(ndomain, 0);
	std::vector<std::vector<int_t>> node_list_recv(ndomain);

	MPI_Alltoall(&num_nodes_send[0], 1, MPI_INT_T, &num_nodes_recv[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	std::vector<int_t> trash_send(ndomain, 0);
	std::vector<int_t> trash_recv(ndomain, 0);

	for (int_t idomain = 0; idomain < ndomain; idomain++)
		node_list_recv[idomain].resize(num_nodes_recv[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_nodes_recv[idomain]>0)
			MPI_Irecv(&node_list_recv[idomain][0], num_nodes_recv[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_nodes_send[idomain]>0)
			MPI_Isend(&node_list_send[idomain][0], num_nodes_send[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	std::vector<std::vector<real_t>> node_coords_send(ndomain);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		node_coords_send[idomain].reserve(dimension_*num_nodes_recv[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
	for (auto&& inode : node_list_recv[idomain])
	for (int_t idimension = 0; idimension < dimension_; idimension++)
		node_coords_send[idomain].push_back(node_coords_[dimension_*(inode - node_dist_[myrank]) + idimension]);

	std::vector<std::vector<real_t>> node_coords_recv(ndomain);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		node_coords_recv[idomain].resize(dimension_*num_nodes_send[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_nodes_send[idomain]>0)
			MPI_Irecv(&node_coords_recv[idomain][0], dimension_*num_nodes_send[idomain], MPI_REAL_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_nodes_recv[idomain]>0)
			MPI_Isend(&node_coords_send[idomain][0], dimension_*num_nodes_recv[idomain], MPI_REAL_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	int_t num_total_nodes_recv = 0;
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		num_total_nodes_recv += num_nodes_send[idomain];
	node_coords_.clear();
	node_coords_.reserve(dimension_*num_total_nodes_recv);
	node_global_index_.reserve(num_total_nodes_recv);
	std::unordered_map<int_t, int_t> global_node_to_local_node;
	int_t index = 0;
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		for (int_t inode = 0; inode < num_nodes_send[idomain]; inode++){
			for (int_t idim = 0; idim < dimension_; idim++){
				node_coords_.push_back(node_coords_recv[idomain][dimension_*inode + idim]);
			}
			node_global_index_.push_back(node_list_send[idomain][inode]);
			global_node_to_local_node[node_list_send[idomain][inode]] = index++;
		}
	}

	for (auto&& inode : cell_to_node_ind_)
		inode = global_node_to_local_node[inode];

	for (auto&& inode : cell_to_subnode_ind_)
		inode = global_node_to_local_node[inode];

	for (int_t idirection = 0; idirection < 3; idirection++)
	for (auto&& inode : peribdry_node_[idirection])
		inode = global_node_to_local_node[inode];
	for (int_t idirection = 0; idirection < 3; idirection++)
	for (auto&& inode : peribdry_node_to_matching_[idirection])
		inode = global_node_to_local_node[inode];
	
	for (auto&& inode : bdry_to_node_ind_)
		inode = global_node_to_local_node[inode];
	for (auto&& inode : peribdry_to_node_ind_)
		inode = global_node_to_local_node[inode];
	for (auto&& inode : peribdry_to_matching_node_ind_)
		inode = global_node_to_local_node[inode];

	num_total_nodes_ = num_total_nodes_recv;

	// renumbering node index by color
	std::vector<int_t> old_to_new_index(num_total_nodes_, -1);
	index = 0;
	for (int_t icell = 0; icell < num_cells_; icell++){
		const int_t& start_index = cell_to_node_ptr_[icell];
		const int_t& end_index = cell_to_node_ptr_[icell + 1];
		for (int_t inode = start_index; inode < end_index; inode++){
			const int_t& node_index = cell_to_node_ind_[inode];
			if (old_to_new_index[node_index] == -1){
				old_to_new_index[node_index] = index++;
			}
		}
	}
	num_nodes_ = index;
	for (auto&& inode : cell_to_node_ind_)
	if (old_to_new_index[inode] == -1)
		old_to_new_index[inode] = index++;
	for (auto&& inode : cell_to_subnode_ind_)
	if (old_to_new_index[inode] == -1)
		old_to_new_index[inode] = index++;

	std::vector<int_t> new_to_old_index(num_total_nodes_);
	for (int_t inode = 0; inode < num_total_nodes_; inode++)
		new_to_old_index[old_to_new_index[inode]] = inode;

	std::vector<real_t> node_coords(num_total_nodes_*dimension_);
	for (int_t inode = 0; inode < num_total_nodes_; inode++)
	for (int_t idim = 0; idim < dimension_; idim++)
		node_coords[inode*dimension_ + idim] = node_coords_[new_to_old_index[inode] * dimension_ + idim];
	node_coords_ = move(node_coords);

	for (int_t idirection = 0; idirection < 3; idirection++)
	for (auto&& inode : peribdry_node_[idirection])
		inode = old_to_new_index[inode];
	for (int_t idirection = 0; idirection < 3; idirection++)
	for (auto&& inode : peribdry_node_to_matching_[idirection])
		inode = old_to_new_index[inode];

	std::vector<int_t> node_global_index(num_total_nodes_);
	for (int_t inode = 0; inode < num_total_nodes_; inode++)
		node_global_index[inode] = node_global_index_[new_to_old_index[inode]];
	node_global_index_ = move(node_global_index);

	for (auto&& inode : cell_to_node_ind_)
		inode = old_to_new_index[inode];
	for (auto&& inode : cell_to_subnode_ind_)
		inode = old_to_new_index[inode];

	for (auto&& inode : bdry_to_node_ind_)
		inode = old_to_new_index[inode];
	for (auto&& inode : peribdry_to_node_ind_)
		inode = old_to_new_index[inode];
	for (auto&& inode : peribdry_to_matching_node_ind_)
		inode = old_to_new_index[inode];

	MASTER_MESSAGE("Redistribute node data(Time: " + TO_STRING(STOP()) + "s)");
}

void GridBuilder::constructFaceData(void)
{
	START();

	int_t myrank = MYRANK();
	int_t ndomain = NDOMAIN();

	int_t num_estimate_faces = num_total_cells_ * 2 * dimension_;

	std::vector<int_t> first_face(num_total_nodes_, -1);
	std::vector<int_t> next_face(num_estimate_faces, -1);

	std::vector<int_t> face_to_node_ptr;
	std::vector<int_t> face_to_node_ind;
	std::vector<int_t> face_owner_cell;
	std::vector<int_t> face_neighbor_cell;
	face_to_node_ptr.reserve(num_estimate_faces);
	face_to_node_ptr.push_back(0);
	face_to_node_ind.reserve(num_estimate_faces*dimension_);
	face_owner_cell.reserve(num_estimate_faces);
	face_neighbor_cell.reserve(num_estimate_faces);

	for (int_t icell = 0; icell < num_total_cells_; icell++){
		Element* element = ElementFactory::getInstance().getElement(static_cast<ElemType>(cell_elem_type_[icell]));
		const std::vector<std::vector<int_t>>& local_faces = element->getFaceNodes();
		for (auto&& local_face : local_faces){
			std::vector<int_t> nodes_index;
			for (auto&& inode : local_face)
				nodes_index.push_back(cell_to_node_ind_[cell_to_node_ptr_[icell] + inode]);
			int_t minnode = *min_element(nodes_index.begin(), nodes_index.end());
			int_t current_face = first_face[minnode];
			int_t previous_face = -1;
			while (true){
				if (current_face == -1){
					face_owner_cell.push_back(icell);
					face_neighbor_cell.resize(face_owner_cell.size(), -1);
					for (auto&& inode : nodes_index)
						face_to_node_ind.push_back(inode);
					face_to_node_ptr.push_back(face_to_node_ind.size());
					if (previous_face == -1) first_face[minnode] = face_owner_cell.size() - 1;
					else next_face[previous_face] = face_owner_cell.size() - 1;
					break;
				}
				else if (isParent(&face_to_node_ind[face_to_node_ptr[current_face]], face_to_node_ptr[current_face + 1] - face_to_node_ptr[current_face],&nodes_index[0],nodes_index.size())){
					face_neighbor_cell[current_face] = icell;
					break;
				}
				previous_face = current_face;
				current_face = next_face[current_face];
			}
		}
	}

	first_face.clear();
	next_face.clear();

	face_to_node_ptr_ = move(face_to_node_ptr);
	face_to_node_ind_ = move(face_to_node_ind);
	face_owner_cell_ = move(face_owner_cell);
	face_neighbor_cell_ = move(face_neighbor_cell);

	// remove duplicated face with boundary
	face_to_node_ptr.reserve(face_to_node_ptr_.size());
	face_to_node_ind.reserve(face_to_node_ind_.size());
	face_owner_cell.reserve(face_owner_cell_.size());
	face_neighbor_cell.reserve(face_neighbor_cell_.size());

	face_to_node_ptr.push_back(0);
	for (int_t iface = 0; iface < face_owner_cell_.size(); iface++){
		if (face_neighbor_cell_[iface] == -1) continue;
		for (int_t inode = face_to_node_ptr_[iface]; inode < face_to_node_ptr_[iface + 1]; inode++)
			face_to_node_ind.push_back(face_to_node_ind_[inode]);
		face_to_node_ptr.push_back(face_to_node_ind.size());
		face_owner_cell.push_back(face_owner_cell_[iface]);
		face_neighbor_cell.push_back(face_neighbor_cell_[iface]);
	}

	face_to_node_ptr_ = move(face_to_node_ptr);
	face_to_node_ind_ = move(face_to_node_ind);
	face_owner_cell_ = move(face_owner_cell);
	face_neighbor_cell_ = move(face_neighbor_cell);

	num_total_faces_ = face_owner_cell_.size();
	for (num_faces_ = 0; num_faces_ < num_total_faces_; num_faces_++){
		if (cell_color_[face_owner_cell_[num_faces_]] != myrank){
			break;
		}
	}

	// set face type
	face_owner_type_.resize(num_total_faces_);
	for (int_t iface = 0; iface < num_total_faces_; iface++){
		const int_t& icell = face_owner_cell_[iface];
		Element* element = ElementFactory::getInstance().getElement(static_cast<ElemType>(cell_elem_type_[icell]));
		face_owner_type_[iface] = findFaceType(element->getFacetypeNodes(1), &cell_to_node_ind_[cell_to_node_ptr_[icell]], &face_to_node_ind_[face_to_node_ptr_[iface]], face_to_node_ptr_[iface + 1] - face_to_node_ptr_[iface]);
	}

	face_neighbor_type_.resize(num_total_faces_);
	for (int_t iface = 0; iface < num_total_faces_; iface++){
		const int_t& icell = face_neighbor_cell_[iface];
		Element* element = ElementFactory::getInstance().getElement(static_cast<ElemType>(cell_elem_type_[icell]));
		face_neighbor_type_[iface] = findFaceType(element->getFacetypeNodes(1), &cell_to_node_ind_[cell_to_node_ptr_[icell]], &face_to_node_ind_[face_to_node_ptr_[iface]], face_to_node_ptr_[iface + 1] - face_to_node_ptr_[iface]);
	}

	bdry_owner_type_.resize(num_total_bdries_);
	for (int_t ibdry = 0; ibdry < num_total_bdries_; ibdry++){
		const int_t& icell = bdry_owner_cell_[ibdry];
		Element* element = ElementFactory::getInstance().getElement(static_cast<ElemType>(cell_elem_type_[icell]));
		bdry_owner_type_[ibdry] = findFaceType(element->getFacetypeNodes(1), &cell_to_node_ind_[cell_to_node_ptr_[icell]], &bdry_to_node_ind_[bdry_to_node_ptr_[ibdry]], bdry_to_node_ptr_[ibdry + 1] - bdry_to_node_ptr_[ibdry]);
	}

	peribdry_owner_type_.resize(num_total_peribdries_);
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++){
		const int_t& icell = peribdry_owner_cell_[ibdry];
		Element* element = ElementFactory::getInstance().getElement(static_cast<ElemType>(cell_elem_type_[icell]));
		peribdry_owner_type_[ibdry] = findFaceType(element->getFacetypeNodes(1), &cell_to_node_ind_[cell_to_node_ptr_[icell]], &peribdry_to_node_ind_[peribdry_to_node_ptr_[ibdry]], peribdry_to_node_ptr_[ibdry + 1] - peribdry_to_node_ptr_[ibdry]);
	}

	peribdry_neighbor_type_.resize(num_total_peribdries_);
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++){
		const int_t& icell = peribdry_neighbor_cell_[ibdry];
		Element* element = ElementFactory::getInstance().getElement(static_cast<ElemType>(cell_elem_type_[icell]));
		peribdry_neighbor_type_[ibdry] = findFaceType(element->getFacetypeNodes(1), &cell_to_node_ind_[cell_to_node_ptr_[icell]], &peribdry_to_matching_node_ind_[peribdry_to_node_ptr_[ibdry]], peribdry_to_node_ptr_[ibdry + 1] - peribdry_to_node_ptr_[ibdry]);
	}
	
	MASTER_MESSAGE("Construct face data(Time: " + TO_STRING(STOP()) + "s)");
}

void GridBuilder::constructNodeToCellsData(void)
{
	START();

	std::vector<std::vector<int_t>> node_to_cells(num_total_nodes_);
	std::vector<std::vector<int_t>> node_to_vertex(num_total_nodes_);
	for (int_t icell = 0; icell < num_total_cells_;icell++){
		const int_t& start_index = cell_to_node_ptr_[icell];
		const int_t& end_index = cell_to_node_ptr_[icell+1];
		for (int_t inode = start_index; inode < end_index; inode++){
			node_to_cells[cell_to_node_ind_[inode]].push_back(icell);
			node_to_vertex[cell_to_node_ind_[inode]].push_back(inode - start_index);
		}
	}
	for (int_t idirection = 0; idirection < 4; idirection++){
		std::vector<bool> done_flag(num_total_nodes_, false);
		for (int_t inode = 0; inode < peribdry_node_[idirection].size(); inode++){
			if (done_flag[peribdry_node_[idirection][inode]] == true) continue;

			const int_t sz = node_to_cells[peribdry_node_[idirection][inode]].size();
			for (int_t icell = 0; icell < node_to_cells[peribdry_node_to_matching_[idirection][inode]].size(); icell++){
				node_to_cells[peribdry_node_[idirection][inode]].push_back(node_to_cells[peribdry_node_to_matching_[idirection][inode]][icell]);
				node_to_vertex[peribdry_node_[idirection][inode]].push_back(node_to_vertex[peribdry_node_to_matching_[idirection][inode]][icell]);
			}

			for (int_t icell = 0; icell < sz; icell++){
				node_to_cells[peribdry_node_to_matching_[idirection][inode]].push_back(node_to_cells[peribdry_node_[idirection][inode]][icell]);
				node_to_vertex[peribdry_node_to_matching_[idirection][inode]].push_back(node_to_vertex[peribdry_node_[idirection][inode]][icell]);
			}

			done_flag[peribdry_node_[idirection][inode]] = true;
			done_flag[peribdry_node_to_matching_[idirection][inode]] = true;
		}
	}
	
	node_to_cells_ = move(node_to_cells);
	node_to_vertex_ = move(node_to_vertex);

	std::vector<std::vector<int_t>> cell_to_cells(num_total_cells_);
	for (int_t iface = 0; iface < num_faces_; iface++){
		cell_to_cells[face_owner_cell_[iface]].push_back(face_neighbor_cell_[iface]);
		cell_to_cells[face_neighbor_cell_[iface]].push_back(face_owner_cell_[iface]);
	}
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++)
		cell_to_cells[peribdry_owner_cell_[ibdry]].push_back(peribdry_neighbor_cell_[ibdry]);
	cell_to_cells_ = move(cell_to_cells);

	std::vector<std::vector<int_t>> cell_to_faces(num_total_cells_);
	for (int_t iface = 0; iface < num_faces_; iface++){
		cell_to_faces[face_owner_cell_[iface]].push_back(iface);
		cell_to_faces[face_owner_cell_[iface]].push_back(1);
		cell_to_faces[face_neighbor_cell_[iface]].push_back(iface);
		cell_to_faces[face_neighbor_cell_[iface]].push_back(1);
	}
	for (int_t ibdry = 0; ibdry < num_total_peribdries_; ibdry++){
		cell_to_faces[peribdry_owner_cell_[ibdry]].push_back(ibdry);
		cell_to_faces[peribdry_owner_cell_[ibdry]].push_back(-1);
	}
	cell_to_faces_ = move(cell_to_faces);

	MASTER_MESSAGE("construct node to cells data(Time: " + TO_STRING(STOP()) + "s)");
}

bool GridBuilder::isMatch(const real_t* coords1, const real_t* coords2, const int_t direction)
{
	for (int_t idimension = 0; idimension < dimension_; idimension++){
		if (idimension == direction) continue;
		if (std::abs(*(coords1 + idimension) - *(coords2 + idimension)) > 1E-10)
			return false;
	}
	return true;
}

void GridBuilder::communicatePeribdryNode(std::vector<std::vector<int_t>>& peribdry_node, std::vector<std::vector<int_t>>& peribdry_node_to_matching)
{
	int_t myrank = MYRANK();
	int_t ndomain = NDOMAIN();

	int_t num_alloc_nodes = node_dist_[1] - node_dist_[0];
	std::unordered_map<int_t, int_t> node_to_processor;
	for (auto&& inode : cell_to_node_ind_){
		int_t idomain = inode / num_alloc_nodes;
		node_to_processor[inode] = idomain;
	}

	std::vector<int_t> num_nodes_send(ndomain, 0);
	std::vector<std::vector<int_t>> node_list_send(ndomain);

	for (auto&& iterator : node_to_processor)
		num_nodes_send[iterator.second]++;

	for (int_t idomain = 0; idomain < ndomain; idomain++)
		node_list_send[idomain].reserve(num_nodes_send[idomain]);
	for (auto&& iterator : node_to_processor)
		node_list_send[iterator.second].push_back(iterator.first);

	std::vector<MPI_Request> recv_req(ndomain);
	std::vector<MPI_Status> recv_sta(ndomain);
	std::vector<MPI_Request> send_req(ndomain);
	std::vector<MPI_Status> send_sta(ndomain);

	std::vector<int_t> num_nodes_recv(ndomain, 0);
	std::vector<std::vector<int_t>> node_list_recv(ndomain);

	MPI_Alltoall(&num_nodes_send[0], 1, MPI_INT_T, &num_nodes_recv[0], 1, MPI_INT_T, MPI_COMM_WORLD);

	std::vector<int_t> trash_send(ndomain, 0);
	std::vector<int_t> trash_recv(ndomain, 0);
	for (int_t idomain = 0; idomain < ndomain; idomain++)
		node_list_recv[idomain].resize(num_nodes_recv[idomain]);
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_nodes_recv[idomain]>0)
			MPI_Irecv(&node_list_recv[idomain][0], num_nodes_recv[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain < ndomain; idomain++){
		if (num_nodes_send[idomain]>0)
			MPI_Isend(&node_list_send[idomain][0], num_nodes_send[idomain], MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

	std::vector<std::vector<int_t>> peribdry_node_temp(4);
	std::vector<std::vector<int_t>> peribdry_node_to_matching_temp(4);
	for (int_t idirection = 0; idirection < 4; idirection++){
		std::vector<int_t> num_matching_nodes_send(ndomain, 0);
		std::vector<std::vector<int_t>> matching_node_list_send(ndomain);
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			for (int_t inode = 0; inode < num_nodes_recv[idomain]; inode++){
				for (int_t jnode = 0; jnode < peribdry_node_[idirection].size(); jnode++){
					if (node_list_recv[idomain][inode] == peribdry_node_[idirection][jnode]){
						num_matching_nodes_send[idomain]++;
						matching_node_list_send[idomain].push_back(peribdry_node_[idirection][jnode]);
						matching_node_list_send[idomain].push_back(peribdry_node_to_matching_[idirection][jnode]);
					}
				}
			}
		}

		std::vector<int_t> num_matching_nodes_recv(ndomain, 0);
		std::vector<std::vector<int_t>> matching_node_list_recv(ndomain);

		MPI_Alltoall(&num_matching_nodes_send[0], 1, MPI_INT_T, &num_matching_nodes_recv[0], 1, MPI_INT_T, MPI_COMM_WORLD);

		for (int_t idomain = 0; idomain < ndomain; idomain++)
			matching_node_list_recv[idomain].resize(num_matching_nodes_recv[idomain] * 2);
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			if (num_matching_nodes_recv[idomain]>0)
				MPI_Irecv(&matching_node_list_recv[idomain][0], num_matching_nodes_recv[idomain] * 2, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
			else
				MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		}
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			if(num_matching_nodes_send[idomain]>0)
				MPI_Isend(&matching_node_list_send[idomain][0], num_matching_nodes_send[idomain] * 2, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
			else
				MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
		}
		MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
		MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

		std::vector<int_t> peribdry_node_direction;
		std::vector<int_t> peribdry_node_to_matching_direction;

		int_t num_peribdry_node = 0;
		for (int_t idomain = 0; idomain < ndomain; idomain++)
			num_peribdry_node += num_matching_nodes_recv[idomain];
		peribdry_node_direction.reserve(num_peribdry_node);
		peribdry_node_to_matching_direction.reserve(num_peribdry_node);
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			for (int_t inode = 0; inode < num_matching_nodes_recv[idomain]; inode++){
				peribdry_node_direction.push_back(matching_node_list_recv[idomain][inode * 2]);
				peribdry_node_to_matching_direction.push_back(matching_node_list_recv[idomain][inode * 2 + 1]);
			}
		}

		peribdry_node_temp[idirection] = move(peribdry_node_direction);
		peribdry_node_to_matching_temp[idirection] = move(peribdry_node_to_matching_direction);
	}

	peribdry_node = move(peribdry_node_temp);
	peribdry_node_to_matching = move(peribdry_node_to_matching_temp);
}

bool GridBuilder::isParent(const int_t* cell_node_index, const int_t cell_num_nodes, const int_t* face_node_index, const int_t face_num_nodes)
{
	for (int_t inode = 0; inode < face_num_nodes; inode++){
		bool match_flag = false;
		for (int_t jnode = 0; jnode < cell_num_nodes; jnode++){
			if (*(cell_node_index + jnode) == *(face_node_index + inode)){
				match_flag = true;
				break;
			}
		}
		if (match_flag == false){
			return false;
		}
	}
	return true;
}

bool GridBuilder::isSame(const int_t* index1, const int_t num_index1, const int_t* index2, const int_t num_index2)
{
	if (num_index1 != num_index2) return false;

	for (int_t i = 0; i < num_index1;i++)
	if (index1[i] != index2[i])
		return false;
	return true;
}

int_t GridBuilder::findFaceType(const std::vector<std::vector<int_t>>& face_type_nodes_index, const int_t* cell_nodes_index, const int_t* face_nodes_index, const int_t num_face_nodes)
{
	for (int_t itype = 0; itype < face_type_nodes_index.size(); itype++){
		std::vector<int_t> facetype_nodes_index;
		for (auto&& inode : face_type_nodes_index[itype])
			facetype_nodes_index.push_back(*(cell_nodes_index + inode));
		if (isSame(&facetype_nodes_index[0], facetype_nodes_index.size(), face_nodes_index, num_face_nodes))
			return itype;
	}
	return -1;
}

void GridBuilder::test(void)
{
	int_t myrank = MYRANK();
	int_t ndomain = NDOMAIN();

	char tag_name[10]; sprintf(tag_name, "%d", MYRANK());
	const std::string title = "test";
	std::string filename = title + tag_name + ".plt";
	std::ofstream outfile(filename);

	if (dimension_ == 2){
		outfile << "TITLE = \"field \"" << std::endl;
		outfile << "VARIABLES = \"x\",\"y\",\"color\"" << std::endl;
		outfile << "ZONE T=\"ZONE " << MYRANK() << "\"" << std::endl;
		outfile << "n=" << num_total_nodes_ << " ,e=" << num_total_cells_ << " ,f=fepoint, et=triangle" << std::endl;
	}
	else if (dimension_ == 3){
		outfile << "TITLE = \"field \"" << std::endl;
		outfile << "VARIABLES = \"x\",\"y\",\"z\",\"color\"" << std::endl;
		outfile << "ZONE T=\"ZONE " << MYRANK() << "\"" << std::endl;
		outfile << "n=" << num_total_nodes_ << " ,e=" << num_total_cells_ << " ,f=fepoint, et=tetrahedron" << std::endl;
	}

	for (int_t inode = 0; inode < num_total_nodes_; inode++){
		for (int_t idim = 0; idim < dimension_; idim++){
			outfile << node_coords_[inode*dimension_ + idim] << "\t";
		}
		outfile << myrank << std::endl;
	}
	
	for (int_t icell = 0; icell < num_total_cells_; icell++){
		const int_t& start_index = cell_to_node_ptr_[icell];
		const int_t& end_index = cell_to_node_ptr_[icell+1];
		for (int_t inode = start_index; inode < end_index; inode++){
			outfile << cell_to_node_ind_[inode] + 1 << "\t";
		}
		outfile << std::endl;
	}

	outfile.close();
}

void GridBuilder::petscReordering(void)
{
	const int_t num_domain = NDOMAIN();
	const int_t rank = MYRANK();

	// Reordering (local Cuthill-McKee ordering)
	START();
	cell_petsc_index_.clear();
	cell_petsc_index_.resize(num_total_cells_, 0);

	/// Finding interior cells
	std::vector<bool> interior_flag(num_total_cells_, true);
	for (int_t icell = 0; icell < num_cells_; icell++)
		for (int_t jcell = 0; jcell < cell_to_cells_[icell].size(); jcell++)
			if (cell_to_cells_[icell][jcell] >= num_cells_) interior_flag[icell] = false;
	for (int_t icell = num_cells_; icell < num_total_cells_; icell++)
		interior_flag[icell] = false;
	int_t num_interior_cell = std::count(interior_flag.begin(), interior_flag.end(), true);

	/// Finding initial cell
	int_t initial_cell = -1;
	for (int_t iflag = 0; iflag < num_total_cells_; iflag++)
		if (interior_flag[iflag] == true)
		{
			initial_cell = iflag;
			break;
		}

	/// Interior cell Cuthill-McKee ordering
	int_t max_index = -1;
	if (initial_cell != -1)
	{
		std::vector<int_t> levset = { initial_cell }; int_t next = 1;
		std::vector<int_t> next_levset;
		std::vector<bool> marker(num_total_cells_, false);
		marker[initial_cell] = true; cell_petsc_index_[initial_cell] = 0;
		while (next < num_interior_cell)
		{
			next_levset.clear();
			for (int_t j = 0; j < levset.size(); j++)
				for (int_t i = 0; i < cell_to_cells_[levset[j]].size(); i++)
					if ((marker[cell_to_cells_[levset[j]][i]] == false) && (interior_flag[cell_to_cells_[levset[j]][i]] == true))
					{
						next_levset.push_back(cell_to_cells_[levset[j]][i]);
						marker[cell_to_cells_[levset[j]][i]] = true;
						cell_petsc_index_[cell_to_cells_[levset[j]][i]] = next++;
					}
			if (next_levset.size() == 0)
				for (int_t iflag = 0; iflag < num_total_cells_; iflag++)
				{
					if ((interior_flag[iflag] == true) && (marker[iflag] == false))
					{
						levset = { iflag };
						marker[iflag] = true;
						cell_petsc_index_[iflag] = next++;
						break;
					}
				}
			else levset = next_levset;
		}
		/// Interior cell Reverse Cuthill-McKee ordering
		max_index = *std::max_element(cell_petsc_index_.begin(), cell_petsc_index_.end());
		for (int_t icell = 0; icell < num_cells_; icell++)
			cell_petsc_index_[icell] = max_index - cell_petsc_index_[icell];
	}

	/// Interface cell ordering
	std::vector<int_t> mean_label(num_cells_, -1);
	for (int_t i = 0; i < num_cells_; i++)
		for (int_t j = 0; j < cell_to_cells_[i].size(); j++)
		{
			if (cell_to_cells_[i][j] >= num_cells_)
				mean_label[i] += cell_color_[cell_to_cells_[i][j]] + 1;
		}			
	int_t index = max_index + 1;
	for (int_t i = 0; i <= *std::max_element(mean_label.begin(), mean_label.end()); i++)
		for (int_t j = 0; j < mean_label.size(); j++)
		{
			if (i == mean_label[j])
				cell_petsc_index_[j] = index++;
		}			

	// Adding cell distance
	std::vector<int_t> num_internal_cells(num_domain);
	std::vector<int_t> num_internal_cells_recv_buff(num_domain);
	num_internal_cells[rank] = num_cells_;
	MPI_Allgather(&num_internal_cells[rank], 1, MPI_INT, &num_internal_cells_recv_buff[0], 1, MPI_INT, MPI_COMM_WORLD);
	num_internal_cells = std::move(num_internal_cells_recv_buff);
	int_t cell_petsc_dist = 0;
	for (int_t idomain = 0; idomain < num_domain; idomain++)
		if (idomain < rank)
			cell_petsc_dist += num_internal_cells[idomain];
	for (int_t icell = 0; icell < num_cells_; icell++)
		cell_petsc_index_[icell] += cell_petsc_dist;

	// Global to petsc index
	std::vector<int_t> cell_global_to_petsc_pair(2 * num_global_cells_, 0);
	std::vector<int_t> petsc_pair_recv_buff(2 * num_global_cells_, 0);
	std::vector<int_t> cell_distance(num_domain + 1, 0);
	for (int_t idomain = 0; idomain < num_domain; idomain++)
		cell_distance[idomain + 1] = cell_distance[idomain] + num_internal_cells[idomain];
	for (int_t icell = 0; icell < num_cells_; icell++)
	{
		cell_global_to_petsc_pair[2 * icell + 2 * cell_distance[rank]] = cell_global_index_[icell];
		cell_global_to_petsc_pair[2 * icell + 2 * cell_distance[rank] + 1] = cell_petsc_index_[icell];
	}

	std::vector<int_t> recvcounts(num_domain);
	std::vector<int_t> displs(num_domain);
	for (int_t idomain = 0; idomain < num_domain; idomain++)
	{
		recvcounts[idomain] = 2 * num_internal_cells[idomain];
		displs[idomain] = 2 * cell_distance[idomain];
	}
	MPI_Allgatherv(&cell_global_to_petsc_pair[2 * cell_distance[rank]], 2 * num_cells_, MPI_INT, &petsc_pair_recv_buff[0], &recvcounts[0], &displs[0], MPI_INT, MPI_COMM_WORLD);
	cell_global_to_petsc_pair = std::move(petsc_pair_recv_buff);

	std::vector<int_t> cell_global_to_petsc_index(num_global_cells_, 0);
	for (int_t icell = 0; icell < num_global_cells_; icell++)
		cell_global_to_petsc_index[cell_global_to_petsc_pair[2 * icell]] = cell_global_to_petsc_pair[2 * icell + 1];

	// External cell petsc index
	for (int_t icell = num_cells_; icell < num_total_cells_; icell++)
		cell_petsc_index_[icell] = cell_global_to_petsc_index[cell_global_index_[icell]];
	MASTER_MESSAGE("Petsc reordering(Time: " + TO_STRING(STOP()) + "s)");
}

void GridBuilder::petscReordering2(void)
{
	const int_t num_domain = NDOMAIN();
	const int_t rank = MYRANK();
	
	// Make domain to domain data
	START();
	std::vector<std::vector<int_t> > domain_to_domain(num_domain);
	std::vector<bool> flag(num_domain, false);
	for (int_t icell = num_cells_; icell < num_total_cells_; icell++)
		flag[cell_color_[icell]] = true;

	for (int_t idomain = 0; idomain < num_domain; idomain++)
		if (flag[idomain] == true)
			domain_to_domain[rank].push_back(idomain);

	std::vector<int_t> num_connected_domain(num_domain, 0);
	std::vector<int_t> num_connected_domain_recv_buff(num_domain, 0);
	num_connected_domain[rank] = domain_to_domain[rank].size();
	MPI_Allgather(&num_connected_domain[rank], 1, MPI_INT, &num_connected_domain_recv_buff[0], 1, MPI_INT, MPI_COMM_WORLD);
	num_connected_domain = std::move(num_connected_domain_recv_buff);

	std::vector<MPI_Request> send_req(num_domain), recv_req(num_domain);
	std::vector<MPI_Status> send_stat(num_domain), recv_stat(num_domain);
	int_t recv_num = 0;
	for (int_t idomain = 0; idomain < num_domain; idomain++)
	{
		if (num_connected_domain[rank] != 0) MPI_Isend(&domain_to_domain[rank][0], num_connected_domain[rank], MPI_INT, idomain, 1, MPI_COMM_WORLD, &send_req[idomain]);
		if (num_connected_domain[idomain] != 0)
		{
			domain_to_domain[idomain].resize(num_connected_domain[idomain]);
			MPI_Irecv(&domain_to_domain[idomain][0], num_connected_domain[idomain], MPI_INT, idomain, 1, MPI_COMM_WORLD, &recv_req[recv_num++]);
		}
	}
	if (num_connected_domain[rank] != 0) MPI_Waitall(num_domain, &send_req[0], &send_stat[0]);
	MPI_Waitall(recv_num, &recv_req[0], &recv_stat[0]);
	
	// Labeling domain (Reverse Cuthill-McKee ordering)
	domain_label_.clear();
	domain_label_.resize(num_domain, 0);
	std::vector<int_t> levset = { 0 }; int_t next = 1;
	std::vector<int_t> next_levset;
	std::vector<bool> marker(num_domain, false);
	marker[0] = true; domain_label_[0] = 0;
	while (next < num_domain)
	{
		next_levset.clear();
		for (int_t j = 0; j < levset.size(); ++j)
			for (int_t i = 0; i < domain_to_domain[levset[j]].size(); ++i)
				if (marker[domain_to_domain[levset[j]][i]] == false)
				{
					next_levset.push_back(domain_to_domain[levset[j]][i]);
					marker[domain_to_domain[levset[j]][i]] = true;
					domain_label_[domain_to_domain[levset[j]][i]] = next++;
				}
		levset = next_levset;
	}
	// Reverse
	for (int_t idomain = 0; idomain < num_domain; ++idomain)
		domain_label_[idomain] = num_domain - 1 - domain_label_[idomain];
	
	// Reordering (local Cuthill-McKee ordering)
	START();
	cell_petsc_index_.clear();
	cell_petsc_index_.resize(num_total_cells_, 0);
	
	/// Finding interior cells
	std::vector<bool> interior_flag(num_total_cells_, true);
	for (int_t icell = 0; icell < num_cells_; icell++)
		for (int_t jcell = 0; jcell < cell_to_cells_[icell].size(); jcell++)
			if (cell_to_cells_[icell][jcell] >= num_cells_) interior_flag[icell] = false;
	for (int_t icell = num_cells_; icell < num_total_cells_; icell++)
		interior_flag[icell] = false;
	int_t num_interior_cell = std::count(interior_flag.begin(), interior_flag.end(), true);
	
	/// Finding initial cell
	int_t initial_cell = -1;
	for (int_t iflag = 0; iflag < num_total_cells_; iflag++)
		if (interior_flag[iflag] == true)
		{
			initial_cell = iflag;
			break;
		}

	/// Interior cell Cuthill-McKee ordering
	int_t max_index = -1;
	if (initial_cell != -1)
	{
		std::vector<int_t> levset = { initial_cell }; int_t next = 1;
		std::vector<int_t> next_levset;
		std::vector<bool> marker(num_total_cells_, false);
		marker[initial_cell] = true; cell_petsc_index_[initial_cell] = 0;
		while (next < num_interior_cell)
		{
			next_levset.clear();
			for (int_t j = 0; j < levset.size(); j++)
				for(int_t i = 0; i < cell_to_cells_[levset[j]].size(); i++)
					if ((marker[cell_to_cells_[levset[j]][i]] == false) && (interior_flag[cell_to_cells_[levset[j]][i]] == true))
					{
						next_levset.push_back(cell_to_cells_[levset[j]][i]);
						marker[cell_to_cells_[levset[j]][i]] = true;
						cell_petsc_index_[cell_to_cells_[levset[j]][i]] = next++;
					}
			if(next_levset.size() == 0)
				for (int_t iflag = 0; iflag < num_total_cells_; iflag++)
				{
					if ((interior_flag[iflag] == true) && (marker[iflag] == false))
					{
						levset = { iflag };
						marker[iflag] = true;
						cell_petsc_index_[iflag] = next++;
						break;
					}
				}
			else levset = next_levset;
		}
		/// Interior cell Reverse Cuthill-McKee ordering
		max_index = *std::max_element(cell_petsc_index_.begin(), cell_petsc_index_.end());
		for (int_t icell = 0; icell < num_cells_; icell++)
			cell_petsc_index_[icell] = max_index - cell_petsc_index_[icell];
	}

	/// Interface cell ordering
	std::vector<int_t> mean_label(num_cells_, -1);
	for (int_t i = 0; i < num_cells_; i++)
		for (int_t j = 0; j < cell_to_cells_[i].size(); j++)
			if (cell_to_cells_[i][j] >= num_cells_)
				mean_label[i] += domain_label_[cell_color_[cell_to_cells_[i][j]]] + 1;
	int_t index = max_index + 1;
	for (int_t i = 0; i <= *std::max_element(mean_label.begin(), mean_label.end()); i++)
		for (int_t j = 0; j < mean_label.size(); j++)
			if (i == mean_label[j])
				cell_petsc_index_[j] = index++;

	// Adding cell distance
	std::vector<int_t> num_internal_cells(num_domain);
	std::vector<int_t> num_internal_cells_recv_buff(num_domain);
	num_internal_cells[rank] = num_cells_;
	MPI_Allgather(&num_internal_cells[rank], 1, MPI_INT, &num_internal_cells_recv_buff[0], 1, MPI_INT, MPI_COMM_WORLD);
	num_internal_cells = std::move(num_internal_cells_recv_buff);

	int_t cell_petsc_dist = 0;
	for (int_t idomain = 0; idomain < num_domain; idomain++)
		if (domain_label_[idomain] < domain_label_[rank])
			cell_petsc_dist += num_internal_cells[idomain];
	for (int_t icell = 0; icell < num_cells_; icell++)
		cell_petsc_index_[icell] += cell_petsc_dist;

	// Global to petsc index
	std::vector<int_t> cell_global_to_petsc_pair(2 * num_global_cells_, 0);
	std::vector<int_t> petsc_pair_recv_buff(2 * num_global_cells_, 0);
	std::vector<int_t> cell_distance(num_domain, 0);
	for (int_t idomain = 0; idomain < num_domain; idomain++)
		cell_distance[idomain + 1] = cell_distance[idomain] + num_internal_cells[idomain];
	for (int_t icell = 0; icell < num_cells_; icell++)
	{
		cell_global_to_petsc_pair[2 * icell + 2 * cell_distance[rank]] = cell_global_index_[icell];
		cell_global_to_petsc_pair[2 * icell + 2 * cell_distance[rank] + 1] = cell_petsc_index_[icell];
	}

	std::vector<int_t> recvcounts(num_domain);
	std::vector<int_t> displs(num_domain);
	for (int_t idomain = 0; idomain < num_domain; idomain++)
	{
		recvcounts[idomain] = 2 * num_internal_cells[idomain];
		displs[idomain] = 2 * cell_distance[idomain];
	}
	MPI_Allgatherv(&cell_global_to_petsc_pair[2 * cell_distance[rank]], 2 * num_cells_, MPI_INT, &petsc_pair_recv_buff[0], &recvcounts[0], &displs[0], MPI_INT, MPI_COMM_WORLD);
	cell_global_to_petsc_pair = std::move(petsc_pair_recv_buff);
	std::vector<int_t> cell_global_to_petsc_index(num_global_cells_, 0);
	for (int_t icell = 0; icell < num_global_cells_; icell++)
		cell_global_to_petsc_index[cell_global_to_petsc_pair[2 * icell]] = cell_global_to_petsc_pair[2 * icell + 1];

	// External cell petsc index
	for (int_t icell = num_cells_; icell < num_total_cells_; icell++)
		cell_petsc_index_[icell] = cell_global_to_petsc_index[cell_global_index_[icell]];
	MASTER_MESSAGE("Petsc reordering(Time: " + TO_STRING(STOP()) + "s)");
}