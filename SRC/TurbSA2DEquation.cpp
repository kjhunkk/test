
#include "../INC/TurbSA2DEquation.h"

void TurbSA2DInitializer::convertPri2Con(std::vector<real_t>& primitive, const int_t start_points, const int_t end_points) const
{
	for (int_t ipoint = start_points; ipoint < end_points; ipoint++) {
		primitive[ipoint * 5 + 1] *= primitive[ipoint * 5];
		primitive[ipoint * 5 + 2] *= primitive[ipoint * 5];
		primitive[ipoint * 5 + 3] = 2.5*primitive[ipoint * 5 + 3] + 0.5*(primitive[ipoint * 5 + 1] * primitive[ipoint * 5 + 1] + primitive[ipoint * 5 + 2] * primitive[ipoint * 5 + 2]) / primitive[ipoint * 5];
		primitive[ipoint * 5 + 4] *= primitive[ipoint * 5];
	}
}

TurbSA2DEquation::TurbSA2DEquation()
{
	num_states_ = 5;
	variable_names_.push_back("rho");
	variable_names_.push_back("rho*U");
	variable_names_.push_back("rho*V");
	variable_names_.push_back("rho*E");
	variable_names_.push_back("rho*nut");
	variable_names_.push_back("pressure");

	setName("TurbSA2D");
	Equation::getInstance().addRegistry(getName(), this);
}
void TurbSA2DEquation::setBoundaries(const std::vector<int_t>& tag_numbers)
{
	int_t num_bdries = tag_numbers.size();
	boundaries_.resize(num_bdries);
	for (int_t ibdry = 0; ibdry < num_bdries; ibdry++) {
		const std::string& type = BoundaryCondition::getInstance().getBCType(tag_numbers[ibdry]);
		const std::string& name = BoundaryCondition::getInstance().getBCName(tag_numbers[ibdry]);
		const std::vector<real_t>& references = BoundaryCondition::getInstance().getBCReferences(tag_numbers[ibdry]);

		boundaries_[ibdry] = BoundaryFactory<TurbSA2DBoundary>::getInstance().getBoundary(type, tag_numbers[ibdry], name, references);
		boundaries_[ibdry]->setFlux(flux_);
	}
}
const std::vector<real_t> TurbSA2DEquation::calPostVariables(const std::vector<real_t>& solutions) const
{
	const int_t num_points = solutions.size() / num_states_;
	const std::vector<real_t> pressure = flux_->calPressure(solutions);
	std::vector<real_t> post_solutions;
	post_solutions.reserve(num_points*(num_states_ + 1));
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		for (int_t istate = 0; istate < num_states_; istate++)
			post_solutions.push_back(solutions[ipoint*num_states_ + istate]);
		post_solutions.push_back(pressure[ipoint]);
	}
	return post_solutions;
}
bool TurbSA2DEquation::applyPressureFix(const std::vector<real_t>& u)
{
	const int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = flux_->calPressure(u);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		if ((pressure[ipoint] < 0.0) || (u[ipoint*num_states_] < 0.0)) // check pressure & density
			return true;
	return false;
}
bool TurbSA2DEquation::isContact(const std::vector<real_t>& u, const std::vector<real_t>& neighbor_u) const
{
	const int_t num_cells = neighbor_u.size() / num_states_;
	const real_t pressure = flux_->calPressure(u)[0];
	const real_t eps = 1.0E-2;
	const std::vector<real_t> neighbor_pressure = flux_->calPressure(neighbor_u);
	for (int_t icell = 0; icell<num_cells; icell++)
		if (std::abs(pressure - neighbor_pressure[icell]) > eps)
			return false;
	return true;
}


TurbSA2DFreeStream::TurbSA2DFreeStream() // time = convergence
{
	readData("EulerEquation", INPUT() + "equation.dat");
	setName("FreeStream");
	TurbSA2DInitializer::getInstance().addRegistry(getName(), this);
}
void TurbSA2DFreeStream::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)) {
		if (text.find(region_start, 0) != std::string::npos) {
			flag = true;
			break;
		}
	}
	if (flag == false) {
		infile.close(); return;
	}

	while (getline(infile, text)) {
		if (text.find("~>Ma_", 0) != std::string::npos)
			infile >> Ma_;
		if (text.find("~>AoA_", 0) != std::string::npos)
			infile >> AoA_;
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}
const std::vector<real_t> TurbSA2DFreeStream::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 5);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		results.push_back(1.0);
		results.push_back(Ma_*std::cos(AoA_ * 3.14159265358979323846264338 / 180.0));
		results.push_back(Ma_*std::sin(AoA_ * 3.14159265358979323846264338 / 180.0));
		results.push_back(1.0 / 1.4);
		results.push_back(4.0);
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
const std::vector<real_t> TurbSA2DFreeStream::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 5);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		results.push_back(1.0);
		results.push_back(Ma_*std::cos(AoA_ * 3.14159265358979323846264338 / 180.0));
		results.push_back(Ma_*std::sin(AoA_ * 3.14159265358979323846264338 / 180.0));
		results.push_back(1.0 / 1.4);
		results.push_back(4.0);
	}
	convertPri2Con(results, 0, num_points);
	return results;
}

TurbSA2DUserDefine::TurbSA2DUserDefine() // time = user
{
	setName("UserDefine");
	TurbSA2DInitializer::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> TurbSA2DUserDefine::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 5);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		results.push_back(1.0);
		results.push_back(1.0);
		results.push_back(0.0);
		results.push_back(1.0 / 1.4);
		results.push_back(4.0);
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
const std::vector<real_t> TurbSA2DUserDefine::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 5);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		results.push_back(1.0);
		results.push_back(1.0);
		results.push_back(0.0);
		results.push_back(1.0 / 1.4);
		results.push_back(4.0);
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
