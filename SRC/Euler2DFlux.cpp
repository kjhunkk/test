
#include "../INC/Euler2DFlux.h"

Euler2DFlux::Euler2DFlux() : eps_(10e-8)
{
	readData("EulerEquation", INPUT() + "equation.dat");
}

void Euler2DFlux::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	Re_ = 1.0;
	Ma_ = 1.0;
	Pr_ = 1.0;
	Tref_ = 1.0;

	while (getline(infile, text)){
		if (text.find("~>viscous_on_", 0) != std::string::npos){
			getline(infile, text);
			if (!text.compare("on")) viscous_on_ = true;
			else viscous_on_ = false;
		}
		if (text.find("~>Reynolds_", 0) != std::string::npos)
			infile >> Re_;
		if (text.find("~>Ma_", 0) != std::string::npos)
			infile >> Ma_;
		if (text.find("~>Prandtl_", 0) != std::string::npos)
			infile >> Pr_;
		if (text.find("~>Tref_", 0) != std::string::npos)
			infile >> Tref_;
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}

	Sutherland_coef_ = 110.55 / Tref_;
	alpha_ = 1.0 * Ma_ / Re_;
	beta_ = 1.4 / Pr_*alpha_;
	max_for_visradii_ = std::max(4.0 / 3.0*alpha_, beta_);
	infile.close();
}

const std::vector<real_t> Euler2DFlux::calViscosity(const std::vector<real_t>& temperature) const
{
	int_t num_points = temperature.size();
	std::vector<real_t> viscosity(num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		viscosity[ipoint] = (1.0 + Sutherland_coef_) / (temperature[ipoint] + Sutherland_coef_)*std::pow(temperature[ipoint], 1.5);
	return viscosity;
}

const std::vector<real_t> Euler2DFlux::calPressure(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
	std::vector<real_t> pressure(num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	for (int_t istate = 0; istate < num_states_; istate++)
		pressure[ipoint] = (u[ipoint*num_states_ + 3] - 0.5*(u[ipoint*num_states_ + 1] * u[ipoint*num_states_ + 1] + u[ipoint*num_states_ + 2] * u[ipoint*num_states_ + 2]) / u[ipoint*num_states_])*0.4;
	return pressure;
}

const std::vector<real_t> Euler2DFlux::calPressureCoefficients(const std::vector<real_t>& u) const
{
	const int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> Cp(num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		Cp[ipoint] = 2.0*(pressure[ipoint] - 1.0 / 1.4) / Ma_ / Ma_;
	return Cp;
}

const std::vector<real_t> Euler2DFlux::calTemperature(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
	std::vector<real_t> temperature(num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	for (int_t istate = 0; istate < num_states_; istate++)
		temperature[ipoint] = (u[ipoint*num_states_ + 3] - 0.5*(u[ipoint*num_states_ + 1] * u[ipoint*num_states_ + 1] + u[ipoint*num_states_ + 2] * u[ipoint*num_states_ + 2]) / u[ipoint*num_states_])*0.56 / u[ipoint*num_states_];
	return temperature;
}

const std::vector<real_t> Euler2DFlux::commonConvFlux(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> Flux(num_points*num_states_*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t& d = u[ipoint*num_states_];
		const real_t& du = u[ipoint*num_states_ + 1];
		const real_t& dv = u[ipoint*num_states_ + 2];
		const real_t& dE = u[ipoint*num_states_ + 3];
		const real_t& p = pressure[ipoint];

		Flux[ipoint*num_states_*dimension_] = du;
		Flux[ipoint*num_states_*dimension_ + 1] = dv;

		Flux[ipoint*num_states_*dimension_ + 2] = du*du / d + p;
		Flux[ipoint*num_states_*dimension_ + 3] = du*dv / d;

		Flux[ipoint*num_states_*dimension_ + 4] = Flux[ipoint*num_states_*dimension_ + 3];
		Flux[ipoint*num_states_*dimension_ + 5] = dv*dv / d + p;

		Flux[ipoint*num_states_*dimension_ + 6] = du / d*(dE + p);
		Flux[ipoint*num_states_*dimension_ + 7] = dv / d*(dE + p);
	}
	return Flux;
}

const std::vector<real_t> Euler2DFlux::commonConvFluxJacobian(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	{
		const real_t& d = u[ipoint*num_states_];
		const real_t& du = u[ipoint*num_states_ + 1];
		const real_t& dv = u[ipoint*num_states_ + 2];
		const real_t& dE = u[ipoint*num_states_ + 3];
		const real_t& p = pressure[ipoint];

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 2] = 1.0; // Fx_01

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 5] = 1.0; // Fy_02

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 8] = (0.2 * dv * dv - 0.8 * du * du) / (d*d); // Fx_10
		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 9] = -du * dv / (d*d); // Fy_10

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 10] = 1.6 * du / d; // Fx_11
		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 11] = dv / d; // Fy_11

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 12] = -0.4*dv / d; // Fx_12
		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 13] = du / d; // Fy_12

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 14] = 0.4; // Fx_13

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 16] = -du * dv / (d*d); // Fx_20
		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 17] = (0.2 * du * du - 0.8 * dv * dv) / (d*d); // Fy_20

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 18] = dv / d; // Fx_21
		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 19] = -0.4*du / d; // Fy_21

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 20] = du / d; // Fx_22
		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 21] = 1.6*dv / d; // Fy_22

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 23] = 0.4; // Fy_23

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 24] = 0.2*du*(du*du + dv * dv) / (d*d*d) - du * (dE + p) / (d*d); // Fx_30
		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 25] = 0.2*dv*(du*du + dv * dv) / (d*d*d) - dv * (dE + p) / (d*d);; // Fy_30

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 26] = (dE + p) / d - 0.4*du*du / (d*d); // Fx_31
		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 27] = - 0.4*du*dv / (d*d); // Fy_31

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 28] = FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 27]; // Fx_32
		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 29] = (dE + p) / d - 0.4*dv*dv / (d*d); // Fy_32

		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 30] = 1.4*du / d; // Fx_33
		FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 31] = 1.4*dv / d; // Fy_33
	}
	return FluxJacobi;
}

const std::vector<real_t> Euler2DFlux::commonConvFlux(const std::vector<real_t>& u, const std::vector<real_t>& normal) const
{
	int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> Flux(num_points*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& d = u[ipoint*num_states_];
		const real_t& du = u[ipoint*num_states_ + 1];
		const real_t& dv = u[ipoint*num_states_ + 2];
		const real_t& dE = u[ipoint*num_states_ + 3];
		const real_t& p = pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V = (du*nx + dv * ny) / d;

		Flux[ipoint*num_states_] = d * V;
		Flux[ipoint*num_states_ + 1] = du * V + nx * p;
		Flux[ipoint*num_states_ + 2] = dv * V + ny * p;
		Flux[ipoint*num_states_ + 3] = V * (dE + p);
	}
	return Flux;
}

const std::vector<real_t> Euler2DFlux::commonConvFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& normal) const
{
	int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_, 0.0);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& d = u[ipoint*num_states_];
		const real_t& du = u[ipoint*num_states_ + 1];
		const real_t& dv = u[ipoint*num_states_ + 2];
		const real_t& dE = u[ipoint*num_states_ + 3];
		const real_t& p = pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V = (du*nx + dv * ny) / d;

		// row 0
		FluxJacobi[ipoint*num_states_*num_states_ + 1] = nx;
		FluxJacobi[ipoint*num_states_*num_states_ + 2] = ny;

		// row 1
		FluxJacobi[ipoint*num_states_*num_states_ + 4] = 0.2*nx*(du*du + dv * dv) / (d*d) - du * V / d;
		FluxJacobi[ipoint*num_states_*num_states_ + 5] = V + 0.6*du * nx / d;
		FluxJacobi[ipoint*num_states_*num_states_ + 6] = du * ny / d - 0.4*dv*nx / d;
		FluxJacobi[ipoint*num_states_*num_states_ + 7] = 0.4*nx;

		// row 2
		FluxJacobi[ipoint*num_states_*num_states_ + 8] = 0.2*ny*(du*du + dv * dv) / (d*d) - dv * V / d;
		FluxJacobi[ipoint*num_states_*num_states_ + 9] = dv * nx / d - 0.4*du*ny / d;
		FluxJacobi[ipoint*num_states_*num_states_ + 10] = V + 0.6*dv * ny / d;
		FluxJacobi[ipoint*num_states_*num_states_ + 11] = 0.4*ny;

		// row 3
		FluxJacobi[ipoint*num_states_*num_states_ + 12] = 0.2*(du*du + dv * dv)*V / (d*d) - (dE + p)*V / d;
		FluxJacobi[ipoint*num_states_*num_states_ + 13] = nx * (dE + p) / d - 0.4*du*V / d;
		FluxJacobi[ipoint*num_states_*num_states_ + 14] = ny * (dE + p) / d - 0.4*dv*V / d;
		FluxJacobi[ipoint*num_states_*num_states_ + 15] = 1.4*V;
	}
	return FluxJacobi;
}

const std::vector<real_t> Euler2DFlux::commonViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& div_u) const
{
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = calTemperature(owner_u);
	const std::vector<real_t> viscosity = calViscosity(temperature);
#endif
	std::vector<real_t> Flux(num_points*num_states_*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &div_u[ipoint*num_states_*dimension_];

		const real_t ux = (t[2] - u[1] / u[0] * t[0]) / u[0];
		const real_t uy = (t[3] - u[1] / u[0] * t[1]) / u[0];
		const real_t vx = (t[4] - u[2] / u[0] * t[0]) / u[0];
		const real_t vy = (t[5] - u[2] / u[0] * t[1]) / u[0];
		const real_t ex = ((t[6] - t[0] * u[3] / u[0]) - u[1] * ux - u[2] * vx) / u[0];
		const real_t ey = ((t[7] - t[1] * u[3] / u[0]) - u[1] * uy - u[2] * vy) / u[0];
		const real_t txx = 2.0 / 3.0*(2.0*ux - vy);
		const real_t txy = uy + vx;
		const real_t tyy = 2.0 / 3.0 * (2.0*vy - ux);
		const real_t alpha = alpha_*viscosity[ipoint];
		const real_t beta = beta_*viscosity[ipoint];

		Flux[ipoint*num_states_*dimension_] = 0.0;
		Flux[ipoint*num_states_*dimension_ + 1] = 0.0;

		Flux[ipoint*num_states_*dimension_ + 2] = alpha*txx;
		Flux[ipoint*num_states_*dimension_ + 3] = alpha*txy;

		Flux[ipoint*num_states_*dimension_ + 4] = alpha*txy;
		Flux[ipoint*num_states_*dimension_ + 5] = alpha*tyy;

		Flux[ipoint*num_states_*dimension_ + 6] = alpha*(txx*u[1] + txy*u[2]) / u[0] + beta*ex;
		Flux[ipoint*num_states_*dimension_ + 7] = alpha*(txy*u[1] + tyy*u[2]) / u[0] + beta*ey;
	}
	return Flux;
}

const std::vector<real_t> Euler2DFlux::commonViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{ // Verified
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = calTemperature(owner_u);
	const std::vector<real_t> viscosity = calViscosity(temperature);
#endif
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &owner_div_u[ipoint*num_states_*dimension_];

		const real_t ux = (t[2] - u[1] / u[0] * t[0]) / u[0];
		const real_t uy = (t[3] - u[1] / u[0] * t[1]) / u[0];
		const real_t vx = (t[4] - u[2] / u[0] * t[0]) / u[0];
		const real_t vy = (t[5] - u[2] / u[0] * t[1]) / u[0];
		const real_t ex = ((t[6] - t[0] * u[3] / u[0]) - u[1] * ux - u[2] * vx) / u[0];
		const real_t ey = ((t[7] - t[1] * u[3] / u[0]) - u[1] * uy - u[2] * vy) / u[0];
		const real_t txx = 2.0 / 3.0*(2.0*ux - vy);
		const real_t txy = uy + vx;
		const real_t tyy = 2.0 / 3.0 * (2.0*vy - ux);
		const real_t alpha = alpha_ * viscosity[ipoint];
		const real_t beta = beta_ * viscosity[ipoint];

		const real_t deno1 = pow(u[0], 2.0);
		const real_t deno2 = pow(u[0], 3.0);
		const real_t deno3 = pow(u[0], 4.0);

		const std::vector<real_t> jacobian_txx = { ((2.0*t[5] - 4.0*t[2])*u[0] + 8.0*t[0] * u[1] - 4.0*t[1] * u[2]) / (3.0*deno2), -4.0*t[0] / (3.0*deno1), 2.0*t[1] / (3.0*deno1), 0.0 };
		const std::vector<real_t> jacobian_txy = { (-(t[3] + t[4])*u[0] + 2.0*t[0] * u[2] + 2.0*t[1] * u[1]) / deno2,-t[1] / deno1,-t[0] / deno1, 0.0 };
		const std::vector<real_t> jacobian_tyy = { ((2.0*t[2] - 4.0*t[5])*u[0] + 8.0*t[1] * u[2] - 4.0*t[0] * u[1]) / (3.0*deno2),2.0*t[0] / (3.0*deno1),-4.0*t[1] / (3.0*deno1), 0.0 };
		const std::vector<real_t> jacobian_ex = { (2.0*(t[0] * u[3] + t[2] * u[1] + t[4] * u[2])*u[0] - t[6] * u[0] * u[0] - 3.0*t[0] * u[1] * u[1] - 3.0*t[0] * u[2] * u[2]) / deno3, (-t[2] * u[0] + 2.0*t[0] * u[1]) / deno2, (-t[4] * u[0] + 2.0*t[0] * u[2]) / deno2, -t[0] / deno1 };
		const std::vector<real_t> jacobian_ey = { (2.0*(t[1] * u[3] + t[3] * u[1] + t[5] * u[2])*u[0] - t[7] * u[0] * u[0] - 3.0*t[1] * u[1] * u[1] - 3.0*t[1] * u[2] * u[2]) / deno3, (-t[3] * u[0] + 2.0*t[1] * u[1]) / deno2, (-t[5] * u[0] + 2.0*t[1] * u[2]) / deno2, -t[1] / deno1 };
		const std::vector<real_t> jacobian_u1u0 = { -u[1] / (u[0] * u[0]), 1.0 / u[0], 0.0, 0.0 };
		const std::vector<real_t> jacobian_u2u0 = { -u[2] / (u[0] * u[0]), 0.0, 1.0 / u[0], 0.0 };
#ifndef _CONSTANT_VISCOSITY_
		const real_t viscosity_temp = viscosity[ipoint] / u[0] * (1.0 / (Sutherland_coef_ + temperature[ipoint]) - 1.5 / temperature[ipoint]);
		const std::vector<real_t> jacobian_viscosity = { viscosity_temp*(temperature[ipoint] - 0.28*(u[1] * u[1] + u[2] * u[2]) / (u[0] * u[0])), 0.56*u[1] / u[0] * viscosity_temp, 0.56*u[2] / u[0] * viscosity_temp,  -0.56*viscosity_temp };
#endif
		for (int_t i = 0; i < 4; i++)
		{
			// istate, jstate, idim
#ifdef _CONSTANT_VISCOSITY_
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 8 + 2 * i] = alpha * jacobian_txx[i];
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 9 + 2 * i] = alpha * jacobian_txy[i];
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 16 + 2 * i] = FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 9 + 2 * i];
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 17 + 2 * i] = alpha * jacobian_tyy[i];
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 24 + 2 * i] = alpha * ((jacobian_txx[i] * u[1] + jacobian_txy[i] * u[2]) / u[0] + (txx*jacobian_u1u0[i] + txy * jacobian_u2u0[i]))
				+ beta * jacobian_ex[i];
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 25 + 2 * i] = alpha * ((jacobian_txy[i] * u[1] + jacobian_tyy[i] * u[2]) / u[0] + (txy*jacobian_u1u0[i] + tyy * jacobian_u2u0[i]))
				+ beta * jacobian_ey[i];
#else
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 8 + 2 * i] = alpha * jacobian_txx[i] + alpha_ * jacobian_viscosity[i] * txx;
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 9 + 2 * i] = alpha * jacobian_txy[i] + alpha_ * jacobian_viscosity[i] * txy;
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 16 + 2 * i] = FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 9 + 2 * i];
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 17 + 2 * i] = alpha * jacobian_tyy[i] + alpha_ * jacobian_viscosity[i] * tyy;
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 24 + 2 * i] = alpha * ((jacobian_txx[i] * u[1] + jacobian_txy[i] * u[2]) / u[0] + (txx*jacobian_u1u0[i] + txy * jacobian_u2u0[i]))
				+ beta * jacobian_ex[i] + jacobian_viscosity[i] * (alpha_*(txx*u[1] + txy * u[2]) / u[0] + beta_ * ex);
			FluxJacobi[ipoint*num_states_*num_states_*dimension_ + 25 + 2 * i] = alpha * ((jacobian_txy[i] * u[1] + jacobian_tyy[i] * u[2]) / u[0] + (txy*jacobian_u1u0[i] + tyy * jacobian_u2u0[i]))
				+ beta * jacobian_ey[i] + jacobian_viscosity[i] * (alpha_*(txy*u[1] + tyy * u[2]) / u[0] + beta_ * ey);
#endif
		}
	}
	return FluxJacobi;
}

const std::vector<real_t> Euler2DFlux::commonViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{ // Verified2
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = calTemperature(owner_u);
	const std::vector<real_t> viscosity = calViscosity(temperature);
#endif
	std::vector<real_t> FluxGradJacobi(num_points*num_states_*num_states_*dimension_*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &owner_div_u[ipoint*num_states_*dimension_];

		const real_t alpha = alpha_ * viscosity[ipoint];
		const real_t beta = beta_ * viscosity[ipoint];

		// (istate, jstate, idim, jdim)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 16] = -4.0*alpha*u[1] / (3.0*u[0] * u[0]); // (1, 0, 0, 0)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 17] = 2.0*alpha*u[2] / (3.0 * u[0] * u[0]); // (1, 0, 0, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 18] = -alpha * u[2] / (u[0] * u[0]);// (1, 0, 1, 0)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 19] = -alpha * u[1] / (u[0] * u[0]); // (1, 0, 1, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 20] = 4.0*alpha / (3.0*u[0]); // (1, 1, 0, 0)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 23] = alpha / u[0]; // (1, 1, 1, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 25] = -2.0*alpha / (3.0*u[0]); // (1, 2, 0, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 26] = alpha / u[0]; // (1, 2, 1, 0)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 32] = -alpha * u[2] / (u[0] * u[0]); // (2, 0, 0, 0)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 33] = -alpha * u[1] / (u[0] * u[0]); // (2, 0, 0, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 34] = 2.0*alpha*u[1] / (3.0*u[0] * u[0]); // (2, 0, 1, 0)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 35] = -4.0*alpha*u[2] / (3.0*u[0] * u[0]); // (2, 0, 1, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 37] = alpha / u[0];  // (2, 1, 0, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 38] = -2.0*alpha / (3.0*u[0]); // (2, 1, 1, 0)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 40] = alpha / u[0]; // (2, 2, 0, 0)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 43] = 4.0*alpha / (3.0*u[0]); // (2, 2, 1, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 48] = beta * (u[1] * u[1] / u[0] + u[2] * u[2] / u[0] - u[3]) / (u[0] * u[0]) - alpha * (4.0*u[1] * u[1] / 3.0 + u[2] * u[2]) / pow(u[0], 3.0); // (3, 0, 0, 0)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 49] = -alpha * u[1] * u[2] / (3.0*pow(u[0], 3.0)); // (3, 0, 0, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 50] = -alpha * u[1] * u[2] / (3.0*pow(u[0], 3.0)); // (3, 0, 1, 0)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 51] = beta * (u[1] * u[1] / u[0] + u[2] * u[2] / u[0] - u[3]) / (u[0] * u[0]) - alpha * (4.0*u[2] * u[2] / 3.0 + u[1] * u[1]) / pow(u[0], 3.0); // (3, 0, 1, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 52] = (4.0*alpha*u[1] / 3.0 - beta * u[1]) / (u[0] * u[0]); // (3, 1, 0, 0)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 53] = alpha * u[2] / (u[0] * u[0]); // (3, 1, 0, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 54] = -2.0*alpha * u[2] / (3.0 *u[0] * u[0]); // (3, 1, 1, 0)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 55] = (alpha*u[1] - beta * u[1]) / (u[0] * u[0]); // (3, 1, 1, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 56] = (alpha - beta)*u[2] / (u[0] * u[0]); // (3, 2, 0, 0)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 57] = -2.0*alpha*u[1] / (3.0*u[0] * u[0]); // (3, 2, 0, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 58] = alpha*u[1] / (u[0] * u[0]);  // (3, 2, 1, 0)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 59] = (4.0*alpha*u[2] / 3.0 - beta * u[2]) / (u[0] * u[0]); // (3, 2, 1, 1)

		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 60] = beta / u[0]; // (3, 3, 0, 0)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_*dimension_ + 63] = beta / u[0]; // (3, 3, 1, 1)
	}
	return FluxGradJacobi;
}

const std::vector<real_t> Euler2DFlux::commonViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = calTemperature(owner_u);
	const std::vector<real_t> viscosity = calViscosity(temperature);
#endif
	std::vector<real_t> Flux(num_points*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t ux = (t[2] - u[1] / u[0] * t[0]) / u[0];
		const real_t uy = (t[3] - u[1] / u[0] * t[1]) / u[0];
		const real_t vx = (t[4] - u[2] / u[0] * t[0]) / u[0];
		const real_t vy = (t[5] - u[2] / u[0] * t[1]) / u[0];
		const real_t ex = ((t[6] - t[0] * u[3] / u[0]) - u[1] * ux - u[2] * vx) / u[0];
		const real_t ey = ((t[7] - t[1] * u[3] / u[0]) - u[1] * uy - u[2] * vy) / u[0];
		const real_t txx = 2.0 / 3.0*(2.0*ux - vy);
		const real_t txy = uy + vx;
		const real_t tyy = 2.0 / 3.0 * (2.0*vy - ux);
		const real_t alpha = alpha_*viscosity[ipoint];
		const real_t beta = beta_*viscosity[ipoint];

		Flux[ipoint*num_states_] = 0.0;
		Flux[ipoint*num_states_ + 1] = alpha*(txx*n[0] + txy*n[1]);
		Flux[ipoint*num_states_ + 2] = alpha*(txy*n[0] + tyy*n[1]);
		Flux[ipoint*num_states_ + 3] = alpha*((txx*u[1] + txy*u[2])*n[0] + (txy*u[1] + tyy*u[2])*n[1]) / u[0] + beta*(ex*n[0] + ey*n[1]);
	}
	return Flux;
}

const std::vector<real_t> Euler2DFlux::commonViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = calTemperature(owner_u);
	const std::vector<real_t> viscosity = calViscosity(temperature);
#endif
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &owner_div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t ux = (t[2] - u[1] / u[0] * t[0]) / u[0];
		const real_t uy = (t[3] - u[1] / u[0] * t[1]) / u[0];
		const real_t vx = (t[4] - u[2] / u[0] * t[0]) / u[0];
		const real_t vy = (t[5] - u[2] / u[0] * t[1]) / u[0];
		const real_t ex = ((t[6] - t[0] * u[3] / u[0]) - u[1] * ux - u[2] * vx) / u[0];
		const real_t ey = ((t[7] - t[1] * u[3] / u[0]) - u[1] * uy - u[2] * vy) / u[0];
		const real_t txx = 2.0 / 3.0 * (2.0*ux - vy);
		const real_t txy = uy + vx;
		const real_t tyy = 2.0 / 3.0 * (2.0*vy - ux);

		const real_t alpha = alpha_ * viscosity[ipoint];
		const real_t beta = beta_ * viscosity[ipoint];

		const real_t deno1 = pow(u[0], 2.0);
		const real_t deno2 = pow(u[0], 3.0);
		const real_t deno3 = pow(u[0], 4.0);

		const std::vector<real_t> jacobian_txx = { ((2.0*t[5] - 4.0*t[2])*u[0] + 8.0*t[0] * u[1] - 4.0*t[1] * u[2]) / (3.0*deno2), -4.0*t[0] / (3.0*deno1), 2.0*t[1] / (3.0*deno1), 0.0 };
		const std::vector<real_t> jacobian_txy = { (-(t[3] + t[4])*u[0] + 2.0*t[0] * u[2] + 2.0*t[1] * u[1]) / deno2,-t[1] / deno1,-t[0] / deno1, 0.0 };
		const std::vector<real_t> jacobian_tyy = { ((2.0*t[2] - 4.0*t[5])*u[0] + 8.0*t[1] * u[2] - 4.0*t[0] * u[1]) / (3.0*deno2),2.0*t[0] / (3.0*deno1),-4.0*t[1] / (3.0*deno1), 0.0 };
		const std::vector<real_t> jacobian_ex = { (2.0*(t[0] * u[3] + t[2] * u[1] + t[4] * u[2])*u[0] - t[6] * u[0] * u[0] - 3.0*t[0] * u[1] * u[1] - 3.0*t[0] * u[2] * u[2]) / deno3, (-t[2] * u[0] + 2.0*t[0] * u[1]) / deno2, (-t[4] * u[0] + 2.0*t[0] * u[2]) / deno2, -t[0] / deno1 };
		const std::vector<real_t> jacobian_ey = { (2.0*(t[1] * u[3] + t[3] * u[1] + t[5] * u[2])*u[0] - t[7] * u[0] * u[0] - 3.0*t[1] * u[1] * u[1] - 3.0*t[1] * u[2] * u[2]) / deno3, (-t[3] * u[0] + 2.0*t[1] * u[1]) / deno2, (-t[5] * u[0] + 2.0*t[1] * u[2]) / deno2, -t[1] / deno1 };
		const std::vector<real_t> jacobian_u1u0 = { -u[1] / (u[0] * u[0]), 1.0 / u[0], 0.0, 0.0 };
		const std::vector<real_t> jacobian_u2u0 = { -u[2] / (u[0] * u[0]), 0.0, 1.0 / u[0], 0.0 };
#ifndef _CONSTANT_VISCOSITY_
		const real_t viscosity_temp = viscosity[ipoint] / u[0] * (1.0 / (Sutherland_coef_ + temperature[ipoint]) - 1.5 / temperature[ipoint]);
		const std::vector<real_t> jacobian_viscosity = { viscosity_temp*(temperature[ipoint] - 0.28*(u[1] * u[1] + u[2] * u[2]) / (u[0] * u[0])), 0.56*u[1] / u[0] * viscosity_temp, 0.56*u[2] / u[0] * viscosity_temp,  -0.56*viscosity_temp };
#endif
		for (int_t i = 0; i < 4; i++)
		{
#ifdef _CONSTANT_VISCOSITY_
			// row 1
			FluxJacobi[ipoint*num_states_*num_states_ + 4 + i] = alpha*(jacobian_txx[i] * n[0] + jacobian_txy[i] * n[1]);
			// row 2
			FluxJacobi[ipoint*num_states_*num_states_ + 8 + i] = alpha*(jacobian_txy[i] * n[0] + jacobian_tyy[i] * n[1]);
			// row 3
			FluxJacobi[ipoint*num_states_*num_states_ + 12 + i] = alpha*(((jacobian_txx[i] * u[1] + jacobian_txy[i] * u[2])*n[0] + (jacobian_txy[i] * u[1] + jacobian_tyy[i] * u[2])*n[1]) / u[0]
				+ (txx*jacobian_u1u0[i] + txy * jacobian_u2u0[i])*n[0] + (txy*jacobian_u1u0[i] + tyy * jacobian_u2u0[i])*n[1]) + beta * (jacobian_ex[i] * n[0] + jacobian_ey[i] * n[1]);
#else
			// row 1
			FluxJacobi[ipoint*num_states_*num_states_ + 4 + i] = alpha * (jacobian_txx[i] * n[0] + jacobian_txy[i] * n[1]) + alpha_ * jacobian_viscosity[i] * (txx*n[0] + txy * n[1]);
			// row 2
			FluxJacobi[ipoint*num_states_*num_states_ + 8 + i] = alpha * (jacobian_txy[i] * n[0] + jacobian_tyy[i] * n[1]) + alpha_ * jacobian_viscosity[i] * (txy*n[0] + tyy * n[1]);
			// row 3
			FluxJacobi[ipoint*num_states_*num_states_ + 12 + i] = alpha * (((jacobian_txx[i] * u[1] + jacobian_txy[i] * u[2])*n[0] + (jacobian_txy[i] * u[1] + jacobian_tyy[i] * u[2])*n[1]) / u[0]
				+ (txx*jacobian_u1u0[i] + txy * jacobian_u2u0[i])*n[0] + (txy*jacobian_u1u0[i] + tyy * jacobian_u2u0[i])*n[1]) + alpha_ * jacobian_viscosity[i] * ((txx*u[1] + txy * u[2])*n[0] + (txy*u[1] + tyy * u[2])*n[1]) / u[0]
				+ beta * (jacobian_ex[i] * n[0] + jacobian_ey[i] * n[1]) + beta_ * jacobian_viscosity[i] * (ex*n[0] + ey * n[1]);
#endif
		}
	}
	return FluxJacobi;
}

const std::vector<real_t> Euler2DFlux::commonViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = calTemperature(owner_u);
	const std::vector<real_t> viscosity = calViscosity(temperature);
#endif
	std::vector<real_t> FluxGradJacobi(num_points*num_states_*num_states_*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &owner_div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t alpha = alpha_ * viscosity[ipoint];
		const real_t beta = beta_ * viscosity[ipoint];

		// (istate, jstate, idim) // need modification
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 8] = -alpha * (4.0*n[0] * u[1] / 3.0 + n[1] * u[2]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 9] = alpha * (2.0*n[0] * u[2] / 3.0 - n[1] * u[1]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 10] = alpha * 4.0*n[0] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 11] = alpha * n[1] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 12] = alpha * n[1] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 13] = alpha * (-2.0)*n[0] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 16] = alpha * (-n[0] * u[2] + 2.0*n[1] * u[1] / 3.0) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 17] = -alpha * (n[0] * u[1] + 4.0*n[1] * u[2] / 3.0) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 18] = alpha * (-2.0)*n[1] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 19] = alpha * n[0] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 20] = alpha * n[0] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 21] = alpha * 4.0*n[1] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 24] = beta * n[0] * (u[1] * u[1] / u[0] + u[2] * u[2] / u[0] - u[3]) / (u[0] * u[0]) - alpha * (n[0] * (4.0*u[1] * u[1] / 3.0 + u[2] * u[2]) + n[1] * u[1] * u[2] / 3.0) / pow(u[0], 3.0);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 25] = beta * n[1] * (u[1] * u[1] / u[0] + u[2] * u[2] / u[0] - u[3]) / (u[0] * u[0]) - alpha * (n[1] * (4.0*u[2] * u[2] / 3.0 + u[1] * u[1]) + n[0] * u[1] * u[2] / 3.0) / pow(u[0], 3.0);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 26] = (alpha*(4.0*n[0] * u[1] - 2.0*n[1] * u[2]) / 3.0 - beta * n[0] *u[1]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 27] = (alpha * (n[0] * u[2] + n[1] * u[1]) - beta * n[1] * u[1]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 28] = (alpha*(n[0] * u[2] + n[1] * u[1]) - beta * n[0] * u[2]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 29] = (alpha * (-2.0*n[0] * u[1] + 4.0*n[1] * u[2]) / 3.0 - beta * n[1] * u[2]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 30] = beta * n[0] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 31] = beta * n[1] / u[0];
	}
	return FluxGradJacobi;
}

const std::vector<real_t> Euler2DFlux::numViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> owner_viscosity(num_points, 1.0);
	const std::vector<real_t> neighbor_viscosity(num_points, 1.0);
#else
	const std::vector<real_t> owner_temperature = calTemperature(owner_u);
	const std::vector<real_t> owner_viscosity = calViscosity(owner_temperature);
	const std::vector<real_t> neighbor_temperature = calTemperature(neighbor_u);
	const std::vector<real_t> neighbor_viscosity = calViscosity(neighbor_temperature);
#endif
	std::vector<real_t> Flux(num_points*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t* u1 = &owner_u[ipoint*num_states_];
		const real_t* t1 = &owner_div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t ux1 = (t1[2] - u1[1] / u1[0] * t1[0]) / u1[0];
		const real_t uy1 = (t1[3] - u1[1] / u1[0] * t1[1]) / u1[0];
		const real_t vx1 = (t1[4] - u1[2] / u1[0] * t1[0]) / u1[0];
		const real_t vy1 = (t1[5] - u1[2] / u1[0] * t1[1]) / u1[0];
		const real_t ex1 = ((t1[6] - t1[0] * u1[3] / u1[0]) - u1[1] * ux1 - u1[2] * vx1) / u1[0];
		const real_t ey1 = ((t1[7] - t1[1] * u1[3] / u1[0]) - u1[1] * uy1 - u1[2] * vy1) / u1[0];
		const real_t txx1 = 2.0 / 3.0 * (2.0*ux1 - vy1);
		const real_t txy1 = uy1 + vx1;
		const real_t tyy1 = 2.0 / 3.0 * (2.0*vy1 - ux1);
		const real_t alpha1 = alpha_*owner_viscosity[ipoint];
		const real_t beta1 = beta_*owner_viscosity[ipoint];

		Flux[ipoint*num_states_] = 0.0;
		Flux[ipoint*num_states_ + 1] = 0.5* alpha1*(txx1*n[0] + txy1*n[1]);
		Flux[ipoint*num_states_ + 2] = 0.5* alpha1*(txy1*n[0] + tyy1*n[1]);
		Flux[ipoint*num_states_ + 3] = 0.5* alpha1*((txx1*u1[1] + txy1*u1[2])*n[0] + (txy1*u1[1] + tyy1*u1[2])*n[1]) / u1[0] + 0.5* beta1*(ex1*n[0] + ey1*n[1]);

		const real_t* u2 = &neighbor_u[ipoint*num_states_];
		const real_t* t2 = &neighbor_div_u[ipoint*num_states_*dimension_];

		const real_t ux2 = (t2[2] - u2[1] / u2[0] * t2[0]) / u2[0];
		const real_t uy2 = (t2[3] - u2[1] / u2[0] * t2[1]) / u2[0];
		const real_t vx2 = (t2[4] - u2[2] / u2[0] * t2[0]) / u2[0];
		const real_t vy2 = (t2[5] - u2[2] / u2[0] * t2[1]) / u2[0];
		const real_t ex2 = ((t2[6] - t2[0] * u2[3] / u2[0]) - u2[1] * ux2 - u2[2] * vx2) / u2[0];
		const real_t ey2 = ((t2[7] - t2[1] * u2[3] / u2[0]) - u2[1] * uy2 - u2[2] * vy2) / u2[0];
		const real_t txx2 = 2.0 / 3.0 * (2.0*ux2 - vy2);
		const real_t txy2 = uy2 + vx2;
		const real_t tyy2 = 2.0 / 3.0 * (2.0*vy2 - ux2);
		const real_t alpha2 = alpha_*neighbor_viscosity[ipoint];
		const real_t beta2 = beta_*neighbor_viscosity[ipoint];

		//Flux[ipoint*num_states_] += 0.0;
		Flux[ipoint*num_states_ + 1] += 0.5* alpha2*(txx2*n[0] + txy2*n[1]);
		Flux[ipoint*num_states_ + 2] += 0.5* alpha2*(txy2*n[0] + tyy2*n[1]);
		Flux[ipoint*num_states_ + 3] += 0.5* alpha2*((txx2*u2[1] + txy2*u2[2])*n[0] + (txy2*u2[1] + tyy2*u2[2])*n[1]) / u2[0] + 0.5* beta2*(ex2*n[0] + ey2*n[1]);
	}
	return Flux;
}

const std::vector<real_t> Euler2DFlux::numViscFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{// Verified
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> owner_viscosity(num_points, 1.0);
#else
	const std::vector<real_t> owner_temperature = calTemperature(owner_u);
	const std::vector<real_t> owner_viscosity = calViscosity(owner_temperature);
#endif
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &owner_div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t ux1 = (t[2] - u[1] / u[0] * t[0]) / u[0];
		const real_t uy1 = (t[3] - u[1] / u[0] * t[1]) / u[0];
		const real_t vx1 = (t[4] - u[2] / u[0] * t[0]) / u[0];
		const real_t vy1 = (t[5] - u[2] / u[0] * t[1]) / u[0];
		const real_t ex = ((t[6] - t[0] * u[3] / u[0]) - u[1] * ux1 - u[2] * vx1) / u[0];
		const real_t ey = ((t[7] - t[1] * u[3] / u[0]) - u[1] * uy1 - u[2] * vy1) / u[0];
		const real_t txx = 2.0 / 3.0 * (2.0*ux1 - vy1);
		const real_t txy = uy1 + vx1;
		const real_t tyy = 2.0 / 3.0 * (2.0*vy1 - ux1);

		const real_t alpha = alpha_ * owner_viscosity[ipoint];
		const real_t beta = beta_ * owner_viscosity[ipoint];

		const real_t deno1 = pow(u[0], 2.0);
		const real_t deno2 = pow(u[0], 3.0);
		const real_t deno3 = pow(u[0], 4.0);

		const std::vector<real_t> jacobian_txx = { ((2.0*t[5] - 4.0*t[2])*u[0] + 8.0*t[0] * u[1] - 4.0*t[1] * u[2]) / (3.0*deno2), -4.0*t[0] / (3.0*deno1), 2.0*t[1] / (3.0*deno1), 0.0 };
		const std::vector<real_t> jacobian_txy = { (-(t[3] + t[4])*u[0] + 2.0*t[0] * u[2] + 2.0*t[1] * u[1]) / deno2,-t[1] / deno1,-t[0] / deno1, 0.0 };
		const std::vector<real_t> jacobian_tyy = { ((2.0*t[2] - 4.0*t[5])*u[0] + 8.0*t[1] * u[2] - 4.0*t[0] * u[1]) / (3.0*deno2),2.0*t[0] / (3.0*deno1),-4.0*t[1] / (3.0*deno1), 0.0 };
		const std::vector<real_t> jacobian_ex = { (2.0*(t[0] * u[3] + t[2] * u[1] + t[4] * u[2])*u[0] - t[6] * u[0] * u[0] - 3.0*t[0] * u[1] * u[1] - 3.0*t[0] * u[2] * u[2]) / deno3, (-t[2] * u[0] + 2.0*t[0] * u[1]) / deno2, (-t[4] * u[0] + 2.0*t[0] * u[2]) / deno2, -t[0] / deno1 };
		const std::vector<real_t> jacobian_ey = { (2.0*(t[1] * u[3] + t[3] * u[1] + t[5] * u[2])*u[0] - t[7] * u[0] * u[0] - 3.0*t[1] * u[1] * u[1] - 3.0*t[1] * u[2] * u[2]) / deno3, (-t[3] * u[0] + 2.0*t[1] * u[1]) / deno2, (-t[5] * u[0] + 2.0*t[1] * u[2]) / deno2, -t[1] / deno1 };
		const std::vector<real_t> jacobian_u1u0 = { -u[1] / (u[0] * u[0]), 1.0 / u[0], 0.0, 0.0 };
		const std::vector<real_t> jacobian_u2u0 = { -u[2] / (u[0] * u[0]), 0.0, 1.0 / u[0], 0.0 };
#ifndef _CONSTANT_VISCOSITY_
		const real_t viscosity_temp = owner_viscosity[ipoint] / u[0] * (1.0 / (Sutherland_coef_ + owner_temperature[ipoint]) - 1.5 / owner_temperature[ipoint]);
		const std::vector<real_t> jacobian_viscosity = { viscosity_temp*(owner_temperature[ipoint] - 0.28*(u[1] * u[1] + u[2] * u[2]) / (u[0] * u[0])), 0.56*u[1] / u[0] * viscosity_temp, 0.56*u[2] / u[0] * viscosity_temp,  -0.56*viscosity_temp };
#endif
		for (int_t i = 0; i < 4; i++)
		{
#ifdef _CONSTANT_VISCOSITY_
			// row 1
			FluxJacobi[ipoint*num_states_*num_states_ + 4 + i] = 0.5*alpha*(jacobian_txx[i] * n[0] + jacobian_txy[i] * n[1]);
			// row 2
			FluxJacobi[ipoint*num_states_*num_states_ + 8 + i] = 0.5*alpha*(jacobian_txy[i] * n[0] + jacobian_tyy[i] * n[1]);
			// row 3
			FluxJacobi[ipoint*num_states_*num_states_ + 12 + i] = 0.5*(alpha*(((jacobian_txx[i] * u[1] + jacobian_txy[i] * u[2])*n[0] + (jacobian_txy[i] * u[1] + jacobian_tyy[i] * u[2])*n[1]) / u[0]
				+ (txx*jacobian_u1u0[i] + txy * jacobian_u2u0[i])*n[0] + (txy*jacobian_u1u0[i] + tyy * jacobian_u2u0[i])*n[1]) + beta * (jacobian_ex[i] * n[0] + jacobian_ey[i] * n[1]));
#else
			// row 1
			FluxJacobi[ipoint*num_states_*num_states_ + 4 + i] = 0.5*(alpha*(jacobian_txx[i] * n[0] + jacobian_txy[i] * n[1]) + alpha_ * jacobian_viscosity[i] * (txx*n[0] + txy * n[1]));
			// row 2
			FluxJacobi[ipoint*num_states_*num_states_ + 8 + i] = 0.5*(alpha*(jacobian_txy[i] * n[0] + jacobian_tyy[i] * n[1]) + alpha_ * jacobian_viscosity[i] * (txy*n[0] + tyy * n[1]));
			// row 3
			FluxJacobi[ipoint*num_states_*num_states_ + 12 + i] = 0.5*(alpha*(((jacobian_txx[i] * u[1] + jacobian_txy[i] * u[2])*n[0] + (jacobian_txy[i] * u[1] + jacobian_tyy[i] * u[2])*n[1]) / u[0]
				+ (txx*jacobian_u1u0[i] + txy * jacobian_u2u0[i])*n[0] + (txy*jacobian_u1u0[i] + tyy * jacobian_u2u0[i])*n[1]) + alpha_ * jacobian_viscosity[i] * ((txx*u[1] + txy * u[2])*n[0] + (txy*u[1] + tyy * u[2])*n[1]) / u[0]
				+ beta * (jacobian_ex[i] * n[0] + jacobian_ey[i] * n[1]) + beta_ * jacobian_viscosity[i] * (ex*n[0] + ey * n[1]));
#endif
		}
	}
	return FluxJacobi;
}

const std::vector<real_t> Euler2DFlux::numViscFluxOwnerGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{ // Verified
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> owner_viscosity(num_points, 1.0);
#else
	const std::vector<real_t> owner_temperature = calTemperature(owner_u);
	const std::vector<real_t> owner_viscosity = calViscosity(owner_temperature);
#endif
	std::vector<real_t> FluxGradJacobi(num_points*num_states_*num_states_*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &owner_div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t alpha = alpha_ * owner_viscosity[ipoint];
		const real_t beta = beta_ * owner_viscosity[ipoint];

		// (istate, jstate, idim)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 8] = -0.5*alpha * (4.0*n[0] * u[1] / 3.0 + n[1] * u[2]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 9] = 0.5*alpha * (2.0*n[0] * u[2] / 3.0 - n[1] * u[1]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 10] = 0.5*alpha * 4.0*n[0] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 11] = 0.5*alpha * n[1] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 12] = 0.5*alpha * n[1] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 13] = 0.5*alpha * (-2.0)*n[0] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 16] = 0.5*alpha * (-n[0] * u[2] + 2.0*n[1] * u[1] / 3.0) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 17] = -0.5*alpha * (n[0] * u[1] + 4.0*n[1] * u[2] / 3.0) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 18] = 0.5*alpha * (-2.0)*n[1] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 19] = 0.5*alpha * n[0] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 20] = 0.5*alpha * n[0] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 21] = 0.5*alpha * 4.0*n[1] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 24] = 0.5*beta * n[0] * (u[1] * u[1] / u[0] + u[2] * u[2] / u[0] - u[3]) / (u[0] * u[0]) - 0.5*alpha * (n[0] * (4.0*u[1] * u[1] / 3.0 + u[2] * u[2]) + n[1] * u[1] * u[2] / 3.0) / pow(u[0], 3.0);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 25] = 0.5*beta * n[1] * (u[1] * u[1] / u[0] + u[2] * u[2] / u[0] - u[3]) / (u[0] * u[0]) - 0.5*alpha * (n[1] * (4.0*u[2] * u[2] / 3.0 + u[1] * u[1]) + n[0] * u[1] * u[2] / 3.0) / pow(u[0], 3.0);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 26] = 0.5*(alpha*(4.0*n[0] * u[1] - 2.0*n[1] * u[2]) / 3.0 - beta * n[0] * u[1]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 27] = 0.5*(alpha * (n[0] * u[2] + n[1] * u[1]) - beta * n[1] * u[1]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 28] = 0.5*(alpha*(n[0] * u[2] + n[1] * u[1]) - beta * n[0] * u[2]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 29] = 0.5*(alpha * (-2.0*n[0] * u[1] + 4.0*n[1] * u[2]) / 3.0 - beta * n[1] * u[2]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 30] = 0.5*beta * n[0] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 31] = 0.5*beta * n[1] / u[0];
	}
	return FluxGradJacobi;
}

const std::vector<real_t> Euler2DFlux::numViscFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{ // Verified
	int_t num_points = neighbor_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> neighbor_viscosity(num_points, 1.0);
#else
	const std::vector<real_t> neighbor_temperature = calTemperature(neighbor_u);
	const std::vector<real_t> neighbor_viscosity = calViscosity(neighbor_temperature);
#endif
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &neighbor_u[ipoint*num_states_];
		const real_t* t = &neighbor_div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t ux2 = (t[2] - u[1] / u[0] * t[0]) / u[0];
		const real_t uy2 = (t[3] - u[1] / u[0] * t[1]) / u[0];
		const real_t vx2 = (t[4] - u[2] / u[0] * t[0]) / u[0];
		const real_t vy2 = (t[5] - u[2] / u[0] * t[1]) / u[0];
		const real_t ex = ((t[6] - t[0] * u[3] / u[0]) - u[1] * ux2 - u[2] * vx2) / u[0];
		const real_t ey = ((t[7] - t[1] * u[3] / u[0]) - u[1] * uy2 - u[2] * vy2) / u[0];
		const real_t txx = 2.0 / 3.0 * (2.0*ux2 - vy2);
		const real_t txy = uy2 + vx2;
		const real_t tyy = 2.0 / 3.0 * (2.0*vy2 - ux2);

		const real_t alpha = alpha_ * neighbor_viscosity[ipoint];
		const real_t beta = beta_ * neighbor_viscosity[ipoint];

		const real_t deno1 = pow(u[0], 2.0);
		const real_t deno2 = pow(u[0], 3.0);
		const real_t deno3 = pow(u[0], 4.0);

		const std::vector<real_t> jacobian_txx = { ((2.0*t[5] - 4.0*t[2])*u[0] + 8.0*t[0] * u[1] - 4.0*t[1] * u[2]) / (3.0*deno2), -4.0*t[0] / (3.0*deno1), 2.0*t[1] / (3.0*deno1), 0.0 };
		const std::vector<real_t> jacobian_txy = { (-(t[3] + t[4])*u[0] + 2.0*t[0] * u[2] + 2.0*t[1] * u[1]) / deno2,-t[1] / deno1,-t[0] / deno1, 0.0 };
		const std::vector<real_t> jacobian_tyy = { ((2.0*t[2] - 4.0*t[5])*u[0] + 8.0*t[1] * u[2] - 4.0*t[0] * u[1]) / (3.0*deno2),2.0*t[0] / (3.0*deno1),-4.0*t[1] / (3.0*deno1), 0.0 };
		const std::vector<real_t> jacobian_ex = { (2.0*(t[0] * u[3] + t[2] * u[1] + t[4] * u[2])*u[0] - t[6] * u[0] * u[0] - 3.0*t[0] * u[1] * u[1] - 3.0*t[0] * u[2] * u[2]) / deno3, (-t[2] * u[0] + 2.0*t[0] * u[1]) / deno2, (-t[4] * u[0] + 2.0*t[0] * u[2]) / deno2, -t[0] / deno1 };
		const std::vector<real_t> jacobian_ey = { (2.0*(t[1] * u[3] + t[3] * u[1] + t[5] * u[2])*u[0] - t[7] * u[0] * u[0] - 3.0*t[1] * u[1] * u[1] - 3.0*t[1] * u[2] * u[2]) / deno3, (-t[3] * u[0] + 2.0*t[1] * u[1]) / deno2, (-t[5] * u[0] + 2.0*t[1] * u[2]) / deno2, -t[1] / deno1 };
		const std::vector<real_t> jacobian_u1u0 = { -u[1] / (u[0] * u[0]), 1.0 / u[0], 0.0, 0.0 };
		const std::vector<real_t> jacobian_u2u0 = { -u[2] / (u[0] * u[0]), 0.0, 1.0 / u[0], 0.0 };
#ifndef _CONSTANT_VISCOSITY_
		const real_t viscosity_temp = neighbor_viscosity[ipoint] / u[0] * (1.0 / (Sutherland_coef_ + neighbor_temperature[ipoint]) - 1.5 / neighbor_temperature[ipoint]);
		const std::vector<real_t> jacobian_viscosity = { viscosity_temp*(neighbor_temperature[ipoint] - 0.28*(u[1] * u[1] + u[2] * u[2]) / (u[0] * u[0])), 0.56*u[1] / u[0] * viscosity_temp, 0.56*u[2] / u[0] * viscosity_temp,  -0.56*viscosity_temp };
#endif
		for (int_t i = 0; i < 4; i++)
		{
#ifdef _CONSTANT_VISCOSITY_
			// row 1
			FluxJacobi[ipoint*num_states_*num_states_ + 4 + i] = 0.5*alpha*(jacobian_txx[i] * n[0] + jacobian_txy[i] * n[1]);
			// row 2
			FluxJacobi[ipoint*num_states_*num_states_ + 8 + i] = 0.5*alpha*(jacobian_txy[i] * n[0] + jacobian_tyy[i] * n[1]);
			// row 3
			FluxJacobi[ipoint*num_states_*num_states_ + 12 + i] = 0.5*(alpha*(((jacobian_txx[i] * u[1] + jacobian_txy[i] * u[2])*n[0] + (jacobian_txy[i] * u[1] + jacobian_tyy[i] * u[2])*n[1]) / u[0]
				+ (txx*jacobian_u1u0[i] + txy * jacobian_u2u0[i])*n[0] + (txy*jacobian_u1u0[i] + tyy * jacobian_u2u0[i])*n[1]) + beta*(jacobian_ex[i] * n[0] + jacobian_ey[i] * n[1]));
#else
			// row 1
			FluxJacobi[ipoint*num_states_*num_states_ + 4 + i] = 0.5*(alpha*(jacobian_txx[i] * n[0] + jacobian_txy[i] * n[1]) + alpha_ * jacobian_viscosity[i] * (txx*n[0] + txy * n[1]));
			// row 2
			FluxJacobi[ipoint*num_states_*num_states_ + 8 + i] = 0.5*(alpha*(jacobian_txy[i] * n[0] + jacobian_tyy[i] * n[1]) + alpha_ * jacobian_viscosity[i] * (txy*n[0] + tyy * n[1]));
			// row 3
			FluxJacobi[ipoint*num_states_*num_states_ + 12 + i] = 0.5*(alpha*(((jacobian_txx[i] * u[1] + jacobian_txy[i] * u[2])*n[0] + (jacobian_txy[i] * u[1] + jacobian_tyy[i] * u[2])*n[1]) / u[0]
				+ (txx*jacobian_u1u0[i] + txy * jacobian_u2u0[i])*n[0] + (txy*jacobian_u1u0[i] + tyy * jacobian_u2u0[i])*n[1]) + alpha_ * jacobian_viscosity[i] * ((txx*u[1] + txy * u[2])*n[0] + (txy*u[1] + tyy * u[2])*n[1]) / u[0]
				+ beta * (jacobian_ex[i] * n[0] + jacobian_ey[i] * n[1]) + beta_ * jacobian_viscosity[i] * (ex*n[0] + ey * n[1]));
#endif
		}
	}
	return FluxJacobi;
}

const std::vector<real_t> Euler2DFlux::numViscFluxNeighborGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{ // Verified
	int_t num_points = neighbor_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> neighbor_viscosity(num_points, 1.0);
#else
	const std::vector<real_t> neighbor_temperature = calTemperature(neighbor_u);
	const std::vector<real_t> neighbor_viscosity = calViscosity(neighbor_temperature);
#endif
	std::vector<real_t> FluxGradJacobi(num_points*num_states_*num_states_*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &neighbor_u[ipoint*num_states_];
		const real_t* t = &neighbor_div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t alpha = alpha_ * neighbor_viscosity[ipoint];
		const real_t beta = beta_ * neighbor_viscosity[ipoint];

		// (istate, jstate, idim)
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 8] = -0.5*alpha * (4.0*n[0] * u[1] / 3.0 + n[1] * u[2]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 9] = 0.5*alpha * (2.0*n[0] * u[2] / 3.0 - n[1] * u[1]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 10] = 0.5*alpha * 4.0*n[0] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 11] = 0.5*alpha * n[1] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 12] = 0.5*alpha * n[1] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 13] = 0.5*alpha * (-2.0)*n[0] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 16] = 0.5*alpha * (-n[0] * u[2] + 2.0*n[1] * u[1] / 3.0) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 17] = -0.5*alpha * (n[0] * u[1] + 4.0*n[1] * u[2] / 3.0) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 18] = 0.5*alpha * (-2.0)*n[1] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 19] = 0.5*alpha * n[0] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 20] = 0.5*alpha * n[0] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 21] = 0.5*alpha * 4.0*n[1] / (3.0*u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 24] = 0.5*beta * n[0] * (u[1] * u[1] / u[0] + u[2] * u[2] / u[0] - u[3]) / (u[0] * u[0]) - 0.5*alpha * (n[0] * (4.0*u[1] * u[1] / 3.0 + u[2] * u[2]) + n[1] * u[1] * u[2] / 3.0) / pow(u[0], 3.0);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 25] = 0.5*beta * n[1] * (u[1] * u[1] / u[0] + u[2] * u[2] / u[0] - u[3]) / (u[0] * u[0]) - 0.5*alpha * (n[1] * (4.0*u[2] * u[2] / 3.0 + u[1] * u[1]) + n[0] * u[1] * u[2] / 3.0) / pow(u[0], 3.0);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 26] = 0.5*(alpha*(4.0*n[0] * u[1] - 2.0*n[1] * u[2]) / 3.0 - beta * n[0] * u[1]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 27] = 0.5*(alpha * (n[0] * u[2] + n[1] * u[1]) - beta * n[1] * u[1]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 28] = 0.5*(alpha*(n[0] * u[2] + n[1] * u[1]) - beta * n[0] * u[2]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 29] = 0.5*(alpha * (-2.0*n[0] * u[1] + 4.0*n[1] * u[2]) / 3.0 - beta * n[1] * u[2]) / (u[0] * u[0]);
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 30] = 0.5*beta * n[0] / u[0];
		FluxGradJacobi[ipoint*num_states_*num_states_*dimension_ + 31] = 0.5*beta * n[1] / u[0];
	}
	return FluxGradJacobi;
}

const std::vector<real_t> Euler2DFlux::convWaveVelocity(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> wave(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t& d = u[ipoint*num_states_];
		const real_t& du = u[ipoint*num_states_ + 1];
		const real_t& dv = u[ipoint*num_states_ + 2];
		const real_t& p = pressure[ipoint];
		const real_t a = std::sqrt(1.4*p / d);

		wave[ipoint*dimension_] = std::abs(du / d) + a;
		wave[ipoint*dimension_ + 1] = std::abs(dv / d) + a;
	}
	return wave;
}

const std::vector<real_t> Euler2DFlux::viscWaveVelocity(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = calTemperature(u);
	const std::vector<real_t> viscosity = calViscosity(temperature);
#endif
	std::vector<real_t> wave(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	for (int_t idim = 0; idim < dimension_; idim++)
		wave[ipoint*dimension_ + idim] = viscosity[ipoint] / u[ipoint*num_states_] * max_for_visradii_;
	return wave;
}

const std::vector<real_t> Euler2DFlux::source(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
	std::vector<real_t> source(num_points*num_states_, 0.0);
	return source;
}

const std::vector<real_t> Euler2DFlux::source(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const
{
	int_t num_points = u.size();
	std::vector<real_t> source(num_points, 0.0);
	return source;
}

const std::vector<real_t> Euler2DFlux::numConvFluxOwnerCentralJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> FluxJacobi = commonConvFluxJacobian(owner_u, normal);
	for (int_t i = 0; i < FluxJacobi.size(); i++)
		FluxJacobi[i] *= 0.5;
	return FluxJacobi;
}

const std::vector<real_t> Euler2DFlux::numConvFluxNeighborCentralJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> FluxJacobi = commonConvFluxJacobian(neighbor_u, normal);
	for (int_t i = 0; i < FluxJacobi.size(); i++)
		FluxJacobi[i] *= 0.5;
	return FluxJacobi;
}

Euler2DLLF::Euler2DLLF()
{
	setName("LLF");
	Euler2DFlux::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DLLF::numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> Flux(num_points*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t& d1 = owner_u[ipoint*num_states_];
		const real_t& du1 = owner_u[ipoint*num_states_ + 1];
		const real_t& dv1 = owner_u[ipoint*num_states_ + 2];
		const real_t& dE1 = owner_u[ipoint*num_states_ + 3];
		const real_t& p1 = owner_pressure[ipoint];
		const real_t& d2 = neighbor_u[ipoint*num_states_];
		const real_t& du2 = neighbor_u[ipoint*num_states_ + 1];
		const real_t& dv2 = neighbor_u[ipoint*num_states_ + 2];
		const real_t& dE2 = neighbor_u[ipoint*num_states_ + 3];
		const real_t& p2 = neighbor_pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V1 = (du1*nx + dv1*ny) / d1;
		const real_t V2 = (du2*nx + dv2*ny) / d2;

		std::vector<real_t> flux1(4);
		flux1[0] = d1*V1;
		flux1[1] = du1*V1 + nx*p1;
		flux1[2] = dv1*V1 + ny*p1;
		flux1[3] = V1*(dE1 + p1);

		std::vector<real_t> flux2(4);
		flux2[0] = d2*V2;
		flux2[1] = du2*V2 + nx*p2;
		flux2[2] = dv2*V2 + ny*p2;
		flux2[3] = V2*(dE2 + p2);

		const real_t a1 = std::sqrt(1.4*p1 / d1);
		const real_t a2 = std::sqrt(1.4*p2 / d2);
		const real_t r_max = std::max(std::abs(V1) + a1, std::abs(V2) + a2);

		for (int_t istate = 0; istate < num_states_; istate++)
			Flux[ipoint*num_states_ + istate] = 0.5*(flux1[istate] + flux2[istate] - r_max*(neighbor_u[ipoint*num_states_ + istate] - owner_u[ipoint*num_states_ + istate]));
	}

	return Flux;
}

const std::vector<real_t> Euler2DLLF::numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_, 0.0);
	FluxJacobi = numConvFluxOwnerCentralJacobian(owner_u, neighbor_u, normal);
	std::vector<real_t> jacobian_r_max(num_states_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	{
		const real_t& d1 = owner_u[ipoint*num_states_];
		const real_t& du1 = owner_u[ipoint*num_states_ + 1];
		const real_t& dv1 = owner_u[ipoint*num_states_ + 2];
		const real_t& dE1 = owner_u[ipoint*num_states_ + 3];
		const real_t& p1 = owner_pressure[ipoint];
		const real_t& d2 = neighbor_u[ipoint*num_states_];
		const real_t& du2 = neighbor_u[ipoint*num_states_ + 1];
		const real_t& dv2 = neighbor_u[ipoint*num_states_ + 2];
		const real_t& dE2 = neighbor_u[ipoint*num_states_ + 3];
		const real_t& p2 = neighbor_pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V1 = (du1*nx + dv1 * ny) / d1;
		const real_t V2 = (du2*nx + dv2 * ny) / d2;

		const real_t a1 = std::sqrt(1.4*p1 / d1);
		const real_t a2 = std::sqrt(1.4*p2 / d2);
		const real_t r_max = std::max(std::abs(V1) + a1, std::abs(V2) + a2);
		for (int_t idiag = 0; idiag < num_states_; idiag++)
			FluxJacobi[ipoint*num_states_*num_states_ + idiag * num_states_ + idiag] += 0.5*r_max;
		if((std::abs(V1) + a1) >= (std::abs(V2) + a2))
		{
			jacobian_r_max = { -(double)sgn(V1)*V1 / d1 - 0.7*(p1*d1 - 0.2*(du1*du1 + dv1 * dv1)) / (a1*pow(d1,3.0)), (double)sgn(V1)*nx / d1 - 0.28*du1 / (a1*d1*d1), (double)sgn(V1)*ny / d1 - 0.28*dv1 / (a1*d1*d1), 0.28 / (d1*a1) };
			for (int_t icolumn = 0; icolumn < num_states_; icolumn++)
			{
				FluxJacobi[ipoint*num_states_*num_states_ + icolumn] -= 0.5*jacobian_r_max[icolumn] * (d2 - d1);
				FluxJacobi[ipoint*num_states_*num_states_ + num_states_ + icolumn] -= 0.5*jacobian_r_max[icolumn] * (du2 - du1);
				FluxJacobi[ipoint*num_states_*num_states_ + 2 * num_states_ + icolumn] -= 0.5*jacobian_r_max[icolumn] * (dv2 - dv1);
				FluxJacobi[ipoint*num_states_*num_states_ + 3 * num_states_ + icolumn] -= 0.5*jacobian_r_max[icolumn] * (dE2 - dE1);
			}
		}
	}
	return FluxJacobi;
}

const std::vector<real_t> Euler2DLLF::numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_, 0.0);
	FluxJacobi = numConvFluxNeighborCentralJacobian(owner_u, neighbor_u, normal);
	std::vector<real_t> jacobian_r_max(num_states_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	{
		const real_t& d1 = owner_u[ipoint*num_states_];
		const real_t& du1 = owner_u[ipoint*num_states_ + 1];
		const real_t& dv1 = owner_u[ipoint*num_states_ + 2];
		const real_t& dE1 = owner_u[ipoint*num_states_ + 3];
		const real_t& p1 = owner_pressure[ipoint];
		const real_t& d2 = neighbor_u[ipoint*num_states_];
		const real_t& du2 = neighbor_u[ipoint*num_states_ + 1];
		const real_t& dv2 = neighbor_u[ipoint*num_states_ + 2];
		const real_t& dE2 = neighbor_u[ipoint*num_states_ + 3];
		const real_t& p2 = neighbor_pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V1 = (du1*nx + dv1 * ny) / d1;
		const real_t V2 = (du2*nx + dv2 * ny) / d2;

		const real_t a1 = std::sqrt(1.4*p1 / d1);
		const real_t a2 = std::sqrt(1.4*p2 / d2);
		const real_t r_max = std::max(std::abs(V1) + a1, std::abs(V2) + a2);
		for (int_t idiag = 0; idiag < num_states_; idiag++)
			FluxJacobi[ipoint*num_states_*num_states_ + idiag * num_states_ + idiag] -= 0.5*r_max;
		if ((std::abs(V1) + a1) <= (std::abs(V2) + a2))
		{
			jacobian_r_max = { -(double)sgn(V2)*V2 / d2 - 0.7*(p2*d2 - 0.2*(du2*du2 + dv2 * dv2)) / (a2*pow(d2,3.0)), (double)sgn(V2)*nx / d2 - 0.28*du2 / (a2*d2*d2), (double)sgn(V2)*ny / d2 - 0.28*dv2 / (a2*d2*d2), 0.28 / (d2*a2) };
			for (int_t icolumn = 0; icolumn < num_states_; icolumn++)
			{
				FluxJacobi[ipoint*num_states_*num_states_ + icolumn] -= 0.5*jacobian_r_max[icolumn] * (d2 - d1);
				FluxJacobi[ipoint*num_states_*num_states_ + num_states_ + icolumn] -= 0.5*jacobian_r_max[icolumn] * (du2 - du1);
				FluxJacobi[ipoint*num_states_*num_states_ + 2 * num_states_ + icolumn] -= 0.5*jacobian_r_max[icolumn] * (dv2 - dv1);
				FluxJacobi[ipoint*num_states_*num_states_ + 3 * num_states_ + icolumn] -= 0.5*jacobian_r_max[icolumn] * (dE2 - dE1);
			}
		}
	}
	return FluxJacobi;
}

Euler2DRoe::Euler2DRoe()
{
	setName("Roe");
	Euler2DFlux::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DRoe::numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> Flux(num_points*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t& d1 = owner_u[ipoint*num_states_];
		const real_t& du1 = owner_u[ipoint*num_states_ + 1];
		const real_t& dv1 = owner_u[ipoint*num_states_ + 2];
		const real_t& dE1 = owner_u[ipoint*num_states_ + 3];
		const real_t& p1 = owner_pressure[ipoint];
		const real_t& d2 = neighbor_u[ipoint*num_states_];
		const real_t& du2 = neighbor_u[ipoint*num_states_ + 1];
		const real_t& dv2 = neighbor_u[ipoint*num_states_ + 2];
		const real_t& dE2 = neighbor_u[ipoint*num_states_ + 3];
		const real_t& p2 = neighbor_pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V1 = (du1*nx + dv1*ny) / d1;
		const real_t V2 = (du2*nx + dv2*ny) / d2;

		std::vector<real_t> flux1(4);
		flux1[0] = d1*V1;
		flux1[1] = du1*V1 + nx*p1;
		flux1[2] = dv1*V1 + ny*p1;
		flux1[3] = V1*(dE1 + p1);

		std::vector<real_t> flux2(4);
		flux2[0] = d2*V2;
		flux2[1] = du2*V2 + nx*p2;
		flux2[2] = dv2*V2 + ny*p2;
		flux2[3] = V2*(dE2 + p2);

		const real_t dsqrt1 = std::sqrt(d1);
		const real_t dsqrt2 = std::sqrt(d2);

		const real_t Rd = dsqrt1*dsqrt2;
		const real_t Ru = (du1 / dsqrt1 + du2 / dsqrt2) / (dsqrt1 + dsqrt2);
		const real_t Rv = (dv1 / dsqrt1 + dv2 / dsqrt2) / (dsqrt1 + dsqrt2);
		const real_t Rh = ((dE1 + p1) / dsqrt1 + (dE2 + p2) / dsqrt2) / (dsqrt1 + dsqrt2);
		const real_t Ra = std::sqrt(0.4*(Rh - 0.5*(Ru*Ru + Rv*Rv)));
		const real_t RV = (V1*dsqrt1 + V2*dsqrt2) / (dsqrt1 + dsqrt2);

		const real_t dd = d2 - d1;
		const real_t du = du2 / d2 - du1 / d1;
		const real_t dv = dv2 / d2 - dv1 / d1;
		const real_t dp = p2 - p1;
		const real_t dV = V2 - V1;

		std::vector<real_t> flux_1(4);
		std::vector<real_t> flux_2(4);
		std::vector<real_t> flux_3(4);
		std::vector<real_t> flux_4(4);

		flux_1[0] = std::abs(RV)*(dd - dp / (Ra*Ra));
		flux_1[1] = flux_1[0] * Ru;
		flux_1[2] = flux_1[0] * Rv;
		flux_1[3] = flux_1[0] * 0.5*(Ru*Ru + Rv*Rv);

		real_t temp = std::abs(RV)*Rd*(du*ny - dv*nx);
		flux_2[0] = 0.0;
		flux_2[1] = temp*ny;
		flux_2[2] = -temp*nx;
		flux_2[3] = flux_2[1] * Ru + flux_2[2] * Rv;

		temp = std::abs(RV + Ra)*(dp + Rd*Ra*dV)*0.5 / (Ra*Ra);
		flux_3[0] = temp;
		flux_3[1] = temp*(Ru + nx*Ra);
		flux_3[2] = temp*(Rv + ny*Ra);
		flux_3[3] = temp*(Rh + Ra*RV);

		temp = std::abs(RV - Ra)*(dp - Rd*Ra*dV)*0.5 / (Ra*Ra);
		flux_4[0] = temp;
		flux_4[1] = temp*(Ru - nx*Ra);
		flux_4[2] = temp*(Rv - ny*Ra);
		flux_4[3] = temp*(Rh - Ra*RV);

		for (int_t istate = 0; istate < num_states_; istate++)
			Flux[ipoint*num_states_ + istate] = 0.5*(flux1[istate] + flux2[istate] - flux_1[istate] - flux_2[istate] - flux_3[istate] - flux_4[istate]);
	}

	return Flux;
}

const std::vector<real_t> Euler2DRoe::numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	std::vector<real_t> fluxjacobi1(num_states_*num_states_, 0.0);
	std::vector<real_t> fluxjacobi_1(16, 0.0);
	std::vector<real_t> fluxjacobi_2(16, 0.0);
	std::vector<real_t> fluxjacobi_3(16, 0.0);
	std::vector<real_t> fluxjacobi_4(16, 0.0);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& d1 = owner_u[ipoint*num_states_];
		const real_t& du1 = owner_u[ipoint*num_states_ + 1];
		const real_t& dv1 = owner_u[ipoint*num_states_ + 2];
		const real_t& dE1 = owner_u[ipoint*num_states_ + 3];
		const real_t& p1 = owner_pressure[ipoint];
		const real_t& d2 = neighbor_u[ipoint*num_states_];
		const real_t& du2 = neighbor_u[ipoint*num_states_ + 1];
		const real_t& dv2 = neighbor_u[ipoint*num_states_ + 2];
		const real_t& dE2 = neighbor_u[ipoint*num_states_ + 3];
		const real_t& p2 = neighbor_pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V1 = (du1*nx + dv1 * ny) / d1;
		const real_t V2 = (du2*nx + dv2 * ny) / d2;
		
		// row 0
		fluxjacobi1[1] = nx;
		fluxjacobi1[2] = ny;
		// row 1
		fluxjacobi1[4] = 0.2*nx*(du1*du1 + dv1 * dv1) / (d1*d1) - du1 * V1 / d1;
		fluxjacobi1[5] = V1 + 0.6*du1 * nx / d1;
		fluxjacobi1[6] = du1 * ny / d1 - 0.4*dv1*nx / d1;
		fluxjacobi1[7] = 0.4*nx;
		// row 2
		fluxjacobi1[8] = 0.2*ny*(du1*du1 + dv1 * dv1) / (d1*d1) - dv1 * V1 / d1;
		fluxjacobi1[9] = dv1 * nx / d1 - 0.4*du1*ny / d1;
		fluxjacobi1[10] = V1 + 0.6*dv1 * ny / d1;
		fluxjacobi1[11] = 0.4*ny;
		// row 3
		fluxjacobi1[12] = 0.2*(du1*du1 + dv1 * dv1)*V1 / (d1*d1) - (dE1 + p1)*V1 / d1;
		fluxjacobi1[13] = nx * (dE1 + p1) / d1 - 0.4*du1*V1 / d1;
		fluxjacobi1[14] = ny * (dE1 + p1) / d1 - 0.4*dv1*V1 / d1;
		fluxjacobi1[15] = 1.4*V1;
		
		const real_t dsqrt1 = std::sqrt(d1);
		const real_t dsqrt2 = std::sqrt(d2);
		const real_t dsqrt1p2 = dsqrt1 + dsqrt2;

		const real_t Rd = dsqrt1 * dsqrt2;
		const real_t Ru = (du1 / dsqrt1 + du2 / dsqrt2) / dsqrt1p2;
		const real_t Rv = (dv1 / dsqrt1 + dv2 / dsqrt2) / dsqrt1p2;
		const real_t Rh = ((dE1 + p1) / dsqrt1 + (dE2 + p2) / dsqrt2) / dsqrt1p2;
		const real_t Ra = std::sqrt(0.4*(Rh - 0.5*(Ru*Ru + Rv * Rv)));
		const real_t RV = (V1*dsqrt1 + V2 * dsqrt2) / dsqrt1p2;

		const std::vector<real_t> jacobian_Rd = { 0.5*dsqrt2 / dsqrt1, 0.0, 0.0, 0.0 };
		const std::vector<real_t> jacobian_Ru = { -0.5*(du1 / d1 + Ru) / (dsqrt1*dsqrt1p2), 1.0 / (dsqrt1*dsqrt1p2), 0.0, 0.0 };
		const std::vector<real_t> jacobian_Rv = { -0.5*(dv1 / d1 + Rv) / (dsqrt1*dsqrt1p2), 0.0, jacobian_Ru[1], 0.0 };
		const std::vector<real_t> jacobian_Rh = { -0.5*((dE1 + p1) / d1 + Rh + 0.4*(du1*du1 + dv1 * dv1) / (d1*d1)) / (dsqrt1*dsqrt1p2), -0.4*du1 / (d1*dsqrt1*dsqrt1p2), -0.4*dv1 / (d1*dsqrt1*dsqrt1p2), 1.4*jacobian_Ru[1] };
		const std::vector<real_t> jacobian_Ra = { 0.2*(jacobian_Rh[0] - Ru * jacobian_Ru[0] - Rv * jacobian_Rv[0]) / Ra, 0.2*(-Ru * jacobian_Ru[1]) / Ra, 0.2*(-Rv * jacobian_Rv[2]) / Ra, 0.2*jacobian_Rh[3] / Ra };
		const std::vector<real_t> jacobian_RV = { -0.5*(V1 + (V1 * dsqrt1 + V2 * dsqrt2) / dsqrt1p2) / (dsqrt1*dsqrt1p2), nx / (dsqrt1*dsqrt1p2), ny / (dsqrt1*dsqrt1p2), 0.0 };
		const std::vector<real_t> jacobian_dp = { -0.2*(du1*du1 + dv1 * dv1) / (d1*d1),0.4*du1 / d1,0.4*dv1 / d1,-0.4 };

		const real_t dd = d2 - d1;
		const real_t du = du2 / d2 - du1 / d1;
		const real_t dv = dv2 / d2 - dv1 / d1;
		const real_t dp = p2 - p1;
		const real_t dV = V2 - V1;

		real_t temp = dd - dp / (Ra*Ra);
		std::vector<real_t> temp_jacobian = { -1.0, 0.0,0.0,0.0 };
		for (int_t i = 0; i < 4; i++)
		{
			fluxjacobi_1[i] = (double)sgn(RV)*jacobian_RV[i] * temp + std::abs(RV)*(temp_jacobian[i] - (jacobian_dp[i]*Ra - 2.0*dp*jacobian_Ra[i]) / pow(Ra,3.0));
			fluxjacobi_1[4 + i] = fluxjacobi_1[i] * Ru + std::abs(RV)*temp*jacobian_Ru[i];
			fluxjacobi_1[8 + i] = fluxjacobi_1[i] * Rv + std::abs(RV)*temp*jacobian_Rv[i];
			fluxjacobi_1[12 + i] = fluxjacobi_1[i] * 0.5*(Ru*Ru + Rv * Rv) + std::abs(RV)*temp*(Ru*jacobian_Ru[i] + Rv * jacobian_Rv[i]);
		}
		
		temp = du*ny - dv * nx;
		temp_jacobian = { (du1*ny - dv1 * nx) / (d1*d1),-ny / d1,nx / d1,0 };
		for (int_t i = 0; i < 4; i++)
		{
			fluxjacobi_2[4 + i] = ((double)sgn(RV)*jacobian_RV[i] * Rd*temp + std::abs(RV)*jacobian_Rd[i] * temp + std::abs(RV)*Rd*temp_jacobian[i])*ny;
			fluxjacobi_2[8 + i] = -((double)sgn(RV)*jacobian_RV[i] * Rd*temp + std::abs(RV)*jacobian_Rd[i] * temp + std::abs(RV)*Rd*temp_jacobian[i])*nx;
			fluxjacobi_2[12 + i] = fluxjacobi_2[4 + i] * Ru + std::abs(RV)*Rd*temp*ny*jacobian_Ru[i] + fluxjacobi_2[8 + i] * Rv - std::abs(RV)*Rd*temp*nx*jacobian_Rv[i];
		}

		temp = (dp + Rd * Ra*dV)*0.5 / (Ra*Ra);
		temp_jacobian = { V1 / d1,-nx / d1,-ny / d1,0.0 };
		for (int_t i = 0; i < 4; i++)
		{
			fluxjacobi_3[i] = (double)sgn(RV + Ra)*(jacobian_RV[i] + jacobian_Ra[i])*temp + 0.5*std::abs(RV + Ra)*(jacobian_dp[i] * Ra - 2.0*dp*jacobian_Ra[i] + pow(Ra, 2.0)*dV*jacobian_Rd[i] - (Rd * Ra * dV + 2.0*dp)*jacobian_Ra[i] + Rd * pow(Ra, 2.0)*temp_jacobian[i]) / pow(Ra, 3.0);
			fluxjacobi_3[4 + i] = fluxjacobi_3[i] * (Ru + nx * Ra) + std::abs(RV + Ra)*temp * (jacobian_Ru[i] + nx * jacobian_Ra[i]);
			fluxjacobi_3[8 + i] = fluxjacobi_3[i] * (Rv + ny * Ra) + std::abs(RV + Ra)*temp * (jacobian_Rv[i] + ny * jacobian_Ra[i]);
			fluxjacobi_3[12 + i] = fluxjacobi_3[i] * (Rh + Ra * RV) + std::abs(RV + Ra)*temp * (jacobian_Rh[i] + Ra * jacobian_RV[i] + jacobian_Ra[i] * RV);
		}

		temp = (dp - Rd * Ra*dV)*0.5 / (Ra*Ra);
		for (int_t i = 0; i < 4; i++)
		{
			fluxjacobi_4[i] = (double)sgn(RV - Ra)*(jacobian_RV[i] - jacobian_Ra[i])*temp + 0.5*std::abs(RV - Ra)*(jacobian_dp[i] * Ra - 2.0*dp*jacobian_Ra[i] - pow(Ra, 2.0)*dV*jacobian_Rd[i] + (Rd * Ra * dV - 2.0*dp)*jacobian_Ra[i] - Rd * pow(Ra, 2.0)*temp_jacobian[i]) / pow(Ra, 3.0);
			fluxjacobi_4[4 + i] = fluxjacobi_4[i] * (Ru - nx * Ra) + std::abs(RV - Ra)*temp * (jacobian_Ru[i] - nx * jacobian_Ra[i]);
			fluxjacobi_4[8 + i] = fluxjacobi_4[i] * (Rv - ny * Ra) + std::abs(RV - Ra)*temp * (jacobian_Rv[i] - ny * jacobian_Ra[i]);
			fluxjacobi_4[12 + i] = fluxjacobi_4[i] * (Rh - Ra * RV) + std::abs(RV - Ra)*temp * (jacobian_Rh[i] - Ra * jacobian_RV[i] - jacobian_Ra[i] * RV);
		}

		for (int_t icomp = 0; icomp < num_states_*num_states_; icomp++)
			FluxJacobi[ipoint*num_states_*num_states_ + icomp] = 0.5*(fluxjacobi1[icomp] - fluxjacobi_1[icomp] - fluxjacobi_2[icomp] - fluxjacobi_3[icomp] - fluxjacobi_4[icomp]);
	}

	return FluxJacobi;
}

const std::vector<real_t> Euler2DRoe::numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	std::vector<real_t> fluxjacobi2(num_states_*num_states_, 0.0);
	std::vector<real_t> fluxjacobi_1(16, 0.0);
	std::vector<real_t> fluxjacobi_2(16, 0.0);
	std::vector<real_t> fluxjacobi_3(16, 0.0);
	std::vector<real_t> fluxjacobi_4(16, 0.0);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& d1 = owner_u[ipoint*num_states_];
		const real_t& du1 = owner_u[ipoint*num_states_ + 1];
		const real_t& dv1 = owner_u[ipoint*num_states_ + 2];
		const real_t& dE1 = owner_u[ipoint*num_states_ + 3];
		const real_t& p1 = owner_pressure[ipoint];
		const real_t& d2 = neighbor_u[ipoint*num_states_];
		const real_t& du2 = neighbor_u[ipoint*num_states_ + 1];
		const real_t& dv2 = neighbor_u[ipoint*num_states_ + 2];
		const real_t& dE2 = neighbor_u[ipoint*num_states_ + 3];
		const real_t& p2 = neighbor_pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V1 = (du1*nx + dv1 * ny) / d1;
		const real_t V2 = (du2*nx + dv2 * ny) / d2;

		// row 0
		fluxjacobi2[1] = nx;
		fluxjacobi2[2] = ny;
		// row 1
		fluxjacobi2[4] = 0.2*nx*(du2*du2 + dv2 * dv2) / (d2*d2) - du2 * V2 / d2;
		fluxjacobi2[5] = V2 + 0.6*du2 * nx / d2;
		fluxjacobi2[6] = du2 * ny / d2 - 0.4*dv2*nx / d2;
		fluxjacobi2[7] = 0.4*nx;
		// row 2
		fluxjacobi2[8] = 0.2*ny*(du2*du2 + dv2 * dv2) / (d2*d2) - dv2 * V2 / d2;
		fluxjacobi2[9] = dv2 * nx / d2 - 0.4*du2*ny / d2;
		fluxjacobi2[10] = V2 + 0.6*dv2 * ny / d2;
		fluxjacobi2[11] = 0.4*ny;
		// row 3
		fluxjacobi2[12] = 0.2*(du2*du2 + dv2 * dv2)*V2 / (d2*d2) - (dE2 + p2)*V2 / d2;
		fluxjacobi2[13] = nx * (dE2 + p2) / d2 - 0.4*du2*V2 / d2;
		fluxjacobi2[14] = ny * (dE2 + p2) / d2 - 0.4*dv2*V2 / d2;
		fluxjacobi2[15] = 1.4*V2;

		const real_t dsqrt1 = std::sqrt(d1);
		const real_t dsqrt2 = std::sqrt(d2);
		const real_t dsqrt1p2 = dsqrt1 + dsqrt2;

		const real_t Rd = dsqrt1 * dsqrt2;
		const real_t Ru = (du1 / dsqrt1 + du2 / dsqrt2) / dsqrt1p2;
		const real_t Rv = (dv1 / dsqrt1 + dv2 / dsqrt2) / dsqrt1p2;
		const real_t Rh = ((dE1 + p1) / dsqrt1 + (dE2 + p2) / dsqrt2) / dsqrt1p2;
		const real_t Ra = std::sqrt(0.4*(Rh - 0.5*(Ru*Ru + Rv * Rv)));
		const real_t RV = (V1*dsqrt1 + V2 * dsqrt2) / dsqrt1p2;

		const std::vector<real_t> jacobian_Rd = { 0.5*dsqrt1 / dsqrt2, 0.0, 0.0, 0.0 };
		const std::vector<real_t> jacobian_Ru = { -0.5*(du2 / d2 + Ru) / (dsqrt2*dsqrt1p2), 1.0 / (dsqrt2*dsqrt1p2), 0.0, 0.0 };
		const std::vector<real_t> jacobian_Rv = { -0.5*(dv2 / d2 + Rv) / (dsqrt2*dsqrt1p2), 0.0, jacobian_Ru[1], 0.0 };
		const std::vector<real_t> jacobian_Rh = { -0.5*((dE2 + p2) / d2 + Rh + 0.4*(du2*du2 + dv2 * dv2) / (d2*d2)) / (dsqrt2*dsqrt1p2), -0.4*du2 / (d2*dsqrt2*dsqrt1p2), -0.4*dv2 / (d2*dsqrt2*dsqrt1p2), 1.4*jacobian_Ru[1] };
		const std::vector<real_t> jacobian_Ra = { 0.2*(jacobian_Rh[0] - Ru * jacobian_Ru[0] - Rv * jacobian_Rv[0]) / Ra, 0.2*(-Ru * jacobian_Ru[1]) / Ra, 0.2*(-Rv * jacobian_Rv[2]) / Ra, 0.2*jacobian_Rh[3] / Ra };
		const std::vector<real_t> jacobian_RV = { -0.5*(V2 + (V1 * dsqrt1 + V2 * dsqrt2) / dsqrt1p2) / (dsqrt2*dsqrt1p2), nx / (dsqrt2*dsqrt1p2), ny / (dsqrt2*dsqrt1p2), 0.0 };
		const std::vector<real_t> jacobian_dp = { 0.2*(du2*du2 + dv2 * dv2) / (d2*d2),-0.4*du2 / d2,-0.4*dv2 / d2,0.4 };

		const real_t dd = d2 - d1;
		const real_t du = du2 / d2 - du1 / d1;
		const real_t dv = dv2 / d2 - dv1 / d1;
		const real_t dp = p2 - p1;
		const real_t dV = V2 - V1;

		real_t temp = dd - dp / (Ra*Ra);
		std::vector<real_t> temp_jacobian = { 1.0, 0.0, 0.0, 0.0 };
		for (int_t i = 0; i < 4; i++)
		{
			fluxjacobi_1[i] = (double)sgn(RV)*jacobian_RV[i] * temp + std::abs(RV)*(temp_jacobian[i] - (jacobian_dp[i]*Ra - 2.0*dp*jacobian_Ra[i]) / pow(Ra,3.0));
			fluxjacobi_1[4 + i] = fluxjacobi_1[i] * Ru + std::abs(RV)*temp*jacobian_Ru[i];
			fluxjacobi_1[8 + i] = fluxjacobi_1[i] * Rv + std::abs(RV)*temp*jacobian_Rv[i];
			fluxjacobi_1[12 + i] = fluxjacobi_1[i] * 0.5*(Ru*Ru + Rv * Rv) + std::abs(RV)*temp*(Ru*jacobian_Ru[i] + Rv * jacobian_Rv[i]);
		}

		temp = du * ny - dv * nx;
		temp_jacobian = { (dv2*nx - du2 * ny) / (d2*d2),ny / d2,-nx / d2,0 };
		for (int_t i = 0; i < 4; i++)
		{
			fluxjacobi_2[4 + i] = ((double)sgn(RV)*jacobian_RV[i] * Rd*temp + std::abs(RV)*jacobian_Rd[i] * temp + std::abs(RV)*Rd*temp_jacobian[i])*ny;
			fluxjacobi_2[8 + i] = -((double)sgn(RV)*jacobian_RV[i] * Rd*temp + std::abs(RV)*jacobian_Rd[i] * temp + std::abs(RV)*Rd*temp_jacobian[i])*nx;
			fluxjacobi_2[12 + i] = fluxjacobi_2[4 + i] * Ru + std::abs(RV)*Rd*temp*ny*jacobian_Ru[i] + fluxjacobi_2[8 + i] * Rv - std::abs(RV)*Rd*temp*nx*jacobian_Rv[i];
		}

		temp = (dp + Rd * Ra*dV)*0.5 / (Ra*Ra);
		temp_jacobian = { -V2 / d2,nx / d2,ny / d2,0.0 };
		for (int_t i = 0; i < 4; i++)
		{
			fluxjacobi_3[i] = (double)sgn(RV + Ra)*(jacobian_RV[i] + jacobian_Ra[i])*temp + 0.5*std::abs(RV + Ra)*(jacobian_dp[i] * Ra - 2.0*dp*jacobian_Ra[i] + pow(Ra, 2.0)*dV*jacobian_Rd[i] - (Rd * Ra * dV + 2.0*dp)*jacobian_Ra[i] + Rd * pow(Ra, 2.0)*temp_jacobian[i]) / pow(Ra, 3.0);
			fluxjacobi_3[4 + i] = fluxjacobi_3[i] * (Ru + nx * Ra) + std::abs(RV + Ra)*temp * (jacobian_Ru[i] + nx * jacobian_Ra[i]);
			fluxjacobi_3[8 + i] = fluxjacobi_3[i] * (Rv + ny * Ra) + std::abs(RV + Ra)*temp * (jacobian_Rv[i] + ny * jacobian_Ra[i]);
			fluxjacobi_3[12 + i] = fluxjacobi_3[i] * (Rh + Ra * RV) + std::abs(RV + Ra)*temp * (jacobian_Rh[i] + Ra * jacobian_RV[i] + jacobian_Ra[i] * RV);
		}

		temp = (dp - Rd * Ra*dV)*0.5 / (Ra*Ra);
		for (int_t i = 0; i < 4; i++)
		{
			fluxjacobi_4[i] = (double)sgn(RV - Ra)*(jacobian_RV[i] - jacobian_Ra[i])*temp + 0.5*std::abs(RV - Ra)*(jacobian_dp[i] * Ra - 2.0*dp*jacobian_Ra[i] - pow(Ra, 2.0)*dV*jacobian_Rd[i] + (Rd * Ra * dV - 2.0*dp)*jacobian_Ra[i] - Rd * pow(Ra, 2.0)*temp_jacobian[i]) / pow(Ra, 3.0);
			fluxjacobi_4[4 + i] = fluxjacobi_4[i] * (Ru - nx * Ra) + std::abs(RV - Ra)*temp * (jacobian_Ru[i] - nx * jacobian_Ra[i]);
			fluxjacobi_4[8 + i] = fluxjacobi_4[i] * (Rv - ny * Ra) + std::abs(RV - Ra)*temp * (jacobian_Rv[i] - ny * jacobian_Ra[i]);
			fluxjacobi_4[12 + i] = fluxjacobi_4[i] * (Rh - Ra * RV) + std::abs(RV - Ra)*temp * (jacobian_Rh[i] - Ra * jacobian_RV[i] - jacobian_Ra[i] * RV);
		}

		for (int_t icomp = 0; icomp < num_states_*num_states_; icomp++)
			FluxJacobi[ipoint*num_states_*num_states_ + icomp] = 0.5*(fluxjacobi2[icomp] - fluxjacobi_1[icomp] - fluxjacobi_2[icomp] - fluxjacobi_3[icomp] - fluxjacobi_4[icomp]);
	}

	return FluxJacobi;
}

Euler2DAUSM::Euler2DAUSM()
{
	setName("AUSM");
	Euler2DFlux::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DAUSM::numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> Flux(num_points*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t& d1 = owner_u[ipoint*num_states_];
		const real_t& du1 = owner_u[ipoint*num_states_ + 1];
		const real_t& dv1 = owner_u[ipoint*num_states_ + 2];
		const real_t& dE1 = owner_u[ipoint*num_states_ + 3];
		const real_t& p1 = owner_pressure[ipoint];
		const real_t& d2 = neighbor_u[ipoint*num_states_];
		const real_t& du2 = neighbor_u[ipoint*num_states_ + 1];
		const real_t& dv2 = neighbor_u[ipoint*num_states_ + 2];
		const real_t& dE2 = neighbor_u[ipoint*num_states_ + 3];
		const real_t& p2 = neighbor_pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V1 = (du1*nx + dv1*ny) / d1;
		const real_t V2 = (du2*nx + dv2*ny) / d2;
		const real_t a1 = std::sqrt(1.4*p1 / d1);
		const real_t a2 = std::sqrt(1.4*p2 / d2);
		const real_t M1 = V1 / a1;
		const real_t M2 = V2 / a2;

		real_t M;
		if (std::abs(M1) <= 1)
			M = 0.25*(M1 + 1.0)*(M1 + 1.0);
		else
			M = 0.5*(M1 + std::abs(M1));
		if (std::abs(M2) <= 1)
			M += -0.25*(M2 - 1.0)*(M2 - 1.0);
		else
			M += 0.5*(M2 - std::abs(M2));

		if (M >= 0){
			Flux[ipoint*num_states_ + 0] = M*a1*d1;
			Flux[ipoint*num_states_ + 1] = M*a1*du1;
			Flux[ipoint*num_states_ + 2] = M*a1*dv1;
			Flux[ipoint*num_states_ + 3] = M*a1*(dE1 + p1);
		}
		else{
			Flux[ipoint*num_states_ + 0] = M*a2*d2;
			Flux[ipoint*num_states_ + 1] = M*a2*du2;
			Flux[ipoint*num_states_ + 2] = M*a2*dv2;
			Flux[ipoint*num_states_ + 3] = M*a2*(dE2 + p2);
		}

		real_t p;
		if (std::abs(M1) <= 1)
			p = 0.5*p1*(1.0 + M1);
		else
			p = 0.5*p1*(M1 + std::abs(M1))/M1;
		if (std::abs(M2) <= 1)
			p += 0.5*p2*(1.0 - M2);
		else
			p += 0.5*p2*(M2 - std::abs(M2)) / M2;

		Flux[ipoint*num_states_ + 1] += nx*p;
		Flux[ipoint*num_states_ + 2] += ny*p;
	}

	return Flux;
}

const std::vector<real_t> Euler2DAUSM::numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> Euler2DAUSM::numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_);

	// jacobian

	return FluxJacobi;
}