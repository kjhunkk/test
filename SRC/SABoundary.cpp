
#include "../INC/SABoundary.h"

const std::vector<real_t> SABCExtrapolate::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFlux(owner_u, normal);
}
const std::vector<real_t> SABCExtrapolate::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFluxJacobian(owner_u, normal);
}
const std::vector<real_t> SABCExtrapolate::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFlux(owner_u, owner_div_u, normal);
}
const std::vector<real_t> SABCExtrapolate::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> SABCExtrapolate::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxGradJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> SABCExtrapolate::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{
	return owner_u;
}
const std::vector<real_t> SABCExtrapolate::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{
	return std::vector<real_t>(owner_u.size(), 1.0);
}

const std::vector<real_t> SABCConstant::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	return flux_->numConvFlux(owner_u, references, normal);
}
const std::vector<real_t> SABCConstant::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	return flux_->numConvFluxOwnerJacobian(owner_u, references, normal);
}
const std::vector<real_t> SABCConstant::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFlux(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> SABCConstant::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> SABCConstant::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFluxOwnerGradJacobian(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> SABCConstant::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	return references;
}
const std::vector<real_t> SABCConstant::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{
	return std::vector<real_t>(owner_u.size(), 0.0);
}