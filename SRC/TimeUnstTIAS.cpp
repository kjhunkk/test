
#include "../INC/TimeUnstTIAS.h"

TimeUnstTIAS::TimeUnstTIAS()
{
	setName("TIAS");
	TimeScheme::getInstance().addRegistry(getName(), this);
	is_implicit_ = true;
}

TimeUnstTIAS::~TimeUnstTIAS()
{
	PetscErrorCode ierr;
	ierr = VecDestroy(&delta_);
	ierr = VecDestroy(&intermediate_solution_vector_);
	ierr = VecDestroy(&TIAS_rhs_);
	VECTOR_ARRAY_DESTROYER(stage_rhs_, ierr);
	VECTOR_ARRAY_DESTROYER(stage_solution_array_, ierr);
	VECTOR_ARRAY_DESTROYER(solution_array_, ierr);
}

void TimeUnstTIAS::initialize()
{
	const Config& config = Config::getInstance();

	// Parent member variables
	if (config.getIsFixedTimeStep().compare("yes") == 0)
		is_fixed_dt_ = true;
	else
	{
		is_fixed_dt_ = false;
		MASTER_ERROR("Adaptive time step is not supported");
	}

	// Time integration configuration
	time_order_ = config.getTimeOrder();
	diagonal_factor_ = 0.0;
	InitializeCoeff();
	PetscErrorCode ierr;
	ierr = VecCreate(MPI_COMM_WORLD, &delta_);
	ierr = VecCreate(MPI_COMM_WORLD, &intermediate_solution_vector_);
	ierr = VecCreate(MPI_COMM_WORLD, &TIAS_rhs_);
	VECTOR_ARRAY_CREATOR(stage_rhs_, ierr);
	VECTOR_ARRAY_CREATOR(stage_solution_array_, ierr);
	solution_array_.resize(time_order_ - 1);
	VECTOR_ARRAY_CREATOR(solution_array_, ierr);

	// Non-linear solver configuration
	max_newton_iteration_ = 1000;
	newton_tolerance_ = 1.0e-8;
	recompute_tol_ = 0.2;

	// Linear solver configuration
	linear_system_solver_ = LinearSystemSolver::getInstance().getType(config.getLinearSystemSolver());
	linear_system_solver_->Initialize();

	// etc
	is_initialized_ = false;
}

void TimeUnstTIAS::integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
	if (is_initialized_ == false)
	{
		InitializeVectors(zone, dt);
		SelfStartingProcedure(zone, dt, current_time, iteration);
		is_initialized_ = true;
	}

	diagonal_factor_ = 1.0 / dt;
	int_t internal_iteration = 0;

	const int_t num_cells = zone->getNumCells();
	const std::vector<int_t> cell_petsc_index = zone->getCellPetscIndex();
	real_t newton_norm = 0.0;
	internal_iteration = SolveStage1(zone, num_cells, cell_petsc_index, newton_norm);
	internal_iteration = SolveStage2(zone, num_cells, cell_petsc_index, newton_norm);
	internal_iteration = SolveStage3(zone, num_cells, cell_petsc_index, newton_norm);
	internal_iteration = SolveStage4(zone, num_cells, cell_petsc_index, newton_norm);

	real_t error = 0;
	VecNorm(TIAS_rhs_, NORM_2, &error);
	status_string_ = "Iteration=" + TO_STRING(internal_iteration) + " L2=" + TO_STRING((double)error);
}

void TimeUnstTIAS::InitializeVectors(std::shared_ptr<Zone> zone, const real_t dt)
{
	const Vec& vec_initializer = zone->getSystemRHS();
	VecDuplicate(vec_initializer, &delta_);
	VecDuplicate(vec_initializer, &intermediate_solution_vector_);
	intermediate_solution_ = zone->getSolution();
	VecDuplicate(vec_initializer, &TIAS_rhs_);
	for(int_t i = 0; i < stage_rhs_.size(); i++)
		VecDuplicate(vec_initializer, &stage_rhs_[i]);
	for (int_t i = 0; i < stage_solution_array_.size(); i++)
		VecDuplicate(vec_initializer, &stage_solution_array_[i]);
	for (int_t i = 0; i < solution_array_.size(); i++)
		VecDuplicate(vec_initializer, &solution_array_[i]);
}

int_t TimeUnstTIAS::SolveStage1(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm)
{
	// Newton-Rapson iteration
	VecCopy(stage_solution_array_[1], intermediate_solution_vector_);
	ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);
	system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_ / BDF_beta_, 1.0);
	int_t newton_iteration = 0;
	int_t krylov_iteration = 0;
	real_t newton_current_norm = 0.0;
	real_t newton_previous_norm = newton_norm;
	
	while (newton_iteration++ < max_newton_iteration_)
	{
		// RHS formulation
		VecCopy(intermediate_solution_vector_, TIAS_rhs_);
		for (int_t istep = 0; istep < time_order_ - 1; istep++)
			VecAXPY(TIAS_rhs_, BDF_alpha_[istep], solution_array_[istep]);
		VecScale(TIAS_rhs_, -diagonal_factor_ / BDF_beta_);
		VecAXPY(TIAS_rhs_, 1.0, zone->calculateInstantSystemRHS(intermediate_solution_));
		
		// Krylov solve
		krylov_iteration = linear_system_solver_->Solve(system_matrix_, TIAS_rhs_, delta_);
		VecNorm(delta_, NORM_2, &newton_current_norm);

		// intermediate solution update
		VecAXPY(delta_, 1.0, intermediate_solution_vector_);
		VecCopy(delta_, intermediate_solution_vector_);
		ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);

		// Check convergence
		if (newton_current_norm < newton_tolerance_) break;
		if ((newton_current_norm / newton_previous_norm) > recompute_tol_)
			system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_ / BDF_beta_, 1.0);
		newton_previous_norm = newton_current_norm;
	}	

	newton_norm = newton_current_norm;
	VecCopy(intermediate_solution_vector_, stage_solution_array_[0]);
	VecCopy(intermediate_solution_vector_, stage_rhs_[0]);
	for (int_t istep = 0; istep < time_order_ - 1; istep++)
		VecAXPY(stage_rhs_[0], BDF_alpha_[istep], solution_array_[istep]);
	VecScale(stage_rhs_[0], -diagonal_factor_ / BDF_beta_);
	return newton_iteration;
}

int_t TimeUnstTIAS::SolveStage2(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm)
{
	// Newton-Rapson iteration
	VecCopy(stage_solution_array_[2], intermediate_solution_vector_);
	ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);
	int_t newton_iteration = 0;
	int_t krylov_iteration = 0;
	real_t newton_current_norm = 0.0;
	real_t newton_previous_norm = newton_norm;
	
	while (newton_iteration++ < max_newton_iteration_)
	{
		// RHS formulation
		VecCopy(intermediate_solution_vector_, TIAS_rhs_);
		VecAXPY(TIAS_rhs_, BDF_alpha_[time_order_ - 2], stage_solution_array_[0]);
		for (int_t istep = 1; istep < time_order_ - 1; istep++)
			VecAXPY(TIAS_rhs_, BDF_alpha_[istep - 1], solution_array_[istep]);
		VecScale(TIAS_rhs_, -diagonal_factor_ / BDF_beta_);
		VecAXPY(TIAS_rhs_, 1.0, zone->calculateInstantSystemRHS(intermediate_solution_));

		// Krylov solve
		krylov_iteration = linear_system_solver_->Solve(system_matrix_, TIAS_rhs_, delta_);
		VecNorm(delta_, NORM_2, &newton_current_norm);

		// intermediate solution update
		VecAXPY(delta_, 1.0, intermediate_solution_vector_);
		VecCopy(delta_, intermediate_solution_vector_);
		ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);

		// Check convergence
		if (newton_current_norm < newton_tolerance_) break;
		else if (newton_current_norm / newton_previous_norm > recompute_tol_)
			system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_ / BDF_beta_, 1.0);
		newton_previous_norm = newton_current_norm;
	}

	newton_norm = newton_current_norm;
	VecCopy(intermediate_solution_vector_, stage_solution_array_[1]);
	VecCopy(intermediate_solution_vector_, stage_rhs_[1]);
	VecAXPY(stage_rhs_[1], BDF_alpha_[time_order_ - 2], stage_solution_array_[0]);
	for (int_t istep = 1; istep < time_order_ - 1; istep++)
		VecAXPY(stage_rhs_[1], BDF_alpha_[istep - 1], solution_array_[istep]);
	VecScale(stage_rhs_[1], -diagonal_factor_ / BDF_beta_);
	return newton_iteration;
}

int_t TimeUnstTIAS::SolveStage3(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm)
{
	// Newton-Rapson iteration
	VecCopy(stage_solution_array_[1], intermediate_solution_vector_);
	ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);
	int_t newton_iteration = 0;
	int_t krylov_iteration = 0;
	real_t newton_current_norm = 0.0;
	real_t newton_previous_norm = newton_norm;
	
	while (newton_iteration++ < max_newton_iteration_)
	{
		// RHS formulation
		VecCopy(intermediate_solution_vector_, TIAS_rhs_);
		VecAXPY(TIAS_rhs_, BDF_alpha_[time_order_ - 3], stage_solution_array_[0]);
		VecAXPY(TIAS_rhs_, BDF_alpha_[time_order_ - 2], stage_solution_array_[1]);
		for (int_t istep = 2; istep < time_order_ - 1; istep++)
			VecAXPY(TIAS_rhs_, BDF_alpha_[istep - 2], solution_array_[istep]);
		VecScale(TIAS_rhs_, -diagonal_factor_ / BDF_beta_);
		VecAXPY(TIAS_rhs_, 1.0, zone->calculateInstantSystemRHS(intermediate_solution_));

		// Krylov solve
		krylov_iteration = linear_system_solver_->Solve(system_matrix_, TIAS_rhs_, delta_);
		VecNorm(delta_, NORM_2, &newton_current_norm);

		// intermediate solution update
		VecAXPY(delta_, 1.0, intermediate_solution_vector_);
		VecCopy(delta_, intermediate_solution_vector_);
		ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);

		// Check convergence
		if (newton_current_norm < newton_tolerance_) break;
		else if (newton_current_norm / newton_previous_norm > recompute_tol_)
			system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_ / BDF_beta_, 1.0);
		newton_previous_norm = newton_current_norm;
	}

	newton_norm = newton_current_norm;
	VecCopy(intermediate_solution_vector_, stage_solution_array_[2]);
	VecCopy(intermediate_solution_vector_, stage_rhs_[2]);
	VecAXPY(stage_rhs_[2], BDF_alpha_[time_order_ - 3], stage_solution_array_[0]);
	VecAXPY(stage_rhs_[2], BDF_alpha_[time_order_ - 2], stage_solution_array_[1]);
	for (int_t istep = 2; istep < time_order_ - 1; istep++)
		VecAXPY(stage_rhs_[2], BDF_alpha_[istep - 2], solution_array_[istep]);
	VecScale(stage_rhs_[2], -diagonal_factor_ / BDF_beta_);
	return newton_iteration;
}

int_t TimeUnstTIAS::SolveStage4(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm)
{
	// Newton-Rapson iteration
	VecCopy(stage_solution_array_[0], intermediate_solution_vector_);
	ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);
	//system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_ / (TIAS_beta_[0] - TIAS_flat_beta_), 1.0);
	MatShift(system_matrix_, -diagonal_factor_ / BDF_beta_ + diagonal_factor_ / (TIAS_beta_[0] - TIAS_flat_beta_));
	int_t newton_iteration = 0;
	int_t krylov_iteration = 0;
	real_t newton_current_norm = 0.0;
	real_t newton_previous_norm = 1000.0;
	
	while (newton_iteration++ < max_newton_iteration_)
	{
		// RHS formulation
		VecCopy(intermediate_solution_vector_, TIAS_rhs_);
		for (int_t istep = 0; istep < time_order_ - 1; istep++)
			VecAXPY(TIAS_rhs_, TIAS_alpha_[istep], solution_array_[istep]);
		VecScale(TIAS_rhs_, diagonal_factor_);
		VecAXPY(TIAS_rhs_, TIAS_flat_beta_, stage_rhs_[0]);
		VecAXPY(TIAS_rhs_, TIAS_beta_[1], stage_rhs_[1]);
		VecAXPY(TIAS_rhs_, TIAS_beta_[2], stage_rhs_[2]);
		VecScale(TIAS_rhs_, -1.0 / (TIAS_beta_[0] - TIAS_flat_beta_));
		VecAXPY(TIAS_rhs_, 1.0, zone->calculateInstantSystemRHS(intermediate_solution_));

		// Krylov solve
		krylov_iteration = linear_system_solver_->Solve(system_matrix_, TIAS_rhs_, delta_);
		VecNorm(delta_, NORM_2, &newton_current_norm);

		// intermediate solution update
		VecAXPY(delta_, 1.0, intermediate_solution_vector_);
		VecCopy(delta_, intermediate_solution_vector_);
		ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);

		// Check convergence
		if (newton_current_norm < newton_tolerance_) break;
		else if (newton_current_norm / newton_previous_norm > recompute_tol_)
			system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_ / (TIAS_beta_[0] - TIAS_flat_beta_), 1.0);
		newton_previous_norm = newton_current_norm;
	}

	newton_norm = newton_current_norm;
	zone->UpdateSolutionFromVec(intermediate_solution_vector_);
	zone->communication();

	for (int_t istep = 1; istep < solution_array_.size(); istep++)
		VecCopy(solution_array_[istep], solution_array_[istep - 1]);
	VecCopy(intermediate_solution_vector_, solution_array_[solution_array_.size() - 1]);

	return newton_iteration;
}

void TimeUnstTIAS::InitializeCoeff()
{
	BDF_alpha_.clear();
	TIAS_alpha_.clear();
	BDF_alpha_.resize(time_order_ - 1);
	TIAS_alpha_.resize(time_order_ - 1);

	switch (time_order_)
	{
	case 2:
		//BDF_alpha_ = { -1.0 };
		//BDF_beta_ = 1.0;
		//TIAS_alpha_ = { 1.0 };
		//TIAS_beta_ = { 3.0 / 2.0, -1.0 / 2.0 };
		MASTER_ERROR("Not supported : time order (" + TO_STRING(time_order_) + ")");
		break;

	case 3:
		//BDF_alpha_ = { 1.0 / 3.0, -4.0 / 3.0 };
		//BDF_beta_ = 2.0 / 3.0;
		//TIAS_alpha_ = { 1.0, 5.0 / 23.0 };
		//TIAS_beta_ = { 22.0 / 23.0, -4.0 / 23.0 };
		MASTER_ERROR("Not supported : time order (" + TO_STRING(time_order_) + ")");
		break;

	case 4:
		// BDF coefficients
		BDF_alpha_ = { -2.0/11.0, 9.0 / 11.0, -18.0 / 11.0 };
		BDF_beta_ = 6.0 / 11.0;

		// TIAS coefficients
		TIAS_flat_beta_ = 0.32;
		TIAS_beta_[2] = 0.23;
		TIAS_alpha_ = { -17.0 / 197.0 + 165.0 / 197.0 * TIAS_beta_[2], 99.0 / 197.0 - 648.0 / 197.0 * TIAS_beta_[2], -279.0 / 197.0 + 483.0 / 197.0 * TIAS_beta_[2] };
		TIAS_beta_[0] = 150.0 / 197.0 + 827.0 / 197.0 * TIAS_beta_[2];
		TIAS_beta_[1] = -18.0 / 197.0 - 706.0 / 197.0 * TIAS_beta_[2];
		break;

	case 5:
		// BDF coefficients
		BDF_alpha_ = { 3.0/25.0, -16.0 / 25.0, 36.0 / 25.0, -48.0 / 25.0 };
		BDF_beta_ = 12.0 / 25.0;

		// TIAS coefficients
		TIAS_flat_beta_ = 0.1;
		TIAS_beta_[2] = 0.1;
		TIAS_alpha_ = { 111.0 / 2501.0 - 8018.0 / 7503.0 * TIAS_beta_[2], -728.0 / 2501.0 + 14427.0 / 2501.0 * TIAS_beta_[2], 2124.0 / 2501.0 - 29106.0 / 2501.0 * TIAS_beta_[2], -4008.0 / 2501.0 + 52055.0 / 7503.0 * TIAS_beta_[2] };
		TIAS_beta_[0] = 1644.0 / 2501.0 + 16767.0 / 2501.0 * TIAS_beta_[2];
		TIAS_beta_[1] = -144.0 / 2501.0 - 10998.0 / 2501.0*TIAS_beta_[2];
		break;

	case 6:
		// BDF coefficients
		BDF_alpha_ = { -12.0 / 137.0, 75.0 / 137.0, -200.0 / 137.0, 300.0 / 137.0, -300.0 / 137.0 };
		BDF_beta_ = 60.0 / 137.0;

		// TIAS coefficients
		TIAS_flat_beta_ = 0.1;
		TIAS_beta_[2] = 0.05;
		TIAS_alpha_ = { -394.0 / 14919.0 + 74711.0 / 59676.0 * TIAS_beta_[2],
			2925.0 / 14919.0 - 123215.0 / 14919.0 * TIAS_beta_[2],
			-3200.0 / 4973.0 + 111762.0 / 4973.0 * TIAS_beta_[2],
			18700.0 / 14919.0 - 459473.0 / 14919.0 * TIAS_beta_[2],
			-26550.0 / 14919.0 + 914897.0 / 59676.0 * TIAS_beta_[2] };
		TIAS_beta_[0] = 2940.0 / 4973.0 + 48933.0 / 4973.0 * TIAS_beta_[2];
		TIAS_beta_[1] = -200.0 / 4973.0 - 25961.0 / 4973.0 * TIAS_beta_[2];
		break;

	default:
		MASTER_ERROR("Time order of TIAS is not A-stabe : time order (" + TO_STRING(time_order_) + ")");
	}
}

void TimeUnstTIAS::ConvertSolutionToPetsc(const std::vector<std::vector<real_t> >& solution, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, Vec& vector)
{
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	PetscScalar *vector_array;
	const PetscInt num_var = solution[0].size();
	VecGetArray(vector, &vector_array);
	for (int_t icell = 0; icell < num_cells; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			vector_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar] = solution[icell][ivar];
	VecRestoreArray(vector, &vector_array);
}

void TimeUnstTIAS::ConvertPetscToSolution(std::shared_ptr<Zone> zone, const Vec& vector, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, std::vector<std::vector<real_t> > & solution)
{
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	const PetscScalar *vector_array;
	const PetscInt num_var = solution[0].size();
	VecGetArrayRead(vector, &vector_array);
	for (int_t icell = 0; icell < num_cells; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			solution[icell][ivar] = vector_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar];
	VecRestoreArrayRead(vector, &vector_array);
	zone->RequestCommunication(solution);
}

void TimeUnstTIAS::SelfStartingProcedure(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
#ifdef _Order_Test_
	for (int_t istep = 0; istep < solution_array_.size(); istep++)
		ConvertSolutionToPetsc(zone->getExactSolution(dt * (double)(istep - time_order_ + 2)), zone->getNumCells(), zone->getCellPetscIndex(), solution_array_[istep]);
#else
	const int_t num_cells = zone->getNumCells();
	const std::vector<int_t> cell_petsc_index = zone->getCellPetscIndex();
	std::vector<std::vector<real_t> > temp_solution = zone->getSolution();
	std::vector<Vec> Y(time_order_ - 1), F(time_order_ - 1);
	switch (time_order_) {
	case 4:
		ConvertSolutionToPetsc(zone->getSolution(), num_cells, cell_petsc_index, solution_array_[0]);
		for (int_t i = 0; i < 3; i++)
		{
			VecCreate(MPI_COMM_WORLD, &Y[i]); VecCreate(MPI_COMM_WORLD, &F[i]);
			VecDuplicate(solution_array_[0], &Y[i]); VecDuplicate(solution_array_[0], &F[i]);
		}

		// 1st step
		VecCopy(solution_array_[0], Y[0]);
		VecCopy(zone->calculateInstantSystemRHS(zone->getSolution()), F[0]);
		VecCopy(Y[0], Y[2]);
		VecAXPY(Y[2], 2 * dt, F[0]);
		ConvertPetscToSolution(zone, Y[2], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[2]);

		// 2nd step
		VecCopy(Y[0], Y[1]); VecCopy(Y[0], Y[2]);
		VecAXPY(Y[1], 0.75*dt, F[0]); VecAXPY(Y[1], 0.25*dt, F[2]);
		VecAXPY(Y[2], dt, F[0]); VecAXPY(Y[2], dt, F[2]);
		ConvertPetscToSolution(zone, Y[1], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[1]);
		ConvertPetscToSolution(zone, Y[2], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[2]);

		// Final
		VecCopy(solution_array_[0], solution_array_[1]);
		VecAXPY(solution_array_[1], 5.0*dt / 12.0, F[0]);
		VecAXPY(solution_array_[1], 2.0*dt / 3.0, F[1]);
		VecAXPY(solution_array_[1], -dt / 12.0, F[2]);
		VecCopy(solution_array_[0], solution_array_[2]);
		VecAXPY(solution_array_[2], dt / 3.0, F[0]);
		VecAXPY(solution_array_[2], 4.0*dt / 3.0, F[1]);
		VecAXPY(solution_array_[2], dt / 3.0, F[2]);
		current_time += 2.0*dt;
		iteration += 2;
		for (int_t i = 0; i < 3; i++) {
			VecDestroy(&Y[i]); VecDestroy(&F[i]);
		}
		break;
	case 5:
		ConvertSolutionToPetsc(zone->getSolution(), num_cells, cell_petsc_index, solution_array_[0]);
		for (int_t i = 0; i < 4; i++)
		{
			VecCreate(MPI_COMM_WORLD, &Y[i]); VecCreate(MPI_COMM_WORLD, &F[i]);
			VecDuplicate(solution_array_[0], &Y[i]); VecDuplicate(solution_array_[0], &F[i]);
		}

		// 1st step
		VecCopy(solution_array_[0], Y[0]);
		VecCopy(zone->calculateInstantSystemRHS(zone->getSolution()), F[0]);
		VecCopy(Y[0], Y[3]);
		VecAXPY(Y[3], 3 * dt, F[0]);
		ConvertPetscToSolution(zone, Y[3], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[3]);

		// 2nd step
		VecCopy(Y[0], Y[1]); VecCopy(Y[0], Y[3]);
		VecAXPY(Y[1], 5.0 * dt / 6.0, F[0]); VecAXPY(Y[1], dt / 6.0, F[3]);
		VecAXPY(Y[3], 3.0 * dt / 2.0, F[0]); VecAXPY(Y[3], 3.0 * dt / 2.0, F[3]);
		ConvertPetscToSolution(zone, Y[1], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[1]);
		ConvertPetscToSolution(zone, Y[3], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[3]);

		// 3rd step
		VecCopy(Y[0], Y[1]); VecCopy(Y[0], Y[2]); VecCopy(Y[0], Y[3]);
		VecAXPY(Y[1], 4.0 * dt / 9.0, F[0]); VecAXPY(Y[1], 7.0 * dt / 12.0, F[1]); VecAXPY(Y[1], -dt / 36.0, F[3]);
		VecAXPY(Y[2], 2.0 * dt / 9.0, F[0]); VecAXPY(Y[2], 5.0 * dt / 3.0, F[1]); VecAXPY(Y[2], dt / 9.0, F[3]);
		VecAXPY(Y[3], 9.0 * dt / 4.0, F[1]); VecAXPY(Y[3], 3.0 * dt / 4.0, F[3]);
		ConvertPetscToSolution(zone, Y[1], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[1]);
		ConvertPetscToSolution(zone, Y[2], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[2]);
		ConvertPetscToSolution(zone, Y[3], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[3]);

		// Final
		VecCopy(solution_array_[0], solution_array_[1]);
		VecAXPY(solution_array_[1], 9.0*dt / 24.0, F[0]);
		VecAXPY(solution_array_[1], 19.0*dt / 24.0, F[1]);
		VecAXPY(solution_array_[1], -5.0*dt / 24.0, F[2]);
		VecAXPY(solution_array_[1], dt / 24.0, F[3]);
		VecCopy(solution_array_[0], solution_array_[2]);
		VecAXPY(solution_array_[2], dt / 3.0, F[0]);
		VecAXPY(solution_array_[2], 4.0*dt / 3.0, F[1]);
		VecAXPY(solution_array_[2], dt / 3.0, F[2]);
		VecCopy(solution_array_[0], solution_array_[3]);
		VecAXPY(solution_array_[3], 3.0*dt / 8.0, F[0]);
		VecAXPY(solution_array_[3], 9.0*dt / 8.0, F[1]);
		VecAXPY(solution_array_[3], 9.0*dt / 8.0, F[2]);
		VecAXPY(solution_array_[3], 3.0*dt / 8.0, F[3]);
		current_time += 3.0*dt;
		iteration += 3;
		for (int_t i = 0; i < 4; i++) {
			VecDestroy(&Y[i]); VecDestroy(&F[i]);
		}
		break;
	case 6:
		ConvertSolutionToPetsc(zone->getSolution(), num_cells, cell_petsc_index, solution_array_[0]);
		for (int_t i = 0; i < 5; i++)
		{
			VecCreate(MPI_COMM_WORLD, &Y[i]); VecCreate(MPI_COMM_WORLD, &F[i]);
			VecDuplicate(solution_array_[0], &Y[i]); VecDuplicate(solution_array_[0], &F[i]);
		}

		// 1st step
		VecCopy(solution_array_[0], Y[0]);
		VecCopy(zone->calculateInstantSystemRHS(zone->getSolution()), F[0]);
		VecCopy(Y[0], Y[4]);
		VecAXPY(Y[4], 4 * dt, F[0]);
		ConvertPetscToSolution(zone, Y[4], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[4]);
		ConvertPetscToSolution(zone, F[4], num_cells, cell_petsc_index, temp_solution);

		// 2nd step
		VecCopy(Y[0], Y[1]); VecCopy(Y[0], Y[4]);
		VecAXPY(Y[1], 7.0 * dt / 8.0, F[0]); VecAXPY(Y[1], dt / 8.0, F[4]);
		VecAXPY(Y[4], 2.0 * dt, F[0]); VecAXPY(Y[4], 2.0 * dt, F[4]);
		ConvertPetscToSolution(zone, Y[1], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[1]);
		ConvertPetscToSolution(zone, Y[4], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[4]);

		// 3rd step
		VecCopy(Y[0], Y[1]); VecCopy(Y[0], Y[3]); VecCopy(Y[0], Y[4]);
		VecView(F[0], PETSC_VIEWER_STDOUT_WORLD);
		VecView(F[1], PETSC_VIEWER_STDOUT_WORLD);
		VecView(F[4], PETSC_VIEWER_STDOUT_WORLD);
		VecAXPY(Y[1], 11.0 * dt / 24.0, F[0]); VecAXPY(Y[1], 5.0 * dt / 9.0, F[1]); VecAXPY(Y[1], -dt / 72.0, F[4]);
		VecAXPY(Y[3], -3.0 * dt / 8.0, F[0]); VecAXPY(Y[3], 3.0 * dt, F[1]); VecAXPY(Y[3], 3.0*dt / 8.0, F[4]);
		VecAXPY(Y[4], -2.0 * dt / 3.0, F[0]); VecAXPY(Y[4], 32.0 * dt / 9.0, F[1]); VecAXPY(Y[4], 10.0 * dt / 9.0, F[4]);
		ConvertPetscToSolution(zone, Y[1], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[1]);
		ConvertPetscToSolution(zone, Y[3], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[3]);
		ConvertPetscToSolution(zone, Y[4], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[4]);

		// 4th step
		VecCopy(Y[0], Y[1]); VecCopy(Y[0], Y[2]); VecCopy(Y[0], Y[3]); VecCopy(Y[0], Y[4]);
		VecAXPY(Y[1], 59.0 * dt / 144.0, F[0]); VecAXPY(Y[1], 47.0 * dt / 72.0, F[1]); VecAXPY(Y[1], -7.0 * dt / 72.0, F[3]); VecAXPY(Y[1], 5.0*dt / 144.0, F[4]);
		VecAXPY(Y[2], 5.0 * dt / 18.0, F[0]); VecAXPY(Y[2], 14.0 * dt / 9.0, F[1]); VecAXPY(Y[2], 2.0 * dt / 9.0, F[3]); VecAXPY(Y[2], -dt / 18.0, F[4]);
		VecAXPY(Y[3], 3.0 * dt / 16.0, F[0]); VecAXPY(Y[3], 15.0 * dt / 8.0, F[1]); VecAXPY(Y[3], 9.0 * dt / 8.0, F[3]); VecAXPY(Y[3], -3.0*dt / 16.0, F[4]);
		VecAXPY(Y[4], 2.0 * dt / 9.0, F[0]); VecAXPY(Y[4], 16.0 * dt / 9.0, F[1]); VecAXPY(Y[4], 16.0 * dt / 9.0, F[3]); VecAXPY(Y[4], 2.0*dt / 9.0, F[4]);
		ConvertPetscToSolution(zone, Y[1], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[1]);
		ConvertPetscToSolution(zone, Y[2], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[2]);
		ConvertPetscToSolution(zone, Y[3], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[3]);
		ConvertPetscToSolution(zone, Y[4], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[4]);

		// Final
		VecCopy(solution_array_[0], solution_array_[1]);
		VecAXPY(solution_array_[1], 251.0*dt / 720.0, F[0]);
		VecAXPY(solution_array_[1], 323.0*dt / 360.0, F[1]);
		VecAXPY(solution_array_[1], -132.0*dt / 360.0, F[2]);
		VecAXPY(solution_array_[1], 53.0*dt / 360.0, F[3]);
		VecAXPY(solution_array_[1], -19.0*dt / 720.0, F[4]);
		VecCopy(solution_array_[0], solution_array_[2]);
		VecAXPY(solution_array_[2], 29.0*dt / 90.0, F[0]);
		VecAXPY(solution_array_[2], 62.0*dt / 45.0, F[1]);
		VecAXPY(solution_array_[2], 12.0*dt / 45.0, F[2]);
		VecAXPY(solution_array_[2], 2.0*dt / 45.0, F[3]);
		VecAXPY(solution_array_[2], -dt / 90.0, F[4]);
		VecCopy(solution_array_[0], solution_array_[3]);
		VecAXPY(solution_array_[3], 27.0*dt / 80.0, F[0]);
		VecAXPY(solution_array_[3], 51.0*dt / 40.0, F[1]);
		VecAXPY(solution_array_[3], 36.0*dt / 40.0, F[2]);
		VecAXPY(solution_array_[3], 21.0*dt / 40.0, F[3]);
		VecAXPY(solution_array_[3], -3.0*dt / 80.0, F[4]);
		VecCopy(solution_array_[0], solution_array_[4]);
		VecAXPY(solution_array_[4], 14.0*dt / 45.0, F[0]);
		VecAXPY(solution_array_[4], 64.0*dt / 45.0, F[1]);
		VecAXPY(solution_array_[4], 24.0*dt / 45.0, F[2]);
		VecAXPY(solution_array_[4], 64.0*dt / 45.0, F[3]);
		VecAXPY(solution_array_[4], 14.0*dt / 45.0, F[4]);
		current_time += 4.0*dt;
		iteration += 4;
		for (int_t i = 0; i < 5; i++) {
			VecDestroy(&Y[i]); VecDestroy(&F[i]);
		}
		break;
	default:
		MASTER_ERROR("Exceeded order range : order(" + TO_STRING(time_order_) + ")");
	}
#endif
	for (int_t istage = 0; istage < stage_solution_array_.size(); istage++)
		VecCopy(solution_array_[solution_array_.size() - 1], stage_solution_array_[istage]);
}