#include "../INC/TimeUnstImpBDF2.h"

TimeUnstImpBDF2::TimeUnstImpBDF2()
{
	setName("BDF2");
	TimeScheme::getInstance().addRegistry(getName(), this);
	is_implicit_ = true;
}

TimeUnstImpBDF2::~TimeUnstImpBDF2()
{
	ierr_ = VecDestroy(&delta_);
	ierr_ = VecDestroy(&unsteady_source_);
	ierr_ = VecDestroy(&unsteady_residual_);
}

void TimeUnstImpBDF2::initialize()
{
	const Config& config = Config::getInstance();
	is_fixed_dt_ = true;
	is_initialized_ = false;
	pseudo_exit_flag_ = false;
	pseudo_iteration_ = 0;
	pseudo_total_iteration_ = 0;
	pseudo_max_iteration_ = config.getPseudoMaxIteration();
	pseudo_target_residual_ = config.getPseudoTargetResidual();
	pseudo_dt_ = 0.0;
	pseudo_CFL_ = config.getPseudoCFL();
	init_pseudo_CFL_ = pseudo_CFL_;
	physical_dt_ = config.getPhysicalTimeStep();

	linear_solver_iteration_ = 0;
	relative_error_ = 0.0;
	reference_error_ = 0.0;

	for (int_t ires = 0; ires < residual_norms_.size(); ires++)
		residual_norms_[ires] = 100.0;

	// RHS vector
	ierr_ = VecCreate(MPI_COMM_WORLD, &delta_);
	ierr_ = VecCreate(MPI_COMM_WORLD, &unsteady_source_);
	ierr_ = VecCreate(MPI_COMM_WORLD, &unsteady_residual_);

	// Linear system solver initialize
	linear_system_solver_ = LinearSystemSolver::getInstance().getType(config.getLinearSystemSolver());
	linear_system_solver_->Initialize();
}

void TimeUnstImpBDF2::integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
	const int_t num_cell = zone->getNumCells();

	real_t coeff1 = 0, coeff2 = 0, coeff3 = 0;
	if (iteration > 1)
	{
		coeff1 = 2.0 / dt;
		coeff2 = -0.5 / dt;
		coeff3 = 1.5 / dt;
	}
	else // starting step
	{
		old_solution_ = zone->getSolution();
		coeff1 = 0.0;
		coeff2 = 1.0 / dt;
		coeff3 = 1.0 / dt;
	}

	// Initializing vectors
	if (is_initialized_ == false)
	{
		const Vec& vec_initializer = zone->getSystemRHS();
		ierr_ = VecDuplicate(vec_initializer, &delta_);
		ierr_ = VecDuplicate(vec_initializer, &unsteady_source_);
		ierr_ = VecDuplicate(vec_initializer, &unsteady_residual_);
		is_initialized_ = true;
	}

	// Calculate source term
	computeUnsteadySource(zone, coeff1, zone->getSolution(), coeff2, old_solution_);
	old_solution_ = zone->getSolution();
	
	pseudo_exit_flag_ = false;
	pseudo_iteration_ = 0;
	while (!pseudo_exit_flag_)
	{
		zone->communication();

		const Vec& rhs = zone->calculateSystemRHS();
		computeUnsteadyResidual(zone, coeff3, zone->getSolution(), rhs);

		// Update pseudo CFL (constant cfl)
		UpdatePseudoCFL();

		// Update pseudo time step
		pseudo_dt_ = zone->getTimeStepFromCFL(pseudo_CFL_);

		// Calculate system matrix
		const real_t diagonal_factor = 1.5 / physical_dt_ + 1.0 / pseudo_dt_;
		const Mat& system_matrix = zone->calculateSystemMatrix(diagonal_factor, 1.0);

		// Krylov solve
		ierr_ = linear_system_solver_->Solve(system_matrix, unsteady_residual_, delta_);
		linear_solver_iteration_ = linear_system_solver_->getIterationNumber();
		
		// Solution update
		zone->UpdateSolutionFromDelta(delta_);
		
		computeError();
		zone->communication();
		zone->limiting();
		zone->pressurefix(current_time, iteration);

		UpdatePseudoStatus();
		//if (linear_solver_iteration_ == 0)
		{
			ierr_ = PetscPrintf(MPI_COMM_WORLD, "Pseudo Iterations = %D : Krylov Iterations = %D : L2 norm = %g\n", pseudo_iteration_, linear_solver_iteration_, (double)relative_error_);
		}		
	}
	status_string_ = "Pseudo/Krylov=" + TO_STRING(pseudo_iteration_) + "/" + TO_STRING(linear_solver_iteration_) + " L2=" + TO_STRING((double)relative_error_);
	//ierr_ = PetscPrintf(MPI_COMM_WORLD, "Pseudo Iterations = %D : Krylov Iterations = %D : L2 norm = %g\n", pseudo_iteration_, iteration_, (double)relative_error_);
}

void TimeUnstImpBDF2::computeUnsteadySource(std::shared_ptr<const Zone> zone, const real_t coeff1, const std::vector<std::vector<real_t> >& solution, const real_t coeff2, const std::vector<std::vector<real_t> >& old_solution)
{
	const int_t num_cells = zone->getNumCells();
	const std::vector<int_t>& cell_petsc_index = zone->getCellPetscIndex();

	PetscScalar *unsteady_source_array;
	const PetscInt num_var = solution[0].size();
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	VecGetArray(unsteady_source_, &unsteady_source_array);
	for (int_t icell = 0; icell < num_cells; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			unsteady_source_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar]
			= coeff1 * solution[icell][ivar] + coeff2 * old_solution[icell][ivar];
	VecRestoreArray(unsteady_source_, &unsteady_source_array);
}

void TimeUnstImpBDF2::computeUnsteadyResidual(std::shared_ptr<const Zone> zone, const real_t coeff, const std::vector<std::vector<real_t> >& solution, const Vec& rhs)
{
	VecWAXPY(unsteady_residual_, 1.0, rhs, unsteady_source_);
	const int_t num_cells = zone->getNumCells();
	const std::vector<int_t> cell_petsc_index = zone->getCellPetscIndex();

	PetscScalar *unsteady_residual_array;
	const PetscInt num_var = solution[0].size();
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	VecGetArray(unsteady_residual_, &unsteady_residual_array);
	for (int_t icell = 0; icell < num_cells; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			unsteady_residual_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar] -= coeff * solution[icell][ivar];
	VecRestoreArray(unsteady_residual_, &unsteady_residual_array);
}

void TimeUnstImpBDF2::computeError()
{
	PetscReal error_sum = 0.0;
	//VecNorm(delta_, NORM_2, &error_sum);
	VecNorm(unsteady_residual_, NORM_2, &error_sum);
	if (pseudo_iteration_ == 0)
	{
		reference_error_ = error_sum;
	}		
	else if (pseudo_iteration_ < 5)
	{
		if (error_sum > reference_error_)
		{
			reference_error_ = error_sum;
		}
	}
	relative_error_ = error_sum / reference_error_;
}

void TimeUnstImpBDF2::UpdatePseudoStatus()
{
	pseudo_iteration_++;
	pseudo_total_iteration_++;

	// Check relative error and set exit flag.
	if (relative_error_ <= pseudo_target_residual_) pseudo_exit_flag_ = true;

	// Check maximum pseudo iteration and set exit flag.
	if (pseudo_iteration_ == pseudo_max_iteration_) pseudo_exit_flag_ = true;
}

void TimeUnstImpBDF2::UpdatePseudoCFL()
{
	pseudo_CFL_ = init_pseudo_CFL_;
}

std::vector<std::vector<real_t> > TimeUnstImpBDF2::PetscToVector(const Vec& petscVec, const int_t num_cells, const int_t num_vars, const std::vector<int_t>& cell_petsc_index)
{
	const PetscScalar *petsc_array;
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	std::vector<std::vector<real_t> > solution(num_cells);
	VecGetArrayRead(petscVec, &petsc_array);
	for (int_t icell = 0; icell < num_cells; icell++)
	{
		solution[icell].resize(num_vars);
		for (int_t ivar = 0; ivar < num_vars; ivar++)
			solution[icell][ivar] = petsc_array[(cell_petsc_index[icell] - petsc_index_start) * num_vars + ivar];
	}
	VecRestoreArrayRead(petscVec, &petsc_array);
	return solution;
}