
#include "../INC/Config.h"

void Config::readConfigFile(const std::string& config_file_name)
{
	config_file_name_ = config_file_name;

	std::string file_name = INPUT() + config_file_name_;
	std::ifstream infile(file_name.c_str());

	MASTER_MESSAGE("-----------------Read Input Data------------------");
	if (infile.is_open()) MASTER_MESSAGE("Open configuration file : " + config_file_name_);
	else MASTER_ERROR("Fail to open configuration file : " + config_file_name_);

	std::string text;
	while (getline(infile, text)){
		ReadDataFromFile(infile, text, "~>dimension_", &dimension_);
		ReadDataFromFile(infile, text, "~>grid_format_", grid_format_);
		ReadDataFromFile(infile, text, "~>grid_file_name_", grid_file_name_);

		ReadDataFromFile(infile, text, "~>CFL_", &CFL_);
		ReadDataFromFile(infile, text, "~>min_pseudo_CFL", &min_pseudo_CFL_); // for implciit
		ReadDataFromFile(infile, text, "~>max_pseudo_CFL", &max_pseudo_CFL_); // for implicit
		ReadDataFromFile(infile, text, "~>SER_up_factor_", &SER_up_factor_); // for implicit
		ReadDataFromFile(infile, text, "~>SER_start_error_", &SER_start_error_); // for implicit
		ReadDataFromFile(infile, text, "~>itermax_", &itermax_);
		ReadDataFromFile(infile, text, "~>start_time_", &start_time_);
		ReadDataFromFile(infile, text, "~>end_time_", &end_time_);

		ReadDataFromFile(infile, text, "~>order_", &order_);
		ReadDataFromFile(infile, text, "~>HOM_name_", HOM_name_);
		ReadDataFromFile(infile, text, "~>equation_name_", equation_name_);
		ReadDataFromFile(infile, text, "~>problem_name_", problem_name_);
		ReadDataFromFile(infile, text, "~>convflux_name_", convflux_name_);
		ReadDataFromFile(infile, text, "~>time_scheme_name_", time_scheme_name_);
		ReadDataFromFile(infile, text, "~>limiter_name_", limiter_name_);
		ReadDataFromFile(infile, text, "~>pressure_fix_", pressure_fix_);

		// for implicit
		ReadDataFromFile(infile, text, "~>pseudo_max_iteration_", &pseudo_max_iteration_); // for implicit
		ReadDataFromFile(infile, text, "~>pseudo_target_residual_", &pseudo_target_residual_); // for implicit
		ReadDataFromFile(infile, text, "~>physical_time_step_", &physical_time_step_); // for implicit
		ReadDataFromFile(infile, text, "~>pseudo_CFL_", &pseudo_CFL_); // for implicit
		ReadDataFromFile(infile, text, "~>nonzero_guess_", nonzero_guess_); // for implicit
		ReadDataFromFile(infile, text, "~>linear_system_solver_", linear_system_solver_); // for implicit
		ReadDataFromFile(infile, text, "~>fixed_time_step_", fixed_time_step_); // for implicit
		ReadDataFromFile(infile, text, "~>time_order_", &time_order_); // for implicit
		// for implicit

		ReadDataFromFile(infile, text, "~>return_address_", return_address_);
		ReadDataFromFile(infile, text, "~>subcell_resolution_", &subcell_resolution_);
		ReadDataFromFile(infile, text, "~>print_option_", &print_option_);
		ReadDataFromFile(infile, text, "~>save_option_", &save_option_);
		ReadDataFromFile(infile, text, "~>history_", history_);
		ReadDataFromFile(infile, text, "~>restart_", restart_);
	}
	infile.close();

	MakeDirectories();
	WriteConfigFile();
}

void Config::ReadDataFromFile(std::ifstream& infile, const std::string& text, const std::string& dataname, real_t* data)
{
	if (text.find(dataname, 0) != std::string::npos){
		infile >> *data;
		MASTER_MESSAGE("   " + dataname + " : " + TO_STRING(*data));
	}
	return;
}

void Config::ReadDataFromFile(std::ifstream& infile, const std::string& text, const std::string& dataname, int_t* data)
{
	if (text.find(dataname, 0) != std::string::npos){
		infile >> *data;
		MASTER_MESSAGE("   " + dataname + " : " + TO_STRING(*data));
	}
	return;
}

void Config::ReadDataFromFile(std::ifstream& infile, const std::string& text, const std::string& dataname, std::array<int_t, 3> *data)
{
	if (text.find(dataname, 0) != std::string::npos){
		infile >> (*data)[0] >> (*data)[1] >> (*data)[2];
		MASTER_MESSAGE("   " + dataname + " : " + TO_STRING((*data)[0]) + ", " + TO_STRING((*data)[1]) + ", " + TO_STRING((*data)[2]));
	}
	return;
}

void Config::ReadDataFromFile(std::ifstream& infile, const std::string& text, const std::string& dataname, std::string& data)
{
	if (text.find(dataname, 0) != std::string::npos){
		getline(infile, data);
		MASTER_MESSAGE("   " + dataname + " : " + data);
	}
	return;
}

void Config::MakeDirectories()
{
	int_t myrank = MYRANK();
	int_t ndomain; MPI_Comm_size(MPI_COMM_WORLD, &ndomain);
	std::string foldername = return_address_;
#ifdef _WINDOW_
	if (myrank == MASTER_NODE){
		_mkdir(foldername.c_str());
		foldername = return_address_ + "/save";
		_mkdir(foldername.c_str());
		foldername = return_address_ + "/during";
		_mkdir(foldername.c_str());
		foldername = return_address_ + "/grids";
		_mkdir(foldername.c_str());
	}
#else
	if (myrank == MASTER_NODE){
		mkdir(foldername.c_str(), 0755);
		foldername = return_address_ + "/save";
		mkdir(foldername.c_str(), 0755);
		foldername = return_address_ + "/during";
		mkdir(foldername.c_str(), 0755);
		foldername = return_address_ + "/grids";
		mkdir(foldername.c_str(), 0755);
	}
#endif
	return;
}

void Config::WriteConfigFile()
{
	int_t myrank = MYRANK();
	if (myrank == MASTER_NODE){
		std::ofstream outfile(return_address_ + "/save/" + config_file_name_.c_str());

		if (outfile.is_open()) MASTER_MESSAGE("Write configuration file : " + config_file_name_);
		else MASTER_ERROR("Fail to write configuration file : " + config_file_name_);

		std::string text;

		outfile << "@Grid Parameter" << std::endl;
		WriteDataToFile(outfile, "~>dimension_", dimension_);
		WriteDataToFile(outfile, "~>grid_format_", grid_format_);
		WriteDataToFile(outfile, "~>grid_file_name_", grid_file_name_);

		outfile << std::endl;
		outfile << "@Time Parameter" << std::endl;
		WriteDataToFile(outfile, "~>CFL_", CFL_);
		WriteDataToFile(outfile, "~>min_pseudo_CFL_", min_pseudo_CFL_); // for implicit
		WriteDataToFile(outfile, "~>max_pseudo_CFL_", max_pseudo_CFL_); // for implicit
		WriteDataToFile(outfile, "~>SER_up_factor_", SER_up_factor_); // for implicit
		WriteDataToFile(outfile, "~>SER_start_error_", SER_start_error_); // for implicit
		WriteDataToFile(outfile, "~>itermax_", itermax_);
		WriteDataToFile(outfile, "~>start_time_", start_time_);
		WriteDataToFile(outfile, "~>end_time_", end_time_);

		outfile << std::endl;
		outfile << "@Control Parameter" << std::endl;
		WriteDataToFile(outfile, "~>order_", order_);
		WriteDataToFile(outfile, "~>HOM_name_", HOM_name_);
		WriteDataToFile(outfile, "~>equation_name_", equation_name_);
		WriteDataToFile(outfile, "~>problem_name_", problem_name_);
		WriteDataToFile(outfile, "~>convflux_name_", convflux_name_);
		WriteDataToFile(outfile, "~>time_scheme_name_", time_scheme_name_);
		WriteDataToFile(outfile, "~>limiter_name_", limiter_name_);
		WriteDataToFile(outfile, "~>pressure_fix_", pressure_fix_);

		// for implicit ~
		outfile << std::endl;
		outfile << "@Implicit Solver Parameter" << std::endl;
		WriteDataToFile(outfile, "~>pseudo_max_iteration_", pseudo_max_iteration_);
		WriteDataToFile(outfile, "~>pseudo_target_residual_", pseudo_target_residual_);
		WriteDataToFile(outfile, "~>physical_time_step_", physical_time_step_);
		WriteDataToFile(outfile, "~>pseudo_CFL_", pseudo_CFL_);
		WriteDataToFile(outfile, "~>nonzero_guess_", nonzero_guess_);
		WriteDataToFile(outfile, "~>linear_system_solver_", linear_system_solver_);
		WriteDataToFile(outfile, "~>fixed_time_step_", fixed_time_step_);
		WriteDataToFile(outfile, "~>time_order_", time_order_);
		// ~ for implicit

		outfile << std::endl;
		outfile << "@Post Option" << std::endl;
		WriteDataToFile(outfile, "~>return_address_", return_address_);
		WriteDataToFile(outfile, "~>subcell_resolution_", subcell_resolution_);
		WriteDataToFile(outfile, "~>print_option_", print_option_);
		WriteDataToFile(outfile, "~>save_option_", save_option_);
		WriteDataToFile(outfile, "~>history_", history_);
		WriteDataToFile(outfile, "~>restart_", restart_);

		outfile.close();
	}
}

void Config::WriteDataToFile(std::ofstream& outfile, const std::string& dataname, const real_t& data)
{
	outfile << dataname << std::endl;
	outfile << data << std::endl;
	return;
}
void Config::WriteDataToFile(std::ofstream& outfile, const std::string& dataname, const int_t& data)
{
	outfile << dataname << std::endl;
	outfile << data << std::endl;
	return;
}
void Config::WriteDataToFile(std::ofstream& outfile, const std::string& dataname, const std::array<int_t, 3>& data)
{
	outfile << dataname << std::endl;
	outfile << data[0] << " " << data[1] << " " << data[2] << std::endl;
	return;
}
void Config::WriteDataToFile(std::ofstream& outfile, const std::string& dataname, const std::string& data)
{
	outfile << dataname << std::endl;
	outfile << data << std::endl;
	return;
}