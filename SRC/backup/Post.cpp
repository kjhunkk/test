
#include "../INC/Post.h"

void Post::initialize(std::shared_ptr<Grid> grid, std::shared_ptr<Zone> zone)
{
	Config& config = Config::getInstance();
	post_order_ = config.getSubcellResolution();
	return_address_ = config.getReturnAddress();
	dimension_ = config.getDimension();
	
	// setting grid
	grid_ = grid;
	num_cells_ = grid->getNumCells();
	num_basis_ = grid->getNumBasis();

	const std::vector<int_t>& element_type = grid->getCellElemType();
	post_elements_.resize(num_cells_);
	for (int_t icell = 0; icell < num_cells_; icell++)
		post_elements_[icell] = PostElementFactory::getInstance().getPostElement(static_cast<ElemType>(element_type[icell]));

	coords_.resize(num_cells_);
	basis_.resize(num_cells_);
	num_subcell_nodes_ = 0;
	num_subcell_cells_ = 0;
	
	const std::vector<int_t>& cell_to_subnode_ptr = grid->getCellToSubnodePtr();
	const std::vector<int_t>& cell_to_subnode_ind = grid->getCellToSubnodeInd();
	const std::vector<int_t>& cell_order = grid->getCellElemOrder();
	const std::vector<real_t>& node_coords = grid->getNodeCoords();
	for (int_t icell = 0; icell < num_cells_; icell++){
	
		std::vector<real_t> cell_coords;
		const int_t& start_index = cell_to_subnode_ptr[icell];
		const int_t& end_index = cell_to_subnode_ptr[icell + 1];
		cell_coords.reserve(dimension_*(end_index - start_index));
		for (int_t inode = start_index; inode < end_index; inode++){
			for (int_t idim = 0; idim < dimension_; idim++)
				cell_coords.push_back(node_coords[cell_to_subnode_ind[inode] * dimension_ + idim]);
		}

		coords_[icell] = post_elements_[icell]->getPostCoords(element_type[icell], cell_order[icell], post_order_, cell_coords);
		basis_[icell] = grid->calBasisValue(icell, coords_[icell]);
		const std::vector<int_t> connects = post_elements_[icell]->getPostConnects(post_order_);

		num_subcell_nodes_ += coords_[icell].size() / dimension_;
		num_subcell_cells_ += connects.size() / (dimension_ + 1);
	}
	
	// setting zone
	solution_ = zone->getSolutionPtr();
	equation_ = zone->getEquation();
	variable_names_ = equation_->getVariableNames();
	num_states_ = equation_->getNumStates();
	
	// initialize aerodynamics
	aerodynamics_.initialize(grid_, solution_, equation_, post_order_);
}

void Post::postSolution(const std::string& title, const real_t time, const bool average)
{
	char tag_name[10]; sprintf(tag_name, "%d", MYRANK());
	std::string filename = return_address_ + "/" + title + tag_name + ".plt";
	std::ofstream outfile(filename);
	//outfile << std::scientific;
	//outfile.precision(16);

	MASTER_MESSAGE("Post: File output(" + filename + ")");

	if (dimension_ == 2){
		outfile << "TITLE = \"field \"" << std::endl;
		outfile << "VARIABLES = \"x\",\"y\"";
		for (int_t ivar = 0; ivar < variable_names_.size(); ivar++)
			outfile << ",\"" << variable_names_[ivar] << "\"";
		outfile << std::endl;
		outfile << "ZONE T=\"ZONE " << MYRANK() << "\" SOLUTIONTIME=" << time << std::endl;
		outfile << "n=" << num_subcell_nodes_ << " ,e=" << num_subcell_cells_ << " ,f=fepoint, et=triangle" << std::endl;
	}
	else if (dimension_ == 3){
		outfile << "TITLE = \"field \"" << std::endl;
		outfile << "VARIABLES = \"x\",\"y\",\"z\"";
		for (int_t ivar = 0; ivar < variable_names_.size(); ivar++)
			outfile << ",\"" << variable_names_[ivar] << "\"";
		outfile << std::endl;
		outfile << "ZONE T=\"ZONE " << MYRANK() << "\" SOLUTIONTIME=" << time << std::endl;
		outfile << "n=" << num_subcell_nodes_ << " ,e=" << num_subcell_cells_ << " ,f=fepoint, et=tetrahedron" << std::endl;
	}

	int_t num_basis = num_basis_;
	if (average == true)
		num_basis = 1;

	for (int_t icell = 0; icell < num_cells_; icell++){
		const int_t num_points = coords_[icell].size() / dimension_;
		std::vector<real_t> solution(num_points*num_states_, 0.0);

		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis; ibasis++)
			solution[ipoint*num_states_ + istate] += (*solution_)[icell][istate*num_basis_ + ibasis] * basis_[icell][ipoint*num_basis_ + ibasis];
		
		std::vector<real_t> post_solution = equation_->calPostVariables(solution);
		const int_t num_states = post_solution.size() / num_points;

		for (int_t ipoint = 0; ipoint < num_points; ipoint++){
			for (int_t idim = 0; idim < dimension_;idim++)
				outfile << coords_[icell][ipoint*dimension_+idim] << "\t";
			for (int_t istate = 0; istate < num_states; istate++)
				outfile << post_solution[ipoint*num_states + istate] << "\t";
			outfile << std::endl;
		}
	}

	int_t inode = 1;
	for (int_t icell = 0; icell < num_cells_; icell++){
		std::vector<int_t> connects = post_elements_[icell]->getPostConnects(post_order_);
		for (int_t isubcell = 0; isubcell < connects.size() / (dimension_ + 1); isubcell++){
			for (int_t idim = 0; idim < dimension_ + 1; idim++)
				outfile << inode + connects[isubcell*(dimension_ + 1) + idim] << "\t";
			outfile << std::endl;
		}
		inode += coords_[icell].size() / dimension_;
	}
	outfile.close();

	aerodynamics_.postPressureCoefficients(title + "_Cp", time);
}

void Post::postDuringSolution(int_t& strandid, const real_t time)
{
	char tag_name[10]; sprintf(tag_name, "%d", MYRANK());
	std::string filename = return_address_ + "/during/RANK" + tag_name + ".plt";
	std::ofstream outfile;
	if (strandid == 1) outfile.open(filename, std::ios::trunc);
	else outfile.open(filename, std::ios::app);
	//outfile << std::scientific;
	//outfile.precision(16);

	MASTER_MESSAGE("Post: File output(" + filename + ")");

	if (dimension_ == 2){
		outfile << "TITLE = \"field \"" << std::endl;
		outfile << "VARIABLES = \"x\",\"y\"";
		for (int_t ivar = 0; ivar < variable_names_.size(); ivar++)
			outfile << ",\"" << variable_names_[ivar] << "\"";
		outfile << std::endl;
		outfile << "ZONE T=\"ZONE " << MYRANK() << "\" SOLUTIONTIME=" << time << std::endl;
		outfile << "n=" << num_subcell_nodes_ << " ,e=" << num_subcell_cells_ << " ,f=fepoint, et=triangle" << std::endl;
	}
	else if (dimension_ == 3){
		outfile << "TITLE = \"field \"" << std::endl;
		outfile << "VARIABLES = \"x\",\"y\",\"z\"";
		for (int_t ivar = 0; ivar < variable_names_.size(); ivar++)
			outfile << ",\"" << variable_names_[ivar] << "\"";
		outfile << std::endl;
		outfile << "ZONE T=\"ZONE " << MYRANK() << "\" SOLUTIONTIME=" << time << std::endl;
		outfile << "n=" << num_subcell_nodes_ << " ,e=" << num_subcell_cells_ << " ,f=fepoint, et=tetrahedron" << std::endl;
	}

	for (int_t icell = 0; icell < num_cells_; icell++){
		const int_t num_points = coords_[icell].size() / dimension_;
		std::vector<real_t> solution(num_points*num_states_, 0.0);

		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			solution[ipoint*num_states_ + istate] += (*solution_)[icell][istate*num_basis_ + ibasis] * basis_[icell][ipoint*num_basis_ + ibasis];

		std::vector<real_t> post_solution = equation_->calPostVariables(solution);
		const int_t num_states = post_solution.size() / num_points;

		for (int_t ipoint = 0; ipoint < num_points; ipoint++){
			for (int_t idim = 0; idim < dimension_; idim++)
				outfile << coords_[icell][ipoint*dimension_ + idim] << "\t";
			for (int_t istate = 0; istate < num_states; istate++)
				outfile << post_solution[ipoint*num_states + istate] << "\t";
			outfile << std::endl;
		}
	}

	int_t inode = 1;
	for (int_t icell = 0; icell < num_cells_; icell++){
		std::vector<int_t> connects = post_elements_[icell]->getPostConnects(post_order_);
		for (int_t isubcell = 0; isubcell < connects.size() / (dimension_ + 1); isubcell++){
			for (int_t idim = 0; idim < dimension_ + 1; idim++)
				outfile << inode + connects[isubcell*(dimension_ + 1) + idim] << "\t";
			outfile << std::endl;
		}
		inode += coords_[icell].size() / dimension_;
	}
	outfile.close();

	aerodynamics_.postDuringPressureCoefficients(strandid, time);
	strandid++;
}

void Post::postHistory(const int_t iteration)
{
	const int_t myrank = MYRANK();
	const int_t target_state = 0;
	std::string filename = return_address_ + "/during/HISTORY.plt";
	std::ofstream outfile;

	if (iteration == 0) {
		pre_solution_ = *solution_;
		if (MYRANK() == MASTER_NODE) {
			outfile.open(filename, std::ios::trunc);
			outfile << "TITLE = \"field \"" << std::endl;
			outfile << "VARIABLES = \"Iteration\",\"Residual\"" << std::endl;
			for (int_t idim = 0; idim < dimension_; idim++)
				outfile << ",\"Cf_" << idim << "\"";
			outfile.close();
		}
		return;
	}

	real_t residual = 0.0;
	real_t total_area = 0.0;

	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_quad_points = grid_->getCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_quad_weights = grid_->getCellQuadWeights();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();
	const std::vector<real_t>& cell_volumes = grid_->getCellVolumes();

	for (int_t icell = 0; icell < num_cells_; icell++) {
		const int_t& num_quad_points = num_cell_quad_points[icell];

		std::vector<real_t> solution(num_quad_points, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				solution[ipoint] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * (*solution_)[icell][target_state*num_basis_ + ibasis];

		std::vector<real_t> pre_solution(num_quad_points, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				pre_solution[ipoint] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * pre_solution_[icell][target_state*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			residual += (solution[ipoint] - pre_solution[ipoint])*(solution[ipoint] - pre_solution[ipoint])*cell_quad_weights[icell][ipoint];
		total_area += cell_volumes[icell];
	}

	real_t sum = 0.0;
	MPI_Allreduce(&residual, &sum, 1, MPI_REAL_T, MPI_SUM, MPI_COMM_WORLD);
	residual = sum;
	MPI_Allreduce(&total_area, &sum, 1, MPI_REAL_T, MPI_SUM, MPI_COMM_WORLD);
	total_area = sum;
	residual = std::sqrt(residual / total_area);

	const std::vector<real_t> Cf = aerodynamics_.calForceCoefficients();

	if (MYRANK() == MASTER_NODE) {
		outfile.open(filename, std::ios::app);
		//outfile << std::scientific;
		//outfile.precision(16);

		outfile << iteration << "\t" << residual;
		for (int_t idim = 0; idim < dimension_; idim++)
			outfile << "\t" << Cf[idim];
		outfile << std::endl;
		outfile.close();
	}
}