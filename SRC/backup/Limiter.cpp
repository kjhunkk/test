
#include "../INC/Limiter.h"

NoneLimiter::NoneLimiter()
{
	setName("None");
	Limiter::getInstance().addRegistry(getName(), this);
}
void NoneLimiter::initialize(std::shared_ptr<Grid> grid, Equation* equation, std::vector<std::vector<real_t>>* solution)
{
	return;
}
void NoneLimiter::limiting()
{
	return;
}

// -------------------------------------- hMLP -------------------------------------

hMLP::hMLP()
{
	setName("hMLP");
	Limiter::getInstance().addRegistry(getName(), this);
}
void hMLP::initialize(std::shared_ptr<Grid> grid, Equation* equation, std::vector<std::vector<real_t>>* solution)
{
	Config& config = Config::getInstance();
	order_ = config.getOrder();
	int_t dimension = config.getDimension();
	num_basis_list_.resize(order_ + 1);
	for (int_t iorder = 0; iorder < order_ + 1; iorder++){
		if (dimension == 2)
			num_basis_list_[iorder] = (iorder + 1)*(iorder + 2) / 2;
		else if(dimension == 3)
			num_basis_list_[iorder] = (iorder + 1)*(iorder + 2)*(iorder + 3) / 6;
	}

	grid_ = grid;
	num_total_nodes_ = grid_->getNumTotalNodes();
	num_total_cells_ = grid_->getNumTotalCells();
	num_cells_ = grid_->getNumCells();
	num_basis_ = grid_->getNumBasis();
	vertex_basis_ = grid_->getCellVertexBasisValue();
	cell_volumes_ = &(grid_->getCellVolumes());

	equation_ = equation;
	num_states_ = equation_->getNumStates();

	solution_ = solution;

	target_state_ = 0;

	return;
}
void hMLP::limiting()
{
	construct();

	for (int_t icell = 0; icell < num_cells_; icell++){
		bool is_trouble = true;
		int_t order = order_; 

		for (;;){
			if (order == 0) {
				is_trouble = false;
				break;
			}

			is_trouble = indicator(icell, order);

			if (is_trouble == true){
				if (order == 1){
					break;
				}
				else if (order == 2){
					order = 1;
					//MEMBER_ERROR("ERROR!");
					//MEMBER_MESSAGE("ERROR!");
					projection(icell, 1);
					break;
				}
				else{
					is_trouble = false;
					order = order - 1;
					//MEMBER_ERROR("ERROR!");
					//MEMBER_MESSAGE("ERROR!");
					projection(icell, order);
				}
			}
			else{
				break;
			}
		}
		if (is_trouble == true){
			//MEMBER_ERROR("ERROR!");
			//MEMBER_MESSAGE("ERROR!");
			MLPu1(icell);
		}
	}
	return;
}

void hMLP::projection(const int_t icell, const int_t iorder)
{
	for (int_t istate = 0; istate < num_states_;istate++)
	for (int_t ibasis = num_basis_list_[iorder]; ibasis < num_basis_; ibasis++)
		(*solution_)[icell][istate*num_basis_ + ibasis] = 0.0;
}

bool hMLP::indicator(const int_t icell, const int_t order)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const int_t num_vertex = cell_to_node_ptr[icell + 1] - cell_to_node_ptr[icell];

	for (int_t ivertex = 0; ivertex < num_vertex; ivertex++){
		bool flag = MLPCondition(icell, ivertex);
		if (flag == true){
			if (order>1){
				flag = smoothDetect(icell, ivertex);
			}
		}
		if (flag == true){
			return true;
		}
	}
	return false;
}
bool hMLP::Deactivation(const int_t icell, const int_t ivertex)
{
	const real_t eps = 1.0e-3;
	real_t vertex_value = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		vertex_value += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];
	real_t average_value = (*solution_)[icell][target_state_*num_basis_] * (*vertex_basis_)[icell][ivertex*num_basis_];
	if (std::abs(vertex_value - average_value) <= std::max(eps*std::abs(average_value), (*cell_volumes_)[icell]))
		return true;
	return false;
}
bool hMLP::MLPCondition(const int_t icell, const int_t ivertex)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();
	
	real_t P1var = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_list_[1]; ibasis++)
		P1var += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];

	if (Deactivation(icell, ivertex) == true)
		return false;

	if (P1var > vertex_max_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) return true;
	else if (P1var < vertex_min_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) return true;
	else return false;
}

bool hMLP::smoothDetect(const int_t icell, const int_t ivertex)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();

	real_t vertex_value = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		vertex_value += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];

	real_t P1term = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_list_[1]; ibasis++)
		P1term += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];

	const real_t high_term = vertex_value - P1term;
	P1term -= (*solution_)[icell][target_state_*num_basis_] * (*vertex_basis_)[icell][0];

	const real_t eps = 1.0E-14;
	if ((P1term>0.0) && (high_term<eps) && (vertex_value>vertex_min_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]])) // c1 condition
		return false;
	else if ((P1term<0.0) && (high_term>-eps) && (vertex_value < vertex_max_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]])) // c2 condition
		return false;

	return true;
}

void hMLP::MLPu1(const int_t icell)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();
	const int_t num_vertex = cell_to_node_ptr[icell + 1] - cell_to_node_ptr[icell];

	std::vector<real_t> p1_value(num_states_*num_vertex, 0.0);
	for (int_t istate = 0; istate < num_states_; istate++)
	for (int_t ivertex = 0; ivertex < num_vertex; ivertex++)
	for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		p1_value[istate*num_vertex + ivertex] += (*solution_)[icell][istate*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];

	for (int_t istate = 0; istate < num_states_; istate++){
		real_t temp;
		real_t phi = 1.0;
		for (int_t ivertex = 0; ivertex < num_vertex; ivertex++){
			temp = (*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - p1_value[istate*num_vertex + ivertex];
			if (p1_value[istate*num_vertex + ivertex] < vertex_min_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]){
				if (std::abs(temp) < 1.0E-14){
					temp = 0.0;
				}
				else{
					temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_min_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
				}
				if (temp < phi){
					phi = temp;
				}
			}
			else if (vertex_max_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]] < p1_value[istate*num_vertex + ivertex]){
				if (std::abs(temp) < 1.0E-14){
					temp = 0.0;
				}
				else{
					temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_max_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
				}
				if (temp < phi){
					phi = temp;
				}
			}
		}
		for (int_t ibasis = 1; ibasis < num_basis_list_[1]; ibasis++)
			(*solution_)[icell][istate*num_basis_ + ibasis] *= phi;
	}
}

void hMLP::construct()
{
	const std::vector<std::vector<real_t>>& basis = grid_->getCellBasisValue();
	const std::vector<std::vector<int_t>>& node_to_cells = grid_->getNodeToCells();

	std::vector<std::vector<real_t>> cell_averages(num_states_);
	for (int_t icell = 0; icell < num_total_cells_; icell++)
	for (int_t istate = 0; istate < num_states_; istate++)
		cell_averages[istate].push_back((*solution_)[icell][istate*num_basis_]*basis[icell][0]);
	cell_averages_ = move(cell_averages);

	std::vector<std::vector<real_t>> vertex_max(num_states_);
	std::vector<std::vector<real_t>> vertex_min(num_states_);
	for (int_t inode = 0; inode < num_total_nodes_; inode++){

		std::vector<std::vector<real_t>> local_cell_averages(num_states_);

		for (auto&& icell : node_to_cells[inode])
		for (int_t istate = 0; istate < num_states_; istate++)
			local_cell_averages[istate].push_back(cell_averages_[istate][icell]);
			
		for (int_t istate = 0; istate < num_states_; istate++){
			vertex_max[istate].push_back(*max_element(local_cell_averages[istate].begin(), local_cell_averages[istate].end()));
			vertex_min[istate].push_back(*min_element(local_cell_averages[istate].begin(), local_cell_averages[istate].end()));
		}
	}
	vertex_max_ = move(vertex_max);
	vertex_min_ = move(vertex_min);
}

// -------------------------------------- hMLP_BD ----------------------------------

hMLPBD::hMLPBD()
{
	setName("hMLPBD");
	Limiter::getInstance().addRegistry(getName(), this);
}
void hMLPBD::initialize(std::shared_ptr<Grid> grid, Equation* equation, std::vector<std::vector<real_t>>* solution)
{
	Config& config = Config::getInstance();
	order_ = config.getOrder();
	int_t dimension = config.getDimension();
	num_basis_list_.resize(order_ + 1);
	for (int_t iorder = 0; iorder < order_ + 1; iorder++){
		if (dimension == 2)
			num_basis_list_[iorder] = (iorder + 1)*(iorder + 2) / 2;
		else if (dimension == 3)
			num_basis_list_[iorder] = (iorder + 1)*(iorder + 2)*(iorder + 3) / 6;
	}

	grid_ = grid;
	num_total_nodes_ = grid_->getNumTotalNodes();
	num_total_cells_ = grid_->getNumTotalCells();
	num_cells_ = grid_->getNumCells();
	num_faces_ = grid_->getNumFaces();
	num_peribdries_ = grid_->getNumPeriBdries();
	num_basis_ = grid_->getNumBasis();
	vertex_basis_ = grid_->getCellVertexBasisValue();
	cell_element_ = &grid_->getCellElement();
	cell_volumes_ = &(grid_->getCellVolumes());

	equation_ = equation;
	num_states_ = equation_->getNumStates();

	solution_ = solution;

	target_state_ = 0;

	return;
}
void hMLPBD::limiting()
{
	construct();

	for (int_t icell = 0; icell < num_cells_; icell++){
		bool is_trouble = true;
		int_t order = order_;

		for (;;){
			if (order == 0) {
				is_trouble = false;
				break;
			}

			is_trouble = indicator(icell, order);

			if (is_trouble == true){
				if (order == 1){
					break;
				}
				else if (order == 2){
					order = 1;
					projection(icell, 1);
					break;
				}
				else{
					is_trouble = false;
					order = order - 1;
					projection(icell, order);
				}
			}
			else{
				if (boundaryDetect(icell, (*cell_element_)[icell]->getNumForTypeOne())){
					if (!isContact(icell))
						projection(icell, 1); // type one boundary detector
				}
				break;
			}
		}
		if (is_trouble == true){
			MLPu1(icell);
		}
	}
	return;
}

void hMLPBD::projection(const int_t icell, const int_t iorder)
{
	for (int_t istate = 0; istate < num_states_; istate++)
	for (int_t ibasis = num_basis_list_[iorder]; ibasis < num_basis_; ibasis++)
		(*solution_)[icell][istate*num_basis_ + ibasis] = 0.0;
}

bool hMLPBD::indicator(const int_t icell, const int_t order)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const int_t num_vertex = cell_to_node_ptr[icell + 1] - cell_to_node_ptr[icell];

	for (int_t ivertex = 0; ivertex < num_vertex; ivertex++){
		bool flag = MLPCondition(icell, ivertex);
		if (flag == true){
			if (order>1){
				flag = smoothDetect(icell, ivertex);
				if (flag == false){
					if (boundaryDetect(icell, (*cell_element_)[icell]->getNumForTypeTwo())) flag = true; // type two boundary detector
				}
				else if (flag == true){
					if (!boundaryDetect(icell, (*cell_element_)[icell]->getNumForTypeTwo())) flag = false; // type two boundary detector
				}
			}
		}
		if (flag == true){
			return true;
		}
	}
	return false;
}

bool hMLPBD::Deactivation(const int_t icell, const int_t ivertex)
{
	const real_t eps = 1.0e-3;
	real_t vertex_value = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		vertex_value += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];
	real_t average_value = (*solution_)[icell][target_state_*num_basis_] * (*vertex_basis_)[icell][ivertex*num_basis_];
	if (std::abs(vertex_value - average_value) <= std::max(eps*std::abs(average_value), (*cell_volumes_)[icell]))
		return true;
	return false;
}
bool hMLPBD::MLPCondition(const int_t icell, const int_t ivertex)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();

	real_t P1var = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_list_[1]; ibasis++)
		P1var += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];

	if (Deactivation(icell, ivertex) == true)
		return false;

	if (P1var > vertex_max_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) return true;
	else if (P1var < vertex_min_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) return true;
	else return false;
}

bool hMLPBD::smoothDetect(const int_t icell, const int_t ivertex)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();

	real_t vertex_value = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		vertex_value += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];

	real_t P1term = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_list_[1]; ibasis++)
		P1term += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];

	const real_t high_term = vertex_value - P1term;
	P1term -= (*solution_)[icell][target_state_*num_basis_] * (*vertex_basis_)[icell][0];

	const real_t eps = 1.0E-14;
	if ((P1term>0.0) && (high_term<eps) && (vertex_value>vertex_min_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]])) // c1 condition
		return false;
	else if ((P1term<0.0) && (high_term>-eps) && (vertex_value < vertex_max_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]])) // c2 condition
		return false;

	return true;
}

void hMLPBD::MLPu1(const int_t icell)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();
	const int_t num_vertex = cell_to_node_ptr[icell + 1] - cell_to_node_ptr[icell];

	std::vector<real_t> p1_value(num_states_*num_vertex, 0.0);
	for (int_t istate = 0; istate < num_states_; istate++)
	for (int_t ivertex = 0; ivertex < num_vertex; ivertex++)
	for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		p1_value[istate*num_vertex + ivertex] += (*solution_)[icell][istate*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];

	for (int_t istate = 0; istate < num_states_; istate++){
		real_t temp;
		real_t phi = 1.0;
		for (int_t ivertex = 0; ivertex < num_vertex; ivertex++){
			temp = (*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - p1_value[istate*num_vertex + ivertex];
			if (p1_value[istate*num_vertex + ivertex] < vertex_min_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]){
				if (std::abs(temp) < 1.0E-14){
					temp = 0.0;
				}
				else{
					//temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_min_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					if ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] > vertex_average_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]){
						temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_average_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					}
					else{
						temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_min_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					}

				}
				if (temp < phi){
					phi = temp;
				}
			}
			else if (vertex_max_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]] < p1_value[istate*num_vertex + ivertex]){
				if (std::abs(temp) < 1.0E-14){
					temp = 0.0;
				}
				else{
					//temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_max_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					if ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] < vertex_average_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]){
						temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_average_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					}
					else{
						temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_max_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					}
				}
				if (temp < phi){
					phi = temp;
				}
			}
		}
		for (int_t ibasis = 1; ibasis < num_basis_list_[1]; ibasis++)
			(*solution_)[icell][istate*num_basis_ + ibasis] *= phi;
	}
}

void hMLPBD::construct()
{
	const std::vector<std::vector<real_t>>& basis = grid_->getCellBasisValue();
	const std::vector<std::vector<int_t>>& node_to_cells = grid_->getNodeToCells();
	const std::vector<real_t>& cell_volumes = grid_->getCellVolumes();

	std::vector<std::vector<real_t>> cell_averages(num_states_);
	for (int_t icell = 0; icell < num_total_cells_; icell++)
	for (int_t istate = 0; istate < num_states_; istate++)
		cell_averages[istate].push_back((*solution_)[icell][istate*num_basis_] * basis[icell][0]);
	cell_averages_ = move(cell_averages);

	std::vector<std::vector<real_t>> vertex_max(num_states_);
	std::vector<std::vector<real_t>> vertex_min(num_states_);
	std::vector<std::vector<real_t>> vertex_average(num_states_);
	for (int_t inode = 0; inode < num_total_nodes_; inode++){

		std::vector<std::vector<real_t>> local_cell_averages(num_states_);
		std::vector<real_t> state_sum(num_states_, 0.0);
		real_t area_sum = 0.0;

		for (auto&& icell : node_to_cells[inode]){
			for (int_t istate = 0; istate < num_states_; istate++){
				local_cell_averages[istate].push_back(cell_averages_[istate][icell]);
				state_sum[istate] += cell_averages_[istate][icell] * cell_volumes[icell];
			}
			area_sum += cell_volumes[icell];
		}

		for (int_t istate = 0; istate < num_states_; istate++){
			vertex_max[istate].push_back(*max_element(local_cell_averages[istate].begin(), local_cell_averages[istate].end()));
			vertex_min[istate].push_back(*min_element(local_cell_averages[istate].begin(), local_cell_averages[istate].end()));
			vertex_average[istate].push_back(state_sum[istate] / area_sum);
		}
	}
	vertex_max_ = move(vertex_max);
	vertex_min_ = move(vertex_min);
	vertex_average_ = move(vertex_average);

	// face sweep
	const std::vector<int_t>& num_face_quad_points = grid_->getNumFaceQuadPointsHMLPBD();
	const std::vector<int_t>& face_owner_cell = grid_->getFaceOwnerCell();
	const std::vector<int_t>& face_neighbor_cell = grid_->getFaceNeighborCell();
	const std::vector<real_t>& face_areas = grid_->getFaceAreas();
	const std::vector<real_t>& face_characteristic_length = grid_->getFaceCharacteristicLength();
	const std::vector<std::vector<real_t>>& face_owner_basis_value = grid_->getFaceOwnerBasisValueHMLPBD();
	const std::vector<std::vector<real_t>>& face_neighbor_basis_value = grid_->getFaceNeighborBasisValueHMLPBD();
	const std::vector<std::vector<real_t>>& face_weights = grid_->getFaceWeightsHMLPBD();

	std::vector<std::vector<real_t>> face_difference(num_states_);
	for (int_t iface = 0; iface < num_faces_; iface++){
		const int_t num_points = num_face_quad_points[iface];
		const int_t owner_cell = face_owner_cell[iface];
		const int_t neighbor_cell = face_neighbor_cell[iface];

		std::vector<real_t> owner_solution(num_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_solution[ipoint*num_states_ + istate] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * (*solution_)[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			neighbor_solution[ipoint*num_states_ + istate] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * (*solution_)[neighbor_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> state_sum(num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
			state_sum[istate] += std::abs(owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*face_weights[iface][ipoint];

		const real_t denominator = std::pow(face_characteristic_length[iface], 0.5*static_cast<real_t>(order_ + 1));
		for (int_t istate = 0; istate < num_states_; istate++){
			state_sum[istate] /= (face_areas[iface]*denominator);
			face_difference[istate].push_back(state_sum[istate]);
		}
	}
	face_difference_ = move(face_difference);

	// peribdry sweep
	const std::vector<int_t>& num_peribdry_quad_points = grid_->getNumPeriBdryQuadPointsHMLPBD();
	const std::vector<int_t>& peribdry_owner_cell = grid_->getPeriBdryOwnerCell();
	const std::vector<int_t>& peribdry_neighbor_cell = grid_->getPeriBdryNeighborCell();
	const std::vector<real_t>& peribdry_areas = grid_->getPeriBdryAreas();
	const std::vector<real_t>& peribdry_characteristic_length = grid_->getPeriBdryCharacteristicLength();
	const std::vector<std::vector<real_t>>& peribdry_owner_basis_value = grid_->getPeriBdryOwnerBasisValueHMLPBD();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_basis_value = grid_->getPeriBdryNeighborBasisValueHMLPBD();
	const std::vector<std::vector<real_t>>& peribdry_weights = grid_->getPeriBdryWeightsHMLPBD();

	std::vector<std::vector<real_t>> bdry_difference(num_states_);
	for (int_t ibdry = 0; ibdry < num_peribdries_; ibdry++){
		const int_t& num_points = num_peribdry_quad_points[ibdry];
		const int_t& owner_cell = peribdry_owner_cell[ibdry];
		const int_t& neighbor_cell = peribdry_neighbor_cell[ibdry];

		std::vector<real_t> owner_solution(num_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_solution[ipoint*num_states_ + istate] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * (*solution_)[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			neighbor_solution[ipoint*num_states_ + istate] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * (*solution_)[neighbor_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> state_sum(num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
			state_sum[istate] += std::abs(owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*peribdry_weights[ibdry][ipoint];

		const real_t denominator = std::pow(peribdry_characteristic_length[ibdry], 0.5*static_cast<real_t>(order_ + 1));
		for (int_t istate = 0; istate < num_states_; istate++){
			state_sum[istate] /= (peribdry_areas[ibdry]*denominator);
			bdry_difference[istate].push_back(state_sum[istate]);
		}
	}
	bdry_difference_ = move(bdry_difference);

	const std::vector<std::vector<int_t>>& cell_to_faces = grid_->getCellToFaces();
	std::vector<int_t> cell_num_troubled_boundary(num_cells_, 0);
	for (int_t icell = 0; icell < num_cells_; icell++){
		const int_t num_faces = cell_to_faces[icell].size() / 2;
		for (int_t iface = 0; iface < num_faces;iface++){
			if (cell_to_faces[icell][iface*2+1] == 1){ // face
				for (int_t istate = 0; istate < num_states_; istate++){
					if (face_difference_[istate][cell_to_faces[icell][iface * 2]]>1.0){
						cell_num_troubled_boundary[icell]++; break;
					}
				}
			}
			else if (cell_to_faces[icell][iface * 2 + 1] == -1){ // peribdry
				for (int_t istate = 0; istate < num_states_; istate++){
					if (bdry_difference_[istate][cell_to_faces[icell][iface * 2]]>1.0){
						cell_num_troubled_boundary[icell]++; break;
					}
				}
			}
		}
	}
	cell_num_troubled_boundary_ = move(cell_num_troubled_boundary);
}

bool hMLPBD::boundaryDetect(const int_t icell, const int_t num_troubled_boundary)
{
	if (cell_num_troubled_boundary_[icell] >= num_troubled_boundary) return true;
	return false;
}

bool hMLPBD::isContact(const int_t icell)
{
	const std::vector<std::vector<real_t>>& basis = grid_->getCellBasisValue();

	std::vector<real_t> u(num_states_);
	for (int_t istate = 0; istate < num_states_; istate++)
		u[istate] = (*solution_)[icell][istate*num_basis_]*basis[icell][0];

	const std::vector<int_t>& cells_index = grid_->getCellToCells()[icell];
	std::vector<real_t> neighbor_u(cells_index.size()*num_states_);
	for (int_t icell = 0; icell < cells_index.size();icell++)
	for (int_t istate = 0; istate < num_states_; istate++)
		neighbor_u[icell*num_states_ + istate] = (*solution_)[cells_index[icell]][istate*num_basis_] * basis[cells_index[icell]][0];

	return equation_->isContact(u, neighbor_u);
}

// -------------------------------------- hMLP_BD_SIM ----------------------------------

hMLPBD_SIM::hMLPBD_SIM()
{
	setName("hMLPBD_SIM");
	Limiter::getInstance().addRegistry(getName(), this);
}
void hMLPBD_SIM::initialize(std::shared_ptr<Grid> grid, Equation* equation, std::vector<std::vector<real_t>>* solution)
{
	Config& config = Config::getInstance();
	order_ = config.getOrder();
	int_t dimension = config.getDimension();
	num_basis_list_.resize(order_ + 1);
	for (int_t iorder = 0; iorder < order_ + 1; iorder++){
		if (dimension == 2)
			num_basis_list_[iorder] = (iorder + 1)*(iorder + 2) / 2;
		else if (dimension == 3)
			num_basis_list_[iorder] = (iorder + 1)*(iorder + 2)*(iorder + 3) / 6;
	}

	grid_ = grid;
	num_total_nodes_ = grid_->getNumTotalNodes();
	num_total_cells_ = grid_->getNumTotalCells();
	num_cells_ = grid_->getNumCells();
	num_faces_ = grid_->getNumFaces();
	num_peribdries_ = grid_->getNumPeriBdries();
	num_basis_ = grid_->getNumBasis();
	vertex_basis_ = grid_->getCellVertexBasisValue();
	cell_element_ = &grid_->getCellElement();
	cell_volumes_ = &(grid_->getCellVolumes());

	grid_->prpcForHmlpBdSim();
	simplex_average_coefficients_ = &grid_->getSimplexAverageCoefficients();
	simplex_p1_coefficients_ = &grid_->getSimplexP1Coefficients();
	simplex_volumes_ = &grid_->getSimplexVolumes();

	equation_ = equation;
	num_states_ = equation_->getNumStates();

	solution_ = solution;

	target_state_ = 0;

	return;
}
void hMLPBD_SIM::limiting()
{
	construct();

	for (int_t icell = 0; icell < num_cells_; icell++){
		bool is_trouble = true;
		int_t order = order_;

		for (;;){
			if (order == 0) {
				is_trouble = false;
				break;
			}

			is_trouble = indicator(icell, order);

			if (is_trouble == true){
				if (order == 1){
					break;
				}
				else if (order == 2){
					order = 1;
					projection(icell, 1);
					break;
				}
				else{
					is_trouble = false;
					order = order - 1;
					projection(icell, order);
				}
			}
			else{
				if (boundaryDetect(icell, (*cell_element_)[icell]->getNumForTypeOne()))
					if (!isContact(icell))
						projection(icell, 1); // type one boundary detector
				break;
			}
		}
		if (is_trouble == true){
			MLPu1(icell);
		}
	}
	return;
}

void hMLPBD_SIM::projection(const int_t icell, const int_t iorder)
{
	for (int_t istate = 0; istate < num_states_; istate++)
	for (int_t ibasis = num_basis_list_[iorder]; ibasis < num_basis_; ibasis++)
		(*solution_)[icell][istate*num_basis_ + ibasis] = 0.0;
}

bool hMLPBD_SIM::indicator(const int_t icell, const int_t order)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const int_t num_vertex = cell_to_node_ptr[icell + 1] - cell_to_node_ptr[icell];

	for (int_t ivertex = 0; ivertex < num_vertex; ivertex++){
		bool flag = MLPCondition(icell, ivertex);
		if (flag == true){
			if (order>1){
				flag = smoothDetect(icell, ivertex);
				if (flag == false){
					if (boundaryDetect(icell, (*cell_element_)[icell]->getNumForTypeTwo())) flag = true; // type two boundary detector
				}
				else if (flag == true){
					if (!boundaryDetect(icell, (*cell_element_)[icell]->getNumForTypeTwo())) flag = false; // type two boundary detector
				}
			}
		}
		if (flag == true){
			return true;
		}
	}
	return false;
}

bool hMLPBD_SIM::Deactivation(const int_t icell, const int_t ivertex)
{
	const real_t eps = 1.0e-3;
	real_t vertex_value = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		vertex_value += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];
	real_t average_value = (*solution_)[icell][target_state_*num_basis_] * (*vertex_basis_)[icell][ivertex*num_basis_];
	if (std::abs(vertex_value - average_value) <= std::max(eps*std::abs(average_value), (*cell_volumes_)[icell]))
		return true;
	return false;
}
bool hMLPBD_SIM::MLPCondition(const int_t icell, const int_t ivertex)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();

	real_t P1var = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		P1var += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*simplex_p1_coefficients_)[icell][ivertex*num_basis_ + ibasis];

	if (Deactivation(icell, ivertex) == true)
		return false;

	if (P1var > vertex_max_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) return true;
	else if (P1var < vertex_min_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) return true;
	else return false;
}

bool hMLPBD_SIM::smoothDetect(const int_t icell, const int_t ivertex)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();

	real_t vertex_value = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		vertex_value += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];

	real_t P1term = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		P1term += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*simplex_p1_coefficients_)[icell][ivertex*num_basis_ + ibasis];

	real_t average = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		average += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*simplex_average_coefficients_)[icell][ivertex*num_basis_ + ibasis];

	const real_t high_term = vertex_value - P1term;
	P1term -= average;

	const real_t eps = 1.0E-14;
	if ((P1term>0.0) && (high_term<eps) && (vertex_value>vertex_min_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]])) // c1 condition
		return false;
	else if ((P1term<0.0) && (high_term>-eps) && (vertex_value < vertex_max_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]])) // c2 condition
		return false;

	return true;
}

void hMLPBD_SIM::MLPu1(const int_t icell)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();
	const int_t num_vertex = cell_to_node_ptr[icell + 1] - cell_to_node_ptr[icell];

	std::vector<real_t> p1_value(num_states_*num_vertex, 0.0);
	for (int_t istate = 0; istate < num_states_; istate++)
	for (int_t ivertex = 0; ivertex < num_vertex; ivertex++)
	for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		p1_value[istate*num_vertex + ivertex] += (*solution_)[icell][istate*num_basis_ + ibasis] * (*simplex_p1_coefficients_)[icell][ivertex*num_basis_ + ibasis];

	for (int_t istate = 0; istate < num_states_; istate++){
		real_t temp;
		real_t phi = 1.0;
		for (int_t ivertex = 0; ivertex < num_vertex; ivertex++){
			temp = (*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - p1_value[istate*num_vertex + ivertex];
			if (p1_value[istate*num_vertex + ivertex] < vertex_min_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]){
				if (std::abs(temp) < 1.0E-14){
					temp = 0.0;
				}
				else{
					//temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_min_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					if ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] > vertex_average_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]){
						temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_average_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					}
					else{
						temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_min_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					}
				}
				if (temp < phi){
					phi = temp;
				}
			}
			else if (vertex_max_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]] < p1_value[istate*num_vertex + ivertex]){
				if (std::abs(temp) < 1.0E-14){
					temp = 0.0;
				}
				else{
					//temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_max_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					if ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] < vertex_average_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]){
						temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_average_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					}
					else{
						temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_max_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
					}
				}
				if (temp < phi){
					phi = temp;
				}
			}
		}
		for (int_t ibasis = 1; ibasis < num_basis_list_[1]; ibasis++)
			(*solution_)[icell][istate*num_basis_ + ibasis] *= phi;
	}
}

void hMLPBD_SIM::construct()
{
	const std::vector<std::vector<real_t>>& basis = grid_->getCellBasisValue();
	const std::vector<std::vector<int_t>>& node_to_cells = grid_->getNodeToCells();
	const std::vector<std::vector<int_t>>& node_to_vertex = grid_->getNodeToVertex();
	const std::vector<real_t>& cell_volumes = grid_->getCellVolumes();
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();

	std::vector<std::vector<real_t>> cell_vertex_averages(num_total_cells_);
	for (int_t icell = 0; icell < num_total_cells_; icell++){
		const int_t num_vertex = cell_to_node_ptr[icell + 1] - cell_to_node_ptr[icell];
		cell_vertex_averages[icell].resize(num_vertex*num_states_, 0.0);
		for (int_t ivertex = 0; ivertex < num_vertex; ivertex++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			cell_vertex_averages[icell][ivertex*num_states_ + istate] += (*solution_)[icell][istate*num_basis_ + ibasis] * (*simplex_average_coefficients_)[icell][ivertex*num_basis_ + ibasis];
	}

	std::vector<std::vector<real_t>> vertex_max(num_states_);
	std::vector<std::vector<real_t>> vertex_min(num_states_);
	std::vector<std::vector<real_t>> vertex_average(num_states_);
	for (int_t inode = 0; inode < num_total_nodes_; inode++){
		std::vector<std::vector<real_t>> local_cell_averages(num_states_);
		std::vector<real_t> state_sum(num_states_, 0.0);
		real_t area_sum = 0.0;

		for (int_t icell = 0; icell < node_to_cells[inode].size(); icell++){
			const int_t& cell_index = node_to_cells[inode][icell];

			for (int_t istate = 0; istate < num_states_; istate++){	
				switch ((*cell_element_)[cell_index]->getElemType()){
				case ElemType::TRIS:
					local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][istate]);
					state_sum[istate] += cell_vertex_averages[cell_index][istate] * (*simplex_volumes_)[cell_index][0];
					if (istate == 0)
						area_sum += (*simplex_volumes_)[cell_index][0];
					break;
				case ElemType::QUAD:
					local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][node_to_vertex[inode][icell]*num_states_ + istate]);
					state_sum[istate] += cell_vertex_averages[cell_index][node_to_vertex[inode][icell] * num_states_ + istate] * (*simplex_volumes_)[cell_index][node_to_vertex[inode][icell]];
					if (istate == 0)
						area_sum += (*simplex_volumes_)[cell_index][node_to_vertex[inode][icell]];
					switch (node_to_vertex[inode][icell]){
					case 0:
					case 2:
						local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][num_states_ + istate]);
						local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][3*num_states_ + istate]);
						state_sum[istate] += cell_vertex_averages[cell_index][num_states_ + istate] * (*simplex_volumes_)[cell_index][1];
						state_sum[istate] += cell_vertex_averages[cell_index][3 * num_states_ + istate] * (*simplex_volumes_)[cell_index][3];
						if (istate == 0){
							area_sum += (*simplex_volumes_)[cell_index][1];
							area_sum += (*simplex_volumes_)[cell_index][3];
						}
						break;
					case 1:
					case 3:
						local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][istate]);
						local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][2 * num_states_ + istate]);
						state_sum[istate] += cell_vertex_averages[cell_index][istate] * (*simplex_volumes_)[cell_index][0];
						state_sum[istate] += cell_vertex_averages[cell_index][2 * num_states_ + istate] * (*simplex_volumes_)[cell_index][2];
						if (istate == 0){
							area_sum += (*simplex_volumes_)[cell_index][0];
							area_sum += (*simplex_volumes_)[cell_index][2];
						}
						break;
					}
					break;
				}
			}
		}

		for (int_t istate = 0; istate < num_states_; istate++){
			vertex_max[istate].push_back(*max_element(local_cell_averages[istate].begin(), local_cell_averages[istate].end()));
			vertex_min[istate].push_back(*min_element(local_cell_averages[istate].begin(), local_cell_averages[istate].end()));
			vertex_average[istate].push_back(state_sum[istate] / area_sum);
		}
	}
	vertex_max_ = move(vertex_max);
	vertex_min_ = move(vertex_min);
	vertex_average_ = move(vertex_average);

	// face sweep
	const std::vector<int_t>& num_face_quad_points = grid_->getNumFaceQuadPointsHMLPBD();
	const std::vector<int_t>& face_owner_cell = grid_->getFaceOwnerCell();
	const std::vector<int_t>& face_neighbor_cell = grid_->getFaceNeighborCell();
	const std::vector<real_t>& face_areas = grid_->getFaceAreas();
	const std::vector<real_t>& face_characteristic_length = grid_->getFaceCharacteristicLength();
	const std::vector<std::vector<real_t>>& face_owner_basis_value = grid_->getFaceOwnerBasisValueHMLPBD();
	const std::vector<std::vector<real_t>>& face_neighbor_basis_value = grid_->getFaceNeighborBasisValueHMLPBD();
	const std::vector<std::vector<real_t>>& face_weights = grid_->getFaceWeightsHMLPBD();

	std::vector<std::vector<real_t>> face_difference(num_states_);
	for (int_t iface = 0; iface < num_faces_; iface++){
		const int_t num_points = num_face_quad_points[iface];
		const int_t owner_cell = face_owner_cell[iface];
		const int_t neighbor_cell = face_neighbor_cell[iface];

		std::vector<real_t> owner_solution(num_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_solution[ipoint*num_states_ + istate] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * (*solution_)[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			neighbor_solution[ipoint*num_states_ + istate] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * (*solution_)[neighbor_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> state_sum(num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
			state_sum[istate] += std::abs(owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*face_weights[iface][ipoint];

		const real_t denominator = std::pow(face_characteristic_length[iface], 0.5*static_cast<real_t>(order_ + 1));
		for (int_t istate = 0; istate < num_states_; istate++){
			state_sum[istate] /= (face_areas[iface] * denominator);
			face_difference[istate].push_back(state_sum[istate]);
		}
	}
	face_difference_ = move(face_difference);

	// peribdry sweep
	const std::vector<int_t>& num_peribdry_quad_points = grid_->getNumPeriBdryQuadPointsHMLPBD();
	const std::vector<int_t>& peribdry_owner_cell = grid_->getPeriBdryOwnerCell();
	const std::vector<int_t>& peribdry_neighbor_cell = grid_->getPeriBdryNeighborCell();
	const std::vector<real_t>& peribdry_areas = grid_->getPeriBdryAreas();
	const std::vector<real_t>& peribdry_characteristic_length = grid_->getPeriBdryCharacteristicLength();
	const std::vector<std::vector<real_t>>& peribdry_owner_basis_value = grid_->getPeriBdryOwnerBasisValueHMLPBD();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_basis_value = grid_->getPeriBdryNeighborBasisValueHMLPBD();
	const std::vector<std::vector<real_t>>& peribdry_weights = grid_->getPeriBdryWeightsHMLPBD();

	std::vector<std::vector<real_t>> bdry_difference(num_states_);
	for (int_t ibdry = 0; ibdry < num_peribdries_; ibdry++){
		const int_t& num_points = num_peribdry_quad_points[ibdry];
		const int_t& owner_cell = peribdry_owner_cell[ibdry];
		const int_t& neighbor_cell = peribdry_neighbor_cell[ibdry];

		std::vector<real_t> owner_solution(num_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_solution[ipoint*num_states_ + istate] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * (*solution_)[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			neighbor_solution[ipoint*num_states_ + istate] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * (*solution_)[neighbor_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> state_sum(num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
			state_sum[istate] += std::abs(owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*peribdry_weights[ibdry][ipoint];

		const real_t denominator = std::pow(peribdry_characteristic_length[ibdry], 0.5*static_cast<real_t>(order_ + 1));
		for (int_t istate = 0; istate < num_states_; istate++){
			state_sum[istate] /= (peribdry_areas[ibdry] * denominator);
			bdry_difference[istate].push_back(state_sum[istate]);
		}
	}
	bdry_difference_ = move(bdry_difference);

	const std::vector<std::vector<int_t>>& cell_to_faces = grid_->getCellToFaces();
	std::vector<int_t> cell_num_troubled_boundary(num_cells_, 0);
	for (int_t icell = 0; icell < num_cells_; icell++){
		const int_t num_faces = cell_to_faces[icell].size() / 2;
		for (int_t iface = 0; iface < num_faces; iface++){
			if (cell_to_faces[icell][iface * 2 + 1] == 1){ // face
				for (int_t istate = 0; istate < num_states_; istate++){
					if (face_difference_[istate][cell_to_faces[icell][iface * 2]]>1.0){
						cell_num_troubled_boundary[icell]++; break;
					}
				}
			}
			else if (cell_to_faces[icell][iface * 2 + 1] == -1){ // peribdry
				for (int_t istate = 0; istate < num_states_; istate++){
					if (bdry_difference_[istate][cell_to_faces[icell][iface * 2]]>1.0){
						cell_num_troubled_boundary[icell]++; break;
					}
				}
			}
		}
	}
	cell_num_troubled_boundary_ = move(cell_num_troubled_boundary);
}

bool hMLPBD_SIM::boundaryDetect(const int_t icell, const int_t num_troubled_boundary)
{
	if (cell_num_troubled_boundary_[icell] >= num_troubled_boundary) return true;
	return false;
}

bool hMLPBD_SIM::isContact(const int_t icell)
{
	const std::vector<std::vector<real_t>>& basis = grid_->getCellBasisValue();

	std::vector<real_t> u(num_states_);
	for (int_t istate = 0; istate < num_states_; istate++)
		u[istate] = (*solution_)[icell][istate*num_basis_] * basis[icell][0];

	const std::vector<int_t>& cells_index = grid_->getCellToCells()[icell];
	std::vector<real_t> neighbor_u(cells_index.size()*num_states_);
	for (int_t icell = 0; icell < cells_index.size(); icell++)
	for (int_t istate = 0; istate < num_states_; istate++)
		neighbor_u[icell*num_states_ + istate] = (*solution_)[cells_index[icell]][istate*num_basis_] * basis[cells_index[icell]][0];

	return equation_->isContact(u, neighbor_u);
}

// -------------------------------------- hMLP_SIM -------------------------------------

hMLP_SIM::hMLP_SIM()
{
	setName("hMLP_SIM");
	Limiter::getInstance().addRegistry(getName(), this);
}
void hMLP_SIM::initialize(std::shared_ptr<Grid> grid, Equation* equation, std::vector<std::vector<real_t>>* solution)
{
	Config& config = Config::getInstance();
	order_ = config.getOrder();
	int_t dimension = config.getDimension();
	num_basis_list_.resize(order_ + 1);
	for (int_t iorder = 0; iorder < order_ + 1; iorder++){
		if (dimension == 2)
			num_basis_list_[iorder] = (iorder + 1)*(iorder + 2) / 2;
		else if (dimension == 3)
			num_basis_list_[iorder] = (iorder + 1)*(iorder + 2)*(iorder + 3) / 6;
	}

	grid_ = grid;
	num_total_nodes_ = grid_->getNumTotalNodes();
	num_total_cells_ = grid_->getNumTotalCells();
	num_cells_ = grid_->getNumCells();
	num_basis_ = grid_->getNumBasis();
	vertex_basis_ = grid_->getCellVertexBasisValue();
	cell_element_ = &grid_->getCellElement();
	cell_volumes_ = &(grid_->getCellVolumes());

	grid_->prpcForHmlpBdSim();
	simplex_average_coefficients_ = &grid_->getSimplexAverageCoefficients();
	simplex_p1_coefficients_ = &grid_->getSimplexP1Coefficients();
	simplex_volumes_ = &grid_->getSimplexVolumes();

	equation_ = equation;
	num_states_ = equation_->getNumStates();

	solution_ = solution;

	target_state_ = 0;

	return;
}
void hMLP_SIM::limiting()
{
	construct();

	/* for debug*/
	//const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	//const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();
	//const std::vector<real_t>& node_coords = grid_->getNodeCoords();
	/* for debug*/

	for (int_t icell = 0; icell < num_cells_; icell++){
		bool is_trouble = true;
		int_t order = order_;

		/* for debug */
		//const std::vector<int_t> target_cell_to_node(cell_to_node_ind.begin() + cell_to_node_ptr[icell], cell_to_node_ind.begin() + cell_to_node_ptr[icell + 1]);
		//std::vector<real_t> target_coords;
		//for (auto&& inode : target_cell_to_node)
		//for (int_t idim = 0; idim < 2; idim++)
		//	target_coords.push_back(node_coords[inode * 2 + idim]);
		/* for debug */

		for (;;){
			if (order == 0) {
				is_trouble = false;
				break;
			}

			is_trouble = indicator(icell, order);

			if (is_trouble == true){
				if (order == 1){
					break;
				}
				else if (order == 2){
					order = 1;
					//MEMBER_ERROR("ERROR!");
					//MEMBER_MESSAGE("ERROR!");
					projection(icell, 1);
					break;
				}
				else{
					is_trouble = false;
					order = order - 1;
					//MEMBER_ERROR("ERROR!");
					//MEMBER_MESSAGE("ERROR!");
					projection(icell, order);
				}
			}
			else{
				break;
			}
		}
		if (is_trouble == true){
			//MEMBER_ERROR("ERROR!");
			//MEMBER_MESSAGE("ERROR!");
			MLPu1(icell);
		}
	}
	return;
}

void hMLP_SIM::projection(const int_t icell, const int_t iorder)
{
	for (int_t istate = 0; istate < num_states_; istate++)
	for (int_t ibasis = num_basis_list_[iorder]; ibasis < num_basis_; ibasis++)
		(*solution_)[icell][istate*num_basis_ + ibasis] = 0.0;
}

bool hMLP_SIM::indicator(const int_t icell, const int_t order)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const int_t num_vertex = cell_to_node_ptr[icell + 1] - cell_to_node_ptr[icell];

	for (int_t ivertex = 0; ivertex < num_vertex; ivertex++){
		bool flag = MLPCondition(icell, ivertex);
		if (flag == true){
			if (order>1){
				flag = smoothDetect(icell, ivertex);
			}
		}
		if (flag == true){
			return true;
		}
	}
	return false;
}

bool hMLP_SIM::Deactivation(const int_t icell, const int_t ivertex)
{
	const real_t eps = 1.0e-3;
	real_t vertex_value = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		vertex_value += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];
	real_t average_value = (*solution_)[icell][target_state_*num_basis_] * (*vertex_basis_)[icell][ivertex*num_basis_];
	if (std::abs(vertex_value - average_value) <= std::max(eps*std::abs(average_value), (*cell_volumes_)[icell]))
		return true;
	return false;
}
bool hMLP_SIM::MLPCondition(const int_t icell, const int_t ivertex)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();

	real_t P1var = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		P1var += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*simplex_p1_coefficients_)[icell][ivertex*num_basis_ + ibasis];

	if (Deactivation(icell, ivertex) == true)
		return false;

	if (P1var > vertex_max_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) return true;
	else if (P1var < vertex_min_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) return true;
	else return false;
}

bool hMLP_SIM::smoothDetect(const int_t icell, const int_t ivertex)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();

	real_t vertex_value = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		vertex_value += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];

	real_t P1term = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		P1term += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*simplex_p1_coefficients_)[icell][ivertex*num_basis_ + ibasis];

	real_t average = 0.0;
	for (int_t ibasis = 0; ibasis<num_basis_; ibasis++)
		average += (*solution_)[icell][target_state_*num_basis_ + ibasis] * (*simplex_average_coefficients_)[icell][ivertex*num_basis_ + ibasis];

	const real_t high_term = vertex_value - P1term;
	P1term -= average;

	const real_t eps = 1.0E-14;
	if ((P1term>0.0) && (high_term<eps) && (vertex_value>vertex_min_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]])) // c1 condition
		return false;
	else if ((P1term<0.0) && (high_term>-eps) && (vertex_value < vertex_max_[target_state_][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]])) // c2 condition
		return false;

	return true;
}

void hMLP_SIM::MLPu1(const int_t icell)
{
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();
	const std::vector<int_t>& cell_to_node_ind = grid_->getCellToNodeInd();
	const int_t num_vertex = cell_to_node_ptr[icell + 1] - cell_to_node_ptr[icell];

	std::vector<real_t> p1_value(num_states_*num_vertex, 0.0);
	for (int_t istate = 0; istate < num_states_; istate++)
	for (int_t ivertex = 0; ivertex < num_vertex; ivertex++)
	for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		p1_value[istate*num_vertex + ivertex] += (*solution_)[icell][istate*num_basis_ + ibasis] * (*vertex_basis_)[icell][ivertex*num_basis_ + ibasis];

	for (int_t istate = 0; istate < num_states_; istate++){
		real_t temp;
		real_t phi = 1.0;
		for (int_t ivertex = 0; ivertex < num_vertex; ivertex++){
			temp = (*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - p1_value[istate*num_vertex + ivertex];
			if (p1_value[istate*num_vertex + ivertex] < vertex_min_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]){
				if (std::abs(temp) < 1.0E-14){
					temp = 0.0;
				}
				else{
					temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_min_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
				}
				if (temp < phi){
					phi = temp;
				}
			}
			else if (vertex_max_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]] < p1_value[istate*num_vertex + ivertex]){
				if (std::abs(temp) < 1.0E-14){
					temp = 0.0;
				}
				else{
					temp = ((*solution_)[icell][istate*num_basis_] * (*vertex_basis_)[icell][0] - vertex_max_[istate][cell_to_node_ind[cell_to_node_ptr[icell] + ivertex]]) / temp;
				}
				if (temp < phi){
					phi = temp;
				}
			}
		}
		for (int_t ibasis = 1; ibasis < num_basis_list_[1]; ibasis++)
			(*solution_)[icell][istate*num_basis_ + ibasis] *= phi;
	}
}

void hMLP_SIM::construct()
{
	const std::vector<std::vector<real_t>>& basis = grid_->getCellBasisValue();
	const std::vector<std::vector<int_t>>& node_to_cells = grid_->getNodeToCells();
	const std::vector<std::vector<int_t>>& node_to_vertex = grid_->getNodeToVertex();
	const std::vector<real_t>& cell_volumes = grid_->getCellVolumes();
	const std::vector<int_t>& cell_to_node_ptr = grid_->getCellToNodePtr();

	std::vector<std::vector<real_t>> cell_vertex_averages(num_total_cells_);
	for (int_t icell = 0; icell < num_total_cells_; icell++){
		const int_t num_vertex = cell_to_node_ptr[icell + 1] - cell_to_node_ptr[icell];
		cell_vertex_averages[icell].resize(num_vertex*num_states_, 0.0);
		for (int_t ivertex = 0; ivertex < num_vertex; ivertex++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			cell_vertex_averages[icell][ivertex*num_states_ + istate] += (*solution_)[icell][istate*num_basis_ + ibasis] * (*simplex_average_coefficients_)[icell][ivertex*num_basis_ + ibasis];
	}

	std::vector<std::vector<real_t>> vertex_max(num_states_);
	std::vector<std::vector<real_t>> vertex_min(num_states_);
	for (int_t inode = 0; inode < num_total_nodes_; inode++){
		std::vector<std::vector<real_t>> local_cell_averages(num_states_);

		for (int_t icell = 0; icell < node_to_cells[inode].size(); icell++){
			const int_t& cell_index = node_to_cells[inode][icell];

			for (int_t istate = 0; istate < num_states_; istate++){
				switch ((*cell_element_)[cell_index]->getElemType()){
				case ElemType::TRIS:
					local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][istate]);
					break;
				case ElemType::QUAD:
					local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][node_to_vertex[inode][icell]*num_states_ + istate]);
					switch (node_to_vertex[inode][icell]){
					case 0:
					case 2:
						local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][num_states_ + istate]);
						local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][3 * num_states_ + istate]);
						break;
					case 1:
					case 3:
						local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][istate]);
						local_cell_averages[istate].push_back(cell_vertex_averages[cell_index][2 * num_states_ + istate]);
						break;
					}
					break;
				}
			}
		}

		for (int_t istate = 0; istate < num_states_; istate++){
			vertex_max[istate].push_back(*max_element(local_cell_averages[istate].begin(), local_cell_averages[istate].end()));
			vertex_min[istate].push_back(*min_element(local_cell_averages[istate].begin(), local_cell_averages[istate].end()));
		}
	}
	vertex_max_ = move(vertex_max);
	vertex_min_ = move(vertex_min);
}




