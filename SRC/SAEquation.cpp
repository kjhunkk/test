
#include "../INC/SAEquation.h"

SAEquation::SAEquation()
{
	num_states_ = 1;
	variable_names_.push_back("var");

	setName("ScalarAdvection");
	Equation::getInstance().addRegistry(getName(), this);
}
void SAEquation::setBoundaries(const std::vector<int_t>& tag_numbers)
{
	int_t num_bdries = tag_numbers.size();
	boundaries_.resize(num_bdries);
	for (int_t ibdry = 0; ibdry < num_bdries; ibdry++){
		const std::string& type = BoundaryCondition::getInstance().getBCType(tag_numbers[ibdry]);
		const std::string& name = BoundaryCondition::getInstance().getBCName(tag_numbers[ibdry]);
		const std::vector<real_t>& references = BoundaryCondition::getInstance().getBCReferences(tag_numbers[ibdry]);

		boundaries_[ibdry] = BoundaryFactory<SABoundary>::getInstance().getBoundary(type, tag_numbers[ibdry], name, references);
		boundaries_[ibdry]->setFlux(flux_);
	}
}

SAConstant::SAConstant()
{
	readData("SAEquation", INPUT() + "equation.dat");
	setName("Constant");
	SAInitializer::getInstance().addRegistry(getName(), this);
}
void SAConstant::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	while (getline(infile, text)){
		if (text.find("~>dimension_", 0) != std::string::npos)
			infile >> dimension_;
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}
const std::vector<real_t> SAConstant::Problem(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 1.0);
	return results;
}
const std::vector<real_t> SAConstant::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 1.0);
	return results;
}

SASquare::SASquare()
{
	readData("SAEquation", INPUT() + "equation.dat");
	setName("Square");
	SAInitializer::getInstance().addRegistry(getName(), this);
}
void SASquare::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	while (getline(infile, text)){
		if (text.find("~>dimension_", 0) != std::string::npos)
			infile >> dimension_;
		if (text.find("~>square_coord_", 0) != std::string::npos){
			square_coord_.resize(6);
			infile >> square_coord_[0] >> square_coord_[1];
			infile >> square_coord_[2] >> square_coord_[3];
			infile >> square_coord_[4] >> square_coord_[5];
		}
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}
const std::vector<real_t> SASquare::Problem(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 1.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	for (int_t idim = 0; idim < dimension_; idim++)
	if (coord[ipoint*dimension_ + idim] < square_coord_[idim * 2] || square_coord_[idim * 2 + 1] < coord[ipoint*dimension_ + idim])
		results[ipoint] = 0.0;
	
	return results;
}
const std::vector<real_t> SASquare::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 1.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			if (coord[ipoint*dimension_ + idim] < square_coord_[idim * 2] || square_coord_[idim * 2 + 1] < coord[ipoint*dimension_ + idim])
				results[ipoint] = 0.0;

	return results;
}


SADoubleSine::SADoubleSine()
{
	readData("SAEquation", INPUT() + "equation.dat");
	setName("DoubleSine");
	SAInitializer::getInstance().addRegistry(getName(), this);
}
void SADoubleSine::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	while (getline(infile, text)){
		if (text.find("~>dimension_", 0) != std::string::npos)
			infile >> dimension_;
		// for implicit ->
		if (text.find("~>advection_", 0) != std::string::npos) {
			advection_.resize(3);
			infile >> advection_[0] >> advection_[1] >> advection_[2];
		}
		if (text.find("~>diffusion_on_", 0) != std::string::npos) {
			getline(infile, text);
			if (!text.compare("on")) diffusion_on_ = true;
			else diffusion_on_ = false;
		}
		if (text.find("~>diffusion_", 0) != std::string::npos) {
			if (diffusion_on_ == true) {
				diffusion_.resize(dimension_*dimension_);
				diffusion_wave_.resize(dimension_);
				if (dimension_ == 2) {
					infile >> diffusion_[0] >> diffusion_[1] >> diffusion_[2];
					infile >> diffusion_[2] >> diffusion_[3];
					arma::mat diff(2, 2);
					for (int_t i = 0; i < dimension_; i++)
						for (int_t j = 0; j < dimension_; j++)
							diff(i, j) = diffusion_[i*dimension_ + j];
					arma::cx_vec val;
					arma::eig_gen(val, diff);
					diffusion_wave_[0] = std::max(std::abs(val(0)), std::abs(val(1)));
					diffusion_wave_[1] = diffusion_wave_[0];
				}
				else if (dimension_ == 3) {
					infile >> diffusion_[0] >> diffusion_[1] >> diffusion_[2];
					infile >> diffusion_[3] >> diffusion_[4] >> diffusion_[5];
					infile >> diffusion_[6] >> diffusion_[7] >> diffusion_[8];
					arma::mat diff(3, 3);
					for (int_t i = 0; i < dimension_; i++)
						for (int_t j = 0; j < dimension_; j++)
							diff(i, j) = diffusion_[i*dimension_ + j];
					arma::cx_vec val;
					arma::eig_gen(val, diff);
					diffusion_wave_[0] = std::max(std::abs(val(0)), std::max(std::abs(val(1)), std::abs(val(2))));
					diffusion_wave_[1] = diffusion_wave_[0];
					diffusion_wave_[2] = diffusion_wave_[0];
				}
			}
		}
		// <- for implicit
		if (text.find("~>sine_wavelength_", 0) != std::string::npos){
			sine_wavelength_.resize(3);
			infile >> sine_wavelength_[0] >> sine_wavelength_[1] >> sine_wavelength_[2];
		}
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}
const std::vector<real_t> SADoubleSine::Problem(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 1.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		for (int_t idim = 0; idim < dimension_; idim++){
			if (std::abs(sine_wavelength_[idim]) < 1E-10) continue;
			results[ipoint] *= std::sin(2 * 3.14159265358979323846264338327950288*coord[ipoint*dimension_ + idim] / sine_wavelength_[idim]);
		}
	}
	//real_t mult = 0.0;
	//for (int_t idim = 0; idim < dimension_; idim++)
	//	if (std::abs(sine_wavelength_[idim]) > 1E-10)
	//		mult += 4.0 * pow(3.14159265358979323846264338327950288, 2.0) * diffusion_[idim*dimension_ + idim] / pow(sine_wavelength_[idim], 2.0);
	//for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
	//	for (int_t idim = 0; idim < dimension_; idim++) {
	//		if (std::abs(sine_wavelength_[idim]) < 1E-10) continue;
	//		results[ipoint] *= std::sin(2 * 3.14159265358979323846264338327950288*(coord[ipoint*dimension_ + idim] - advection_[idim]) / sine_wavelength_[idim]);
	//	}
	//	results[ipoint] *= exp(-mult);
	//}
	return results;
}
const std::vector<real_t> SADoubleSine::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 1.0);
	if (!diffusion_on_)
	{
		for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
			for (int_t idim = 0; idim < dimension_; idim++) {
				if (std::abs(sine_wavelength_[idim]) < 1E-10) continue;
				results[ipoint] *= std::sin(2 * 3.14159265358979323846264338327950288*(coord[ipoint*dimension_ + idim] - time * advection_[idim]) / sine_wavelength_[idim]);
			}
		}
	}
	else
	{
		real_t mult = 0.0;
		for (int_t idim = 0; idim < dimension_; idim++)
			if(std::abs(sine_wavelength_[idim]) > 1E-10)
				mult += 4.0 * pow(3.14159265358979323846264338327950288, 2.0) * diffusion_[idim*dimension_ + idim] / pow(sine_wavelength_[idim], 2.0);
		for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
			for (int_t idim = 0; idim < dimension_; idim++) {
				if (std::abs(sine_wavelength_[idim]) < 1E-10) continue;
				results[ipoint] *= std::sin(2 * 3.14159265358979323846264338327950288*(coord[ipoint*dimension_ + idim] - time * advection_[idim]) / sine_wavelength_[idim]);
			}
			results[ipoint] *= exp(-time * mult);
		}
	}
	return results;
}

SAUserDefine::SAUserDefine()
{
	readData("SAEquation", INPUT() + "equation.dat");
	setName("UserDefine");
	SAInitializer::getInstance().addRegistry(getName(), this);
}
void SAUserDefine::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	while (getline(infile, text)){
		if (text.find("~>dimension_", 0) != std::string::npos)
			infile >> dimension_;
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}
const std::vector<real_t> SAUserDefine::Problem(const std::vector<real_t>& coord) const
{
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 0.0);
	return results;
}
const std::vector<real_t> SAUserDefine::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results(num_points, 0.0);
	return results;
}