
#include "../INC/Euler2DEquation.h"

void Euler2DInitializer::convertPri2Con(std::vector<real_t>& primitive, const int_t start_points, const int_t end_points) const
{
	for (int_t ipoint = start_points; ipoint < end_points; ipoint++){
		primitive[ipoint * 4 + 1] *= primitive[ipoint * 4];
		primitive[ipoint * 4 + 2] *= primitive[ipoint * 4];
		primitive[ipoint * 4 + 3] = 2.5*primitive[ipoint * 4 + 3] + 0.5*(primitive[ipoint * 4 + 1] * primitive[ipoint * 4 + 1] + primitive[ipoint * 4 + 2] * primitive[ipoint * 4 + 2]) / primitive[ipoint * 4];
	}
}

Euler2DEquation::Euler2DEquation()
{
	num_states_ = 4;
	variable_names_.push_back("rho");
	variable_names_.push_back("rho*U");
	variable_names_.push_back("rho*V");
	variable_names_.push_back("rho*E");
	variable_names_.push_back("pressure");

	setName("Euler2D");
	Equation::getInstance().addRegistry(getName(), this);
}
void Euler2DEquation::setBoundaries(const std::vector<int_t>& tag_numbers)
{
	int_t num_bdries = tag_numbers.size();
	boundaries_.resize(num_bdries);
	for (int_t ibdry = 0; ibdry < num_bdries; ibdry++){
		const std::string& type = BoundaryCondition::getInstance().getBCType(tag_numbers[ibdry]);
		const std::string& name = BoundaryCondition::getInstance().getBCName(tag_numbers[ibdry]);
		const std::vector<real_t>& references = BoundaryCondition::getInstance().getBCReferences(tag_numbers[ibdry]);

		boundaries_[ibdry] = BoundaryFactory<Euler2DBoundary>::getInstance().getBoundary(type, tag_numbers[ibdry], name, references);
		boundaries_[ibdry]->setFlux(flux_);
	}
}
const std::vector<real_t> Euler2DEquation::calPostVariables(const std::vector<real_t>& solutions) const
{
	const int_t num_points = solutions.size() / num_states_;
	const std::vector<real_t> pressure = flux_->calPressure(solutions);
	std::vector<real_t> post_solutions;
	post_solutions.reserve(num_points*(num_states_ + 1));
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		for (int_t istate = 0; istate < num_states_; istate++)
			post_solutions.push_back(solutions[ipoint*num_states_ + istate]);
		post_solutions.push_back(pressure[ipoint]);
	}
	return post_solutions;
}
bool Euler2DEquation::applyPressureFix(const std::vector<real_t>& u)
{
	const int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = flux_->calPressure(u);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	if ((pressure[ipoint] < 0.0) || (u[ipoint*num_states_] < 0.0)) // check pressure & density
		return true;
	return false;
}
bool Euler2DEquation::isContact(const std::vector<real_t>& u, const std::vector<real_t>& neighbor_u) const
{
	const int_t num_cells = neighbor_u.size() / num_states_;
	const real_t pressure = flux_->calPressure(u)[0];
	const real_t eps = 1.0E-2;
	const std::vector<real_t> neighbor_pressure = flux_->calPressure(neighbor_u);
	for (int_t icell = 0; icell<num_cells;icell++)
	if (std::abs(pressure - neighbor_pressure[icell]) > eps)
		return false;
	return true;
}

Euler2DSodTest::Euler2DSodTest() // time = 0.2
{
	setName("SodTest");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DSodTest::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	const real_t split = 0.3;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		if (coord[ipoint*dimension_] <= split){
			results.push_back(1.0);
			results.push_back(0.75);
			results.push_back(0.0);
			results.push_back(2.78125);
		}
		else{
			results.push_back(0.125);
			results.push_back(0.0);
			results.push_back(0.0);
			results.push_back(0.25);
		}
	}
	return results;
}
const std::vector<real_t> Euler2DSodTest::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	const real_t split = 0.3;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		if (coord[ipoint*dimension_] <= split) {
			results.push_back(1.0);
			results.push_back(0.75);
			results.push_back(0.0);
			results.push_back(2.78125);
		}
		else {
			results.push_back(0.125);
			results.push_back(0.0);
			results.push_back(0.0);
			results.push_back(0.25);
		}
	}
	return results;
}

Euler2DShuOsher::Euler2DShuOsher() // time = 0.178
{
	setName("ShuOsher");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DShuOsher::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	const real_t split = 0.125;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		if (coord[ipoint*dimension_] <= split){
			results.push_back(3.857143);
			results.push_back(10.14185223);
			results.push_back(0.0);
			results.push_back(39.1666666666667);
		}
		else{
			results.push_back(1.0 + 0.2*std::sin(16.0*3.14159265358979323846264338327950288*coord[ipoint*dimension_]));
			results.push_back(0.0);
			results.push_back(0.0);
			results.push_back(2.5);
		}
	}
	return results;
}
const std::vector<real_t> Euler2DShuOsher::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	const real_t split = 0.125;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		if (coord[ipoint*dimension_] <= split) {
			results.push_back(3.857143);
			results.push_back(10.14185223);
			results.push_back(0.0);
			results.push_back(39.1666666666667);
		}
		else {
			results.push_back(1.0 + 0.2*std::sin(16.0*3.14159265358979323846264338327950288*coord[ipoint*dimension_]));
			results.push_back(0.0);
			results.push_back(0.0);
			results.push_back(2.5);
		}
	}
	return results;
}


Euler2DExplosion::Euler2DExplosion() // time = 0.25
{
	setName("Explosion");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DExplosion::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	const real_t split = 0.4;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t r2 = coord[ipoint*dimension_] * coord[ipoint*dimension_] + coord[ipoint*dimension_ + 1] * coord[ipoint*dimension_ + 1];
		if (r2 <= split*split){
			results.push_back(1.0);
			results.push_back(0.0);
			results.push_back(0.0);
			results.push_back(2.5);
		}
		else{
			results.push_back(0.125);
			results.push_back(0.0);
			results.push_back(0.0);
			results.push_back(0.25);
		}
	}
	return results;
}
const std::vector<real_t> Euler2DExplosion::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	const real_t split = 0.4;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t r2 = coord[ipoint*dimension_] * coord[ipoint*dimension_] + coord[ipoint*dimension_ + 1] * coord[ipoint*dimension_ + 1];
		if (r2 <= split * split) {
			results.push_back(1.0);
			results.push_back(0.0);
			results.push_back(0.0);
			results.push_back(2.5);
		}
		else {
			results.push_back(0.125);
			results.push_back(0.0);
			results.push_back(0.0);
			results.push_back(0.25);
		}
	}
	return results;
}

Euler2DShockVortex::Euler2DShockVortex() // time = 0.7
{
	setName("ShockVortex");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
	Ms_ = 1.5; Mv_ = 0.9;
	a_ = 0.075; a2_ = a_*a_;
	b_ = 0.175; b2_ = b_*b_;
	xv_ = 0.25; yv_ = 0.5;
	split_ = 0.5;

	vm_ = Mv_*std::sqrt(1.4);
	rho1_ = 1.0; u1_ = Ms_*std::sqrt(1.4); p1_ = 1.0;
	rho2_ = rho1_*(2.4*Ms_*Ms_) / (2.0 + 0.4*Ms_*Ms_);
	u2_ = u1_*rho1_ / rho2_;
	p2_ = p1_*(7.0*Ms_*Ms_ - 1.0) / 6.0;
}
const std::vector<real_t> Euler2DShockVortex::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.resize(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		if (coord[ipoint*dimension_] > split_){
			results[ipoint * 4 + 0] = rho2_;
			results[ipoint * 4 + 1] = u2_;
			results[ipoint * 4 + 2] = 0.0;
			results[ipoint * 4 + 3] = p2_;
		}
		else{
			results[ipoint * 4 + 0] = rho1_;
			results[ipoint * 4 + 1] = u1_;
			results[ipoint * 4 + 2] = 0.0;
			results[ipoint * 4 + 3] = p1_;
		}
		real_t rad = (coord[ipoint*dimension_] - xv_)*(coord[ipoint*dimension_] - xv_) + (coord[ipoint*dimension_ + 1] - yv_)*(coord[ipoint*dimension_ + 1] - yv_);
		if (rad < 1E-10){
			const real_t Tr = 1.0 + rho1_ / p1_ / 7.0*(std::pow(((vm_*a_) / (a2_ - b2_)), 2) * (a2_ - b2_*b2_ / a2_ - 4.0 * b2_ * std::log(a_) + 4.0 * b2_ * std::log(b_)) + (vm_*vm_ / a2_) * (rad*rad - a2_));
			const real_t pr = p1_*std::pow(Tr, 3.5);
			const real_t rhor = rho1_*std::pow(Tr, 2.5);
			const real_t vtheta = vm_*rad / a_;

			results[ipoint * 4 + 3] = pr;
		}
		else if (rad <= a2_){
			rad = std::sqrt(rad);
			const real_t Tr = 1.0 + rho1_ / p1_ / 7.0*(std::pow(((vm_*a_) / (a2_ - b2_)), 2) * (a2_ - b2_*b2_ / a2_ - 4.0 * b2_ * std::log(a_) + 4.0 * b2_ * std::log(b_)) + (vm_*vm_ / a2_) * (rad*rad - a2_));
			const real_t pr = p1_*std::pow(Tr, 3.5);
			const real_t rhor = rho1_*std::pow(Tr, 2.5);
			const real_t vtheta = vm_*rad / a_;
			const real_t temp = std::abs(coord[ipoint*dimension_ + 1] - yv_) / std::abs(coord[ipoint*dimension_] - xv_);
			real_t ur = vtheta*std::sin(std::atan(temp));
			real_t vr = vtheta*std::cos(std::atan(temp));
			if ((coord[ipoint*dimension_] >= xv_) && (coord[ipoint*dimension_+1] >= yv_)){
				ur = -ur;
			}
			else if ((coord[ipoint*dimension_] <= xv_) && (coord[ipoint*dimension_+1] >= yv_)){
				ur = -ur; vr = -vr;
			}
			else if ((coord[ipoint*dimension_] <= xv_) && (coord[ipoint*dimension_+1] <= yv_)){
				vr = -vr;
			}

			results[ipoint * 4 + 0] = rhor;
			results[ipoint * 4 + 1] = u1_+ur;
			results[ipoint * 4 + 2] = vr;
			results[ipoint * 4 + 3] = pr;
		}
		else if (rad <= b2_){
			rad = std::sqrt(rad);
			const real_t Tr = 1.0 + std::pow((vm_*a_) / (a2_ - b2_), 2) * rho1_ / p1_ / 7.0*(rad*rad - b2_*b2_ / rad / rad - 4.0 * b2_ * std::log(rad) + 4.0 * b2_ * std::log(b_));
			const real_t pr = p1_*std::pow(Tr, 3.5);
			const real_t rhor = rho1_*std::pow(Tr, 2.5);
			const real_t vtheta = vm_*a_ / (a2_ - b2_)*(rad - b2_ / rad);
			const real_t temp = std::abs(coord[ipoint*dimension_ + 1] - yv_) / std::abs(coord[ipoint*dimension_] - xv_);
			real_t ur = vtheta*std::sin(std::atan(temp));
			real_t vr = vtheta*std::cos(std::atan(temp));
			if ((coord[ipoint*dimension_] >= xv_) && (coord[ipoint*dimension_+1] >= yv_)){
				ur = -ur;
			}
			else if ((coord[ipoint*dimension_] <= xv_) && (coord[ipoint*dimension_+1] >= yv_)){
				ur = -ur; vr = -vr;
			}
			else if ((coord[ipoint*dimension_] <= xv_) && (coord[ipoint*dimension_+1] <= yv_)){
				vr = -vr;
			}

			results[ipoint * 4 + 0] = rhor;
			results[ipoint * 4 + 1] = u1_ + ur;
			results[ipoint * 4 + 2] = vr;
			results[ipoint * 4 + 3] = pr;
		}
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
const std::vector<real_t> Euler2DShockVortex::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.resize(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		if (coord[ipoint*dimension_] > split_) {
			results[ipoint * 4 + 0] = rho2_;
			results[ipoint * 4 + 1] = u2_;
			results[ipoint * 4 + 2] = 0.0;
			results[ipoint * 4 + 3] = p2_;
		}
		else {
			results[ipoint * 4 + 0] = rho1_;
			results[ipoint * 4 + 1] = u1_;
			results[ipoint * 4 + 2] = 0.0;
			results[ipoint * 4 + 3] = p1_;
		}
		real_t rad = (coord[ipoint*dimension_] - xv_)*(coord[ipoint*dimension_] - xv_) + (coord[ipoint*dimension_ + 1] - yv_)*(coord[ipoint*dimension_ + 1] - yv_);
		if (rad < 1E-10) {
			const real_t Tr = 1.0 + rho1_ / p1_ / 7.0*(std::pow(((vm_*a_) / (a2_ - b2_)), 2) * (a2_ - b2_ * b2_ / a2_ - 4.0 * b2_ * std::log(a_) + 4.0 * b2_ * std::log(b_)) + (vm_*vm_ / a2_) * (rad*rad - a2_));
			const real_t pr = p1_ * std::pow(Tr, 3.5);
			const real_t rhor = rho1_ * std::pow(Tr, 2.5);
			const real_t vtheta = vm_ * rad / a_;

			results[ipoint * 4 + 3] = pr;
		}
		else if (rad <= a2_) {
			rad = std::sqrt(rad);
			const real_t Tr = 1.0 + rho1_ / p1_ / 7.0*(std::pow(((vm_*a_) / (a2_ - b2_)), 2) * (a2_ - b2_ * b2_ / a2_ - 4.0 * b2_ * std::log(a_) + 4.0 * b2_ * std::log(b_)) + (vm_*vm_ / a2_) * (rad*rad - a2_));
			const real_t pr = p1_ * std::pow(Tr, 3.5);
			const real_t rhor = rho1_ * std::pow(Tr, 2.5);
			const real_t vtheta = vm_ * rad / a_;
			const real_t temp = std::abs(coord[ipoint*dimension_ + 1] - yv_) / std::abs(coord[ipoint*dimension_] - xv_);
			real_t ur = vtheta * std::sin(std::atan(temp));
			real_t vr = vtheta * std::cos(std::atan(temp));
			if ((coord[ipoint*dimension_] >= xv_) && (coord[ipoint*dimension_ + 1] >= yv_)) {
				ur = -ur;
			}
			else if ((coord[ipoint*dimension_] <= xv_) && (coord[ipoint*dimension_ + 1] >= yv_)) {
				ur = -ur; vr = -vr;
			}
			else if ((coord[ipoint*dimension_] <= xv_) && (coord[ipoint*dimension_ + 1] <= yv_)) {
				vr = -vr;
			}

			results[ipoint * 4 + 0] = rhor;
			results[ipoint * 4 + 1] = u1_ + ur;
			results[ipoint * 4 + 2] = vr;
			results[ipoint * 4 + 3] = pr;
		}
		else if (rad <= b2_) {
			rad = std::sqrt(rad);
			const real_t Tr = 1.0 + std::pow((vm_*a_) / (a2_ - b2_), 2) * rho1_ / p1_ / 7.0*(rad*rad - b2_ * b2_ / rad / rad - 4.0 * b2_ * std::log(rad) + 4.0 * b2_ * std::log(b_));
			const real_t pr = p1_ * std::pow(Tr, 3.5);
			const real_t rhor = rho1_ * std::pow(Tr, 2.5);
			const real_t vtheta = vm_ * a_ / (a2_ - b2_)*(rad - b2_ / rad);
			const real_t temp = std::abs(coord[ipoint*dimension_ + 1] - yv_) / std::abs(coord[ipoint*dimension_] - xv_);
			real_t ur = vtheta * std::sin(std::atan(temp));
			real_t vr = vtheta * std::cos(std::atan(temp));
			if ((coord[ipoint*dimension_] >= xv_) && (coord[ipoint*dimension_ + 1] >= yv_)) {
				ur = -ur;
			}
			else if ((coord[ipoint*dimension_] <= xv_) && (coord[ipoint*dimension_ + 1] >= yv_)) {
				ur = -ur; vr = -vr;
			}
			else if ((coord[ipoint*dimension_] <= xv_) && (coord[ipoint*dimension_ + 1] <= yv_)) {
				vr = -vr;
			}

			results[ipoint * 4 + 0] = rhor;
			results[ipoint * 4 + 1] = u1_ + ur;
			results[ipoint * 4 + 2] = vr;
			results[ipoint * 4 + 3] = pr;
		}
	}
	convertPri2Con(results, 0, num_points);
	return results;
}

Euler2DRiemann::Euler2DRiemann() // time = 0.8
{
	setName("Riemann");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DRiemann::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	const real_t xsplit = 0.8;
	const real_t ysplit = 0.8;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		if ((coord[ipoint*dimension_] > xsplit) && (coord[ipoint*dimension_ + 1] > ysplit)){ // 1 quad
			results.push_back(1.5);
			results.push_back(0.0);
			results.push_back(0.0);
			results.push_back(1.5);
		}
		else if ((coord[ipoint*dimension_] > xsplit) && (coord[ipoint*dimension_ + 1] < ysplit)){ // 4 quad
			results.push_back(0.532258064516129);
			results.push_back(0.0);
			results.push_back(1.206045378311055);
			results.push_back(0.3);
		}
		else if ((coord[ipoint*dimension_] < xsplit) && (coord[ipoint*dimension_ + 1] > ysplit)){ // 2 quad
			results.push_back(0.532258064516129);
			results.push_back(1.206045378311055);
			results.push_back(0.0);
			results.push_back(0.3);
		}
		else if ((coord[ipoint*dimension_] < xsplit) && (coord[ipoint*dimension_ + 1] < ysplit)){ // 3 quad
			results.push_back(0.137992831541219);
			results.push_back(1.206045378311055);
			results.push_back(1.206045378311055);
			results.push_back(0.029032258064516);
		}
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
const std::vector<real_t> Euler2DRiemann::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	const real_t xsplit = 0.8;
	const real_t ysplit = 0.8;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		if ((coord[ipoint*dimension_] > xsplit) && (coord[ipoint*dimension_ + 1] > ysplit)) { // 1 quad
			results.push_back(1.5);
			results.push_back(0.0);
			results.push_back(0.0);
			results.push_back(1.5);
		}
		else if ((coord[ipoint*dimension_] > xsplit) && (coord[ipoint*dimension_ + 1] < ysplit)) { // 4 quad
			results.push_back(0.532258064516129);
			results.push_back(0.0);
			results.push_back(1.206045378311055);
			results.push_back(0.3);
		}
		else if ((coord[ipoint*dimension_] < xsplit) && (coord[ipoint*dimension_ + 1] > ysplit)) { // 2 quad
			results.push_back(0.532258064516129);
			results.push_back(1.206045378311055);
			results.push_back(0.0);
			results.push_back(0.3);
		}
		else if ((coord[ipoint*dimension_] < xsplit) && (coord[ipoint*dimension_ + 1] < ysplit)) { // 3 quad
			results.push_back(0.137992831541219);
			results.push_back(1.206045378311055);
			results.push_back(1.206045378311055);
			results.push_back(0.029032258064516);
		}
	}
	convertPri2Con(results, 0, num_points);
	return results;
}


Euler2DFreeStream::Euler2DFreeStream() // time = convergence
{
	readData("EulerEquation", INPUT() + "equation.dat");
	setName("FreeStream");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
void Euler2DFreeStream::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	while (getline(infile, text)){
		if (text.find("~>Ma_", 0) != std::string::npos)
			infile >> Ma_;
		if (text.find("~>AoA_", 0) != std::string::npos)
			infile >> AoA_;
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}
const std::vector<real_t> Euler2DFreeStream::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		results.push_back(1.0);
		results.push_back(Ma_*std::cos(AoA_ * 3.14159265358979323846264338 / 180.0));
		results.push_back(Ma_*std::sin(AoA_ * 3.14159265358979323846264338 / 180.0));
		results.push_back(1.0/1.4);
	}
	convertPri2Con(results,0,num_points);
	return results;
}
const std::vector<real_t> Euler2DFreeStream::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		results.push_back(1.0);
		results.push_back(Ma_*std::cos(AoA_ * 3.14159265358979323846264338 / 180.0));
		results.push_back(Ma_*std::sin(AoA_ * 3.14159265358979323846264338 / 180.0));
		results.push_back(1.0 / 1.4);
	}
	convertPri2Con(results, 0, num_points);
	return results;
}


Euler2DUserShockTube::Euler2DUserShockTube() // time = user
{
	readData("EulerEquation", INPUT() + "equation.dat");
	setName("UserShockTube");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
void Euler2DUserShockTube::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	while (getline(infile, text)){
		if (text.find("~>UserShockTube_", 0) != std::string::npos){
			infile >> split_;
			getline(infile, text);
			UL_.resize(4);
			infile >> UL_[0] >> UL_[1] >> UL_[2] >> UL_[3];
			getline(infile, text);
			UR_.resize(4);
			infile >> UR_[0] >> UR_[1] >> UR_[2] >> UR_[3];
		}
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}
const std::vector<real_t> Euler2DUserShockTube::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		if (coord[ipoint*dimension_] <= split_){
			for (int_t istate = 0; istate < 4;istate++)
				results.push_back(UL_[istate]);
		}
		else{
			for (int_t istate = 0; istate < 4; istate++)
				results.push_back(UR_[istate]);
		}
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
const std::vector<real_t> Euler2DUserShockTube::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		if (coord[ipoint*dimension_] <= split_) {
			for (int_t istate = 0; istate < 4; istate++)
				results.push_back(UL_[istate]);
		}
		else {
			for (int_t istate = 0; istate < 4; istate++)
				results.push_back(UR_[istate]);
		}
	}
	convertPri2Con(results, 0, num_points);
	return results;
}

Euler2DDoubleMach::Euler2DDoubleMach() // time = 0.2
{
	setName("DoubleMach");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DDoubleMach::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	const real_t rho_u = 1.4;
	const real_t u_u = 0.0;
	const real_t p_u = 1.0;
	const real_t Ms = 10.0;

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		if (coord[ipoint*dimension_] <= 0) {
			const real_t rho_d = rho_u*(2.4*Ms*Ms) / (2.0 + 0.4*Ms*Ms);
			results.push_back(rho_d);
			results.push_back(Ms*std::sqrt(1.4*p_u / rho_u)*(1.0 - rho_u / rho_d));
			results.push_back(0.0);
			results.push_back(p_u*(1.0 + 7.0 / 6.0*(Ms*Ms - 1.0)));
		}
		else {
			results.push_back(rho_u);
			results.push_back(u_u);
			results.push_back(0.0);
			results.push_back(p_u);
		}
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
const std::vector<real_t> Euler2DDoubleMach::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	const real_t rho_u = 1.4;
	const real_t u_u = 0.0;
	const real_t p_u = 1.0;
	const real_t Ms = 10.0;

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		if (coord[ipoint*dimension_] <= 0) {
			const real_t rho_d = rho_u * (2.4*Ms*Ms) / (2.0 + 0.4*Ms*Ms);
			results.push_back(rho_d);
			results.push_back(Ms*std::sqrt(1.4*p_u / rho_u)*(1.0 - rho_u / rho_d));
			results.push_back(0.0);
			results.push_back(p_u*(1.0 + 7.0 / 6.0*(Ms*Ms - 1.0)));
		}
		else {
			results.push_back(rho_u);
			results.push_back(u_u);
			results.push_back(0.0);
			results.push_back(p_u);
		}
	}
	convertPri2Con(results, 0, num_points);
	return results;
}

Euler2DShockWedge::Euler2DShockWedge() // time = 3.25
{
	setName("ShockWedge");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DShockWedge::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	const real_t rho_u = 1.4;
	const real_t u_u = 0.0;
	const real_t p_u = 1.0;
	const real_t Ms = 1.34;

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		if (coord[ipoint*dimension_] <= 0) {
			const real_t rho_d = rho_u*(2.4*Ms*Ms) / (2.0 + 0.4*Ms*Ms);
			results.push_back(rho_d);
			results.push_back(Ms*std::sqrt(1.4*p_u / rho_u)*(1.0 - rho_u / rho_d));
			results.push_back(0.0);
			results.push_back(p_u*(1.0 + 7.0 / 6.0*(Ms*Ms - 1.0)));
		}
		else {
			results.push_back(rho_u);
			results.push_back(u_u);
			results.push_back(0.0);
			results.push_back(p_u);
		}
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
const std::vector<real_t> Euler2DShockWedge::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	const real_t rho_u = 1.4;
	const real_t u_u = 0.0;
	const real_t p_u = 1.0;
	const real_t Ms = 1.34;

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		if (coord[ipoint*dimension_] <= 0) {
			const real_t rho_d = rho_u * (2.4*Ms*Ms) / (2.0 + 0.4*Ms*Ms);
			results.push_back(rho_d);
			results.push_back(Ms*std::sqrt(1.4*p_u / rho_u)*(1.0 - rho_u / rho_d));
			results.push_back(0.0);
			results.push_back(p_u*(1.0 + 7.0 / 6.0*(Ms*Ms - 1.0)));
		}
		else {
			results.push_back(rho_u);
			results.push_back(u_u);
			results.push_back(0.0);
			results.push_back(p_u);
		}
	}
	convertPri2Con(results, 0, num_points);
	return results;
}

Euler2DVortexTransport::Euler2DVortexTransport() // time = user (1T = 1/Ma)
{
	setName("VortexTransport");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DVortexTransport::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	//const real_t Ma = 0.05; // slow vortex
	//const real_t beta = 0.02; // slow vortex
	//const real_t R = 0.05; // slow vortex

	const real_t Ma = 0.5; // fast vortex
	const real_t beta = 0.2; // fast vortex
	const real_t R = 0.05; // fast vortex

	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		const real_t& x = coord[ipoint*dimension_];
		const real_t& y = coord[ipoint*dimension_ + 1];
		const real_t r2 = ((x - 0.5)*(x - 0.5) + (y - 0.5)*(y - 0.5)) / R / R;
		const real_t u = Ma*(1.0 - beta*(y - 0.5) / R*std::exp(-0.5*r2));
		const real_t v = Ma*beta*(x - 0.5) / R*std::exp(-0.5*r2);
		const real_t T = 1.0 - 0.2*Ma*Ma*beta*beta*std::exp(-r2);
		const real_t rho = std::pow(T, 2.5);
		const real_t P = rho*T / 1.4;
		results.push_back(rho);
		results.push_back(u);
		results.push_back(v);
		results.push_back(P);
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
const std::vector<real_t> Euler2DVortexTransport::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	//const real_t Ma = 0.05; // slow vortex
	//const real_t beta = 0.02; // slow vortex
	//const real_t R = 0.05; // slow vortex

	const real_t Ma = 0.5; // fast vortex
	const real_t beta = 0.2; // fast vortex
	const real_t R = 0.05; // fast vortex

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& x = coord[ipoint*dimension_];
		const real_t& y = coord[ipoint*dimension_ + 1];
		const real_t r2 = ((x - 0.5)*(x - 0.5) + (y - 0.5)*(y - 0.5)) / R / R;
		const real_t u = Ma * (1.0 - beta * (y - 0.5) / R * std::exp(-0.5*r2));
		const real_t v = Ma * beta*(x - 0.5) / R * std::exp(-0.5*r2);
		const real_t T = 1.0 - 0.2*Ma*Ma*beta*beta*std::exp(-r2);
		const real_t rho = std::pow(T, 2.5);
		const real_t P = rho * T / 1.4;
		results.push_back(rho);
		results.push_back(u);
		results.push_back(v);
		results.push_back(P);
	}
	convertPri2Con(results, 0, num_points);
	return results;
}

Euler2DDoubleSine::Euler2DDoubleSine() // time = user
{
	setName("DoubleSine");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DDoubleSine::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	const real_t pi = 4.0*std::atan(1.0);
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& x = coord[ipoint*dimension_];
		const real_t& y = coord[ipoint*dimension_ + 1];
		results.push_back(1.0 + 0.2*std::sin(2 * pi*x)*std::sin(2 * pi*y));
		results.push_back(1.0);
		results.push_back(0.5);
		results.push_back(1.0);
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
const std::vector<real_t> Euler2DDoubleSine::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	const int_t num_points = coord.size() / dimension_;
	const real_t pi = 4.0*std::atan(1.0);
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& x = coord[ipoint*dimension_];
		const real_t& y = coord[ipoint*dimension_ + 1];
		results.push_back(1.0 + 0.2*std::sin(2 * pi*(x - 1.0*time))*std::sin(2 * pi*(y - 0.5*time)));
		results.push_back(1.0);
		results.push_back(0.5);
		results.push_back(1.0);
	}
	convertPri2Con(results, 0, num_points);
	return results;
}

Euler2DTravellingWaves::Euler2DTravellingWaves() // time = user
{
	setName("TravellingWaves");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DTravellingWaves::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	const real_t pi = 4.0*std::atan(1.0);
	const real_t nu = 1.0e-2;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& x = coord[ipoint*dimension_];
		const real_t& y = coord[ipoint*dimension_ + 1];
		results.push_back(1.0);
		results.push_back(1.0 + 2.0*std::cos(2.0*pi*x)*std::sin(2.0*pi*y));
		results.push_back(1.0 - 2.0*std::sin(2.0*pi*x)*std::cos(2.0*pi*y));
		results.push_back(- (std::cos(4.0*pi*x) + std::cos(4.0*pi*y)));
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
const std::vector<real_t> Euler2DTravellingWaves::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	const int_t num_points = coord.size() / dimension_;
	const real_t pi = 4.0*std::atan(1.0);
	const real_t nu = 1.0e-2;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& x = coord[ipoint*dimension_];
		const real_t& y = coord[ipoint*dimension_ + 1];
		results.push_back(1.0);
		results.push_back(1.0 + 2.0*std::cos(2.0*pi*(x - time))*std::sin(2.0*pi*(y - time))*std::exp(-8.0*pi*pi*nu*time));
		results.push_back(1.0 - 2.0*std::sin(2.0*pi*(x - time))*std::cos(2.0*pi*(y - time))*std::exp(-8.0*pi*pi*nu*time));
		results.push_back(- (std::cos(4.0*pi*(x - time)) + std::cos(4.0*pi*(y - time)))*std::exp(-16.0*pi*pi*nu*time));
	}
	convertPri2Con(results, 0, num_points);
	return results;
}

Euler2DUserDefine::Euler2DUserDefine() // time = user
{
	setName("UserDefine");
	Euler2DInitializer::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> Euler2DUserDefine::Problem(const std::vector<real_t>& coord) const
{
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	//const real_t Ma = 0.05; // slow vortex
	//const real_t beta = 0.02; // slow vortex
	//const real_t R = 0.05; // slow vortex

	const real_t Ma = 0.5; // fast vortex
	const real_t beta = 0.2; // fast vortex
	const real_t R = 0.05; // fast vortex

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& x = coord[ipoint*dimension_];
		const real_t& y = coord[ipoint*dimension_ + 1];
		const real_t r2 = ((x - 0.5)*(x - 0.5) + (y - 0.5)*(y - 0.5)) / R / R;
		const real_t u = Ma*(1.0 - beta*(y - 0.5) / R*std::exp(-0.5*r2));
		const real_t v = Ma*beta*(x - 0.5) / R*std::exp(-0.5*r2);
		const real_t T = 1.0 - 0.2*Ma*Ma*beta*beta*std::exp(-r2);
		const real_t rho = std::pow(T, 2.5);
		const real_t P = rho*T / 1.4;
		results.push_back(rho);
		results.push_back(u);
		results.push_back(v);
		results.push_back(P);
	}
	convertPri2Con(results, 0, num_points);
	return results;
}
const std::vector<real_t> Euler2DUserDefine::ExactVal(const std::vector<real_t>& coord, const real_t time) const
{
	MASTER_ERROR("Cannot use exact value");
	const int_t num_points = coord.size() / dimension_;
	std::vector<real_t> results;
	results.reserve(num_points * 4);

	//const real_t Ma = 0.05; // slow vortex
	//const real_t beta = 0.02; // slow vortex
	//const real_t R = 0.05; // slow vortex

	const real_t Ma = 0.5; // fast vortex
	const real_t beta = 0.2; // fast vortex
	const real_t R = 0.05; // fast vortex

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& x = coord[ipoint*dimension_];
		const real_t& y = coord[ipoint*dimension_ + 1];
		const real_t r2 = ((x - 0.5)*(x - 0.5) + (y - 0.5)*(y - 0.5)) / R / R;
		const real_t u = Ma * (1.0 - beta * (y - 0.5) / R * std::exp(-0.5*r2));
		const real_t v = Ma * beta*(x - 0.5) / R * std::exp(-0.5*r2);
		const real_t T = 1.0 - 0.2*Ma*Ma*beta*beta*std::exp(-r2);
		const real_t rho = std::pow(T, 2.5);
		const real_t P = rho * T / 1.4;
		results.push_back(rho);
		results.push_back(u);
		results.push_back(v);
		results.push_back(P);
	}
	convertPri2Con(results, 0, num_points);
	return results;
}

