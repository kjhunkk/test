#include "../INC/TimeUnstRosenbrock.h"

TimeUnstRosenbrock::TimeUnstRosenbrock()
{
	is_implicit_ = true;
}

TimeUnstRosenbrock::~TimeUnstRosenbrock()
{
	ierr_ = VecDestroy(&delta_);
	ierr_ = VecDestroy(&pseudo_solution_vector_);
	ierr_ = VecDestroy(&rhs_vector_);
	for(int_t i = 0; i < num_stages_; i++)
		ierr_ = VecDestroy(&stage_solution_[i]);
}

void TimeUnstRosenbrock::initialize()
{
	const Config& config = Config::getInstance();
	if (config.getIsFixedTimeStep().compare("yes") == 0)
		is_fixed_dt_ = true;
	else is_fixed_dt_ = false;
	linear_solver_iteration_ = 0;
	is_initialized_ = false;

	// Rosenbrock coefficients initialize including order and stage
	InitializeCoeff();

	// Vectors
	ierr_ = VecCreate(MPI_COMM_WORLD, &delta_);
	ierr_ = VecCreate(MPI_COMM_WORLD, &pseudo_solution_vector_);
	ierr_ = VecCreate(MPI_COMM_WORLD, &rhs_vector_);
	stage_solution_.resize(num_stages_);
	for (int_t istage = 0; istage < num_stages_; istage++)
		ierr_ = VecCreate(MPI_COMM_WORLD, &stage_solution_[istage]);
	
	// Linear system solver initialize
	linear_system_solver_ = LinearSystemSolver::getInstance().getType(config.getLinearSystemSolver());
	linear_system_solver_->Initialize();
}

void TimeUnstRosenbrock::integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
	const int_t num_cells = zone->getNumCells();
	const std::vector<int_t> cell_petsc_index = zone->getCellPetscIndex();

	if (is_initialized_ == false)
	{
		const Vec& vec_initializer = zone->getSystemRHS();
		ierr_ = VecDuplicate(vec_initializer, &delta_);
		ierr_ = VecDuplicate(vec_initializer, &pseudo_solution_vector_);
		ierr_ = VecDuplicate(vec_initializer, &rhs_vector_);
		for (int_t istage = 0; istage < num_stages_; istage++)
			ierr_ = VecDuplicate(vec_initializer, &stage_solution_[istage]);
		is_initialized_ = true;
	}

	// system matrix
	const real_t diagonal_factor = 1.0 / (dt*coeff_gamma_);
	Mat system_matrix = zone->calculateSystemMatrix(diagonal_factor, 1.0);
	
	// Stage solve
	for (int_t istage = 0; istage < num_stages_; istage++)
	{
		// Load n step solution
		pseudo_solution_ = zone->getSolution();

		for (int_t jstage = 0; jstage < istage; jstage++)
			AddVectorToSolution(coeff_a_[istage][jstage], stage_solution_[jstage], num_cells, cell_petsc_index, pseudo_solution_);
		zone->RequestCommunication(pseudo_solution_);
		
		VecCopy(zone->calculateInstantSystemRHS(pseudo_solution_), rhs_vector_);
		for (int_t jstage = 0; jstage < istage; jstage++)
			VecAXPY(rhs_vector_, coeff_c_[istage][jstage] / dt, stage_solution_[jstage]);
		ierr_ = linear_system_solver_->Solve(system_matrix, rhs_vector_, stage_solution_[istage]);
		linear_solver_iteration_ = linear_system_solver_->getIterationNumber();
	}

	// Summation
	VecCopy(stage_solution_[0], delta_);
	VecScale(delta_, coeff_m_[0]);
	for (int_t istage = 1; istage < num_stages_; istage++)
		VecAXPY(delta_, coeff_m_[istage], stage_solution_[istage]);

	// Calculating delta norm
	real_t delta_norm = 0;
	VecNorm(delta_, NORM_2, &delta_norm);
	status_string_ = "KrylovIteration=" + TO_STRING(linear_solver_iteration_) + " L2=" + TO_STRING((double)delta_norm);

	// Solution update
	zone->UpdateSolutionFromDelta(delta_);
	zone->communication();
	zone->limiting();
	zone->pressurefix(current_time, iteration);
}

void TimeUnstRosenbrock::ConvertSolutionArray(Vec& vector, const int_t num_cells, const std::vector<std::vector<real_t> >& solution, const std::vector<int_t>& cell_petsc_index)
{
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	PetscScalar *vector_array;
	const PetscInt num_var = solution[0].size();
	VecGetArray(vector, &vector_array);
	for (int_t icell = 0; icell < num_cells; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			vector_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar] = solution[icell][ivar];
	VecRestoreArray(vector, &vector_array);
}

void TimeUnstRosenbrock::AddVectorToSolution(const real_t scale_scalar, const Vec& vector, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, std::vector<std::vector<real_t> >& solution)
{
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	const PetscScalar *vector_array;
	const PetscInt num_var = solution[0].size();
	VecGetArrayRead(vector, &vector_array);
	for (int_t icell = 0; icell < num_cells; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			solution[icell][ivar] += scale_scalar * vector_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar];
	VecRestoreArrayRead(vector, &vector_array);
}

// Lang-Verwer ROS3P (RO3-3)
RosenbrockROS3P::RosenbrockROS3P()
{
	setName("ROS3P");
	TimeScheme::getInstance().addRegistry(getName(), this);
}

RosenbrockROS3P::~RosenbrockROS3P()
{

}

void RosenbrockROS3P::InitializeCoeff()
{
	// time order and the number of stages
	time_order_ = 3; num_stages_ = 3;

	coeff_a_.resize(num_stages_);
	coeff_c_.resize(num_stages_);

	// coefficients
	coeff_gamma_ = 0.5 + std::sqrt(3.0) / 6.0;
	coeff_a_[1] = { 1.0 / coeff_gamma_ };
	coeff_a_[2] = { 1.0 / coeff_gamma_, 0.0 };
	coeff_c_[1] = { -1.0 / pow(coeff_gamma_, 2.0) };
	coeff_c_[2] = { -(1.0 + (2.0 - 0.5 / coeff_gamma_) / coeff_gamma_) / coeff_gamma_, -(2.0 - 0.5 / coeff_gamma_) / coeff_gamma_ };
	coeff_m_ = { (1.0 + (2.0 / 3.0 - 1.0 / (6.0*coeff_gamma_)) / coeff_gamma_) / coeff_gamma_, (2.0 / 3.0 - 1.0 / (6.0*coeff_gamma_)) / coeff_gamma_, 1.0 / (3.0*coeff_gamma_) };
}

// Hairer-Wanner RODAS3 (RO3-4)
RosenbrockRODAS3::RosenbrockRODAS3()
{
	setName("RODAS3");
	TimeScheme::getInstance().addRegistry(getName(), this);
}

RosenbrockRODAS3::~RosenbrockRODAS3()
{

}

void RosenbrockRODAS3::InitializeCoeff()
{
	// time order and the number of stages
	time_order_ = 3; num_stages_ = 4;

	coeff_a_.resize(num_stages_);
	coeff_c_.resize(num_stages_);

	// coefficients
	coeff_gamma_ = 0.5;
	coeff_a_[1] = { 0.0 };
	coeff_a_[2] = { 2.0, 0.0 };
	coeff_a_[3] = { 2.0, 0.0, 1.0 };
	coeff_c_[1] = { 4.0 };
	coeff_c_[2] = { 1.0, -1.0 };
	coeff_c_[3] = { 1.0, -1.0, -8.0 / 3.0 };
	coeff_m_ = { 2.0, 0.0, 1.0, 1.0 };
}

// Shampine ROS4 (RO4-4)
RosenbrockROS4::RosenbrockROS4()
{
	setName("ROS4");
	TimeScheme::getInstance().addRegistry(getName(), this);
}

RosenbrockROS4::~RosenbrockROS4()
{

}

void RosenbrockROS4::InitializeCoeff()
{
	// time order and the number of stages
	time_order_ = 4; num_stages_ = 4;

	coeff_a_.resize(num_stages_);
	coeff_c_.resize(num_stages_);

	// coefficients
	coeff_gamma_ = 0.5;
	coeff_a_[1] = { 2.0 };
	coeff_a_[2] = { 48.0 / 25.0, 6.0 / 25.0 };
	coeff_a_[3] = { 48.0 / 25.0, 6.0 / 25.0, 0.0 };
	coeff_c_[1] = { -8.0 };
	coeff_c_[2] = { 372.0 / 25.0, 12.0 / 5.0 };
	coeff_c_[3] = { -112.0 / 125.0, -54.0 / 125.0, -2.0 / 5.0 };
	coeff_m_ = { 19.0 / 9.0, 0.5, 25.0 / 108.0, 125.0 / 108.0 };
}

// Steinebach RODASP (RO4-6)
RosenbrockRODASP::RosenbrockRODASP()
{
	setName("RODASP");
	TimeScheme::getInstance().addRegistry(getName(), this);
}

RosenbrockRODASP::~RosenbrockRODASP()
{

}

void RosenbrockRODASP::InitializeCoeff()
{
	// time order and the number of stages
	time_order_ = 4; num_stages_ = 6;
	
	coeff_a_.resize(num_stages_);
	coeff_c_.resize(num_stages_);

	// coefficients
	coeff_gamma_ = 0.25;
	coeff_a_[1] = { 3.0 };
	coeff_a_[2] = { 1.831036793486759, 0.4955183967433795 };
	coeff_a_[3] = { 2.304376582692669, -0.05249275245743001, -1.176798761832782 };
	coeff_a_[4] = { -7.170454962423024, -4.741636671481785, -16.31002631330971, -1.062004044111401 };
	coeff_a_[5] = { -7.170454962423024, -4.741636671481785, -16.31002631330971, -1.062004044111401, 1.0 };
	coeff_c_[1] = { -12.0 };
	coeff_c_[2] = { -8.791795173947035, -2.207865586973518 };
	coeff_c_[3] = { 10.81793056857153, 6.780270611428266, 19.53485944642410 };
	coeff_c_[4] = { 34.19095006749676, 15.49671153725963, 54.74760875964130, 14.16005392148534 };
	coeff_c_[5] = { 34.62605830930532, 15.30084976114473, 56.99955578662667, 18.40807009793095, -5.714285714285717 };
	coeff_m_ = { -7.170454962423024, -4.741636671481785, -16.31002631330971, -1.062004044111401, 1.0, 1.0 };
}

// Di Marzo RODASS-Rod5_1 (RO5-8)
RosenbrockRODASS::RosenbrockRODASS()
{
	setName("RODASS");
	TimeScheme::getInstance().addRegistry(getName(), this);
}

RosenbrockRODASS::~RosenbrockRODASS()
{

}

void RosenbrockRODASS::InitializeCoeff()
{
	// time order and the number of stages
	time_order_ = 5; num_stages_ = 8;

	coeff_a_.resize(num_stages_);
	coeff_c_.resize(num_stages_);

	// coefficients
	coeff_gamma_ = 0.19;
	coeff_a_[1] = { 2.0 };
	coeff_a_[2] = { 3.040894194418781, 1.041747909077569 };
	coeff_a_[3] = { 2.576417536461461, 1.622083060776640, -0.9089668560264532 };
	coeff_a_[4] = { 2.760842080225597 , 1.446624659844071 ,-0.3036980084553738, 0.2877498600325443 };
	coeff_a_[5] = { -14.09640773051259, 6.925207756232704, -41.47510893210728, 2.343771018586405, 24.13215229196062 };
	coeff_a_[6] = { -14.09640773051259, 6.925207756232704, -41.47510893210728, 2.343771018586405, 24.13215229196062, 1.0 };
	coeff_a_[7] = { -14.09640773051259, 6.925207756232704, -41.47510893210728, 2.343771018586405, 24.13215229196062, 1.0, 1.0 };
	coeff_c_[1] = { -10.31323885133993 };
	coeff_c_[2] = { -21.04823117650003, -7.234992135176716 };
	coeff_c_[3] = { 32.22751541853323, -4.943732386540191, 19.44922031041879 };
	coeff_c_[4] = { -20.69865579590063, -8.816374604402768, 1.260436877740897, -0.7495647613787146 };
	coeff_c_[5] = { -46.22004352711257, -17.49534862857472, -289.6389582892057, 93.60855400400906, 318.3822534212147 };
	coeff_c_[6] = { 34.20013733472935, -14.15535402717690, 57.82335640988400, 25.83362985412365, 1.408950972071624, -6.551835421242162 };
	coeff_c_[7] = { 42.57076742291101, -13.80770672017997, 93.98938432427124, 18.77919633714503, -31.58359187223370, -6.685968952921985, -5.810979938412932 };
	coeff_m_ = { -14.09640773051259, 6.925207756232704, -41.47510893210728, 2.343771018586405, 24.13215229196062, 1.0, 1.0, 1.0 };
}

// Kaps and Wanner ROW6A (RO6-6)
RosenbrockROW6A::RosenbrockROW6A()
{
	setName("ROW6A");
	TimeScheme::getInstance().addRegistry(getName(), this);
}

RosenbrockROW6A::~RosenbrockROW6A()
{

}

void RosenbrockROW6A::InitializeCoeff()
{
	// time order and the number of stages
	time_order_ = 6; num_stages_ = 6;

	coeff_a_.resize(num_stages_);
	coeff_c_.resize(num_stages_);

	// coefficients
	coeff_gamma_ = 0.3341423670680504;
	coeff_a_[1] = { 2.0 };
	coeff_a_[2] = { 1.751493065942685, -0.1454290536332865 };
	coeff_a_[3] = { -1.847093912231436, -2.513756792158473, 1.874707432337999 };
	coeff_a_[4] = { 10.59634783677141, 1.974951525952609, -1.905211286263863, -3.575118228830491 };
	coeff_a_[5] = { 2.417642067883312, 0.3050984437044573, -0.2346208879122501, -0.1327038464607418 , 0.03912922779645768 };
	coeff_c_[1] = { -17.45029492512995 };
	coeff_c_[2] = { -12.02359936227844, 1.315910110742745 };
	coeff_c_[3] = { 23.11230597159272, 12.97893129565445, -8.445374594562038 };
	coeff_c_[4] = { -3.147228891330713, -1.761332622909965, 6.115295934038585, 14.99319950457112 };
	coeff_c_[5] = { -20.15840911262880, -1.603923799800133, 1.155870096920252, 0.6304639815292044, -0.1602510215637174 };
	coeff_m_ = { 33.99347452674165, -20.91829882847333, -13.75688477471081, -11.13925929930077, 2.873406527609468, 38.76609945620840 };
}