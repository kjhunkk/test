
#include "../INC/TurbSA2DFlux.h"

TurbSA2DFlux::TurbSA2DFlux()
{
	readData("EulerEquation", INPUT() + "equation.dat");
	
	eps_ = 1.0E-6;
	sigma_ = 0.666666666666666666666666;
	k_ = 0.41;
	Prt_ = 0.9;
	rlim_ = 5.0;
	b_ = 100.0;

	cb1_ = 0.1355;	cb2_ = 0.622;
	cv13_ = std::pow(7.1, 3.0);	cv2_ = 0.7;	cv3_ = 0.9;
	ct3_ = 1.2;	ct4_ = 0.5;
	cw1_ = cb1_ / k_ / k_ + (1.0 + cb2_) / sigma_;
	cw2_ = 0.3; cw36_ = std::pow(2.0, 6.0);
	cn1_ = 16.0;

	rpi_ = 1.0/(4.0*std::atan(1.0));
}

void TurbSA2DFlux::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)) {
		if (text.find(region_start, 0) != std::string::npos) {
			flag = true;
			break;
		}
	}
	if (flag == false) {
		infile.close(); return;
	}

	Re_ = 1.0;
	Ma_ = 1.0;
	Pr_ = 1.0;
	Tref_ = 1.0;

	while (getline(infile, text)) {
		if (text.find("~>viscous_on_", 0) != std::string::npos) {
			getline(infile, text);
			if (!text.compare("on")) viscous_on_ = true;
			else viscous_on_ = false;
		}
		if (text.find("~>Reynolds_", 0) != std::string::npos)
			infile >> Re_;
		if (text.find("~>Ma_", 0) != std::string::npos)
			infile >> Ma_;
		if (text.find("~>Prandtl_", 0) != std::string::npos)
			infile >> Pr_;
		if (text.find("~>Tref_", 0) != std::string::npos)
			infile >> Tref_;
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}

	Sutherland_coef_ = 110.55 / Tref_;
	alpha_ = Ma_ / Re_;
	beta_ = 1.4 / Pr_*alpha_;
	Pr_ratio_ = Pr_ / Prt_;
	max_for_visradii_ = std::max(4.0 / 3.0*alpha_, beta_);
	infile.close();
}

const std::vector<real_t> TurbSA2DFlux::calViscosity(const std::vector<real_t>& temperature) const
{
	int_t num_points = temperature.size();
	std::vector<real_t> viscosity(num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		viscosity[ipoint] = (1.0 + Sutherland_coef_) / (temperature[ipoint] + Sutherland_coef_)*std::pow(temperature[ipoint], 1.5);
	return viscosity;
}

const std::vector<real_t> TurbSA2DFlux::calPressure(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
	std::vector<real_t> pressure(num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
			pressure[ipoint] = (u[ipoint*num_states_ + 3] - 0.5*(u[ipoint*num_states_ + 1] * u[ipoint*num_states_ + 1] + u[ipoint*num_states_ + 2] * u[ipoint*num_states_ + 2]) / u[ipoint*num_states_])*0.4;
	return pressure;
}

const std::vector<real_t> TurbSA2DFlux::calPressureCoefficients(const std::vector<real_t>& u) const
{
	const int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> Cp(num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		Cp[ipoint] = 2.0*(pressure[ipoint] - 1.0 / 1.4) / Ma_ / Ma_;
	return Cp;
}

const std::vector<real_t> TurbSA2DFlux::calTemperature(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
	std::vector<real_t> temperature(num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
			temperature[ipoint] = (u[ipoint*num_states_ + 3] - 0.5*(u[ipoint*num_states_ + 1] * u[ipoint*num_states_ + 1] + u[ipoint*num_states_ + 2] * u[ipoint*num_states_ + 2]) / u[ipoint*num_states_])*0.56 / u[ipoint*num_states_];
	return temperature;
}

const std::vector<real_t> TurbSA2DFlux::commonConvFlux(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> Flux(num_points*num_states_*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& d = u[ipoint*num_states_];
		const real_t& du = u[ipoint*num_states_ + 1];
		const real_t& dv = u[ipoint*num_states_ + 2];
		const real_t& dE = u[ipoint*num_states_ + 3];
		const real_t& dnu = u[ipoint*num_states_ + 4];
		const real_t& p = pressure[ipoint];

		Flux[ipoint*num_states_*dimension_] = du;
		Flux[ipoint*num_states_*dimension_ + 1] = dv;

		Flux[ipoint*num_states_*dimension_ + 2] = du*du / d + p;
		Flux[ipoint*num_states_*dimension_ + 3] = du*dv / d;

		Flux[ipoint*num_states_*dimension_ + 4] = Flux[ipoint*num_states_*dimension_ + 3];
		Flux[ipoint*num_states_*dimension_ + 5] = dv*dv / d + p;

		Flux[ipoint*num_states_*dimension_ + 6] = du / d*(dE + p);
		Flux[ipoint*num_states_*dimension_ + 7] = dv / d*(dE + p);

		Flux[ipoint*num_states_*dimension_ + 8] = dnu * du / d;
		Flux[ipoint*num_states_*dimension_ + 9] = dnu * dv / d;
	}
	return Flux;
}

const std::vector<real_t> TurbSA2DFlux::commonConvFluxJacobian(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_*dimension_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DFlux::commonConvFlux(const std::vector<real_t>& u, const std::vector<real_t>& normal) const
{
	int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> Flux(num_points*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& d = u[ipoint*num_states_];
		const real_t& du = u[ipoint*num_states_ + 1];
		const real_t& dv = u[ipoint*num_states_ + 2];
		const real_t& dE = u[ipoint*num_states_ + 3];
		const real_t& dnu = u[ipoint*num_states_ + 4];
		const real_t& p = pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V = (du*nx + dv*ny) / d;

		Flux[ipoint*num_states_] = d*V;
		Flux[ipoint*num_states_ + 1] = du*V + nx*p;
		Flux[ipoint*num_states_ + 2] = dv*V + ny*p;
		Flux[ipoint*num_states_ + 3] = V*(dE + p);
		Flux[ipoint*num_states_ + 4] = dnu*V;
	}
	return Flux;
}

const std::vector<real_t> TurbSA2DFlux::commonConvFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& normal) const
{
	int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DFlux::commonViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& div_u) const
{
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = calTemperature(owner_u);
	const std::vector<real_t> viscosity = calViscosity(temperature);
#endif
	std::vector<real_t> Flux(num_points*num_states_*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &div_u[ipoint*num_states_*dimension_];

		const real_t ux = (t[2] - u[1] / u[0] * t[0]) / u[0];
		const real_t uy = (t[3] - u[1] / u[0] * t[1]) / u[0];
		const real_t vx = (t[4] - u[2] / u[0] * t[0]) / u[0];
		const real_t vy = (t[5] - u[2] / u[0] * t[1]) / u[0];
		const real_t ex = ((t[6] - t[0] * u[3] / u[0]) - u[1] * ux - u[2] * vx) / u[0];
		const real_t ey = ((t[7] - t[1] * u[3] / u[0]) - u[1] * uy - u[2] * vy) / u[0];
		const real_t nutx = (t[8] - u[4] / u[0] * t[0]) / u[0];
		const real_t nuty = (t[9] - u[4] / u[0] * t[1]) / u[0];
		const real_t txx = 2.0 / 3.0*(2.0*ux - vy);
		const real_t txy = uy + vx;
		const real_t tyy = 2.0 / 3.0 * (2.0*vy - ux);
		const real_t chi = u[4] / viscosity[ipoint];
		const real_t chi3 = chi*chi*chi;
		real_t turbulent_viscosity = 0;
		real_t fn = 1.0;
		if (u[4] >= 0.0) {
			const real_t fv1 = chi3 / (chi3 + cv13_);
			turbulent_viscosity = fv1*u[4];
		}
		else {
			fn = (cn1_ + chi3) / (cn1_ - chi3);
		}
		const real_t alpha = alpha_*(viscosity[ipoint] + turbulent_viscosity);
		const real_t beta = beta_*(viscosity[ipoint] + Pr_ratio_*turbulent_viscosity);

		Flux[ipoint*num_states_*dimension_] = 0.0;
		Flux[ipoint*num_states_*dimension_ + 1] = 0.0;

		Flux[ipoint*num_states_*dimension_ + 2] = alpha*txx;
		Flux[ipoint*num_states_*dimension_ + 3] = alpha*txy;

		Flux[ipoint*num_states_*dimension_ + 4] = alpha*txy;
		Flux[ipoint*num_states_*dimension_ + 5] = alpha*tyy;

		Flux[ipoint*num_states_*dimension_ + 6] = alpha*(txx*u[1] + txy*u[2]) / u[0] + beta*ex;
		Flux[ipoint*num_states_*dimension_ + 7] = alpha*(txy*u[1] + tyy*u[2]) / u[0] + beta*ey;

		Flux[ipoint*num_states_*dimension_ + 8] = (viscosity[ipoint] + fn*u[4])*nutx *alpha_ / sigma_;
		Flux[ipoint*num_states_*dimension_ + 9] = (viscosity[ipoint] + fn*u[4])*nuty *alpha_ / sigma_;
	}
	return Flux;
}

const std::vector<real_t> TurbSA2DFlux::commonViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(owner_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_*dimension_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DFlux::commonViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(owner_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_*dimension_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DFlux::commonViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = calTemperature(owner_u);
	const std::vector<real_t> viscosity = calViscosity(temperature);
#endif
	std::vector<real_t> Flux(num_points*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &owner_u[ipoint*num_states_];
		const real_t* t = &div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t ux = (t[2] - u[1] / u[0] * t[0]) / u[0];
		const real_t uy = (t[3] - u[1] / u[0] * t[1]) / u[0];
		const real_t vx = (t[4] - u[2] / u[0] * t[0]) / u[0];
		const real_t vy = (t[5] - u[2] / u[0] * t[1]) / u[0];
		const real_t ex = ((t[6] - t[0] * u[3] / u[0]) - u[1] * ux - u[2] * vx) / u[0];
		const real_t ey = ((t[7] - t[1] * u[3] / u[0]) - u[1] * uy - u[2] * vy) / u[0];
		const real_t nutx = (t[8] - u[4] / u[0] * t[0]) / u[0];
		const real_t nuty = (t[9] - u[4] / u[0] * t[1]) / u[0];
		const real_t txx = 2.0 / 3.0*(2.0*ux - vy);
		const real_t txy = uy + vx;
		const real_t tyy = 2.0 / 3.0 * (2.0*vy - ux);
		const real_t chi = u[4] / viscosity[ipoint];
		const real_t chi3 = chi*chi*chi;
		real_t turbulent_viscosity = 0;
		real_t fn = 1.0;
		if (u[4] >= 0.0) {
			const real_t fv1 = chi3 / (chi3 + cv13_);
			turbulent_viscosity = fv1*u[4];
		}
		else {
			fn = (cn1_ + chi3) / (cn1_ - chi3);
		}
		const real_t alpha = alpha_*(viscosity[ipoint] + turbulent_viscosity);
		const real_t beta = beta_*(viscosity[ipoint] + Pr_ratio_*turbulent_viscosity);

		Flux[ipoint*num_states_] = 0.0;
		Flux[ipoint*num_states_ + 1] = alpha*(txx*n[0] + txy*n[1]);
		Flux[ipoint*num_states_ + 2] = alpha*(txy*n[0] + tyy*n[1]);
		Flux[ipoint*num_states_ + 3] = alpha*((txx*u[1] + txy*u[2])*n[0] + (txy*u[1] + tyy*u[2])*n[1]) / u[0] + beta*(ex*n[0] + ey*n[1]);
		Flux[ipoint*num_states_ + 4] = (viscosity[ipoint] + fn*u[4])*(nutx*n[0] + nuty*n[1]) *alpha_ / sigma_;
	}
	return Flux;
}

const std::vector<real_t> TurbSA2DFlux::commonViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(owner_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DFlux::commonViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(owner_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DFlux::numViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> owner_viscosity(num_points, 1.0);
	const std::vector<real_t> neighbor_viscosity(num_points, 1.0);
#else
	const std::vector<real_t> owner_temperature = calTemperature(owner_u);
	const std::vector<real_t> owner_viscosity = calViscosity(owner_temperature);
	const std::vector<real_t> neighbor_temperature = calTemperature(neighbor_u);
	const std::vector<real_t> neighbor_viscosity = calViscosity(neighbor_temperature);
#endif
	std::vector<real_t> Flux(num_points*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u1 = &owner_u[ipoint*num_states_];
		const real_t* t1 = &owner_div_u[ipoint*num_states_*dimension_];
		const real_t* n = &normal[ipoint*dimension_];

		const real_t ux1 = (t1[2] - u1[1] / u1[0] * t1[0]) / u1[0];
		const real_t uy1 = (t1[3] - u1[1] / u1[0] * t1[1]) / u1[0];
		const real_t vx1 = (t1[4] - u1[2] / u1[0] * t1[0]) / u1[0];
		const real_t vy1 = (t1[5] - u1[2] / u1[0] * t1[1]) / u1[0];
		const real_t ex1 = ((t1[6] - t1[0] * u1[3] / u1[0]) - u1[1] * ux1 - u1[2] * vx1) / u1[0];
		const real_t ey1 = ((t1[7] - t1[1] * u1[3] / u1[0]) - u1[1] * uy1 - u1[2] * vy1) / u1[0];
		const real_t nutx1 = (t1[8] - u1[4] / u1[0] * t1[0]) / u1[0];
		const real_t nuty1 = (t1[9] - u1[4] / u1[0] * t1[1]) / u1[0];
		const real_t txx1 = 2.0 / 3.0 * (2.0*ux1 - vy1);
		const real_t txy1 = uy1 + vx1;
		const real_t tyy1 = 2.0 / 3.0 * (2.0*vy1 - ux1);
		const real_t chi1 = u1[4] / owner_viscosity[ipoint];
		const real_t owner_chi3 = chi1*chi1*chi1;
		real_t owner_turbulent_viscosity = 0;
		real_t fn1 = 1.0;
		if (u1[4] >= 0.0) {
			const real_t fv1 = owner_chi3 / (owner_chi3 + cv13_);
			owner_turbulent_viscosity = fv1*u1[4];
		}
		else {
			fn1 = (cn1_ + owner_chi3) / (cn1_ - owner_chi3);
		}
		const real_t alpha1 = alpha_*(owner_viscosity[ipoint] + owner_turbulent_viscosity);
		const real_t beta1 = beta_*(owner_viscosity[ipoint] + Pr_ratio_*owner_turbulent_viscosity);

		Flux[ipoint*num_states_] = 0.0;
		Flux[ipoint*num_states_ + 1] = 0.5* alpha1*(txx1*n[0] + txy1*n[1]);
		Flux[ipoint*num_states_ + 2] = 0.5* alpha1*(txy1*n[0] + tyy1*n[1]);
		Flux[ipoint*num_states_ + 3] = 0.5* alpha1*((txx1*u1[1] + txy1*u1[2])*n[0] + (txy1*u1[1] + tyy1*u1[2])*n[1]) / u1[0] + 0.5* beta1*(ex1*n[0] + ey1*n[1]);
		Flux[ipoint*num_states_ + 4] = 0.5* (owner_viscosity[ipoint] + fn1*u1[4])*(nutx1*n[0] + nuty1*n[1]) *alpha_ / sigma_;

		const real_t* u2 = &neighbor_u[ipoint*num_states_];
		const real_t* t2 = &neighbor_div_u[ipoint*num_states_*dimension_];

		const real_t ux2 = (t2[2] - u2[1] / u2[0] * t2[0]) / u2[0];
		const real_t uy2 = (t2[3] - u2[1] / u2[0] * t2[1]) / u2[0];
		const real_t vx2 = (t2[4] - u2[2] / u2[0] * t2[0]) / u2[0];
		const real_t vy2 = (t2[5] - u2[2] / u2[0] * t2[1]) / u2[0];
		const real_t ex2 = ((t2[6] - t2[0] * u2[3] / u2[0]) - u2[1] * ux2 - u2[2] * vx2) / u2[0];
		const real_t ey2 = ((t2[7] - t2[1] * u2[3] / u2[0]) - u2[1] * uy2 - u2[2] * vy2) / u2[0];
		const real_t nutx2 = (t2[8] - u2[4] / u2[0] * t2[0]) / u2[0];
		const real_t nuty2 = (t2[9] - u2[4] / u2[0] * t2[1]) / u2[0];
		const real_t txx2 = 2.0 / 3.0 * (2.0*ux2 - vy2);
		const real_t txy2 = uy2 + vx2;
		const real_t tyy2 = 2.0 / 3.0 * (2.0*vy2 - ux2);
		const real_t chi2 = u2[4] / neighbor_viscosity[ipoint];
		const real_t neighbor_chi3 = chi2*chi2*chi2;
		real_t neighbor_turbulent_viscosity = 0;
		real_t fn2 = 1.0;
		if (u2[4] >= 0.0) {
			const real_t fv1 = neighbor_chi3 / (neighbor_chi3 + cv13_);
			neighbor_turbulent_viscosity = fv1*u2[4];
		}
		else {
			fn2 = (cn1_ + neighbor_chi3) / (cn1_ - neighbor_chi3);
		}
		const real_t alpha2 = alpha_*(neighbor_viscosity[ipoint] + neighbor_turbulent_viscosity);
		const real_t beta2 = beta_*(neighbor_viscosity[ipoint] + Pr_ratio_*neighbor_turbulent_viscosity);

		//Flux[ipoint*num_states_] += 0.0;
		Flux[ipoint*num_states_ + 1] += 0.5* alpha2*(txx2*n[0] + txy2*n[1]);
		Flux[ipoint*num_states_ + 2] += 0.5* alpha2*(txy2*n[0] + tyy2*n[1]);
		Flux[ipoint*num_states_ + 3] += 0.5* alpha2*((txx2*u2[1] + txy2*u2[2])*n[0] + (txy2*u2[1] + tyy2*u2[2])*n[1]) / u2[0] + 0.5* beta2*(ex2*n[0] + ey2*n[1]);
		Flux[ipoint*num_states_ + 4] += 0.5* (neighbor_viscosity[ipoint] + fn2*u2[4])*(nutx2*n[0] + nuty2*n[1]) *alpha_ / sigma_;
	}
	return Flux;
}

const std::vector<real_t> TurbSA2DFlux::numViscFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(owner_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DFlux::numViscFluxOwnerGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(owner_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DFlux::numViscFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(owner_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DFlux::numViscFluxNeighborGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(owner_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DFlux::convWaveVelocity(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
	const std::vector<real_t> pressure = calPressure(u);
	std::vector<real_t> wave(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& d = u[ipoint*num_states_];
		const real_t& du = u[ipoint*num_states_ + 1];
		const real_t& dv = u[ipoint*num_states_ + 2];
		const real_t& p = pressure[ipoint];
		const real_t a = std::sqrt(1.4*p / d);

		wave[ipoint*dimension_] = std::abs(du / d) + a;
		wave[ipoint*dimension_ + 1] = std::abs(dv / d) + a;
	}
	return wave;
}

const std::vector<real_t> TurbSA2DFlux::viscWaveVelocity(const std::vector<real_t>& u) const
{
	int_t num_points = u.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = calTemperature(u);
	const std::vector<real_t> viscosity = calViscosity(temperature);
#endif
	std::vector<real_t> wave(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		real_t turbulent_viscosity = 0.0;
		real_t fn = 1.0;
		const real_t chi = u[ipoint*num_states_ + 4] / viscosity[ipoint];
		const real_t chi3 = chi*chi*chi;
		if (u[ipoint*num_states_ + 4] >= 0.0) {
			const real_t fv1 = chi3 / (chi3 + cv13_);
			turbulent_viscosity = fv1*u[ipoint*num_states_ + 4];
		}
		else {
			fn = (cn1_ + chi3) / (cn1_ - chi3);
		}
		const real_t max_visradii = std::max(1 / sigma_*(viscosity[ipoint] + fn*u[ipoint*num_states_ + 4]), 1.4*(viscosity[ipoint] / Pr_ + turbulent_viscosity / Prt_))*alpha_ / u[ipoint*num_states_];
		for (int_t idim = 0; idim < dimension_; idim++)
			wave[ipoint*dimension_ + idim] = max_visradii;
	}
	return wave;
}

const std::vector<real_t> TurbSA2DFlux::source(const std::vector<real_t>& ui, const std::vector<real_t>& div_u, const std::vector<real_t>& assist) const
{
	const int_t num_points = ui.size() / num_states_;
#ifdef _CONSTANT_VISCOSITY_
	const std::vector<real_t> viscosity(num_points, 1.0);
#else
	const std::vector<real_t> temperature = calTemperature(ui);
	const std::vector<real_t> viscosity = calViscosity(temperature);
#endif
	std::vector<real_t> source(num_points*num_states_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t* u = &ui[ipoint*num_states_];
		const real_t* t = &div_u[ipoint*num_states_*dimension_];
		const real_t& wd = assist[ipoint];

		const real_t uy = (t[3] - u[1] / u[0] * t[1]) / u[0];
		const real_t vx = (t[4] - u[2] / u[0] * t[0]) / u[0];
		const real_t nutx = (t[8] - u[4] / u[0] * t[0]) / u[0];
		const real_t nuty = (t[9] - u[4] / u[0] * t[1]) / u[0];
		const real_t chi = u[4] / viscosity[ipoint];
		const real_t chi3 = chi*chi*chi;
		real_t fn = 1.0;

		const real_t nut = u[4] / u[0];
		real_t S = std::abs(vx - uy);
		if (S < eps_)
			S = 0.5*(eps_ + S*S / eps_); // check eps_ value!

		real_t production, destruction, source1, source2;

		source1 = cb2_ / sigma_*u[0] * (nutx*nutx + nuty*nuty);

		if (u[4] >= 0.0) {
			const real_t fv1 = chi3 / (chi3 + cv13_);
			const real_t fv2 = 1.0 - chi / (1.0 + chi*fv1);

			const real_t Sb = nut / k_ / k_ / wd / wd*fv2 / Re_;
			real_t St = S;
			if (Sb >= -cv2_*S) {
				St += Sb;
			}
			else {
				St += (S*(cv2_*cv2_*S + cv3_*Sb) / ((cv3_ - 2.0*cv2_)*S - Sb));
			}

			const real_t ft2 = ct3_*std::exp(-ct4_*chi*chi);
			production = cb1_*(1.0 - ft2)*St*u[4];

			const real_t rb = nut / St / k_ / k_ / wd / wd / Re_;
			const real_t r = rlim_ - (rlim_ - rb)*(rpi_*std::atan(b_*(rlim_ - rb)) + 0.5) + rpi_*std::atan(b_) - 0.5;
			const real_t g = r + cw2_*(std::pow(r, 6) - r);
			const real_t fw = g*std::pow((1.0 + cw36_) / (std::pow(g, 6) + cw36_), 0.16666666666666666666666666);
			destruction = u[0] * (cw1_*fw - cb1_ / k_ / k_*ft2)*nut*nut / wd / wd;
		}
		else {
			production = cb1_*(1.0 - ct3_)*S*u[4];
			destruction = -u[0] * cw1_*nut*nut / wd / wd;
			fn = (cn1_ + chi3) / (cn1_ - chi3);
		}

		source2 = (viscosity[ipoint] + fn*u[4]) / u[0] / sigma_*(nutx*t[0] + nuty*t[1]);

		source[ipoint*num_states_ + 4] = production + (-destruction + source1 - source2)*alpha_;
	}
	return source;
}



TurbSA2DLLF::TurbSA2DLLF()
{
	setName("LLF");
	TurbSA2DFlux::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> TurbSA2DLLF::numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> Flux(num_points*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& d1 = owner_u[ipoint*num_states_];
		const real_t& du1 = owner_u[ipoint*num_states_ + 1];
		const real_t& dv1 = owner_u[ipoint*num_states_ + 2];
		const real_t& dE1 = owner_u[ipoint*num_states_ + 3];
		const real_t& dnut1 = owner_u[ipoint*num_states_ + 4];
		const real_t& p1 = owner_pressure[ipoint];
		const real_t& d2 = neighbor_u[ipoint*num_states_];
		const real_t& du2 = neighbor_u[ipoint*num_states_ + 1];
		const real_t& dv2 = neighbor_u[ipoint*num_states_ + 2];
		const real_t& dE2 = neighbor_u[ipoint*num_states_ + 3];
		const real_t& dnut2 = neighbor_u[ipoint*num_states_ + 4];
		const real_t& p2 = neighbor_pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V1 = (du1*nx + dv1*ny) / d1;
		const real_t V2 = (du2*nx + dv2*ny) / d2;

		std::vector<real_t> flux1(num_states_);
		flux1[0] = d1*V1;
		flux1[1] = du1*V1 + nx*p1;
		flux1[2] = dv1*V1 + ny*p1;
		flux1[3] = V1*(dE1 + p1);
		flux1[4] = dnut1*V1;

		std::vector<real_t> flux2(num_states_);
		flux2[0] = d2*V2;
		flux2[1] = du2*V2 + nx*p2;
		flux2[2] = dv2*V2 + ny*p2;
		flux2[3] = V2*(dE2 + p2);
		flux2[4] = dnut2*V2;

		const real_t a1 = std::sqrt(1.4*p1 / d1);
		const real_t a2 = std::sqrt(1.4*p2 / d2);
		const real_t r_max = std::max(std::abs(V1) + a1, std::abs(V2) + a2);

		for (int_t istate = 0; istate < num_states_; istate++)
			Flux[ipoint*num_states_ + istate] = 0.5*(flux1[istate] + flux2[istate] - r_max*(neighbor_u[ipoint*num_states_ + istate] - owner_u[ipoint*num_states_ + istate]));
	}

	return Flux;
}

const std::vector<real_t> TurbSA2DLLF::numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DLLF::numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	// jacobian

	return FluxJacobi;
}

TurbSA2DRoe::TurbSA2DRoe()
{
	setName("Roe");
	TurbSA2DFlux::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> TurbSA2DRoe::numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> Flux(num_points*num_states_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const real_t& d1 = owner_u[ipoint*num_states_];
		const real_t& du1 = owner_u[ipoint*num_states_ + 1];
		const real_t& dv1 = owner_u[ipoint*num_states_ + 2];
		const real_t& dE1 = owner_u[ipoint*num_states_ + 3];
		const real_t& dnu1 = owner_u[ipoint*num_states_ + 4];
		const real_t& p1 = owner_pressure[ipoint];
		const real_t& d2 = neighbor_u[ipoint*num_states_];
		const real_t& du2 = neighbor_u[ipoint*num_states_ + 1];
		const real_t& dv2 = neighbor_u[ipoint*num_states_ + 2];
		const real_t& dE2 = neighbor_u[ipoint*num_states_ + 3];
		const real_t& dnu2 = neighbor_u[ipoint*num_states_ + 4];
		const real_t& p2 = neighbor_pressure[ipoint];
		const real_t& nx = normal[ipoint*dimension_];
		const real_t& ny = normal[ipoint*dimension_ + 1];
		const real_t V1 = (du1*nx + dv1*ny) / d1;
		const real_t V2 = (du2*nx + dv2*ny) / d2;

		std::vector<real_t> flux1(num_states_);
		flux1[0] = d1*V1;
		flux1[1] = du1*V1 + nx*p1;
		flux1[2] = dv1*V1 + ny*p1;
		flux1[3] = V1*(dE1 + p1);
		flux1[4] = dnu1*V1;

		std::vector<real_t> flux2(num_states_);
		flux2[0] = d2*V2;
		flux2[1] = du2*V2 + nx*p2;
		flux2[2] = dv2*V2 + ny*p2;
		flux2[3] = V2*(dE2 + p2);
		flux2[4] = dnu2*V2;

		const real_t dsqrt1 = std::sqrt(d1);
		const real_t dsqrt2 = std::sqrt(d2);

		const real_t Rd = dsqrt1*dsqrt2;
		const real_t Ru = (du1 / dsqrt1 + du2 / dsqrt2) / (dsqrt1 + dsqrt2);
		const real_t Rv = (dv1 / dsqrt1 + dv2 / dsqrt2) / (dsqrt1 + dsqrt2);
		const real_t Rnu = (dnu1 / dsqrt1 + dnu2 / dsqrt2) / (dsqrt1 + dsqrt2);
		const real_t Rh = ((dE1 + p1) / dsqrt1 + (dE2 + p2) / dsqrt2) / (dsqrt1 + dsqrt2);
		const real_t Ra = std::sqrt(0.4*(Rh - 0.5*(Ru*Ru + Rv*Rv)));
		const real_t RV = (V1*dsqrt1 + V2*dsqrt2) / (dsqrt1 + dsqrt2);

		const real_t dd = d2 - d1;
		const real_t du = du2 / d2 - du1 / d1;
		const real_t dv = dv2 / d2 - dv1 / d1;
		const real_t dnu = dnu2 / d2 - dnu1 / d1;
		const real_t dp = p2 - p1;
		const real_t dV = V2 - V1;

		std::vector<real_t> flux_1(num_states_);
		std::vector<real_t> flux_2(num_states_);
		std::vector<real_t> flux_3(num_states_);
		std::vector<real_t> flux_4(num_states_);
		std::vector<real_t> flux_5(num_states_);

		flux_1[0] = std::abs(RV)*(dd - dp / (Ra*Ra));
		flux_1[1] = flux_1[0] * Ru;
		flux_1[2] = flux_1[0] * Rv;
		flux_1[3] = flux_1[0] * 0.5*(Ru*Ru + Rv*Rv);
		flux_1[4] = flux_1[0] * Rnu;

		real_t temp = std::abs(RV)*Rd*(du*ny - dv*nx);
		flux_2[0] = 0.0;
		flux_2[1] = temp*ny;
		flux_2[2] = -temp*nx;
		flux_2[3] = flux_2[1] * Ru + flux_2[2] * Rv;
		flux_2[4] = 0.0;

		temp = std::abs(RV + Ra)*(dp + Rd*Ra*dV)*0.5 / (Ra*Ra);
		flux_3[0] = temp;
		flux_3[1] = temp*(Ru + nx*Ra);
		flux_3[2] = temp*(Rv + ny*Ra);
		flux_3[3] = temp*(Rh + Ra*RV);
		flux_3[4] = temp*Rnu;

		temp = std::abs(RV - Ra)*(dp - Rd*Ra*dV)*0.5 / (Ra*Ra);
		flux_4[0] = temp;
		flux_4[1] = temp*(Ru - nx*Ra);
		flux_4[2] = temp*(Rv - ny*Ra);
		flux_4[3] = temp*(Rh - Ra*RV);
		flux_4[4] = temp*Rnu;

		flux_5[0] = 0.0;
		flux_5[1] = 0.0;
		flux_5[2] = 0.0;
		flux_5[3] = 0.0;
		flux_5[4] = std::abs(RV)*Rd*dnu;

		for (int_t istate = 0; istate < num_states_; istate++)
			Flux[ipoint*num_states_ + istate] = 0.5*(flux1[istate] + flux2[istate] - flux_1[istate] - flux_2[istate] - flux_3[istate] - flux_4[istate] - flux_5[istate]);
	}

	return Flux;
}

const std::vector<real_t> TurbSA2DRoe::numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	// jacobian

	return FluxJacobi;
}

const std::vector<real_t> TurbSA2DRoe::numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size() / num_states_;
	const std::vector<real_t> owner_pressure = calPressure(owner_u);
	const std::vector<real_t> neighbor_pressure = calPressure(neighbor_u);
	std::vector<real_t> FluxJacobi(num_points*num_states_*num_states_);

	// jacobian

	return FluxJacobi;
}