
#include "../INC/PostElement.h"


PostElementFactory::PostElementFactory() {
	registry_.resize(static_cast<size_t>(ElemType::NUM_OF_ELEMENT));
}

PostElementFactory::~PostElementFactory() {
	for (auto&& element : registry_){
		if (element != static_cast<PostElement*>(0)){
			delete element;
		}
	}
}

PostElementFactory& PostElementFactory::getInstance() { static PostElementFactory instance; return instance; }

PostElement* PostElementFactory::getPostElement(ElemType element_type) {
	if (element_type >= ElemType::NUM_OF_ELEMENT){
		return static_cast<PostElement*>(0);
	}
	if (registry_[static_cast<int_t>(element_type)] == static_cast<PostElement*>(0)) registry_[static_cast<int_t>(element_type)] = newPostElement(element_type);
	return registry_[static_cast<int_t>(element_type)];
}

PostElement* PostElementFactory::newPostElement(ElemType element_type) {
	switch (element_type) {
	case ElemType::LINE: return new LinePostElement();
	case ElemType::TRIS: return new TrisPostElement();
	case ElemType::QUAD: return new QuadPostElement();
		//case ElemType::HEXG: return new HexgElement();
	case ElemType::TETS: return new TetsPostElement();
	case ElemType::HEXA: return new HexaPostElement();
	case ElemType::PRIS: return new PrisPostElement();
		//case ElemType::PYRA: return new PyraElement();
	}
	return static_cast<PostElement*>(0);
}

const std::vector<real_t> PostElement::getPostCoords(const int_t element_type, const int_t element_order, const int_t post_order, const std::vector<real_t>& coords)
{
	std::shared_ptr<Jacobian> jacobian = JacobianFactory::getJacobian(static_cast<ElemType>(element_type), element_order);
	jacobian->setCoords(coords);
	jacobian->setTopology();

	std::vector<real_t> com_coords = getPostComCoords(post_order);
	return jacobian->calComToPhyCoord(com_coords);
}

const std::vector<real_t> PostElement::getFacePostCoords(const int_t face_element_type, const int_t face_element_order, const int_t post_order, const std::vector<real_t>& face_coords)
{
	std::shared_ptr<Jacobian> jacobian = JacobianFactory::getFaceJacobian(static_cast<ElemType>(face_element_type), face_element_order);
	jacobian->setCoords(face_coords);
	jacobian->setTopology();

	std::vector<real_t> com_coords = getPostComCoords(post_order);
	return jacobian->calComToPhyCoord(com_coords);
}

const std::vector<real_t> LinePostElement::getPostComCoords(const int_t post_order)
{
	std::vector<real_t> coords;
	coords.reserve(post_order + 1);

	for (int_t ix = 0; ix <= post_order; ix++)
		coords.push_back(real_t(2 * ix) / real_t(post_order) - 1.0);
	
	return coords;
}
const std::vector<int_t> LinePostElement::getPostConnects(const int_t post_order)
{
	std::vector<int_t> connects;
	connects.reserve(2 * post_order);

	for (int_t ix = 0; ix<post_order; ix++) {
		connects.push_back(ix);
		connects.push_back(ix + 1);
	}
	return connects;
}

const std::vector<real_t> TrisPostElement::getPostComCoords(const int_t post_order)
{
	std::vector<real_t> coords;
	coords.reserve((post_order + 1)*(post_order + 2));

	for (int_t iy = 0; iy <= post_order; iy++){
		for (int_t ix = 0; ix <= post_order - iy; ix++){
			coords.push_back(real_t(2 * ix) / real_t(post_order) - 1.0);
			coords.push_back(real_t(2 * iy) / real_t(post_order) - 1.0);
		}
	}

	return coords;
}
const std::vector<int_t> TrisPostElement::getPostConnects(const int_t post_order)
{
	std::vector<int_t> connects;
	connects.reserve(3 * post_order*post_order);

	for (int_t iy = 0; iy<post_order; iy++){
		for (int_t ix = 0; ix<post_order - iy; ix++){
			int_t var1 = (post_order + 2)*iy - iy*(iy + 1) / 2 + ix;
			int_t var2 = (post_order + 2)*(iy + 1) - (iy + 1)*(iy + 2) / 2 + ix;
			connects.push_back(var1);
			connects.push_back(var1 + 1);
			connects.push_back(var2);

			if (ix == post_order - iy - 1) continue;

			connects.push_back(var1 + 1);
			connects.push_back(var2 + 1);
			connects.push_back(var2);
		}
	}
	return connects;
}

const std::vector<real_t> QuadPostElement::getPostComCoords(const int_t post_order)
{
	std::vector<real_t> coords;
	coords.reserve(2 * (post_order + 1)*(post_order + 1));

	for (int_t iy = 0; iy <= post_order; iy++){
		for (int_t ix = 0; ix <= post_order; ix++){
			coords.push_back(real_t(2 * ix) / real_t(post_order) - 1.0);
			coords.push_back(real_t(2 * iy) / real_t(post_order) - 1.0);
		}
	}

	return coords;
}
const std::vector<int_t> QuadPostElement::getPostConnects(const int_t post_order)
{
	std::vector<int_t> connects;
	connects.reserve(6 * post_order*post_order);

	for (int_t iy = 0; iy<post_order; iy++){
		for (int_t ix = 0; ix<post_order; ix++){
			int_t var1 = (post_order + 1)*iy + ix;
			int_t var2 = (post_order + 1)*(iy + 1) + ix;
			connects.push_back(var1);
			connects.push_back(var1 + 1);
			connects.push_back(var2);

			connects.push_back(var1 + 1);
			connects.push_back(var2 + 1);
			connects.push_back(var2);
		}
	}
	return connects;
}

const std::vector<real_t> TetsPostElement::getPostComCoords(const int_t post_order)
{
	std::vector<real_t> coords;
	coords.reserve((post_order + 1)*(post_order + 2)*(post_order + 3) / 2);

	for (int_t iz = 0; iz <= post_order; iz++){
		for (int_t iy = 0; iy <= post_order - iz; iy++){
			for (int_t ix = 0; ix <= post_order - iz - iy; ix++){
				coords.push_back(real_t(2 * ix) / real_t(post_order) - 1.0);
				coords.push_back(real_t(2 * iy) / real_t(post_order) - 1.0);
				coords.push_back(real_t(2 * iz) / real_t(post_order) - 1.0);
			}
		}
	}

	return coords;
}
const std::vector<int_t> TetsPostElement::getPostConnects(const int_t post_order)
{
	std::vector<int_t> connects;
	connects.reserve(4 * post_order*post_order*post_order);

	for (int_t iz = 0; iz < post_order; iz++){
		for (int_t iy = 0; iy < post_order - iz; iy++){
			for (int_t ix = 0; ix < post_order - iz - iy; ix++){
				int_t sum = ix + iy + iz;
				if (sum == post_order - 1){
					int_t var1 = (post_order - iz + 2)*iy - iy*(iy + 1) / 2 + ix + iz*(3 * post_order*post_order - 3 * post_order*iz + 12 * post_order + iz*iz - 6 * iz + 11) / 6;
					int_t var2 = (post_order - iz + 2)*(iy + 1) - (iy + 1)*(iy + 2) / 2 + ix + iz*(3 * post_order*post_order - 3 * post_order*iz + 12 * post_order + iz*iz - 6 * iz + 11) / 6;
					int_t var3 = (post_order - iz + 1)*iy - iy*(iy + 1) / 2 + ix + (iz + 1)*(3 * post_order*post_order - 3 * post_order*iz + 9 * post_order + iz*iz - 4 * iz + 6) / 6;
					std::vector<int_t> type1 = {var1, var1 + 1, var2, var3};
					for (auto&& i : getType1Connects(type1))
						connects.push_back(i);

				}
				else if (sum == post_order - 2){
					int_t var1 = (post_order - iz + 2)*iy - iy*(iy + 1) / 2 + ix + iz*(3 * post_order*post_order - 3 * post_order*iz + 12 * post_order + iz*iz - 6 * iz + 11) / 6;
					int_t var2 = (post_order - iz + 2)*(iy + 1) - (iy + 1)*(iy + 2) / 2 + ix + iz*(3 * post_order*post_order - 3 * post_order*iz + 12 * post_order + iz*iz - 6 * iz + 11) / 6;
					int_t var3 = (post_order - iz + 1)*iy - iy*(iy + 1) / 2 + ix + (iz + 1)*(3 * post_order*post_order - 3 * post_order*iz + 9 * post_order + iz*iz - 4 * iz + 6) / 6;
					int_t var4 = (post_order - iz + 1)*(iy + 1) - (iy + 1)*(iy + 2) / 2 + ix + (iz + 1)*(3 * post_order*post_order - 3 * post_order*iz + 9 * post_order + iz*iz - 4 * iz + 6) / 6;
					std::vector<int_t> type2 = { var1, var1 + 1, var2 + 1, var2, var3, var3 + 1, var4 };
					for (auto&& i : getType2Connects(type2))
						connects.push_back(i);
				}
				else{
					int_t var1 = (post_order - iz + 2)*iy - iy*(iy + 1) / 2 + ix + iz*(3 * post_order*post_order - 3 * post_order*iz + 12 * post_order + iz*iz - 6 * iz + 11) / 6;
					int_t var2 = (post_order - iz + 2)*(iy + 1) - (iy + 1)*(iy + 2) / 2 + ix + iz*(3 * post_order*post_order - 3 * post_order*iz + 12 * post_order + iz*iz - 6 * iz + 11) / 6;
					int_t var3 = (post_order - iz + 1)*iy - iy*(iy + 1) / 2 + ix + (iz + 1)*(3 * post_order*post_order - 3 * post_order*iz + 9 * post_order + iz*iz - 4 * iz + 6) / 6;
					int_t var4 = (post_order - iz + 1)*(iy + 1) - (iy + 1)*(iy + 2) / 2 + ix + (iz + 1)*(3 * post_order*post_order - 3 * post_order*iz + 9 * post_order + iz*iz - 4 * iz + 6) / 6;
					std::vector<int_t> type3 = { var1, var1 + 1, var2 + 1, var2, var3, var3 + 1, var4 + 1, var4 };
					for (auto&& i : getType3Connects(type3))
						connects.push_back(i);
				}
			}
		}
	}
	return connects;
}
const std::vector<int_t> TetsPostElement::getType1Connects(const std::vector<int_t>& pts)
{
	//                   z
	//	                .
	//         	      ,/
	//	             /
	//	           3  
	//	         ,/|`\
	//	       ,/  |  `\
	//	     ,/    '.   `\    
	//	   ,/       |     `\
	//   ,/         |       `\
	//	0-----------'.--------2 --> y
	//	 `\.        |        ,/
	//	    `\.     |      ,/ 
	//	       `\.   '.  ,/   
	//	          `\. | / 
	//	             `1    
	//	               `\.
	//	                  `x

	std::vector<int_t> results = { pts[0], pts[1], pts[2], pts[3] };
	return results;
}
const std::vector<int_t> TetsPostElement::getType2Connects(const std::vector<int_t>& pts)
{

	//         z
	//	4----------6 
	//	|\     ^   |\ 
	//	| \    |   | \
	//	|  \   |   |  \
	//	|   5------+---none
	//	|   |  +-- |-- |->y
	//	0---+---\--3   | 
	//	 \  |    \  \  | 
	//	  \ |     \  \ | 
	//	   \|      x  \| 
	//	    1----------2 

	std::vector<int_t> results = { pts[0], pts[1], pts[3], pts[4],
									pts[1], pts[3], pts[5], pts[4],
									pts[3], pts[4], pts[5], pts[6],
									pts[1], pts[2], pts[3], pts[5],
									pts[2], pts[3], pts[5], pts[6]};
	return results;
}
const std::vector<int_t> TetsPostElement::getType3Connects(const std::vector<int_t>& pts)
{

	//         z
	//	4----------7 
	//	|\     ^   |\ 
	//	| \    |   | \
	//	|  \   |   |  \
	//	|   5------+---6
	//	|   |  +-- |-- |->y
	//	0---+---\--3   | 
	//	 \  |    \  \  | 
	//	  \ |     \  \ | 
	//	   \|      x  \| 
	//	    1----------2 

	std::vector<int_t> results = { pts[0], pts[1], pts[3], pts[4],
		pts[1], pts[3], pts[5], pts[4],
		pts[3], pts[4], pts[5], pts[7],
		pts[1], pts[2], pts[3], pts[5],
		pts[2], pts[3], pts[5], pts[7],
		pts[2], pts[5], pts[6], pts[7]};
	return results;
}

const std::vector<real_t> HexaPostElement::getPostComCoords(const int_t post_order)
{
	std::vector<real_t> coords;
	coords.reserve((post_order + 1)*(post_order + 1)*(post_order + 1) * 3);

	for (int_t iz = 0; iz <= post_order; iz++){
		for (int_t iy = 0; iy <= post_order; iy++){
			for (int_t ix = 0; ix <= post_order; ix++){
				coords.push_back(real_t(2 * ix) / real_t(post_order) - 1.0);
				coords.push_back(real_t(2 * iy) / real_t(post_order) - 1.0);
				coords.push_back(real_t(2 * iz) / real_t(post_order) - 1.0);
			}
		}
	}

	return coords;
}
const std::vector<int_t> HexaPostElement::getPostConnects(const int_t post_order)
{
	std::vector<int_t> connects;
	connects.reserve(24 * post_order*post_order*post_order);

	for (int_t iz = 0; iz < post_order; iz++){
		for (int_t iy = 0; iy < post_order; iy++){
			for (int_t ix = 0; ix < post_order; ix++){
				int_t var1 = (post_order + 1)*(post_order + 1)*iz + (post_order + 1)*iy + ix;
				int_t var2 = (post_order + 1)*(post_order + 1)*iz + (post_order + 1)*(iy + 1) + ix;
				int_t var3 = (post_order + 1)*(post_order + 1)*(iz + 1) + (post_order + 1)*iy + ix;
				int_t var4 = (post_order + 1)*(post_order + 1)*(iz + 1) + (post_order + 1)*(iy + 1) + ix;
				std::vector<int_t> type1 = { var1, var1 + 1, var2 + 1, var2, var3, var3+1, var4+1, var4};
				for (auto&& i : getType1Connects(type1))
					connects.push_back(i);

			}
		}
	}
	return connects;
}
const std::vector<int_t> HexaPostElement::getType1Connects(const std::vector<int_t>& pts)
{

	//         z
	//	4----------7 
	//	|\     ^   |\ 
	//	| \    |   | \
	//	|  \   |   |  \
	//	|   5------+---6
	//	|   |  +-- |-- |->y
	//	0---+---\--3   | 
	//	 \  |    \  \  | 
	//	  \ |     \  \ | 
	//	   \|      x  \| 
	//	    1----------2 

	std::vector<int_t> results = { pts[0], pts[1], pts[3], pts[4],
		pts[1], pts[3], pts[5], pts[4],
		pts[3], pts[4], pts[5], pts[7],
		pts[1], pts[2], pts[3], pts[5],
		pts[2], pts[3], pts[5], pts[7],
		pts[2], pts[5], pts[6], pts[7] };
	return results;
}

const std::vector<real_t> PrisPostElement::getPostComCoords(const int_t post_order)
{
	std::vector<real_t> coords;
	coords.reserve((post_order + 1)*(post_order + 2)*(post_order + 1) / 2 * 3);

	for (int_t iz = 0; iz <= post_order; iz++) {
		for (int_t iy = 0; iy <= post_order; iy++) {
			for (int_t ix = 0; ix <= post_order - iy; ix++) {
				coords.push_back(real_t(2 * ix) / real_t(post_order) - 1.0);
				coords.push_back(real_t(2 * iy) / real_t(post_order) - 1.0);
				coords.push_back(real_t(2 * iz) / real_t(post_order) - 1.0);
			}
		}
	}

	return coords;
}
const std::vector<int_t> PrisPostElement::getPostConnects(const int_t post_order)
{
	std::vector<int_t> connects;
	connects.reserve(12 * post_order*post_order*post_order);

	for (int_t iz = 0; iz < post_order; iz++) {
		for (int_t iy = 0; iy < post_order; iy++) {
			for (int_t ix = 0; ix < post_order - iy; ix++) {
				int_t sum = ix + iy;
				if (sum == post_order - 1) {
					int_t var1 = (post_order + 1)*(post_order + 2) / 2 * iz + (2 * post_order - iy + 3)*iy / 2 + ix;
					int_t var2 = (post_order + 1)*(post_order + 2) / 2 * iz + (2 * post_order - iy + 2)*(iy + 1) / 2 + ix;
					int_t var3 = (post_order + 1)*(post_order + 2) / 2 * (iz + 1) + (2 * post_order - iy + 3)*iy / 2 + ix;
					int_t var4 = (post_order + 1)*(post_order + 2) / 2 * (iz + 1) + (2 * post_order - iy + 2)*(iy + 1) / 2 + ix;
					std::vector<int_t> type2 = { var1, var1 + 1, var2, var3, var3 + 1, var4 };
					for (auto&& i : getType2Connects(type2))
						connects.push_back(i);

				}
				else {
					int_t var1 = (post_order + 1)*(post_order + 2) / 2 * iz + (2 * post_order - iy + 3)*iy / 2 + ix;
					int_t var2 = (post_order + 1)*(post_order + 2) / 2 * iz + (2 * post_order - iy + 2)*(iy + 1) / 2 + ix;
					int_t var3 = (post_order + 1)*(post_order + 2) / 2 * (iz + 1) + (2 * post_order - iy + 3)*iy / 2 + ix;
					int_t var4 = (post_order + 1)*(post_order + 2) / 2 * (iz + 1) + (2 * post_order - iy + 2)*(iy + 1) / 2 + ix;
					std::vector<int_t> type1 = { var1, var1 + 1, var2 + 1, var2, var3, var3 + 1, var4 + 1, var4 };
					for (auto&& i : getType1Connects(type1))
						connects.push_back(i);
				}
			}
		}
	}
	return connects;
}
const std::vector<int_t> PrisPostElement::getType1Connects(const std::vector<int_t>& pts)
{

	//         z
	//	4----------7 
	//	|\     ^   |\ 
	//	| \    |   | \
	//	|  \   |   |  \
	//	|   5------+---6
	//	|   |  +-- |-- |->y
	//	0---+---\--3   | 
	//	 \  |    \  \  | 
	//	  \ |     \  \ | 
	//	   \|      x  \| 
	//	    1----------2 

	std::vector<int_t> results = { pts[0], pts[1], pts[3], pts[4],
		pts[1], pts[3], pts[5], pts[4],
		pts[3], pts[4], pts[5], pts[7],
		pts[1], pts[2], pts[3], pts[5],
		pts[2], pts[3], pts[5], pts[7],
		pts[2], pts[5], pts[6], pts[7] };
	return results;
}
const std::vector<int_t> PrisPostElement::getType2Connects(const std::vector<int_t>& pts)
{

	//         z
	//	3----------5
	//	|\     ^   |\ 
	//	| \    |   | \
	//	|  \   |   |  \
	//	|   4------+---none
	//	|   |  +-- |-- |->y
	//	0---+---\--2   | 
	//	 \  |    \  \  | 
	//	  \ |     \  \ | 
	//	   \|      x  \| 
	//	    1----------none 

	std::vector<int_t> results = { pts[0], pts[1], pts[2], pts[3],
		pts[1], pts[2], pts[4], pts[3],
		pts[2], pts[3], pts[4], pts[5] };
	return results;
}