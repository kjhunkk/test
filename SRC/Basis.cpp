#include "../INC/Basis.h"

std::shared_ptr<Basis> BasisFactory::getBasis(const int_t dimension, const ElemType elemtype)
{
	std::shared_ptr<Basis> bs;
	switch (dimension)
	{
	case 1: bs = std::make_shared<BasisLine>(); break;
	case 2:
		switch (elemtype) {
		case ElemType::SPLX:
		case ElemType::TRIS:
			bs = std::make_shared<BasisTris>(); break;
		case ElemType::QUAD:
			bs = std::make_shared<BasisQuad>(); break;
		}
		break;
	case 3:
		switch (elemtype) {
		case ElemType::SPLX:
		case ElemType::TETS:
			bs = std::make_shared<BasisTets>(); break;
		case ElemType::HEXA:
			bs = std::make_shared<BasisHexa>(); break;
		case ElemType::PRIS:
			bs = std::make_shared<BasisPris>(); break;
		case ElemType::PYRA:
			bs = std::make_shared<BasisPyra>(); break;
		}
		break;
	}
	return bs;
}

//Basis
int_t Basis::dimension_;
void Basis::calculateBasis(const int_t order, const std::vector<real_t>& center, const std::vector<real_t>& quad_points, const std::vector<real_t>& quad_weights)
{
	order_ = order;
	center_ = center;
	num_basis_ = calNumBasis(order);
	int_t num_quad_points = quad_weights.size();
	std::vector<std::vector<real_t>> q_var(num_basis_);
	for (int_t i = 0; i < num_basis_; i++)
		q_var[i].resize(num_quad_points);

	for (int_t i = 0; i < num_quad_points; i++) {
		std::vector<real_t> value = calMonomial(order_, &quad_points[dimension_*i]);
		for (int_t j = 0; j < num_basis_; j++)
			q_var[j][i] = value[j];
	}

	std::vector<int_t> index_start;
	std::vector<real_t> coefficients;
	index_start.resize(num_basis_ + 1);
	for (int_t i = 0; i < num_basis_ + 1; i++)
		index_start[i] = i * (i + 1) / 2;
	coefficients.resize(index_start.back(), 0.0);
	for (int_t i = 0; i < num_basis_; i++)
		coefficients[index_start[i + 1] - 1] = 1.0;

	for (int_t i = 0; i < num_basis_; i++) {
		for (int_t iter = 0; iter < iteration_; iter++) {
			std::vector<real_t> r(i + 1);
			for (int_t j = 0; j < i; j++) {
				for (int_t q = 0; q < num_quad_points; q++)
					r[j] += q_var[i][q] * q_var[j][q] * quad_weights[q];
				for (int_t q = 0; q < num_quad_points; q++)
					q_var[i][q] -= r[j] * q_var[j][q];
			}
			real_t norm2 = 0.0;
			for (int_t q = 0; q < num_quad_points; q++)
				norm2 += q_var[i][q] * q_var[i][q] * quad_weights[q];
			r[i] = 1.0 / std::sqrt(norm2);
			for (int_t q = 0; q < num_quad_points; q++)
				q_var[i][q] *= r[i];
			for (int_t j = 0; j < i; j++)
				r[j] *= (-r[i]);
			for (int_t j = 0; j <= i; j++) {
				real_t sum = 0.0;
				for (int_t k = j; k <= i; k++)
					sum += r[k] * coefficients[index_start[k] + j];
				coefficients[index_start[i] + j] = sum;
			}
		}
	}
	coefficients_ = move(coefficients);
}
const std::vector<real_t> Basis::getBasis(const int_t start_order, const int_t end_order, const std::vector<real_t>& coords) const
{
	const int_t num_points = coords.size() / dimension_;
	const int_t start_index = calNumBasis(start_order - 1);
	const int_t end_index = calNumBasis(end_order);
	std::vector<real_t> results;
	results.reserve((end_index - start_index)*num_points);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const std::vector<real_t> monomials = calMonomial(end_order, &coords[dimension_ * ipoint]);
		for (int_t ibasis = start_index; ibasis < end_index; ibasis++) {
			int_t jump = ibasis * (ibasis + 1) / 2;
			real_t sum = 0.0;
			for (int_t i = 0; i <= ibasis; i++)
				sum += coefficients_[jump + i] * monomials[i];
			results.push_back(sum);
		}
	}
	return results;
}
const std::vector<real_t> Basis::getDivBasis(const int_t start_order, const int_t end_order, const std::vector<real_t>& coords) const
{
	int_t num_points = coords.size() / dimension_;
	const int_t start_index = calNumBasis(start_order - 1);
	const int_t end_index = calNumBasis(end_order);
	std::vector<real_t> results;
	results.reserve((end_index - start_index)*dimension_*num_points);

	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		const std::vector<real_t> monomials = calDiffMonomial(end_order, &coords[dimension_ * ipoint]);
		for (int_t ibasis = start_index; ibasis < end_index; ibasis++) {
			int_t jump = ibasis * (ibasis + 1) / 2;
			for (int_t idim = 0; idim < dimension_; idim++) {
				real_t sum = 0.0;
				for (int_t i = 0; i <= ibasis; i++)
					sum += coefficients_[jump + i] * monomials[dimension_*i + idim];
				results.push_back(sum);
			}
		}
	}
	return results;
}
void Basis::setRotation(const std::vector<real_t> coords, ElemType elemtype)
{
	ElemType elemtype_ = elemtype;
	if (elemtype == ElemType::SPLX) {
		if (dimension_ == 2)
			elemtype_ = ElemType::TRIS;
		else if (dimension_ == 3)
			elemtype_ = ElemType::TETS;
	}
	int_t np;
	std::vector<real_t> weight;
	switch (elemtype_) {
	case ElemType::TRIS: np = 3;
		weight = { -0.5, 0.5, 0.0,
			-0.5, 0.0, 0.5 };
		break;
	case ElemType::QUAD: np = 4;
		weight = { -0.25, 0.25, 0.25, -0.25,
			-0.25, -0.25, 0.25, 0.25 };
		break;
	case ElemType::TETS: np = 4;
		weight = { -0.5, 0.5, 0.0, 0.0,
			-0.5, 0.0, 0.5, 0.0,
			-0.5, 0.0, 0.0, 0.5 };
		break;
	case ElemType::HEXA: np = 8;
		weight = { -0.125, 0.125, 0.125, -0.125, -0.125, 0.125, 0.125, -0.125,
			-0.125, -0.125, 0.125, 0.125, -0.125, -0.125, 0.125, 0.125,
			-0.125, -0.125, -0.125, -0.125, 0.125, 0.125, 0.125, 0.125 };
		break;
	case ElemType::PRIS: np = 6;
		weight = { -0.25, 0.25, 0.0, -0.25, 0.25, 0.0,
			-0.25, 0.0, 0.25, -0.25, 0.0, 0.25,
			-0.16666666666666666666666666667, -0.16666666666666666666666666667, -0.16666666666666666666666666667, 0.16666666666666666666666666667, 0.16666666666666666666666666667, 0.16666666666666666666666666667 };
		break;
	case ElemType::PYRA: np = 5;
		weight = { -0.25, 0.25, 0.25, -0.25, 0.0,
			-0.25, -0.25, 0.25, 0.25, 0.0,
			-0.125, -0.125, -0.125, -0.125, 0.5 };
		break;
	default:
		return;
	}

	arma::mat rotate(dimension_, dimension_);
	for (int_t i = 0; i < dimension_; i++) {
		for (int_t j = 0; j < dimension_; j++) {
			rotate(i, j) = 0.0;
			for (int_t k = 0; k < np; k++) {
				rotate(i, j) += coords[k*dimension_ + i] * weight[j*np + k];
			}
		}
	}
	rotate = arma::inv(rotate);
	std::vector<real_t> rotation(dimension_*dimension_);
	for (int_t i = 0; i < dimension_; i++)
		for (int_t j = 0; j < dimension_; j++)
			rotation[i * dimension_ + j] = rotate(i, j);

	rotation_ = move(rotation);
}
const std::vector<real_t> Basis::calRotation(const std::vector<real_t> coords) const
{
	if (rotation_.size() == 4) {
		return { rotation_[0] * coords[0] + rotation_[1] * coords[1],
			rotation_[2] * coords[0] + rotation_[3] * coords[1],
		};
	}
	else if (rotation_.size() == 9) {
		return { rotation_[0] * coords[0] + rotation_[1] * coords[1] + rotation_[2] * coords[2],
			rotation_[3] * coords[0] + rotation_[4] * coords[1] + rotation_[5] * coords[2],
			rotation_[6] * coords[0] + rotation_[7] * coords[1] + rotation_[8] * coords[2]
		};
	}
	return coords;
}

// Basis Line
const std::vector<real_t> BasisLine::calMonomial(const int_t order, const real_t* coord) const
{
	return Monomial::monomialLine(order, coord[0] - center_[0]);
}
const std::vector<real_t> BasisLine::calDiffMonomial(const int_t order, const real_t* coord) const
{
	return Monomial::monomialDrLine(order, coord[0] - center_[0]);
}
int_t BasisLine::calNumBasis(const int_t order) const
{
	return Monomial::numMonomialLine(order);
}

// Basis Tris
const std::vector<real_t> BasisTris::calMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1] });
	return Monomial::monomialTris(order, coord_new[0], coord_new[1]);
}
const std::vector<real_t> BasisTris::calDiffMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1] });
	const std::vector<real_t> dr = Monomial::monomialDrTris(order, coord_new[0], coord_new[1]);
	const std::vector<real_t> ds = Monomial::monomialDsTris(order, coord_new[0], coord_new[1]);
	std::vector<real_t> result;
	result.reserve(dr.size() * dimension_);
	if (rotation_.size() > 0) {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i] * rotation_[0] + ds[i] * rotation_[2]);
			result.push_back(dr[i] * rotation_[1] + ds[i] * rotation_[3]);
		}
	}
	else {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i]);
			result.push_back(ds[i]);
		}
	}
	return result;
}
int_t BasisTris::calNumBasis(const int_t order) const
{
	return Monomial::numMonomialTris(order);
}

// Basis Quad
const std::vector<real_t> BasisQuad::calMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1] });
	return Monomial::monomialQuad(order, coord_new[0], coord_new[1]);
}
const std::vector<real_t> BasisQuad::calDiffMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1] });
	const std::vector<real_t> dr = Monomial::monomialDrQuad(order, coord_new[0], coord_new[1]);
	const std::vector<real_t> ds = Monomial::monomialDsQuad(order, coord_new[0], coord_new[1]);
	std::vector<real_t> result;
	result.reserve(dr.size() * dimension_);
	if (rotation_.size() > 0) {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i] * rotation_[0] + ds[i] * rotation_[2]);
			result.push_back(dr[i] * rotation_[1] + ds[i] * rotation_[3]);
		}
	}
	else {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i]);
			result.push_back(ds[i]);
		}
	}
	return result;
}
int_t BasisQuad::calNumBasis(const int_t order) const
{
	return Monomial::numMonomialQuad(order);
}

// Basis Tets
const std::vector<real_t> BasisTets::calMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1], coord[2] - center_[2] });
	return Monomial::monomialTets(order, coord_new[0], coord_new[1], coord_new[2]);
}
const std::vector<real_t> BasisTets::calDiffMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1], coord[2] - center_[2] });
	const std::vector<real_t> dr = Monomial::monomialDrTets(order, coord_new[0], coord_new[1], coord_new[2]);
	const std::vector<real_t> ds = Monomial::monomialDsTets(order, coord_new[0], coord_new[1], coord_new[2]);
	const std::vector<real_t> dt = Monomial::monomialDtTets(order, coord_new[0], coord_new[1], coord_new[2]);
	std::vector<real_t> result;
	result.reserve(dr.size() * dimension_);
	if (rotation_.size() > 0) {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i] * rotation_[0] + ds[i] * rotation_[3] + dt[i] * rotation_[6]);
			result.push_back(dr[i] * rotation_[1] + ds[i] * rotation_[4] + dt[i] * rotation_[7]);
			result.push_back(dr[i] * rotation_[2] + ds[i] * rotation_[5] + dt[i] * rotation_[8]);
		}
	}
	else {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i]);
			result.push_back(ds[i]);
			result.push_back(dt[i]);
		}
	}
	return result;
}
int_t BasisTets::calNumBasis(const int_t order) const
{
	return Monomial::numMonomialTets(order);
}

// Basis Hexa
const std::vector<real_t> BasisHexa::calMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1], coord[2] - center_[2] });
	return Monomial::monomialHexa(order, coord_new[0], coord_new[1], coord_new[2]);
}
const std::vector<real_t> BasisHexa::calDiffMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1], coord[2] - center_[2] });
	const std::vector<real_t> dr = Monomial::monomialDrHexa(order, coord_new[0], coord_new[1], coord_new[2]);
	const std::vector<real_t> ds = Monomial::monomialDsHexa(order, coord_new[0], coord_new[1], coord_new[2]);
	const std::vector<real_t> dt = Monomial::monomialDtHexa(order, coord_new[0], coord_new[1], coord_new[2]);
	std::vector<real_t> result;
	result.reserve(dr.size() * dimension_);
	if (rotation_.size() > 0) {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i] * rotation_[0] + ds[i] * rotation_[3] + dt[i] * rotation_[6]);
			result.push_back(dr[i] * rotation_[1] + ds[i] * rotation_[4] + dt[i] * rotation_[7]);
			result.push_back(dr[i] * rotation_[2] + ds[i] * rotation_[5] + dt[i] * rotation_[8]);
		}
	}
	else {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i]);
			result.push_back(ds[i]);
			result.push_back(dt[i]);
		}
	}
	return result;
}
int_t BasisHexa::calNumBasis(const int_t order) const
{
	return Monomial::numMonomialHexa(order);
}

// Basis Pris
const std::vector<real_t> BasisPris::calMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1], coord[2] - center_[2] });
	return Monomial::monomialPris(order, coord_new[0], coord_new[1], coord_new[2]);
}
const std::vector<real_t> BasisPris::calDiffMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1], coord[2] - center_[2] });
	const std::vector<real_t> dr = Monomial::monomialDrPris(order, coord_new[0], coord_new[1], coord_new[2]);
	const std::vector<real_t> ds = Monomial::monomialDsPris(order, coord_new[0], coord_new[1], coord_new[2]);
	const std::vector<real_t> dt = Monomial::monomialDtPris(order, coord_new[0], coord_new[1], coord_new[2]);
	std::vector<real_t> result;
	result.reserve(dr.size() * dimension_);
	if (rotation_.size() > 0) {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i] * rotation_[0] + ds[i] * rotation_[3] + dt[i] * rotation_[6]);
			result.push_back(dr[i] * rotation_[1] + ds[i] * rotation_[4] + dt[i] * rotation_[7]);
			result.push_back(dr[i] * rotation_[2] + ds[i] * rotation_[5] + dt[i] * rotation_[8]);
		}
	}
	else {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i]);
			result.push_back(ds[i]);
			result.push_back(dt[i]);
		}
	}
	return result;
}
int_t BasisPris::calNumBasis(const int_t order) const
{
	return Monomial::numMonomialPris(order);
}

// Basis Pyra
const std::vector<real_t> BasisPyra::calMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1], coord[2] - center_[2] });
	return Monomial::monomialPyra2(order, coord_new[0], coord_new[1], coord_new[2]);
}
const std::vector<real_t> BasisPyra::calDiffMonomial(const int_t order, const real_t* coord) const
{
	const std::vector<real_t> coord_new = calRotation({ coord[0] - center_[0], coord[1] - center_[1], coord[2] - center_[2] });
	const std::vector<real_t> dr = Monomial::monomialDrPyra2(order, coord_new[0], coord_new[1], coord_new[2]);
	const std::vector<real_t> ds = Monomial::monomialDsPyra2(order, coord_new[0], coord_new[1], coord_new[2]);
	const std::vector<real_t> dt = Monomial::monomialDtPyra2(order, coord_new[0], coord_new[1], coord_new[2]);
	std::vector<real_t> result;
	result.reserve(dr.size() * dimension_);
	if (rotation_.size() > 0) {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i] * rotation_[0] + ds[i] * rotation_[3] + dt[i] * rotation_[6]);
			result.push_back(dr[i] * rotation_[1] + ds[i] * rotation_[4] + dt[i] * rotation_[7]);
			result.push_back(dr[i] * rotation_[2] + ds[i] * rotation_[5] + dt[i] * rotation_[8]);
		}
	}
	else {
		for (int_t i = 0; i < dr.size(); i++) {
			result.push_back(dr[i]);
			result.push_back(ds[i]);
			result.push_back(dt[i]);
		}
	}
	return result;
}
int_t BasisPyra::calNumBasis(const int_t order) const
{
	return Monomial::numMonomialPyra(order);
}