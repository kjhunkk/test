
#include "../INC/BGBoundary.h"

const std::vector<real_t> BGBCExtrapolate::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFlux(owner_u, normal);
}
const std::vector<real_t> BGBCExtrapolate::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	return flux_->commonConvFluxJacobian(owner_u, normal);
}
const std::vector<real_t> BGBCExtrapolate::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFlux(owner_u, owner_div_u, normal);
}
const std::vector<real_t> BGBCExtrapolate::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> BGBCExtrapolate::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	return flux_->commonViscFluxGradJacobian(owner_u, owner_div_u, normal);
}
const std::vector<real_t> BGBCExtrapolate::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{
	return owner_u;
}
const std::vector<real_t> BGBCExtrapolate::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{
	return std::vector<real_t>(owner_u.size(), 1.0);
}

const std::vector<real_t> BGBCConstant::calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	return flux_->numConvFlux(owner_u, references, normal);
}
const std::vector<real_t> BGBCConstant::calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	return flux_->numConvFluxOwnerJacobian(owner_u, references, normal);
}
const std::vector<real_t> BGBCConstant::calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFlux(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> BGBCConstant::calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> BGBCConstant::calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	const std::vector<real_t> references_div(owner_div_u.size(), 0.0);
	return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, references, references_div, normal);
}
const std::vector<real_t> BGBCConstant::calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{
	std::vector<real_t> references(owner_u.size(), references_[0]);
	return references;
}
const std::vector<real_t> BGBCConstant::calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const
{
	return std::vector<real_t>(owner_u.size(), 0.0);
}