#include "../INC/Zone.h"

//#include <float.h> // for debug

void Zone::initialize()
{
	ndomain_ = NDOMAIN();
	myrank_ = MYRANK();

	const Config& config = Config::getInstance();
	
	order_ = config.getOrder();
	dimension_ = config.getDimension();
	CFL_ = config.getCFL();
	
	// setting grid
	num_basis_ = grid_->getNumBasis();
	num_cells_ = grid_->getNumCells();
	num_total_cells_ = grid_->getNumTotalCells();
	num_faces_ = grid_->getNumFaces();
	num_bdries_ = grid_->getNumBdries();
	num_peribdries_ = grid_->getNumPeriBdries();
	
	// setting equation
	equation_ = Equation::getInstance().getType(config.getEquationName());
	equation_->setFlux(config.getConvFluxName());
	equation_->setInitializer(config.getProblemName());
	equation_->setBoundaries(grid_->getBdryType());
	visc_flux_ = equation_->isViscFlux();
	source_ = equation_->isSource();
	num_states_ = equation_->getNumStates();
	
	if (source_ == true)
		grid_->calWallDistance();
	
	//// setting limiter
	limiter_ = Limiter::getInstance().getType(config.getLimiterName());
	limiter_->initialize(grid_, equation_, &solution_);
	
	// setting pressurefix
	std::string pf = TO_UPPER(config.getPressureFix());
	if (!pf.compare("ON")) pressure_fix_ = true;
	else pressure_fix_ = false;
	
#ifdef _Pressure_Fix_Hist_
	if (pressure_fix_ == true && MYRANK() == MASTER_NODE) {
		const std::string return_address = config.getReturnAddress();
		const std::string filename = return_address + "/pressureFix.dat";
		pressure_fix_hist_file_.open(filename);
	}
#endif
	
	// communication pre-processing
	if (ndomain_ > 1) {
		const std::vector<std::vector<int_t>>& send_cells = grid_->getSendCellList();
		const std::vector<std::vector<int_t>>& recv_cells = grid_->getRecvCellList();

		num_send_data_.resize(ndomain_);
		num_recv_data_.resize(ndomain_);
		for (int idomain = 0; idomain < ndomain_; idomain++) {
			num_send_data_[idomain] = num_states_ * num_basis_*send_cells[idomain].size();
			num_recv_data_[idomain] = num_states_ * num_basis_*recv_cells[idomain].size();
		}

		send_data_.resize(ndomain_);
		recv_data_.resize(ndomain_);
		for (int_t idomain = 0; idomain < ndomain_; idomain++) {
			if (num_send_data_[idomain] == 0) send_data_[idomain].resize(1);
			else send_data_[idomain].resize(num_send_data_[idomain]);
			if (num_recv_data_[idomain] == 0) recv_data_[idomain].resize(1);
			else recv_data_[idomain].resize(num_recv_data_[idomain]);
		}
	}

	// file save pre-processing
	const int_t disp = sizeof(int_t)*num_cells_ + sizeof(real_t)*num_cells_*num_states_*num_basis_;
	const int_t disp_app = sizeof(int_t)*num_cells_ + sizeof(real_t)*num_cells_*num_states_; // for implciit
	std::vector<int_t> disp_gather(ndomain_);

	MPI_Allgather(&disp, 1, MPI_INT_T, &disp_gather[0], 1, MPI_INT_T, MPI_COMM_WORLD);
	save_disp_ = 0;
	for (int_t idomain = 0; idomain < myrank_; idomain++)
		save_disp_ += disp_gather[idomain];
	if (myrank_ != MASTER_NODE)
		save_disp_ += (sizeof(int_t) * 5 + sizeof(real_t) * 2);

	// for implicit
	MPI_Allgather(&disp_app, 1, MPI_INT_T, &disp_gather[0], 1, MPI_INT_T, MPI_COMM_WORLD);
	save_app_disp_ = 0;
	for (int_t idomain = 0; idomain < myrank_; idomain++)
		save_app_disp_ += disp_gather[idomain];
	if (myrank_ != MASTER_NODE)
		save_app_disp_ += (sizeof(int_t) * 5 + sizeof(real_t) * 2);
	// for implicit

	// solution, rhs initialization
	std::string rs = TO_UPPER(config.getRestart());
	if (rs.compare("ON")){
		initializeSolution();
		communication();
		limiting();
		pressurefix(0.0, -1);
	}
	initializeAuxiliary();
}

void Zone::initializeSolution()
{
	START();
	std::vector<std::vector<real_t>> solution(num_total_cells_);
	for (int_t icell = 0; icell < num_total_cells_; icell++)
		solution[icell].resize(num_states_*num_basis_, 0.0);
	
	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_quad_points = grid_->getCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_quad_weights = grid_->getCellQuadWeights();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();
	
	//grid_->calWallDistance(); // for debug
	//const std::vector<std::vector<real_t>>& wall_dist = grid_->getWallDistances(); // for debug

	for (int_t icell = 0; icell < num_total_cells_; icell++){
		std::vector<real_t> initial_value = equation_->getInitializerVal(cell_quad_points[icell]);
		
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		for (int_t ipoint = 0; ipoint < num_cell_quad_points[icell]; ipoint++)
			solution[icell][istate*num_basis_ + ibasis] += initial_value[ipoint*num_states_ + istate] * cell_basis_value[icell][ipoint*num_basis_ + ibasis] * cell_quad_weights[icell][ipoint];
		
		/*if (icell < num_cells_) { // for debug
			for (int_t istate = 0; istate < 1; istate++) {
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++) {
					solution[icell][istate*num_basis_ + ibasis] = 0.0;
					for (int_t ipoint = 0; ipoint < num_cell_quad_points[icell]; ipoint++) {
						solution[icell][istate*num_basis_ + ibasis] += wall_dist[icell][ipoint] * cell_basis_value[icell][ipoint*num_basis_ + ibasis] * cell_quad_weights[icell][ipoint];
					}
				}
			}
		}*/
	}
	solution_ = move(solution);
	MASTER_MESSAGE("Message(Zone): Solutions are initialized(Time: " + TO_STRING(STOP()) + "s)");
}

std::vector<std::vector<real_t> > Zone::getExactSolution(const real_t time) // for implicit
{
	std::vector<std::vector<real_t>> solution(num_total_cells_);
	for (int_t icell = 0; icell < num_total_cells_; icell++)
		solution[icell].resize(num_states_*num_basis_, 0.0);

	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_quad_points = grid_->getCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_quad_weights = grid_->getCellQuadWeights();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();

	//grid_->calWallDistance(); // for debug
	//const std::vector<std::vector<real_t>>& wall_dist = grid_->getWallDistances(); // for debug

	for (int_t icell = 0; icell < num_total_cells_; icell++) {
		std::vector<real_t> initial_value = equation_->getExactVal(cell_quad_points[icell], time);

		for (int_t istate = 0; istate < num_states_; istate++)
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				for (int_t ipoint = 0; ipoint < num_cell_quad_points[icell]; ipoint++)
					solution[icell][istate*num_basis_ + ibasis] += initial_value[ipoint*num_states_ + istate] * cell_basis_value[icell][ipoint*num_basis_ + ibasis] * cell_quad_weights[icell][ipoint];

		/*if (icell < num_cells_) { // for debug
		for (int_t istate = 0; istate < 1; istate++) {
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++) {
		solution[icell][istate*num_basis_ + ibasis] = 0.0;
		for (int_t ipoint = 0; ipoint < num_cell_quad_points[icell]; ipoint++) {
		solution[icell][istate*num_basis_ + ibasis] += wall_dist[icell][ipoint] * cell_basis_value[icell][ipoint*num_basis_ + ibasis] * cell_quad_weights[icell][ipoint];
		}
		}
		}
		}*/
	}
	return solution;
}

void Zone::initializeImpOperator()
{
	// Matrix create
	PetscInt block_size = num_states_ * num_basis_;
	MatCreate(PETSC_COMM_WORLD, &implicit_operator_);
	MatSetSizes(implicit_operator_, num_cells_*block_size, num_cells_*block_size, PETSC_DECIDE, PETSC_DECIDE);
	MatSetFromOptions(implicit_operator_);

	// Matrix preallocation
	std::vector<PetscInt> d_nnz(num_cells_*block_size, 3*block_size);
	std::vector<PetscInt> o_nnz(num_cells_*block_size, 3*block_size);
	const std::vector<std::vector<int_t> >& cell_to_cells = grid_->getCellToCells();
	const std::vector<PetscInt>& cell_petsc_index = grid_->getCellPetscIndex();
	PetscInt petsc_index_start = *std::min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells_);
	PetscInt petsc_index_end = *std::max_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells_);

	for (int_t icell = 0; icell < num_cells_; icell++)
		for (int_t jcell = 0; jcell < cell_to_cells[icell].size(); jcell++)
		{
			if (cell_to_cells[icell][jcell] < num_cells_)
				for (int_t icont = 0; icont < block_size; icont++)
					d_nnz[(cell_petsc_index[icell] - petsc_index_start)*block_size + icont] += block_size;
			else
				for (int_t icont = 0; icont < block_size; icont++)
					o_nnz[(cell_petsc_index[icell] - petsc_index_start)*block_size + icont] += block_size;
		}
	if (NDOMAIN() == 1)
		MatSeqAIJSetPreallocation(implicit_operator_, NULL, &d_nnz[0]);
	else
		MatMPIAIJSetPreallocation(implicit_operator_, NULL, &d_nnz[0], NULL, &o_nnz[0]);
	//MatSetOption(implicit_operator_, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);

	std::vector<PetscInt> idxm(block_size);
	std::vector<PetscInt> idxn(block_size);
	std::vector<PetscReal> initial_values(block_size*block_size, 0.0);
	for (int_t icell = 0; icell < num_cells_; icell++)
	{
		// Self
		idxm[0] = cell_petsc_index[icell] * block_size;
		for (int_t ielem = 1; ielem < block_size; ielem++)
			idxm[ielem] = idxm[ielem - 1] + 1;
		for (int_t irow = 0; irow < block_size; irow++)
			for (int_t icolumn = 0; icolumn < block_size; icolumn++)
				initial_values[irow*block_size + icolumn] = cell_petsc_index[icell];
		
		MatSetValues(implicit_operator_, block_size, &idxm[0], block_size, &idxm[0], &initial_values[0], INSERT_VALUES);
		
		// Neighbor cells
		for (int_t jcell = 0; jcell < cell_to_cells[icell].size(); jcell++)
		{
			idxm[0] = cell_petsc_index[icell] * block_size;
			idxn[0] = cell_petsc_index[cell_to_cells[icell][jcell]] * block_size;
			for (int_t ielem = 1; ielem < block_size; ielem++)
			{
				idxm[ielem] = idxm[ielem - 1] + 1;
				idxn[ielem] = idxn[ielem - 1] + 1;
			}
			MatSetValues(implicit_operator_, block_size, &idxm[0], block_size, &idxn[0], &initial_values[0], INSERT_VALUES);
		}
	}
	MatAssemblyBegin(implicit_operator_, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(implicit_operator_, MAT_FINAL_ASSEMBLY);
	MASTER_MESSAGE("Message(Zone): System matrix is initialized(Time: " + TO_STRING(STOP()) + "s)");
}

void Zone::initializeRHS()
{
	rhs_.resize(num_total_cells_);
	for (int_t icell = 0; icell < num_total_cells_; icell++)
		rhs_[icell].resize(num_states_*num_basis_, 0.0);
}

void Zone::initializeRHSImplicit()
{
	PetscInt num_global_cells = grid_->getNumGlobalCells();
	VecCreate(MPI_COMM_WORLD, &implicit_rhs_);
	VecSetSizes(implicit_rhs_, num_cells_*num_states_*num_basis_, num_global_cells*num_states_*num_basis_);
	VecSetFromOptions(implicit_rhs_);
	const std::vector<PetscInt>& cell_petsc_index = grid_->getCellPetscIndex();

	std::vector<PetscInt> vector_index(num_cells_*num_states_*num_basis_);
	for (int_t icell = 0; icell < num_cells_; icell++)
		for (int_t istate = 0; istate < num_states_; istate++)
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				vector_index[icell*num_states_*num_basis_ + istate * num_basis_ + ibasis] = cell_petsc_index[icell] * num_states_*num_basis_ + istate * num_basis_ + ibasis;
	std::vector<PetscReal> vector_initial_value(num_cells_*num_states_*num_basis_, 0.0);
	for (int_t icell = 0; icell < num_cells_; icell++)
		for (int_t istate = 0; istate < num_states_; istate++)
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				vector_initial_value[icell*num_states_*num_basis_ + istate * num_basis_ + ibasis] = 0.0;
	VecSetValues(implicit_rhs_, num_cells_*num_states_*num_basis_, &vector_index[0], &vector_initial_value[0], INSERT_VALUES);
	VecAssemblyBegin(implicit_rhs_);
	VecAssemblyEnd(implicit_rhs_);
	if (visc_flux_ == true) initializeAuxiliaryJacobian();
	MASTER_MESSAGE("Message(Zone): RHS is initialized(Time: " + TO_STRING(STOP()) + "s)");
}

void Zone::resetRHS()
{
	for (int_t icell = 0; icell < num_total_cells_; icell++)
	for (auto&& var : rhs_[icell])
		var = 0.0;
}

void Zone::resetRHSImplicit()
{
	PetscReal* temp;
	VecGetArray(implicit_rhs_, &temp);
	for (int_t ivar = 0; ivar < num_cells_*num_states_*num_basis_; ivar++)
		temp[ivar] = 0.0;
	VecRestoreArray(implicit_rhs_, &temp);
}

void Zone::initializeAuxiliary()
{
	auxiliary_.resize(num_total_cells_);
	for (int_t icell = 0; icell < num_total_cells_; icell++)
		auxiliary_[icell].resize(num_states_*dimension_*num_basis_, 0.0);
}

void Zone::initializeAuxiliaryJacobian()
{
	auxiliary_jacobian_.resize(num_total_cells_);
	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	for (int_t icell = 0; icell < num_total_cells_; icell++)
		auxiliary_jacobian_[icell].resize(num_cell_quad_points[icell] * num_states_*num_states_ * num_basis_*dimension_);
}

void Zone::resetAuxiliary()
{
	for (int_t icell = 0; icell < num_total_cells_; icell++)
	for (auto&& var : auxiliary_[icell])
		var = 0.0;
}

void Zone::resetAuxiliaryImplicit()
{
	for (int_t icell = 0; icell < num_total_cells_; icell++)
	{
		for (auto&& var : auxiliary_[icell])
			var = 0.0;
		for (auto&& var : auxiliary_jacobian_[icell])
			var = 0.0;
	}
}

void Zone::resetSystemMatrix()
{
	MatZeroEntries(implicit_operator_);
}

const std::vector<std::vector<real_t>>& Zone::calculateRHS()
{
	resetRHS();
	if (visc_flux_ == true) resetAuxiliary();

	// periodic boundary sweep
	const std::vector<int_t>& num_peribdry_quad_points = grid_->getNumPeriBdryQuadPoints();
	const std::vector<int_t>& peribdry_owner_cell = grid_->getPeriBdryOwnerCell();
	const std::vector<int_t>& peribdry_neighbor_cell = grid_->getPeriBdryNeighborCell();
	const std::vector<std::vector<real_t>>& peribdry_owner_basis_value = grid_->getPeriBdryOwnerBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_basis_value = grid_->getPeriBdryNeighborBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_owner_div_basis_value = grid_->getPeriBdryOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_div_basis_value = grid_->getPeriBdryNeighborDivBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_normals = grid_->getPeriBdryNormals();
	const std::vector<std::vector<real_t>>& peribdry_owner_coefficients = grid_->getPeriBdryOwnerCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_coefficients = grid_->getPeriBdryNeighborCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_owner_auxiliary_coefficients = grid_->getPeriBdryOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_auxiliary_coefficients = grid_->getPeriBdryNeighborAuxiliaryCoefficients();

	for (int_t ibdry = 0; ibdry < num_peribdries_; ibdry++){
		const int_t& num_quad_points = num_peribdry_quad_points[ibdry];
		const int_t& owner_cell = peribdry_owner_cell[ibdry];
		const int_t& neighbor_cell = peribdry_neighbor_cell[ibdry];

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_solution[ipoint*num_states_ + istate] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			neighbor_solution[ipoint*num_states_ + istate] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution_[neighbor_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux = equation_->calNumConvFlux(owner_solution, neighbor_solution, peribdry_normals[ibdry]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			rhs_[owner_cell][istate*num_basis_ + ibasis] += peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];

		if (visc_flux_ == false) continue;

		std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_auxiliary[istate*dimension_*num_basis_ + idim*num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*peribdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim*num_basis_ + ibasis];

		std::vector<real_t> neighbor_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			neighbor_auxiliary[istate*dimension_*num_basis_ + idim*num_basis_ + ibasis] += (owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*peribdry_neighbor_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim*num_basis_ + ibasis];

		std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		for (int_t idim = 0; idim < dimension_; idim++)
			owner_div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += peribdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis*dimension_ + idim] * solution_[owner_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim*num_basis_ + ibasis];

		std::vector<real_t> neighbor_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		for (int_t idim = 0; idim < dimension_; idim++)
			neighbor_div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += peribdry_neighbor_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis*dimension_ + idim] * solution_[neighbor_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			neighbor_div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * neighbor_auxiliary[istate*dimension_*num_basis_ + idim*num_basis_ + ibasis];

		const std::vector<real_t> visc_flux = equation_->calNumViscFlux(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, peribdry_normals[ibdry]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			rhs_[owner_cell][istate*num_basis_ + ibasis] -= peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];
	}
	
	// boundary sweep
	const std::vector<int_t>& num_bdry_quad_points = grid_->getNumBdryQuadPoints();
	const std::vector<int_t>& bdry_owner_cell = grid_->getBdryOwnerCell();
	const std::vector<std::vector<real_t>>& bdry_owner_basis_value = grid_->getBdryOwnerBasisValue();
	const std::vector<std::vector<real_t>>& bdry_owner_div_basis_value = grid_->getBdryOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& bdry_normals = grid_->getBdryNormals();
	const std::vector<std::vector<real_t>>& bdry_owner_coefficients = grid_->getBdryOwnerCoefficients();
	const std::vector<std::vector<real_t>>& bdry_owner_auxiliary_coefficients = grid_->getBdryOwnerAuxiliaryCoefficients();

	for (int_t ibdry = 0; ibdry < num_bdries_; ibdry++){
		const int_t& num_quad_points = num_bdry_quad_points[ibdry];
		const int_t& owner_cell = bdry_owner_cell[ibdry];

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_solution[ipoint*num_states_ + istate] += bdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux = equation_->calNumBdryConvFlux(owner_solution, bdry_normals[ibdry], ibdry);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			rhs_[owner_cell][istate*num_basis_ + ibasis] += bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];

		if (visc_flux_ == false) continue;

		std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		for (int_t idim = 0; idim < dimension_; idim++)
			owner_div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += bdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis*dimension_ + idim] * solution_[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution = equation_->calNumBdrySolution(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);

		std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_auxiliary[istate*dimension_*num_basis_ + idim*num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*bdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += bdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim*num_basis_ + ibasis];

		const std::vector<real_t> visc_flux = equation_->calNumBdryViscFlux(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			rhs_[owner_cell][istate*num_basis_ + ibasis] -= bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];
	}
	
	// face sweep
	const std::vector<int_t>& num_face_quad_points = grid_->getNumFaceQuadPoints();
	const std::vector<int_t>& face_owner_cell = grid_->getFaceOwnerCell();
	const std::vector<int_t>& face_neighbor_cell = grid_->getFaceNeighborCell();
	const std::vector<std::vector<real_t>>& face_owner_basis_value = grid_->getFaceOwnerBasisValue();
	const std::vector<std::vector<real_t>>& face_neighbor_basis_value = grid_->getFaceNeighborBasisValue();
	const std::vector<std::vector<real_t>>& face_owner_div_basis_value = grid_->getFaceOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& face_neighbor_div_basis_value = grid_->getFaceNeighborDivBasisValue();
	const std::vector<std::vector<real_t>>& face_normals = grid_->getFaceNormals();
	const std::vector<std::vector<real_t>>& face_owner_coefficients = grid_->getFaceOwnerCoefficients();
	const std::vector<std::vector<real_t>>& face_neighbor_coefficients = grid_->getFaceNeighborCoefficients();
	const std::vector<std::vector<real_t>>& face_owner_auxiliary_coefficients = grid_->getFaceOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& face_neighbor_auxiliary_coefficients = grid_->getFaceNeighborAuxiliaryCoefficients();
	
	for (int_t iface = 0; iface < num_faces_; iface++){
		const int_t& num_quad_points = num_face_quad_points[iface];
		const int_t& owner_cell = face_owner_cell[iface];
		const int_t& neighbor_cell = face_neighbor_cell[iface];
		
		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_solution[ipoint*num_states_ + istate] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];
		
		std::vector<real_t> neighbor_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			neighbor_solution[ipoint*num_states_ + istate] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * solution_[neighbor_cell][istate*num_basis_ + ibasis];
		
		const std::vector<real_t> conv_flux = equation_->calNumConvFlux(owner_solution, neighbor_solution, face_normals[iface]);
		
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++){
			for (int_t istate = 0; istate < num_states_; istate++){
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++){
					rhs_[owner_cell][istate*num_basis_ + ibasis] += face_owner_coefficients[iface][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];
					rhs_[neighbor_cell][istate*num_basis_ + ibasis] -= face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];
				}
			}
		}
		
		if (visc_flux_ == false) continue;

		std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_auxiliary[istate*dimension_*num_basis_ + idim*num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*face_owner_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim*num_basis_ + ibasis];

		std::vector<real_t> neighbor_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			neighbor_auxiliary[istate*dimension_*num_basis_ + idim*num_basis_ + ibasis] += (owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*face_neighbor_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim*num_basis_ + ibasis];

		std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		for (int_t idim = 0; idim < dimension_; idim++)
			owner_div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += face_owner_div_basis_value[iface][ipoint*num_basis_*dimension_ + ibasis*dimension_+idim] * solution_[owner_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			owner_div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim*num_basis_ + ibasis];

		std::vector<real_t> neighbor_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		for (int_t idim = 0; idim < dimension_; idim++)
			neighbor_div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += face_neighbor_div_basis_value[iface][ipoint*num_basis_*dimension_ + ibasis*dimension_ + idim] * solution_[neighbor_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			neighbor_div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * neighbor_auxiliary[istate*dimension_*num_basis_ + idim*num_basis_ + ibasis];

		const std::vector<real_t> visc_flux = equation_->calNumViscFlux(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, face_normals[iface]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++){
			for (int_t istate = 0; istate < num_states_; istate++){
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++){
					rhs_[owner_cell][istate*num_basis_ + ibasis] -= face_owner_coefficients[iface][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];
					rhs_[neighbor_cell][istate*num_basis_ + ibasis] += face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];
				}
			}
		}

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[neighbor_cell][ivar] += neighbor_auxiliary[ivar];
	}
	
	// cell sweep
	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();
	const std::vector<std::vector<real_t>>& cell_div_basis_value = grid_->getCellDivBasisValue();
	const std::vector<std::vector<real_t>>& cell_coefficients = grid_->getCellCoefficients();
	const std::vector<std::vector<real_t>>& cell_source_coefficients = grid_->getCellSourceCoefficients();
	const std::vector<std::vector<real_t>>& wall_distance = grid_->getWallDistances();

	for (int_t icell = 0; icell < num_cells_; icell++){
		const int_t& num_quad_points = num_cell_quad_points[icell];

		std::vector<real_t> solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			solution[ipoint*num_states_ + istate] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * solution_[icell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux = equation_->calComConvFlux(solution);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
		for (int_t idim = 0; idim < dimension_; idim++)
			rhs_[icell][istate*num_basis_ + ibasis] -= (cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis*dimension_ + idim] * conv_flux[ipoint*num_states_*dimension_ + istate*dimension_ + idim]);

		if (visc_flux_ == true) {
			std::vector<real_t> div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			for (int_t idim = 0; idim < dimension_; idim++)
				div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += cell_div_basis_value[icell][ipoint*num_basis_*dimension_ + ibasis*dimension_ + idim] * solution_[icell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
			for (int_t idim = 0; idim < dimension_; idim++)
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
				div_solution[ipoint*num_states_*dimension_ + istate*dimension_ + idim] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * auxiliary_[icell][istate*dimension_*num_basis_ + idim*num_basis_ + ibasis];

			const std::vector<real_t> visc_flux = equation_->calComViscFlux(solution, div_solution);

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			for (int_t idim = 0; idim < dimension_; idim++)
				rhs_[icell][istate*num_basis_ + ibasis] += (cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis*dimension_ + idim] * visc_flux[ipoint*num_states_*dimension_ + istate*dimension_ + idim]);

			if (source_ == true) {
				const std::vector<real_t> source = equation_->calSource(solution, div_solution, wall_distance[icell]);

				for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
					for (int_t istate = 0; istate < num_states_; istate++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							rhs_[icell][istate*num_basis_ + ibasis] -= (cell_source_coefficients[icell][ipoint*num_basis_ + ibasis] * source[ipoint*num_states_ + istate]);
			}
		}
	}

	return rhs_;
}

const Vec& Zone::calculateSystemRHS()
{
	resetRHSImplicit();
	if (visc_flux_ == true) resetAuxiliary();
	PetscReal* rhs;
	VecGetArray(implicit_rhs_, &rhs);
	const std::vector<int_t>& cell_petsc_index = grid_->getCellPetscIndex();
	const PetscInt petsc_index_start = *std::min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells_);

	// periodic boundary sweep
	const std::vector<int_t>& num_peribdry_quad_points = grid_->getNumPeriBdryQuadPoints();
	const std::vector<int_t>& peribdry_owner_cell = grid_->getPeriBdryOwnerCell();
	const std::vector<int_t>& peribdry_neighbor_cell = grid_->getPeriBdryNeighborCell();
	const std::vector<std::vector<real_t>>& peribdry_owner_basis_value = grid_->getPeriBdryOwnerBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_basis_value = grid_->getPeriBdryNeighborBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_owner_div_basis_value = grid_->getPeriBdryOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_div_basis_value = grid_->getPeriBdryNeighborDivBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_normals = grid_->getPeriBdryNormals();
	const std::vector<std::vector<real_t>>& peribdry_owner_coefficients = grid_->getPeriBdryOwnerCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_coefficients = grid_->getPeriBdryNeighborCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_owner_auxiliary_coefficients = grid_->getPeriBdryOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_auxiliary_coefficients = grid_->getPeriBdryNeighborAuxiliaryCoefficients();

	for (int_t ibdry = 0; ibdry < num_peribdries_; ibdry++) {
		const int_t& num_quad_points = num_peribdry_quad_points[ibdry];
		const int_t& owner_cell = peribdry_owner_cell[ibdry];
		const int_t& neighbor_cell = peribdry_neighbor_cell[ibdry];

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					neighbor_solution[ipoint*num_states_ + istate] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution_[neighbor_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux = equation_->calNumConvFlux(owner_solution, neighbor_solution, peribdry_normals[ibdry]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] -= peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];

		if (visc_flux_ == false) continue;

		std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*peribdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> neighbor_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*peribdry_neighbor_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[owner_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> neighbor_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_neighbor_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[neighbor_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		const std::vector<real_t> visc_flux = equation_->calNumViscFlux(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, peribdry_normals[ibdry]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] += peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];
	}

	// boundary sweep
	const std::vector<int_t>& num_bdry_quad_points = grid_->getNumBdryQuadPoints();
	const std::vector<int_t>& bdry_owner_cell = grid_->getBdryOwnerCell();
	const std::vector<std::vector<real_t>>& bdry_owner_basis_value = grid_->getBdryOwnerBasisValue();
	const std::vector<std::vector<real_t>>& bdry_owner_div_basis_value = grid_->getBdryOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& bdry_normals = grid_->getBdryNormals();
	const std::vector<std::vector<real_t>>& bdry_owner_coefficients = grid_->getBdryOwnerCoefficients();
	const std::vector<std::vector<real_t>>& bdry_owner_auxiliary_coefficients = grid_->getBdryOwnerAuxiliaryCoefficients();

	for (int_t ibdry = 0; ibdry < num_bdries_; ibdry++) {
		const int_t& num_quad_points = num_bdry_quad_points[ibdry];
		const int_t& owner_cell = bdry_owner_cell[ibdry];

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += bdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux = equation_->calNumBdryConvFlux(owner_solution, bdry_normals[ibdry], ibdry);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate*num_basis_ + ibasis] -= bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];

		if (visc_flux_ == false) continue;

		std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += bdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution = equation_->calNumBdrySolution(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);

		std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*bdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += bdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		const std::vector<real_t> visc_flux = equation_->calNumBdryViscFlux(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate*num_basis_ + ibasis] += bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];
	}

	// face sweep
	const std::vector<int_t>& num_face_quad_points = grid_->getNumFaceQuadPoints();
	const std::vector<int_t>& face_owner_cell = grid_->getFaceOwnerCell();
	const std::vector<int_t>& face_neighbor_cell = grid_->getFaceNeighborCell();
	const std::vector<std::vector<real_t>>& face_owner_basis_value = grid_->getFaceOwnerBasisValue();
	const std::vector<std::vector<real_t>>& face_neighbor_basis_value = grid_->getFaceNeighborBasisValue();
	const std::vector<std::vector<real_t>>& face_owner_div_basis_value = grid_->getFaceOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& face_neighbor_div_basis_value = grid_->getFaceNeighborDivBasisValue();
	const std::vector<std::vector<real_t>>& face_normals = grid_->getFaceNormals();
	const std::vector<std::vector<real_t>>& face_owner_coefficients = grid_->getFaceOwnerCoefficients();
	const std::vector<std::vector<real_t>>& face_neighbor_coefficients = grid_->getFaceNeighborCoefficients();
	const std::vector<std::vector<real_t>>& face_owner_auxiliary_coefficients = grid_->getFaceOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& face_neighbor_auxiliary_coefficients = grid_->getFaceNeighborAuxiliaryCoefficients();
	
	for (int_t iface = 0; iface < num_faces_; iface++) {
		const int_t& num_quad_points = num_face_quad_points[iface];
		const int_t& owner_cell = face_owner_cell[iface];
		const int_t& neighbor_cell = face_neighbor_cell[iface];

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					neighbor_solution[ipoint*num_states_ + istate] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * solution_[neighbor_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux = equation_->calNumConvFlux(owner_solution, neighbor_solution, face_normals[iface]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++) {
			for (int_t istate = 0; istate < num_states_; istate++) {
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++) {
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate*num_basis_ + ibasis] -= face_owner_coefficients[iface][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];
					if (neighbor_cell < num_cells_)
					rhs[(cell_petsc_index[neighbor_cell] - petsc_index_start)*num_states_*num_basis_ + istate*num_basis_ + ibasis] += face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];
				}
			}
		}

		if (visc_flux_ == false) continue;

		std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*face_owner_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> neighbor_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*face_neighbor_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_owner_div_basis_value[iface][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[owner_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> neighbor_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_neighbor_div_basis_value[iface][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[neighbor_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		const std::vector<real_t> visc_flux = equation_->calNumViscFlux(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, face_normals[iface]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++) {
			for (int_t istate = 0; istate < num_states_; istate++) {
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++) {
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate*num_basis_ + ibasis] += face_owner_coefficients[iface][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];
					if(neighbor_cell < num_cells_)
					rhs[(cell_petsc_index[neighbor_cell] - petsc_index_start)*num_states_*num_basis_ + istate*num_basis_ + ibasis] -= face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];
				}
			}
		}

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[neighbor_cell][ivar] += neighbor_auxiliary[ivar];
	}
	
	// cell sweep
	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();
	const std::vector<std::vector<real_t>>& cell_div_basis_value = grid_->getCellDivBasisValue();
	const std::vector<std::vector<real_t>>& cell_coefficients = grid_->getCellCoefficients();
	const std::vector<std::vector<real_t>>& cell_source_coefficients = grid_->getCellSourceCoefficients();
	const std::vector<std::vector<real_t>>& wall_distance = grid_->getWallDistances();
	
	for (int_t icell = 0; icell < num_cells_; icell++) {
		const int_t& num_quad_points = num_cell_quad_points[icell];

		std::vector<real_t> solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					solution[ipoint*num_states_ + istate] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * solution_[icell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux = equation_->calComConvFlux(solution);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						rhs[(cell_petsc_index[icell] - petsc_index_start)*num_states_*num_basis_ + istate*num_basis_ + ibasis] += (cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * conv_flux[ipoint*num_states_*dimension_ + istate * dimension_ + idim]);

		if (visc_flux_ == true) {

			std::vector<real_t> div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += cell_div_basis_value[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[icell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * auxiliary_[icell][istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			const std::vector<real_t> visc_flux = equation_->calComViscFlux(solution, div_solution);

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							rhs[(cell_petsc_index[icell] - petsc_index_start)*num_states_*num_basis_ + istate*num_basis_ + ibasis] -= (cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * visc_flux[ipoint*num_states_*dimension_ + istate * dimension_ + idim]);

			if (source_ == true) {
				const std::vector<real_t> source = equation_->calSource(solution, div_solution, wall_distance[icell]);

				for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
					for (int_t istate = 0; istate < num_states_; istate++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							rhs[(cell_petsc_index[icell] - petsc_index_start)*num_states_*num_basis_ + istate*num_basis_ + ibasis] += (cell_source_coefficients[icell][ipoint*num_basis_ + ibasis] * source[ipoint*num_states_ + istate]);
			}
		}
	}
	VecRestoreArray(implicit_rhs_, &rhs);
	return implicit_rhs_;
}

const Vec& Zone::calculateInstantSystemRHS(const std::vector<std::vector<real_t> >& solution)
{
	resetRHSImplicit();
	if (visc_flux_ == true) resetAuxiliary();
	PetscReal* rhs;
	VecGetArray(implicit_rhs_, &rhs);
	const std::vector<int_t>& cell_petsc_index = grid_->getCellPetscIndex();
	const PetscInt petsc_index_start = *std::min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells_);

	// periodic boundary sweep
	const std::vector<int_t>& num_peribdry_quad_points = grid_->getNumPeriBdryQuadPoints();
	const std::vector<int_t>& peribdry_owner_cell = grid_->getPeriBdryOwnerCell();
	const std::vector<int_t>& peribdry_neighbor_cell = grid_->getPeriBdryNeighborCell();
	const std::vector<std::vector<real_t>>& peribdry_owner_basis_value = grid_->getPeriBdryOwnerBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_basis_value = grid_->getPeriBdryNeighborBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_owner_div_basis_value = grid_->getPeriBdryOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_div_basis_value = grid_->getPeriBdryNeighborDivBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_normals = grid_->getPeriBdryNormals();
	const std::vector<std::vector<real_t>>& peribdry_owner_coefficients = grid_->getPeriBdryOwnerCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_coefficients = grid_->getPeriBdryNeighborCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_owner_auxiliary_coefficients = grid_->getPeriBdryOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_auxiliary_coefficients = grid_->getPeriBdryNeighborAuxiliaryCoefficients();

	for (int_t ibdry = 0; ibdry < num_peribdries_; ibdry++) {
		const int_t& num_quad_points = num_peribdry_quad_points[ibdry];
		const int_t& owner_cell = peribdry_owner_cell[ibdry];
		const int_t& neighbor_cell = peribdry_neighbor_cell[ibdry];

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					neighbor_solution[ipoint*num_states_ + istate] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution[neighbor_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux = equation_->calNumConvFlux(owner_solution, neighbor_solution, peribdry_normals[ibdry]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] -= peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];

		if (visc_flux_ == false) continue;

		std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*peribdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> neighbor_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*peribdry_neighbor_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution[owner_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> neighbor_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_neighbor_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution[neighbor_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		const std::vector<real_t> visc_flux = equation_->calNumViscFlux(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, peribdry_normals[ibdry]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] += peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];
	}

	// boundary sweep
	const std::vector<int_t>& num_bdry_quad_points = grid_->getNumBdryQuadPoints();
	const std::vector<int_t>& bdry_owner_cell = grid_->getBdryOwnerCell();
	const std::vector<std::vector<real_t>>& bdry_owner_basis_value = grid_->getBdryOwnerBasisValue();
	const std::vector<std::vector<real_t>>& bdry_owner_div_basis_value = grid_->getBdryOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& bdry_normals = grid_->getBdryNormals();
	const std::vector<std::vector<real_t>>& bdry_owner_coefficients = grid_->getBdryOwnerCoefficients();
	const std::vector<std::vector<real_t>>& bdry_owner_auxiliary_coefficients = grid_->getBdryOwnerAuxiliaryCoefficients();

	for (int_t ibdry = 0; ibdry < num_bdries_; ibdry++) {
		const int_t& num_quad_points = num_bdry_quad_points[ibdry];
		const int_t& owner_cell = bdry_owner_cell[ibdry];

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += bdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution[owner_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux = equation_->calNumBdryConvFlux(owner_solution, bdry_normals[ibdry], ibdry);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] -= bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];

		if (visc_flux_ == false) continue;

		std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += bdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution = equation_->calNumBdrySolution(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);

		std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*bdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += bdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		const std::vector<real_t> visc_flux = equation_->calNumBdryViscFlux(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] += bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];
	}

	// face sweep
	const std::vector<int_t>& num_face_quad_points = grid_->getNumFaceQuadPoints();
	const std::vector<int_t>& face_owner_cell = grid_->getFaceOwnerCell();
	const std::vector<int_t>& face_neighbor_cell = grid_->getFaceNeighborCell();
	const std::vector<std::vector<real_t>>& face_owner_basis_value = grid_->getFaceOwnerBasisValue();
	const std::vector<std::vector<real_t>>& face_neighbor_basis_value = grid_->getFaceNeighborBasisValue();
	const std::vector<std::vector<real_t>>& face_owner_div_basis_value = grid_->getFaceOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& face_neighbor_div_basis_value = grid_->getFaceNeighborDivBasisValue();
	const std::vector<std::vector<real_t>>& face_normals = grid_->getFaceNormals();
	const std::vector<std::vector<real_t>>& face_owner_coefficients = grid_->getFaceOwnerCoefficients();
	const std::vector<std::vector<real_t>>& face_neighbor_coefficients = grid_->getFaceNeighborCoefficients();
	const std::vector<std::vector<real_t>>& face_owner_auxiliary_coefficients = grid_->getFaceOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& face_neighbor_auxiliary_coefficients = grid_->getFaceNeighborAuxiliaryCoefficients();

	for (int_t iface = 0; iface < num_faces_; iface++) {
		const int_t& num_quad_points = num_face_quad_points[iface];
		const int_t& owner_cell = face_owner_cell[iface];
		const int_t& neighbor_cell = face_neighbor_cell[iface];

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * solution[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					neighbor_solution[ipoint*num_states_ + istate] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * solution[neighbor_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux = equation_->calNumConvFlux(owner_solution, neighbor_solution, face_normals[iface]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++) {
			for (int_t istate = 0; istate < num_states_; istate++) {
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++) {
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] -= face_owner_coefficients[iface][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];
					if (neighbor_cell < num_cells_)
						rhs[(cell_petsc_index[neighbor_cell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] += face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis] * conv_flux[ipoint*num_states_ + istate];
				}
			}
		}

		if (visc_flux_ == false) continue;

		std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*face_owner_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> neighbor_auxiliary(num_states_*dimension_*num_basis_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*face_neighbor_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_owner_div_basis_value[iface][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution[owner_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		std::vector<real_t> neighbor_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_neighbor_div_basis_value[iface][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution[neighbor_cell][istate*num_basis_ + ibasis];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t idim = 0; idim < dimension_; idim++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

		const std::vector<real_t> visc_flux = equation_->calNumViscFlux(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, face_normals[iface]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++) {
			for (int_t istate = 0; istate < num_states_; istate++) {
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++) {
					rhs[(cell_petsc_index[owner_cell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] += face_owner_coefficients[iface][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];
					if (neighbor_cell < num_cells_)
						rhs[(cell_petsc_index[neighbor_cell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] -= face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis] * visc_flux[ipoint*num_states_ + istate];
				}
			}
		}

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];

		for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
			auxiliary_[neighbor_cell][ivar] += neighbor_auxiliary[ivar];
	}

	// cell sweep
	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();
	const std::vector<std::vector<real_t>>& cell_div_basis_value = grid_->getCellDivBasisValue();
	const std::vector<std::vector<real_t>>& cell_coefficients = grid_->getCellCoefficients();
	const std::vector<std::vector<real_t>>& cell_source_coefficients = grid_->getCellSourceCoefficients();
	const std::vector<std::vector<real_t>>& wall_distance = grid_->getWallDistances();

	for (int_t icell = 0; icell < num_cells_; icell++) {
		const int_t& num_quad_points = num_cell_quad_points[icell];

		std::vector<real_t> cell_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					cell_solution[ipoint*num_states_ + istate] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * solution[icell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux = equation_->calComConvFlux(cell_solution);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t idim = 0; idim < dimension_; idim++)
						rhs[(cell_petsc_index[icell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] += (cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * conv_flux[ipoint*num_states_*dimension_ + istate * dimension_ + idim]);

		if (visc_flux_ == true) {

			std::vector<real_t> div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += cell_div_basis_value[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution[icell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * auxiliary_[icell][istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			const std::vector<real_t> visc_flux = equation_->calComViscFlux(cell_solution, div_solution);

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							rhs[(cell_petsc_index[icell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] -= (cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * visc_flux[ipoint*num_states_*dimension_ + istate * dimension_ + idim]);

			if (source_ == true) {
				const std::vector<real_t> source = equation_->calSource(cell_solution, div_solution, wall_distance[icell]);

				for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
					for (int_t istate = 0; istate < num_states_; istate++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							rhs[(cell_petsc_index[icell] - petsc_index_start)*num_states_*num_basis_ + istate * num_basis_ + ibasis] += (cell_source_coefficients[icell][ipoint*num_basis_ + ibasis] * source[ipoint*num_states_ + istate]);
			}
		}
	}
	VecRestoreArray(implicit_rhs_, &rhs);
	return implicit_rhs_;
}

const Mat& Zone::calculateSystemMatrix(const real_t diagonal_factor, const real_t jacobian_factor)
{
	resetSystemMatrix();
	if (visc_flux_ == true) resetAuxiliaryImplicit();
	const PetscInt blocksize = num_states_ * num_basis_;
	const std::vector<int_t>& cell_petsc_index = grid_->getCellPetscIndex();
	std::vector<int_t> idxm(blocksize), idxn(blocksize);
	std::vector<real_t> owner_component_owner_values(blocksize*blocksize), owner_component_neighbor_values(blocksize*blocksize);
	std::vector<real_t> neighbor_component_owner_values(blocksize*blocksize), neighbor_component_neighbor_values(blocksize*blocksize);

	// periodic boundary sweep
	const std::vector<int_t>& num_peribdry_quad_points = grid_->getNumPeriBdryQuadPoints();
	const std::vector<int_t>& peribdry_owner_cell = grid_->getPeriBdryOwnerCell();
	const std::vector<int_t>& peribdry_neighbor_cell = grid_->getPeriBdryNeighborCell();
	const std::vector<std::vector<real_t>>& peribdry_owner_basis_value = grid_->getPeriBdryOwnerBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_basis_value = grid_->getPeriBdryNeighborBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_owner_div_basis_value = grid_->getPeriBdryOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_div_basis_value = grid_->getPeriBdryNeighborDivBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_normals = grid_->getPeriBdryNormals();
	const std::vector<std::vector<real_t>>& peribdry_owner_coefficients = grid_->getPeriBdryOwnerCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_coefficients = grid_->getPeriBdryNeighborCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_owner_auxiliary_coefficients = grid_->getPeriBdryOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_auxiliary_coefficients = grid_->getPeriBdryNeighborAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_owner_to_owner_auxiliary_jacobian = grid_->getPeriBdryOwnerToOwnerAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& peribdry_owner_to_neighbor_auxiliary_jacobian = grid_->getPeriBdryOwnerToNeighborAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_to_owner_auxiliary_jacobian = grid_->getPeriBdryNeighborToOwnerAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_to_neighbor_auxiliary_jacobian = grid_->getPeriBdryNeighborToNeighborAuxiliaryJacobian();
	
	for (int_t ibdry = 0; ibdry < num_peribdries_; ibdry++)
	{
		const int_t& num_quad_points = num_peribdry_quad_points[ibdry];
		const int_t& owner_cell = peribdry_owner_cell[ibdry];
		const int_t& neighbor_cell = peribdry_neighbor_cell[ibdry];
		int_t irow, icolumn;

		for (int_t i = 0; i < blocksize; i++)
		{
			idxm[i] = cell_petsc_index[owner_cell] * blocksize + i;
			idxn[i] = cell_petsc_index[neighbor_cell] * blocksize + i;
		}
		for (int_t i = 0; i < blocksize*blocksize; i++)
		{
			owner_component_owner_values[i] = 0.0;
			owner_component_neighbor_values[i] = 0.0;
		}

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					neighbor_solution[ipoint*num_states_ + istate] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution_[neighbor_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux_owner_jacobian = equation_->calNumConvFluxOwnerJacobian(owner_solution, neighbor_solution, peribdry_normals[ibdry]);
		const std::vector<real_t> conv_flux_neighbor_jacobian = equation_->calNumConvFluxNeighborJacobian(owner_solution, neighbor_solution, peribdry_normals[ibdry]);
		
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		{
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t jstate = 0; jstate < num_states_; jstate++)
						for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						{
							irow = ibasis + num_basis_ * istate;
							icolumn = jbasis + num_basis_ * jstate;
							owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
								* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
								* conv_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + jbasis]; // -
							owner_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor 
								* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
								* conv_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + jbasis]; // +
						}
		}

		if (visc_flux_ == true)
		{
			std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*peribdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> neighbor_auxiliary(num_states_*dimension_*num_basis_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*peribdry_neighbor_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[owner_cell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> neighbor_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_neighbor_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[neighbor_cell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			const std::vector<real_t> visc_flux_owner_jacobian = equation_->calNumViscFluxOwnerJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, peribdry_normals[ibdry]);
			const std::vector<real_t> visc_flux_neighbor_jacobian = equation_->calNumViscFluxNeighborJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, peribdry_normals[ibdry]);
			const std::vector<real_t> visc_flux_owner_grad_jacobian = equation_->calNumViscFluxOwnerGradJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, peribdry_normals[ibdry]);
			const std::vector<real_t> visc_flux_neighbor_grad_jacobian = equation_->calNumViscFluxNeighborGradJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, peribdry_normals[ibdry]);

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			{
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t jstate = 0; jstate < num_states_; jstate++)
							for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
							{
								irow = ibasis + num_basis_ * istate;
								icolumn = jbasis + num_basis_ * jstate;

								// Flux Jacobian * Basis
								owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
									* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
									* visc_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + jbasis]; //-
								owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
									* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
									* visc_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + jbasis]; //+

								for (int_t idim = 0; idim < dimension_; idim++)
								{
									// Flux Gradient Jacobian * div_Basis
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_neighbor_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];

									// Flux Gradient Jacobian * Auxiliary Jacobian * Basis
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_owner_to_owner_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_neighbor_to_owner_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_neighbor_to_neighbor_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_owner_to_neighbor_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
								}
							}
			}

			for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
				auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];
		}
		MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxm[0], &owner_component_owner_values[0], ADD_VALUES);
		MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxn[0], &owner_component_neighbor_values[0], ADD_VALUES);
	}
	
	// boundary sweep
	const std::vector<int_t>& num_bdry_quad_points = grid_->getNumBdryQuadPoints();
	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	const std::vector<int_t>& bdry_owner_cell = grid_->getBdryOwnerCell();
	const std::vector<std::vector<real_t>>& bdry_owner_basis_value = grid_->getBdryOwnerBasisValue();
	const std::vector<std::vector<real_t>>& bdry_owner_div_basis_value = grid_->getBdryOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();
	const std::vector<std::vector<real_t>>& cell_div_basis_value = grid_->getCellDivBasisValue();
	const std::vector<std::vector<real_t>>& bdry_normals = grid_->getBdryNormals();
	const std::vector<std::vector<real_t>>& bdry_owner_coefficients = grid_->getBdryOwnerCoefficients();
	const std::vector<std::vector<real_t>>& bdry_owner_auxiliary_coefficients = grid_->getBdryOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& bdry_auxiliary_jacobian_coefficients = grid_->getBdryAuxiliaryJacobianCoefficients();
	const std::vector<std::vector<real_t>>& bdry_cell_auxiliary_jacobian_coefficients = grid_->getBdryCellAuxiliaryJacobianCoefficients();
	
	for (int_t ibdry = 0; ibdry < num_bdries_; ibdry++)
	{
		const int_t& num_quad_points = num_bdry_quad_points[ibdry];
		const int_t& owner_cell = bdry_owner_cell[ibdry];
		int_t irow, icolumn;

		for (int_t i = 0; i < blocksize; i++)
			idxm[i] = cell_petsc_index[owner_cell] * blocksize + i;

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += bdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux_jacobian = equation_->calNumBdryConvFluxJacobian(owner_solution, bdry_normals[ibdry], ibdry);
			
		for (int_t i = 0; i < blocksize*blocksize; i++)
			owner_component_owner_values[i] = 0;
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t jstate = 0; jstate < num_states_; jstate++)
						for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						{
							irow = ibasis + num_basis_ * istate;
							icolumn = jbasis + num_basis_ * jstate;
							owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor 
								* bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
								* conv_flux_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* bdry_owner_basis_value[ibdry][ipoint*num_basis_ + jbasis]; //-							
						}

		if (visc_flux_ == true)
		{
			std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += bdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[owner_cell][istate*num_basis_ + ibasis];

			std::vector<real_t> neighbor_solution = equation_->calNumBdrySolution(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);
			std::vector<real_t> solution_jacobian = equation_->calNumBdrySolutionJacobian(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);

			std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*bdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += bdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			const std::vector<real_t> visc_flux_jacobian = equation_->calNumBdryViscFluxJacobian(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);
			const std::vector<real_t> visc_flux_grad_jacobian = equation_->calNumBdryViscFluxGradJacobian(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);

			for (int_t i = 0; i < blocksize; i++)
				idxm[i] = cell_petsc_index[owner_cell] * blocksize + i;

			for (int_t i = 0; i < blocksize*blocksize; i++)
				owner_component_owner_values[i] = 0;
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t jstate = 0; jstate < num_states_; jstate++)
							for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
							{
								irow = ibasis + num_basis_ * istate;
								icolumn = jbasis + num_basis_ * jstate;

								// Flux Jacobian * Basis
								owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
									* bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
									* visc_flux_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* bdry_owner_basis_value[ibdry][ipoint*num_basis_ + jbasis]; //-

								for (int_t idim = 0; idim < dimension_; idim++)
								{
									// Flux Gradient Jacobian * div_Basis
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate*dimension_ + idim]
										* bdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + jbasis*dimension_ + idim];


									// Flux Gradient Jacobian * Auxiliary Jacobian * Basis
									//owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
									//	* bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
									//	* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
									//	* bdry_owner_to_owner_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									for(int_t jpoint = 0; jpoint < num_quad_points; jpoint++)
									for (int_t kstate = 0; kstate < 4; kstate++)
									{
										owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
											* bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
											* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + kstate * dimension_ + idim]
											* solution_jacobian[jpoint*num_states_*num_states_ + kstate * num_states_ + jstate] * bdry_auxiliary_jacobian_coefficients[ibdry][ipoint*num_basis_*dimension_*num_quad_points + jbasis * dimension_*num_quad_points + idim * num_quad_points + jpoint];
									}
										
								}
							}

			for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
				auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];

			for (int_t ipoint = 0; ipoint < num_cell_quad_points[owner_cell]; ipoint++) // for implicit
			for (int_t istate = 0; istate < num_states_; istate++)
			for (int_t jstate = 0; jstate < num_states_; jstate++)
			for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			for (int_t idim = 0; idim < dimension_; idim++)
			for (int_t jpoint = 0; jpoint < num_quad_points; jpoint++)
				{
				auxiliary_jacobian_[owner_cell][ipoint*num_states_*num_states_*num_basis_*dimension_ + istate * num_states_*num_basis_*dimension_ + jstate * num_basis_*dimension_ + ibasis * dimension_ + idim]
					+= solution_jacobian[jpoint*num_states_*num_states_ + istate * num_states_ + jstate] * bdry_cell_auxiliary_jacobian_coefficients[ibdry][ipoint*num_basis_*dimension_*num_quad_points + ibasis * dimension_*num_quad_points + idim*num_quad_points + jpoint];
				}				
		}
		MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxm[0], &owner_component_owner_values[0], ADD_VALUES);
	}
	
	// face sweep
	const std::vector<int_t>& num_face_quad_points = grid_->getNumFaceQuadPoints();
	const std::vector<int_t>& face_owner_cell = grid_->getFaceOwnerCell();
	const std::vector<int_t>& face_neighbor_cell = grid_->getFaceNeighborCell();
	const std::vector<std::vector<real_t>>& face_owner_basis_value = grid_->getFaceOwnerBasisValue();
	const std::vector<std::vector<real_t>>& face_neighbor_basis_value = grid_->getFaceNeighborBasisValue();
	const std::vector<std::vector<real_t>>& face_owner_div_basis_value = grid_->getFaceOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& face_neighbor_div_basis_value = grid_->getFaceNeighborDivBasisValue();
	const std::vector<std::vector<real_t>>& face_normals = grid_->getFaceNormals();
	const std::vector<std::vector<real_t>>& face_owner_coefficients = grid_->getFaceOwnerCoefficients();
	const std::vector<std::vector<real_t>>& face_neighbor_coefficients = grid_->getFaceNeighborCoefficients();
	const std::vector<std::vector<real_t>>& face_owner_auxiliary_coefficients = grid_->getFaceOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& face_neighbor_auxiliary_coefficients = grid_->getFaceNeighborAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& face_owner_to_owner_auxiliary_jacobian = grid_->getFaceOwnerToOwnerAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& face_owner_to_neighbor_auxiliary_jacobian = grid_->getFaceOwnerToNeighborAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& face_neighbor_to_owner_auxiliary_jacobian = grid_->getFaceNeighborToOwnerAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& face_neighbor_to_neighbor_auxiliary_jacobian = grid_->getFaceNeighborToNeighborAuxiliaryJacobian();

	for (int_t iface = 0; iface < num_faces_; iface++)
	{
		const int_t& num_quad_points = num_face_quad_points[iface];
		const int_t& owner_cell = face_owner_cell[iface];
		const int_t& neighbor_cell = face_neighbor_cell[iface];
		int_t irow, icolumn;

		for (int_t i = 0; i < blocksize; i++)
		{
			idxm[i] = cell_petsc_index[owner_cell] * blocksize + i;
			idxn[i] = cell_petsc_index[neighbor_cell] * blocksize + i;
		}
		for (int_t i = 0; i < blocksize*blocksize; i++)
		{
			owner_component_owner_values[i] = 0.0;
			owner_component_neighbor_values[i] = 0.0;
			neighbor_component_neighbor_values[i] = 0.0;
			neighbor_component_owner_values[i] = 0.0;
		}

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					neighbor_solution[ipoint*num_states_ + istate] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * solution_[neighbor_cell][istate*num_basis_ + ibasis];
		
		const std::vector<real_t> conv_flux_owner_jacobian = equation_->calNumConvFluxOwnerJacobian(owner_solution, neighbor_solution, face_normals[iface]);
		const std::vector<real_t> conv_flux_neighbor_jacobian = equation_->calNumConvFluxNeighborJacobian(owner_solution, neighbor_solution, face_normals[iface]);
		
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t jstate = 0; jstate < num_states_; jstate++)
						for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						{
							irow = ibasis + num_basis_ * istate;
							icolumn = jbasis + num_basis_ * jstate;
							owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
								* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
								* conv_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* face_owner_basis_value[iface][ipoint*num_basis_ + jbasis]; //-
							owner_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
								* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
								* conv_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* face_neighbor_basis_value[iface][ipoint*num_basis_ + jbasis]; //+
							neighbor_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
								* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
								* conv_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* face_neighbor_basis_value[iface][ipoint*num_basis_ + jbasis]; //+
							neighbor_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
								* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
								* conv_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* face_owner_basis_value[iface][ipoint*num_basis_ + jbasis]; //-
						}

		if (visc_flux_ == true)
		{
			std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*face_owner_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> neighbor_auxiliary(num_states_*dimension_*num_basis_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*face_neighbor_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_owner_div_basis_value[iface][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[owner_cell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> neighbor_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_neighbor_div_basis_value[iface][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[neighbor_cell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			const std::vector<real_t> visc_flux_owner_jacobian = equation_->calNumViscFluxOwnerJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, face_normals[iface]);
			const std::vector<real_t> visc_flux_neighbor_jacobian = equation_->calNumViscFluxNeighborJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, face_normals[iface]);
			const std::vector<real_t> visc_flux_owner_grad_jacobian = equation_->calNumViscFluxOwnerGradJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, face_normals[iface]);
			const std::vector<real_t> visc_flux_neighbor_grad_jacobian = equation_->calNumViscFluxNeighborGradJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, face_normals[iface]);

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t jstate = 0; jstate < num_states_; jstate++)
							for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
							{
								irow = ibasis + num_basis_ * istate;
								icolumn = jbasis + num_basis_ * jstate;

								// Flux Jacobian * Basis
								owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
									* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
									* visc_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* face_owner_basis_value[iface][ipoint*num_basis_ + jbasis];
								owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
									* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
									* visc_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* face_neighbor_basis_value[iface][ipoint*num_basis_ + jbasis];
								neighbor_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
									* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
									* visc_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* face_neighbor_basis_value[iface][ipoint*num_basis_ + jbasis];
								neighbor_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
									* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
									* visc_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* face_owner_basis_value[iface][ipoint*num_basis_ + jbasis];

								for (int_t idim = 0; idim < dimension_; idim++)
								{
									// Gradient Jacobian * div_Basis
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate*dimension_ + idim]
										* face_owner_div_basis_value[iface][ipoint*num_basis_*dimension_ + jbasis*dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_div_basis_value[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_div_basis_value[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_owner_div_basis_value[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];

									// Gradient Jacobian * Auxiliary Jacobian * Basis
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_owner_to_owner_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_to_owner_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_owner_to_neighbor_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_to_neighbor_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_owner_to_neighbor_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_to_neighbor_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_owner_to_owner_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_to_owner_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
								}
							}
			for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
				auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];

			for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
				auxiliary_[neighbor_cell][ivar] += neighbor_auxiliary[ivar];
		}		
		if (owner_cell < num_cells_)
		{
			MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxm[0], &owner_component_owner_values[0], ADD_VALUES);
			MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxn[0], &owner_component_neighbor_values[0], ADD_VALUES);
		}
		if (neighbor_cell < num_cells_)
		{
			MatSetValues(implicit_operator_, blocksize, &idxn[0], blocksize, &idxn[0], &neighbor_component_neighbor_values[0], ADD_VALUES);
			MatSetValues(implicit_operator_, blocksize, &idxn[0], blocksize, &idxm[0], &neighbor_component_owner_values[0], ADD_VALUES);
		}
	}
	
	// cell sweep
	//const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	//const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();
	//const std::vector<std::vector<real_t>>& cell_div_basis_value = grid_->getCellDivBasisValue();
	const std::vector<std::vector<real_t>>& cell_coefficients = grid_->getCellCoefficients();
	const std::vector<std::vector<real_t>>& cell_source_coefficients = grid_->getCellSourceCoefficients();
	const std::vector<std::vector<real_t>>& wall_distance = grid_->getWallDistances();
	const std::vector<std::vector<real_t>>& cell_owner_to_owner_auxiliary_jacobian = grid_->getCellOwnerToOwnerAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& cell_owner_to_neighbor_auxiliary_jacobian = grid_->getCellOwnerToNeighborAuxiliaryJacobian();
	const std::vector<std::vector<int_t>>& cell_to_cells = grid_->getCellToCells();
	
	for (int_t icell = 0; icell < num_cells_; icell++)
	{
		const int_t& num_quad_points = num_cell_quad_points[icell];
		int_t irow, icolumn;
		for (int_t i = 0; i < blocksize; i++)
			idxm[i] = cell_petsc_index[icell] * blocksize + i;		
		for (int_t i = 0; i < blocksize*blocksize; i++)
			owner_component_owner_values[i] = 0;

		std::vector<real_t> solution(num_quad_points*num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					solution[ipoint*num_states_ + istate] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * solution_[icell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux_jacobian = equation_->calComConvFluxJacobian(solution);
		
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t jstate = 0; jstate < num_states_; jstate++)
						for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
							for(int_t idim = 0; idim < dimension_; idim++)
							{
								irow = ibasis + num_basis_ * istate;
								icolumn = jbasis + num_basis_ * jstate;
								owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
									* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
									* conv_flux_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
									* cell_basis_value[icell][ipoint*num_basis_ + jbasis];
							}

		if (visc_flux_ == true)
		{
			std::vector<real_t> div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += cell_div_basis_value[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * solution_[icell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * auxiliary_[icell][istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			const std::vector<real_t> visc_flux_jacobian = equation_->calComViscFluxJacobian(solution, div_solution);
			const std::vector<real_t> visc_flux_grad_jacobian = equation_->calComViscFluxGradJacobian(solution, div_solution);
			
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t jstate = 0; jstate < num_states_; jstate++)
							for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
								for (int_t idim = 0; idim < dimension_; idim++)
								{
									irow = ibasis + num_basis_ * istate;
									icolumn = jbasis + num_basis_ * jstate;

									// Flux Jacobian * Basis
									owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
										* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
										* visc_flux_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* cell_basis_value[icell][ipoint*num_basis_ + jbasis]; //+

									for (int_t jdim = 0; jdim < dimension_; jdim++)
									{
										// Gradient Jacobian * div_Basis
										owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
											* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
											* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_*dimension_ + istate * num_states_*dimension_*dimension_ + jstate * dimension_*dimension_ + idim * dimension_ + jdim]
											* cell_div_basis_value[icell][ipoint*num_basis_*dimension_ + jbasis * dimension_ + jdim];

										// Gradient Jacobian * Auxiliary Jacobian * Basis
										owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
											* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
											* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_*dimension_ + istate * num_states_*dimension_*dimension_ + jstate * dimension_*dimension_ + idim * dimension_ + jdim]
											* cell_owner_to_owner_auxiliary_jacobian[icell][ipoint*num_basis_*dimension_ + jbasis * dimension_ + jdim];

										for (int_t kstate = 0; kstate < num_states_; kstate++)
											owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
											* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
											* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_*dimension_ + istate * num_states_*dimension_*dimension_ + kstate * dimension_*dimension_ + idim * dimension_ + jdim]
											* auxiliary_jacobian_[icell][ipoint*num_states_*num_states_*num_basis_*dimension_ + kstate * num_states_*num_basis_*dimension_ + jstate * num_basis_*dimension_ + jbasis * dimension_ + jdim];
									}
								}

			for (int_t jneighbor = 0; jneighbor < cell_to_cells[icell].size(); jneighbor++)
			{
				// Initializing
				const int_t jcell = cell_to_cells[icell][jneighbor];
				for (int_t i = 0; i < blocksize; i++)
					idxn[i] = cell_petsc_index[jcell] * blocksize + i;
				for (int_t i = 0; i < blocksize*blocksize; i++)
					owner_component_neighbor_values[i] = 0;

				for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
					for (int_t istate = 0; istate < num_states_; istate++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							for (int_t jstate = 0; jstate < num_states_; jstate++)
								for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
								{
									irow = ibasis + num_basis_ * istate;
									icolumn = jbasis + num_basis_ * jstate;

									for (int_t idim = 0; idim < dimension_; idim++)
										for (int_t jdim = 0; jdim < dimension_; jdim++)
										{
											// Gradient Jacobian * Auxiliary Jacobian * Basis
											owner_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
												* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
												* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_*dimension_ + istate * num_states_*dimension_*dimension_ + jstate * dimension_*dimension_ + idim * dimension_ + jdim]
												* cell_owner_to_neighbor_auxiliary_jacobian[icell][jneighbor*num_quad_points*num_basis_*dimension_ + ipoint * num_basis_*dimension_ + jbasis * dimension_ + jdim];
										}
								}

				MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxn[0], &owner_component_neighbor_values[0], ADD_VALUES);
			}

			if (source_ == true)
			{
				// source term
			}
		}

		MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxm[0], &owner_component_owner_values[0], ADD_VALUES);

		for (int_t i = 0; i < blocksize; i++)
			MatSetValues(implicit_operator_, 1, &idxm[i], 1, &idxm[i], &diagonal_factor, ADD_VALUES);
	}	

	MatAssemblyBegin(implicit_operator_, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(implicit_operator_, MAT_FINAL_ASSEMBLY);
	return implicit_operator_;
}

const Mat& Zone::calculateInstantSystemMatrix(const std::vector<std::vector<real_t> >& instant_solution, const real_t diagonal_factor, const real_t jacobian_factor)
{
	resetSystemMatrix();
	if (visc_flux_ == true) resetAuxiliaryImplicit();
	const PetscInt blocksize = num_states_ * num_basis_;
	const std::vector<int_t>& cell_petsc_index = grid_->getCellPetscIndex();
	std::vector<int_t> idxm(blocksize), idxn(blocksize);
	std::vector<real_t> owner_component_owner_values(blocksize*blocksize), owner_component_neighbor_values(blocksize*blocksize);
	std::vector<real_t> neighbor_component_owner_values(blocksize*blocksize), neighbor_component_neighbor_values(blocksize*blocksize);

	// periodic boundary sweep
	const std::vector<int_t>& num_peribdry_quad_points = grid_->getNumPeriBdryQuadPoints();
	const std::vector<int_t>& peribdry_owner_cell = grid_->getPeriBdryOwnerCell();
	const std::vector<int_t>& peribdry_neighbor_cell = grid_->getPeriBdryNeighborCell();
	const std::vector<std::vector<real_t>>& peribdry_owner_basis_value = grid_->getPeriBdryOwnerBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_basis_value = grid_->getPeriBdryNeighborBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_owner_div_basis_value = grid_->getPeriBdryOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_div_basis_value = grid_->getPeriBdryNeighborDivBasisValue();
	const std::vector<std::vector<real_t>>& peribdry_normals = grid_->getPeriBdryNormals();
	const std::vector<std::vector<real_t>>& peribdry_owner_coefficients = grid_->getPeriBdryOwnerCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_coefficients = grid_->getPeriBdryNeighborCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_owner_auxiliary_coefficients = grid_->getPeriBdryOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_auxiliary_coefficients = grid_->getPeriBdryNeighborAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& peribdry_owner_to_owner_auxiliary_jacobian = grid_->getPeriBdryOwnerToOwnerAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& peribdry_owner_to_neighbor_auxiliary_jacobian = grid_->getPeriBdryOwnerToNeighborAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_to_owner_auxiliary_jacobian = grid_->getPeriBdryNeighborToOwnerAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& peribdry_neighbor_to_neighbor_auxiliary_jacobian = grid_->getPeriBdryNeighborToNeighborAuxiliaryJacobian();

	for (int_t ibdry = 0; ibdry < num_peribdries_; ibdry++)
	{
		const int_t& num_quad_points = num_peribdry_quad_points[ibdry];
		const int_t& owner_cell = peribdry_owner_cell[ibdry];
		const int_t& neighbor_cell = peribdry_neighbor_cell[ibdry];
		int_t irow, icolumn;

		for (int_t i = 0; i < blocksize; i++)
		{
			idxm[i] = cell_petsc_index[owner_cell] * blocksize + i;
			idxn[i] = cell_petsc_index[neighbor_cell] * blocksize + i;
		}
		for (int_t i = 0; i < blocksize*blocksize; i++)
		{
			owner_component_owner_values[i] = 0.0;
			owner_component_neighbor_values[i] = 0.0;
		}

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * instant_solution[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					neighbor_solution[ipoint*num_states_ + istate] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * instant_solution[neighbor_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux_owner_jacobian = equation_->calNumConvFluxOwnerJacobian(owner_solution, neighbor_solution, peribdry_normals[ibdry]);
		const std::vector<real_t> conv_flux_neighbor_jacobian = equation_->calNumConvFluxNeighborJacobian(owner_solution, neighbor_solution, peribdry_normals[ibdry]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		{
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t jstate = 0; jstate < num_states_; jstate++)
						for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						{
							irow = ibasis + num_basis_ * istate;
							icolumn = jbasis + num_basis_ * jstate;
							owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
								* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
								* conv_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + jbasis]; // -
							owner_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
								* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
								* conv_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + jbasis]; // +
						}
		}

		if (visc_flux_ == true)
		{
			std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*peribdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> neighbor_auxiliary(num_states_*dimension_*num_basis_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*peribdry_neighbor_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * instant_solution[owner_cell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> neighbor_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_neighbor_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * instant_solution[neighbor_cell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + ibasis] * neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			const std::vector<real_t> visc_flux_owner_jacobian = equation_->calNumViscFluxOwnerJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, peribdry_normals[ibdry]);
			const std::vector<real_t> visc_flux_neighbor_jacobian = equation_->calNumViscFluxNeighborJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, peribdry_normals[ibdry]);
			const std::vector<real_t> visc_flux_owner_grad_jacobian = equation_->calNumViscFluxOwnerGradJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, peribdry_normals[ibdry]);
			const std::vector<real_t> visc_flux_neighbor_grad_jacobian = equation_->calNumViscFluxNeighborGradJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, peribdry_normals[ibdry]);

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			{
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t jstate = 0; jstate < num_states_; jstate++)
							for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
							{
								irow = ibasis + num_basis_ * istate;
								icolumn = jbasis + num_basis_ * jstate;

								// Flux Jacobian * Basis
								owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
									* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
									* visc_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + jbasis]; //-
								owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
									* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
									* visc_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* peribdry_neighbor_basis_value[ibdry][ipoint*num_basis_ + jbasis]; //+

								for (int_t idim = 0; idim < dimension_; idim++)
								{
									// Flux Gradient Jacobian * div_Basis
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_neighbor_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];

									// Flux Gradient Jacobian * Auxiliary Jacobian * Basis
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_owner_to_owner_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_neighbor_to_owner_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_neighbor_to_neighbor_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* peribdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_ * dimension_ + jstate * dimension_ + idim]
										* peribdry_owner_to_neighbor_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
								}
							}
			}

			for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
				auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];
		}
		MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxm[0], &owner_component_owner_values[0], ADD_VALUES);
		MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxn[0], &owner_component_neighbor_values[0], ADD_VALUES);
	}

	// boundary sweep
	const std::vector<int_t>& num_bdry_quad_points = grid_->getNumBdryQuadPoints();
	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	const std::vector<int_t>& bdry_owner_cell = grid_->getBdryOwnerCell();
	const std::vector<std::vector<real_t>>& bdry_owner_basis_value = grid_->getBdryOwnerBasisValue();
	const std::vector<std::vector<real_t>>& bdry_owner_div_basis_value = grid_->getBdryOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();
	const std::vector<std::vector<real_t>>& cell_div_basis_value = grid_->getCellDivBasisValue();
	const std::vector<std::vector<real_t>>& bdry_normals = grid_->getBdryNormals();
	const std::vector<std::vector<real_t>>& bdry_owner_coefficients = grid_->getBdryOwnerCoefficients();
	const std::vector<std::vector<real_t>>& bdry_owner_auxiliary_coefficients = grid_->getBdryOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& bdry_auxiliary_jacobian_coefficients = grid_->getBdryAuxiliaryJacobianCoefficients();
	const std::vector<std::vector<real_t>>& bdry_cell_auxiliary_jacobian_coefficients = grid_->getBdryCellAuxiliaryJacobianCoefficients();

	for (int_t ibdry = 0; ibdry < num_bdries_; ibdry++)
	{
		const int_t& num_quad_points = num_bdry_quad_points[ibdry];
		const int_t& owner_cell = bdry_owner_cell[ibdry];
		int_t irow, icolumn;

		for (int_t i = 0; i < blocksize; i++)
			idxm[i] = cell_petsc_index[owner_cell] * blocksize + i;

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += bdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * instant_solution[owner_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux_jacobian = equation_->calNumBdryConvFluxJacobian(owner_solution, bdry_normals[ibdry], ibdry);

		for (int_t i = 0; i < blocksize*blocksize; i++)
			owner_component_owner_values[i] = 0;
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t jstate = 0; jstate < num_states_; jstate++)
						for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						{
							irow = ibasis + num_basis_ * istate;
							icolumn = jbasis + num_basis_ * jstate;
							owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
								* bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
								* conv_flux_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* bdry_owner_basis_value[ibdry][ipoint*num_basis_ + jbasis]; //-							
						}

		if (visc_flux_ == true)
		{
			std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += bdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * instant_solution[owner_cell][istate*num_basis_ + ibasis];

			std::vector<real_t> neighbor_solution = equation_->calNumBdrySolution(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);
			std::vector<real_t> solution_jacobian = equation_->calNumBdrySolutionJacobian(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);

			std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*bdry_owner_auxiliary_coefficients[ibdry][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += bdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			const std::vector<real_t> visc_flux_jacobian = equation_->calNumBdryViscFluxJacobian(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);
			const std::vector<real_t> visc_flux_grad_jacobian = equation_->calNumBdryViscFluxGradJacobian(owner_solution, owner_div_solution, bdry_normals[ibdry], ibdry);

			for (int_t i = 0; i < blocksize; i++)
				idxm[i] = cell_petsc_index[owner_cell] * blocksize + i;

			for (int_t i = 0; i < blocksize*blocksize; i++)
				owner_component_owner_values[i] = 0;
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t jstate = 0; jstate < num_states_; jstate++)
							for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
							{
								irow = ibasis + num_basis_ * istate;
								icolumn = jbasis + num_basis_ * jstate;

								// Flux Jacobian * Basis
								owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
									* bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
									* visc_flux_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* bdry_owner_basis_value[ibdry][ipoint*num_basis_ + jbasis]; //-

								for (int_t idim = 0; idim < dimension_; idim++)
								{
									// Flux Gradient Jacobian * div_Basis
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
										* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* bdry_owner_div_basis_value[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];


									// Flux Gradient Jacobian * Auxiliary Jacobian * Basis
									//owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
									//	* bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
									//	* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
									//	* bdry_owner_to_owner_auxiliary_jacobian[ibdry][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									for (int_t jpoint = 0; jpoint < num_quad_points; jpoint++)
										for (int_t kstate = 0; kstate < 4; kstate++)
										{
											owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
												* bdry_owner_coefficients[ibdry][ipoint*num_basis_ + ibasis]
												* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + kstate * dimension_ + idim]
												* solution_jacobian[jpoint*num_states_*num_states_ + kstate * num_states_ + jstate] * bdry_auxiliary_jacobian_coefficients[ibdry][ipoint*num_basis_*dimension_*num_quad_points + jbasis * dimension_*num_quad_points + idim * num_quad_points + jpoint];
										}

								}
							}

			for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
				auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];

			for (int_t ipoint = 0; ipoint < num_cell_quad_points[owner_cell]; ipoint++) // for implicit
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t jstate = 0; jstate < num_states_; jstate++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							for (int_t idim = 0; idim < dimension_; idim++)
								for (int_t jpoint = 0; jpoint < num_quad_points; jpoint++)
								{
									auxiliary_jacobian_[owner_cell][ipoint*num_states_*num_states_*num_basis_*dimension_ + istate * num_states_*num_basis_*dimension_ + jstate * num_basis_*dimension_ + ibasis * dimension_ + idim]
										+= solution_jacobian[jpoint*num_states_*num_states_ + istate * num_states_ + jstate] * bdry_cell_auxiliary_jacobian_coefficients[ibdry][ipoint*num_basis_*dimension_*num_quad_points + ibasis * dimension_*num_quad_points + idim * num_quad_points + jpoint];
								}
		}
		MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxm[0], &owner_component_owner_values[0], ADD_VALUES);
	}

	// face sweep
	const std::vector<int_t>& num_face_quad_points = grid_->getNumFaceQuadPoints();
	const std::vector<int_t>& face_owner_cell = grid_->getFaceOwnerCell();
	const std::vector<int_t>& face_neighbor_cell = grid_->getFaceNeighborCell();
	const std::vector<std::vector<real_t>>& face_owner_basis_value = grid_->getFaceOwnerBasisValue();
	const std::vector<std::vector<real_t>>& face_neighbor_basis_value = grid_->getFaceNeighborBasisValue();
	const std::vector<std::vector<real_t>>& face_owner_div_basis_value = grid_->getFaceOwnerDivBasisValue();
	const std::vector<std::vector<real_t>>& face_neighbor_div_basis_value = grid_->getFaceNeighborDivBasisValue();
	const std::vector<std::vector<real_t>>& face_normals = grid_->getFaceNormals();
	const std::vector<std::vector<real_t>>& face_owner_coefficients = grid_->getFaceOwnerCoefficients();
	const std::vector<std::vector<real_t>>& face_neighbor_coefficients = grid_->getFaceNeighborCoefficients();
	const std::vector<std::vector<real_t>>& face_owner_auxiliary_coefficients = grid_->getFaceOwnerAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& face_neighbor_auxiliary_coefficients = grid_->getFaceNeighborAuxiliaryCoefficients();
	const std::vector<std::vector<real_t>>& face_owner_to_owner_auxiliary_jacobian = grid_->getFaceOwnerToOwnerAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& face_owner_to_neighbor_auxiliary_jacobian = grid_->getFaceOwnerToNeighborAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& face_neighbor_to_owner_auxiliary_jacobian = grid_->getFaceNeighborToOwnerAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& face_neighbor_to_neighbor_auxiliary_jacobian = grid_->getFaceNeighborToNeighborAuxiliaryJacobian();

	for (int_t iface = 0; iface < num_faces_; iface++)
	{
		const int_t& num_quad_points = num_face_quad_points[iface];
		const int_t& owner_cell = face_owner_cell[iface];
		const int_t& neighbor_cell = face_neighbor_cell[iface];
		int_t irow, icolumn;

		for (int_t i = 0; i < blocksize; i++)
		{
			idxm[i] = cell_petsc_index[owner_cell] * blocksize + i;
			idxn[i] = cell_petsc_index[neighbor_cell] * blocksize + i;
		}
		for (int_t i = 0; i < blocksize*blocksize; i++)
		{
			owner_component_owner_values[i] = 0.0;
			owner_component_neighbor_values[i] = 0.0;
			neighbor_component_neighbor_values[i] = 0.0;
			neighbor_component_owner_values[i] = 0.0;
		}

		std::vector<real_t> owner_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[ipoint*num_states_ + istate] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * instant_solution[owner_cell][istate*num_basis_ + ibasis];

		std::vector<real_t> neighbor_solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					neighbor_solution[ipoint*num_states_ + istate] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * instant_solution[neighbor_cell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux_owner_jacobian = equation_->calNumConvFluxOwnerJacobian(owner_solution, neighbor_solution, face_normals[iface]);
		const std::vector<real_t> conv_flux_neighbor_jacobian = equation_->calNumConvFluxNeighborJacobian(owner_solution, neighbor_solution, face_normals[iface]);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t jstate = 0; jstate < num_states_; jstate++)
						for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
						{
							irow = ibasis + num_basis_ * istate;
							icolumn = jbasis + num_basis_ * jstate;
							owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
								* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
								* conv_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* face_owner_basis_value[iface][ipoint*num_basis_ + jbasis]; //-
							owner_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
								* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
								* conv_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* face_neighbor_basis_value[iface][ipoint*num_basis_ + jbasis]; //+
							neighbor_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
								* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
								* conv_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* face_neighbor_basis_value[iface][ipoint*num_basis_ + jbasis]; //+
							neighbor_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
								* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
								* conv_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
								* face_owner_basis_value[iface][ipoint*num_basis_ + jbasis]; //-
						}

		if (visc_flux_ == true)
		{
			std::vector<real_t> owner_auxiliary(num_states_*dimension_*num_basis_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (neighbor_solution[ipoint*num_states_ + istate] - owner_solution[ipoint*num_states_ + istate])*face_owner_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> neighbor_auxiliary(num_states_*dimension_*num_basis_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis] += (owner_solution[ipoint*num_states_ + istate] - neighbor_solution[ipoint*num_states_ + istate])*face_neighbor_auxiliary_coefficients[iface][ipoint*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> owner_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_owner_div_basis_value[iface][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * instant_solution[owner_cell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							owner_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * owner_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			std::vector<real_t> neighbor_div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_neighbor_div_basis_value[iface][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * instant_solution[neighbor_cell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							neighbor_div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * neighbor_auxiliary[istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			const std::vector<real_t> visc_flux_owner_jacobian = equation_->calNumViscFluxOwnerJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, face_normals[iface]);
			const std::vector<real_t> visc_flux_neighbor_jacobian = equation_->calNumViscFluxNeighborJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, face_normals[iface]);
			const std::vector<real_t> visc_flux_owner_grad_jacobian = equation_->calNumViscFluxOwnerGradJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, face_normals[iface]);
			const std::vector<real_t> visc_flux_neighbor_grad_jacobian = equation_->calNumViscFluxNeighborGradJacobian(owner_solution, owner_div_solution, neighbor_solution, neighbor_div_solution, face_normals[iface]);

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t jstate = 0; jstate < num_states_; jstate++)
							for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
							{
								irow = ibasis + num_basis_ * istate;
								icolumn = jbasis + num_basis_ * jstate;

								// Flux Jacobian * Basis
								owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
									* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
									* visc_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* face_owner_basis_value[iface][ipoint*num_basis_ + jbasis];
								owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
									* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
									* visc_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* face_neighbor_basis_value[iface][ipoint*num_basis_ + jbasis];
								neighbor_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
									* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
									* visc_flux_neighbor_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* face_neighbor_basis_value[iface][ipoint*num_basis_ + jbasis];
								neighbor_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
									* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
									* visc_flux_owner_jacobian[ipoint*num_states_*num_states_ + istate * num_states_ + jstate]
									* face_owner_basis_value[iface][ipoint*num_basis_ + jbasis];

								for (int_t idim = 0; idim < dimension_; idim++)
								{
									// Gradient Jacobian * div_Basis
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_owner_div_basis_value[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_div_basis_value[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_div_basis_value[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_owner_div_basis_value[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];

									// Gradient Jacobian * Auxiliary Jacobian * Basis
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_owner_to_owner_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_to_owner_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_owner_to_neighbor_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									owner_component_neighbor_values[irow*blocksize + icolumn] -= jacobian_factor
										* face_owner_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_to_neighbor_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_owner_to_neighbor_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_to_neighbor_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_owner_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_owner_to_owner_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
									neighbor_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
										* face_neighbor_coefficients[iface][ipoint*num_basis_ + ibasis]
										* visc_flux_neighbor_grad_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* face_neighbor_to_owner_auxiliary_jacobian[iface][ipoint*num_basis_*dimension_ + jbasis * dimension_ + idim];
								}
							}
			for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
				auxiliary_[owner_cell][ivar] += owner_auxiliary[ivar];

			for (int_t ivar = 0; ivar < num_states_*dimension_*num_basis_; ivar++)
				auxiliary_[neighbor_cell][ivar] += neighbor_auxiliary[ivar];
		}
		if (owner_cell < num_cells_)
		{
			MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxm[0], &owner_component_owner_values[0], ADD_VALUES);
			MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxn[0], &owner_component_neighbor_values[0], ADD_VALUES);
		}
		if (neighbor_cell < num_cells_)
		{
			MatSetValues(implicit_operator_, blocksize, &idxn[0], blocksize, &idxn[0], &neighbor_component_neighbor_values[0], ADD_VALUES);
			MatSetValues(implicit_operator_, blocksize, &idxn[0], blocksize, &idxm[0], &neighbor_component_owner_values[0], ADD_VALUES);
		}
	}

	// cell sweep
	//const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	//const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();
	//const std::vector<std::vector<real_t>>& cell_div_basis_value = grid_->getCellDivBasisValue();
	const std::vector<std::vector<real_t>>& cell_coefficients = grid_->getCellCoefficients();
	const std::vector<std::vector<real_t>>& cell_source_coefficients = grid_->getCellSourceCoefficients();
	const std::vector<std::vector<real_t>>& wall_distance = grid_->getWallDistances();
	const std::vector<std::vector<real_t>>& cell_owner_to_owner_auxiliary_jacobian = grid_->getCellOwnerToOwnerAuxiliaryJacobian();
	const std::vector<std::vector<real_t>>& cell_owner_to_neighbor_auxiliary_jacobian = grid_->getCellOwnerToNeighborAuxiliaryJacobian();
	const std::vector<std::vector<int_t>>& cell_to_cells = grid_->getCellToCells();

	for (int_t icell = 0; icell < num_cells_; icell++)
	{
		const int_t& num_quad_points = num_cell_quad_points[icell];
		int_t irow, icolumn;
		for (int_t i = 0; i < blocksize; i++)
			idxm[i] = cell_petsc_index[icell] * blocksize + i;
		for (int_t i = 0; i < blocksize*blocksize; i++)
			owner_component_owner_values[i] = 0;

		std::vector<real_t> solution(num_quad_points*num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					solution[ipoint*num_states_ + istate] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * instant_solution[icell][istate*num_basis_ + ibasis];

		const std::vector<real_t> conv_flux_jacobian = equation_->calComConvFluxJacobian(solution);

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					for (int_t jstate = 0; jstate < num_states_; jstate++)
						for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
							for (int_t idim = 0; idim < dimension_; idim++)
							{
								irow = ibasis + num_basis_ * istate;
								icolumn = jbasis + num_basis_ * jstate;
								owner_component_owner_values[irow*blocksize + icolumn] -= jacobian_factor
									* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
									* conv_flux_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
									* cell_basis_value[icell][ipoint*num_basis_ + jbasis];
							}

		if (visc_flux_ == true)
		{
			std::vector<real_t> div_solution(num_quad_points * num_states_ * dimension_, 0.0);
			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t idim = 0; idim < dimension_; idim++)
							div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += cell_div_basis_value[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim] * instant_solution[icell][istate*num_basis_ + ibasis];

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t idim = 0; idim < dimension_; idim++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							div_solution[ipoint*num_states_*dimension_ + istate * dimension_ + idim] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * auxiliary_[icell][istate*dimension_*num_basis_ + idim * num_basis_ + ibasis];

			const std::vector<real_t> visc_flux_jacobian = equation_->calComViscFluxJacobian(solution, div_solution);
			const std::vector<real_t> visc_flux_grad_jacobian = equation_->calComViscFluxGradJacobian(solution, div_solution);

			for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
						for (int_t jstate = 0; jstate < num_states_; jstate++)
							for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
								for (int_t idim = 0; idim < dimension_; idim++)
								{
									irow = ibasis + num_basis_ * istate;
									icolumn = jbasis + num_basis_ * jstate;

									// Flux Jacobian * Basis
									owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
										* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
										* visc_flux_jacobian[ipoint*num_states_*num_states_*dimension_ + istate * num_states_*dimension_ + jstate * dimension_ + idim]
										* cell_basis_value[icell][ipoint*num_basis_ + jbasis]; //+

									for (int_t jdim = 0; jdim < dimension_; jdim++)
									{
										// Gradient Jacobian * div_Basis
										owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
											* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
											* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_*dimension_ + istate * num_states_*dimension_*dimension_ + jstate * dimension_*dimension_ + idim * dimension_ + jdim]
											* cell_div_basis_value[icell][ipoint*num_basis_*dimension_ + jbasis * dimension_ + jdim];

										// Gradient Jacobian * Auxiliary Jacobian * Basis
										owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
											* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
											* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_*dimension_ + istate * num_states_*dimension_*dimension_ + jstate * dimension_*dimension_ + idim * dimension_ + jdim]
											* cell_owner_to_owner_auxiliary_jacobian[icell][ipoint*num_basis_*dimension_ + jbasis * dimension_ + jdim];

										for (int_t kstate = 0; kstate < num_states_; kstate++)
											owner_component_owner_values[irow*blocksize + icolumn] += jacobian_factor
											* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
											* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_*dimension_ + istate * num_states_*dimension_*dimension_ + kstate * dimension_*dimension_ + idim * dimension_ + jdim]
											* auxiliary_jacobian_[icell][ipoint*num_states_*num_states_*num_basis_*dimension_ + kstate * num_states_*num_basis_*dimension_ + jstate * num_basis_*dimension_ + jbasis * dimension_ + jdim];
									}
								}

			for (int_t jneighbor = 0; jneighbor < cell_to_cells[icell].size(); jneighbor++)
			{
				// Initializing
				const int_t jcell = cell_to_cells[icell][jneighbor];
				for (int_t i = 0; i < blocksize; i++)
					idxn[i] = cell_petsc_index[jcell] * blocksize + i;
				for (int_t i = 0; i < blocksize*blocksize; i++)
					owner_component_neighbor_values[i] = 0;

				for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
					for (int_t istate = 0; istate < num_states_; istate++)
						for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
							for (int_t jstate = 0; jstate < num_states_; jstate++)
								for (int_t jbasis = 0; jbasis < num_basis_; jbasis++)
								{
									irow = ibasis + num_basis_ * istate;
									icolumn = jbasis + num_basis_ * jstate;

									for (int_t idim = 0; idim < dimension_; idim++)
										for (int_t jdim = 0; jdim < dimension_; jdim++)
										{
											// Gradient Jacobian * Auxiliary Jacobian * Basis
											owner_component_neighbor_values[irow*blocksize + icolumn] += jacobian_factor
												* cell_coefficients[icell][ipoint*num_basis_*dimension_ + ibasis * dimension_ + idim]
												* visc_flux_grad_jacobian[ipoint*num_states_*num_states_*dimension_*dimension_ + istate * num_states_*dimension_*dimension_ + jstate * dimension_*dimension_ + idim * dimension_ + jdim]
												* cell_owner_to_neighbor_auxiliary_jacobian[icell][jneighbor*num_quad_points*num_basis_*dimension_ + ipoint * num_basis_*dimension_ + jbasis * dimension_ + jdim];
										}
								}

				MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxn[0], &owner_component_neighbor_values[0], ADD_VALUES);
			}

			if (source_ == true)
			{
				// source term
			}
		}

		MatSetValues(implicit_operator_, blocksize, &idxm[0], blocksize, &idxm[0], &owner_component_owner_values[0], ADD_VALUES);

		for (int_t i = 0; i < blocksize; i++)
			MatSetValues(implicit_operator_, 1, &idxm[i], 1, &idxm[i], &diagonal_factor, ADD_VALUES);
	}

	MatAssemblyBegin(implicit_operator_, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(implicit_operator_, MAT_FINAL_ASSEMBLY);
	return implicit_operator_;
}

void Zone::UpdateSolutionFromVec(const Vec& new_solution)
{
	const std::vector<int_t>& cell_petsc_index = grid_->getCellPetscIndex();
	const PetscScalar *vec_array;
	const PetscInt num_var = solution_[0].size();
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells_);
	VecGetArrayRead(new_solution, &vec_array);
	for (int_t icell = 0; icell < num_cells_; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			solution_[icell][ivar] = vec_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar];
	VecRestoreArrayRead(new_solution, &vec_array);
}

void Zone::UpdateSolutionFromDelta(const Vec& delta)
{
	const std::vector<int_t>& cell_petsc_index = grid_->getCellPetscIndex();
	const PetscScalar *delta_array;
	const PetscInt num_var = solution_[0].size();
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells_);
	VecGetArrayRead(delta, &delta_array);
	for (int_t icell = 0; icell < num_cells_; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			solution_[icell][ivar] += delta_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar];
	VecRestoreArrayRead(delta, &delta_array);
}

void Zone::communication()
{
	int_t ndomain = NDOMAIN();
	if (ndomain > 1){
		const std::vector<std::vector<int_t>>& send_cells = grid_->getSendCellList();
		const std::vector<std::vector<int_t>>& recv_cells = grid_->getRecvCellList();

		std::vector<int_t> num_send_data(ndomain);
		std::vector<int_t> num_recv_data(ndomain);
		for (int idomain = 0; idomain < ndomain; idomain++){
			num_send_data[idomain] = num_states_*num_basis_*send_cells[idomain].size();
			num_recv_data[idomain] = num_states_*num_basis_*recv_cells[idomain].size();
		}

		std::vector<std::vector<real_t>> send_data(ndomain);
		std::vector<std::vector<real_t>> recv_data(ndomain);
		for (int_t idomain = 0; idomain < ndomain; idomain++){
			if (num_send_data[idomain] == 0) send_data[idomain].resize(1);
			else send_data[idomain].resize(num_send_data[idomain]);
			if (num_recv_data[idomain] == 0) recv_data[idomain].resize(1);
			else recv_data[idomain].resize(num_recv_data[idomain]);
			int_t loc = 0;
			for (auto&& cell : send_cells[idomain]){
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t i = loc; i < loc + num_basis_; i++)
					send_data[idomain][i + istate*num_basis_] = solution_[cell][istate*num_basis_ + i - loc];
				loc += num_basis_*num_states_;
			}
		}

		std::vector<MPI_Request> recv_req(ndomain); std::vector<MPI_Status> recv_sta(ndomain);
		std::vector<MPI_Request> send_req(ndomain); std::vector<MPI_Status> send_sta(ndomain);
		for (int idomain = 0; idomain < ndomain; idomain++){
			MPI_Irecv(&recv_data[idomain][0], num_recv_data[idomain], MPI_REAL_T, idomain, 22, MPI_COMM_WORLD, &recv_req[idomain]);
			MPI_Isend(&send_data[idomain][0], num_send_data[idomain], MPI_REAL_T, idomain, 22, MPI_COMM_WORLD, &send_req[idomain]);
		}
		MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
		MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

		for (int idomain = 0; idomain < ndomain; idomain++){
			int loc = 0;
			for (auto&& cell : recv_cells[idomain]){
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t i = loc; i < loc + num_basis_; i++)
					solution_[cell][istate*num_basis_ + i - loc] = recv_data[idomain][i + istate*num_basis_];
				loc = loc + num_basis_*num_states_;
			}
		}
	}
}

void Zone::RequestCommunication(std::vector<std::vector<real_t> >& solution) // for implicit
{
	int_t ndomain = NDOMAIN();
	if (ndomain > 1) {
		const std::vector<std::vector<int_t>>& send_cells = grid_->getSendCellList();
		const std::vector<std::vector<int_t>>& recv_cells = grid_->getRecvCellList();

		std::vector<int_t> num_send_data(ndomain);
		std::vector<int_t> num_recv_data(ndomain);
		for (int idomain = 0; idomain < ndomain; idomain++) {
			num_send_data[idomain] = num_states_ * num_basis_*send_cells[idomain].size();
			num_recv_data[idomain] = num_states_ * num_basis_*recv_cells[idomain].size();
		}

		std::vector<std::vector<real_t>> send_data(ndomain);
		std::vector<std::vector<real_t>> recv_data(ndomain);
		for (int_t idomain = 0; idomain < ndomain; idomain++) {
			if (num_send_data[idomain] == 0) send_data[idomain].resize(1);
			else send_data[idomain].resize(num_send_data[idomain]);
			if (num_recv_data[idomain] == 0) recv_data[idomain].resize(1);
			else recv_data[idomain].resize(num_recv_data[idomain]);
			int_t loc = 0;
			for (auto&& cell : send_cells[idomain]) {
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t i = loc; i < loc + num_basis_; i++)
						send_data[idomain][i + istate * num_basis_] = solution[cell][istate*num_basis_ + i - loc];
				loc += num_basis_ * num_states_;
			}
		}

		std::vector<MPI_Request> recv_req(ndomain); std::vector<MPI_Status> recv_sta(ndomain);
		std::vector<MPI_Request> send_req(ndomain); std::vector<MPI_Status> send_sta(ndomain);
		for (int idomain = 0; idomain < ndomain; idomain++) {
			MPI_Irecv(&recv_data[idomain][0], num_recv_data[idomain], MPI_REAL_T, idomain, 22, MPI_COMM_WORLD, &recv_req[idomain]);
			MPI_Isend(&send_data[idomain][0], num_send_data[idomain], MPI_REAL_T, idomain, 22, MPI_COMM_WORLD, &send_req[idomain]);
		}
		MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
		MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);

		for (int idomain = 0; idomain < ndomain; idomain++) {
			int loc = 0;
			for (auto&& cell : recv_cells[idomain]) {
				for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t i = loc; i < loc + num_basis_; i++)
						solution[cell][istate*num_basis_ + i - loc] = recv_data[idomain][i + istate * num_basis_];
				loc = loc + num_basis_ * num_states_;
			}
		}
	}
}

void Zone::limiting()
{
	limiter_->limiting();
}

void Zone::pressurefix(const real_t current_time, const int_t iteration)
{
	if (pressure_fix_ == false) return;

	std::vector<bool> activation(num_total_cells_, false);

	const int_t iter = 10;

	// cell sweep
	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();

	for (int_t icell = 0; icell < num_cells_; icell++){
		bool activate = false;
		const int_t num_points = num_cell_quad_points[icell];

		for (int_t ipoint = 0; ipoint < num_points; ipoint++){
			bool flag = true;
			for (int_t i = 0; i < iter; i++){

				std::vector<real_t> solution(num_states_, 0.0);
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					solution[istate] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * solution_[icell][istate*num_basis_ + ibasis];

				if (equation_->applyPressureFix(solution) == true){
					activate = true;
					for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 1; ibasis < num_basis_; ibasis++)
						solution_[icell][istate*num_basis_ + ibasis] *= 0.5;
				}
				else{
					flag = false; break;
				}
			}
			if (flag == true){
				activate = true;
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 1; ibasis < num_basis_; ibasis++)
					solution_[icell][istate*num_basis_ + ibasis] = 0.0;
			}
		}
		if (activate == true)
			activation[icell] = true;
	}

	// face sweep
	const std::vector<int_t>& num_face_quad_points = grid_->getNumFaceQuadPoints();
	const std::vector<int_t>& face_owner_cell = grid_->getFaceOwnerCell();
	const std::vector<int_t>& face_neighbor_cell = grid_->getFaceNeighborCell();
	const std::vector<std::vector<real_t>>& face_owner_basis_value = grid_->getFaceOwnerBasisValue();
	const std::vector<std::vector<real_t>>& face_neighbor_basis_value = grid_->getFaceNeighborBasisValue();

	for (int_t iface = 0; iface < num_faces_; iface++){
		bool owner_activate = false;
		bool neighbor_activate = false;
		const int_t owner_cell = face_owner_cell[iface];
		const int_t neighbor_cell = face_neighbor_cell[iface];
		const int_t num_quad_points = num_face_quad_points[iface];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++){
			bool flag = true;
			for (int_t i = 0; i < iter; i++){

				std::vector<real_t> owner_solution(num_states_, 0.0);
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[istate] += face_owner_basis_value[iface][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];

				if (equation_->applyPressureFix(owner_solution) == true){
					owner_activate = true;
					for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 1; ibasis < num_basis_; ibasis++)
						solution_[owner_cell][istate*num_basis_ + ibasis] *= 0.5;
				}
				else{
					flag = false; break;
				}
			}
			if (flag == true){
				owner_activate = true;
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 1; ibasis < num_basis_; ibasis++)
					solution_[owner_cell][istate*num_basis_ + ibasis] = 0.0;
			}

			flag = true;
			for (int_t i = 0; i < iter; i++){

				std::vector<real_t> neighbor_solution(num_states_, 0.0);
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					neighbor_solution[istate] += face_neighbor_basis_value[iface][ipoint*num_basis_ + ibasis] * solution_[neighbor_cell][istate*num_basis_ + ibasis];

				if (equation_->applyPressureFix(neighbor_solution) == true){
					neighbor_activate = true;
					for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 1; ibasis < num_basis_; ibasis++)
						solution_[neighbor_cell][istate*num_basis_ + ibasis] *= 0.5;
				}
				else{
					flag = false; break;
				}
			}
			if (flag == true){
				neighbor_activate = true;
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 1; ibasis < num_basis_; ibasis++)
					solution_[neighbor_cell][istate*num_basis_ + ibasis] = 0.0;
			}
		}
		if (owner_activate == true)
			activation[owner_cell] = true;
		if (neighbor_activate == true)
			activation[neighbor_cell] = true;
	}

	// boundary sweep
	const std::vector<int_t>& num_bdry_quad_points = grid_->getNumBdryQuadPoints();
	const std::vector<int_t>& bdry_owner_cell = grid_->getBdryOwnerCell();
	const std::vector<std::vector<real_t>>& bdry_owner_basis_value = grid_->getBdryOwnerBasisValue();
	
	for (int_t ibdry = 0; ibdry < num_bdries_; ibdry++){
		bool owner_activate = false;
		const int_t owner_cell = bdry_owner_cell[ibdry];
		const int_t num_quad_points = num_bdry_quad_points[ibdry];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++){
			bool flag = true;
			for (int_t i = 0; i < iter; i++){

				std::vector<real_t> owner_solution(num_states_, 0.0);
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[istate] += bdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];

				if (equation_->applyPressureFix(owner_solution) == true){
					owner_activate = true;
					for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 1; ibasis < num_basis_; ibasis++)
						solution_[owner_cell][istate*num_basis_ + ibasis] *= 0.5;
				}
				else{
					flag = false; break;
				}
			}
			if (flag == true){
				owner_activate = true;
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 1; ibasis < num_basis_; ibasis++)
					solution_[owner_cell][istate*num_basis_ + ibasis] = 0.0;
			}
		}
		if (owner_activate == true)
			activation[owner_cell] = true;
	}

	// periodic boundary sweep
	const std::vector<int_t>& num_peribdry_quad_points = grid_->getNumPeriBdryQuadPoints();
	const std::vector<int_t>& peribdry_owner_cell = grid_->getPeriBdryOwnerCell();
	const std::vector<std::vector<real_t>>& peribdry_owner_basis_value = grid_->getPeriBdryOwnerBasisValue();

	for (int_t ibdry = 0; ibdry < num_peribdries_; ibdry++){
		bool owner_activate = false;
		const int_t owner_cell = peribdry_owner_cell[ibdry];
		const int_t num_quad_points = num_peribdry_quad_points[ibdry];

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++){
			bool flag = true;
			for (int_t i = 0; i < iter; i++){

				std::vector<real_t> owner_solution(num_states_, 0.0);
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					owner_solution[istate] += peribdry_owner_basis_value[ibdry][ipoint*num_basis_ + ibasis] * solution_[owner_cell][istate*num_basis_ + ibasis];

				if (equation_->applyPressureFix(owner_solution) == true){
					owner_activate = true;
					for (int_t istate = 0; istate < num_states_; istate++)
					for (int_t ibasis = 1; ibasis < num_basis_; ibasis++)
						solution_[owner_cell][istate*num_basis_ + ibasis] *= 0.5;
				}
				else{
					flag = false; break;
				}
			}
			if (flag == true){
				owner_activate = true;
				for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 1; ibasis < num_basis_; ibasis++)
					solution_[owner_cell][istate*num_basis_ + ibasis] = 0.0;
			}
		}
		if (owner_activate == true)
			activation[owner_cell] = true;
	}

#ifdef _Pressure_Fix_Hist_
	int_t sum_activation = 0;
	for (int_t icell = 0; icell < num_cells_; icell++)
		if (activation[icell] == true)
			sum_activation++;
	int_t sum_activation_total;
	MPI_Allreduce(&sum_activation, &sum_activation_total, 1, MPI_INT_T, MPI_SUM, MPI_COMM_WORLD);
	if (MYRANK() == MASTER_NODE)
		pressure_fix_hist_file_ << iteration << "\t" << current_time << "\t" << sum_activation_total << std::endl;
#endif
}

real_t Zone::getTimeStep()
{
	const std::vector<real_t>& cell_proj_volumes = grid_->getCellProjVolumes();
	const std::vector<real_t>& cell_volumes = grid_->getCellVolumes();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();

	std::vector<real_t> dt(num_cells_);
	real_t dtmin, dtmin_total;

	for (int_t icell = 0; icell < num_cells_; icell++){

		std::vector<real_t> sol(num_states_);
		for (int_t istate = 0; istate < num_states_; istate++)
			sol[istate] = solution_[icell][istate*num_basis_] * cell_basis_value[icell][0];

		const std::vector<real_t> conv_wave = equation_->calConvWaveVelocity(sol);
		real_t sum = 0.0;
		for (int_t idim = 0; idim < dimension_; idim++)
			sum += conv_wave[idim] * cell_proj_volumes[icell*dimension_ + idim];

		if (visc_flux_ == true){
			const std::vector<real_t> visc_wave = equation_->calViscWaveVelocity(sol);
			real_t sum_visc = 0.0;
			for (int_t idim = 0; idim < dimension_; idim++)
				sum_visc += visc_wave[idim] * cell_proj_volumes[icell*dimension_ + idim] * cell_proj_volumes[icell*dimension_ + idim];
			sum_visc  = dimension_*sum_visc / cell_volumes[icell] * real_t(2 * order_ + 1);
			sum += sum_visc;
		}
		dt[icell] = cell_volumes[icell] / sum;
	}

	dtmin = *min_element(dt.begin(), dt.end());
	if (!std::isnormal(dtmin)) MEMBER_ERROR("Timestep is wrong! (dt = " + TO_STRING(dtmin) + ")");
	MPI_Allreduce(&dtmin, &dtmin_total, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
	return dtmin_total*CFL_ / real_t(2 * order_ + 1);
}

real_t Zone::getTimeStepFromCFL(const real_t cfl)
{
	const std::vector<real_t>& cell_proj_volumes = grid_->getCellProjVolumes();
	const std::vector<real_t>& cell_volumes = grid_->getCellVolumes();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();

	std::vector<real_t> dt(num_cells_);
	real_t dtmin, dtmin_total;

	for (int_t icell = 0; icell < num_cells_; icell++) {

		std::vector<real_t> sol(num_states_);
		for (int_t istate = 0; istate < num_states_; istate++)
			sol[istate] = solution_[icell][istate*num_basis_] * cell_basis_value[icell][0];

		const std::vector<real_t> conv_wave = equation_->calConvWaveVelocity(sol);
		real_t sum = 0.0;
		for (int_t idim = 0; idim < dimension_; idim++)
			sum += conv_wave[idim] * cell_proj_volumes[icell*dimension_ + idim];

		if (visc_flux_ == true) {
			const std::vector<real_t> visc_wave = equation_->calViscWaveVelocity(sol);
			real_t sum_visc = 0.0;
			for (int_t idim = 0; idim < dimension_; idim++)
				sum_visc += visc_wave[idim] * cell_proj_volumes[icell*dimension_ + idim] * cell_proj_volumes[icell*dimension_ + idim];
			sum_visc = dimension_ * sum_visc / cell_volumes[icell] * real_t(2 * order_ + 1);
			sum += sum_visc;
		}
		dt[icell] = cell_volumes[icell] / sum;
	}

	dtmin = *min_element(dt.begin(), dt.end());
	if (!std::isnormal(dtmin)) MEMBER_ERROR("Timestep is wrong! (dt = " + TO_STRING(dtmin) + ")");
	MPI_Allreduce(&dtmin, &dtmin_total, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
	return dtmin_total * cfl / real_t(2 * order_ + 1);
}

void Zone::orderTest()
{
	std::vector<real_t> L2_error(num_states_, 0.0);
	std::vector<real_t> L1_error(num_states_, 0.0);
	std::vector<real_t> Linf_error(num_states_, 0.0);
	real_t total_area = 0.0;

	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_quad_points = grid_->getCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_quad_weights = grid_->getCellQuadWeights();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();
	const std::vector<real_t>& cell_volumes = grid_->getCellVolumes();

	//const real_t factor = std::exp(-0.003 * 4.0 * 3.14159265358979323846264338327950288*3.14159265358979323846264338327950288); // for debug
	Config& config = Config::getInstance(); // for debug

	for (int_t icell = 0; icell < num_cells_; icell++){
		const int_t& num_quad_points = num_cell_quad_points[icell];

		//std::vector<real_t> initial_value = equation_->getInitializerVal(cell_quad_points[icell]);
		std::vector<real_t> initial_value = equation_->getExactVal(cell_quad_points[icell], config.getIterMax()*config.getPhysicalTimeStep()); // for debug
		//for (auto&& value : initial_value)
		//	value *= factor;		// for debug

		std::vector<real_t> solution(num_quad_points * num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
		for (int_t istate = 0; istate < num_states_; istate++)
		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
			solution[ipoint*num_states_ + istate] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * solution_[icell][istate*num_basis_ + ibasis];

		//for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++) { // vortex benchmark test, for debug
		//	initial_value[ipoint*num_states_ + 1] /= initial_value[ipoint*num_states_];
		//	initial_value[ipoint*num_states_ + 2] /= initial_value[ipoint*num_states_];
		//	solution[ipoint*num_states_ + 1] /= solution[ipoint*num_states_];
		//	solution[ipoint*num_states_ + 2] /= solution[ipoint*num_states_];
		//}

		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++){
			for (int_t istate = 0; istate < num_states_; istate++){
				L2_error[istate] += (initial_value[ipoint*num_states_ + istate] - solution[ipoint*num_states_ + istate])*(initial_value[ipoint*num_states_ + istate] - solution[ipoint*num_states_ + istate])*cell_quad_weights[icell][ipoint];
				L1_error[istate] += std::abs(initial_value[ipoint*num_states_ + istate] - solution[ipoint*num_states_ + istate])*cell_quad_weights[icell][ipoint];
				Linf_error[istate] = std::max(std::abs(initial_value[ipoint*num_states_ + istate] - solution[ipoint*num_states_ + istate]), Linf_error[istate]);
			}
		}
		total_area += cell_volumes[icell];
	}

	real_t sum = 0.0;
	for (int_t istate = 0; istate < num_states_; istate++){
		MPI_Allreduce(&L2_error[istate], &sum, 1, MPI_REAL_T, MPI_SUM, MPI_COMM_WORLD);
		L2_error[istate] = sum;
		MPI_Allreduce(&L1_error[istate], &sum, 1, MPI_REAL_T, MPI_SUM, MPI_COMM_WORLD);
		L1_error[istate] = sum;
		MPI_Allreduce(&Linf_error[istate], &sum, 1, MPI_REAL_T, MPI_MAX, MPI_COMM_WORLD);
		Linf_error[istate] = sum;
	}
	MPI_Allreduce(&total_area, &sum, 1, MPI_REAL_T, MPI_SUM, MPI_COMM_WORLD);
	total_area = sum;
	for (int_t istate = 0; istate < num_states_; istate++){
		L2_error[istate] = std::sqrt(L2_error[istate] / total_area);
		L1_error[istate] = L1_error[istate] / total_area;
		MASTER_MESSAGE("L2 error(state:" + TO_STRING(istate) + ") is " + TO_STRING(L2_error[istate]));
		MASTER_MESSAGE("L1 error(state:" + TO_STRING(istate) + ") is " + TO_STRING(L1_error[istate]));
		MASTER_MESSAGE("Linf error(state:" + TO_STRING(istate) + ") is " + TO_STRING(Linf_error[istate]));
	}
	if (MYRANK() == MASTER_NODE){
		Config& config = Config::getInstance();
		std::ofstream outfile(config.getReturnAddress() + "/error.txt");
		for (int_t istate = 0; istate < num_states_; istate++){
			outfile << "L2 error(state:" << TO_STRING(istate) << ") is " << TO_STRING(L2_error[istate]) << std::endl;
			outfile << "L1 error(state:" << TO_STRING(istate) << ") is " << TO_STRING(L1_error[istate]) << std::endl;
			outfile << "Linf error(state:" << TO_STRING(istate) << ") is " << TO_STRING(Linf_error[istate]) << std::endl;
		}
		outfile.close();
	}
}

//void Zone::save(const real_t solution_time, const real_t physical_time, const int_t iteration, const int_t strandid)
//{
//	Config& config = Config::getInstance();
//
//	std::string filename = config.getReturnAddress() + "/save/INFO.SAVE";
//	std::ofstream outfile;
//
//	MASTER_MESSAGE("Post: Save file output(" + filename + ")");
//
//	if (MYRANK() == MASTER_NODE){
//		outfile.open(filename);
//		outfile.precision(16);
//
//		outfile << "~>Order" << std::endl;
//		outfile << order_ << std::endl;
//		outfile << "~>Solution Time" << std::endl;
//		outfile << solution_time << std::endl;
//		outfile << "~>Physical Time" << std::endl;
//		outfile << physical_time << std::endl;
//		outfile << "~>Strandid" << std::endl;
//		outfile << strandid << std::endl;
//		outfile << "~>Iteration" << std::endl;
//		outfile << iteration << std::endl;
//		outfile << "~>Number of Core" << std::endl;
//		outfile << NDOMAIN() << std::endl;
//
//		outfile.close();
//	}
//
//	char tag_name[10]; sprintf(tag_name, "%d", MYRANK());
//	filename = config.getReturnAddress() + "/save/" + tag_name + ".SAVE";
//	outfile.open(filename);
//	//outfile << std::scientific;
//	//outfile.precision(16);
//
//	const std::vector<int_t>& cell_global_index = grid_->getCellGlobalIndex();
//	outfile << num_cells_ << std::endl;
//	for (int_t icell = 0; icell < num_cells_;icell++){
//		outfile << cell_global_index[icell];
//		for (int_t istate = 0; istate < num_states_; istate++)
//		for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
//			outfile << " " << solution_[icell][istate*num_basis_ + ibasis];
//		outfile << std::endl;
//	}
//
//	outfile.close();
//}
//
//void Zone::read(real_t& solution_time, int_t& iteration, int_t& strandid)
//{
//	Config& config = Config::getInstance();
//
//	std::string filename = config.getReturnAddress() + "/save/INFO.SAVE";
//	std::ifstream infile;
//
//	MASTER_MESSAGE("Post: Load save data(" + filename + ")");
//	infile.open(filename);
//	int_t order;
//	int_t num_cores;
//	std::string text;
//	while (getline(infile, text)){
//		if (text.find("~>Order", 0) != std::string::npos)
//			infile >> order;
//		if (text.find("~>Solution Time", 0) != std::string::npos){
//			infile >> solution_time;
//			MASTER_MESSAGE("Restart Time : " + TO_STRING(solution_time));
//		}
//		if (text.find("~>Strandid", 0) != std::string::npos)
//			infile >> strandid;
//		if (text.find("~>Iteration", 0) != std::string::npos)
//			infile >> iteration;
//		if (text.find("~>Number of Core", 0) != std::string::npos)
//			infile >> num_cores;
//	}
//	infile.close();
//
//	int_t num_basis;
//	if (dimension_ == 2)
//		num_basis = (order + 1)*(order + 2) / 2;
//	if (dimension_ == 3)
//		num_basis = (order + 1)*(order + 2)*(order + 3) / 6;
//
//	int_t num_cells = 0;
//	std::vector<int_t> cell_global_index;
//	std::vector<std::vector<real_t>> solution;
//
//	int_t myrank = MYRANK();
//	int_t ndomain = NDOMAIN();
//
//	// read data
//	for (int_t icore = myrank; icore < num_cores; icore+=ndomain){
//
//		char tag_name[10]; sprintf(tag_name, "%d", icore);
//		filename = config.getReturnAddress() + "/save/" + tag_name + ".SAVE";
//		infile.open(filename);
//
//		int_t num_local_cells;
//		infile >> num_local_cells;
//
//		cell_global_index.resize(num_cells + num_local_cells);
//		solution.resize(num_cells + num_local_cells);
//
//		for (int_t icell = num_cells; icell < num_cells+num_local_cells; icell++){
//			infile >> cell_global_index[icell];
//			solution[icell].resize(num_states_*num_basis);
//			for (int_t istate = 0; istate < num_states_; istate++)
//			for (int_t ibasis = 0; ibasis < num_basis; ibasis++)
//				infile >> solution[icell][istate*num_basis + ibasis];
//		}
//		num_cells += num_local_cells;
//
//		infile.close();
//	}
//
//	// redistribute data
//	int_t max_var = 0;
//	if(num_cells>0)
//		max_var = *std::max_element(cell_global_index.begin(), cell_global_index.end());
//	std::vector<bool> cell_global_index_flag(max_var + 1, false);
//	for (auto&& index : cell_global_index)
//		cell_global_index_flag[index] = true;
//
//	std::unordered_map<int_t, int_t> index_location;
//	for (int_t icell = 0; icell < num_cells; icell++)
//		index_location[cell_global_index[icell]] = icell;
//
//	std::vector<int_t> index_send;
//	std::vector<int_t> index_recv = grid_->getCellGlobalIndex();
//	std::vector<int_t> num_index(ndomain);
//	
//	int_t num_local_index = index_recv.size();
//	MPI_Allgather(&num_local_index, 1, MPI_INT_T, &num_index[0], 1, MPI_INT_T, MPI_COMM_WORLD);
//
//	MPI_Request recv_req_local;
//	MPI_Status recv_sta_local;
//	MPI_Request send_req_local;
//	MPI_Status send_sta_local;
//
//	int_t before_domain = myrank - 1;
//	int_t next_domain = myrank + 1;
//	if (myrank == MASTER_NODE) before_domain = ndomain - 1;
//	if (myrank == ndomain - 1) next_domain = MASTER_NODE;
//
//	int_t send_domain = myrank;
//	std::vector<int_t> trash_recv(ndomain, 0);
//	std::vector<int_t> trash_send(ndomain, 0);
//	std::vector<std::vector<int_t>> send_cells(ndomain);
//	for (int_t idomain = 0; idomain < ndomain; idomain++){
//		for (int_t icell = 0; icell < index_recv.size(); icell++){
//			if (index_recv[icell] < 0) continue;
//			if (index_recv[icell] > max_var) continue;
//
//			if (cell_global_index_flag[index_recv[icell]] == true){
//				send_cells[send_domain].push_back(index_location[index_recv[icell]]);
//				index_recv[icell] = -myrank - 1;
//			}
//		}
//
//		send_domain = (send_domain + ndomain - 1) % ndomain;
//		index_send = move(index_recv);
//		index_recv.resize(num_index[send_domain]);
//		if (num_index[send_domain]>0)
//			MPI_Irecv(&index_recv[0], num_index[send_domain], MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
//		else
//			MPI_Irecv(&trash_recv[0], 1, MPI_INT_T, before_domain, idomain, MPI_COMM_WORLD, &recv_req_local);
//		if (index_send.size()>0)
//			MPI_Isend(&index_send[0], index_send.size(), MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
//		else
//			MPI_Isend(&trash_send[0], 1, MPI_INT_T, next_domain, idomain, MPI_COMM_WORLD, &send_req_local);
//		MPI_Wait(&recv_req_local, &recv_sta_local);
//		MPI_Wait(&send_req_local, &send_sta_local);
//	}
//
//	std::vector<int_t> num_send(ndomain);
//	std::vector<int_t> num_recv(ndomain);
//	for (int_t idomain = 0; idomain < ndomain; idomain++)
//		num_send[idomain] = send_cells[idomain].size()*num_states_*num_basis;
//
//	MPI_Alltoall(&num_send[0], 1, MPI_INT_T, &num_recv[0], 1, MPI_INT_T, MPI_COMM_WORLD);
//
//	std::vector<std::vector<real_t>> send_solutions(ndomain);
//	for (int_t idomain = 0; idomain < ndomain; idomain++)
//		send_solutions[idomain].reserve(num_send[idomain]);
//	for (int_t idomain = 0; idomain < ndomain; idomain++)
//	for (int_t icell = 0; icell < send_cells[idomain].size(); icell++)
//	for (int_t istate = 0; istate < num_states_; istate++)
//	for (int_t ibasis = 0; ibasis < num_basis; ibasis++)
//		send_solutions[idomain].push_back(solution[send_cells[idomain][icell]][istate*num_basis + ibasis]);
//
//	std::vector<MPI_Request> recv_req(ndomain);
//	std::vector<MPI_Status> recv_sta(ndomain);
//	std::vector<MPI_Request> send_req(ndomain);
//	std::vector<MPI_Status> send_sta(ndomain);
//	
//	std::vector<std::vector<real_t>> recv_solutions(ndomain);
//	for (int_t idomain = 0; idomain < ndomain; idomain++)
//		recv_solutions[idomain].resize(num_recv[idomain]);
//	for (int_t idomain = 0; idomain < ndomain; idomain++){
//		if (num_recv[idomain]>0)
//			MPI_Irecv(&recv_solutions[idomain][0], num_recv[idomain], MPI_REAL_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
//		else
//			MPI_Irecv(&trash_recv[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
//	}
//	for (int_t idomain = 0; idomain < ndomain; idomain++){
//		if (num_send[idomain]>0)
//			MPI_Isend(&send_solutions[idomain][0], num_send[idomain], MPI_REAL_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
//		else
//			MPI_Isend(&trash_send[idomain], 1, MPI_INT_T, idomain, myrank, MPI_COMM_WORLD, &send_req[idomain]);
//	}
//	MPI_Waitall(ndomain, &recv_req[0], &recv_sta[0]);
//	MPI_Waitall(ndomain, &send_req[0], &send_sta[0]);
//
//	solution_.resize(num_total_cells_);
//	for (int_t icell = 0; icell < num_total_cells_; icell++)
//		solution_[icell].resize(num_states_*num_basis_, 0.0);
//
//	int_t min_num_basis = std::min(num_basis, num_basis_);
//	std::vector<int_t> local_index(ndomain, 0);
//	for (int_t icell = 0; icell < num_total_cells_; icell++){
//		int_t idomain = -index_recv[icell] - 1;
//		for (int_t istate = 0; istate < num_states_; istate++)
//		for (int_t ibasis = 0; ibasis < min_num_basis; ibasis++)
//			solution_[icell][istate*num_basis_ + ibasis] = recv_solutions[idomain][local_index[idomain] * num_states_*num_basis + istate*num_basis + ibasis];
//		local_index[idomain]++;
//	}
//}

void Zone::save(const real_t solution_time, const real_t physical_time, const int_t iteration, const int_t strandid)
{
	Config& config = Config::getInstance();

	std::string filename;
	std::ofstream outfile;

	MASTER_MESSAGE("Post: Save file output(" + filename + ")");

	if (myrank_ == MASTER_NODE) {
		filename = config.getReturnAddress() + "/save/INFO.SAVE";
		outfile.open(filename);

		outfile << "~>Order" << std::endl;
		outfile << order_ << std::endl;
		outfile << "~>Solution Time" << std::endl;
		outfile << solution_time << std::endl;
		outfile << "~>Physical Time" << std::endl;
		outfile << physical_time << std::endl;
		outfile << "~>Strandid" << std::endl;
		outfile << strandid << std::endl;
		outfile << "~>Iteration" << std::endl;
		outfile << iteration << std::endl;
		outfile << "~>Number of Core" << std::endl;
		outfile << ndomain_ << std::endl;

		outfile.close();
	}

	filename = config.getReturnAddress() + "/save/solution" + TO_STRING(iteration) + ".SAVE";
	MPI_File savefile;
	MPI_File_open(MPI_COMM_WORLD, filename.c_str(), MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &savefile);
	MPI_File_set_view(savefile, save_disp_, MPI_INT_T, MPI_INT_T, "native", MPI_INFO_NULL);

	const int_t num_global_cells = grid_->getNumGlobalCells();
	if (myrank_ == MASTER_NODE) {
		MPI_File_write(savefile, &order_, 1, MPI_INT_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &solution_time, 1, MPI_REAL_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &physical_time, 1, MPI_REAL_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &strandid, 1, MPI_INT_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &iteration, 1, MPI_INT_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &ndomain_, 1, MPI_INT_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &num_global_cells, 1, MPI_INT_T, MPI_STATUS_IGNORE);
	}
	const std::vector<int_t>& cell_global_index = grid_->getCellGlobalIndex();
	for (int_t icell = 0; icell < num_cells_; icell++) {
		MPI_File_write(savefile, &cell_global_index[icell], 1, MPI_INT_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &solution_[icell][0], num_states_*num_basis_, MPI_REAL_T, MPI_STATUS_IGNORE);
	}
	MPI_File_close(&savefile);
}

void Zone::saveApprox(const real_t solution_time, const real_t physical_time, const int_t iteration, const int_t strandid)
{
	Config& config = Config::getInstance();
	if (iteration < config.getIterMax()) return;

	std::string filename;
	std::ofstream outfile;

	MASTER_MESSAGE("Post: Save file output(" + filename + ")");

	filename = config.getReturnAddress() + "/save/refsolution" + TO_STRING(iteration) + ".SAVE";
	MPI_File savefile;
	MPI_File_open(MPI_COMM_WORLD, filename.c_str(), MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &savefile);
	MPI_File_set_view(savefile, save_app_disp_, MPI_INT_T, MPI_INT_T, "native", MPI_INFO_NULL);

	// generating approximate solution
	const std::vector<int_t>& num_cell_quad_points = grid_->getNumCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_quad_points = grid_->getCellQuadPoints();
	const std::vector<std::vector<real_t>>& cell_quad_weights = grid_->getCellQuadWeights();
	const std::vector<std::vector<real_t>>& cell_basis_value = grid_->getCellBasisValue();
	const std::vector<real_t>& cell_volumes = grid_->getCellVolumes();
	const std::vector<std::vector<real_t> > basis = grid_->getCellBasisValue();
	std::vector<std::vector<real_t> > approx_solution(num_total_cells_);
	for (int_t icell = 0; icell < num_total_cells_; icell++)
	{
		const int_t& num_quad_points = num_cell_quad_points[icell];
		std::vector<real_t> solution(num_quad_points*num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				for (int_t ibasis = 0; ibasis < num_basis_; ibasis++)
					solution[ipoint*num_states_ + istate] += cell_basis_value[icell][ipoint*num_basis_ + ibasis] * solution_[icell][istate*num_basis_ + ibasis];

		approx_solution[icell].resize(num_states_, 0.0);
		for (int_t ipoint = 0; ipoint < num_quad_points; ipoint++)
			for (int_t istate = 0; istate < num_states_; istate++)
				approx_solution[icell][istate] += solution[ipoint*num_states_ + istate] * cell_quad_weights[icell][ipoint];
		for (int_t istate = 0; istate < num_states_; istate++)
			approx_solution[icell][istate] /= cell_volumes[icell];
	}		

	const int_t num_global_cells = grid_->getNumGlobalCells();
	if (myrank_ == MASTER_NODE) {
		MPI_File_write(savefile, &order_, 1, MPI_INT_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &solution_time, 1, MPI_REAL_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &physical_time, 1, MPI_REAL_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &strandid, 1, MPI_INT_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &iteration, 1, MPI_INT_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &ndomain_, 1, MPI_INT_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &num_global_cells, 1, MPI_INT_T, MPI_STATUS_IGNORE);
	}
	const std::vector<int_t>& cell_global_index = grid_->getCellGlobalIndex();
	for (int_t icell = 0; icell < num_cells_; icell++) {
		MPI_File_write(savefile, &cell_global_index[icell], 1, MPI_INT_T, MPI_STATUS_IGNORE);
		MPI_File_write(savefile, &approx_solution[icell][0], num_states_, MPI_REAL_T, MPI_STATUS_IGNORE);
	}
	MPI_File_close(&savefile);
}

void Zone::read(real_t& solution_time, int_t& iteration, int_t& strandid)
{
	Config& config = Config::getInstance();

	std::string filename = config.getReturnAddress() + "/save/solution.SAVE";
	std::ifstream infile(filename, std::ios::binary);

	MASTER_MESSAGE("Post: Load save data(" + filename + ")");

	int_t order;
	int_t num_cores;
	std::string text;
	real_t physical_time;
	infile.read((char*)&order, sizeof(int_t));
	infile.read((char*)&solution_time, sizeof(real_t));
	MASTER_MESSAGE("Restart Time : " + TO_STRING(solution_time));
	infile.read((char*)&physical_time, sizeof(real_t));
	infile.read((char*)&strandid, sizeof(int_t));
	infile.read((char*)&iteration, sizeof(int_t));
	infile.read((char*)&num_cores, sizeof(int_t));

	int_t num_basis;
	if (dimension_ == 2)
		num_basis = (order + 1)*(order + 2) / 2;
	if (dimension_ == 3)
		num_basis = (order + 1)*(order + 2)*(order + 3) / 6;

	std::vector<std::vector<real_t>> solution(num_total_cells_);
	for (int_t icell = 0; icell < num_total_cells_; icell++)
		solution[icell].resize(num_states_*num_basis_, 0.0);
	const std::vector<int_t>& cell_global_index = grid_->getCellGlobalIndex();

	int_t num_global_cells;
	infile.read((char*)&num_global_cells, sizeof(int_t));

	std::vector<bool> cell_flag(num_global_cells, false);
	for (int_t icell = 0; icell < num_cells_; icell++)
		cell_flag[cell_global_index[icell]] = true;

	std::unordered_map<int_t, int_t> global_to_local;
	for (int_t icell = 0; icell < num_cells_; icell++)
		global_to_local[cell_global_index[icell]] = icell;

	int_t global_index;
	bool isrestrict = (num_basis_ < num_basis);
	int_t min_num_basis = std::min(num_basis_, num_basis);
	std::vector<real_t> read_buffer(num_basis*num_states_);
	for (int_t icell = 0; icell < num_global_cells; icell++) {
		infile.read((char*)&global_index, sizeof(int_t));
		if (!cell_flag[global_index]) {
			infile.seekg(sizeof(real_t)*num_basis*num_states_, std::ios::cur);
			continue;
		}
		infile.read((char*)&read_buffer[0], sizeof(real_t)*num_basis*num_states_);
		for (int_t istate = 0; istate < num_states_; istate++)
			for (int_t ibasis = 0; ibasis < min_num_basis; ibasis++)
				solution[global_to_local[global_index]][istate*num_basis_ + ibasis] = read_buffer[istate*num_basis + ibasis];
	}
	infile.close();
	solution_ = move(solution);

	communication();
}