
#include "../INC/SAFlux.h"

SAFlux::SAFlux()
{
	readData("SAEquation", INPUT() + "equation.dat");
}

void SAFlux::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	while (getline(infile, text)){
		if (text.find("~>dimension_", 0) != std::string::npos)
			infile >> dimension_;
		if (text.find("~>advection_", 0) != std::string::npos){
			advection_.resize(3);
			infile >> advection_[0] >> advection_[1] >> advection_[2];
		}
		if (text.find("~>diffusion_on_", 0) != std::string::npos){
			getline(infile, text);
			if (!text.compare("on")) diffusion_on_ = true;
			else diffusion_on_ = false;
		}
		if (text.find("~>diffusion_", 0) != std::string::npos){
			if (diffusion_on_ == true){
				diffusion_.resize(dimension_*dimension_);
				diffusion_wave_.resize(dimension_);
				if (dimension_ == 2){
					infile >> diffusion_[0] >> diffusion_[1] >> diffusion_[2];
					infile >> diffusion_[2] >> diffusion_[3];
					arma::mat diff(2, 2);
					for (int_t i = 0; i < dimension_;i++)
					for (int_t j = 0; j < dimension_; j++)
						diff(i, j) = diffusion_[i*dimension_ + j];
					arma::cx_vec val;
					arma::eig_gen(val, diff);
					diffusion_wave_[0] = std::max(std::abs(val(0)), std::abs(val(1)));
					diffusion_wave_[1] = diffusion_wave_[0];
				}
				else if (dimension_ == 3){
					infile >> diffusion_[0] >> diffusion_[1] >> diffusion_[2];
					infile >> diffusion_[3] >> diffusion_[4] >> diffusion_[5];
					infile >> diffusion_[6] >> diffusion_[7] >> diffusion_[8];
					arma::mat diff(3, 3);
					for (int_t i = 0; i < dimension_; i++)
					for (int_t j = 0; j < dimension_; j++)
						diff(i, j) = diffusion_[i*dimension_ + j];
					arma::cx_vec val;
					arma::eig_gen(val, diff);
					diffusion_wave_[0] = std::max(std::abs(val(0)), std::max(std::abs(val(1)), std::abs(val(2))));
					diffusion_wave_[1] = diffusion_wave_[0];
					diffusion_wave_[2] = diffusion_wave_[0];
				}
			}
		}
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}

const std::vector<real_t> SAFlux::commonConvFlux(const std::vector<real_t>& u) const
{
	int_t num_points = u.size();
	std::vector<real_t> Flux(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	for (int_t idim = 0; idim < dimension_; idim++)
		Flux[ipoint*dimension_+idim] = advection_[idim] * u[ipoint];
	return Flux;
}

const std::vector<real_t> SAFlux::commonConvFluxJacobian(const std::vector<real_t>& u) const
{
	int_t num_points = u.size();
	std::vector<real_t> FluxJacobi(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			FluxJacobi[ipoint*dimension_ + idim] = advection_[idim];
	return FluxJacobi;
}

const std::vector<real_t> SAFlux::commonConvFlux(const std::vector<real_t>& u, const std::vector<real_t>& normal) const
{
	int_t num_points = u.size();
	std::vector<real_t> Flux(num_points);
	std::vector<real_t> an(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	for (int_t idim = 0; idim < dimension_; idim++)
		an[ipoint] += normal[ipoint*dimension_ + idim] * advection_[idim];
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		Flux[ipoint] = an[ipoint] * u[ipoint];
	return Flux;
}

const std::vector<real_t> SAFlux::commonConvFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& normal) const
{
	int_t num_points = u.size();
	std::vector<real_t> FluxJacobi(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			FluxJacobi[ipoint] += normal[ipoint*dimension_ + idim] * advection_[idim];
	return FluxJacobi;
}

const std::vector<real_t> SAFlux::commonViscFlux(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const
{
	int_t num_points = u.size();
	std::vector<real_t> Flux(num_points*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points;ipoint++)
	for (int_t idim = 0; idim < dimension_; idim++)
	for (int_t jdim = 0; jdim < dimension_; jdim++)
		Flux[ipoint*dimension_ + idim] += diffusion_[idim*dimension_ + jdim] * div_u[ipoint*dimension_ + jdim];
	return Flux;
}

const std::vector<real_t> SAFlux::commonViscFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const
{
	int_t num_points = u.size();
	std::vector<real_t> FluxJacobi(num_points*dimension_, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> SAFlux::commonViscFluxGradJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const
{
	int_t num_points = u.size();
	std::vector<real_t> FluxGradJacobi(num_points*dimension_*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			for (int_t jdim = 0; jdim < dimension_; jdim++)
				FluxGradJacobi[ipoint*dimension_*dimension_ + idim * dimension_ + jdim] = diffusion_[idim*dimension_ + jdim];
	return FluxGradJacobi;
}

const std::vector<real_t> SAFlux::commonViscFlux(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = u.size();
	std::vector<real_t> Flux(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		std::vector<real_t> F(dimension_, 0.0);
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t jdim = 0; jdim < dimension_; jdim++)
			F[idim] += diffusion_[idim*dimension_ + jdim] * div_u[ipoint*dimension_ + jdim];
		for (int_t idim = 0; idim < dimension_; idim++)
			Flux[ipoint] += normal[ipoint*dimension_ + idim] * F[idim];
	}
	return Flux;
}

const std::vector<real_t> SAFlux::commonViscFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = u.size();
	std::vector<real_t> FluxJacobi(num_points, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> SAFlux::commonViscFluxGradJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = u.size();
	std::vector<real_t> FluxGradJacobi(num_points*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			for (int_t jdim = 0; jdim < dimension_; jdim++)
				FluxGradJacobi[ipoint*dimension_ + jdim] += normal[ipoint*dimension_ + idim] * diffusion_[idim*dimension_ + jdim];
	return FluxGradJacobi;
}

const std::vector<real_t> SAFlux::numViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size();
	std::vector<real_t> Flux(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		std::vector<real_t> F(dimension_, 0.0);
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t jdim = 0; jdim < dimension_; jdim++)
			F[idim] += diffusion_[idim*dimension_ + jdim] * (owner_div_u[ipoint*dimension_ + jdim] + neighbor_div_u[ipoint*dimension_ + jdim]);
		for (int_t idim = 0; idim < dimension_; idim++)
			Flux[ipoint] += 0.5* normal[ipoint*dimension_ + idim] * F[idim];
	}
	return Flux;
}

const std::vector<real_t> SAFlux::numViscFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = owner_u.size();
	std::vector<real_t> FluxJacobi(num_points, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> SAFlux::numViscFluxOwnerGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = owner_u.size();
	std::vector<real_t> FluxGradJacobi(num_points*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			for (int_t jdim = 0; jdim < dimension_; jdim++)
				FluxGradJacobi[ipoint*dimension_ + jdim] += 0.5*normal[ipoint*dimension_ + idim] * diffusion_[idim*dimension_ + jdim];
	return FluxGradJacobi;
}

const std::vector<real_t> SAFlux::numViscFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = owner_u.size();
	std::vector<real_t> FluxJacobi(num_points, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> SAFlux::numViscFluxNeighborGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = neighbor_u.size();
	std::vector<real_t> FluxGradJacobi(num_points*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			for (int_t jdim = 0; jdim < dimension_; jdim++)
				FluxGradJacobi[ipoint*dimension_ + jdim] += 0.5*normal[ipoint*dimension_ + idim] * diffusion_[idim*dimension_ + jdim];
	return FluxGradJacobi;
}

const std::vector<real_t> SAFlux::convWaveVelocity(const std::vector<real_t>& u) const
{
	int_t num_points = u.size();
	std::vector<real_t> wave(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	for (int_t idim = 0; idim < dimension_; idim++)
		wave[ipoint*dimension_+idim] = std::abs(advection_[idim]);
	return wave;
}

const std::vector<real_t> SAFlux::viscWaveVelocity(const std::vector<real_t>& u) const
{
	int_t num_points = u.size();
	std::vector<real_t> wave(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	for (int_t idim = 0; idim < dimension_; idim++)
		wave[ipoint*dimension_ + idim] = diffusion_wave_[idim];
	return wave;
}

const std::vector<real_t> SAFlux::source(const std::vector<real_t>& u) const
{
	int_t num_points = u.size();
	std::vector<real_t> source(num_points, 0.0);
	return source;
}


SAUpwind::SAUpwind()
{
	setName("Upwind");
	SAFlux::getInstance().addRegistry(getName(), this);
}
const std::vector<real_t> SAUpwind::numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size();
	std::vector<real_t> Flux(num_points);
	std::vector<real_t> an(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	for (int_t idim = 0; idim < dimension_; idim++)
		an[ipoint] += normal[ipoint*dimension_ + idim] * advection_[idim];
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		Flux[ipoint] = std::max(an[ipoint], 0.0)*owner_u[ipoint] + std::min(an[ipoint], 0.0)*neighbor_u[ipoint];
	return Flux;
}

const std::vector<real_t> SAUpwind::numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size();
	std::vector<real_t> FluxJacobi(num_points);
	std::vector<real_t> an(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			an[ipoint] += normal[ipoint*dimension_ + idim] * advection_[idim];
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		FluxJacobi[ipoint] = std::max(an[ipoint], 0.0);
	return FluxJacobi;
}

const std::vector<real_t> SAUpwind::numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size();
	std::vector<real_t> FluxJacobi(num_points);
	std::vector<real_t> an(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			an[ipoint] += normal[ipoint*dimension_ + idim] * advection_[idim];
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		FluxJacobi[ipoint] = std::min(an[ipoint], 0.0);
	return FluxJacobi;
}