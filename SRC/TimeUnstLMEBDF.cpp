
#include "../INC/TimeUnstLMEBDF.h"

TimeUnstLMEBDF::TimeUnstLMEBDF()
{
	setName("LMEBDF");
	TimeScheme::getInstance().addRegistry(getName(), this);
	is_implicit_ = true;
}

TimeUnstLMEBDF::~TimeUnstLMEBDF()
{
	ierr_ = VecDestroy(&delta_);
	ierr_ = VecDestroy(&MEBDF_rhs_);
	VECTOR_ARRAY_DESTROYER(history_array_, ierr_);
	VECTOR_ARRAY_DESTROYER(inter_history_array_, ierr_);
	VECTOR_ARRAY_DESTROYER(stage_rhs_, ierr_);
	VECTOR_ARRAY_DESTROYER(solution_array_, ierr_);
}

void TimeUnstLMEBDF::initialize()
{
	const Config& config = Config::getInstance();
	if (config.getIsFixedTimeStep().compare("yes") == 0)
		is_fixed_dt_ = true;
	else is_fixed_dt_ = false;
	time_order_ = config.getTimeOrder();
#ifdef _Order_Test_
	MASTER_MESSAGE("!!!Initializing for Order Test");
	current_time_order_ = time_order_;
#else
	current_time_order_ = 2;
#endif
	recompute_step_ = 0;
	recompute_tol_ = 0.25;
	old_physical_dt_ = 0.0;
	is_initialized_ = false;
	for (int_t i = 0; i < WRMS_norm_.size(); i++)
		WRMS_norm_[i] = 100.0;

	// Vectors
	ierr_ = VecCreate(MPI_COMM_WORLD, &delta_);
	ierr_ = VecCreate(MPI_COMM_WORLD, &MEBDF_rhs_);
	history_array_.resize(time_order_);
	inter_history_array_.resize(time_order_);
	VECTOR_ARRAY_CREATOR(history_array_, ierr_);
	VECTOR_ARRAY_CREATOR(inter_history_array_, ierr_);
	VECTOR_ARRAY_CREATOR(stage_rhs_, ierr_);
	VECTOR_ARRAY_CREATOR(solution_array_, ierr_);

	// BDF & MEBDF coefficients initialize
	InitializeCoeff();
	
	// Linear system solver initialize
	linear_system_solver_ = LinearSystemSolver::getInstance().getType(config.getLinearSystemSolver());
	linear_system_solver_->Initialize();
}

//void TimeUnstLMEBDF::integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
//{
//	if (is_initialized_ == false) InitializeVectors(zone, dt);
//
//	// adaptation
//	if (iteration > 1)
//	{
//		Adaptation();
//		//if(is_fixed_dt_ == false) RescaleHistoryArray(dt / old_physical_dt_);
//		RescaleHistoryArray(dt / old_physical_dt_);
//	}
//	old_physical_dt_ = dt;
//
//	// system matrix
//	Mat system_matrix;
//	const real_t diagonal_factor = 1.0 / dt;
//
//	int_t linear_solver_iteration = 0;
//	for (int_t istage = 0; istage < 3; istage++)
//	{
//		switch (istage) {
//		case 0:
//			linear_solver_iteration = SolveStage1(zone, diagonal_factor, system_matrix);
//			break;
//		case 1:
//			linear_solver_iteration = SolveStage2(zone, diagonal_factor, system_matrix);
//			break;
//		case 2:
//			linear_solver_iteration = SolveStage3(zone, diagonal_factor, system_matrix);
//			break;
//		default:;
//		}
//		zone->limiting();
//		zone->pressurefix(current_time, iteration);
//	}
//
//	real_t error = 0;
//	VecNorm(MEBDF_rhs_, NORM_2, &error);
//	status_string_ = "KrylovIteration=" + TO_STRING(linear_solver_iteration) + " L2=" + TO_STRING((double)error);
//}

void TimeUnstLMEBDF::integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
	const int_t num_cells = zone->getNumCells();
	const std::vector<int_t> cell_petsc_index = zone->getCellPetscIndex();
	int linear_solver_iteration = 0;

	if (is_initialized_ == false)
	{
		const Vec& vec_initializer = zone->getSystemRHS();
		ierr_ = VecDuplicate(vec_initializer, &delta_);
		ierr_ = VecDuplicate(vec_initializer, &MEBDF_rhs_);
		for (int_t i = 0; i < history_array_.size(); i++)
			ierr_ = VecDuplicate(vec_initializer, &history_array_[i]);
		for (int_t i = 0; i < inter_history_array_.size(); i++)
			ierr_ = VecDuplicate(vec_initializer, &inter_history_array_[i]);
		for (int_t i = 0; i < stage_rhs_.size(); i++)
			ierr_ = VecDuplicate(vec_initializer, &stage_rhs_[i]);
		for (int_t i = 0; i < solution_array_.size(); i++)
			ierr_ = VecDuplicate(vec_initializer, &solution_array_[i]);
#ifdef _Order_Test_
		GenerateInitialHistory(zone, dt); // for order test
#else
		UpdateHistoryArray(history_array_, num_cells, zone->getSolution(), cell_petsc_index); // for order test(x)
		UpdateHistoryArray(inter_history_array_, num_cells, zone->getSolution(), cell_petsc_index); // for order test(x)
#endif
		is_initialized_ = true;
	}

	// adaptation
	if (iteration > 1)
	{
		Adaptation();
		//if(is_fixed_dt_ == false) RescaleHistoryArray(dt / old_physical_dt_);
		RescaleHistoryArray(dt / old_physical_dt_);
	}
	old_physical_dt_ = dt;

	// system matrix
	Mat system_matrix;
	const real_t diagonal_factor = 1.0 / dt;

	// Save n step solution
	ConvertSolutionArray(solution_array_[0], num_cells, zone->getSolution(), cell_petsc_index);

	// Zonzero initial guess for stage 1
	if (linear_system_solver_->getInitialGuessNonzero() == PETSC_TRUE)
	{
		VecCopy(solution_array_[2], delta_);
	}	

	// Stage 1
	system_matrix = zone->calculateSystemMatrix(diagonal_factor, BDF_beta_);
	VecCopy(zone->calculateSystemRHS(), stage_rhs_[0]);
	VecCopy(stage_rhs_[0], MEBDF_rhs_);
	VecScale(MEBDF_rhs_, BDF_beta_);
	for (int_t i = 1; i < current_time_order_ - 1; i++) // 1 ~ k-1 (k+1 order)
	{
		const real_t coeff = diagonal_factor * BDF_gamma_[i];
		VecAXPY(MEBDF_rhs_, coeff, history_array_[i]);
	}
	ierr_ = linear_system_solver_->Solve(system_matrix, MEBDF_rhs_, delta_);
	linear_solver_iteration = linear_system_solver_->getIterationNumber();
	zone->UpdateSolutionFromDelta(delta_);
	ConvertSolutionArray(solution_array_[1], num_cells, zone->getSolution(), cell_petsc_index);
	for (int_t i = 0; i < history_array_.size(); i++)
		VecCopy(history_array_[i], inter_history_array_[i]);
	UpdateHistoryArray(inter_history_array_, num_cells, zone->getSolution(), cell_petsc_index);

	// for debug
	//real_t test_error = 0;
	//VecNorm(MEBDF_rhs_, NORM_2, &test_error);
	//MASTER_MESSAGE("L2 = " + TO_STRING(test_error));

	zone->communication();
	zone->limiting();
	zone->pressurefix(current_time, iteration);

	// Zonzero initial guess for stage 2
	if (linear_system_solver_->getInitialGuessNonzero() == PETSC_TRUE)
	{		
		VecSet(delta_, 0.0);
		for (int_t i = 0; i < current_time_order_; i++)
			VecAXPY(delta_, 1.0, inter_history_array_[i]);
	}

	// Stage 2
	system_matrix = zone->calculateSystemMatrix(diagonal_factor, BDF_beta_);
	VecCopy(zone->calculateSystemRHS(), stage_rhs_[1]);
	VecCopy(stage_rhs_[1], MEBDF_rhs_);
	VecScale(MEBDF_rhs_, BDF_beta_);
	for (int_t i = 1; i < current_time_order_ - 1; i++) // 1 ~ k-1 (k+1 order)
	{
		const real_t coeff = diagonal_factor * BDF_gamma_[i];
		VecAXPY(MEBDF_rhs_, coeff, inter_history_array_[i]);
	}
	ierr_ = linear_system_solver_->Solve(system_matrix, MEBDF_rhs_, delta_);
	linear_solver_iteration = linear_system_solver_->getIterationNumber();
	zone->UpdateSolutionFromDelta(delta_);
	ConvertSolutionArray(solution_array_[2], num_cells, zone->getSolution(), cell_petsc_index);

	zone->communication();
	zone->limiting();
	zone->pressurefix(current_time, iteration);

	// Zonzero initial guess for stage 3
	if (linear_system_solver_->getInitialGuessNonzero() == PETSC_TRUE)
		VecCopy(solution_array_[1], delta_);

	// Stage 3
	system_matrix = zone->calculateSystemMatrix(diagonal_factor, BDF_beta_);
	VecCopy(zone->calculateSystemRHS(), MEBDF_rhs_);
	VecScale(MEBDF_rhs_, MEBDF_beta_[1]);
	VecAXPY(MEBDF_rhs_, MEBDF_beta_[0] - BDF_beta_, stage_rhs_[1]);
	VecAXPY(MEBDF_rhs_, BDF_beta_, stage_rhs_[0]);
	for (int_t i = 1; i < current_time_order_ - 1; i++) // 1 ~ k-1 (k+1 order)
	{
		const real_t coeff = diagonal_factor * MEBDF_gamma_[i];
		VecAXPY(MEBDF_rhs_, coeff, history_array_[i]);
	}
	ierr_ = linear_system_solver_->Solve(system_matrix, MEBDF_rhs_, delta_);
	linear_solver_iteration = linear_system_solver_->getIterationNumber();

	//linear_system_solver_->PostMatrix(system_matrix, "Matrix" + TO_STRING(iteration));
	//linear_system_solver_->PostVector(MEBDF_rhs_, "bVector" + TO_STRING(iteration));
	//linear_system_solver_->PostVector(delta_, "xVector" + TO_STRING(iteration));

	VecAXPY(delta_, 1.0, solution_array_[0]);
	VecAXPY(delta_, -1.0, solution_array_[2]);
	zone->UpdateSolutionFromDelta(delta_);
	UpdateHistoryArray(history_array_, num_cells, zone->getSolution(), cell_petsc_index);

	real_t error = 0;
	VecNorm(MEBDF_rhs_, NORM_2, &error);
	status_string_ = "KrylovIteration=" + TO_STRING(linear_solver_iteration) + " L2=" + TO_STRING((double)error);

	zone->communication();
	zone->limiting();
	zone->pressurefix(current_time, iteration);
}

void TimeUnstLMEBDF::InitializeVectors(std::shared_ptr<Zone> zone, const real_t dt)
{
	const Vec& vec_initializer = zone->getSystemRHS();
	ierr_ = VecDuplicate(vec_initializer, &delta_);
	ierr_ = VecDuplicate(vec_initializer, &MEBDF_rhs_);
	for (int_t i = 0; i < history_array_.size(); i++)
		ierr_ = VecDuplicate(vec_initializer, &history_array_[i]);
	for (int_t i = 0; i < inter_history_array_.size(); i++)
		ierr_ = VecDuplicate(vec_initializer, &inter_history_array_[i]);
	for (int_t i = 0; i < stage_rhs_.size(); i++)
		ierr_ = VecDuplicate(vec_initializer, &stage_rhs_[i]);
	for (int_t i = 0; i < solution_array_.size(); i++)
		ierr_ = VecDuplicate(vec_initializer, &solution_array_[i]);
#ifdef _Order_Test_
	GenerateInitialHistory(zone, dt); // for order test
#else
	UpdateHistoryArray(history_array_, zone->getNumCells(), zone->getSolution(), zone->getCellPetscIndex()); // for order test(x)
	UpdateHistoryArray(inter_history_array_, zone->getNumCells(), zone->getSolution(), zone->getCellPetscIndex()); // for order test(x)
#endif
	is_initialized_ = true;
}

void TimeUnstLMEBDF::InitializeCoeff()
{
	if (current_time_order_ > 8)
		MASTER_ERROR("Time order of MEBDF cannot exceed 8 : current time order (" + TO_STRING(current_time_order_) + ")");
	else if (current_time_order_ > 4)
		MASTER_MESSAGE("Warnning!!! MEBDF is no more A-stable : current time order (" + TO_STRING(current_time_order_) + ")");

	BDF_gamma_.clear();
	MEBDF_gamma_.clear();
	BDF_gamma_.resize(current_time_order_ - 1);
	MEBDF_gamma_.resize(current_time_order_ - 1);
	switch (current_time_order_)
	{
	case 2:
		BDF_gamma_ = { 1.0 };
		BDF_beta_ = 1.0;
		MEBDF_gamma_ = { 1.0 };
		MEBDF_beta_ = { 3.0 / 2.0, -1.0 / 2.0 };
		break;
	case 3:
		BDF_gamma_ = { 1.0, 1.0 / 3.0 };
		BDF_beta_ = 2.0 / 3.0;
		MEBDF_gamma_ = { 1.0, 5.0 / 23.0 };
		MEBDF_beta_ = { 22.0 / 23.0, -4.0 / 23.0 };
		break;
	case 4:
		BDF_gamma_ = { 1.0, 5.0 / 11.0, 2.0 / 11.0 };
		BDF_beta_ = 6.0 / 11.0;
		MEBDF_gamma_ = { 1.0, 65.0 / 197.0, 17.0 / 197.0 };
		MEBDF_beta_ = { 150.0 / 197.0, -18.0 / 197.0 };
		break;
	case 5:
		BDF_gamma_ = { 1.0, 13.0 / 25.0, 7.0 / 25.0, 3.0 / 25.0 };
		BDF_beta_ = 12.0 / 25.0;
		MEBDF_gamma_ = { 1.0, 1001.0 / 2501.0, 395.0 / 2501.0, 111.0 / 2501.0 };
		MEBDF_beta_ = { 1644.0 / 2501.0, -144.0 / 2501.0 };
		break;
	case 6:
		BDF_gamma_ = { 1.0, 77.0 / 137.0, 47.0 / 137.0, 27.0 / 137.0, 12.0 / 137.0 };
		BDF_beta_ = 60.0 / 137.0;
		MEBDF_gamma_ = { 1.0, 6699.0 / 14919.0, 3189.0 / 14919.0, 1349.0 / 14919.0, 394.0 / 14919.0 };
		MEBDF_beta_ = { 8820.0 / 14919.0, -600.0 / 14919.0 };
		break;
	case 7:
		BDF_gamma_ = { 1.0, 87.0 / 147.0, 57.0 / 147.0, 37.0 / 147.0, 22.0 / 147.0, 10.0 / 147.0 };
		BDF_beta_ = 60.0 / 147.0;
		MEBDF_gamma_ = { 1.0, 19401.0 / 39981.0, 10311.0 / 39981.0, 5251.0 / 39981.0, 2306.0 / 39981.0, 690.0 / 39981.0 };
		MEBDF_beta_ = { 21780.0 / 39981.0, -1200.0 / 39981.0 };
		break;
	case 8:
		BDF_gamma_ = { 1.0, 669.0 / 1089.0, 459.0 / 1089.0, 319.0 / 1089.0, 214.0 / 1089.0, 130.0 / 1089.0, 60.0 / 1089.0 };
		BDF_beta_ = 420.0 / 1089.0;
		MEBDF_gamma_ = { 1.0, 321789.0 / 626709.0, 184029.0 / 626709.0, 104439.0 / 626709.0, 55159.0 / 626709.0, 24800.0 / 626709.0, 7545.0 / 626709.0 };
		MEBDF_beta_ = { 319620.0 / 626709.0, -14700.0 / 626709.0 };
		break;
	default:;
	}
}

const Mat& TimeUnstLMEBDF::UpdateSystemMatrix(std::shared_ptr<Zone> zone, const real_t diagonal_factor, const real_t beta)
{
	//const real_t convergence_rate = WRMS_norm_[1] / WRMS_norm_[0];
	//const real_t tolerance = 0.25;
	//if ((recompute_step_ >= 10) || (convergence_rate >= tolerance)) recompute_step_ = 0;
	//if (recompute_step_ != 0)
	//	return zone->getSystemMatrix();
	//else return zone->calculateSystemMatrix(diagonal_factor, beta);
	return zone->calculateSystemMatrix(diagonal_factor, beta);
}

void TimeUnstLMEBDF::ConvertSolutionArray(Vec& vector, const int_t num_cells, const std::vector<std::vector<real_t> >& solution, const std::vector<int_t>& cell_petsc_index)
{
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	PetscScalar *vector_array;
	const PetscInt num_var = solution[0].size();
	VecGetArray(vector, &vector_array);
	for (int_t icell = 0; icell < num_cells; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			vector_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar] = solution[icell][ivar];
	VecRestoreArray(vector, &vector_array);
}

void TimeUnstLMEBDF::UpdateHistoryArray(std::vector<Vec>& history_array, const int_t num_cells, const std::vector<std::vector<real_t> >& solution, const std::vector<int_t>& cell_petsc_index)
{
	Vec buff;
	VecCreate(MPI_COMM_WORLD, &buff);
	VecDuplicate(history_array[0], &buff);
	ConvertSolutionArray(buff, num_cells, solution, cell_petsc_index);
	for (int_t i = 0; i < current_time_order_ - 1; i++)
	{
		VecAYPX(history_array[i], -1.0, buff);
		VecSwap(history_array[i], buff);
	}
	VecCopy(buff, history_array[current_time_order_ - 1]);
	VecDestroy(&buff);
}

void TimeUnstLMEBDF::RescaleHistoryArray(const real_t eta)
{
	std::vector<std::vector<real_t> > rescaling_matrix(6);
	for (int_t i = 0; i < 6; i++)
		rescaling_matrix[i].resize(6, 0.0);
	rescaling_matrix[0][0] = 1.0;
	for (int_t i = 1; i < 6; i++)
		rescaling_matrix[i][i] = eta * rescaling_matrix[i - 1][i - 1];
	for (int_t i = 2; i < 6; i++)
		rescaling_matrix[1][i] = rescaling_matrix[1][i - 1] * (i - 1 - eta) / (double)i;
	rescaling_matrix[2][3] = (1.0 - eta)*rescaling_matrix[2][2];
	rescaling_matrix[2][4] = rescaling_matrix[2][3] * (11.0 - 7.0*eta) / 12.0;
	rescaling_matrix[2][5] = rescaling_matrix[2][3] * (3.0*eta - 5.0)*(eta - 2.0) / 12.0;
	rescaling_matrix[3][4] = rescaling_matrix[3][3] * 1.5*(1.0 - eta); rescaling_matrix[3][5] = rescaling_matrix[3][4] * (7.0 - 5.0*eta) / 6.0;
	rescaling_matrix[4][5] = rescaling_matrix[4][4] * 2.0*(1 - eta);

	for (int_t i = 1; i < current_time_order_; i++)
	{
		VecScale(history_array_[i], rescaling_matrix[i][i]);
		for (int_t j = i + 1; j < current_time_order_; j++)
			VecAXPY(history_array_[i], rescaling_matrix[i][j], history_array_[j]);
	}
}

void TimeUnstLMEBDF::Adaptation()
{
	if (current_time_order_ < time_order_)
	{
		current_time_order_++;
		InitializeCoeff();
	}		
}

void TimeUnstLMEBDF::GenerateInitialHistory(std::shared_ptr<Zone> zone, const real_t dt)
{
	std::vector<Vec> solution_vector(time_order_);
	for (int_t i = 0; i < solution_vector.size(); i++)
	{
		VecDuplicate(history_array_[0], &solution_vector[i]);
		ConvertSolutionArray(solution_vector[i], zone->getNumCells(), zone->getExactSolution(-dt * (double)i), zone->getCellPetscIndex());
	}
	VecCopy(solution_vector[0], history_array_[0]);
	if (time_order_ >= 2)
	{
		VecCopy(solution_vector[0], history_array_[1]); VecAXPY(history_array_[1], -1.0, solution_vector[1]);
	}
	if (time_order_ >= 3)
	{
		VecCopy(solution_vector[0], history_array_[2]); VecAXPY(history_array_[2], -2.0, solution_vector[1]); VecAXPY(history_array_[2], 1.0, solution_vector[2]);
	}
	if (time_order_ >= 4)
	{
		VecCopy(solution_vector[0], history_array_[3]); VecAXPY(history_array_[3], -3.0, solution_vector[1]);
		VecAXPY(history_array_[3], 3.0, solution_vector[2]); VecAXPY(history_array_[3], -1.0, solution_vector[3]);
	}
	if (time_order_ >= 5)
	{
		VecCopy(solution_vector[0], history_array_[4]); VecAXPY(history_array_[4], -4.0, solution_vector[1]);
		VecAXPY(history_array_[4], 6.0, solution_vector[2]); VecAXPY(history_array_[4], -4.0, solution_vector[3]);
		VecAXPY(history_array_[4], 1.0, solution_vector[4]);
	}
}