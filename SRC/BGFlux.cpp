
#include "../INC/BGFlux.h"

BGFlux::BGFlux()
{
	readData("BGEquation", INPUT() + "equation.dat");
}

void BGFlux::readData(const std::string& region, const std::string& data_file_name)
{
	std::ifstream infile(data_file_name.c_str());
	const std::string region_start = "@" + region;
	const std::string region_end = "@End" + region;
	std::string text;
	bool flag = false;
	while (getline(infile, text)){
		if (text.find(region_start, 0) != std::string::npos){
			flag = true;
			break;
		}
	}
	if (flag == false){
		infile.close(); return;
	}

	while (getline(infile, text)){
		if (text.find("~>dimension_", 0) != std::string::npos)
			infile >> dimension_;
		if (text.find("~>diffusion_on_", 0) != std::string::npos){
			getline(infile, text);
			if (!text.compare("on")) diffusion_on_ = true;
			else diffusion_on_ = false;
		}
		if (text.find("~>diffusion_", 0) != std::string::npos){
			if (diffusion_on_ == true){
				diffusion_.resize(dimension_*dimension_);
				diffusion_wave_.resize(dimension_);
				if (dimension_ == 2){
					infile >> diffusion_[0] >> diffusion_[1] >> diffusion_[2];
					infile >> diffusion_[2] >> diffusion_[3];
					arma::mat diff(2, 2);
					for (int_t i = 0; i < dimension_;i++)
					for (int_t j = 0; j < dimension_; j++)
						diff(i, j) = diffusion_[i*dimension_ + j];
					arma::cx_vec val;
					arma::eig_gen(val, diff);
					diffusion_wave_[0] = std::max(std::abs(val(0)), std::abs(val(1)));
					diffusion_wave_[1] = diffusion_wave_[0];
				}
				else if (dimension_ == 3){
					infile >> diffusion_[0] >> diffusion_[1] >> diffusion_[2];
					infile >> diffusion_[3] >> diffusion_[4] >> diffusion_[5];
					infile >> diffusion_[6] >> diffusion_[7] >> diffusion_[8];
					arma::mat diff(3, 3);
					for (int_t i = 0; i < dimension_; i++)
					for (int_t j = 0; j < dimension_; j++)
						diff(i, j) = diffusion_[i*dimension_ + j];
					arma::cx_vec val;
					arma::eig_gen(val, diff);
					diffusion_wave_[0] = std::max(std::abs(val(0)), std::max(std::abs(val(1)), std::abs(val(2))));
					diffusion_wave_[1] = diffusion_wave_[0];
					diffusion_wave_[2] = diffusion_wave_[0];
				}
			}
		}
		if (text.find(region_end, 0) != std::string::npos)
			break;
	}
	infile.close();
}

const std::vector<real_t> BGFlux::commonConvFlux(const std::vector<real_t>& u) const
{
	int_t num_points = u.size();
	std::vector<real_t> Flux(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			Flux[ipoint*dimension_ + idim] = 0.5 * u[ipoint] * u[ipoint];
	return Flux;
}

const std::vector<real_t> BGFlux::commonConvFluxJacobian(const std::vector<real_t>& u) const // for implicit
{
	int_t num_points = u.size();
	std::vector<real_t> FluxJacobi(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			FluxJacobi[ipoint*dimension_ + idim] = u[ipoint];
	return FluxJacobi;
}

const std::vector<real_t> BGFlux::commonConvFlux(const std::vector<real_t>& u, const std::vector<real_t>& normal) const
{
	int_t num_points = u.size();
	std::vector<real_t> Flux(num_points);
	std::vector<real_t> an(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	for (int_t idim = 0; idim < dimension_; idim++)
		an[ipoint] += normal[ipoint*dimension_ + idim] * u[ipoint];
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		Flux[ipoint] = 0.5 * an[ipoint] * u[ipoint];
	return Flux;
}

const std::vector<real_t> BGFlux::commonConvFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = u.size();
	std::vector<real_t> FluxJacobi(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			FluxJacobi[ipoint] += normal[ipoint*dimension_ + idim] * u[ipoint];
	return FluxJacobi;
}

const std::vector<real_t> BGFlux::commonViscFlux(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const
{
	int_t num_points = u.size();
	std::vector<real_t> Flux(num_points*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points;ipoint++)
	for (int_t idim = 0; idim < dimension_; idim++)
	for (int_t jdim = 0; jdim < dimension_; jdim++)
		Flux[ipoint*dimension_ + idim] += diffusion_[idim*dimension_ + jdim] * div_u[ipoint*dimension_ + jdim];
	return Flux;
}

const std::vector<real_t> BGFlux::commonViscFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const // for implicit
{
	int_t num_points = u.size();
	std::vector<real_t> FluxJacobi(num_points*dimension_, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> BGFlux::commonViscFluxGradJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const // for implicit
{
	int_t num_points = u.size();
	std::vector<real_t> FluxGradJacobi(num_points*dimension_*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			for (int_t jdim = 0; jdim < dimension_; jdim++)
				FluxGradJacobi[ipoint*dimension_*dimension_ + idim * dimension_ + jdim] = diffusion_[idim*dimension_ + jdim];
	return FluxGradJacobi;
}

const std::vector<real_t> BGFlux::commonViscFlux(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = u.size();
	std::vector<real_t> Flux(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		std::vector<real_t> F(dimension_, 0.0);
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t jdim = 0; jdim < dimension_; jdim++)
			F[idim] += diffusion_[idim*dimension_ + jdim] * div_u[ipoint*dimension_ + jdim];
		for (int_t idim = 0; idim < dimension_; idim++)
			Flux[ipoint] += normal[ipoint*dimension_ + idim] * F[idim];
	}
	return Flux;
}

const std::vector<real_t> BGFlux::commonViscFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = u.size();
	std::vector<real_t> FluxJacobi(num_points, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> BGFlux::commonViscFluxGradJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = u.size();
	std::vector<real_t> FluxGradJacobi(num_points*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			for (int_t jdim = 0; jdim < dimension_; jdim++)
				FluxGradJacobi[ipoint*dimension_ + jdim] += normal[ipoint*dimension_ + idim] * diffusion_[idim*dimension_ + jdim];
	return FluxGradJacobi;
}

const std::vector<real_t> BGFlux::numViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size();
	std::vector<real_t> Flux(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++){
		std::vector<real_t> F(dimension_, 0.0);
		for (int_t idim = 0; idim < dimension_; idim++)
		for (int_t jdim = 0; jdim < dimension_; jdim++)
			F[idim] += diffusion_[idim*dimension_ + jdim] * (owner_div_u[ipoint*dimension_ + jdim] + neighbor_div_u[ipoint*dimension_ + jdim]);
		for (int_t idim = 0; idim < dimension_; idim++)
			Flux[ipoint] += 0.5* normal[ipoint*dimension_ + idim] * F[idim];
	}
	return Flux;
}

const std::vector<real_t> BGFlux::numViscFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = owner_u.size();
	std::vector<real_t> FluxJacobi(num_points, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> BGFlux::numViscFluxOwnerGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = owner_u.size();
	std::vector<real_t> FluxGradJacobi(num_points*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			for (int_t jdim = 0; jdim < dimension_; jdim++)
				FluxGradJacobi[ipoint*dimension_ + jdim] += 0.5*normal[ipoint*dimension_ + idim] * diffusion_[idim*dimension_ + jdim];
	return FluxGradJacobi;
}

const std::vector<real_t> BGFlux::numViscFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = owner_u.size();
	std::vector<real_t> FluxJacobi(num_points, 0.0);
	return FluxJacobi;
}

const std::vector<real_t> BGFlux::numViscFluxNeighborGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
	const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = neighbor_u.size();
	std::vector<real_t> FluxGradJacobi(num_points*dimension_, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			for (int_t jdim = 0; jdim < dimension_; jdim++)
				FluxGradJacobi[ipoint*dimension_ + jdim] += 0.5*normal[ipoint*dimension_ + idim] * diffusion_[idim*dimension_ + jdim];
	return FluxGradJacobi;
}

const std::vector<real_t> BGFlux::convWaveVelocity(const std::vector<real_t>& u) const
{
	int_t num_points = u.size();
	std::vector<real_t> wave(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		for (int_t idim = 0; idim < dimension_; idim++)
			wave[ipoint*dimension_ + idim] = std::abs(u[ipoint]);
	return wave;
}

const std::vector<real_t> BGFlux::viscWaveVelocity(const std::vector<real_t>& u) const
{
	int_t num_points = u.size();
	std::vector<real_t> wave(num_points*dimension_);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	for (int_t idim = 0; idim < dimension_; idim++)
		wave[ipoint*dimension_ + idim] = diffusion_wave_[idim];
	return wave;
}

const std::vector<real_t> BGFlux::source(const std::vector<real_t>& u) const
{
	int_t num_points = u.size();
	std::vector<real_t> source(num_points, 0.0);
	return source;
}

BGUpwind::BGUpwind() : eps_(0.0) // for implicit
{
	setName("Upwind");
	BGFlux::getInstance().addRegistry(getName(), this);
}

const std::vector<real_t> BGUpwind::numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const
{
	int_t num_points = owner_u.size();
	std::vector<real_t> Flux(num_points);
	std::vector<real_t> normal_sum(num_points, 0.0);
	std::vector<real_t> an(num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++) {
		for (int_t idim = 0; idim < dimension_; idim++)
			normal_sum[ipoint] += normal[ipoint*dimension_ + idim];
		an[ipoint] = normal_sum[ipoint]*(owner_u[ipoint] + neighbor_u[ipoint]);
	}
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		Flux[ipoint] = 0.25*(normal_sum[ipoint] * (owner_u[ipoint] * owner_u[ipoint] + neighbor_u[ipoint] * neighbor_u[ipoint]) - std::abs(an[ipoint])*(neighbor_u[ipoint] - owner_u[ipoint]));
	return Flux;
}

const std::vector<real_t> BGUpwind::numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = owner_u.size();
	std::vector<real_t> FluxJacobi(num_points);
	std::vector<real_t> normal_sum(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	{
		for (int_t idim = 0; idim < dimension_; idim++)
			normal_sum[ipoint] += normal[ipoint*dimension_ + idim];
		real_t sum = normal_sum[ipoint] * (owner_u[ipoint] + neighbor_u[ipoint]);
		if (sum >= eps_)
			FluxJacobi[ipoint] = normal_sum[ipoint] * owner_u[ipoint];
		else if (sum < -eps_)
			FluxJacobi[ipoint] = 0.0;
		else
			FluxJacobi[ipoint] = normal_sum[ipoint] * (0.5*owner_u[ipoint] - 0.125*(neighbor_u[ipoint] * neighbor_u[ipoint] - 2.0*neighbor_u[ipoint] * owner_u[ipoint] - 3.0*owner_u[ipoint] * owner_u[ipoint]) / eps_);
	}
	return FluxJacobi;
}

const std::vector<real_t> BGUpwind::numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const // for implicit
{
	int_t num_points = owner_u.size();
	std::vector<real_t> FluxJacobi(num_points);
	std::vector<real_t> normal_sum(num_points, 0.0);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
	{
		for (int_t idim = 0; idim < dimension_; idim++)
			normal_sum[ipoint] += normal[ipoint*dimension_ + idim];
		real_t sum = normal_sum[ipoint] * (owner_u[ipoint] + neighbor_u[ipoint]);
		if (sum >= eps_)
			FluxJacobi[ipoint] = 0.0;
		else if (sum < -eps_)
			FluxJacobi[ipoint] = normal_sum[ipoint] * neighbor_u[ipoint];
		else
			FluxJacobi[ipoint] = normal_sum[ipoint] * (0.5*neighbor_u[ipoint] - 0.125*(3.0*neighbor_u[ipoint] * neighbor_u[ipoint] + 2.0*owner_u[ipoint] * neighbor_u[ipoint] - owner_u[ipoint] * owner_u[ipoint]) / eps_);
	}
	return FluxJacobi;
}