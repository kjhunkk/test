
#include "../INC/Quadrature.h"


// using this function
void Quadrature::Volume::getVolumeQdrt(const ElemType elemtype, std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	switch (elemtype) {
	case ElemType::TRIS:
		getStdTrisQdrt(jacobian, degree, quad_points, quad_weights); break;
	case ElemType::QUAD:
		getStdQuadQdrt(jacobian, degree, quad_points, quad_weights); break;
	case ElemType::TETS:
		getStdTetsQdrt(jacobian, degree, quad_points, quad_weights); break;
	case ElemType::HEXA:
		getStdHexaQdrt(jacobian, degree, quad_points, quad_weights); break;
	case ElemType::PRIS:
		getStdPrisQdrt(jacobian, degree, quad_points, quad_weights); break;
	}
	return;
}

// experimental version. generate non-optimal quadrature on physical domain
bool Quadrature::Volume::getVolumePhyQdrt(const Element* element, const int_t element_order, const std::vector<real_t>& coords, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	ElemType elemtype = element->getElemType();
	
	return true;
}

// read vandermonde points for non-optimal quadrature on physical domain
bool Quadrature::Volume::readVanPts(const Element* element, const int_t degree, std::vector<real_t>& points)
{
	ElemType elemtype = element->getElemType();
	switch (elemtype) {
	case ElemType::TRIS:
	case ElemType::QUAD:
	case ElemType::TETS:
		break;
	default:
		return false; // dose not support until now
	}

	bool support;
	switch (elemtype) {
	case ElemType::TRIS:
		support = readVanTrisPts(degree, points);
		break;
	case ElemType::QUAD:
		support = readVanQuadPts(degree, points);
		break;
	case ElemType::TETS:
		support = readVanTetsPts(degree, points);
		break;
	}
	if (support == false)
		return false; // dose not support input degree until now
	return true;
}
bool Quadrature::Volume::readVanTrisPts(const int_t degree, std::vector<real_t>& points)
{
	if (degree > 8)
		return false;

	const int_t num_points = (degree + 1)*(degree + 2) / 2;

	char npoint_name[5]; sprintf(npoint_name, "%d", num_points);
	const std::string filename = RESOURCE() + "VanPoints/Tris/alpha-opt-n" + npoint_name + "-su.txt";

	std::ifstream infile(filename.c_str());
	std::vector<real_t> points_temp(num_points * 2);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		infile >> points_temp[2 * ipoint] >> points_temp[2 * ipoint + 1];

	infile.close();
	points = move(points_temp);
	return true;
}
bool Quadrature::Volume::readVanQuadPts(const int_t degree, std::vector<real_t>& points)
{
	if (degree > 7)
		return false;

	char degree_name[5]; sprintf(degree_name, "%d", degree);
	const std::string filename = RESOURCE() + "VanPoints/Quad/d" + degree_name + ".txt";

	std::ifstream infile(filename.c_str());
	int_t num_points;
	infile >> num_points;

	std::vector<real_t> points_temp(num_points * 2);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		infile >> points_temp[2 * ipoint] >> points_temp[2 * ipoint + 1];

	infile.close();
	points = move(points_temp);
	return true;
}
bool Quadrature::Volume::readVanTetsPts(const int_t degree, std::vector<real_t>& points)
{
	if (degree > 9)
		return false;

	const int_t num_points = (degree + 1)*(degree + 2)*(degree + 3) / 6;

	char npoint_name[5]; sprintf(npoint_name, "%d", num_points);
	const std::string filename = RESOURCE() + "VanPoints/Tets/alpha-opt-n" + npoint_name + "-su.txt";

	std::ifstream infile(filename.c_str());
	std::vector<real_t> points_temp(num_points * 3);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		infile >> points_temp[3 * ipoint] >> points_temp[3 * ipoint + 1] >> points_temp[3 * ipoint + 2];

	infile.close();
	points = move(points_temp);
	return true;
}


// get transformed standard element quadrature
void Quadrature::Volume::getStdTrisQdrt(std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	const int_t& element_order = jacobian->getOrder();
	int_t degree_req = element_order*degree + 2 * (element_order - 1);

	std::vector<real_t> quad_points_ST;
	readStdTrisQdrt(degree_req, quad_points_ST, quad_weights);

	std::vector<real_t> quad_points_temp = jacobian->calComToPhyCoord(quad_points_ST);
	std::vector<real_t> jacobian_val = jacobian->calJacobian(quad_points_ST);
	for (int_t ipoint = 0; ipoint < quad_weights.size(); ipoint++)
		quad_weights[ipoint] *= jacobian_val[ipoint];
	quad_points = move(quad_points_temp);
}
void Quadrature::Volume::getStdQuadQdrt(std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	const int_t& element_order = jacobian->getOrder();
	int_t degree_req = element_order*degree + 2 * element_order - 1;

	std::vector<real_t> quad_points_ST;
	readStdQuadQdrt(degree_req, quad_points_ST, quad_weights);

	std::vector<real_t> quad_points_temp = jacobian->calComToPhyCoord(quad_points_ST);
	std::vector<real_t> jacobian_val = jacobian->calJacobian(quad_points_ST);
	for (int_t ipoint = 0; ipoint < quad_weights.size(); ipoint++)
		quad_weights[ipoint] *= jacobian_val[ipoint];
	quad_points = move(quad_points_temp);
}
void Quadrature::Volume::getStdTetsQdrt(std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	const int_t& element_order = jacobian->getOrder();
	int_t degree_req = element_order*degree + 3 * (element_order - 1);

	std::vector<real_t> quad_points_ST;
	readStdTetsQdrt(degree_req, quad_points_ST, quad_weights);

	std::vector<real_t> quad_points_temp = jacobian->calComToPhyCoord(quad_points_ST);
	std::vector<real_t> jacobian_val = jacobian->calJacobian(quad_points_ST);
	for (int_t ipoint = 0; ipoint < quad_weights.size(); ipoint++)
		quad_weights[ipoint] *= jacobian_val[ipoint];
	quad_points = move(quad_points_temp);
}
void Quadrature::Volume::getStdHexaQdrt(std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	const int_t& element_order = jacobian->getOrder();
	int_t degree_req = element_order*degree + 3 * element_order - 1;

	std::vector<real_t> quad_points_ST;
	readStdHexaQdrt(degree_req, quad_points_ST, quad_weights);

	std::vector<real_t> quad_points_temp = jacobian->calComToPhyCoord(quad_points_ST);
	std::vector<real_t> jacobian_val = jacobian->calJacobian(quad_points_ST);
	for (int_t ipoint = 0; ipoint < quad_weights.size(); ipoint++)
		quad_weights[ipoint] *= jacobian_val[ipoint];
	quad_points = move(quad_points_temp);
}
void Quadrature::Volume::getStdPrisQdrt(std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	const int_t& element_order = jacobian->getOrder();
	int_t degree_req = element_order*degree + 3 * element_order - 2;

	std::vector<real_t> quad_points_ST;
	readStdPrisQdrt(degree_req, quad_points_ST, quad_weights);

	std::vector<real_t> quad_points_temp = jacobian->calComToPhyCoord(quad_points_ST);
	std::vector<real_t> jacobian_val = jacobian->calJacobian(quad_points_ST);
	for (int_t ipoint = 0; ipoint < quad_weights.size(); ipoint++)
		quad_weights[ipoint] *= jacobian_val[ipoint];
	quad_points = move(quad_points_temp);
}

// read standard element quadrature (optimal)
void Quadrature::Volume::readStdTrisQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	const std::vector<int_t> support_degree_list = { 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
	const std::vector<int_t> num_points_list = { 1, 3, 6, 7, 12, 15, 16, 19, 25, 28, 33, 37, 42, 49, 55, 60, 67, 73, 79 };

	if (degree > support_degree_list.back()) {
		readDgenStdTrisQdrt(degree, quad_points, quad_weights); return;
	}

	int_t ith = 0;
	for (ith = 0; ith < support_degree_list.size(); ith++)
		if (support_degree_list[ith] >= degree)
			break;

	const int_t& degree_ith = support_degree_list[ith];
	const int_t& num_points_ith = num_points_list[ith];

	char degree_name[5]; sprintf(degree_name, "%d", degree_ith);
	char npoint_name[5]; sprintf(npoint_name, "%d", num_points_ith);
	const std::string filename = RESOURCE() + "quadrules/tri/witherden-vincent-n" + npoint_name + "-d" + degree_name + "-sp.txt";

	std::ifstream infile(filename.c_str());
	std::vector<real_t> quad_points_temp(num_points_ith * 2);
	std::vector<real_t> quad_weights_temp(num_points_ith);
	for (int_t ipoint = 0; ipoint < num_points_ith; ipoint++)
		infile >> quad_points_temp[2 * ipoint] >> quad_points_temp[2 * ipoint + 1] >> quad_weights_temp[ipoint];

	infile.close();
	quad_points = move(quad_points_temp);
	quad_weights = move(quad_weights_temp);
}
void Quadrature::Volume::readStdQuadQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	int_t num_points = std::ceil(double(degree + 1)*0.5);
	std::vector<real_t> GL_points;
	std::vector<real_t> GL_weights;
	readGaussLegendrePoints(num_points, GL_points, GL_weights);

	std::vector<real_t> quad_points_temp(num_points*num_points * 2);
	std::vector<real_t> quad_weights_temp(num_points*num_points);
	int_t index = 0;
	for (int_t i = 0; i < num_points; i++) {
		for (int_t j = 0; j < num_points; j++) {
			quad_points_temp[2 * index] = GL_points[i];
			quad_points_temp[2 * index + 1] = GL_points[j];
			quad_weights_temp[index] = GL_weights[i] * GL_weights[j];
			index++;
		}
	}
	quad_points = move(quad_points_temp);
	quad_weights = move(quad_weights_temp);
}
void Quadrature::Volume::readStdTetsQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	const std::vector<int_t> support_degree_list = { 1, 2, 3, 5, 6, 7, 8, 9, 10 };
	const std::vector<int_t> num_points_list = { 1, 4, 8, 14, 24, 35, 46, 59, 81 };

	if (degree > support_degree_list.back()) {
		readDgenStdTetsQdrt(degree, quad_points, quad_weights); return;
	}

	int_t ith = 0;
	for (ith = 0; ith < support_degree_list.size(); ith++)
		if (support_degree_list[ith] >= degree)
			break;

	const int_t& degree_ith = support_degree_list[ith];
	const int_t& num_points_ith = num_points_list[ith];

	char degree_name[5]; sprintf(degree_name, "%d", degree_ith);
	char npoint_name[5]; sprintf(npoint_name, "%d", num_points_ith);
	const std::string filename = RESOURCE() + "quadrules/tet/witherden-vincent-n" + npoint_name + "-d" + degree_name + "-sp.txt";

	std::ifstream infile(filename.c_str());
	std::vector<real_t> quad_points_temp(num_points_ith * 3);
	std::vector<real_t> quad_weights_temp(num_points_ith);
	for (int_t ipoint = 0; ipoint < num_points_ith; ipoint++)
		infile >> quad_points_temp[3 * ipoint] >> quad_points_temp[3 * ipoint + 1] >> quad_points_temp[3 * ipoint + 2] >> quad_weights_temp[ipoint];

	infile.close();
	quad_points = move(quad_points_temp);
	quad_weights = move(quad_weights_temp);
}
void Quadrature::Volume::readStdHexaQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	int_t num_points = std::ceil(double(degree + 1)*0.5);
	std::vector<real_t> GL_points;
	std::vector<real_t> GL_weights;
	readGaussLegendrePoints(num_points, GL_points, GL_weights);

	std::vector<real_t> quad_points_temp(num_points*num_points*num_points * 3);
	std::vector<real_t> quad_weights_temp(num_points*num_points*num_points);
	int_t index = 0;
	for (int_t i = 0; i < num_points; i++) {
		for (int_t j = 0; j < num_points; j++) {
			for (int_t k = 0; k < num_points; k++) {
				quad_points_temp[3 * index] = GL_points[i];
				quad_points_temp[3 * index + 1] = GL_points[j];
				quad_points_temp[3 * index + 2] = GL_points[k];
				quad_weights_temp[index] = GL_weights[i] * GL_weights[j] * GL_weights[k];
				index++;
			}
		}
	}
	quad_points = move(quad_points_temp);
	quad_weights = move(quad_weights_temp);
}
void Quadrature::Volume::readStdPrisQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	int_t num_points = std::ceil(double(degree + 2)*0.5);
	std::vector<real_t> GL_points;
	std::vector<real_t> GL_weights;
	readGaussLegendrePoints(num_points, GL_points, GL_weights);

	std::vector<real_t> tris_points;
	std::vector<real_t> tris_weights;
	readStdTrisQdrt(degree, tris_points, tris_weights);

	std::vector<real_t> quad_points_temp(num_points*tris_weights.size() * 3);
	std::vector<real_t> quad_weights_temp(num_points*tris_weights.size());
	int_t index = 0;
	for (int_t i = 0; i < num_points; i++) {
		for (int_t j = 0; j < tris_weights.size(); j++) {
			quad_points_temp[3 * index] = tris_points[2 * j];
			quad_points_temp[3 * index + 1] = tris_points[2 * j + 1];
			quad_points_temp[3 * index + 2] = GL_points[i];
			quad_weights_temp[index] = tris_weights[j] * GL_weights[i];
			index++;
		}
	}
	quad_points = move(quad_points_temp);
	quad_weights = move(quad_weights_temp);
}

// read degenerated standard element quadrature for high degree
void Quadrature::Volume::readDgenStdTrisQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	int_t num_points = std::ceil(double(degree + 2)*0.5);
	std::vector<real_t> GL_points;
	std::vector<real_t> GL_weights;
	readGaussLegendrePoints(num_points, GL_points, GL_weights);

	std::vector<real_t> quad_points_temp(num_points*num_points * 2);
	std::vector<real_t> quad_weights_temp(num_points*num_points);
	int_t index = 0;
	for (int_t i = 0; i < num_points; i++) {
		for (int_t j = 0; j < num_points; j++) {
			quad_points_temp[2 * index] = 0.5*(GL_points[i] * (1.0 - GL_points[j]) - (1.0 + GL_points[j]));
			quad_points_temp[2 * index + 1] = GL_points[j];
			quad_weights_temp[index] = GL_weights[i] * GL_weights[j] * (1.0 - GL_points[j])*0.5;
			index++;
		}
	}
	quad_points = move(quad_points_temp);
	quad_weights = move(quad_weights_temp);
}
void Quadrature::Volume::readDgenStdTetsQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights)
{
	int_t num_points = std::ceil(double(degree + 3)*0.5);
	std::vector<real_t> GL_points;
	std::vector<real_t> GL_weights;
	readGaussLegendrePoints(num_points, GL_points, GL_weights); // t-axis

	std::vector<real_t> quad_points_tris;
	std::vector<real_t> quad_weights_tris;
	readStdTrisQdrt(degree, quad_points_tris, quad_weights_tris); // r,s-plane
	int_t num_points_tris = quad_weights_tris.size();

	std::vector<real_t> quad_points_temp(num_points*num_points_tris * 3);
	std::vector<real_t> quad_weights_temp(num_points*num_points_tris);
	int_t index = 0;
	for (int_t i = 0; i < num_points; i++) {
		for (int_t j = 0; j < num_points_tris; j++) {
			quad_points_temp[3 * index] = 0.5*(quad_points_tris[2 * j] * (1.0 - GL_points[i]) - (1.0 + GL_points[i]));
			quad_points_temp[3 * index + 1] = 0.5*(quad_points_tris[2 * j + 1] * (1.0 - GL_points[i]) - (1.0 + GL_points[i]));
			quad_points_temp[3 * index + 2] = GL_points[i];
			quad_weights_temp[index] = GL_weights[i] * quad_weights_tris[j] * (1.0 - GL_points[i])*(1.0 - GL_points[i])*0.25;
			index++;
		}
	}
	quad_points = move(quad_points_temp);
	quad_weights = move(quad_weights_temp);
}

// elementary function
void Quadrature::Volume::readGaussLegendrePoints(const int_t num_points, std::vector<real_t>& GL_points, std::vector<real_t>& GL_weights)
{
	char npoint_name[5]; sprintf(npoint_name, "%d", num_points);
	const std::string filename = RESOURCE() + "GLPoints/n" + npoint_name + ".txt";
	std::ifstream infile(filename.c_str());

	std::vector<real_t> GL_points_temp(num_points);
	std::vector<real_t> GL_weights_temp(num_points);
	for (int_t ipoint = 0; ipoint < num_points; ipoint++)
		infile >> GL_points_temp[ipoint] >> GL_weights_temp[ipoint];

	infile.close();
	GL_points = move(GL_points_temp);
	GL_weights = move(GL_weights_temp);
}

// using this function
void Quadrature::Surface::getSurfaceQdrt(const ElemType elemtype, std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights, std::vector<real_t>& adjmat)
{
	std::vector<real_t> quad_points_com;
	const int_t element_order = jacobian->getOrder();

	switch (elemtype) {
	case ElemType::LINE:
		Quadrature::Volume::readGaussLegendrePoints(std::ceil(0.5*real_t(element_order*(degree + 1))), quad_points_com, quad_weights);
		break;
	case ElemType::TRIS:
		Quadrature::Volume::readStdTrisQdrt(element_order*(degree + 2) - 2, quad_points_com, quad_weights);
		break;
	case ElemType::QUAD:
		Quadrature::Volume::readStdQuadQdrt(element_order*(degree + 2) - 1, quad_points_com, quad_weights);
		break;
	}

	quad_points = jacobian->calComToPhyCoord(quad_points_com);
	adjmat = jacobian->calJacobianAdjMat(quad_points_com);

	return;
}