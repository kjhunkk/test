#include "../INC/MPIHeader.h"

int_t MPIHeader::myrank_;
int_t MPIHeader::ndomain_;
std::chrono::system_clock::time_point MPIHeader::start_time_;
std::chrono::system_clock::time_point MPIHeader::end_time_;
MPIHeader::MPIHeader()
{
	PetscErrorCode ierr; // for implicit
	int argc = 2;
	char **args = new char*[argc];
	args[0] = new char[0]; args[1] = new char[9];// args[2] = new char[5];
	args[0] = ""; args[1] = "-log-view";// args[2] = "-info";
	MPI_Init(0, 0);
	ierr = PetscInitialize(&argc, &args, (char*)0, (char*)0); // for implicit
	if (ierr) MASTER_MESSAGE("Petsc & MPI Initialization fail"); // for implicit
	else MASTER_MESSAGE("Petsc & MPI Initialization succeed"); // for implicit
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank_);
	MPI_Comm_size(MPI_COMM_WORLD, &ndomain_);
	//MASTER_MESSAGE("MPI Initialization");
	SYNCRO();
}
MPIHeader::~MPIHeader()
{
	MASTER_MESSAGE("Petsc & MPI Finalization"); // for implicit
	PetscFinalize(); // for implicit
	MPI_Finalize();
	//MASTER_MESSAGE("MPI Finalization");
}
void MPIHeader::Error(bool master_only, const std::string& message, const std::string& file, const int_t line)
{
	std::cout << "Error(" + file + "," + to_string(line) + "):" + message << std::endl;
	Message(master_only, "Error(" + file + "," + to_string(line) + "):" + message);
	MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
}
void MPIHeader::Message(bool master_only, const std::string& message)
{
	if (master_only){
		if (myrank_ == MASTER_NODE){
			std::cout << code_name_ << "(MASTER) >> " << message << std::endl;
		}
	}
	else{
		std::stringstream tmp;
		tmp << std::setw(6) << std::setfill('0') << myrank_;
		std::cout << code_name_ << "(" << tmp.str() << ") >> " << message << std::endl;
	}
}
void MPIHeader::ListPrint(bool master_only, const int_t rank, const std::string& header, const std::vector<int_t> list)
{
	if (master_only){
		if (myrank_ == MASTER_NODE){
			std::cout << header << " : ";
			for (auto&& alist : list)
				std::cout << alist << ", ";
			std::cout << std::endl;
		}
	}
	else{
		if (myrank_ == rank){
			std::cout << header << " : ";
			for (auto&& alist : list)
				std::cout << alist << ", ";
			std::cout << std::endl;
		}
	}
}
void MPIHeader::ListPrint(bool master_only, const int_t rank, const std::string& header, const std::vector<real_t> list)
{
	if (master_only){
		if (myrank_ == MASTER_NODE){
			std::cout << header << " : ";
			for (auto&& alist : list)
				std::cout << alist << ", ";
			std::cout << std::endl;
		}
	}
	else{
		if (myrank_ == rank){
			std::cout << header << " : ";
			for (auto&& alist : list)
				std::cout << alist << ", ";
			std::cout << std::endl;
		}
	}
}
int_t MPIHeader::getMyrank()
{
	return myrank_;
}
int_t MPIHeader::getNumDomain()
{
	return ndomain_;
}
const std::string MPIHeader::to_string(const int_t data)
{
	char str[20]; sprintf(str, "%d", data);
	return str;
}
const std::string MPIHeader::to_string(const real_t data)
{
	char str[20];
	if (std::abs(data) > 1e6 || 1e-3 > std::abs(data)) sprintf(str, "%e", data);
	else sprintf(str, "%lf", data);
	return str;
}
const std::string MPIHeader::to_uppercase(const std::string& str)
{
	std::string tmp = str;
	transform(tmp.begin(), tmp.end(), tmp.begin(), ::toupper);
	return tmp;
}
void MPIHeader::Syncro()
{
	MPI_Barrier(MPI_COMM_WORLD);
}
void MPIHeader::startStopWatch()
{
	start_time_ = std::chrono::system_clock::now();
}
real_t MPIHeader::stopStopWatch()
{
	end_time_ = std::chrono::system_clock::now();
	return getTimeStopWatch();
}
real_t MPIHeader::getTimeStopWatch()
{
	std::chrono::duration<real_t> gap = end_time_ - start_time_;
	return gap.count();
}

void MPIHeader::Pause(const std::string& file, const int_t line)
{
	if (myrank_ == MASTER_NODE)
	{
		std::cout << code_name_ << "(MASTER) >> pause at (" << file << ", " << line << ")" << std::endl;
		system("pause");
	}
	MPI_Barrier(MPI_COMM_WORLD);
}

void MPIHeader::MasterPause(const std::string& file, const int_t line)
{
	if (myrank_ == MASTER_NODE)
	{
		std::cout << code_name_ << "(MASTER) >> pause at (" << file << ", " << line << ")" << std::endl;
		system("pause");
	}
}

void MPIHeader::dataCommunication(const std::vector<real_t*>& send_data, const std::vector<int_t>& send_size, std::vector<real_t*>& recv_data, const std::vector<int_t>& recv_size)
{
	std::vector<MPI_Request> recv_req(ndomain_);
	std::vector<MPI_Status> recv_sta(ndomain_);
	std::vector<MPI_Request> send_req(ndomain_);
	std::vector<MPI_Status> send_sta(ndomain_);

	std::vector<int_t> send_trash(ndomain_, 0);
	std::vector<int_t> recv_trash(ndomain_, 0);
	for (int_t idomain = 0; idomain<ndomain_; idomain++) {
		if (recv_size[idomain] > 0)
			MPI_Irecv(&recv_data[idomain][0], recv_size[idomain], MPI_REAL_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&recv_trash[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain<ndomain_; idomain++) {
		if (send_size[idomain] > 0)
			MPI_Isend(&send_data[idomain][0], send_size[idomain], MPI_REAL_T, idomain, myrank_, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&send_trash[idomain], 1, MPI_INT_T, idomain, myrank_, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain_, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain_, &send_req[0], &send_sta[0]);
}

void MPIHeader::dataCommunication(const std::vector<int_t*>& send_data, const std::vector<int_t>& send_size, std::vector<int_t*>& recv_data, const std::vector<int_t>& recv_size)
{
	std::vector<MPI_Request> recv_req(ndomain_);
	std::vector<MPI_Status> recv_sta(ndomain_);
	std::vector<MPI_Request> send_req(ndomain_);
	std::vector<MPI_Status> send_sta(ndomain_);

	std::vector<int_t> send_trash(ndomain_, 0);
	std::vector<int_t> recv_trash(ndomain_, 0);
	for (int_t idomain = 0; idomain<ndomain_; idomain++) {
		if (recv_size[idomain] > 0)
			MPI_Irecv(&recv_data[idomain][0], recv_size[idomain], MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
		else
			MPI_Irecv(&recv_trash[idomain], 1, MPI_INT_T, idomain, idomain, MPI_COMM_WORLD, &recv_req[idomain]);
	}
	for (int_t idomain = 0; idomain<ndomain_; idomain++) {
		if (send_size[idomain] > 0)
			MPI_Isend(&send_data[idomain][0], send_size[idomain], MPI_INT_T, idomain, myrank_, MPI_COMM_WORLD, &send_req[idomain]);
		else
			MPI_Isend(&send_trash[idomain], 1, MPI_INT_T, idomain, myrank_, MPI_COMM_WORLD, &send_req[idomain]);
	}
	MPI_Waitall(ndomain_, &recv_req[0], &recv_sta[0]);
	MPI_Waitall(ndomain_, &send_req[0], &send_sta[0]);
}