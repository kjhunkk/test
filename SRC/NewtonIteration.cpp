#include "../INC/NewtonIteration.h"

NewtonIteration::NewtonIteration()
{
	const Config& config = Config::getInstance();

	newton_iteration_ = 0;
	newton_max_iteration_ = 10000;
	old_rms_error_ = 0.0;
	newton_convergence_tol_ = 0.1;
}

void NewtonIteration::Start()
{
	newton_iteration_ = 0;
	old_rms_error_ = 0.0;
}

void NewtonIteration::Initialize(const Vec& vector)
{
	const Config& config = Config::getInstance();
	VecDuplicate(vector, &old_solution_);
	VecDuplicate(vector, &solution_delta_);
	linear_system_solver_ = LinearSystemSolver::getInstance().getType(config.getLinearSystemSolver());
	linear_system_solver_->Initialize();
}

bool NewtonIteration::NewtonSolve(const Mat& A, const Vec& b, Vec& x, int_t& linear_solver_iteration, real_t& newton_convergence_ratio)
{
	linear_system_solver_->Solve(A, b, x);
	linear_solver_iteration = linear_system_solver_->getIterationNumber();

	VecWAXPY(solution_delta_, -1.0, old_solution_, x);
	real_t new_rms_error_ = CalRmsError(solution_delta_);
	newton_convergence_ratio = new_rms_error_ / old_rms_error_;

	old_rms_error_ = new_rms_error_;
	VecCopy(x, old_solution_);

	if (newton_convergence_ratio <= newton_convergence_tol_) return true;
	else if (++newton_iteration_ >= newton_max_iteration_) return true;
	else return false;
}

real_t NewtonIteration::CalRmsError(const Vec& x)
{
	real_t rms_error;
	VecNorm(x, NORM_2, &rms_error);
	return rms_error;
}