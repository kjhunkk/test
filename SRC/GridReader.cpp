#include "../INC/GridReader.h"

GmshGridReader::GmshGridReader()
{
	setName("Gmsh");
	GridReader::getInstance().addRegistry(getName(), this);
}

void GmshGridReader::open(const std::string& grid_file_name, const int_t dimension)
{
	dimension_ = dimension;
	infile_.open(grid_file_name.c_str());
	if (infile_.is_open()){
		MASTER_MESSAGE("Open Gmsh-type grid file: "+grid_file_name);
	}
	else{
		MASTER_ERROR("Fail to open Gmsh-type grid file: " + grid_file_name);
	}
	return;
}

void GmshGridReader::readGridInfo(int_t& num_global_nodes, int_t& num_global_cells, int_t& num_global_bdries, int_t& num_global_peribdries)
{
	infile_.clear();

	num_global_nodes_ = 0;
	num_global_cells_ = 0;
	num_global_bdries_ = 0;
	num_global_peribdries_ = 0;

	periodic_direction_[0] = BoundaryCondition::getInstance().getTagNumber("x_periodic");
	periodic_direction_[1] = BoundaryCondition::getInstance().getTagNumber("y_periodic");
	periodic_direction_[2] = BoundaryCondition::getInstance().getTagNumber("z_periodic");
	periodic_direction_[3] = BoundaryCondition::getInstance().getTagNumber("matching");

	std::string text;
	while (getline(infile_, text)){
		// allocate stream position of node
		if (text.find("$Nodes", 0) != std::string::npos){
			infile_ >> num_global_nodes_; pos_node_ = infile_.tellg();
			MASTER_MESSAGE("   ~> Number of nodes: " + TO_STRING(num_global_nodes_));
		}
		// allocate stream position of cell and boundary
		if (text.find("$Elements", 0) != std::string::npos){
			int_t num_total;
			infile_ >> num_total;
			MASTER_MESSAGE("   ~> Number of cells and boundaries: " + TO_STRING(num_total));
			std::vector<int_t> num_elem_types(static_cast<int_t>(ElemType::NUM_OF_ELEMENT), 0);
			int_t cell_index, gmsh_elem_type, elem_type, garbage, bdry_type;
			std::streampos pos_temp;
			bool cell_flag = false, bdry_flag = false;
			for (int_t icell = 0; icell < num_total; icell++){
				pos_temp = infile_.tellg(); getline(infile_, text);
				infile_ >> cell_index >> gmsh_elem_type >> garbage >> bdry_type;
				elem_type = GetElementTypeFromGmshType(gmsh_elem_type);
				if (isBoundary(elem_type, dimension_)){
					if (bdry_flag == false){
						pos_bdry_ = pos_temp;
						bdry_flag = true;
					}
					bool peribdry_flag = false;
					for (int_t idirection = 0; idirection < 4; idirection++){
						if (bdry_type == periodic_direction_[idirection]){
							num_global_peribdries_++;
							peribdry_flag = true;
							break;
						}
					}
					if (peribdry_flag == true) continue;
					num_global_bdries_++;
				}
				else {
					if (cell_flag == false){
						pos_cell_ = pos_temp;
						cell_flag = true;
					}
					num_global_cells_++; num_elem_types[elem_type]++;
				}
			}
			MASTER_MESSAGE("      ~> Number of cells: " + TO_STRING(num_global_cells_));
			for (int_t ielement = 0; ielement < static_cast<int_t>(ElemType::NUM_OF_ELEMENT); ielement++)
				MASTER_MESSAGE("         ~> Number of cells(" + TO_STRING(ielement) + "): " + TO_STRING(num_elem_types[ielement]));
			MASTER_MESSAGE("      ~> Number of boundaries: " + TO_STRING(num_global_bdries_));
			MASTER_MESSAGE("      ~> Number of periodic boundaries: " + TO_STRING(num_global_peribdries_));
		}
	}

	num_global_nodes = num_global_nodes_;
	num_global_cells = num_global_cells_;
	num_global_bdries = num_global_bdries_;
	num_global_peribdries = num_global_peribdries_;
}

void GmshGridReader::readNodeData(const int_t start_index, const int_t end_index, std::vector<real_t>& node_coords)
{
	infile_.clear();
	infile_.seekg(pos_node_);

	int_t num_nodes = end_index - start_index;
	node_coords.resize(num_nodes*dimension_);
	int_t node_index; std::array<real_t, 3> coords;
	for (int_t inode = 0; inode < start_index; inode++)
		infile_ >> node_index >> coords[0] >> coords[1] >> coords[2];
	for (int_t local_inode = 0; local_inode < num_nodes; local_inode++){
		infile_ >> node_index >> coords[0] >> coords[1] >> coords[2];
		int_t local_coords_index = local_inode*dimension_;
		for (int_t idim = 0; idim < dimension_; idim++)
			node_coords[local_coords_index + idim] = coords[idim];
	}
}

void GmshGridReader::readCellData(const int_t start_index, const int_t end_index, std::vector<int_t>& cell_to_node_ind, std::vector<int_t>& cell_to_node_ptr, std::vector<int_t>& cell_elem_type, std::vector<int_t>& cell_order, std::vector<int_t>& cell_to_subnode_ptr, std::vector<int_t>& cell_to_subnode_ind)
{
	infile_.clear();
	infile_.seekg(pos_cell_); 

	std::string text;
	for (int_t icell = 0; icell < start_index; icell++)
		getline(infile_, text);
	
	int_t num_cells = end_index - start_index;
	int_t cell_index, node_index, gmsh_elem_type, garbage, elem_type;
	cell_elem_type.resize(num_cells);
	cell_order.resize(num_cells);
	cell_to_subnode_ptr.resize(num_cells + 1);
	cell_to_subnode_ptr[0] = 0;
	
	//read subcell nodes
	for (int_t local_icell = 0; local_icell < num_cells; local_icell++){
		getline(infile_, text);
		infile_ >> cell_index >> gmsh_elem_type;
		elem_type = GetElementTypeFromGmshType(gmsh_elem_type);

		cell_elem_type[local_icell] = elem_type;
		cell_order[local_icell] = GetGmshElemOrder(gmsh_elem_type);
		infile_ >> garbage >> garbage >> garbage;
		for (int_t inode = 0; inode < getNumSubNodesPerElement(gmsh_elem_type); inode++){
			infile_ >> node_index;
			cell_to_subnode_ind.push_back(node_index - 1);
		}
		cell_to_subnode_ptr[local_icell + 1] = cell_to_subnode_ind.size();
	}

	//transform into cell nodes from subcell nodes
	cell_to_node_ptr.resize(num_cells + 1);
	cell_to_node_ptr.push_back(0);
	for (int_t local_icell = 0; local_icell < num_cells; local_icell++){
		for (int_t inode = 0; inode < getNumNodesPerElement(cell_elem_type[local_icell]); inode++)
			cell_to_node_ind.push_back(cell_to_subnode_ind[cell_to_subnode_ptr[local_icell] + inode]);
		cell_to_node_ptr[local_icell + 1] = cell_to_node_ind.size();
	}
}

void GmshGridReader::readBdryData(const int_t start_index, const int_t end_index, std::vector<int_t>& bdry_to_node_ptr, std::vector<int_t>& bdry_to_node_ind, std::vector<int_t>& bdry_type)
{
	infile_.clear();
	infile_.seekg(pos_bdry_);

	int_t bdry_index, gmsh_elem_type, garbage, bdry_tag, node_index, global_ibdry = -1;
	bdry_to_node_ptr.push_back(0);

	std::string text;
	for (int_t ibdry = 0; ibdry < num_global_bdries_ + num_global_peribdries_; ibdry++){
		getline(infile_, text);
		infile_ >> bdry_index >> gmsh_elem_type >> garbage >> bdry_tag >> garbage;

		bool bdry_flag = false;
		for (int_t idirection = 0; idirection < 4; idirection++){
			if (bdry_tag == periodic_direction_[idirection]){
				bdry_flag = true;
				break;
			}
		}
		if (bdry_flag == true) continue;

		global_ibdry++;
		if (global_ibdry < start_index || end_index <= global_ibdry) continue;

		bdry_type.push_back(bdry_tag);
		for (int_t inode = 0; inode < getNumNodesPerElement(GetElementTypeFromGmshType(gmsh_elem_type)); inode++){
			infile_ >> node_index;
			bdry_to_node_ind.push_back(node_index - 1);
		}
		bdry_to_node_ptr.push_back(bdry_to_node_ind.size());
	}
}

void GmshGridReader::readPeriBdryData(const int_t start_index, const int_t end_index, std::vector<int_t>& peribdry_to_node_ptr, std::vector<int_t>& peribdry_to_node_ind, std::vector<int_t>& peribdry_direction)
{
	infile_.clear();
	infile_.seekg(pos_bdry_);

	int_t bdry_index, gmsh_elem_type, garbage, bdry_tag, node_index, global_ibdry = -1;
	peribdry_to_node_ptr.push_back(0);

	std::string text;
	for (int_t ibdry = 0; ibdry < num_global_bdries_+num_global_peribdries_; ibdry++){
		getline(infile_, text);
		infile_ >> bdry_index >> gmsh_elem_type >> garbage >> bdry_tag >> garbage;

		bool peribdry_flag = false;
		for (int_t idirection = 0; idirection < 4; idirection++){
			if (bdry_tag == periodic_direction_[idirection]){
				global_ibdry++;
				bdry_tag = idirection;
				peribdry_flag = true;
				break;
			}
		}
		if (peribdry_flag == false) continue;
		if (global_ibdry < start_index || end_index <= global_ibdry) continue;

		peribdry_direction.push_back(bdry_tag);
		for (int_t inode = 0; inode < getNumNodesPerElement(GetElementTypeFromGmshType(gmsh_elem_type)); inode++){
			infile_ >> node_index;
			peribdry_to_node_ind.push_back(node_index - 1);
		}
		peribdry_to_node_ptr.push_back(peribdry_to_node_ind.size());
	}
}

void GmshGridReader::close()
{
	infile_.close();
}

bool GmshGridReader::isBoundary(const int_t elem_num_type, const int_t dimension)
{
	if (dimension == 2){
		switch (static_cast<ElemType>(elem_num_type)){
		case ElemType::LINE: return true;
		}
	}
	else if (dimension == 3){
		switch (static_cast<ElemType>(elem_num_type)){
		case ElemType::TRIS: return true;
		case ElemType::QUAD: return true;
		}
	}
	return false;
}

int_t GmshGridReader::getNumSubNodesPerElement(const int_t elem_num_type)
{
	switch (static_cast<GmshElemType>(elem_num_type)){
	case GmshElemType::LINE_P1: return 2;
	case GmshElemType::LINE_P2: return 3;
	case GmshElemType::LINE_P3: return 4;
	case GmshElemType::LINE_P4: return 5;
	case GmshElemType::LINE_P5: return 6;
	case GmshElemType::LINE_P6: return 7;
	case GmshElemType::TRIS_P1: return 3;
	case GmshElemType::TRIS_P2: return 6;
	case GmshElemType::TRIS_P3: return 10;
	case GmshElemType::TRIS_P4: return 15;
	case GmshElemType::TRIS_P5: return 21;
	case GmshElemType::QUAD_P1: return 4;
	case GmshElemType::QUAD_P2: return 9;
	case GmshElemType::QUAD_P3: return 16;
	case GmshElemType::QUAD_P4: return 25;
	case GmshElemType::QUAD_P5: return 36;
	case GmshElemType::QUAD_P6: return 49;
	case GmshElemType::TETS_P1: return 4;
	case GmshElemType::TETS_P2: return 10;
	case GmshElemType::TETS_P3: return 20;
	case GmshElemType::TETS_P4: return 35;
	case GmshElemType::TETS_P5: return 56;
	case GmshElemType::HEXA_P1: return 8;
	case GmshElemType::HEXA_P2: return 27;
	case GmshElemType::HEXA_P3: return 64;
	case GmshElemType::HEXA_P4: return 125;
	case GmshElemType::HEXA_P5: return 216;
	case GmshElemType::PRIS_P1: return 6;
	case GmshElemType::PRIS_P2: return 18;
	case GmshElemType::PRIS_P3: return 40;
	case GmshElemType::PRIS_P4: return 75;
	case GmshElemType::PRIS_P5: return 126;
	case GmshElemType::PYRA_P1: return 5;
	default:
		MASTER_ERROR("Gmsh-element type number(" + TO_STRING(elem_num_type) + ") is not supported");
	}
	return -1;
}

int_t GmshGridReader::getNumNodesPerElement(const int_t elem_num_type)
{
	switch (static_cast<ElemType>(elem_num_type)){
	case ElemType::LINE: return 2;
	case ElemType::TRIS: return 3;
	case ElemType::QUAD: return 4;
	case ElemType::TETS: return 4;
	case ElemType::HEXA: return 8;
	case ElemType::PRIS: return 6;
	case ElemType::PYRA: return 5;
	default:
		MASTER_ERROR("Element type number(" + TO_STRING(elem_num_type) + ") is not supported");
	}
	return -1;
}

int_t GmshGridReader::GetGmshElemOrder(const int_t elem_num_type)
{
	switch (static_cast<GmshElemType>(elem_num_type)){
	case GmshElemType::TRIS_P1: 
	case GmshElemType::QUAD_P1:
	case GmshElemType::TETS_P1:
	case GmshElemType::HEXA_P1:
	case GmshElemType::PRIS_P1:
	case GmshElemType::PYRA_P1: return 1;
	case GmshElemType::TRIS_P2:
	case GmshElemType::QUAD_P2:
	case GmshElemType::TETS_P2:
	case GmshElemType::HEXA_P2:
	case GmshElemType::PRIS_P2: return 2;
	case GmshElemType::TRIS_P3:
	case GmshElemType::QUAD_P3:
	case GmshElemType::TETS_P3:
	case GmshElemType::HEXA_P3:
	case GmshElemType::PRIS_P3: return 3;
	case GmshElemType::TRIS_P4:
	case GmshElemType::QUAD_P4:
	case GmshElemType::TETS_P4:
	case GmshElemType::HEXA_P4:
	case GmshElemType::PRIS_P4: return 4;
	case GmshElemType::TRIS_P5:
	case GmshElemType::QUAD_P5:
	case GmshElemType::TETS_P5:
	case GmshElemType::HEXA_P5:
	case GmshElemType::PRIS_P5: return 5;
	case GmshElemType::QUAD_P6: return 6;
	default:
		MASTER_ERROR("Gmsh-element type number(" + TO_STRING(elem_num_type) + ") is not supported");
	}
	return -1;
}

int_t GmshGridReader::GetElementTypeFromGmshType(const int_t elem_num_type)
{
	switch (static_cast<GmshElemType>(elem_num_type)){
	case GmshElemType::LINE_P1:
	case GmshElemType::LINE_P2:
	case GmshElemType::LINE_P3:
	case GmshElemType::LINE_P4:
	case GmshElemType::LINE_P5:
	case GmshElemType::LINE_P6:
		return static_cast<int_t>(ElemType::LINE);
	case GmshElemType::TRIS_P1:
	case GmshElemType::TRIS_P2:
	case GmshElemType::TRIS_P3:
	case GmshElemType::TRIS_P4:
	case GmshElemType::TRIS_P5:
		return static_cast<int_t>(ElemType::TRIS);
	case GmshElemType::QUAD_P1:
	case GmshElemType::QUAD_P2:
	case GmshElemType::QUAD_P3:
	case GmshElemType::QUAD_P4:
	case GmshElemType::QUAD_P5:
	case GmshElemType::QUAD_P6:
		return static_cast<int_t>(ElemType::QUAD);
	case GmshElemType::TETS_P1:
	case GmshElemType::TETS_P2:
	case GmshElemType::TETS_P3:
	case GmshElemType::TETS_P4:
	case GmshElemType::TETS_P5:
		return static_cast<int_t>(ElemType::TETS);
	case GmshElemType::HEXA_P1:
	case GmshElemType::HEXA_P2:
	case GmshElemType::HEXA_P3:
	case GmshElemType::HEXA_P4:
	case GmshElemType::HEXA_P5:
		return static_cast<int_t>(ElemType::HEXA);
	case GmshElemType::PRIS_P1:
	case GmshElemType::PRIS_P2:
	case GmshElemType::PRIS_P3:
	case GmshElemType::PRIS_P4:
	case GmshElemType::PRIS_P5:
		return static_cast<int_t>(ElemType::PRIS);
	case GmshElemType::PYRA_P1:
		return static_cast<int_t>(ElemType::PYRA);
	default:
		MASTER_ERROR("Gmsh-element type number(" + TO_STRING(elem_num_type) + ") is not supported");
	}
	return static_cast<int_t>(ElemType::NOT_IN_LIST);
}


// Gmsh2DTrisGridReader
Gmsh2DTrisGridReader::Gmsh2DTrisGridReader():
GmshGridReader(true)
{
	setName("Gmsh2DTris");
	GridReader::getInstance().addRegistry(getName(), this);
}


void Gmsh2DTrisGridReader::open(const std::string& grid_file_name, const int_t dimension)
{
	dimension_ = dimension;
	grid_file_name_ = grid_file_name;

	readOption();
	generateGmshFile();
	SYNCRO();
	infile_.open(grid_file_name.c_str());
	if (infile_.is_open()){
		MASTER_MESSAGE("Open Gmsh-type grid file: " + grid_file_name_);
	}
	else{
		MASTER_ERROR("Fail to open Gmsh-type grid file: " + grid_file_name_);
	}
	return;
}

void Gmsh2DTrisGridReader::readOption()
{
	std::ifstream option_infile(INPUT() + "option.dat");
	if (option_infile.is_open()){
		MASTER_MESSAGE("Open Gmsh-2D-tris-generation info file: " + INPUT() + "option.dat");
	}
	else{
		MASTER_ERROR("Fail to open Gmsh-2D-tris-generation info file: " + INPUT() + "option.dat");
	}

	std::string text;
	while (getline(option_infile, text)){
		if (text.find("$GridNames", 0) != std::string::npos){
			getline(option_infile, grid_file_name_);
			MASTER_MESSAGE("   ~> grid file name: " + grid_file_name_);
		}
		if (text.find("$Sizes", 0) != std::string::npos){
			start_coords_.resize(dimension_);
			end_coords_.resize(dimension_);
			for (int_t idim = 0; idim < dimension_;idim++)
				option_infile >> start_coords_[idim] >> end_coords_[idim];
			MASTER_MESSAGE("   ~> domain size: (" + TO_STRING(start_coords_[0]) + ", " + TO_STRING(start_coords_[1]) + ") to (" + TO_STRING(end_coords_[0]) + ", " + TO_STRING(end_coords_[1]) + ")");
		}
		if (text.find("$Nums", 0) != std::string::npos){
			num_division_.resize(dimension_);
			for (int_t idim = 0; idim < dimension_;idim++)
				option_infile >> num_division_[idim];
			MASTER_MESSAGE("   ~> number of elements: (" + TO_STRING(num_division_[0]) + ", " + TO_STRING(num_division_[1]) + ")");
		}
		if (text.find("$BdryTags", 0) != std::string::npos){
			bdry_tags_.resize(2*dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> bdry_tags_[idim * 2] >> bdry_tags_[idim * 2 + 1];
			MASTER_MESSAGE("   ~> x-direction boundary tags: (" + TO_STRING(bdry_tags_[0]) + ", " + TO_STRING(bdry_tags_[1]) + ")");
			MASTER_MESSAGE("   ~> y-direction boundary tags: (" + TO_STRING(bdry_tags_[2]) + ", " + TO_STRING(bdry_tags_[3]) + ")");
		}
	}
	option_infile.close();
}

void Gmsh2DTrisGridReader::generateGmshFile()
{
	if (MYRANK() != MASTER_NODE) return;

	MASTER_MESSAGE("Grid file is being generated..");
	
	std::ofstream outfile(grid_file_name_);

	outfile << "$MeshFormat" << std::endl;
	outfile << "Gmsh2DTris by " << CODENAME() << std::endl;
	outfile << "$EndMeshFormat" << std::endl;

	START();
	int_t num_nodes = (num_division_[0] + 1)*(num_division_[1] + 1);
	const real_t dx = (end_coords_[0] - start_coords_[0]) / double(num_division_[0]);
	const real_t dy = (end_coords_[1] - start_coords_[1]) / double(num_division_[1]);

	outfile << "$Nodes" << std::endl;
	outfile << num_nodes << std::endl;
	int_t index = 0;
	for (int_t iy = 0; iy < num_division_[1] + 1; iy++)
	for (int_t ix = 0; ix < num_division_[0] + 1; ix++)
		outfile << ++index << " " << start_coords_[0] + dx*double(ix) << " " << start_coords_[1] + dy*double(iy) << " 0" << std::endl;
	outfile << "$EndNodes" << std::endl;
	MASTER_MESSAGE("Generage node data(Time: " + TO_STRING(STOP()) + "s)");

	START();
	int_t num_elements = 2 * (num_nodes - 1);
	outfile << "$Elements" << std::endl;
	outfile << num_elements << std::endl;
	index = 0;
	//          3*
	//    4  --------- 3
	//    |            |
	//0*  |            |  1*
	//    1  --------- 2
	//          2*
	for (int_t ix = 0; ix < num_division_[0]; ix++) // 1 - 2
		outfile << ++index << " 1 0 " << bdry_tags_[2] << " 0 " << ix+1 << " " << ix + 2 << std::endl;
	for (int_t iy = 0; iy < num_division_[1]; iy++) // 2 - 3
		outfile << ++index << " 1 0 " << bdry_tags_[1] << " 0 " << (num_division_[0] + 1) * (iy + 1) << " " << (num_division_[0] + 1) * (iy + 2) << std::endl;
	for (int_t ix = 0; ix < num_division_[0]; ix++) // 4 - 3
		outfile << ++index << " 1 0 " << bdry_tags_[3] << " 0 " << ix + (num_division_[0] + 1)*num_division_[1] + 1 << " " << ix + (num_division_[0] + 1)*num_division_[1] + 2 << std::endl;
	for (int_t iy = 0; iy < num_division_[1]; iy++) // 1 - 4
		outfile << ++index << " 1 0 " << bdry_tags_[0] << " 0 " << (num_division_[0] + 1) * iy + 1 << " " << (num_division_[0] + 1) * (iy + 1) + 1 << std::endl;

	for (int_t iy = 0; iy < num_division_[1]; iy++){
		for (int_t ix = 0; ix < num_division_[0]; ix++){
			outfile << ++index << " 2 0 0 0 " << ix + iy*(num_division_[0] + 1) + 1 << " " << ix + iy*(num_division_[0] + 1) + 2 << " " << ix + (iy + 1)*(num_division_[0] + 1) + 2 << std::endl;
			outfile << ++index << " 2 0 0 0 " << ix + iy*(num_division_[0] + 1) + 1 << " " << ix + (iy + 1)*(num_division_[0] + 1) + 2 << " " << ix + (iy + 1)*(num_division_[0] + 1) + 1 << std::endl;
		}
	}
	outfile << "$EndElements" << std::endl;
	MASTER_MESSAGE("Generage element data(Time: " + TO_STRING(STOP()) + "s)");

	outfile.close();
}

// Gmsh2DQuadGridReader
Gmsh2DQuadGridReader::Gmsh2DQuadGridReader() :
GmshGridReader(true)
{
	setName("Gmsh2DQuad");
	GridReader::getInstance().addRegistry(getName(), this);
}


void Gmsh2DQuadGridReader::open(const std::string& grid_file_name, const int_t dimension)
{
	dimension_ = dimension;
	grid_file_name_ = grid_file_name;

	readOption();
	generateGmshFile();
	SYNCRO();
	infile_.open(grid_file_name.c_str());
	if (infile_.is_open()){
		MASTER_MESSAGE("Open Gmsh-type grid file: " + grid_file_name_);
	}
	else{
		MASTER_ERROR("Fail to open Gmsh-type grid file: " + grid_file_name_);
	}
}

void Gmsh2DQuadGridReader::readOption()
{
	std::ifstream option_infile(INPUT() + "option.dat");
	if (option_infile.is_open()){
		MASTER_MESSAGE("Open Gmsh-2D-quad-generation info file: " + INPUT() + "option.dat");
	}
	else{
		MASTER_ERROR("Fail to open Gmsh-2D-quad-generation info file: " + INPUT() + "option.dat");
	}

	std::string text;
	while (getline(option_infile, text)){
		if (text.find("$GridNames", 0) != std::string::npos){
			getline(option_infile, grid_file_name_);
			MASTER_MESSAGE("   ~> grid file name: " + grid_file_name_);
		}
		if (text.find("$Sizes", 0) != std::string::npos){
			start_coords_.resize(dimension_);
			end_coords_.resize(dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> start_coords_[idim] >> end_coords_[idim];
			MASTER_MESSAGE("   ~> domain size: (" + TO_STRING(start_coords_[0]) + ", " + TO_STRING(start_coords_[1]) + ") to (" + TO_STRING(end_coords_[0]) + ", " + TO_STRING(end_coords_[1]) + ")");
		}
		if (text.find("$Nums", 0) != std::string::npos){
			num_division_.resize(dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> num_division_[idim];
			MASTER_MESSAGE("   ~> number of elements: (" + TO_STRING(num_division_[0]) + ", " + TO_STRING(num_division_[1]) + ")");
		}
		if (text.find("$BdryTags", 0) != std::string::npos){
			bdry_tags_.resize(2 * dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> bdry_tags_[idim * 2] >> bdry_tags_[idim * 2 + 1];
			MASTER_MESSAGE("   ~> x-direction boundary tags: (" + TO_STRING(bdry_tags_[0]) + ", " + TO_STRING(bdry_tags_[1]) + ")");
			MASTER_MESSAGE("   ~> y-direction boundary tags: (" + TO_STRING(bdry_tags_[2]) + ", " + TO_STRING(bdry_tags_[3]) + ")");
		}
	}
	option_infile.close();
}

void Gmsh2DQuadGridReader::generateGmshFile()
{
	if (MYRANK() != MASTER_NODE) return;

	MASTER_MESSAGE("Grid file is being generated..");

	std::ofstream outfile(grid_file_name_);

	outfile << "$MeshFormat" << std::endl;
	outfile << "Gmsh2DQuad by " << CODENAME() << std::endl;
	outfile << "$EndMeshFormat" << std::endl;

	START();
	int_t num_nodes = (num_division_[0] + 1)*(num_division_[1] + 1);
	const real_t dx = (end_coords_[0] - start_coords_[0]) / double(num_division_[0]);
	const real_t dy = (end_coords_[1] - start_coords_[1]) / double(num_division_[1]);

	outfile << "$Nodes" << std::endl;
	outfile << num_nodes << std::endl;
	int_t index = 0;
	for (int_t iy = 0; iy < num_division_[1] + 1; iy++)
	for (int_t ix = 0; ix < num_division_[0] + 1; ix++)
		outfile << ++index << " " << start_coords_[0] + dx*double(ix) << " " << start_coords_[1] + dy*double(iy) << " 0" << std::endl;
	outfile << "$EndNodes" << std::endl;
	MASTER_MESSAGE("Generage node data(Time: " + TO_STRING(STOP()) + "s)");

	START();
	int_t num_elements = (num_division_[0] + 2)*(num_division_[1] + 2) - 4;
	outfile << "$Elements" << std::endl;
	outfile << num_elements << std::endl;
	index = 0;
	//          3*
	//    4  --------- 3
	//    |            |
	//0*  |            |  1*
	//    1  --------- 2
	//          2*
	for (int_t ix = 0; ix < num_division_[0]; ix++) // 1 - 2
		outfile << ++index << " 1 0 " << bdry_tags_[2] << " 0 " << ix + 1 << " " << ix + 2 << std::endl;
	for (int_t iy = 0; iy < num_division_[1]; iy++) // 2 - 3
		outfile << ++index << " 1 0 " << bdry_tags_[1] << " 0 " << (num_division_[0] + 1) * (iy + 1) << " " << (num_division_[0] + 1) * (iy + 2) << std::endl;
	for (int_t ix = 0; ix < num_division_[0]; ix++) // 4 - 3
		outfile << ++index << " 1 0 " << bdry_tags_[3] << " 0 " << ix + (num_division_[0] + 1)*num_division_[1] + 1 << " " << ix + (num_division_[0] + 1)*num_division_[1] + 2 << std::endl;
	for (int_t iy = 0; iy < num_division_[1]; iy++) // 1 - 4
		outfile << ++index << " 1 0 " << bdry_tags_[0] << " 0 " << (num_division_[0] + 1) * iy + 1 << " " << (num_division_[0] + 1) * (iy + 1) + 1 << std::endl;

	for (int_t iy = 0; iy < num_division_[1]; iy++)
	for (int_t ix = 0; ix < num_division_[0]; ix++)
		outfile << ++index << " 3 0 0 0 " << ix + iy*(num_division_[0] + 1) + 1 << " " << ix + iy*(num_division_[0] + 1) + 2 << " " << ix + (iy + 1)*(num_division_[0] + 1) + 2 << " " << ix + (iy + 1)*(num_division_[0] + 1) + 1 << std::endl;
	outfile << "$EndElements" << std::endl;
	MASTER_MESSAGE("Generage element data(Time: " + TO_STRING(STOP()) + "s)");

	outfile.close();
}

// Gmsh3DTetsGridReader
Gmsh3DTetsGridReader::Gmsh3DTetsGridReader() :
GmshGridReader(true)
{
	setName("Gmsh3DTets");
	GridReader::getInstance().addRegistry(getName(), this);
}

void Gmsh3DTetsGridReader::open(const std::string& grid_file_name, const int_t dimension)
{
	dimension_ = dimension;
	grid_file_name_ = grid_file_name;

	readOption();
	generateGmshFile();
	SYNCRO();
	infile_.open(grid_file_name.c_str());
	if (infile_.is_open()){
		MASTER_MESSAGE("Open Gmsh-type grid file: " + grid_file_name_);
	}
	else{
		MASTER_ERROR("Fail to open Gmsh-type grid file: " + grid_file_name_);
	}
}

void Gmsh3DTetsGridReader::readOption()
{
	std::ifstream option_infile(INPUT() + "option.dat");
	if (option_infile.is_open()){
		MASTER_MESSAGE("Open Gmsh-3D-tets-generation info file: " + INPUT() + "option.dat");
	}
	else{
		MASTER_ERROR("Fail to open Gmsh-3D-tets-generation info file: " + INPUT() + "option.dat");
	}

	std::string text;
	while (getline(option_infile, text)){
		if (text.find("$GridNames", 0) != std::string::npos){
			getline(option_infile, grid_file_name_);
			MASTER_MESSAGE("   ~> grid file name: " + grid_file_name_);
		}
		if (text.find("$Sizes", 0) != std::string::npos){
			start_coords_.resize(dimension_);
			end_coords_.resize(dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> start_coords_[idim] >> end_coords_[idim];
			MASTER_MESSAGE("   ~> domain size: (" + TO_STRING(start_coords_[0]) + ", " + TO_STRING(start_coords_[1]) + ", " + TO_STRING(start_coords_[2]) + ") to (" + TO_STRING(end_coords_[0]) + ", " + TO_STRING(end_coords_[1]) + ", " + TO_STRING(end_coords_[2]) + ")");
		}
		if (text.find("$Nums", 0) != std::string::npos){
			num_division_.resize(dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> num_division_[idim];
			MASTER_MESSAGE("   ~> number of elements: (" + TO_STRING(num_division_[0]) + ", " + TO_STRING(num_division_[1]) + ", " + TO_STRING(num_division_[2]) + ")");
		}
		if (text.find("$BdryTags", 0) != std::string::npos){
			bdry_tags_.resize(2 * dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> bdry_tags_[idim * 2] >> bdry_tags_[idim * 2 + 1];
			MASTER_MESSAGE("   ~> x-direction boundary tags: (" + TO_STRING(bdry_tags_[0]) + ", " + TO_STRING(bdry_tags_[1]) + ")");
			MASTER_MESSAGE("   ~> y-direction boundary tags: (" + TO_STRING(bdry_tags_[2]) + ", " + TO_STRING(bdry_tags_[3]) + ")");
			MASTER_MESSAGE("   ~> z-direction boundary tags: (" + TO_STRING(bdry_tags_[4]) + ", " + TO_STRING(bdry_tags_[5]) + ")");
		}
	}
	option_infile.close();
}

void Gmsh3DTetsGridReader::generateGmshFile()
{
	if (MYRANK() != MASTER_NODE) return;

	MASTER_MESSAGE("Grid file is being generated..");

	std::ofstream outfile(grid_file_name_);

	outfile << "$MeshFormat" << std::endl;
	outfile << "Gmsh3DTets by " << CODENAME() << std::endl;
	outfile << "$EndMeshFormat" << std::endl;

	START();
	int_t num_nodes = (num_division_[0] + 1)*(num_division_[1] + 1)*(num_division_[2] + 1);
	const real_t dx = (end_coords_[0] - start_coords_[0]) / double(num_division_[0]);
	const real_t dy = (end_coords_[1] - start_coords_[1]) / double(num_division_[1]);
	const real_t dz = (end_coords_[2] - start_coords_[2]) / double(num_division_[2]);

	outfile << "$Nodes" << std::endl;
	outfile << num_nodes << std::endl;
	int_t index = 0;
	for (int_t iz = 0; iz < num_division_[2] + 1; iz++)
	for (int_t iy = 0; iy < num_division_[1] + 1; iy++)
	for (int_t ix = 0; ix < num_division_[0] + 1; ix++)
		outfile << ++index << " " << start_coords_[0] + dx*double(ix) << " " << start_coords_[1] + dy*double(iy) << " " << start_coords_[2] + dz*double(iz) << std::endl;
	outfile << "$EndNodes" << std::endl;
	MASTER_MESSAGE("Generage node data(Time: " + TO_STRING(STOP()) + "s)");

	START();
	int_t num_elements = 6 * num_division_[0] * num_division_[1] * num_division_[2] + 4 * (num_division_[0] * num_division_[1] + num_division_[1] * num_division_[2] + num_division_[2] * num_division_[0]);
	outfile << "$Elements" << std::endl;
	outfile << num_elements << std::endl;
	index = 0;
	//		    5*
	//          z
	//	 3----------2
	//	 |\     ^   |\ 
	//	 | \ 0* |   | \
	//	 |  \   |   |  \
	//	 |   7------+---6
	//2* |   |  +-- | --|->y 3*
	//	 0---+---\--1   |
	//	 \   |    \  \  |
	//	  \  |     \  \ |
	//	   \ |  4*  x  \|
	//	     4----------5
	//               1*    
	for (int_t iy = 0; iy < num_division_[1]; iy++){ // 0 - 1 - 4 - 5
		for (int_t ix = 0; ix < num_division_[0]; ix++){
			outfile << ++index << " 2 0 " << bdry_tags_[4] << " 0 " << ix + iy*(num_division_[0] + 1) + 1 << " " << ix + (iy + 1)*(num_division_[0] + 1) + 1 << " " << ix + 2 + (iy + 1)*(num_division_[0] + 1) << std::endl;
			outfile << ++index << " 2 0 " << bdry_tags_[4] << " 0 " << ix + iy*(num_division_[0] + 1) + 1 << " " << ix + 2 + (iy + 1)*(num_division_[0] + 1) << " " << ix + 2 + iy*(num_division_[0] + 1) << std::endl;
		}
	}
	for (int_t iy = 0; iy < num_division_[1]; iy++){ // 3 - 2 - 7 - 6
		for (int_t ix = 0; ix < num_division_[0]; ix++){
			outfile << ++index << " 2 0 " << bdry_tags_[5] << " 0 " << ix + 1 + iy*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 1 + (iy + 1)*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + (iy + 1)*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;
			outfile << ++index << " 2 0 " << bdry_tags_[5] << " 0 " << ix + 1 + iy*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + (iy + 1)*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + iy*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;
			
		}
	}
	for (int_t iz = 0; iz < num_division_[2]; iz++){ // 1 - 2 - 3 - 0
		for (int_t iy = 0; iy < num_division_[1]; iy++){
			outfile << ++index << " 2 0 " << bdry_tags_[0] << " 0 " << iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << " " << (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << " " << (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << std::endl;
			outfile << ++index << " 2 0 " << bdry_tags_[0] << " 0 " << iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << " " << (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << " " << iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << std::endl;
		}
	}
	for (int_t iz = 0; iz < num_division_[2]; iz++){ // 4 - 5 - 6 - 7
		for (int_t iy = 0; iy < num_division_[1]; iy++){
			outfile << ++index << " 2 0 " << bdry_tags_[1] << " 0 " << num_division_[0] + 1 + iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << num_division_[0] + 1 + (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << num_division_[0] + 1 + (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;
			outfile << ++index << " 2 0 " << bdry_tags_[1] << " 0 " << num_division_[0] + 1 + iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << num_division_[0] + 1 + (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << num_division_[0] + 1 + iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;
		}
	}
	for (int_t iz = 0; iz < num_division_[2]; iz++){ // 0 - 3 - 4 - 7
		for (int_t ix = 0; ix < num_division_[0]; ix++){
			outfile << ++index << " 2 0 " << bdry_tags_[2] << " 0 " << ix + 1 + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;
			outfile << ++index << " 2 0 " << bdry_tags_[2] << " 0 " << ix + 1 + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 1 + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;
		}
	}
	for (int_t iz = 0; iz < num_division_[2]; iz++){ // 1 - 2 - 5 - 6
		for (int_t ix = 0; ix < num_division_[0]; ix++){
			outfile << ++index << " 2 0 " << bdry_tags_[3] << " 0 " << ix + 1 + num_division_[1] * (num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + num_division_[1] * (num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + num_division_[1] * (num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;
			outfile << ++index << " 2 0 " << bdry_tags_[3] << " 0 " << ix + 1 + num_division_[1] * (num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + num_division_[1] * (num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 1 + num_division_[1] * (num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;
		}
	}

	for (int_t iz = 0; iz < num_division_[2]; iz++){
		for (int_t iy = 0; iy < num_division_[1]; iy++){
			for (int_t ix = 0; ix < num_division_[0]; ix++){
				std::vector<int_t> nodes_index;
				nodes_index.push_back(ix + 1 + iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 1 + (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 1 + (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 1 + iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				for (auto&& sub_nodes_index : makeTetsFromHexa(nodes_index))
					outfile << ++index << " 4 0 0 0 " << sub_nodes_index[0] << " " << sub_nodes_index[1] << " " << sub_nodes_index[2] << " " << sub_nodes_index[3] << std::endl;
			}
		}
	}
	outfile << "$EndElements" << std::endl;
	MASTER_MESSAGE("Generage element data(Time: " + TO_STRING(STOP()) + "s)");

	outfile.close();
}
const std::vector<std::vector<int_t>> Gmsh3DTetsGridReader::makeTetsFromHexa(const std::vector<int_t>& nodes_index)
{
	std::vector<std::vector<int_t>> total_nodes_index;
	total_nodes_index.push_back({ nodes_index[0], nodes_index[1], nodes_index[2], nodes_index[6] });
	total_nodes_index.push_back({ nodes_index[0], nodes_index[1], nodes_index[5], nodes_index[6] });
	total_nodes_index.push_back({ nodes_index[0], nodes_index[2], nodes_index[3], nodes_index[6] });
	total_nodes_index.push_back({ nodes_index[0], nodes_index[3], nodes_index[6], nodes_index[7] });
	total_nodes_index.push_back({ nodes_index[0], nodes_index[4], nodes_index[5], nodes_index[6] });
	total_nodes_index.push_back({ nodes_index[0], nodes_index[4], nodes_index[6], nodes_index[7] });
	return total_nodes_index;
}

// Gmsh3DHexaGridReader
Gmsh3DHexaGridReader::Gmsh3DHexaGridReader() :
GmshGridReader(true)
{
	setName("Gmsh3DHexa");
	GridReader::getInstance().addRegistry(getName(), this);
}

void Gmsh3DHexaGridReader::open(const std::string& grid_file_name, const int_t dimension)
{
	dimension_ = dimension;
	grid_file_name_ = grid_file_name;

	readOption();
	generateGmshFile();
	SYNCRO();
	infile_.open(grid_file_name.c_str());
	if (infile_.is_open()){
		MASTER_MESSAGE("Open Gmsh-type grid file: " + grid_file_name_);
	}
	else{
		MASTER_ERROR("Fail to open Gmsh-type grid file: " + grid_file_name_);
	}
}

void Gmsh3DHexaGridReader::readOption()
{
	std::ifstream option_infile(INPUT() + "option.dat");
	if (option_infile.is_open()){
		MASTER_MESSAGE("Open Gmsh-3D-hexa-generation info file: " + INPUT() + "option.dat");
	}
	else{
		MASTER_ERROR("Fail to open Gmsh-3D-hexa-generation info file: " + INPUT() + "option.dat");
	}

	std::string text;
	while (getline(option_infile, text)){
		if (text.find("$GridNames", 0) != std::string::npos){
			getline(option_infile, grid_file_name_);
			MASTER_MESSAGE("   ~> grid file name: " + grid_file_name_);
		}
		if (text.find("$Sizes", 0) != std::string::npos){
			start_coords_.resize(dimension_);
			end_coords_.resize(dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> start_coords_[idim] >> end_coords_[idim];
			MASTER_MESSAGE("   ~> domain size: (" + TO_STRING(start_coords_[0]) + ", " + TO_STRING(start_coords_[1]) + ", " + TO_STRING(start_coords_[2]) + ") to (" + TO_STRING(end_coords_[0]) + ", " + TO_STRING(end_coords_[1]) + ", " + TO_STRING(end_coords_[2]) + ")");
		}
		if (text.find("$Nums", 0) != std::string::npos){
			num_division_.resize(dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> num_division_[idim];
			MASTER_MESSAGE("   ~> number of elements: (" + TO_STRING(num_division_[0]) + ", " + TO_STRING(num_division_[1]) + ", " + TO_STRING(num_division_[2]) + ")");
		}
		if (text.find("$BdryTags", 0) != std::string::npos){
			bdry_tags_.resize(2 * dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> bdry_tags_[idim * 2] >> bdry_tags_[idim * 2 + 1];
			MASTER_MESSAGE("   ~> x-direction boundary tags: (" + TO_STRING(bdry_tags_[0]) + ", " + TO_STRING(bdry_tags_[1]) + ")");
			MASTER_MESSAGE("   ~> y-direction boundary tags: (" + TO_STRING(bdry_tags_[2]) + ", " + TO_STRING(bdry_tags_[3]) + ")");
			MASTER_MESSAGE("   ~> z-direction boundary tags: (" + TO_STRING(bdry_tags_[4]) + ", " + TO_STRING(bdry_tags_[5]) + ")");
		}
	}
	option_infile.close();
}

void Gmsh3DHexaGridReader::generateGmshFile()
{
	if (MYRANK() != MASTER_NODE) return;

	MASTER_MESSAGE("Grid file is being generated..");

	std::ofstream outfile(grid_file_name_);

	outfile << "$MeshFormat" << std::endl;
	outfile << "Gmsh3DHexa by " << CODENAME() << std::endl;
	outfile << "$EndMeshFormat" << std::endl;

	START();
	int_t num_nodes = (num_division_[0] + 1)*(num_division_[1] + 1)*(num_division_[2] + 1);
	const real_t dx = (end_coords_[0] - start_coords_[0]) / double(num_division_[0]);
	const real_t dy = (end_coords_[1] - start_coords_[1]) / double(num_division_[1]);
	const real_t dz = (end_coords_[2] - start_coords_[2]) / double(num_division_[2]);

	outfile << "$Nodes" << std::endl;
	outfile << num_nodes << std::endl;
	int_t index = 0;
	for (int_t iz = 0; iz < num_division_[2] + 1; iz++)
	for (int_t iy = 0; iy < num_division_[1] + 1; iy++)
	for (int_t ix = 0; ix < num_division_[0] + 1; ix++)
		outfile << ++index << " " << start_coords_[0] + dx*double(ix) << " " << start_coords_[1] + dy*double(iy) << " " << start_coords_[2] + dz*double(iz) << std::endl;
	outfile << "$EndNodes" << std::endl;
	MASTER_MESSAGE("Generage node data(Time: " + TO_STRING(STOP()) + "s)");

	START();
	int_t num_elements = num_division_[0] * num_division_[1] * num_division_[2] + 2 * (num_division_[0] * num_division_[1] + num_division_[1] * num_division_[2] + num_division_[2] * num_division_[0]);
	outfile << "$Elements" << std::endl;
	outfile << num_elements << std::endl;
	index = 0;
	//		    5*
	//          z
	//	 3----------2
	//	 |\     ^   |\ 
	//	 | \ 0* |   | \
	//	 |  \   |   |  \
	//	 |   7------+---6
	//2* |   |  +-- | --|->y 3*
	//	 0---+---\--1   |
	//	 \   |    \  \  |
	//	  \  |     \  \ |
	//	   \ |  4*  x  \|
	//	     4----------5
	//               1*    
	for (int_t iy = 0; iy < num_division_[1]; iy++) // 0 - 1 - 4 - 5
	for (int_t ix = 0; ix < num_division_[0]; ix++)
		outfile << ++index << " 3 0 " << bdry_tags_[4] << " 0 " <<
		ix + iy*(num_division_[0] + 1) + 1 << " " << ix + (iy + 1)*(num_division_[0] + 1) + 1 << " " <<
		ix + 2 + (iy + 1)*(num_division_[0] + 1) << " " << ix + 2 + iy*(num_division_[0] + 1) << std::endl;
		
	for (int_t iy = 0; iy < num_division_[1]; iy++) // 3 - 2 - 7 - 6
	for (int_t ix = 0; ix < num_division_[0]; ix++)
		outfile << ++index << " 3 0 " << bdry_tags_[5] << " 0 " << 
		ix + 1 + iy*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 1 + (iy + 1)*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << " " <<
		ix + 2 + (iy + 1)*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + iy*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;
	
	for (int_t iz = 0; iz < num_division_[2]; iz++) // 1 - 2 - 3 - 0
	for (int_t iy = 0; iy < num_division_[1]; iy++)
		outfile << ++index << " 3 0 " << bdry_tags_[0] << " 0 " << 
		iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << " " << (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << " " <<
		(iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << " " << iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1<< std::endl;
	
	for (int_t iz = 0; iz < num_division_[2]; iz++) // 4 - 5 - 6 - 7
	for (int_t iy = 0; iy < num_division_[1]; iy++)
		outfile << ++index << " 3 0 " << bdry_tags_[1] << " 0 " << 
		num_division_[0] + 1 + iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << num_division_[0] + 1 + (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " <<
		num_division_[0] + 1 + (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << num_division_[0] + iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << std::endl;
	
	for (int_t iz = 0; iz < num_division_[2]; iz++) // 0 - 3 - 4 - 7
	for (int_t ix = 0; ix < num_division_[0]; ix++)
		outfile << ++index << " 3 0 " << bdry_tags_[2] << " 0 " << 
		ix + 1 + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " <<
		ix + 2 + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << std::endl;
	
	for (int_t iz = 0; iz < num_division_[2]; iz++) // 1 - 2 - 5 - 6
	for (int_t ix = 0; ix < num_division_[0]; ix++)
		outfile << ++index << " 3 0 " << bdry_tags_[3] << " 0 " <<
		ix + 1 + num_division_[1] * (num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + num_division_[1] * (num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " <<
		ix + 2 + num_division_[1] * (num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + num_division_[1] * (num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << std::endl;

	for (int_t iz = 0; iz < num_division_[2]; iz++){
		for (int_t iy = 0; iy < num_division_[1]; iy++){
			for (int_t ix = 0; ix < num_division_[0]; ix++){
				std::vector<int_t> nodes_index;
				nodes_index.push_back(ix + 1 + iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 1 + (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 1 + (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 1 + iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				outfile << ++index << " 5 0 0 0";
				for (auto&& node : nodes_index)
					outfile << " " << node;
				outfile << std::endl;
			}
		}
	}
	outfile << "$EndElements" << std::endl;
	MASTER_MESSAGE("Generage element data(Time: " + TO_STRING(STOP()) + "s)");

	outfile.close();
}

// Gmsh3DPrisGridReader
Gmsh3DPrisGridReader::Gmsh3DPrisGridReader() :
	GmshGridReader(true)
{
	setName("Gmsh3DPris");
	GridReader::getInstance().addRegistry(getName(), this);
}

void Gmsh3DPrisGridReader::open(const std::string& grid_file_name, const int_t dimension)
{
	dimension_ = dimension;
	grid_file_name_ = grid_file_name;

	readOption();
	generateGmshFile();
	SYNCRO();
	infile_.open(grid_file_name.c_str());
	if (infile_.is_open()) {
		MASTER_MESSAGE("Open Gmsh-type grid file: " + grid_file_name_);
	}
	else {
		MASTER_ERROR("Fail to open Gmsh-type grid file: " + grid_file_name_);
	}
}

void Gmsh3DPrisGridReader::readOption()
{
	std::ifstream option_infile(INPUT() + "option.dat");
	if (option_infile.is_open()) {
		MASTER_MESSAGE("Open Gmsh-3D-pris-generation info file: " + INPUT() + "option.dat");
	}
	else {
		MASTER_ERROR("Fail to open Gmsh-3D-pris-generation info file: " + INPUT() + "option.dat");
	}

	std::string text;
	while (getline(option_infile, text)) {
		if (text.find("$GridNames", 0) != std::string::npos) {
			getline(option_infile, grid_file_name_);
			MASTER_MESSAGE("   ~> grid file name: " + grid_file_name_);
		}
		if (text.find("$Sizes", 0) != std::string::npos) {
			start_coords_.resize(dimension_);
			end_coords_.resize(dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> start_coords_[idim] >> end_coords_[idim];
			MASTER_MESSAGE("   ~> domain size: (" + TO_STRING(start_coords_[0]) + ", " + TO_STRING(start_coords_[1]) + ", " + TO_STRING(start_coords_[2]) + ") to (" + TO_STRING(end_coords_[0]) + ", " + TO_STRING(end_coords_[1]) + ", " + TO_STRING(end_coords_[2]) + ")");
		}
		if (text.find("$Nums", 0) != std::string::npos) {
			num_division_.resize(dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> num_division_[idim];
			MASTER_MESSAGE("   ~> number of elements: (" + TO_STRING(num_division_[0]) + ", " + TO_STRING(num_division_[1]) + ", " + TO_STRING(num_division_[2]) + ")");
		}
		if (text.find("$BdryTags", 0) != std::string::npos) {
			bdry_tags_.resize(2 * dimension_);
			for (int_t idim = 0; idim < dimension_; idim++)
				option_infile >> bdry_tags_[idim * 2] >> bdry_tags_[idim * 2 + 1];
			MASTER_MESSAGE("   ~> x-direction boundary tags: (" + TO_STRING(bdry_tags_[0]) + ", " + TO_STRING(bdry_tags_[1]) + ")");
			MASTER_MESSAGE("   ~> y-direction boundary tags: (" + TO_STRING(bdry_tags_[2]) + ", " + TO_STRING(bdry_tags_[3]) + ")");
			MASTER_MESSAGE("   ~> z-direction boundary tags: (" + TO_STRING(bdry_tags_[4]) + ", " + TO_STRING(bdry_tags_[5]) + ")");
		}
	}
	option_infile.close();
}

void Gmsh3DPrisGridReader::generateGmshFile()
{
	if (MYRANK() != MASTER_NODE) return;

	MASTER_MESSAGE("Grid file is being generated..");

	std::ofstream outfile(grid_file_name_);

	outfile << "$MeshFormat" << std::endl;
	outfile << "Gmsh3DPris by " << CODENAME() << std::endl;
	outfile << "$EndMeshFormat" << std::endl;

	START();
	int_t num_nodes = (num_division_[0] + 1)*(num_division_[1] + 1)*(num_division_[2] + 1);
	const real_t dx = (end_coords_[0] - start_coords_[0]) / double(num_division_[0]);
	const real_t dy = (end_coords_[1] - start_coords_[1]) / double(num_division_[1]);
	const real_t dz = (end_coords_[2] - start_coords_[2]) / double(num_division_[2]);

	outfile << "$Nodes" << std::endl;
	outfile << num_nodes << std::endl;
	int_t index = 0;
	for (int_t iz = 0; iz < num_division_[2] + 1; iz++)
		for (int_t iy = 0; iy < num_division_[1] + 1; iy++)
			for (int_t ix = 0; ix < num_division_[0] + 1; ix++)
				outfile << ++index << " " << start_coords_[0] + dx*double(ix) << " " << start_coords_[1] + dy*double(iy) << " " << start_coords_[2] + dz*double(iz) << std::endl;
	outfile << "$EndNodes" << std::endl;
	MASTER_MESSAGE("Generage node data(Time: " + TO_STRING(STOP()) + "s)");

	START();
	// prism is directed to x-axis
	int_t num_elements = 2 * num_division_[0] * num_division_[1] * num_division_[2] + 2 * (num_division_[0] * num_division_[1] + 2 * num_division_[1] * num_division_[2] + num_division_[2] * num_division_[0]);
	outfile << "$Elements" << std::endl;
	outfile << num_elements << std::endl;
	index = 0;
	//		    5*
	//          z
	//	 3----------2
	//	 |\     ^   |\ 
	//	 | \ 0* |   | \
	//	 |  \   |   |  \
	//	 |   7------+---6
	//2* |   |  +-- | --|->y 3*
	//	 0---+---\--1   |
	//	 \   |    \  \  |
	//	  \  |     \  \ |
	//	   \ |  4*  x  \|
	//	     4----------5
	//               1*    
	for (int_t iy = 0; iy < num_division_[1]; iy++) // 0 - 1 - 4 - 5
		for (int_t ix = 0; ix < num_division_[0]; ix++)
			outfile << ++index << " 3 0 " << bdry_tags_[4] << " 0 " <<
			ix + iy*(num_division_[0] + 1) + 1 << " " << ix + (iy + 1)*(num_division_[0] + 1) + 1 << " " <<
			ix + 2 + (iy + 1)*(num_division_[0] + 1) << " " << ix + 2 + iy*(num_division_[0] + 1) << std::endl;

	for (int_t iy = 0; iy < num_division_[1]; iy++) // 3 - 2 - 7 - 6
		for (int_t ix = 0; ix < num_division_[0]; ix++)
			outfile << ++index << " 3 0 " << bdry_tags_[5] << " 0 " <<
			ix + 1 + iy*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 1 + (iy + 1)*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << " " <<
			ix + 2 + (iy + 1)*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + iy*(num_division_[0] + 1) + num_division_[2] * (num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;


	for (int_t iz = 0; iz < num_division_[2]; iz++) { // 1 - 2 - 3 - 0
		for (int_t iy = 0; iy < num_division_[1]; iy++) {
			outfile << ++index << " 2 0 " << bdry_tags_[0] << " 0 " << iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << " " << (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << " " << (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << std::endl;
			outfile << ++index << " 2 0 " << bdry_tags_[0] << " 0 " << iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << " " << (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << " " << iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << std::endl;
		}
	}

	for (int_t iz = 0; iz < num_division_[2]; iz++) { // 4 - 5 - 6 - 7
		for (int_t iy = 0; iy < num_division_[1]; iy++) {
			outfile << ++index << " 2 0 " << bdry_tags_[1] << " 0 " << num_division_[0] + 1 + iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << num_division_[0] + 1 + (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << num_division_[0] + 1 + (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;
			outfile << ++index << " 2 0 " << bdry_tags_[1] << " 0 " << num_division_[0] + 1 + iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << num_division_[0] + 1 + (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << num_division_[0] + 1 + iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << std::endl;
		}
	}

	for (int_t iz = 0; iz < num_division_[2]; iz++) // 0 - 3 - 4 - 7
		for (int_t ix = 0; ix < num_division_[0]; ix++)
			outfile << ++index << " 3 0 " << bdry_tags_[2] << " 0 " <<
			ix + 1 + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " <<
			ix + 2 + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << std::endl;

	for (int_t iz = 0; iz < num_division_[2]; iz++) // 1 - 2 - 5 - 6
		for (int_t ix = 0; ix < num_division_[0]; ix++)
			outfile << ++index << " 3 0 " << bdry_tags_[3] << " 0 " <<
			ix + 1 + num_division_[1] * (num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + 2 + num_division_[1] * (num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1) << " " <<
			ix + 2 + num_division_[1] * (num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) << " " << ix + num_division_[1] * (num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1) + 1 << std::endl;

	for (int_t iz = 0; iz < num_division_[2]; iz++) {
		for (int_t iy = 0; iy < num_division_[1]; iy++) {
			for (int_t ix = 0; ix < num_division_[0]; ix++) {
				std::vector<int_t> nodes_index;
				nodes_index.push_back(ix + 1 + iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 1 + (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 1 + (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 1 + iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + iy*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + (iy + 1)*(num_division_[0] + 1) + iz*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + (iy + 1)*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				nodes_index.push_back(ix + 2 + iy*(num_division_[0] + 1) + (iz + 1)*(num_division_[0] + 1)*(num_division_[1] + 1));
				for (auto&& sub_nodes_index : makePrisFromHexa(nodes_index))
					outfile << ++index << " 6 2 0 0 " << sub_nodes_index[0] << " " << sub_nodes_index[1] << " " << sub_nodes_index[2] << " " << sub_nodes_index[3] << " " << sub_nodes_index[4] << " " << sub_nodes_index[5] << std::endl;
			}
		}
	}
	outfile << "$EndElements" << std::endl;
	MASTER_MESSAGE("Generage element data(Time: " + TO_STRING(STOP()) + "s)");

	outfile.close();
}

const std::vector<std::vector<int_t>> Gmsh3DPrisGridReader::makePrisFromHexa(const std::vector<int_t>& nodes_index)
{
	std::vector<std::vector<int_t>> total_nodes_index;
	total_nodes_index.push_back({ nodes_index[0], nodes_index[1], nodes_index[2], nodes_index[4], nodes_index[5], nodes_index[6] });
	total_nodes_index.push_back({ nodes_index[0], nodes_index[2], nodes_index[3], nodes_index[4], nodes_index[6], nodes_index[7] });
	return total_nodes_index;
}