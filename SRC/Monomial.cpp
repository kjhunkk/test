#include "../INC/Monomial.h"


int_t Monomial::numMonomialLine(const int_t order)
{
	return order + 1;
}
int_t Monomial::numMonomialTris(const int_t order)
{
	return (order + 1)*(order + 2) / 2;
}
int_t Monomial::numMonomialQuad(const int_t order)
{
	return (order + 1)*(order + 1);
}
int_t Monomial::numMonomialTets(const int_t order)
{
	return (order + 1)*(order + 2)*(order + 3) / 6;
}
int_t Monomial::numMonomialHexa(const int_t order)
{
	return (order + 1)*(order + 1)*(order + 1);
}
int_t Monomial::numMonomialPris(const int_t order)
{
	return (order + 1)*(order + 1)*(order + 2) / 2;
}
int_t Monomial::numMonomialPyra(const int_t order)
{
	return (order + 1)*(order + 2)*(2 * order + 3) / 6;
}
int_t Monomial::numMonomialUserDefine(const int_t order)
{
	//return (order + 1)*(order + 2)*(2 * order + 3) / 6;
	//return (order + 1)*(order + 1)*(order + 1);
	return (order + 1)*((order + 2)*(order + 3) + 3 * order) / 6;
}
const std::vector<real_t> Monomial::monomialLine(const int_t order, const real_t r)
{
	std::vector<real_t> results(numMonomialLine(order));
	results[0] = 1.0;
	for (int_t i = 1; i < order + 1; i++)
		results[i] = results[i - 1] * r;
	return results;
}
const std::vector<real_t> Monomial::monomialTris(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results(numMonomialTris(order));
	results[0] = 1.0;
	int_t index = 1;
	for (int_t i = 1; i <= order; i++) {
		results[index] = results[index - i] * r;
		index++;
		for (int_t j = 0; j < i; j++) {
			results[index] = results[index - i - 1] * s;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialQuad(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results(numMonomialQuad(order));
	results[0] = 1.0;
	int_t index = 1;
	for (int_t i = 1; i <= order; i++) {
		results[index] = results[index - 2 * i + 1] * r;
		index++;
		for (int_t j = 0; j < i; j++) {
			results[index] = results[index - 1] * s;
			index++;
		}
		for (int_t j = 0; j < i; j++) {
			results[index] = results[index - 2 * i - 1] * s;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialTets(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialTets(order));
	results[0] = 1.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		results[index] = results[bef_index] * r;
		index++;
		def = index - bef_index;
		bef_index = index - 1;
		for (int_t j = 1; j <= i; j++) {
			results[index] = results[index - def] * s;
			index++; def++;
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialHexa(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialHexa(order));
	results[0] = 1.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		def = index - bef_index;
		results[index] = results[bef_index] * r;
		bef_index = index;
		index++;
		for (int_t j = 1; j <= i; j++) { // bottom
			results[index] = results[index - 1] * s;
			index++;
		}
		def += 2;
		for (int_t j = 1; j <= i; j++) {
			results[index] = results[index - def] * s;
			index++;
		}
		def = 2 * i + 1;
		for (int_t j = 1; j < i; j++) { // wall
			for (int_t k = 0; k < def; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		// top
		def = i*(3 * i + 1);
		for (int_t j = 0; j < i; j++) {
			for (int_t k = 0; k < 2 * j + 1; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		def = (i + 1)*(i + 1);
		for (int_t j = 0; j < 2 * i + 1; j++) {
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialPris(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPris(order));
	results[0] = 1.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		results[index] = results[bef_index] * r;
		index++;
		def = index - bef_index;
		bef_index = index - 1;
		for (int_t j = 1; j <= i; j++) { // bottom
			results[index] = results[index - def] * s;
			index++;
		}
		def = i + 1;
		for (int_t j = 1; j < i; j++) { // wall
			for (int_t k = 0; k < def; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		// top
		def = i*(i + 1) * 3 / 2;
		for (int_t j = 1; j <= i; j++) {
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		def = (i + 1)*(i + 2) / 2;
		for (int_t j = 0; j < i + 1; j++) {
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialPyra(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPyra(order));
	results[0] = 1.0;

	if (order == 0) return results;

	real_t mix = 0.0;
	if (std::abs(1.0 - t) > 1E-10)
		mix = r*s / (1.0 - t);
	const std::vector<real_t> mix_line = monomialLine(order, mix);
	const std::vector<real_t> rst_tets = monomialTets(order, r, s, t);
	const std::vector<real_t> rs_tris = monomialTris(order - 1, r, s);

	std::vector<int_t> tets_index(order + 2);
	std::vector<int_t> tris_index(order + 2);
	for (int_t i = 0; i <= order + 1; i++) {
		tets_index[i] = i*(i + 1)*(i + 2) / 6;
		tris_index[i] = i*(i + 1) / 2;
	}

	int_t index = 1;
	for (int_t i = 1; i <= order; i++) {
		for (int_t j = tets_index[i]; j < tets_index[i + 1]; j++)
			results[index++] = rst_tets[j];
		for (int_t j = 0; j < i; j++)
			for (int_t k = tris_index[i - j - 1]; k < tris_index[i - j]; k++)
				results[index++] = mix_line[j + 1] * rs_tris[k];
	}
	return results;
}
const std::vector<real_t> Monomial::monomialPyra2(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPyra(order));
	results[0] = 1.0;

	if (order == 0) return results;

	const std::vector<real_t> rs_quad = monomialQuad(order, r, s);
	const std::vector<real_t> t_line = monomialLine(order, t);

	int_t index = 1;
	for (int_t i = 1; i <= order; i++) {
		for (int_t j = 0; j <= i; j++) {
			for (int_t k = j*j; k < (j + 1)*(j + 1); k++) {
				results[index++] = rs_quad[k] * t_line[i - j];
			}
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDrLine(const int_t order, const real_t r)
{
	std::vector<real_t> results(numMonomialLine(order));
	results[0] = 0.0;
	if (order == 0)
		return results;
	results[1] = 1.0;
	for (int_t i = 2; i < order + 1; i++)
		results[i] = real_t(i) / real_t(i - 1)*results[i - 1] * r;
	return results;
}
const std::vector<real_t> Monomial::monomialDrTris(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results(numMonomialTris(order));
	const std::vector<real_t> monomials = monomialDrLine(order, r);
	int_t index = 0;
	for (int_t i = 0; i <= order; i++) {
		results[index] = monomials[i];
		index++;
		for (int_t j = 0; j < i; j++) {
			results[index] = results[index - i - 1] * s;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDsTris(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results(numMonomialTris(order));
	const std::vector<real_t> monomials = monomialDrLine(order, s);
	int_t index = 0;
	for (int_t i = 0; i <= order; i++) {
		for (int_t j = 0; j < i; j++) {
			results[index] = results[index - i] * r;
			index++;
		}
		results[index] = monomials[i];
		index++;
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDrQuad(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results(numMonomialQuad(order));
	const std::vector<real_t> monomials = monomialDrLine(order, r);

	int_t index = 0;
	for (int_t i = 0; i <= order; i++) {
		results[index] = monomials[i];
		index++;
		for (int_t j = 0; j < i; j++) {
			results[index] = results[index - 1] * s;
			index++;
		}
		for (int_t j = 0; j < i; j++) {
			results[index] = results[index - 2 * i - 1] * s;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDsQuad(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results(numMonomialQuad(order));
	const std::vector<real_t> monomials = monomialDrLine(order, s);

	results[0] = 0.0;
	int_t index = 1;
	for (int_t i = 1; i <= order; i++) {
		for (int_t j = 0; j < i; j++) {
			results[index] = results[index - 2 * i + 1] * r;
			index++;
		}
		index = (i + 1)*(i + 1) - 1;
		results[index] = monomials[i];
		index--;
		for (int_t j = 0; j < i; j++) {
			results[index] = results[index + 1] * r;
			index--;
		}
		index = (i + 1)*(i + 1);
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDrTets(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialTets(order));
	const std::vector<real_t> monomials = monomialDrLine(order, r);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		results[index] = monomials[i];
		index++;
		def = index - bef_index;
		bef_index = index - 1;
		for (int_t j = 1; j <= i; j++) {
			results[index] = results[index - def] * s;
			index++; def++;
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDsTets(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialTets(order));
	const std::vector<real_t> monomials = monomialDrLine(order, s);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		def = index - bef_index;
		bef_index = index;
		for (int_t j = 0; j < i; j++) {
			for (int_t k = 0; k <= j; k++) {
				results[index] = results[index - def] * r;
				index++;
			}
		}
		results[index] = monomials[i];
		index++;
		def += (i + 1);
		for (int_t k = 1; k <= i; k++) {
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDtTets(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialTets(order));
	const std::vector<real_t> monomials = monomialDrLine(order, t);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		def = index - bef_index;
		bef_index = index;
		for (int_t j = 0; j < i; j++) {
			for (int_t k = 0; k <= j; k++) {
				results[index] = results[index - def] * r;
				index++;
			}
		}
		def += i;
		for (int_t k = 0; k < i; k++) {
			results[index] = results[index - def] * s;
			index++;
		}
		results[index] = monomials[i];
		index++;
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDrHexa(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialHexa(order));
	const std::vector<real_t> monomials = monomialDrLine(order, r);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		results[index] = monomials[i];
		def = index - bef_index;
		bef_index = index;
		index++;
		for (int_t j = 1; j <= i; j++) { // bottom
			results[index] = results[index - 1] * s;
			index++;
		}
		def += 2;
		for (int_t j = 1; j < i; j++) {
			results[index] = results[index - def] * s;
			index++;
		}
		results[index++] = 0.0;
		def = 2 * i + 1;
		for (int_t j = 1; j < i; j++) { // wall
			for (int_t k = 1; k < def; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
			results[index++] = 0.0;
		}
		// top
		def = i*(3 * i + 1);
		for (int_t j = 0; j < i; j++) {
			for (int_t k = 1; k < 2 * j + 1; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
			results[index++] = 0.0;
		}
		def = (i + 1)*(i + 1);
		for (int_t j = 1; j < 2 * i + 1; j++) {
			results[index] = results[index - def] * t;
			index++;
		}
		results[index++] = 0.0;
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDsHexa(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialHexa(order));
	const std::vector<real_t> monomials = monomialDrLine(order, s);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		def = index - bef_index;
		bef_index = index;
		results[index++] = 0.0;
		for (int_t j = 1; j < i; j++) {// bottom
			results[index] = results[index - def] * r;
			index++;
		}
		index += i;
		results[index] = monomials[i];
		for (int_t j = 1; j <= i; j++) {
			index--;
			results[index] = results[index + 1] * r;
		}
		index += (i + 1);
		def = 2 * i + 1;
		for (int_t j = 1; j < i; j++) { // wall
			results[index++] = 0.0;
			for (int_t k = 1; k < def; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		// top
		def = i*(3 * i + 1);
		for (int_t j = 0; j < i; j++) {
			results[index++] = 0.0;
			for (int_t k = 1; k < 2 * j + 1; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		def = (i + 1)*(i + 1);
		results[index++] = 0.0;
		for (int_t j = 1; j < 2 * i + 1; j++) {
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDtHexa(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialHexa(order));
	const std::vector<real_t> monomials = monomialDrLine(order, t);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		def = index - bef_index + 2;
		bef_index = index;
		for (int_t j = 0; j < 2 * i + 1; j++) { // bottom
			results[index++] = 0.0;
		}
		for (int_t j = 1; j < i; j++) { // wall
			if (j == i - 1) {
				def -= (i - 1)*(i - 1);
			}
			results[index] = results[index - def] * r;
			index++;
			for (int_t k = 1; k <= i; k++) {
				results[index] = results[index - 1] * s;
				index++;
			}
			def += 2;
			for (int_t k = 1; k <= i; k++) {
				results[index] = results[index - def] * s;
				index++;
			}
		}
		// top
		results[index++] = monomials[i];
		def = 1;
		for (int_t j = 1; j <= i; j++) {
			results[index] = results[index - def] * r;
			index++;
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - 1] * s;
				index++;
			}
			def += 2;
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - def] * s;
				index++;
			}
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDrPris(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPris(order));
	const std::vector<real_t> monomials = monomialDrLine(order, r);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		results[index] = monomials[i];
		index++;
		def = index - bef_index;
		bef_index = index - 1;
		for (int_t j = 1; j <= i; j++) { // bottom
			results[index] = results[index - def] * s;
			index++;
		}
		def = i + 1;
		for (int_t j = 1; j < i; j++) { // wall
			for (int_t k = 0; k < def; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		// top
		def = i*(i + 1) * 3 / 2;
		for (int_t j = 1; j <= i; j++) {
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		def = (i + 1)*(i + 2) / 2;
		for (int_t j = 0; j < i + 1; j++) {
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDsPris(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPris(order));
	const std::vector<real_t> monomials = monomialDrLine(order, s);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		def = index - bef_index;
		bef_index = index;
		results[index++] = 0.0;
		for (int_t j = 1; j < i; j++) { // bottom
			results[index] = results[index - def] * r;
			index++;
		}
		results[index++] = monomials[i];
		def = i + 1;
		for (int_t j = 1; j < i; j++) { // wall
			for (int_t k = 0; k < def; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		// top
		def = i*(i + 1) * 3 / 2;
		for (int_t j = 1; j <= i; j++) {
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - def] * t;
				index++;
			}
		}
		def = (i + 1)*(i + 2) / 2;
		for (int_t j = 0; j < i + 1; j++) {
			results[index] = results[index - def] * t;
			index++;
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDtPris(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPris(order));
	const std::vector<real_t> monomials = monomialDrLine(order, t);

	results[0] = 0.0;
	int_t index = 1;
	int_t bef_index = 0;
	int_t def = 0;
	for (int_t i = 1; i <= order; i++) {
		def = index - bef_index;
		bef_index = index;

		for (int_t j = 1; j <= i + 1; j++) { // bottom
			results[index++] = 0.0;
		}

		def++;
		for (int_t j = 1; j < i; j++) { // wall
			if (j == i - 1) {
				def = i*(i + 1) - 1;
			}
			results[index] = results[index - def] * r;
			index++;
			def++;
			for (int_t k = 1; k <= i; k++) {
				results[index] = results[index - def] * s;
				index++;
			}
		}

		// top
		results[index++] = monomials[i];
		def = 1;
		for (int_t j = 1; j <= i; j++) {
			results[index] = results[index - def] * r;
			index++;
			def++;
			for (int_t k = 1; k <= j; k++) {
				results[index] = results[index - def] * s;
				index++;
			}
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDrPyra(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPyra(order));
	results[0] = 0.0;

	if (order == 0) return results;

	real_t mix = 0.0;
	real_t mix_dr = 0.0;
	if (std::abs(1.0 - t) > 1E-10) {
		mix = r*s / (1.0 - t);
		mix_dr = s / (1.0 - t);
	}

	const std::vector<real_t> mix_line = monomialLine(order, mix);
	const std::vector<real_t> rst_tets = monomialTets(order, r, s, t);
	const std::vector<real_t> rs_tris = monomialTris(order - 1, r, s);
	const std::vector<real_t> mix_line_dr = monomialDrLine(order, mix);
	const std::vector<real_t> rst_tets_dr = monomialDrTets(order, r, s, t);
	const std::vector<real_t> rs_tris_dr = monomialDrTris(order - 1, r, s);

	std::vector<int_t> tets_index(order + 2);
	std::vector<int_t> tris_index(order + 2);
	for (int_t i = 0; i <= order + 1; i++) {
		tets_index[i] = i*(i + 1)*(i + 2) / 6;
		tris_index[i] = i*(i + 1) / 2;
	}

	int_t index = 1;
	for (int_t i = 1; i <= order; i++) {
		for (int_t j = tets_index[i]; j < tets_index[i + 1]; j++)
			results[index++] = rst_tets_dr[j];
		for (int_t j = 0; j < i; j++)
			for (int_t k = tris_index[i - j - 1]; k < tris_index[i - j]; k++)
				results[index++] = mix_line_dr[j + 1] * mix_dr * rs_tris[k] + mix_line[j + 1] * rs_tris_dr[k];
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDsPyra(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPyra(order));
	results[0] = 0.0;

	if (order == 0) return results;

	real_t mix = 0.0;
	real_t mix_ds = 0.0;
	if (std::abs(1.0 - t) > 1E-10) {
		mix = r*s / (1.0 - t);
		mix_ds = r / (1.0 - t);
	}

	const std::vector<real_t> mix_line = monomialLine(order, mix);
	const std::vector<real_t> rst_tets = monomialTets(order, r, s, t);
	const std::vector<real_t> rs_tris = monomialTris(order - 1, r, s);
	const std::vector<real_t> mix_line_ds = monomialDrLine(order, mix);
	const std::vector<real_t> rst_tets_ds = monomialDsTets(order, r, s, t);
	const std::vector<real_t> rs_tris_ds = monomialDsTris(order - 1, r, s);

	std::vector<int_t> tets_index(order + 2);
	std::vector<int_t> tris_index(order + 2);
	for (int_t i = 0; i <= order + 1; i++) {
		tets_index[i] = i*(i + 1)*(i + 2) / 6;
		tris_index[i] = i*(i + 1) / 2;
	}

	int_t index = 1;
	for (int_t i = 1; i <= order; i++) {
		for (int_t j = tets_index[i]; j < tets_index[i + 1]; j++)
			results[index++] = rst_tets_ds[j];
		for (int_t j = 0; j < i; j++)
			for (int_t k = tris_index[i - j - 1]; k < tris_index[i - j]; k++)
				results[index++] = mix_line_ds[j + 1] * mix_ds * rs_tris[k] + mix_line[j + 1] * rs_tris_ds[k];
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDtPyra(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPyra(order));
	results[0] = 0.0;

	if (order == 0) return results;

	real_t mix = 0.0;
	real_t mix_dt = 0.0;
	if (std::abs(1.0 - t) > 1E-10) {
		mix = r*s / (1.0 - t);
		mix_dt = mix / (1.0 - t);
	}

	const std::vector<real_t> mix_line = monomialLine(order, mix);
	const std::vector<real_t> rst_tets = monomialTets(order, r, s, t);
	const std::vector<real_t> rs_tris = monomialTris(order - 1, r, s);
	const std::vector<real_t> mix_line_dt = monomialDrLine(order, mix);
	const std::vector<real_t> rst_tets_dt = monomialDtTets(order, r, s, t);

	std::vector<int_t> tets_index(order + 2);
	std::vector<int_t> tris_index(order + 2);
	for (int_t i = 0; i <= order + 1; i++) {
		tets_index[i] = i*(i + 1)*(i + 2) / 6;
		tris_index[i] = i*(i + 1) / 2;
	}

	int_t index = 1;
	for (int_t i = 1; i <= order; i++) {
		for (int_t j = tets_index[i]; j < tets_index[i + 1]; j++)
			results[index++] = rst_tets_dt[j];
		for (int_t j = 0; j < i; j++)
			for (int_t k = tris_index[i - j - 1]; k < tris_index[i - j]; k++)
				results[index++] = mix_line_dt[j + 1] * mix_dt * rs_tris[k];
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDrPyra2(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPyra(order));
	results[0] = 0.0;

	if (order == 0) return results;

	const std::vector<real_t> rs_quad = monomialDrQuad(order, r, s);
	const std::vector<real_t> t_line = monomialLine(order, t);

	int_t index = 1;
	for (int_t i = 1; i <= order; i++) {
		for (int_t j = 0; j <= i; j++) {
			for (int_t k = j*j; k < (j + 1)*(j + 1); k++) {
				results[index++] = rs_quad[k] * t_line[i - j];
			}
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDsPyra2(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPyra(order));
	results[0] = 0.0;

	if (order == 0) return results;

	const std::vector<real_t> rs_quad = monomialDsQuad(order, r, s);
	const std::vector<real_t> t_line = monomialLine(order, t);

	int_t index = 1;
	for (int_t i = 1; i <= order; i++) {
		for (int_t j = 0; j <= i; j++) {
			for (int_t k = j*j; k < (j + 1)*(j + 1); k++) {
				results[index++] = rs_quad[k] * t_line[i - j];
			}
		}
	}
	return results;
}
const std::vector<real_t> Monomial::monomialDtPyra2(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialPyra(order));
	results[0] = 0.0;

	if (order == 0) return results;

	const std::vector<real_t> rs_quad = monomialQuad(order, r, s);
	const std::vector<real_t> t_line = monomialDrLine(order, t);

	int_t index = 1;
	for (int_t i = 1; i <= order; i++) {
		for (int_t j = 0; j <= i; j++) {
			for (int_t k = j*j; k < (j + 1)*(j + 1); k++) {
				results[index++] = rs_quad[k] * t_line[i - j];
			}
		}
	}
	return results;
}

const std::vector<real_t> Monomial::monomialTrisRpri(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results(numMonomialTris(order));
	const std::vector<real_t> r_line = monomialLine(order, r);
	const std::vector<real_t> s_line = monomialLine(order, s);
	int_t index = 0;
	for (int_t i = 0; i <= order; i++) {
		for (int_t j = 0; j <= order - i; j++) {
			results[index++] = s_line[i] * r_line[j];
		}
	}
	return results;
}

const std::vector<real_t> Monomial::monomialQuadRpri(const int_t order, const real_t r, const real_t s)
{
	std::vector<real_t> results(numMonomialQuad(order));
	const std::vector<real_t> r_line = monomialLine(order, r);
	const std::vector<real_t> s_line = monomialLine(order, s);
	int_t index = 0;
	for (int_t i = 0; i <= order; i++) {
		for (int_t j = 0; j <= order; j++) {
			results[index++] = s_line[i] * r_line[j];
		}
	}
	return results;
}

const std::vector<real_t> Monomial::monomialTetsRSpri(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialTets(order));
	const std::vector<real_t> rs_tris = monomialTris(order, r, s);
	const std::vector<real_t> t_line = monomialLine(order, t);
	int_t index = 0;
	for (int_t i = 0; i <= order; i++) {
		for (int_t j = 0; j < numMonomialTris(order - i); j++) {
			results[index++] = rs_tris[j] * t_line[i];
		}
	}
	return results;
}

const std::vector<real_t> Monomial::monomialHexaRSpri(const int_t order, const real_t r, const real_t s, const real_t t)
{
	std::vector<real_t> results(numMonomialHexa(order));
	const std::vector<real_t> rs_quad = monomialQuad(order, r, s);
	const std::vector<real_t> t_line = monomialLine(order, t);
	int_t index = 0;
	for (int_t i = 0; i <= order; i++) {
		for (int_t j = 0; j < numMonomialQuad(order); j++) {
			results[index++] = rs_quad[j] * t_line[i];
		}
	}
	return results;
}

const std::vector<real_t> Monomial::monomialUserDefine(const int_t order, const real_t r, const real_t s, const real_t t)
{
	/*std::vector<real_t> results(numMonomialUserDefine(order));
	const std::vector<real_t> rs_quad = monomialQuad(order, r, s);
	const std::vector<real_t> t_line = monomialLine(order, t);
	int_t index = 0;
	for (int_t i = 0; i <= order; i++) {
		for (int_t j = 0; j < numMonomialQuad(order - i); j++) {
			results[index++] = rs_quad[j] * t_line[i];
		}
	}
	return results;*/
	std::vector<real_t> results(numMonomialUserDefine(order));
	const std::vector<real_t> rs_quad = monomialQuad(order, r, s);
	const std::vector<real_t> rs_tris = monomialTris(order, r, s);
	const std::vector<real_t> t_line = monomialLine(order, t);
	int_t index = 0;
	for (int_t j = 0; j < numMonomialQuad(order); j++) {
		results[index++] = rs_quad[j] * t_line[0];
	}
	for (int_t i = 1; i <= order; i++) {
		for (int_t j = 0; j < numMonomialTris(order - i); j++) {
			results[index++] = rs_tris[j] * t_line[i];
		}
	}
	return results;
}