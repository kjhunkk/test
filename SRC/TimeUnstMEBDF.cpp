
#include "../INC/TimeUnstMEBDF.h"

TimeUnstMEBDF::TimeUnstMEBDF()
{
	setName("MEBDF");
	TimeScheme::getInstance().addRegistry(getName(), this);
	is_implicit_ = true;
}

TimeUnstMEBDF::~TimeUnstMEBDF()
{
	PetscErrorCode ierr;
	ierr = VecDestroy(&delta_);
	ierr = VecDestroy(&intermediate_solution_vector_);
	ierr = VecDestroy(&MEBDF_rhs_);
	ierr = VecDestroy(&base_rhs_);
	VECTOR_ARRAY_DESTROYER(stage_rhs_, ierr);
	VECTOR_ARRAY_DESTROYER(stage_solution_array_, ierr);
	VECTOR_ARRAY_DESTROYER(solution_history_array_, ierr);
	VECTOR_ARRAY_DESTROYER(intermediate_history_array_, ierr);
}

void TimeUnstMEBDF::initialize()
{
	const Config& config = Config::getInstance();

	// Parent member variables
	if (config.getIsFixedTimeStep().compare("yes") == 0)
		is_fixed_dt_ = true;
	else
	{
		is_fixed_dt_ = false;
		MASTER_ERROR("Adaptive time step is not supported");
	}

	// Time integration configuration
	time_order_ = config.getTimeOrder();
	diagonal_factor_ = 0.0;
	InitializeCoeff();
	PetscErrorCode ierr;
	ierr = VecCreate(MPI_COMM_WORLD, &delta_);
	ierr = VecCreate(MPI_COMM_WORLD, &intermediate_solution_vector_);
	ierr = VecCreate(MPI_COMM_WORLD, &MEBDF_rhs_);
	ierr = VecCreate(MPI_COMM_WORLD, &base_rhs_);
	VECTOR_ARRAY_CREATOR(stage_rhs_, ierr);
	VECTOR_ARRAY_CREATOR(stage_solution_array_, ierr);
	solution_history_array_.resize(time_order_);
	VECTOR_ARRAY_CREATOR(solution_history_array_, ierr);
	intermediate_history_array_.resize(time_order_);
	VECTOR_ARRAY_CREATOR(intermediate_history_array_, ierr);

	// Non-linear solver configuration
	max_newton_iteration_ = 1000;
	newton_tolerance_ = 1.0e-8;
	recompute_tol_ = 0.2;

	// Linear solver configuration
	linear_system_solver_ = LinearSystemSolver::getInstance().getType(config.getLinearSystemSolver());
	linear_system_solver_->Initialize();

	// etc
	is_initialized_ = false;
}

void TimeUnstMEBDF::integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
	if (is_initialized_ == false)
	{
		InitializeVectors(zone, dt);
		SelfStartingProcedure(zone, dt, current_time, iteration);
		is_initialized_ = true;
	}

	diagonal_factor_ = 1.0 / dt;
	int_t internal_iteration = 0;

	const int_t num_cells = zone->getNumCells();
	const std::vector<int_t> cell_petsc_index = zone->getCellPetscIndex();
	real_t newton_norm = 0.0;
	internal_iteration = SolveStage1(zone, num_cells, cell_petsc_index, newton_norm);
	internal_iteration = SolveStage2(zone, num_cells, cell_petsc_index, newton_norm);
	internal_iteration = SolveStage3(zone, num_cells, cell_petsc_index, newton_norm);

	real_t error = 0;
	VecNorm(MEBDF_rhs_, NORM_2, &error);
	status_string_ = "Iteration=" + TO_STRING(internal_iteration) + " L2=" + TO_STRING((double)error);
}

void TimeUnstMEBDF::InitializeVectors(std::shared_ptr<Zone> zone, const real_t dt)
{
	const Vec& vec_initializer = zone->getSystemRHS();
	VecDuplicate(vec_initializer, &delta_);
	VecDuplicate(vec_initializer, &intermediate_solution_vector_);
	intermediate_solution_ = zone->getSolution();
	VecDuplicate(vec_initializer, &MEBDF_rhs_);
	VecDuplicate(vec_initializer, &base_rhs_);
	for (int_t i = 0; i < stage_rhs_.size(); i++)
		VecDuplicate(vec_initializer, &stage_rhs_[i]);
	for (int_t i = 0; i < stage_solution_array_.size(); i++)
		VecDuplicate(vec_initializer, &stage_solution_array_[i]);
	for (int_t i = 0; i < solution_history_array_.size(); i++)
		VecDuplicate(vec_initializer, &solution_history_array_[i]);
	for (int_t i = 0; i < intermediate_history_array_.size(); i++)
		VecDuplicate(vec_initializer, &intermediate_history_array_[i]);
}

int_t TimeUnstMEBDF::SolveStage1(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm)
{
	// Newton-Rapson iteration
	VecCopy(stage_solution_array_[1], intermediate_solution_vector_);
	ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);
	system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_ / BDF_beta_, 1.0);
	int_t newton_iteration = 0;
	int_t krylov_iteration = 0;
	real_t newton_current_norm = 0.0;
	real_t newton_previous_norm = newton_norm;

	// Predictor stage 1 base rhs
	VecSet(base_rhs_, 0.0);
	for (int_t istep = 0; istep < time_order_ - 1; istep++)
		VecAXPY(base_rhs_, -BDF_gamma_[istep], solution_history_array_[istep]);

	while (newton_iteration++ < max_newton_iteration_)
	{
		// RHS formulation
		VecCopy(intermediate_solution_vector_, MEBDF_rhs_);
		VecAXPY(MEBDF_rhs_, 1.0, base_rhs_);
		VecScale(MEBDF_rhs_, -diagonal_factor_ / BDF_beta_);
		VecAXPY(MEBDF_rhs_, 1.0, zone->calculateInstantSystemRHS(intermediate_solution_));

		// Krylov solve
		krylov_iteration = linear_system_solver_->Solve(system_matrix_, MEBDF_rhs_, delta_);
		VecNorm(delta_, NORM_2, &newton_current_norm);

		// intermediate solution update
		VecAXPY(delta_, 1.0, intermediate_solution_vector_);
		VecCopy(delta_, intermediate_solution_vector_);
		ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);

		// Check convergence
		if (newton_current_norm < newton_tolerance_) break;
		if ((newton_current_norm / newton_previous_norm) > recompute_tol_)
			system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_ / BDF_beta_, 1.0);
		newton_previous_norm = newton_current_norm;
	}

	newton_norm = newton_current_norm;
	VecCopy(intermediate_solution_vector_, stage_solution_array_[0]);
	for(int_t i = 0; i < solution_history_array_.size(); i++)
		VecCopy(solution_history_array_[i], intermediate_history_array_[i]);
	UpdateHistoryArray(intermediate_history_array_, intermediate_solution_vector_);
	VecCopy(intermediate_solution_vector_, stage_rhs_[0]);
	VecAXPY(stage_rhs_[0], 1.0, base_rhs_);
	VecScale(stage_rhs_[0], -diagonal_factor_ / BDF_beta_);
	return newton_iteration;
}

int_t TimeUnstMEBDF::SolveStage2(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm)
{
	// Newton-Rapson iteration
	VecCopy(solution_history_array_[1], intermediate_solution_vector_);
	for (int_t i = 2; i < solution_history_array_.size(); i++)
		VecAXPY(intermediate_solution_vector_, 1.0, solution_history_array_[i]);
	ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);
	int_t newton_iteration = 0;
	int_t krylov_iteration = 0;
	real_t newton_current_norm = 0.0;
	real_t newton_previous_norm = newton_norm;

	// Predictor stage 2 base rhs
	VecSet(base_rhs_, 0.0);
	for (int_t istep = 0; istep < time_order_ - 1; istep++)
		VecAXPY(base_rhs_, -BDF_gamma_[istep], intermediate_history_array_[istep]);

	while (newton_iteration++ < max_newton_iteration_)
	{
		// RHS formulation
		VecCopy(intermediate_solution_vector_, MEBDF_rhs_);
		VecAXPY(MEBDF_rhs_, 1.0, base_rhs_);
		VecScale(MEBDF_rhs_, -diagonal_factor_ / BDF_beta_);
		VecAXPY(MEBDF_rhs_, 1.0, zone->calculateInstantSystemRHS(intermediate_solution_));

		// Krylov solve
		krylov_iteration = linear_system_solver_->Solve(system_matrix_, MEBDF_rhs_, delta_);
		VecNorm(delta_, NORM_2, &newton_current_norm);

		// intermediate solution update
		VecAXPY(delta_, 1.0, intermediate_solution_vector_);
		VecCopy(delta_, intermediate_solution_vector_);
		ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);

		// Check convergence
		if (newton_current_norm < newton_tolerance_) break;
		if ((newton_current_norm / newton_previous_norm) > recompute_tol_)
			system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_ / BDF_beta_, 1.0);
		newton_previous_norm = newton_current_norm;
	}

	newton_norm = newton_current_norm;
	VecCopy(intermediate_solution_vector_, stage_solution_array_[1]);
	VecCopy(intermediate_solution_vector_, stage_rhs_[1]);
	VecAXPY(stage_rhs_[1], 1.0, base_rhs_);
	VecScale(stage_rhs_[1], -diagonal_factor_ / BDF_beta_);
	return newton_iteration;
}

int_t TimeUnstMEBDF::SolveStage3(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm)
{
	// Newton-Rapson iteration
	VecCopy(stage_solution_array_[0], intermediate_solution_vector_);
	ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);
	int_t newton_iteration = 0;
	int_t krylov_iteration = 0;
	real_t newton_current_norm = 0.0;
	real_t newton_previous_norm = 1000.0;

	// Corrector stage base rhs
	VecSet(base_rhs_, 0.0);
	for (int_t istep = 0; istep < time_order_ - 1; istep++)
		VecAXPY(base_rhs_, -MEBDF_gamma_[istep] * diagonal_factor_ / BDF_beta_, solution_history_array_[istep]);
	VecAXPY(base_rhs_, (MEBDF_beta_[0] - BDF_beta_) / BDF_beta_, stage_rhs_[0]);
	VecAXPY(base_rhs_, MEBDF_beta_[1] / BDF_beta_, stage_rhs_[1]);

	while (newton_iteration++ < max_newton_iteration_)
	{
		// RHS formulation
		VecCopy(intermediate_solution_vector_, MEBDF_rhs_);
		VecScale(MEBDF_rhs_, -diagonal_factor_ / BDF_beta_);
		VecAXPY(MEBDF_rhs_, -1.0, base_rhs_);
		VecAXPY(MEBDF_rhs_, 1.0, zone->calculateInstantSystemRHS(intermediate_solution_));

		// Krylov solve
		krylov_iteration = linear_system_solver_->Solve(system_matrix_, MEBDF_rhs_, delta_);
		VecNorm(delta_, NORM_2, &newton_current_norm);

		// intermediate solution update
		VecAXPY(delta_, 1.0, intermediate_solution_vector_);
		VecCopy(delta_, intermediate_solution_vector_);
		ConvertPetscToSolution(zone, intermediate_solution_vector_, num_cells, cell_petsc_index, intermediate_solution_);

		// Check convergence
		if (newton_current_norm < newton_tolerance_) break;
		else if (newton_current_norm / newton_previous_norm > recompute_tol_)
			system_matrix_ = zone->calculateInstantSystemMatrix(intermediate_solution_, diagonal_factor_ / BDF_beta_, 1.0);
		newton_previous_norm = newton_current_norm;
	}

	newton_norm = newton_current_norm;
	zone->UpdateSolutionFromVec(intermediate_solution_vector_);
	zone->communication();

	UpdateHistoryArray(solution_history_array_, intermediate_solution_vector_);

	return newton_iteration;
}

void TimeUnstMEBDF::InitializeCoeff()
{
	if (time_order_ > 8)
		MASTER_ERROR("Time order of MEBDF cannot exceed 8 : current time order (" + TO_STRING(time_order_) + ")");
	else if (time_order_ > 4)
		MASTER_MESSAGE("Warnning!!! MEBDF is no more A-stable : current time order (" + TO_STRING(time_order_) + ")");

	BDF_gamma_.clear();
	MEBDF_gamma_.clear();
	BDF_gamma_.resize(time_order_ - 1);
	MEBDF_gamma_.resize(time_order_ - 1);
	switch (time_order_)
	{
	case 2:
		BDF_gamma_ = { 1.0 };
		BDF_beta_ = 1.0;
		MEBDF_gamma_ = { 1.0 };
		MEBDF_beta_ = { 3.0 / 2.0, -1.0 / 2.0 };
		break;
	case 3:
		BDF_gamma_ = { 1.0, 1.0 / 3.0 };
		BDF_beta_ = 2.0 / 3.0;
		MEBDF_gamma_ = { 1.0, 5.0 / 23.0 };
		MEBDF_beta_ = { 22.0 / 23.0, -4.0 / 23.0 };
		break;
	case 4:
		BDF_gamma_ = { 1.0, 5.0 / 11.0, 2.0 / 11.0 };
		BDF_beta_ = 6.0 / 11.0;
		MEBDF_gamma_ = { 1.0, 65.0 / 197.0, 17.0 / 197.0 };
		MEBDF_beta_ = { 150.0 / 197.0, -18.0 / 197.0 };
		break;
	case 5:
		BDF_gamma_ = { 1.0, 13.0 / 25.0, 7.0 / 25.0, 3.0 / 25.0 };
		BDF_beta_ = 12.0 / 25.0;
		MEBDF_gamma_ = { 1.0, 1001.0 / 2501.0, 395.0 / 2501.0, 111.0 / 2501.0 };
		MEBDF_beta_ = { 1644.0 / 2501.0, -144.0 / 2501.0 };
		break;
	case 6:
		BDF_gamma_ = { 1.0, 77.0 / 137.0, 47.0 / 137.0, 27.0 / 137.0, 12.0 / 137.0 };
		BDF_beta_ = 60.0 / 137.0;
		MEBDF_gamma_ = { 1.0, 6699.0 / 14919.0, 3189.0 / 14919.0, 1349.0 / 14919.0, 394.0 / 14919.0 };
		MEBDF_beta_ = { 8820.0 / 14919.0, -600.0 / 14919.0 };
		break;
	case 7:
		BDF_gamma_ = { 1.0, 87.0 / 147.0, 57.0 / 147.0, 37.0 / 147.0, 22.0 / 147.0, 10.0 / 147.0 };
		BDF_beta_ = 60.0 / 147.0;
		MEBDF_gamma_ = { 1.0, 19401.0 / 39981.0, 10311.0 / 39981.0, 5251.0 / 39981.0, 2306.0 / 39981.0, 690.0 / 39981.0 };
		MEBDF_beta_ = { 21780.0 / 39981.0, -1200.0 / 39981.0 };
		break;
	case 8:
		BDF_gamma_ = { 1.0, 669.0 / 1089.0, 459.0 / 1089.0, 319.0 / 1089.0, 214.0 / 1089.0, 130.0 / 1089.0, 60.0 / 1089.0 };
		BDF_beta_ = 420.0 / 1089.0;
		MEBDF_gamma_ = { 1.0, 321789.0 / 626709.0, 184029.0 / 626709.0, 104439.0 / 626709.0, 55159.0 / 626709.0, 24800.0 / 626709.0, 7545.0 / 626709.0 };
		MEBDF_beta_ = { 319620.0 / 626709.0, -14700.0 / 626709.0 };
		break;
	default:;
	}
}

void TimeUnstMEBDF::ConvertSolutionToPetsc(const std::vector<std::vector<real_t> >& solution, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, Vec& vector)
{
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	PetscScalar *vector_array;
	const PetscInt num_var = solution[0].size();
	VecGetArray(vector, &vector_array);
	for (int_t icell = 0; icell < num_cells; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			vector_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar] = solution[icell][ivar];
	VecRestoreArray(vector, &vector_array);
}

void TimeUnstMEBDF::ConvertPetscToSolution(std::shared_ptr<Zone> zone, const Vec& vector, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, std::vector<std::vector<real_t> > & solution)
{
	const PetscInt petsc_index_start = *min_element(cell_petsc_index.begin(), cell_petsc_index.begin() + num_cells);
	const PetscScalar *vector_array;
	const PetscInt num_var = solution[0].size();
	VecGetArrayRead(vector, &vector_array);
	for (int_t icell = 0; icell < num_cells; icell++)
		for (int_t ivar = 0; ivar < num_var; ivar++)
			solution[icell][ivar] = vector_array[(cell_petsc_index[icell] - petsc_index_start) * num_var + ivar];
	VecRestoreArrayRead(vector, &vector_array);
	zone->RequestCommunication(solution);
}

void TimeUnstMEBDF::UpdateHistoryArray(std::vector<Vec>& history_array, Vec& new_solution)
{
	VecCopy(new_solution, history_array[time_order_ - 1]);
	for (int_t i = 0; i < time_order_ - 1; i++)
	{
		VecAYPX(history_array[i], -1.0, history_array[time_order_ - 1]);
		VecSwap(history_array[i], history_array[time_order_ - 1]);
	}
}

void TimeUnstMEBDF::SelfStartingProcedure(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration)
{
	std::vector<Vec> solution_vector(time_order_);
	for (int_t i = 0; i < solution_vector.size(); i++)
		VecDuplicate(solution_history_array_[0], &solution_vector[i]);

#ifdef _Order_Test_
	for (int_t i = 0; i < solution_vector.size(); i++)
		ConvertSolutionToPetsc(zone->getExactSolution(-dt * (double)i), zone->getNumCells(), zone->getCellPetscIndex(), solution_vector[i]);
#else
	const int_t num_cells = zone->getNumCells();
	const std::vector<int_t> cell_petsc_index = zone->getCellPetscIndex();
	std::vector<std::vector<real_t> > temp_solution = zone->getSolution();
	std::vector<Vec> Y(time_order_), F(time_order_);
	for (int_t i = 0; i < time_order_; i++)
	{
		VecCreate(MPI_COMM_WORLD, &Y[i]); VecCreate(MPI_COMM_WORLD, &F[i]);
		VecDuplicate(solution_vector[0], &Y[i]); VecDuplicate(solution_vector[0], &F[i]);
	}
	ConvertSolutionToPetsc(zone->getSolution(), num_cells, cell_petsc_index, solution_vector[time_order_ - 1]);
	switch (time_order_) {
	case 2:
		// 1st step
		VecCopy(solution_vector[time_order_ - 1], Y[0]);
		VecCopy(zone->calculateInstantSystemRHS(zone->getSolution()), F[0]);
		VecCopy(Y[0], Y[1]);
		VecAXPY(Y[1], dt, F[0]);
		ConvertPetscToSolution(zone, Y[1], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[1]);
		
		// Final
		VecCopy(solution_vector[1], solution_vector[0]);
		VecAXPY(solution_vector[0], dt / 2.0, F[0]);
		VecAXPY(solution_vector[0], dt / 2.0, F[1]);
		current_time += dt;
		iteration += 1;
		break;
	case 3:
		// 1st step
		VecCopy(solution_vector[2], Y[0]);
		VecCopy(zone->calculateInstantSystemRHS(zone->getSolution()), F[0]);
		VecCopy(Y[0], Y[2]);
		VecAXPY(Y[2], 2 * dt, F[0]);
		ConvertPetscToSolution(zone, Y[2], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[2]);

		// 2nd step
		VecCopy(Y[0], Y[1]); VecCopy(Y[0], Y[2]);
		VecAXPY(Y[1], 0.75*dt, F[0]); VecAXPY(Y[1], 0.25*dt, F[2]);
		VecAXPY(Y[2], dt, F[0]); VecAXPY(Y[2], dt, F[2]);
		ConvertPetscToSolution(zone, Y[1], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[1]);
		ConvertPetscToSolution(zone, Y[2], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[2]);

		// Final
		VecCopy(solution_vector[2], solution_vector[1]);
		VecAXPY(solution_vector[1], 5.0*dt / 12.0, F[0]);
		VecAXPY(solution_vector[1], 2.0*dt / 3.0, F[1]);
		VecAXPY(solution_vector[1], -dt / 12.0, F[2]);
		VecCopy(solution_vector[2], solution_vector[0]);
		VecAXPY(solution_vector[0], dt / 3.0, F[0]);
		VecAXPY(solution_vector[0], 4.0*dt / 3.0, F[1]);
		VecAXPY(solution_vector[0], dt / 3.0, F[2]);
		current_time += 2.0*dt;
		iteration += 2;
		break;
	case 4:
		// 1st step
		VecCopy(solution_vector[3], Y[0]);
		VecCopy(zone->calculateInstantSystemRHS(zone->getSolution()), F[0]);
		VecCopy(Y[0], Y[3]);
		VecAXPY(Y[3], 3 * dt, F[0]);
		ConvertPetscToSolution(zone, Y[3], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[3]);

		// 2nd step
		VecCopy(Y[0], Y[1]); VecCopy(Y[0], Y[3]);
		VecAXPY(Y[1], 5.0 * dt / 6.0, F[0]); VecAXPY(Y[1], dt / 6.0, F[3]);
		VecAXPY(Y[3], 3.0 * dt / 2.0, F[0]); VecAXPY(Y[3], 3.0 * dt / 2.0, F[3]);
		ConvertPetscToSolution(zone, Y[1], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[1]);
		ConvertPetscToSolution(zone, Y[3], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[3]);

		// 3rd step
		VecCopy(Y[0], Y[1]); VecCopy(Y[0], Y[2]); VecCopy(Y[0], Y[3]);
		VecAXPY(Y[1], 4.0 * dt / 9.0, F[0]); VecAXPY(Y[1], 7.0 * dt / 12.0, F[1]); VecAXPY(Y[1], -dt / 36.0, F[3]);
		VecAXPY(Y[2], 2.0 * dt / 9.0, F[0]); VecAXPY(Y[2], 5.0 * dt / 3.0, F[1]); VecAXPY(Y[2], dt / 9.0, F[3]);
		VecAXPY(Y[3], 9.0 * dt / 4.0, F[1]); VecAXPY(Y[3], 3.0 * dt / 4.0, F[3]);
		ConvertPetscToSolution(zone, Y[1], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[1]);
		ConvertPetscToSolution(zone, Y[2], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[2]);
		ConvertPetscToSolution(zone, Y[3], num_cells, cell_petsc_index, temp_solution);
		VecCopy(zone->calculateInstantSystemRHS(temp_solution), F[3]);

		// Final
		VecCopy(solution_vector[3], solution_vector[2]);
		VecAXPY(solution_vector[2], 9.0*dt / 24.0, F[0]);
		VecAXPY(solution_vector[2], 19.0*dt / 24.0, F[1]);
		VecAXPY(solution_vector[2], -5.0*dt / 24.0, F[2]);
		VecAXPY(solution_vector[2], dt / 24.0, F[3]);
		VecCopy(solution_vector[3], solution_vector[1]);
		VecAXPY(solution_vector[1], dt / 3.0, F[0]);
		VecAXPY(solution_vector[1], 4.0*dt / 3.0, F[1]);
		VecAXPY(solution_vector[1], dt / 3.0, F[2]);
		VecCopy(solution_vector[3], solution_vector[0]);
		VecAXPY(solution_vector[0], 3.0*dt / 8.0, F[0]);
		VecAXPY(solution_vector[0], 9.0*dt / 8.0, F[1]);
		VecAXPY(solution_vector[0], 9.0*dt / 8.0, F[2]);
		VecAXPY(solution_vector[0], 3.0*dt / 8.0, F[3]);
		current_time += 3.0*dt;
		iteration += 3;
		break;
	}
	for (int_t i = 0; i < time_order_; i++) {
		VecDestroy(&Y[i]); VecDestroy(&F[i]);
	}
#endif
	VecCopy(solution_vector[0], solution_history_array_[0]);
	if (time_order_ >= 2)
	{
		VecCopy(solution_vector[0], solution_history_array_[1]); VecAXPY(solution_history_array_[1], -1.0, solution_vector[1]);
	}
	if (time_order_ >= 3)
	{
		VecCopy(solution_vector[0], solution_history_array_[2]); VecAXPY(solution_history_array_[2], -2.0, solution_vector[1]);
		VecAXPY(solution_history_array_[2], 1.0, solution_vector[2]);
	}
	if (time_order_ >= 4)
	{
		VecCopy(solution_vector[0], solution_history_array_[3]); VecAXPY(solution_history_array_[3], -3.0, solution_vector[1]);
		VecAXPY(solution_history_array_[3], 3.0, solution_vector[2]); VecAXPY(solution_history_array_[3], -1.0, solution_vector[3]);
	}
	if (time_order_ >= 5)
	{
		VecCopy(solution_vector[0], solution_history_array_[4]); VecAXPY(solution_history_array_[4], -4.0, solution_vector[1]);
		VecAXPY(solution_history_array_[4], 6.0, solution_vector[2]); VecAXPY(solution_history_array_[4], -4.0, solution_vector[3]);
		VecAXPY(solution_history_array_[4], 1.0, solution_vector[4]);
	}
	for (int_t istage = 0; istage < stage_solution_array_.size(); istage++)
		VecCopy(solution_vector[0], stage_solution_array_[istage]);
}