
#pragma once

#include <vector>
#include <memory>
#include <algorithm>
#include <cmath>

#include "Config.h"
#include "DataType.h"
#include "SingletonRegistry.h"
#include "Grid.h"
#include "Equation.h"
#include "Element.h"

class Limiter : public SingletonRegistry<Limiter>
{
protected:
	std::shared_ptr<Grid> grid_;

	Equation* equation_;

protected:
	Limiter() {};

	~Limiter() {};

public:
	virtual void limiting() = 0;

	virtual void initialize(std::shared_ptr<Grid> grid, Equation* equation, std::vector<std::vector<real_t>>* solution) = 0;

};

class NoneLimiter : public Limiter
{
private:
	static NoneLimiter none_limiter_;

protected:
	NoneLimiter();

	~NoneLimiter() {};

public:
	virtual void limiting();

	virtual void initialize(std::shared_ptr<Grid> grid, Equation* equation, std::vector<std::vector<real_t>>* solution);
};

class hMLP : public Limiter
{
private:
	static hMLP hmlp_;

	int_t num_total_nodes_;

	int_t num_total_cells_;

	int_t order_;

	int_t num_basis_;

	int_t num_cells_;

	int_t num_states_;

	int_t target_state_;

	std::vector<int_t> num_basis_list_;

	const std::vector<real_t>* cell_volumes_;

	std::vector<std::vector<real_t>> cell_averages_; // state, cell

	std::vector<std::vector<real_t>> vertex_max_; // state, node

	std::vector<std::vector<real_t>> vertex_min_; // state, node

	std::vector<std::vector<real_t>>* solution_;

	std::vector<std::vector<real_t>>* vertex_basis_;

protected:
	hMLP();

	~hMLP() {};

public:
	virtual void limiting();

	virtual void initialize(std::shared_ptr<Grid> grid, Equation* equation, std::vector<std::vector<real_t>>* solution);

private:
	bool indicator(const int_t icell, const int_t order);

	bool MLPCondition(const int_t icell, const int_t ivertex);

	bool smoothDetect(const int_t icell, const int_t ivertex);

	bool Deactivation(const int_t icell, const int_t ivertex);

	void MLPu1(const int_t icell);

	void projection(const int_t icell, const int_t iorder);

	void construct();
};

class hMLPBD : public Limiter
{
private:
	static hMLPBD hmlp_bd_;

	int_t num_total_nodes_;

	int_t num_total_cells_;

	int_t order_;

	int_t num_basis_;

	int_t num_cells_;

	int_t num_faces_;

	int_t num_peribdries_;

	int_t num_states_;

	int_t target_state_;

	std::vector<int_t> num_basis_list_;

	const std::vector<real_t>* cell_volumes_;

	std::vector<std::vector<real_t>> cell_averages_; // state, cell

	std::vector<int_t> cell_num_troubled_boundary_; // cell

	std::vector<std::vector<real_t>> vertex_average_; // state, node

	std::vector<std::vector<real_t>> vertex_max_; // state, node

	std::vector<std::vector<real_t>> vertex_min_; // state, node

	std::vector<std::vector<real_t>> face_difference_; // state, face

	std::vector<std::vector<real_t>> bdry_difference_; // state, periodic boundary

	std::vector<std::vector<real_t>>* solution_;

	std::vector<std::vector<real_t>>* vertex_basis_;

	std::vector<real_t> face_differene_;

	std::vector<real_t> bdry_differene_;

	const std::vector<Element*>* cell_element_;

protected:
	hMLPBD();

	~hMLPBD() {};

public:
	virtual void limiting();

	virtual void initialize(std::shared_ptr<Grid> grid, Equation* equation, std::vector<std::vector<real_t>>* solution);

private:
	bool indicator(const int_t icell, const int_t order);

	bool MLPCondition(const int_t icell, const int_t ivertex);

	bool smoothDetect(const int_t icell, const int_t ivertex);

	bool Deactivation(const int_t icell, const int_t ivertex);

	void MLPu1(const int_t icell);

	void projection(const int_t icell, const int_t iorder);

	void construct();

	bool boundaryDetect(const int_t icell, const int_t num_troubled_boundary);

	bool isContact(const int_t icell);
};

class hMLPBD_SIM : public Limiter
{
private:
	static hMLPBD_SIM hmlp_bd_sim_;

	int_t num_total_nodes_;

	int_t num_total_cells_;

	int_t order_;

	int_t num_basis_;

	int_t num_cells_;

	int_t num_faces_;

	int_t num_peribdries_;

	int_t num_states_;

	int_t target_state_;

	std::vector<int_t> num_basis_list_;

	const std::vector<real_t>* cell_volumes_;

	std::vector<int_t> cell_num_troubled_boundary_; // cell

	std::vector<std::vector<real_t>> vertex_average_; // state, node

	std::vector<std::vector<real_t>> vertex_max_; // state, node

	std::vector<std::vector<real_t>> vertex_min_; // state, node

	std::vector<std::vector<real_t>> face_difference_; // state, face

	std::vector<std::vector<real_t>> bdry_difference_; // state, periodic boundary

	std::vector<std::vector<real_t>>* solution_;

	std::vector<std::vector<real_t>>* vertex_basis_;

	const std::vector<std::vector<real_t>>* simplex_average_coefficients_;

	const std::vector<std::vector<real_t>>* simplex_p1_coefficients_;

	const std::vector<std::vector<real_t>>* simplex_volumes_;

	std::vector<real_t> face_differene_;

	std::vector<real_t> bdry_differene_;

	const std::vector<Element*>* cell_element_;

protected:
	hMLPBD_SIM();

	~hMLPBD_SIM() {};

public:
	virtual void limiting();

	virtual void initialize(std::shared_ptr<Grid> grid, Equation* equation, std::vector<std::vector<real_t>>* solution);

private:
	bool indicator(const int_t icell, const int_t order);

	bool MLPCondition(const int_t icell, const int_t ivertex);

	bool smoothDetect(const int_t icell, const int_t ivertex);

	bool Deactivation(const int_t icell, const int_t ivertex);

	void MLPu1(const int_t icell);

	void projection(const int_t icell, const int_t iorder);

	void construct();

	bool boundaryDetect(const int_t icell, const int_t num_troubled_boundary);

	bool isContact(const int_t icell);
};

class hMLP_SIM : public Limiter
{
private:
	static hMLP_SIM hmlp_sim_;

	int_t num_total_nodes_;

	int_t num_total_cells_;

	int_t order_;

	int_t num_basis_;

	int_t num_cells_;

	int_t num_states_;

	int_t target_state_;

	std::vector<int_t> num_basis_list_;

	const std::vector<real_t>* cell_volumes_;

	std::vector<std::vector<real_t>> vertex_max_; // state, node

	std::vector<std::vector<real_t>> vertex_min_; // state, node

	std::vector<std::vector<real_t>>* solution_;

	std::vector<std::vector<real_t>>* vertex_basis_;

	const std::vector<std::vector<real_t>>* simplex_average_coefficients_;

	const std::vector<std::vector<real_t>>* simplex_p1_coefficients_;

	const std::vector<std::vector<real_t>>* simplex_volumes_;

	const std::vector<Element*>* cell_element_;

protected:
	hMLP_SIM();

	~hMLP_SIM() {};

public:
	virtual void limiting();

	virtual void initialize(std::shared_ptr<Grid> grid, Equation* equation, std::vector<std::vector<real_t>>* solution);

private:
	bool indicator(const int_t icell, const int_t order);

	bool MLPCondition(const int_t icell, const int_t ivertex);

	bool smoothDetect(const int_t icell, const int_t ivertex);

	bool Deactivation(const int_t icell, const int_t ivertex);

	void MLPu1(const int_t icell);

	void projection(const int_t icell, const int_t iorder);

	void construct();
};