
#pragma once

#include <vector>
#include <string>
#include <memory>
#include <fstream>

#include "MPIHeader.h"
#include "DataType.h"
#include "Config.h"
#include "Grid.h"
#include "Zone.h"
#include "Equation.h"
#include "PostElement.h"
#include "Aerodynamics.h"

class Post
{
private:
	int_t dimension_;

	int_t post_order_;

	int_t num_cells_;

	int_t num_basis_;

	int_t num_states_;

	int_t num_subcell_nodes_;

	int_t num_subcell_cells_;

	Equation* equation_;

	Aerodynamics aerodynamics_;

	std::shared_ptr<Grid> grid_;

	std::string return_address_;

	std::vector<std::string> variable_names_;

	std::vector<std::vector<real_t>>* solution_;

	std::vector<std::vector<real_t>> pre_solution_;

	std::vector<PostElement*> post_elements_;

	std::vector<std::vector<real_t>> coords_;

	std::vector<std::vector<real_t>> basis_;

public:
	Post() {};

	~Post() {};

public:
	void initialize(std::shared_ptr<Grid> grid, std::shared_ptr<Zone> zone);

	void postSolution(const std::string& title, const real_t time, const bool average = false);

	void postDuringSolution(int_t& strandid, const real_t time);

	void postHistory(const int_t iteration);

	inline void updatePreSolution() { pre_solution_ = *solution_; };
};

