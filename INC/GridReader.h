
#pragma once

#include <string>
#include <fstream>
#include <vector>
#include <array>

#include "DataType.h"
#include "MPIHeader.h"
#include "SingletonRegistry.h"
#include "BoundaryCondition.h"

// Caution: end of line should be "Carrage Return" + "Line Feeding"
class GridReader : public SingletonRegistry<GridReader>
{
protected:
	GridReader() {};

	~GridReader() {};

public:
	virtual void open(const std::string& grid_file_name, const int_t dimension) = 0;

	virtual void readGridInfo(int_t& num_global_nodes, int_t& num_global_cells, int_t& num_global_bdries, int_t& num_global_peribdries) = 0;

	virtual void readNodeData(const int_t start_index, const int_t end_index, std::vector<real_t>& node_coords) = 0;

	virtual void readCellData(const int_t start_index, const int_t end_index, std::vector<int_t>& cell_to_node_ind, std::vector<int_t>& cell_to_node_ptr, std::vector<int_t>& cell_elem_type, std::vector<int_t>& cell_order, std::vector<int_t>& cell_to_subnode_ptr, std::vector<int_t>& cell_to_subnode_ind) = 0;

	virtual void readBdryData(const int_t start_index, const int_t end_index, std::vector<int_t>& bdry_to_node_ptr, std::vector<int_t>& bdry_to_node_ind, std::vector<int_t>& bdry_type) = 0;

	virtual void readPeriBdryData(const int_t start_index, const int_t end_index, std::vector<int_t>& peribdry_to_node_ptr, std::vector<int_t>& peribdry_to_node_ind, std::vector<int_t>& peribdry_direction) = 0;

	virtual void close() = 0;
};

class GmshGridReader : public GridReader
{
private:
	static GmshGridReader gmsh_grid_reader_;

protected:
	enum class GmshElemType{
		LINE_P1 = 1, LINE_P2 = 8, LINE_P3 = 26, LINE_P4 = 27, LINE_P5 = 28, LINE_P6 = 62,
		TRIS_P1 = 2, TRIS_P2 = 9, TRIS_P3 = 21, TRIS_P4 = 23, TRIS_P5 = 25,
		QUAD_P1 = 3, QUAD_P2 = 10, QUAD_P3 = 36, QUAD_P4 = 37, QUAD_P5 = 38, QUAD_P6 = 47,
		TETS_P1 = 4, TETS_P2 = 11, TETS_P3 = 29, TETS_P4 = 30, TETS_P5 = 31,
		HEXA_P1 = 5, HEXA_P2 = 12, HEXA_P3 = 92, HEXA_P4 = 93, HEXA_P5 = 94,
		PRIS_P1 = 6, PRIS_P2 = 13, PRIS_P3 = 90, PRIS_P4 = 91, PRIS_P5 = 106,
		PYRA_P1 = 7
	};

	std::ifstream infile_;

	int_t dimension_;

	int_t num_global_nodes_;

	int_t num_global_cells_;

	int_t num_global_bdries_;

	int_t num_global_peribdries_;

	std::array<int_t, 4> periodic_direction_;

	std::streampos pos_node_;

	std::streampos pos_cell_;

	std::streampos pos_bdry_;

protected:
	GmshGridReader();

	GmshGridReader(bool isSon) {};

	~GmshGridReader() {};

public:
	virtual void open(const std::string& grid_file_name, const int_t dimension);

	virtual void readGridInfo(int_t& num_global_nodes, int_t& num_global_cells, int_t& num_global_bdries, int_t& num_global_peribdries);

	virtual void readNodeData(const int_t start_index, const int_t end_index, std::vector<real_t>& node_coords);

	virtual void readCellData(const int_t start_index, const int_t end_index, std::vector<int_t>& cell_to_node_ind, std::vector<int_t>& cell_to_node_ptr, std::vector<int_t>& cell_elem_type, std::vector<int_t>& cell_order, std::vector<int_t>& cell_to_subnode_ptr, std::vector<int_t>& cell_to_subnode_ind);

	virtual void readBdryData(const int_t start_index, const int_t end_index, std::vector<int_t>& bdry_to_node_ptr, std::vector<int_t>& bdry_to_node_ind, std::vector<int_t>& bdry_type);

	virtual void readPeriBdryData(const int_t start_index, const int_t end_index, std::vector<int_t>& peribdry_to_node_ptr, std::vector<int_t>& peribdry_to_node_ind, std::vector<int_t>& peribdry_direction);

	virtual void close();

protected:
	bool isBoundary(const int_t elem_num_type, const int_t dimension);

	int_t getNumSubNodesPerElement(const int_t elem_num_type);

	int_t getNumNodesPerElement(const int_t elem_num_type);

	int_t GetGmshElemOrder(const int_t elem_num_type);

	int_t GetElementTypeFromGmshType(const int_t elem_num_type);
};

class Gmsh2DTrisGridReader : public GmshGridReader
{
private:
	static Gmsh2DTrisGridReader gmsh2dtris_grid_reader_;

	std::string grid_file_name_;

	std::vector<real_t> start_coords_;

	std::vector<real_t> end_coords_;

	std::vector<int_t> num_division_;

	std::vector<int_t> bdry_tags_;

protected:
	Gmsh2DTrisGridReader();

	~Gmsh2DTrisGridReader() {};

public:
	virtual void open(const std::string& grid_file_name, const int_t dimension);

private:
	void readOption();

	void generateGmshFile();
};

class Gmsh2DQuadGridReader : public GmshGridReader
{
private:
	static Gmsh2DQuadGridReader gmsh2dquad_grid_reader_;

	std::ifstream option_infile_;

	std::string grid_file_name_;

	std::vector<real_t> start_coords_;

	std::vector<real_t> end_coords_;

	std::vector<int_t> num_division_;

	std::vector<int_t> bdry_tags_;

protected:
	Gmsh2DQuadGridReader();

	~Gmsh2DQuadGridReader() {};

public:
	virtual void open(const std::string& grid_file_name, const int_t dimension);

private:
	void readOption();

	void generateGmshFile();
};

class Gmsh3DTetsGridReader : public GmshGridReader
{
private:
	static Gmsh3DTetsGridReader gmsh3dtets_grid_reader_;

	std::ifstream option_infile_;

	std::string grid_file_name_;

	std::vector<real_t> start_coords_;

	std::vector<real_t> end_coords_;

	std::vector<int_t> num_division_;

	std::vector<int_t> bdry_tags_;

protected:
	Gmsh3DTetsGridReader();

	~Gmsh3DTetsGridReader() {};

public:
	virtual void open(const std::string& grid_file_name, const int_t dimension);

private:
	void readOption();

	void generateGmshFile();

	const std::vector<std::vector<int_t>> makeTetsFromHexa(const std::vector<int_t>& nodes_index);
};

class Gmsh3DHexaGridReader : public GmshGridReader
{
private:
	static Gmsh3DHexaGridReader gmsh3dhexa_grid_reader_;

	std::ifstream option_infile_;

	std::string grid_file_name_;

	std::vector<real_t> start_coords_;

	std::vector<real_t> end_coords_;

	std::vector<int_t> num_division_;

	std::vector<int_t> bdry_tags_;

protected:
	Gmsh3DHexaGridReader();

	~Gmsh3DHexaGridReader() {};

public:
	virtual void open(const std::string& grid_file_name, const int_t dimension);

private:
	void readOption();

	void generateGmshFile();
};

class Gmsh3DPrisGridReader : public GmshGridReader
{
private:
	static Gmsh3DPrisGridReader gmsh3dpris_grid_reader_;

	std::ifstream option_infile_;

	std::string grid_file_name_;

	std::vector<real_t> start_coords_;

	std::vector<real_t> end_coords_;

	std::vector<int_t> num_division_;

	std::vector<int_t> bdry_tags_;

protected:
	Gmsh3DPrisGridReader();

	~Gmsh3DPrisGridReader() {};

public:
	virtual void open(const std::string& grid_file_name, const int_t dimension);

private:
	void readOption();

	void generateGmshFile();

	const std::vector<std::vector<int_t>> makePrisFromHexa(const std::vector<int_t>& nodes_index);
};