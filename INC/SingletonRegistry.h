#pragma once

#include <string>
#include <list>
#include <utility>
#include <algorithm>

#include "MPIHeader.h"

template<typename Singleton>
class SingletonRegistry
{
private:
	std::list<std::pair<std::string, Singleton*>> registry_;

	std::string name_;

protected:
	SingletonRegistry() {};

	~SingletonRegistry() {};

public:
	static SingletonRegistry<Singleton>& getInstance()
	{
		static SingletonRegistry<Singleton> instance;
		return instance;
	}

	void addRegistry(const std::string& name, Singleton* singleton);

	void getTypeList();

	void failToFind(const std::string& name);

	Singleton* getType(const std::string& name, const bool error = true);

	inline const std::string& getName() const { return name_; };

	void setName(const std::string& name) { name_ = name; };

	std::list<std::pair<std::string, Singleton*>>& getRegistry() { return registry_; };
};

template<typename Singleton>
void SingletonRegistry<Singleton>::addRegistry(const std::string& name, Singleton* singleton)
{
	std::string tmp = name;
	transform(tmp.begin(), tmp.end(), tmp.begin(), ::toupper);
	registry_.push_back(std::make_pair(tmp, singleton));
	MASTER_MESSAGE(name + " is registred");
}

template<typename Singleton>
void SingletonRegistry<Singleton>::getTypeList()
{
	MASTER_MESSAGE("Registered type list: ");
	int_t i = 0;
	for (auto&& apair : registry_)
		MASTER_MESSAGE(TO_STRING(++i) + ": " + apair.first);
}

template<typename Singleton>
void SingletonRegistry<Singleton>::failToFind(const std::string& name)
{
	MASTER_MESSAGE("Fail to find " + name);
	getTypeList();
	MASTER_ERROR("");
}

template<typename Singleton>
Singleton* SingletonRegistry<Singleton>::getType(const std::string& name, const bool error)
{
	std::string tmp = name;
	transform(tmp.begin(), tmp.end(), tmp.begin(), ::toupper);
	for (auto&& apair : registry_)
	if (!apair.first.compare(tmp)) return apair.second;
	if (error == true) failToFind(name);
	return 0;
}
