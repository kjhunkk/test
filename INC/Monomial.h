#pragma once

#include <vector>
#include <cmath>

#include "DataType.h"

namespace Monomial
{
	int_t numMonomialLine(const int_t order);

	int_t numMonomialTris(const int_t order);

	int_t numMonomialQuad(const int_t order);

	int_t numMonomialTets(const int_t order);

	int_t numMonomialHexa(const int_t order);

	int_t numMonomialPris(const int_t order);

	int_t numMonomialPyra(const int_t order);

	int_t numMonomialUserDefine(const int_t order);

	const std::vector<real_t> monomialLine(const int_t order, const real_t r);

	const std::vector<real_t> monomialTris(const int_t order, const real_t r, const real_t s);

	const std::vector<real_t> monomialTrisRpri(const int_t order, const real_t r, const real_t s);

	const std::vector<real_t> monomialQuad(const int_t order, const real_t r, const real_t s);

	const std::vector<real_t> monomialQuadRpri(const int_t order, const real_t r, const real_t s);

	const std::vector<real_t> monomialTets(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialTetsRSpri(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialHexa(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialHexaRSpri(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialPris(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialPyra(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialPyra2(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDrLine(const int_t order, const real_t r);

	const std::vector<real_t> monomialDrTris(const int_t order, const real_t r, const real_t s);

	const std::vector<real_t> monomialDsTris(const int_t order, const real_t r, const real_t s);

	const std::vector<real_t> monomialDrQuad(const int_t order, const real_t r, const real_t s);

	const std::vector<real_t> monomialDsQuad(const int_t order, const real_t r, const real_t s);

	const std::vector<real_t> monomialDrTets(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDsTets(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDtTets(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDrHexa(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDsHexa(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDtHexa(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDrPris(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDsPris(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDtPris(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDrPyra(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDsPyra(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDtPyra(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDrPyra2(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDsPyra2(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialDtPyra2(const int_t order, const real_t r, const real_t s, const real_t t);

	const std::vector<real_t> monomialUserDefine(const int_t order, const real_t r, const real_t s, const real_t t);
};