
#pragma once

#include <memory>
#include <petsc.h>

#include "MPIHeader.h"
#include "DataType.h"
#include "Grid.h"
#include "Equation.h"
#include "Config.h"
#include "Boundary.h"
#include "SABoundary.h"
#include "Limiter.h"

class Zone
{
private:
	int_t ndomain_;

	int_t myrank_;

	int_t order_;

	int_t dimension_;

	int_t num_basis_;

	int_t num_total_cells_;

	int_t num_cells_;

	int_t num_faces_;

	int_t num_bdries_;

	int_t num_peribdries_;

	int_t num_states_;

	real_t CFL_;

	bool visc_flux_;

	bool source_;

	bool pressure_fix_;

	std::shared_ptr<Grid> grid_;

	Equation* equation_;

	Limiter* limiter_;

	Mat implicit_operator_; // for implicit

	Vec implicit_rhs_; // for implicit

	std::vector<std::vector<real_t>> solution_; // cell, state, basis

	std::vector<std::vector<real_t>> auxiliary_; // cell, state, dim, basis

	std::vector<std::vector<real_t>> auxiliary_jacobian_; // cell, state, state, point, basis, dim

	std::vector<std::vector<real_t>> rhs_; // cell, state, basis

	std::ofstream pressure_fix_hist_file_;

	std::vector<int_t> num_send_data_;
	std::vector<int_t> num_recv_data_;
	std::vector<std::vector<real_t>> send_data_;
	std::vector<std::vector<real_t>> recv_data_;

	MPI_Offset save_disp_;
	MPI_Offset save_app_disp_; // for implicit(viscous order test)

public:
	Zone()
	{
		VecCreate(MPI_COMM_WORLD, &implicit_rhs_); // for implicit
		MatCreate(MPI_COMM_WORLD, &implicit_operator_); // for implicit
	}

	~Zone() {
		BoundaryFactory<SABoundary>::getInstance().clearRegistry();
		VecDestroy(&implicit_rhs_); // for implicit
		MatDestroy(&implicit_operator_); // for implicit
#ifdef _Pressure_Fix_Hist_
		pressure_fix_hist_file_.close();
#endif
	};

public:
	inline void setGrid(std::shared_ptr<Grid> grid) { grid_ = grid; };

	inline void setPressureFix(const bool pressure_fix) { pressure_fix_ = pressure_fix; };

	inline int_t getNumCells() const { return num_cells_; }

	inline std::vector<std::vector<real_t>>& getSolution() { return solution_; };

	inline std::vector<std::vector<real_t>>* getSolutionPtr() { return &solution_; };

	inline Equation* getEquation() { return equation_; };
	
	inline const Vec& getSystemRHS() const { return implicit_rhs_; }; // for implicit

	inline const std::vector<int_t>& getCellPetscIndex() const { return grid_->getCellPetscIndex(); } // for implicit

	inline void setCFL(const real_t CFL) { CFL_ = CFL; } // for implicit

	void initialize();

	void initializeSolution();

	std::vector<std::vector<real_t>> getExactSolution(const real_t time); // for implicit

	void initializeImpOperator(); // for implicit

	void initializeRHS();

	void initializeRHSImplicit(); // for implicit

	void initializeAuxiliary();

	void initializeAuxiliaryJacobian(); // for implicit

	void resetRHS();

	void resetRHSImplicit(); // for implicit

	void resetAuxiliary();

	void resetAuxiliaryImplicit(); // for implicit

	void resetSystemMatrix(); // for implicit

	const std::vector<std::vector<real_t>>& calculateRHS();

	const Vec& calculateSystemRHS(); // for implicit

	const Vec& calculateInstantSystemRHS(const std::vector<std::vector<real_t> >& instant_solution); // for implicit

	const Mat& calculateSystemMatrix(const real_t diagonal_factor, const real_t jacobian_factor); // for implicit

	const Mat& calculateInstantSystemMatrix(const std::vector<std::vector<real_t> >& instant_solution, const real_t diagonal_factor, const real_t jacobian_factor); // for implicit

	const Mat& getSystemMatrix() const { return implicit_operator_; } // for implicit

	void UpdateSolutionFromVec(const Vec& new_solution); // for implicit
	
	void UpdateSolutionFromDelta(const Vec& delta); // for implicit

	void communication();

	void RequestCommunication(std::vector<std::vector<real_t> >& solution); // for implicit

	void limiting();

	void pressurefix(const real_t current_time, const int_t iteration);

	real_t getTimeStep();

	real_t getTimeStepFromCFL(const real_t cfl);
	
	void save(const real_t solution_time, const real_t physical_time, const int_t iteration, const int_t strandid);

	void saveApprox(const real_t solution_time, const real_t physical_time, const int_t iteration, const int_t strandid); // for implicit

	void read(real_t& solution_time, int_t& iteration, int_t& strandid);

	void orderTest();
};
