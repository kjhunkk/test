
#pragma once

#include <vector>
#include <string>
#include <memory>
#include <fstream>

#include "DataType.h"
#include "MPIHeader.h"
#include "SingletonRegistry.h"
#include "BoundaryCondition.h"

class Equation : public SingletonRegistry<Equation>
{
protected:
	int_t num_states_;

	std::vector<std::string> variable_names_;

protected:
	Equation() {};

	~Equation() {};

protected:
	virtual void readData(const std::string& data_file_name) { return; };

public:
	inline int_t getNumStates(void) const { return num_states_; };

	inline const std::vector<std::string>& getVariableNames() const { return variable_names_; };

	virtual const std::vector<real_t> calPostVariables(const std::vector<real_t>& solutions) const = 0;

	virtual void setBoundaries(const std::vector<int_t>& tag_numbers) = 0;

	virtual inline void setFlux(const std::string& flux_name) = 0;

	virtual inline void setInitializer(const std::string& initializer_name) = 0;

	virtual inline const std::vector<real_t> getInitializerVal(const std::vector<real_t>& coords) = 0;

	virtual inline const std::vector<real_t> getExactVal(const std::vector<real_t>& coords, const real_t time) = 0; // for implicit

	virtual inline const std::vector<real_t> calNumConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) = 0;

	virtual inline const std::vector<real_t> calNumConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) = 0; // for implicit

	virtual inline const std::vector<real_t> calNumConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) = 0; // for implicit

	virtual inline const std::vector<real_t> calComConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) = 0;

	virtual inline const std::vector<real_t> calComConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) = 0; // for implicit

	virtual inline const std::vector<real_t> calNumBdryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal, const int_t ibdry) = 0;

	virtual inline const std::vector<real_t> calNumBdryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal, const int_t ibdry) = 0; // for implicit

	virtual inline const std::vector<real_t> calComConvFlux(const std::vector<real_t>& u) = 0;

	virtual inline const std::vector<real_t> calComConvFluxJacobian(const std::vector<real_t>& u) = 0; // for implicit

	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u) = 0;

	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u, const std::vector<real_t>& div_u) = 0;

	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& assist) = 0;

	virtual inline bool isViscFlux() const = 0;

	virtual inline bool isSource() const = 0;

	virtual inline const std::vector<real_t> calComViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) = 0;

	virtual inline const std::vector<real_t> calComViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) = 0; // for impliict

	virtual inline const std::vector<real_t> calComViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) = 0; // for implicit

	virtual inline const std::vector<real_t> calNumViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) = 0;

	virtual inline const std::vector<real_t> calNumViscFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) = 0; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxOwnerGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) = 0; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) = 0; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxNeighborGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) = 0; // for implicit

	virtual inline const std::vector<real_t> calNumBdryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) = 0;

	virtual inline const std::vector<real_t> calNumBdryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) = 0; // for implicit

	virtual inline const std::vector<real_t> calNumBdryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) = 0; // for implicit

	virtual inline const std::vector<real_t> calNumBdrySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) = 0;

	virtual inline const std::vector<real_t> calNumBdrySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) = 0; // for implicit

	virtual inline const std::vector<real_t> calComViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) = 0;

	virtual inline const std::vector<real_t> calComViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) = 0; // for implicit

	virtual inline const std::vector<real_t> calComViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) = 0; // for implicit

	virtual inline const std::vector<real_t> calConvWaveVelocity(const std::vector<real_t>& u) = 0;

	virtual inline const std::vector<real_t> calViscWaveVelocity(const std::vector<real_t>& u) = 0;

	virtual bool applyPressureFix(const std::vector<real_t>& u) = 0;

	virtual bool isContact(const std::vector<real_t>& u, const std::vector<real_t>& neighbor_u) const = 0;

	virtual inline const std::vector<real_t> calPressureCoefficients(const std::vector<real_t>& u) = 0;
};