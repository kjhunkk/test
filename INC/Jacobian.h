#pragma once

#include <vector>
#include <memory>

#include "DataType.h"
#include "Element.h"
#include "../LIB/armadillo"

#define ARMA_USE_LAPACK
#define ARMA_USE_BLAS

class Jacobian;

class JacobianFactory
{
public:
	static std::shared_ptr<Jacobian> getJacobian(ElemType element_type, const int_t element_order);

	static std::shared_ptr<Jacobian> getFaceJacobian(ElemType face_element_type, const int_t element_order);
};

class Jacobian
{
protected:
	class JacobianMath{
	public:
		static const std::vector<real_t> monomialLine(const int_t order, const real_t r);

		static const std::vector<real_t> monomialTris(const int_t order, const real_t r, const real_t s);

		static const std::vector<real_t> monomialQuad(const int_t order, const real_t r, const real_t s);

		static const std::vector<real_t> monomialTets(const int_t order, const real_t r, const real_t s, const real_t t);

		static const std::vector<real_t> monomialHexa(const int_t order, const real_t r, const real_t s, const real_t t);

		static const std::vector<real_t> monomialPris(const int_t order, const real_t r, const real_t s, const real_t t);

		static const std::vector<real_t> monomialDrLine(const int_t order, const real_t r);

		static const std::vector<real_t> monomialDrTris(const int_t order, const real_t r, const real_t s);

		static const std::vector<real_t> monomialDsTris(const int_t order, const real_t r, const real_t s);

		static const std::vector<real_t> monomialDrQuad(const int_t order, const real_t r, const real_t s);

		static const std::vector<real_t> monomialDsQuad(const int_t order, const real_t r, const real_t s);

		static const std::vector<real_t> monomialDrTets(const int_t order, const real_t r, const real_t s, const real_t t);

		static const std::vector<real_t> monomialDsTets(const int_t order, const real_t r, const real_t s, const real_t t);

		static const std::vector<real_t> monomialDtTets(const int_t order, const real_t r, const real_t s, const real_t t);

		static const std::vector<real_t> monomialDrHexa(const int_t order, const real_t r, const real_t s, const real_t t);

		static const std::vector<real_t> monomialDsHexa(const int_t order, const real_t r, const real_t s, const real_t t);

		static const std::vector<real_t> monomialDtHexa(const int_t order, const real_t r, const real_t s, const real_t t);

		static const std::vector<real_t> monomialDrPris(const int_t order, const real_t r, const real_t s, const real_t t);

		static const std::vector<real_t> monomialDsPris(const int_t order, const real_t r, const real_t s, const real_t t);

		static const std::vector<real_t> monomialDtPris(const int_t order, const real_t r, const real_t s, const real_t t);
	};

protected:
	int_t element_order_;

	std::vector<std::vector<real_t>> comtophy_coefficients_;

	std::vector<real_t> coords_;

public:
	Jacobian() {};

	Jacobian(const int_t element_order) :element_order_(element_order) {};

	Jacobian(const int_t element_order, const std::vector<real_t>& coords) :element_order_(element_order), coords_(coords) {};

	~Jacobian() {};

	inline void setTopology() { comtophy_coefficients_ = calComToPhyCoefficients(element_order_, coords_); };

	inline void setElementOrder(const int_t element_order) { element_order_ = element_order; };

	inline void setCoords(const std::vector<real_t>& coords) { coords_ = coords; };

	inline int_t getOrder() const { return element_order_; };

	inline const std::vector<real_t>& getCoords() const { return coords_; };

	virtual const std::vector<real_t> calComToPhyCoord(const std::vector<real_t>& coord) const = 0;

	virtual const std::vector<real_t> calJacobian(const std::vector<real_t>& coord) const = 0;

	virtual const std::vector<real_t> calJacobianAdjMat(const std::vector<real_t>& coord) const = 0; // |J|inv(J)

protected:
	virtual const std::vector<std::vector<real_t>> calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords) = 0;

	real_t Poly(const std::vector<real_t>& coef, const std::vector<real_t>& var) const
	{
		real_t result = 0.0;
		for (int_t i = 0; i < coef.size(); i++){
			result += coef[i] * var[i];
		}
		return result;
	}
};

class TrisJacobian : public Jacobian
{
private:
	const ElemType element_type_ = ElemType::TRIS;

public:
	TrisJacobian() {};

	TrisJacobian(const int_t element_order) : Jacobian(element_order) {};

	TrisJacobian(const int_t element_order, const std::vector<real_t>& coords) : Jacobian(element_order, coords) {};

	~TrisJacobian() {};

	virtual const std::vector<real_t> calComToPhyCoord(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobian(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobianAdjMat(const std::vector<real_t>& coord) const;

private:
	virtual const std::vector<std::vector<real_t>> calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords);
};

class QuadJacobian : public Jacobian
{
private:
	const ElemType element_type_ = ElemType::QUAD;

public:
	QuadJacobian() {};

	QuadJacobian(const int_t element_order) : Jacobian(element_order) {};

	QuadJacobian(const int_t element_order, const std::vector<real_t>& coords) : Jacobian(element_order, coords) {};

	~QuadJacobian() {};

	virtual const std::vector<real_t> calComToPhyCoord(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobian(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobianAdjMat(const std::vector<real_t>& coord) const;

private:
	virtual const std::vector<std::vector<real_t>> calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords);
};

class TetsJacobian : public Jacobian
{
private:
	const ElemType element_type_ = ElemType::TETS;

public:
	TetsJacobian() {};

	TetsJacobian(const int_t element_order) : Jacobian(element_order) {};

	TetsJacobian(const int_t element_order, const std::vector<real_t>& coords) : Jacobian(element_order, coords) {};

	~TetsJacobian() {};

	virtual const std::vector<real_t> calComToPhyCoord(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobian(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobianAdjMat(const std::vector<real_t>& coord) const;

private:
	virtual const std::vector<std::vector<real_t>> calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords);
};

class HexaJacobian : public Jacobian
{
private:
	const ElemType element_type_ = ElemType::HEXA;

public:
	HexaJacobian() {};

	HexaJacobian(const int_t element_order) : Jacobian(element_order) {};

	HexaJacobian(const int_t element_order, const std::vector<real_t>& coords) : Jacobian(element_order, coords) {};

	~HexaJacobian() {};

	virtual const std::vector<real_t> calComToPhyCoord(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobian(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobianAdjMat(const std::vector<real_t>& coord) const;

private:
	virtual const std::vector<std::vector<real_t>> calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords);
};

class PrisJacobian : public Jacobian
{
private:
	const ElemType element_type_ = ElemType::PRIS;

public:
	PrisJacobian() {};

	PrisJacobian(const int_t element_order) : Jacobian(element_order) {};

	PrisJacobian(const int_t element_order, const std::vector<real_t>& coords) : Jacobian(element_order, coords) {};

	~PrisJacobian() {};

	virtual const std::vector<real_t> calComToPhyCoord(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobian(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobianAdjMat(const std::vector<real_t>& coord) const;

private:
	virtual const std::vector<std::vector<real_t>> calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords);
};

class LineFaceJacobian : public Jacobian
{
private:
	const ElemType element_type_ = ElemType::LINE;

public:
	LineFaceJacobian() {};

	LineFaceJacobian(const int_t element_order) : Jacobian(element_order) {};

	LineFaceJacobian(const int_t element_order, const std::vector<real_t>& coords) : Jacobian(element_order, coords) {};

	~LineFaceJacobian() {};

	virtual const std::vector<real_t> calComToPhyCoord(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobian(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobianAdjMat(const std::vector<real_t>& coord) const;

private:
	virtual const std::vector<std::vector<real_t>> calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords);
};

class TrisFaceJacobian : public Jacobian
{
private:
	const ElemType element_type_ = ElemType::TRIS;

public:
	TrisFaceJacobian() {};

	TrisFaceJacobian(const int_t element_order) : Jacobian(element_order) {};

	TrisFaceJacobian(const int_t element_order, const std::vector<real_t>& coords) : Jacobian(element_order, coords) {};

	~TrisFaceJacobian() {};

	virtual const std::vector<real_t> calComToPhyCoord(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobian(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobianAdjMat(const std::vector<real_t>& coord) const;

private:
	virtual const std::vector<std::vector<real_t>> calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords);
};

class QuadFaceJacobian : public Jacobian
{
private:
	const ElemType element_type_ = ElemType::QUAD;

public:
	QuadFaceJacobian() {};

	QuadFaceJacobian(const int_t element_order) : Jacobian(element_order) {};

	QuadFaceJacobian(const int_t element_order, const std::vector<real_t>& coords) : Jacobian(element_order, coords) {};

	~QuadFaceJacobian() {};

	virtual const std::vector<real_t> calComToPhyCoord(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobian(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> calJacobianAdjMat(const std::vector<real_t>& coord) const;

private:
	virtual const std::vector<std::vector<real_t>> calComToPhyCoefficients(const int_t element_order, const std::vector<real_t>& coords);
};