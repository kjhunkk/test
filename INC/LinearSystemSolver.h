#pragma once

#include <memory>
#include <string>
#include <fstream>

#include "DataType.h"
#include "SingletonRegistry.h"
#include "MPIHeader.h"
#include "Config.h"

class LinearSystemSolver : public SingletonRegistry<LinearSystemSolver> // for implicit
{
protected:
	LinearSystemSolver() { iteration_ = 0; nonzero_guess_ = PETSC_FALSE; }

	virtual ~LinearSystemSolver() {}
	
public:
	virtual void Initialize() = 0;

	virtual PetscErrorCode Solve(const Mat& A, const Vec& b, Vec& solution) = 0;

	inline PetscInt getIterationNumber() const { return iteration_; }

	inline PetscBool getInitialGuessNonzero() const { return nonzero_guess_; }

	void PostMatrix(const Mat& A, const std::string& title);

	void PostVector(const Vec& x, const std::string& title);

protected:
	PetscInt iteration_;

	PetscBool nonzero_guess_;
};