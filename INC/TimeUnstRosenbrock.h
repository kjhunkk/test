#pragma once

#include "TimeUnsteady.h"
#include "LinearSystemSolver.h"

class TimeUnstRosenbrock : public TimeUnsteady
{
protected:
	TimeUnstRosenbrock();

	virtual ~TimeUnstRosenbrock();

public:
	virtual void initialize();

	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);

	inline int_t getTimeOrder() const { return time_order_; }

	inline void setTimeOrder(const int_t time_order) { time_order_ = time_order; }

protected:
	void ConvertSolutionArray(Vec& vector, const int_t num_cells, const std::vector<std::vector<real_t> >& solution, const std::vector<int_t>& cell_petsc_index);

	void AddVectorToSolution(const real_t scale_scalar, const Vec& vector, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, std::vector<std::vector<real_t> >& solution);
	
	virtual void InitializeCoeff() = 0;

protected:
	LinearSystemSolver * linear_system_solver_;

	int_t time_order_;

	int_t num_stages_;

	PetscInt linear_solver_iteration_;

	bool is_initialized_;

	PetscErrorCode ierr_;

	std::vector<std::vector<real_t> > pseudo_solution_;

	Vec delta_;

	Vec pseudo_solution_vector_;

	Vec rhs_vector_;

	std::vector<Vec> stage_solution_;

	real_t coeff_gamma_;

	std::vector<std::vector<real_t> > coeff_a_;

	std::vector<std::vector<real_t> > coeff_c_;

	std::vector<real_t> coeff_m_;
};

class RosenbrockROS3P : public TimeUnstRosenbrock // Lang-Verwer ROS3P (RO3-3)
{
private:
	static RosenbrockROS3P rosenbrock_ros3p_;

protected:
	RosenbrockROS3P();

	virtual ~RosenbrockROS3P();

protected:
	virtual void InitializeCoeff();
};

class RosenbrockRODAS3 : public TimeUnstRosenbrock // Hairer-Wanner RODAS3 (RO3-4)
{
private:
	static RosenbrockRODAS3 rosenbrock_rodas3_;

protected:
	RosenbrockRODAS3();

	virtual ~RosenbrockRODAS3();

protected:
	virtual void InitializeCoeff();
};

class RosenbrockROS4 : public TimeUnstRosenbrock // Shampine ROS4 (RO4-4)
{
private:
	static RosenbrockROS4 rosenbrock_ros4_;

protected:
	RosenbrockROS4();

	virtual ~RosenbrockROS4();

protected:
	virtual void InitializeCoeff();
};

class RosenbrockRODASP : public TimeUnstRosenbrock // Steinebach RODASP (RO4-6)
{
private:
	static RosenbrockRODASP rosenbrock_rodasp_;

protected:
	RosenbrockRODASP();

	virtual ~RosenbrockRODASP();

protected:
	virtual void InitializeCoeff();
};

class RosenbrockRODASS : public TimeUnstRosenbrock // Di Marzo RODASS-Rod5_1 (RO5-8)
{
private:
	static RosenbrockRODASS rosenbrock_rodass_;

protected:
	RosenbrockRODASS();

	virtual ~RosenbrockRODASS();

protected:
	virtual void InitializeCoeff();
};

class RosenbrockROW6A : public TimeUnstRosenbrock // Kaps and Wanner ROW6A (RO6-6)
{
private:
	static RosenbrockROW6A rosenbrock_row6a_;

protected:
	RosenbrockROW6A();

	virtual ~RosenbrockROW6A();

protected:
	virtual void InitializeCoeff();
};
