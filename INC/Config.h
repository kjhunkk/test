#pragma once

#include <string>
#include <array>
#include <fstream>
#include <mpi.h>

#include "DataType.h"
#include "MPIHeader.h"

#ifdef _WINDOW_
#include <direct.h>
#else
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#endif

class Config
{
private:
	static Config config_;

private:
	std::string config_file_name_;

	std::string grid_format_;

	std::string grid_file_name_;

	std::string return_address_;

	std::string HOM_name_;

	std::string equation_name_;

	std::string problem_name_;

	std::string convflux_name_;

	std::string time_scheme_name_;

	std::string limiter_name_;

	std::string pressure_fix_;

	std::string history_;

	int_t dimension_;

	int_t order_;

	real_t CFL_;

	real_t min_pseudo_CFL_; // for implicit

	real_t max_pseudo_CFL_; // for implicit

	real_t SER_up_factor_; // for implicit

	real_t SER_start_error_; // for implicit

	int_t itermax_;

	real_t start_time_;

	real_t end_time_;

	int_t subcell_resolution_;

	std::array<int_t, 3> print_option_;

	std::array<int_t, 3> save_option_;

	std::string restart_;

	int_t pseudo_max_iteration_; // for implicit

	real_t pseudo_target_residual_; // for implicit

	real_t physical_time_step_; // for implicit

	real_t pseudo_CFL_; // for implicit

	std::string nonzero_guess_; // for implicit

	std::string linear_system_solver_; // for implicit

	std::string fixed_time_step_; // for implicit

	int_t time_order_; // for implicit

protected:
	Config() {};

	~Config() {};

public:
	static Config& getInstance()
	{
		static Config instance;
		return instance;
	}

	void readConfigFile(const std::string& config_file_name);

	inline void renewal() { MakeDirectories(); WriteConfigFile(); };

	inline void setGridFileName(const std::string& grid_file_name) { grid_file_name_ = grid_file_name; };

	inline void setReturnAddress(const std::string& return_address) { return_address_ = return_address; };

	inline void setOrder(const int_t order) { order_ = order; };

	inline void setTimeSchemeName(const std::string& time_scheme_name) { time_scheme_name_ = time_scheme_name; };

	inline void setLimiterName(const std::string& limiter_name) { limiter_name_ = limiter_name; };

	inline void setPressureFix(const std::string& pressure_fix) { pressure_fix_ = pressure_fix; };

	inline void setIterMax(const int_t itermax) { itermax_ = itermax; } // for implicit

	inline void setPhysicalTimeStep(const real_t physical_time_step) {	physical_time_step_ = physical_time_step; } // for implicit

	inline void setTimeOrder(const int_t time_order) { time_order_ = time_order; } // for implicit

	inline void setReternAddress(const std::string& return_address) { return_address_ = return_address; } // for implicit

	inline const std::string& getGridFormat() const { return grid_format_; };

	inline const std::string& getGridFileName() const { return grid_file_name_; };

	inline const std::string& getReturnAddress() const { return return_address_; };

	inline const std::string& getHOMName() const { return HOM_name_; };

	inline const std::string& getEquationName() const { return equation_name_; };

	inline const std::string& getProblemName() const { return problem_name_; };

	inline const std::string& getConvFluxName() const { return convflux_name_; };

	inline const std::string& getTimeSchemeName() const { return time_scheme_name_; };

	inline const std::string& getLimiterName() const { return limiter_name_; };

	inline const std::string& getPressureFix() const { return pressure_fix_; };

	inline const std::string& getHistory() const { return history_; };

	inline const std::string& getRestart() const { return restart_; };

	inline int_t getDimension() const { return dimension_; };

	inline int_t getOrder() const { return order_; };

	inline int_t getSubcellResolution() const { return subcell_resolution_; };

	inline int_t getIterMax() const { return itermax_; };

	inline const std::array<int_t, 3>& getPrintOption() const { return print_option_; };

	inline const std::array<int_t, 3>& getSaveOption() const { return save_option_; };

	inline real_t getStartTime() const { return start_time_; };

	inline real_t getEndTime() const { return end_time_; };

	inline real_t getCFL() const { return CFL_; };
	
	inline int_t getPseudoMaxIteration() const { return pseudo_max_iteration_; } // for implicit

	inline real_t getPseudoTargetResidual() const { return pseudo_target_residual_; } // for implicit

	inline real_t getPhysicalTimeStep() const { return physical_time_step_; } // for implicit

	inline real_t getPseudoCFL() const { return pseudo_CFL_; } // for implicit

	inline const std::string& getNonzeroGuess() const { return nonzero_guess_; } // for implicit

	inline const std::string& getLinearSystemSolver() const { return linear_system_solver_; } // for implicit

	inline const std::string& getIsFixedTimeStep() const { return fixed_time_step_; } // for implicit

	inline int_t getTimeOrder() const { return time_order_; } // for implicit

	inline real_t getPseudoCFLmin() const { return min_pseudo_CFL_; } // for implicit

	inline real_t getPseudoCFLmax() const { return max_pseudo_CFL_; } // for implicit

	inline real_t getSERupfactor() const { return SER_up_factor_; } // for implicit

	inline real_t getSERstarterror() const { return SER_start_error_; } // for implicit

private:
	void ReadDataFromFile(std::ifstream& infile, const std::string& text, const std::string& dataname, real_t* data);

	void ReadDataFromFile(std::ifstream& infile, const std::string& text, const std::string& dataname, int_t* data);

	void ReadDataFromFile(std::ifstream& infile, const std::string& text, const std::string& dataname, std::array<int_t, 3> *data);

	void ReadDataFromFile(std::ifstream& infile, const std::string& text, const std::string& dataname, std::string& data);

	void WriteDataToFile(std::ofstream& outfile, const std::string& dataname, const real_t& data);

	void WriteDataToFile(std::ofstream& outfile, const std::string& dataname, const int_t& data);

	void WriteDataToFile(std::ofstream& outfile, const std::string& dataname, const std::array<int_t, 3>& data);

	void WriteDataToFile(std::ofstream& outfile, const std::string& dataname, const std::string& data);

	void MakeDirectories();

	void WriteConfigFile();
};
