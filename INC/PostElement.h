
#pragma once

#include <vector>

#include "MPIHeader.h"
#include "DataType.h"
#include "Jacobian.h"

class PostElement;

class PostElementFactory
{
public:
	PostElementFactory();

	~PostElementFactory();

	static PostElementFactory& getInstance();

	PostElement* getPostElement(ElemType element_type);

private:
	PostElement* newPostElement(ElemType element_type);

private:
	std::vector<PostElement*> registry_;
};

class PostElement
{
protected:
	int_t dimension_;

public:
	PostElement() {};

	PostElement(int_t dimension):dimension_(dimension) {};

	~PostElement() {};

public:
	const std::vector<real_t> getPostCoords(const int_t element_type, const int_t element_order, const int_t post_order, const std::vector<real_t>& coords);

	const std::vector<real_t> getFacePostCoords(const int_t face_element_type, const int_t face_element_order, const int_t post_order, const std::vector<real_t>& face_coords);

	virtual const std::vector<int_t> getPostConnects(const int_t post_order) = 0;

protected:
	virtual const std::vector<real_t> getPostComCoords(const int_t post_order) = 0;
};

class LinePostElement : public PostElement
{
public:
	LinePostElement() :PostElement(1) {};

	~LinePostElement() {};

public:
	virtual const std::vector<int_t> getPostConnects(const int_t post_order);

protected:
	virtual const std::vector<real_t> getPostComCoords(const int_t post_order);
};

class TrisPostElement : public PostElement
{
public:
	TrisPostElement():PostElement(2) {};

	~TrisPostElement() {};

public:
	virtual const std::vector<int_t> getPostConnects(const int_t post_order);

protected:
	virtual const std::vector<real_t> getPostComCoords(const int_t post_order);
};

class QuadPostElement : public PostElement
{
public:
	QuadPostElement() :PostElement(2) {};

	~QuadPostElement() {};

public:
	virtual const std::vector<int_t> getPostConnects(const int_t post_order);

protected:
	virtual const std::vector<real_t> getPostComCoords(const int_t post_order);
};

class TetsPostElement : public PostElement
{
public:
	TetsPostElement() :PostElement(3) {};

	~TetsPostElement() {};

public:
	virtual const std::vector<int_t> getPostConnects(const int_t post_order);

protected:
	virtual const std::vector<real_t> getPostComCoords(const int_t post_order);

private:
	const std::vector<int_t> getType1Connects(const std::vector<int_t>& pts);

	const std::vector<int_t> getType2Connects(const std::vector<int_t>& pts);

	const std::vector<int_t> getType3Connects(const std::vector<int_t>& pts);
};

class HexaPostElement : public PostElement
{
public:
	HexaPostElement() :PostElement(3) {};

	~HexaPostElement() {};

public:
	virtual const std::vector<int_t> getPostConnects(const int_t post_order);

protected:
	virtual const std::vector<real_t> getPostComCoords(const int_t post_order);

private:
	const std::vector<int_t> getType1Connects(const std::vector<int_t>& pts);
};

class PrisPostElement : public PostElement
{
public:
	PrisPostElement() :PostElement(3) {};

	~PrisPostElement() {};

public:
	virtual const std::vector<int_t> getPostConnects(const int_t post_order);

protected:
	virtual const std::vector<real_t> getPostComCoords(const int_t post_order);

private:
	const std::vector<int_t> getType1Connects(const std::vector<int_t>& pts);

	const std::vector<int_t> getType2Connects(const std::vector<int_t>& pts);
};