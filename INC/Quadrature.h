
#pragma once

#include <vector>
#include <fstream>
#include <cmath>
#include <memory>

#include "DataType.h"
#include "MPIHeader.h"
#include "Jacobian.h"
#include "Element.h"
#include "Basis.h"

#include "../LIB/armadillo"

#define ARMA_USE_LAPACK
#define ARMA_USE_BLAS

namespace Quadrature
{
	namespace Volume
	{
		// using this function
		void getVolumeQdrt(const ElemType elemtype, std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);

		// experimental version. generate non-optimal quadrature on physical domain (DQM)
		bool getVolumePhyQdrt(const Element* element, const int_t element_order, const std::vector<real_t>& coords, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);

		// experimental version. generate DRM stiff matrices (cell_coefficients_)
		// bool getCellCoefficients(const Element* element, const int_t element_order, const std::vector<real_t>& coords, const int_t degree)

		// read vandermonde points for non-optimal quadrature on physical domain
		bool readVanPts(const Element* element, const int_t degree, std::vector<real_t>& points);
		bool readVanTrisPts(const int_t degree, std::vector<real_t>& points);
		bool readVanQuadPts(const int_t degree, std::vector<real_t>& points);
		bool readVanTetsPts(const int_t degree, std::vector<real_t>& points);

		// get transformed standard element quadrature
		void getStdTrisQdrt(std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);
		void getStdQuadQdrt(std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);
		void getStdTetsQdrt(std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);
		void getStdHexaQdrt(std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);
		void getStdPrisQdrt(std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);

		// read standard element quadrature (optimal)
		void readStdTrisQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);
		void readStdQuadQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);
		void readStdTetsQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);
		void readStdHexaQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);
		void readStdPrisQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);

		// read degenerated standard element quadrature for high degree 
		void readDgenStdTrisQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);
		void readDgenStdTetsQdrt(const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights);

		// elementary function
		void readGaussLegendrePoints(const int_t num_points, std::vector<real_t>& GL_points, std::vector<real_t>& GL_weights);
	}

	namespace Surface
	{
		// using this function
		void getSurfaceQdrt(const ElemType elemtype, std::shared_ptr<Jacobian> jacobian, const int_t degree, std::vector<real_t>& quad_points, std::vector<real_t>& quad_weights, std::vector<real_t>& adjmat);
	}
}