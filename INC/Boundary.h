
#pragma once

#include <vector>
#include <string>

#include "DataType.h"
#include "MPIHeader.h"

class Boundary
{
public:
	Boundary() {};

	~Boundary() {};

public:
	inline const std::string& getTitle() const { return title_; };

	inline const std::string& getName() const { return name_; };

	inline const std::string& getType() const { return type_; };

	inline int_t getTagNumber() const { return tag_number_; };

	inline const std::vector<real_t>& getReferences() const { return references_; };

	inline void setTitle(const std::string& title) { title_ = title; };

	inline void setName(const std::string& name) { name_ = name; };

	inline void setType(const std::string& type) { type_ = type; };

	inline void setTagNumber(const int_t tag_number) { tag_number_ = tag_number; };

	inline void setReferences(const std::vector<real_t> references) { references_ = references; };

protected:
	std::string title_;

	std::string name_;

	std::string type_;

	int_t tag_number_;

	std::vector<real_t> references_;
};

template<typename Boundary>
class BoundaryFactory
{
protected:
	BoundaryFactory() {};

	~BoundaryFactory() {};

public:
	static BoundaryFactory<Boundary>& getInstance()
	{
		static BoundaryFactory<Boundary> instance;
		return instance;
	}

	void clearRegistry(){
		for (auto&& it : registry_){
			MASTER_MESSAGE("Boundary condition (name: " + it->getType() + ", tag: " + TO_STRING(it->getTagNumber()) + ") is deleted!");
			delete (it);
		}
	}

	void addClone(Boundary* boundary)
	{
		for (auto&& one : clone_){
			if (!boundary->getType().compare(one->getType()))
				return;
		}
		clone_.push_back(boundary);
	}

	Boundary* getBoundary(const std::string& type, const int_t tag_number, const std::string& name, const std::vector<real_t>& references)
	{
		std::string bc_title = TO_UPPER(type) + TO_STRING(tag_number);
		for (auto&& one : registry_){
			if (!bc_title.compare(one->getTitle())) return one;
		}
		std::string bc_type = TO_UPPER(type);
		for (auto&& one : clone_){
			if (!bc_type.compare(one->getType())){
				Boundary* newone = one->clone();
				newone->setTitle(bc_title);
				newone->setType(bc_type);
				newone->setTagNumber(tag_number);
				newone->setName(name);
				newone->setReferences(references);
				registry_.push_back(newone);
				return newone;
			}
		}
		MASTER_ERROR("Requested boundary condition(Name: " + name + ", Type: " + type + ", Tag: " + TO_STRING(tag_number) + ") is not supported");
		return 0;
	}

private:
	std::vector<Boundary*> registry_;

	std::vector<Boundary*> clone_;
};

