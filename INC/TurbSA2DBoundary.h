
#pragma once

#include <vector>
#include <memory>
#include <algorithm>
#include <string>

#include "DataType.h"
#include "MPIHeader.h"
#include "Boundary.h"
#include "TurbSA2DFlux.h"

class TurbSA2DBoundary : public Boundary
{
protected:
	TurbSA2DFlux* flux_;

	const real_t free_dnut_ = 4.0;

public:
	TurbSA2DBoundary() {};

	~TurbSA2DBoundary() {};

public:
	void setFlux(TurbSA2DFlux* flux) { flux_ = flux; };

	virtual TurbSA2DBoundary* clone() = 0;

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0; // for implicit
};

class TurbSA2DBCExtrapolate : public TurbSA2DBoundary
{
private:
	static TurbSA2DBCExtrapolate TurbSA_2d_extrapolate_;

public:
	TurbSA2DBCExtrapolate() { type_ = TO_UPPER("Extrapolate"); BoundaryFactory<TurbSA2DBoundary>::getInstance().addClone(this); };

	~TurbSA2DBCExtrapolate() {};

public:
	virtual TurbSA2DBoundary* clone() { return new TurbSA2DBCExtrapolate; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class TurbSA2DBCConstant : public TurbSA2DBoundary
{
private:
	static TurbSA2DBCConstant TurbSA_2d_constant_;

public:
	TurbSA2DBCConstant() { type_ = TO_UPPER("Constant"); BoundaryFactory<TurbSA2DBoundary>::getInstance().addClone(this); };

	~TurbSA2DBCConstant() {};

public:
	virtual TurbSA2DBoundary* clone() { return new TurbSA2DBCConstant; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class TurbSA2DBCWall : public TurbSA2DBoundary
{
private:
	static TurbSA2DBCWall TurbSA_2d_wall_;

public:
	TurbSA2DBCWall() { type_ = TO_UPPER("Wall"); BoundaryFactory<TurbSA2DBoundary>::getInstance().addClone(this); };

	~TurbSA2DBCWall() {};

public:
	virtual TurbSA2DBoundary* clone() { return new TurbSA2DBCWall; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class TurbSA2DBCSlipWall : public TurbSA2DBoundary
{
private:
	static TurbSA2DBCSlipWall TurbSA_2d_slipwall_;

public:
	TurbSA2DBCSlipWall() { type_ = TO_UPPER("SlipWall"); BoundaryFactory<TurbSA2DBoundary>::getInstance().addClone(this); };

	~TurbSA2DBCSlipWall() {};

public:
	virtual TurbSA2DBoundary* clone() { return new TurbSA2DBCSlipWall; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class TurbSA2DBCSubOut : public TurbSA2DBoundary
{
private:
	static TurbSA2DBCSubOut TurbSA_2d_subout_;

public:
	TurbSA2DBCSubOut() { type_ = TO_UPPER("SubOut"); BoundaryFactory<TurbSA2DBoundary>::getInstance().addClone(this); };

	~TurbSA2DBCSubOut() {};

public:
	virtual TurbSA2DBoundary* clone() { return new TurbSA2DBCSubOut; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class TurbSA2DBCSubIn : public TurbSA2DBoundary
{
private:
	static TurbSA2DBCSubIn TurbSA_2d_subin_;

public:
	TurbSA2DBCSubIn() { type_ = TO_UPPER("SubIn"); BoundaryFactory<TurbSA2DBoundary>::getInstance().addClone(this); };

	~TurbSA2DBCSubIn() {};

public:
	virtual TurbSA2DBoundary* clone() { return new TurbSA2DBCSubIn; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class TurbSA2DBCSupOut : public TurbSA2DBoundary
{
private:
	static TurbSA2DBCSupOut TurbSA_2d_supout_;

public:
	TurbSA2DBCSupOut() { type_ = TO_UPPER("SupOut"); BoundaryFactory<TurbSA2DBoundary>::getInstance().addClone(this); };

	~TurbSA2DBCSupOut() {};

public:
	virtual TurbSA2DBoundary* clone() { return new TurbSA2DBCSupOut; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class TurbSA2DBCSupIn : public TurbSA2DBoundary
{
private:
	static TurbSA2DBCSupIn TurbSA_2d_supin_;

public:
	TurbSA2DBCSupIn() { type_ = TO_UPPER("SupIn"); BoundaryFactory<TurbSA2DBoundary>::getInstance().addClone(this); };

	~TurbSA2DBCSupIn() {};

public:
	virtual TurbSA2DBoundary* clone() { return new TurbSA2DBCSupIn; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class TurbSA2DBCFarField : public TurbSA2DBoundary
{
private:
	static TurbSA2DBCFarField TurbSA_2d_farfield_;

public:
	TurbSA2DBCFarField() { type_ = TO_UPPER("FarField"); BoundaryFactory<TurbSA2DBoundary>::getInstance().addClone(this); };

	~TurbSA2DBCFarField() {};

public:
	virtual TurbSA2DBoundary* clone() { return new TurbSA2DBCFarField; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};