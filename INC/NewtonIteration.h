#pragma once

#include <memory>
#include <string>
#include <fstream>

#include "DataType.h"
#include "MPIHeader.h"
#include "Config.h"
#include "Zone.h"
#include "LinearSystemSolver.h"

class NewtonIteration
{
public:
	NewtonIteration();

	~NewtonIteration() { VecDestroy(&old_solution_); VecDestroy(&solution_delta_); };

public:
	void Start();

	void Initialize(const Vec& vector);

	bool NewtonSolve(const Mat& A, const Vec& b, Vec& x, int_t& linear_solver_iteration, real_t& newton_convergence_ratio);

protected:
	LinearSystemSolver * linear_system_solver_;

	int_t newton_iteration_;

	int_t newton_max_iteration_;

	real_t old_rms_error_;

	real_t newton_convergence_tol_;

	Vec old_solution_;

	Vec solution_delta_;

protected:
	real_t CalRmsError(const Vec& x);
};