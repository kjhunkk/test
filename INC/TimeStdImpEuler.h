#pragma once

#include <petsc.h>

#include "TimeSteady.h"
#include "LinearSystemSolver.h"

class TimeStdImpEuler : public TimeSteady // for implicit
{
private:
	static TimeStdImpEuler std_imp_euler_;

protected:
	TimeStdImpEuler();

	virtual ~TimeStdImpEuler();

public:
	virtual void initialize();

	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);

	const std::array<real_t, 2>& getResidualNorms() const {	return residual_norms_;	}

protected:
	void computeError(const Vec& input_vector, const int_t iteration);

	// convergence acceleration

protected:
	LinearSystemSolver * linear_system_solver_;

	Vec delta_;

	PetscErrorCode ierr_;

	PetscInt linear_solver_iteration_;

	int_t pseudo_iteration_;

	PetscReal reference_error_;
	
	std::array<real_t, 2> residual_norms_;

	bool is_initialized_;
};