#pragma once

#include <string>

#define MPI_INT_T MPI_INT

#define MPI_REAL_T MPI_DOUBLE

typedef int int_t;

typedef double real_t;

const int_t MASTER_NODE = 0;

enum class ElemType
{
	LINE = 0,
	TRIS = 1,
	QUAD = 2,
	TETS = 3,
	HEXA = 4,
	PRIS = 5,
	PYRA = 6,
	SPLX = 7,
	NUM_OF_ELEMENT = 7,
	NOT_IN_LIST = -1
};

template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

//#define _LINUX_
#ifndef _LINUX_
#define _WINDOW_ // for Config.h
#endif

#define _Pressure_Fix_Hist_
// #define _Pressure_Fix_Numoff_ 1

#define _CONSTANT_VISCOSITY_

//#define _Order_Test_ // for implicit