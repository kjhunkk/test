#pragma once

#include "LinearSystemSolver.h"

class LinearSystemGMRES : public LinearSystemSolver // for implicit
{
private:
	static LinearSystemGMRES gmres_;

protected:
	LinearSystemGMRES();

	virtual ~LinearSystemGMRES();

public:
	virtual void Initialize();

	virtual PetscErrorCode Solve(const Mat& A, const Vec& b, Vec& solution);

protected:
	KSP ksp_;

	PC pc_;

	PetscErrorCode ierr_;

	PetscInt GMRES_max_iter_;

	PCSide pc_side_;

	PCType pc_type_;

	PetscReal rel_tol;

	PetscReal abs_tol;

	PetscReal div_tol;

	PetscInt Krylov_max_iter;
};