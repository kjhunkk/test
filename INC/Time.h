
#pragma once

#include <memory>
#include <vector>
#include <array>
#include <array>
#include <chrono>
#include <bitset>
#include <fstream>

#include "DataType.h"
#include "MPIHeader.h"
#include "Zone.h"
#include "Config.h"
#include "TimeScheme.h"
#include "Post.h"

class Time
{
private:
	TimeScheme* timescheme_;

	std::shared_ptr<Post> post_;

	int_t itermax_;

	std::array<int_t, 3> print_option_;

	std::array<int_t, 3> save_option_;

	real_t start_time_;

	real_t end_time_;

	real_t CFL_;

	real_t pseudo_CFL_min_; // for implicit

	real_t pseudo_CFL_max_; // for implicit

	real_t SER_up_factor_; // for implicit

	real_t SER_start_error_; // for implicit

	bool is_variable_cfl_; // for implicit

	real_t current_time_;

	real_t dt_;

	int_t iteration_;

	int_t strandid_;

	std::chrono::system_clock::time_point start_clock_;

	std::chrono::system_clock::time_point now_clock_;

	std::chrono::duration<real_t> time_gap_;

	bool complete_;

	bool history_;

public:
	Time();

	~Time() {};

	void integrate(std::shared_ptr<Zone> zone);

	void setPost(std::shared_ptr<Post> post) { post_ = post; };

private:
	void setTimeStep(const real_t dt); // for implicit

	void printMessage(const std::string& input_message); // for implicit

	real_t getUpdatedCFL(const real_t relative_error); // for implciit

	bool printCheck();

	bool saveCheck();
};