
#pragma once

#include <string>
#include <vector>
#include <cmath>
#include <fstream>

#include "SingletonRegistry.h"

class Euler2DFlux : public SingletonRegistry<Euler2DFlux>
{
protected:
	const int_t dimension_ = 2;

	const int_t num_states_ = 4;

	bool viscous_on_;

	real_t Re_;

	real_t Ma_;

	real_t Pr_;

	real_t Tref_;

	real_t Sutherland_coef_;

	real_t alpha_;

	real_t beta_;

	real_t max_for_visradii_;

	const real_t eps_; // for implicit

protected:
	Euler2DFlux();

	~Euler2DFlux() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name);

public:
	inline bool isViscFlux() const { return viscous_on_; };

	inline bool isSource() const { return false; };

	inline real_t getReynolds() const { return Re_; };

	inline real_t getMach() const { return Ma_; };

	inline real_t getPrandtl() const { return Pr_; };

	inline real_t getAlpha() const { return alpha_; };

	inline real_t getBeta() const { return beta_; };

	inline real_t getSutherlandCoeff() const { return Sutherland_coef_; }; // for implicit

	const std::vector<real_t> calPressure(const std::vector<real_t>& u) const;

	const std::vector<real_t> calTemperature(const std::vector<real_t>& u) const;

	const std::vector<real_t> calViscosity(const std::vector<real_t>& temperature) const;

	const std::vector<real_t> calPressureCoefficients(const std::vector<real_t>& u) const;

	const std::vector<real_t> commonConvFlux(const std::vector<real_t>& u) const;

	const std::vector<real_t> commonConvFluxJacobian(const std::vector<real_t>& u) const; // for implicit

	const std::vector<real_t> commonConvFlux(const std::vector<real_t>& u, const std::vector<real_t>& normal) const;

	const std::vector<real_t> commonConvFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& normal) const; // for implicit
	
	const std::vector<real_t> commonViscFlux(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const;

	const std::vector<real_t> commonViscFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const; // for implicit

	const std::vector<real_t> commonViscFluxGradJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const; // for implicit

	const std::vector<real_t> commonViscFlux(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const;

	const std::vector<real_t> commonViscFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> commonViscFluxGradJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const;

	const std::vector<real_t> numViscFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFluxOwnerGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFluxNeighborGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> convWaveVelocity(const std::vector<real_t>& u) const;

	const std::vector<real_t> viscWaveVelocity(const std::vector<real_t>& u) const;

	const std::vector<real_t> source(const std::vector<real_t>& u) const;

	const std::vector<real_t> source(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const;

	virtual const std::vector<real_t> numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const = 0;

	const std::vector<real_t> numConvFluxOwnerCentralJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numConvFluxNeighborCentralJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const = 0; // for implicit
};


class Euler2DLLF : public Euler2DFlux
{
private:
	static Euler2DLLF Euler_2d_LLF_;

protected:
	Euler2DLLF();

	~Euler2DLLF() {};

public:
	virtual const std::vector<real_t> numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;
};


class Euler2DRoe : public Euler2DFlux
{
private:
	static Euler2DRoe Euler_2d_Roe_;

protected:
	Euler2DRoe();

	~Euler2DRoe() {};

public:
	virtual const std::vector<real_t> numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;
};


class Euler2DAUSM : public Euler2DFlux
{
private:
	static Euler2DAUSM Euler_2d_ausm_;

protected:
	Euler2DAUSM();

	~Euler2DAUSM() {};

public:
	virtual const std::vector<real_t> numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;
};