#pragma once

#include <vector>
#include <string>
#include <algorithm>
#include <fstream>

#include "mpi.h"

#define MASTER_NODE 0

class TecWrite
{
public:
	enum class FileType {
		FULL = 0, GRID = 1, SOLUTION = 2
	};
	enum class ZoneType {
		FETRIS = 2, FETETS = 4
	};

public:
	TecWrite();

	~TecWrite() {};

private:
	const char* version_ = "#!TDV112";

	std::vector<int> buffer_;

	int strandid_index_;

	int solution_time_index_;

	int myrank_;

	int ndomain_;

	int header_size_;

	int first_var_displacement_;

	int next_var_displacement_;

	int next_connect_displacement_;

	int first_node_index_;

public:
	void resetBuffer();

	void setHeader(const FileType filetype, const std::string& title, const int num_variables, const std::vector<std::string>& variable_names, const ZoneType zonetype, const int num_nodes, const int num_cells);

	void setStrandID(const int strandid);

	void setSolutionTime(const double solution_time);

	void writeTecGridFile(const std::string& filename, const std::vector<std::vector<float>>& coords, const std::vector<int>& connectivity);

	void writeTecSolutionFile(const std::string& filename, const std::vector<std::vector<float>>& solutions);

private:
	void writeIntDataBuffer(const int data);

	void writeReal32DataBuffer(const float data);

	void writeReal64DataBuffer(const double data);

	void writeStringDataBuffer(const std::string data);

};
