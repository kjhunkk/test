#pragma once

#include "mpi.h"
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <cmath>
#include <chrono>
#include <petsc.h>

#include "DataType.h"

class MPIHeader
{
private:
	static std::string code_name_;

	static int_t myrank_;

	static int_t ndomain_;

	static std::chrono::system_clock::time_point start_time_;

	static std::chrono::system_clock::time_point end_time_;

public:
	static MPIHeader MPI_header_;

protected:
	MPIHeader();

	~MPIHeader();

public:
	static const std::string getInputHeader() { return "INP/"; };

	static const std::string getResourceHeader() { return "RSC/"; };

	static const std::string getCodeName() { return code_name_; };

	static void Error(bool master_only, const std::string& message, const std::string& file, const int_t line);

	static void Message(bool master_only, const std::string& message);

	static void ListPrint(bool master_only, const int_t rank, const std::string& header, const std::vector<int_t> list);

	static void ListPrint(bool master_only, const int_t rank, const std::string& header, const std::vector<real_t> list);

	static int_t getMyrank();

	static int_t getNumDomain();

	static const std::string to_string(const int_t data);

	static const std::string to_string(const real_t data);

	static const std::string to_uppercase(const std::string& str);

	static void Syncro();

	static void startStopWatch();

	static real_t stopStopWatch();

	static real_t getTimeStopWatch();

	static void Pause(const std::string& file, const int_t line); // for implicit

	static void MasterPause(const std::string& file, const int_t line); // for implicit

	static void dataCommunication(const std::vector<real_t*>& send_data, const std::vector<int_t>& send_size, std::vector<real_t*>& recv_data, const std::vector<int_t>& recv_size);

	static void dataCommunication(const std::vector<int_t*>& send_data, const std::vector<int_t>& send_size, std::vector<int_t*>& recv_data, const std::vector<int_t>& recv_size);
};

#define INPUT() MPIHeader::getInputHeader()
#define RESOURCE() MPIHeader::getResourceHeader()
#define CODENAME() MPIHeader::getCodeName()
#define MASTER_MESSAGE(message) MPIHeader::Message(true, message)
#define MEMBER_MESSAGE(message) MPIHeader::Message(false, message)
#define MASTER_ERROR(message) MPIHeader::Error(true, message, __FILE__, __LINE__)
#define MEMBER_ERROR(message) MPIHeader::Error(false, message, __FILE__, __LINE__)
#define MASTER_LISTPRINT(header,list) MPIHeader::ListPrint(true, MASTER_NODE, header, list)
#define MEMBER_LISTPRINT(rank,header,list) MPIHeader::ListPrint(false, rank, header, list)
#define MYRANK() MPIHeader::getMyrank()
#define NDOMAIN() MPIHeader::getNumDomain()
#define TO_STRING(value) MPIHeader::to_string(value)
#define TO_UPPER(str) MPIHeader::to_uppercase(str)
#define SYNCRO() MPIHeader::Syncro()
#define START() MPIHeader::startStopWatch()
#define STOP() MPIHeader::stopStopWatch()
#define TIME() MPIHeader::getTimeStopWatch()
#define PAUSE() MPIHeader::Pause(__FILE__, __LINE__) // for implicit
#define MASTER_PAUSE() MPIHeader::MasterPause(__FILE__, __LINE__); // for implicit
