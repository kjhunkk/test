#pragma once

#include "TimeUnsteady.h"
#include "LinearSystemSolver.h"

#define VECTOR_ARRAY_CREATOR(arr, ierr) \
	for (int_t i = 0; i < arr.size(); i++) \
		{ ierr = VecCreate(MPI_COMM_WORLD, &arr[i]); }

#define VECTOR_ARRAY_DESTROYER(arr, ierr) \
	for (int_t i = 0; i < arr.size(); i++) \
		{ ierr = VecDestroy(&arr[i]); }

class TimeUnstTIAS : public TimeUnsteady // for implicit
{
private:
	static TimeUnstTIAS unst_imp_tias_;

protected:
	TimeUnstTIAS();

	virtual ~TimeUnstTIAS();

public:
	virtual void initialize();

	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);

	inline int_t getTimeOrder() const { return time_order_; }

	inline void setTimeOrder(const int_t time_order) { time_order_ = time_order; }

protected:
	void InitializeVectors(std::shared_ptr<Zone> zone, const real_t dt);

	inline int_t SolveStage1(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm);

	inline int_t SolveStage2(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm);

	inline int_t SolveStage3(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm);

	inline int_t SolveStage4(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm);

	void InitializeCoeff();

	inline void ConvertSolutionToPetsc(const std::vector<std::vector<real_t> >& solution, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, Vec& vector);

	inline void ConvertPetscToSolution(std::shared_ptr<Zone> zone, const Vec& vector, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, std::vector<std::vector<real_t> >& solution);

	void SelfStartingProcedure(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);

protected:
	// Time integration configuration
	int_t time_order_;

	real_t diagonal_factor_;

	std::vector<real_t> BDF_alpha_;

	real_t BDF_beta_;

	std::vector<real_t> TIAS_alpha_;

	std::array<real_t, 3> TIAS_beta_;

	real_t TIAS_flat_beta_;

	Vec delta_;

	Vec intermediate_solution_vector_;

	std::vector<std::vector<real_t> > intermediate_solution_;

	Vec TIAS_rhs_;

	std::array<Vec, 3> stage_rhs_;

	std::array<Vec, 3> stage_solution_array_;

	std::vector<Vec> solution_array_;

	Mat system_matrix_;

	// Non-linear solver configuration
	int_t max_newton_iteration_;

	real_t newton_tolerance_;	

	real_t recompute_tol_;

	// Linear solver configuration
	LinearSystemSolver * linear_system_solver_;

	// etc
	bool is_initialized_;
};