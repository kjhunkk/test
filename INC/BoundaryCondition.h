#pragma once

#include <fstream>
#include <string>
#include <vector>

#include "DataType.h"
#include "MPIHeader.h"

class BoundaryCondition
{
private:
	static BoundaryCondition boundary_condition_;

private:
	std::vector<std::string> name_;

	std::vector<std::string> type_;

	std::vector<int_t> tag_number_;

	std::vector<int_t> references_index_;

	std::vector<std::vector<real_t>> references_;

protected:
	BoundaryCondition() {};

	~BoundaryCondition() {};

public:
	static BoundaryCondition& getInstance()
	{
			static BoundaryCondition instance;
			return instance;
	}

	void readBC(const std::string& file_name);

	const std::string& getBCType(int_t tag_number);

	const std::string& getBCName(int_t tag_number);

	const std::vector<real_t> getBCReferences(int_t tag_number);

	int_t getTagNumber(const std::string& type);
};
