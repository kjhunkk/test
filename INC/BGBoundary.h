
#pragma once

#include <vector>
#include <memory>
#include <algorithm>
#include <string>

#include "DataType.h"
#include "MPIHeader.h"
#include "Boundary.h"
#include "BGFlux.h"

class BGBoundary : public Boundary
{
protected:
	BGFlux* flux_;

public:
	BGBoundary() {};

	~BGBoundary() {};

public:
	void setFlux(BGFlux* flux) { flux_ = flux; };

	virtual BGBoundary* clone() = 0;

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const = 0;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const = 0; // for implicit
};

class BGBCExtrapolate : public BGBoundary
{
private:
	static BGBCExtrapolate BG_extrapolate_;

public:
	BGBCExtrapolate() { type_ = TO_UPPER("Extrapolate"); BoundaryFactory<BGBoundary>::getInstance().addClone(this); };

	~BGBCExtrapolate() {};

public:
	virtual BGBoundary* clone() { return new BGBCExtrapolate; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const; // for implicit
};

class BGBCConstant : public BGBoundary
{
private:
	static BGBCConstant BG_constant_;

public:
	BGBCConstant() { type_ = TO_UPPER("Constant"); BoundaryFactory<BGBoundary>::getInstance().addClone(this); };

	~BGBCConstant() {};

public:
	virtual BGBoundary* clone() { return new BGBCConstant; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const; // for implicit
};