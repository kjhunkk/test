#pragma once

#include "TimeScheme.h"

class TimeUnsteady : public TimeScheme // for implicit
{
protected:
	TimeUnsteady() {}

	virtual ~TimeUnsteady() {}

public:
	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& integration) = 0;
};