
#pragma once

#include <memory>

#include "DataType.h"
#include "SingletonRegistry.h"
#include "MPIHeader.h"
#include "Zone.h"

class TimeScheme : public SingletonRegistry<TimeScheme>
{
protected:
	TimeScheme() { status_string_ = ""; relative_error_ = 1.0; }; // for implicit

	virtual ~TimeScheme() {};
public:
	virtual void initialize() = 0; // for implicit

	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration) = 0;

	inline bool getIsImplicit() const { return is_implicit_; } // for implicit

	inline bool getIsFixedDt() const { return is_fixed_dt_; } // for implicit

	inline const std::string& getStatusString() const { return status_string_; } // for implicit

	inline real_t getRelError() const { return relative_error_; } // for implicit(variable CFL)

protected:
	bool is_implicit_; // for implicit

	bool is_fixed_dt_; // for implicit

	std::string status_string_; // for implicit

	real_t relative_error_; // for implciit(variable CFL)
};

class ExplicitEuler : public TimeScheme
{
private:
	static ExplicitEuler explicit_euler_;

protected:
	ExplicitEuler();

	virtual ~ExplicitEuler() {}; // for implicit
public:
	virtual void initialize(); // for implicit

	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);
};

class TVDRK : public TimeScheme
{
private:
	static TVDRK tvdrk_;

protected:
	TVDRK();

	virtual ~TVDRK() {}; // for implicit
public:
	virtual void initialize(); // for implicit

	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);
};

class SSPRK : public TimeScheme
{
private:
	static SSPRK ssprk_;

protected:
	SSPRK();

	virtual ~SSPRK() {}; // for implicit
public:
	virtual void initialize(); // for implicit

	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);
};