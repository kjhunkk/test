#pragma once

#include "MPIHeader.h"
#include "DataType.h"
#include "Config.h"
#include "Grid.h"
#include "Equation.h"
#include "BoundaryCondition.h"
#include "Quadrature.h"
#include "PostElement.h"

class Aerodynamics
{
private:
	bool aerodynamics_;

	int_t order_;

	int_t post_order_;

	int_t dimension_;

	int_t num_states_;

	int_t num_basis_;

	int_t num_wall_bdries_;

	std::shared_ptr<Grid> grid_;

	std::vector<int_t> wall_bdry_list_;

	std::vector<int_t> num_bdry_quad_points_;

	std::vector<std::vector<real_t>> bdry_owner_basis_value_;

	std::vector<std::vector<real_t>> bdry_owner_coefficients_;

	std::vector<std::vector<real_t>>* solution_;

	Equation* equation_;

	// for Cp curve
	int_t num_subcell_nodes_;

	int_t num_subcell_cells_;

	std::string return_address_;

	std::vector<PostElement*> post_elements_;

	std::vector<std::vector<real_t>> coords_;

	std::vector<std::vector<real_t>> basis_;

public:
	Aerodynamics() {};

	~Aerodynamics() {};

public:
	void initialize(std::shared_ptr<Grid> grid, std::vector<std::vector<real_t>>* solution, Equation* equation, const int_t post_order);

	const std::vector<real_t> calForceCoefficients();

	void postPressureCoefficients(const std::string& title, const real_t time);

	void postDuringPressureCoefficients(int_t& strandid, const real_t time);
};