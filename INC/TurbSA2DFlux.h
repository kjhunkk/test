
#pragma once

#include <string>
#include <vector>
#include <cmath>
#include <fstream>

#include "SingletonRegistry.h"

class TurbSA2DFlux : public SingletonRegistry<TurbSA2DFlux>
{
protected:
	const int_t dimension_ = 2;

	const int_t num_states_ = 5;

	bool viscous_on_;

	real_t Re_;

	real_t Ma_;

	real_t Pr_;

	real_t Tref_;

	real_t Sutherland_coef_;

	real_t alpha_;

	real_t beta_;

	real_t max_for_visradii_;

	// turbulence model closure coefficients
	real_t eps_, sigma_, k_, Prt_, rlim_, b_;

	real_t cb1_, cb2_;

	real_t cv13_, cv2_, cv3_;

	real_t ct3_, ct4_;

	real_t cw1_, cw2_, cw36_;

	real_t cn1_;

	real_t Pr_ratio_;

	real_t rpi_;

protected:
	TurbSA2DFlux();

	~TurbSA2DFlux() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name);

public:
	inline bool isViscFlux() const { return viscous_on_; };

	inline bool isSource() const { return true; };

	inline real_t getReynolds() const { return Re_; };

	inline real_t getMach() const { return Ma_; };

	inline real_t getPrandtl() const { return Pr_; };

	inline real_t getAlpha() const { return alpha_; };

	inline real_t getBeta() const { return beta_; };

	const std::vector<real_t> calPressure(const std::vector<real_t>& u) const;

	const std::vector<real_t> calTemperature(const std::vector<real_t>& u) const;

	const std::vector<real_t> calViscosity(const std::vector<real_t>& temperature) const;

	const std::vector<real_t> calPressureCoefficients(const std::vector<real_t>& u) const;

	const std::vector<real_t> commonConvFlux(const std::vector<real_t>& u) const;

	const std::vector<real_t> commonConvFluxJacobian(const std::vector<real_t>& u) const; // for implicit

	const std::vector<real_t> commonConvFlux(const std::vector<real_t>& u, const std::vector<real_t>& normal) const;

	const std::vector<real_t> commonConvFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& normal) const; // for implicit
	
	const std::vector<real_t> commonViscFlux(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const;

	const std::vector<real_t> commonViscFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const; // for implicit

	const std::vector<real_t> commonViscFluxGradJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const; // for implicit

	const std::vector<real_t> commonViscFlux(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const;

	const std::vector<real_t> commonViscFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> commonViscFluxGradJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const;

	const std::vector<real_t> numViscFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFluxOwnerGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFluxNeighborGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> convWaveVelocity(const std::vector<real_t>& u) const;

	const std::vector<real_t> viscWaveVelocity(const std::vector<real_t>& u) const;

	const std::vector<real_t> source(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& assist) const;

	virtual const std::vector<real_t> numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const = 0; // for implicit
};


class TurbSA2DLLF : public TurbSA2DFlux
{
private:
	static TurbSA2DLLF TurbSA_2d_LLF_;

protected:
	TurbSA2DLLF();

	~TurbSA2DLLF() {};

public:
	virtual const std::vector<real_t> numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;
};


class TurbSA2DRoe : public TurbSA2DFlux
{
private:
	static TurbSA2DRoe TurbSA_2d_Roe_;

protected:
	TurbSA2DRoe();

	~TurbSA2DRoe() {};

public:
	virtual const std::vector<real_t> numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;
};