#pragma once

#include "TimeUnsteady.h"
#include "LinearSystemSolver.h"

#define VECTOR_ARRAY_CREATOR(arr, ierr) \
	for (int_t i = 0; i < arr.size(); i++) \
		{ ierr = VecCreate(MPI_COMM_WORLD, &arr[i]); }

#define VECTOR_ARRAY_DESTROYER(arr, ierr) \
	for (int_t i = 0; i < arr.size(); i++) \
		{ ierr = VecDestroy(&arr[i]); }

class TimeUnstMEBDF : public TimeUnsteady // for implicit
{
private:
	static TimeUnstMEBDF unst_imp_mebdf_;

protected:
	TimeUnstMEBDF();

	virtual ~TimeUnstMEBDF();

public:
	virtual void initialize();

	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);

	inline int_t getTimeOrder() const { return time_order_; }

	inline void setTimeOrder(const int_t time_order) { time_order_ = time_order; }

protected:
	void InitializeVectors(std::shared_ptr<Zone> zone, const real_t dt);

	inline int_t SolveStage1(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm);

	inline int_t SolveStage2(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm);

	inline int_t SolveStage3(std::shared_ptr<Zone> zone, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, real_t& newton_norm);

	void InitializeCoeff();

	inline void ConvertSolutionToPetsc(const std::vector<std::vector<real_t> >& solution, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, Vec& vector);

	inline void ConvertPetscToSolution(std::shared_ptr<Zone> zone, const Vec& vector, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, std::vector<std::vector<real_t> >& solution);

	inline void UpdateHistoryArray(std::vector<Vec>& history_array, Vec& new_solution);

	void SelfStartingProcedure(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);

protected:
	// Time integration configuration
	int_t time_order_;

	real_t diagonal_factor_;

	std::vector<real_t> BDF_gamma_;

	real_t BDF_beta_;

	std::vector<real_t> MEBDF_gamma_;

	std::array<real_t, 2> MEBDF_beta_;

	Vec delta_;

	Vec intermediate_solution_vector_;

	std::vector<std::vector<real_t> > intermediate_solution_;

	Vec MEBDF_rhs_;

	Vec base_rhs_;

	std::array<Vec, 2> stage_rhs_;

	std::array<Vec, 2> stage_solution_array_;

	std::vector<Vec> solution_history_array_;

	std::vector<Vec> intermediate_history_array_;

	Mat system_matrix_;

	// Non-linear solver configuration
	int_t max_newton_iteration_;

	real_t newton_tolerance_;

	real_t recompute_tol_;

	// Linear solver configuration
	LinearSystemSolver * linear_system_solver_;

	// etc
	bool is_initialized_;
};