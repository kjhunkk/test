
#pragma once

#include "DataType.h"
#include "Equation.h"
#include "SingletonRegistry.h"
#include "TurbSA2DFlux.h"
#include "TurbSA2DBoundary.h"

class TurbSA2DInitializer : public SingletonRegistry<TurbSA2DInitializer>
{
protected:
	const int_t dimension_ = 2;

protected:
	TurbSA2DInitializer() {};

	~TurbSA2DInitializer() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name) { return; };

	void convertPri2Con(std::vector<real_t>& primitive, const int_t start_points, const int_t end_points) const;

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const = 0;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const = 0; // for implicit
};

class TurbSA2DEquation : public Equation
{
private:
	TurbSA2DFlux* flux_;

	TurbSA2DInitializer* initializer_;

	std::vector<TurbSA2DBoundary*> boundaries_;

	static TurbSA2DEquation equation_;

protected:
	TurbSA2DEquation();

	~TurbSA2DEquation() {};

public:
	virtual const std::vector<real_t> calPostVariables(const std::vector<real_t>& solutions) const;

	virtual void setBoundaries(const std::vector<int_t>& tag_numbers);

	virtual inline void setFlux(const std::string& flux_name) { flux_ = TurbSA2DFlux::getInstance().getType(flux_name); };

	virtual inline void setInitializer(const std::string& initializer_name) { initializer_ = TurbSA2DInitializer::getInstance().getType(initializer_name); };

	virtual inline const std::vector<real_t> getInitializerVal(const std::vector<real_t>& coords) { return initializer_->Problem(coords); };

	virtual inline const std::vector<real_t> getExactVal(const std::vector<real_t>& coords, const real_t time) { return initializer_->ExactVal(coords, time); }; // for implicit

	virtual inline const std::vector<real_t> calNumConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) { return flux_->numConvFlux(owner_u, neighbor_u, normal); };

	virtual inline const std::vector<real_t> calNumConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) { return flux_->numConvFluxOwnerJacobian(owner_u, neighbor_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calNumConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) { return flux_->numConvFluxNeighborJacobian(owner_u, neighbor_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calComConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) { return flux_->commonConvFlux(owner_u, normal); };

	virtual inline const std::vector<real_t> calComConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) { return flux_->commonConvFluxJacobian(owner_u, normal); } // for implicit
	
	virtual inline const std::vector<real_t> calNumBdryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundaryConvFlux(owner_u, normal); };

	virtual inline const std::vector<real_t> calNumBdryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundaryConvFluxJacobian(owner_u, normal); } // for implicit
	
	virtual inline const std::vector<real_t> calComConvFlux(const std::vector<real_t>& u) { return flux_->commonConvFlux(u); };

	virtual inline const std::vector<real_t> calComConvFluxJacobian(const std::vector<real_t>& u) { return flux_->commonConvFluxJacobian(u); } // for implicit
	
	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u) { return std::vector<real_t>(0); };

	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u, const std::vector<real_t>& div_u) { return std::vector<real_t>(0); };

	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& assist) { return flux_->source(u, div_u, assist); };

	virtual inline bool isViscFlux() const { return flux_->isViscFlux(); };

	virtual inline bool isSource() const { return flux_->isSource(); };

	virtual inline const std::vector<real_t> calComViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) { return flux_->commonViscFlux(owner_u, owner_div_u, normal); };

	virtual inline const std::vector<real_t> calComViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal)
	{ return flux_->commonViscFluxJacobian(owner_u, owner_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calComViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal)
	{ return flux_->commonViscFluxGradJacobian(owner_u, owner_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) { return flux_->numViscFlux(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); };

	virtual inline const std::vector<real_t> calNumViscFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxOwnerGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxOwnerGradJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxNeighborJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxNeighborGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxNeighborGradJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumBdryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundaryViscFlux(owner_u, owner_div_u, normal); };

	virtual inline const std::vector<real_t> calNumBdryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry)
	{ return boundaries_[ibdry]->calBoundaryViscFluxJacobian(owner_u, owner_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumBdryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry)
	{ return boundaries_[ibdry]->calBoundaryViscFluxGradJacobian(owner_u, owner_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumBdrySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundarySolution(owner_u, owner_div_u, normal); };

	virtual inline const std::vector<real_t> calNumBdrySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundarySolutionJacobian(owner_u, owner_div_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calComViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) { return flux_->commonViscFlux(owner_u, owner_div_u); };

	virtual inline const std::vector<real_t> calComViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u)
	{ return flux_->commonViscFluxJacobian(owner_u, owner_div_u); }; // for implicit

	virtual inline const std::vector<real_t> calComViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u)
	{ return flux_->commonViscFluxGradJacobian(owner_u, owner_div_u); }; // for implicit

	virtual inline const std::vector<real_t> calConvWaveVelocity(const std::vector<real_t>& u) { return flux_->convWaveVelocity(u); };

	virtual inline const std::vector<real_t> calViscWaveVelocity(const std::vector<real_t>& u) { return flux_->viscWaveVelocity(u); };

	virtual bool applyPressureFix(const std::vector<real_t>& u);

	virtual bool isContact(const std::vector<real_t>& u, const std::vector<real_t>& neighbor_u) const;

	virtual inline const std::vector<real_t> calPressureCoefficients(const std::vector<real_t>& u) { return flux_->calPressureCoefficients(u); };
};

class TurbSA2DFreeStream : public TurbSA2DInitializer
{
private:
	static TurbSA2DFreeStream TurbSA_2d_freestream_;

	real_t Ma_;

	real_t AoA_;

protected:
	TurbSA2DFreeStream();

	~TurbSA2DFreeStream() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name);

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};

class TurbSA2DUserDefine : public TurbSA2DInitializer
{
private:
	static TurbSA2DUserDefine TurbSA_2d_user_define_;

protected:
	TurbSA2DUserDefine();

	~TurbSA2DUserDefine() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};