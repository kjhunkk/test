#pragma once

#include <vector>
#include <memory>
#include <cmath>

#include "DataType.h"
#include "Monomial.h"
#include "../LIB/armadillo"

#define ARMA_USE_LAPACK
#define ARMA_USE_BLAS

class Basis;

class BasisFactory
{
public:
	static std::shared_ptr<Basis> getBasis(const int_t dimension, const ElemType elemtype);
};


class Basis
{
protected:
	static const int_t iteration_ = 3;

	static int_t dimension_;

	std::vector<real_t> rotation_;

	int_t order_;

	int_t num_basis_;

	std::vector<real_t> center_;

	std::vector<real_t> coefficients_;

public:
	Basis() : order_(-1), num_basis_(0) {};

	Basis(const int_t dimension) : order_(-1), num_basis_(0) { dimension_ = dimension; };

	~Basis() {};

public:
	inline int_t getDimension(void) const { return dimension_; };

	inline int_t getOrder(void) const { return order_; };

	inline int_t getNumBasis(void) const { return num_basis_; };

	inline const std::vector<real_t>& getCoefficients(void) const { return coefficients_; };

	void calculateBasis(const int_t order, const std::vector<real_t>& center, const std::vector<real_t>& quad_points, const std::vector<real_t>& quad_weights);

	const std::vector<real_t> getBasis(const int_t start_order, const int_t end_order, const std::vector<real_t>& coords) const;

	const std::vector<real_t> getDivBasis(const int_t start_order, const int_t end_order, const std::vector<real_t>& coords) const;

	const std::vector<real_t> calRotation(const std::vector<real_t> coords) const;

	void setRotation(const std::vector<real_t> coords, ElemType elemtype);

	virtual inline ElemType getElemType() const = 0;

	virtual const std::vector<real_t> calMonomial(const int_t order, const real_t* coord) const = 0;

	virtual const std::vector<real_t> calDiffMonomial(const int_t order, const real_t* coord) const = 0;

	virtual int_t calNumBasis(const int_t order) const = 0;
};

class BasisLine : public Basis
{
private:
	static const ElemType elemtype_ = ElemType::LINE;

public:
	BasisLine() :Basis(1) {};

	~BasisLine() {};

public:
	virtual inline ElemType getElemType() const { return elemtype_; };

	virtual const std::vector<real_t> calMonomial(const int_t order, const real_t* coord) const;

	virtual const std::vector<real_t> calDiffMonomial(const int_t order, const real_t* coord) const;

	virtual int_t calNumBasis(const int_t order) const;
};

class BasisTris : public Basis
{
private:
	static const ElemType elemtype_ = ElemType::TRIS;

public:
	BasisTris() :Basis(2) {};

	~BasisTris() {};

public:
	virtual inline ElemType getElemType() const { return elemtype_; };

	virtual const std::vector<real_t> calMonomial(const int_t order, const real_t* coord) const;

	virtual const std::vector<real_t> calDiffMonomial(const int_t order, const real_t* coord) const;

	virtual int_t calNumBasis(const int_t order) const;
};

class BasisQuad : public Basis
{
private:
	static const ElemType elemtype_ = ElemType::QUAD;

public:
	BasisQuad() :Basis(2) {};

	~BasisQuad() {};

public:
	virtual inline ElemType getElemType() const { return elemtype_; };

	virtual const std::vector<real_t> calMonomial(const int_t order, const real_t* coord) const;

	virtual const std::vector<real_t> calDiffMonomial(const int_t order, const real_t* coord) const;

	virtual int_t calNumBasis(const int_t order) const;
};

class BasisTets : public Basis
{
private:
	static const ElemType elemtype_ = ElemType::TETS;

public:
	BasisTets() :Basis(3) {};

	~BasisTets() {};

public:
	virtual inline ElemType getElemType() const { return elemtype_; };

	virtual const std::vector<real_t> calMonomial(const int_t order, const real_t* coord) const;

	virtual const std::vector<real_t> calDiffMonomial(const int_t order, const real_t* coord) const;

	virtual int_t calNumBasis(const int_t order) const;
};

class BasisHexa : public Basis
{
private:
	static const ElemType elemtype_ = ElemType::HEXA;

public:
	BasisHexa() :Basis(3) {};

	~BasisHexa() {};

public:
	virtual inline ElemType getElemType() const { return elemtype_; };

	virtual const std::vector<real_t> calMonomial(const int_t order, const real_t* coord) const;

	virtual const std::vector<real_t> calDiffMonomial(const int_t order, const real_t* coord) const;

	virtual int_t calNumBasis(const int_t order) const;
};

class BasisPris : public Basis
{
private:
	static const ElemType elemtype_ = ElemType::PRIS;

public:
	BasisPris() :Basis(3) {};

	~BasisPris() {};

public:
	virtual inline ElemType getElemType() const { return elemtype_; };

	virtual const std::vector<real_t> calMonomial(const int_t order, const real_t* coord) const;

	virtual const std::vector<real_t> calDiffMonomial(const int_t order, const real_t* coord) const;

	virtual int_t calNumBasis(const int_t order) const;
};

class BasisPyra : public Basis
{
private:
	static const ElemType elemtype_ = ElemType::PYRA;

public:
	BasisPyra() :Basis(3) {};

	~BasisPyra() {};

public:
	virtual inline ElemType getElemType() const { return elemtype_; };

	virtual const std::vector<real_t> calMonomial(const int_t order, const real_t* coord) const;

	virtual const std::vector<real_t> calDiffMonomial(const int_t order, const real_t* coord) const;

	virtual int_t calNumBasis(const int_t order) const;
};