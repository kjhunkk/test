#pragma once

#include <string>
#include <vector>
#include <memory>

#include "MPIHeader.h"
#include "DataType.h"
#include "GridBuilder.h"
#include "Config.h"
#include "Basis.h"
#include "Quadrature.h"

class Grid
{
private:
	int_t dimension_;
	int_t order_;
	int_t num_basis_;

	int_t num_global_nodes_;
	int_t num_total_nodes_;
	int_t num_nodes_;
	// std::vector<int_t> node_dist_;
	std::vector<real_t> node_coords_;
	//std::vector<std::vector<int_t>> peribdry_node_;
	//std::vector<std::vector<int_t>> peribdry_node_to_matching_;
	std::vector<int_t> node_global_index_;
	std::vector<std::vector<int_t>> node_to_cells_;
	std::vector<std::vector<int_t>> node_to_vertex_;

	int_t num_global_cells_;
	int_t num_total_cells_;
	int_t num_cells_;
	// std::vector<int_t> cell_dist_;
	std::vector<int_t> cell_to_node_ptr_;
	std::vector<int_t> cell_to_node_ind_;
	std::vector<int_t> cell_elem_type_;
	std::vector<int_t> cell_order_;
	std::vector<int_t> cell_to_subnode_ptr_;
	std::vector<int_t> cell_to_subnode_ind_;
	// std::vector<int_t> cell_color_;
	std::vector<int_t> cell_global_index_;
	std::vector<std::vector<int_t>> cell_to_cells_;
	std::vector<std::vector<int_t>> cell_to_faces_;

	std::vector<Element*> cell_element_;
	std::vector<int_t> num_cell_quad_points_;
	std::vector<real_t> cell_volumes_;
	std::vector<real_t> cell_proj_volumes_;
	std::vector<std::vector<real_t>> cell_quad_points_;
	std::vector<std::vector<real_t>> cell_quad_weights_;
	std::vector<std::shared_ptr<Basis>> cell_basis_;
	std::vector<std::vector<real_t>> cell_basis_value_;
	std::vector<std::vector<real_t>> cell_vertex_basis_value_;
	std::vector<std::vector<real_t>> cell_div_basis_value_;
	std::vector<std::vector<real_t>> cell_coefficients_;
	std::vector<std::vector<real_t>> cell_source_coefficients_;
	std::vector<std::vector<real_t>> cell_owner_to_owner_auxiliary_jacobian_; // for implicit (icell, ipoint, ibasis, idim)
	std::vector<std::vector<real_t>> cell_owner_to_neighbor_auxiliary_jacobian_; // for implicit (icell, jcell, ipoint, ibasis, idim)

	std::vector<std::vector<real_t>> simplex_average_coefficients_;
	std::vector<std::vector<real_t>> simplex_p1_coefficients_;
	std::vector<std::vector<real_t>> simplex_volume_;

	std::vector<std::vector<real_t>> wall_distance_;

	int_t num_total_faces_;
	int_t num_faces_;
	//std::vector<int_t> face_to_node_ptr_;
	//std::vector<int_t> face_to_node_ind_;
	std::vector<int_t> face_owner_cell_;
	std::vector<int_t> face_neighbor_cell_;
	std::vector<int_t> face_owner_type_;
	std::vector<int_t> face_neighbor_type_;

	std::vector<int_t> num_face_quad_points_;
	std::vector<std::vector<real_t>> face_owner_basis_value_;
	std::vector<std::vector<real_t>> face_neighbor_basis_value_;
	std::vector<std::vector<real_t>> face_owner_div_basis_value_;
	std::vector<std::vector<real_t>> face_neighbor_div_basis_value_;
	std::vector<std::vector<real_t>> face_normals_;
	std::vector<std::vector<real_t>> face_owner_coefficients_;
	std::vector<std::vector<real_t>> face_neighbor_coefficients_;
	std::vector<std::vector<real_t>> face_owner_auxiliary_coefficients_;
	std::vector<std::vector<real_t>> face_neighbor_auxiliary_coefficients_;
	std::vector<std::vector<real_t>> face_owner_to_owner_auxiliary_jacobian_; // for implicit (iface, ibasis, idim)
	std::vector<std::vector<real_t>> face_owner_to_neighbor_auxiliary_jacobian_; // for implicit (iface, ibasis, idim)
	std::vector<std::vector<real_t>> face_neighbor_to_owner_auxiliary_jacobian_; // for implicit (iface, ibasis, idim)
	std::vector<std::vector<real_t>> face_neighbor_to_neighbor_auxiliary_jacobian_; // for implicit (iface, ibasis, idim)

	std::vector<int_t> num_face_quad_points_hMLPBD_;
	std::vector<real_t> face_areas_;
	std::vector<real_t> face_characteristic_length_;
	std::vector<std::vector<real_t>> face_owner_basis_value_hMLPBD_;
	std::vector<std::vector<real_t>> face_neighbor_basis_value_hMLPBD_;
	std::vector<std::vector<real_t>> face_weights_hMLPBD_;

	int_t num_global_bdries_;
	int_t num_total_bdries_;
	int_t num_bdries_;
	//std::vector<int_t> bdry_dist_;
	//std::vector<int_t> bdry_to_node_ptr_;
	//std::vector<int_t> bdry_to_node_ind_;
	std::vector<int_t> bdry_type_;
	//std::vector<int_t> bdry_color_;
	std::vector<int_t> bdry_owner_cell_;
	std::vector<int_t> bdry_owner_type_;

	std::vector<int_t> num_bdry_quad_points_;
	std::vector<std::vector<real_t>> bdry_owner_basis_value_;
	std::vector<std::vector<real_t>> bdry_owner_div_basis_value_;
	std::vector<std::vector<real_t>> bdry_normals_;
	std::vector<std::vector<real_t>> bdry_owner_coefficients_;
	std::vector<std::vector<real_t>> bdry_owner_auxiliary_coefficients_;
	std::vector<std::vector<real_t>> bdry_auxiliary_jacobian_coefficients_; // for implicit (ibdry, ipoint, ibasis, idim, jpoint)
	std::vector<std::vector<real_t>> bdry_cell_auxiliary_jacobian_coefficients_; // for implicit (ibdry, ipoint, ibasis, idim, jpoint)

	int_t num_global_peribdries_;
	int_t num_total_peribdries_;
	int_t num_peribdries_;
	//std::vector<int_t> peribdry_dist_;
	//std::vector<int_t> peribdry_to_node_ptr_;
	//std::vector<int_t> peribdry_to_node_ind_;
	//std::vector<int_t> peribdry_direction_;
	//std::vector<int_t> peribdry_color_;
	std::vector<int_t> peribdry_owner_cell_;
	std::vector<int_t> peribdry_neighbor_cell_;
	//std::vector<int_t> peribdry_to_matching_node_ind_;
	std::vector<int_t> peribdry_owner_type_;
	std::vector<int_t> peribdry_neighbor_type_;

	std::vector<int_t> num_peribdry_quad_points_;
	std::vector<std::vector<real_t>> peribdry_owner_basis_value_;
	std::vector<std::vector<real_t>> peribdry_neighbor_basis_value_;
	std::vector<std::vector<real_t>> peribdry_owner_div_basis_value_;
	std::vector<std::vector<real_t>> peribdry_neighbor_div_basis_value_;
	std::vector<std::vector<real_t>> peribdry_normals_;
	std::vector<std::vector<real_t>> peribdry_owner_coefficients_;
	std::vector<std::vector<real_t>> peribdry_neighbor_coefficients_;
	std::vector<std::vector<real_t>> peribdry_owner_auxiliary_coefficients_;
	std::vector<std::vector<real_t>> peribdry_neighbor_auxiliary_coefficients_;
	std::vector<std::vector<real_t>> peribdry_owner_to_owner_auxiliary_jacobian_; // for implicit (ibdry, ibasis, idim)
	std::vector<std::vector<real_t>> peribdry_owner_to_neighbor_auxiliary_jacobian_; // for implicit (ibdry, ibasis, idim)
	std::vector<std::vector<real_t>> peribdry_neighbor_to_owner_auxiliary_jacobian_; // for implicit (ibdry, ibasis, idim)
	std::vector<std::vector<real_t>> peribdry_neighbor_to_neighbor_auxiliary_jacobian_; // for implicit (ibdry, ibasis, idim)

	std::vector<int_t> num_peribdry_quad_points_hMLPBD_;
	std::vector<real_t> peribdry_areas_;
	std::vector<real_t> peribdry_characteristic_length_;
	std::vector<std::vector<real_t>> peribdry_owner_basis_value_hMLPBD_;
	std::vector<std::vector<real_t>> peribdry_neighbor_basis_value_hMLPBD_;
	std::vector<std::vector<real_t>> peribdry_weights_hMLPBD_;

	std::vector<int_t> num_send_cells_;
	std::vector<int_t> num_recv_cells_;
	std::vector<std::vector<int_t>> send_cell_list_;
	std::vector<std::vector<int_t>> recv_cell_list_;

	std::vector<int_t> cell_petsc_index_; // for implicit
	std::vector<int_t> domain_label_; // for implicit

public:
	Grid() {};

	~Grid() {};

private:
	inline real_t calDist(const real_t& x1, const real_t& y1, const real_t& x2, const real_t& y2) const
	{
		return (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2);
	};

	inline real_t calDist(const real_t* x, const real_t* y, const int_t dimension) const
	{
		if(dimension == 2)
			return (x[0] - y[0])*(x[0] - y[0]) + (x[1] - y[1])*(x[1] - y[1]);
		else
			return (x[0] - y[0])*(x[0] - y[0]) + (x[1] - y[1])*(x[1] - y[1]) + (x[2] - y[2])*(x[2] - y[2]);
	};

	inline real_t calDist(const real_t& x1, const real_t& y1, const real_t& z1, const real_t& x2, const real_t& y2, const real_t& z2) const
	{
		return (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) + (z1 - z2)*(z1 - z2);
	};

	real_t calMinimumDist(std::shared_ptr<Jacobian> wbdry_jacobian, const real_t* x, const int_t dimension);

public:
	void initialize();

	void prpcForHmlpBdSim();

	void calWallDistance();

	real_t calProjVolume(const std::vector<real_t>& coords, const int idim) const;

	int_t normalOrientation(const std::vector<real_t>& normal, const int_t* cell_to_node_ind, const std::vector<int_t>& face_to_node_ind) const;

	inline const std::vector<real_t> calBasisValue(const int_t icell, std::vector<real_t> coords) const { return cell_basis_[icell]->getBasis(0, order_, coords); };

	inline int_t getNumBasis() const { return num_basis_; };

	inline int_t getNumNodes() const { return num_nodes_; }

	inline int_t getNumTotalNodes() const { return num_total_nodes_; };

	inline int_t getNumCells() const { return num_cells_; };

	inline int_t getNumTotalCells() const { return num_total_cells_; };

	inline int_t getNumGlobalCells() const { return num_global_cells_; };

	inline int_t getNumFaces() const { return num_faces_; };

	inline int_t getNumBdries() const { return num_bdries_; };

	inline int_t getNumPeriBdries() const { return num_peribdries_; };

	inline const std::vector<Element*>& getCellElement() const { return cell_element_; };

	inline const std::vector<int_t>& getNumSendCells() const { return num_send_cells_; };

	inline const std::vector<int_t>& getNumRecvCells() const { return num_recv_cells_; };

	inline const std::vector<int_t>& getBdryType() const { return bdry_type_; };

	inline const std::vector<int_t>& getBdryOwnerType() const { return bdry_owner_type_; };

	inline const std::vector<int_t>& getCellGlobalIndex() const { return cell_global_index_; };

	inline const std::vector<int_t>& getNumCellQuadPoints() const { return num_cell_quad_points_; };

	inline const std::vector<int_t>& getCellElemType() const { return cell_elem_type_; };

	inline const std::vector<int_t>& getCellElemOrder() const { return cell_order_; };

	inline const std::vector<int_t>& getCellToNodePtr() const { return cell_to_node_ptr_; };

	inline const std::vector<int_t>& getCellToNodeInd() const { return cell_to_node_ind_; };

	inline const std::vector<int_t>& getCellToSubnodePtr() const { return cell_to_subnode_ptr_; };

	inline const std::vector<int_t>& getCellToSubnodeInd() const { return cell_to_subnode_ind_; };

	inline const std::vector<int_t>& getNumFaceQuadPoints() const { return num_face_quad_points_; };

	inline const std::vector<int_t>& getNumFaceQuadPointsHMLPBD() const { return num_face_quad_points_hMLPBD_; }; // for hMLP_BD

	inline const std::vector<int_t>& getFaceOwnerCell() const { return face_owner_cell_; };

	inline const std::vector<int_t>& getFaceNeighborCell() const { return face_neighbor_cell_; };

	inline const std::vector<int_t>& getNumPeriBdryQuadPoints() const { return num_peribdry_quad_points_; };

	inline const std::vector<int_t>& getNumPeriBdryQuadPointsHMLPBD() const { return num_peribdry_quad_points_hMLPBD_; }; // for hMLP_BD

	inline const std::vector<int_t>& getPeriBdryOwnerCell() const { return peribdry_owner_cell_; };

	inline const std::vector<int_t>& getPeriBdryNeighborCell() const { return peribdry_neighbor_cell_; };

	inline const std::vector<int_t>& getNumBdryQuadPoints() const { return num_bdry_quad_points_; };

	inline const std::vector<int_t>& getBdryOwnerCell() const { return bdry_owner_cell_; };

	inline const std::vector<real_t>& getNodeCoords() const { return node_coords_; };

	inline const std::vector<real_t>& getCellVolumes() const { return cell_volumes_; };

	inline const std::vector<real_t>& getCellProjVolumes() const { return cell_proj_volumes_; };

	inline const std::vector<real_t>& getFaceAreas() const { return face_areas_; }; // for hMLP_BD

	inline const std::vector<real_t>& getFaceCharacteristicLength() const { return face_characteristic_length_; }; // for hMLP_BD

	inline const std::vector<real_t>& getPeriBdryAreas() const { return peribdry_areas_; }; // for hMLP_BD

	inline const std::vector<real_t>& getPeriBdryCharacteristicLength() const { return peribdry_characteristic_length_; }; // for hMLP_BD

	inline const std::vector<std::vector<int_t>>& getSendCellList() const { return send_cell_list_; };

	inline const std::vector<std::vector<int_t>>& getRecvCellList() const { return recv_cell_list_; };

	inline const std::vector<std::vector<int_t>>& getNodeToCells() const { return node_to_cells_; };

	inline const std::vector<std::vector<int_t>>& getNodeToVertex() const { return node_to_vertex_; }; // for hMLP_BD_SIM

	inline const std::vector<std::vector<int_t>>& getCellToCells() const { return cell_to_cells_; };

	inline const std::vector<std::vector<int_t>>& getCellToFaces() const { return cell_to_faces_; };

	inline const std::vector<std::vector<real_t>>& getCellQuadPoints() const { return cell_quad_points_; };

	inline const std::vector<std::vector<real_t>>& getCellQuadWeights() const { return cell_quad_weights_; };

	inline const std::vector<std::vector<real_t>>& getCellBasisValue() const { return cell_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getCellDivBasisValue() const { return cell_div_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getCellCoefficients() const { return cell_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getCellSourceCoefficients() const { return cell_source_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getSimplexAverageCoefficients() const { return simplex_average_coefficients_; }; // for hMLP_BD_SIM

	inline const std::vector<std::vector<real_t>>& getSimplexP1Coefficients() const { return simplex_p1_coefficients_; }; // for hMLP_BD_SIM

	inline const std::vector<std::vector<real_t>>& getSimplexVolumes() const { return simplex_volume_; };

	inline const std::vector<std::vector<real_t>>& getWallDistances() const { return wall_distance_; };

	inline const std::vector<std::vector<real_t>>& getFaceOwnerBasisValue() const { return face_owner_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getFaceNeighborBasisValue() const { return face_neighbor_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getFaceOwnerBasisValueHMLPBD() const { return face_owner_basis_value_hMLPBD_; }; // for hMLP_BD

	inline const std::vector<std::vector<real_t>>& getFaceNeighborBasisValueHMLPBD() const { return face_neighbor_basis_value_hMLPBD_; }; // for hMLP_BD

	inline const std::vector<std::vector<real_t>>& getFaceOwnerDivBasisValue() const { return face_owner_div_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getFaceNeighborDivBasisValue() const { return face_neighbor_div_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getFaceNormals() const { return face_normals_; };

	inline const std::vector<std::vector<real_t>>& getFaceOwnerCoefficients() const { return face_owner_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getFaceNeighborCoefficients() const { return face_neighbor_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getFaceWeightsHMLPBD() const { return face_weights_hMLPBD_; }; // for hMLP_BD

	inline const std::vector<std::vector<real_t>>& getFaceOwnerAuxiliaryCoefficients() const { return face_owner_auxiliary_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getFaceNeighborAuxiliaryCoefficients() const { return face_neighbor_auxiliary_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getFaceOwnerToOwnerAuxiliaryJacobian() const { return face_owner_to_owner_auxiliary_jacobian_; } // for implicit

	inline const std::vector<std::vector<real_t>>& getFaceOwnerToNeighborAuxiliaryJacobian() const { return face_owner_to_neighbor_auxiliary_jacobian_; } // for implicit

	inline const std::vector<std::vector<real_t>>& getFaceNeighborToOwnerAuxiliaryJacobian() const { return face_neighbor_to_owner_auxiliary_jacobian_; } // for implicit

	inline const std::vector<std::vector<real_t>>& getFaceNeighborToNeighborAuxiliaryJacobian() const { return face_neighbor_to_neighbor_auxiliary_jacobian_; } // for implicit

	inline const std::vector<std::vector<real_t>>& getPeriBdryOwnerBasisValue() const { return peribdry_owner_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getPeriBdryNeighborBasisValue() const { return peribdry_neighbor_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getPeriBdryOwnerBasisValueHMLPBD() const { return peribdry_owner_basis_value_hMLPBD_; }; // for hMLP_BD

	inline const std::vector<std::vector<real_t>>& getPeriBdryNeighborBasisValueHMLPBD() const { return peribdry_neighbor_basis_value_hMLPBD_; }; // for hMLP_BD

	inline const std::vector<std::vector<real_t>>& getPeriBdryOwnerDivBasisValue() const { return peribdry_owner_div_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getPeriBdryNeighborDivBasisValue() const { return peribdry_neighbor_div_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getPeriBdryNormals() const { return peribdry_normals_; };

	inline const std::vector<std::vector<real_t>>& getPeriBdryOwnerCoefficients() const { return peribdry_owner_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getPeriBdryNeighborCoefficients() const { return peribdry_neighbor_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getPeriBdryWeightsHMLPBD() const { return peribdry_weights_hMLPBD_; }; // for hMLP_BD

	inline const std::vector<std::vector<real_t>>& getPeriBdryOwnerAuxiliaryCoefficients() const { return peribdry_owner_auxiliary_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getPeriBdryNeighborAuxiliaryCoefficients() const { return peribdry_neighbor_auxiliary_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getPeriBdryOwnerToOwnerAuxiliaryJacobian() const { return peribdry_owner_to_owner_auxiliary_jacobian_; }; // for implicit

	inline const std::vector<std::vector<real_t>>& getPeriBdryOwnerToNeighborAuxiliaryJacobian() const { return peribdry_owner_to_neighbor_auxiliary_jacobian_; }; // for implicit

	inline const std::vector<std::vector<real_t>>& getPeriBdryNeighborToOwnerAuxiliaryJacobian() const { return peribdry_neighbor_to_owner_auxiliary_jacobian_; }; // for implicit

	inline const std::vector<std::vector<real_t>>& getPeriBdryNeighborToNeighborAuxiliaryJacobian() const { return peribdry_neighbor_to_neighbor_auxiliary_jacobian_; }; // for implicit

	inline const std::vector<std::vector<real_t>>& getBdryOwnerBasisValue() const { return bdry_owner_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getBdryOwnerDivBasisValue() const { return bdry_owner_div_basis_value_; };

	inline const std::vector<std::vector<real_t>>& getBdryNormals() const { return bdry_normals_; };

	inline const std::vector<std::vector<real_t>>& getBdryOwnerCoefficients() const { return bdry_owner_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getBdryOwnerAuxiliaryCoefficients() const { return bdry_owner_auxiliary_coefficients_; };

	inline const std::vector<std::vector<real_t>>& getBdryAuxiliaryJacobianCoefficients() const { return bdry_auxiliary_jacobian_coefficients_; }; // for implicit

	inline const std::vector<std::vector<real_t>>& getBdryCellAuxiliaryJacobianCoefficients() const { return bdry_cell_auxiliary_jacobian_coefficients_; }; // for implicit

	inline const std::vector<std::vector<real_t>>& getCellOwnerToOwnerAuxiliaryJacobian() const { return cell_owner_to_owner_auxiliary_jacobian_; }; // for implicit

	inline const std::vector<std::vector<real_t>>& getCellOwnerToNeighborAuxiliaryJacobian() const { return cell_owner_to_neighbor_auxiliary_jacobian_; }; // for implicit

	inline std::vector<std::vector<real_t>>* getCellVertexBasisValue() { return &cell_vertex_basis_value_; };

	inline const std::vector<int_t>& getCellPetscIndex() { return cell_petsc_index_; } // for implicit

	inline const std::vector<int_t>& getDomainLabel() { return domain_label_; } // for implicit
};