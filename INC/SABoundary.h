
#pragma once

#include <vector>
#include <memory>
#include <algorithm>
#include <string>

#include "DataType.h"
#include "MPIHeader.h"
#include "Boundary.h"
#include "SAFlux.h"

class SABoundary : public Boundary
{
protected:
	SAFlux* flux_;

public:
	SABoundary() {};

	~SABoundary() {};

public:
	void setFlux(SAFlux* flux) { flux_ = flux; };

	virtual SABoundary* clone() = 0;

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const = 0;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const = 0; // for implicit
};

class SABCExtrapolate : public SABoundary
{
private:
	static SABCExtrapolate SA_extrapolate_;

public:
	SABCExtrapolate() { type_ = TO_UPPER("Extrapolate"); BoundaryFactory<SABoundary>::getInstance().addClone(this); };

	~SABCExtrapolate() {};

public:
	virtual SABoundary* clone() { return new SABCExtrapolate; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const; // for implicit
};

class SABCConstant : public SABoundary
{
private:
	static SABCConstant SA_constant_;

public:
	SABCConstant() { type_ = TO_UPPER("Constant"); BoundaryFactory<SABoundary>::getInstance().addClone(this); };

	~SABCConstant() {};

public:
	virtual SABoundary* clone() { return new SABCConstant; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) const; // for implicit
};