#pragma once

#include "TimeUnsteady.h"
#include "LinearSystemSolver.h"

class TimeUnstImpBDF2 : public TimeUnsteady // for implicit
{
private:
	static TimeUnstImpBDF2 ustd_imp_bdf2_;

protected:
	TimeUnstImpBDF2();

	virtual ~TimeUnstImpBDF2();

public:
	virtual void initialize();

	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);

	const std::array<real_t, 2>& getResidualNorms() const { return residual_norms_; }

protected:
	void computeUnsteadySource(std::shared_ptr<const Zone> zone, const real_t coeff1, const std::vector<std::vector<real_t> >& solution, const real_t coeff2, const std::vector<std::vector<real_t> >& old_solution);

	void computeUnsteadyResidual(std::shared_ptr<const Zone> zone, const real_t coeff, const std::vector<std::vector<real_t> >& solution, const Vec& rhs);
	
	void computeError();

	void UpdatePseudoStatus();

	void UpdatePseudoCFL(); // fix

	std::vector<std::vector<real_t> > PetscToVector(const Vec& petscVec, const int_t num_cells, const int_t num_var, const std::vector<int_t>& cell_petsc_index); // for implicit debug

protected:
	LinearSystemSolver* linear_system_solver_;

	Vec delta_;

	Vec unsteady_source_;

	Vec unsteady_residual_;

	PetscErrorCode ierr_;

	PetscInt linear_solver_iteration_;

	std::array<real_t, 2> residual_norms_;

	bool is_initialized_;

	bool pseudo_exit_flag_;

	int_t pseudo_iteration_;

	int_t pseudo_total_iteration_;

	int_t pseudo_max_iteration_;

	real_t pseudo_target_residual_;

	real_t pseudo_dt_;

	real_t init_pseudo_CFL_;

	real_t pseudo_CFL_;

	real_t physical_dt_;

	real_t reference_error_;

	std::vector<std::vector<real_t> > old_solution_;
};