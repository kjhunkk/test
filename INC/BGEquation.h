
#pragma once

#include "DataType.h"
#include "Equation.h"
#include "SingletonRegistry.h"
#include "BGFlux.h"
#include "BGBoundary.h"

class BGInitializer : public SingletonRegistry<BGInitializer>
{
protected:
	int_t dimension_;

protected:
	BGInitializer() {};

	~BGInitializer() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name) { return; };

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const = 0;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const = 0; // for implicit
};

class BGEquation : public Equation
{
private:
	BGFlux* flux_;

	BGInitializer* initializer_;

	std::vector<BGBoundary*> boundaries_;

	static BGEquation equation_;

protected:
	BGEquation();

	~BGEquation() {};

public:
	virtual const std::vector<real_t> calPostVariables(const std::vector<real_t>& solutions) const { return solutions; };

	virtual void setBoundaries(const std::vector<int_t>& tag_numbers);

	virtual inline void setFlux(const std::string& flux_name) { flux_ = BGFlux::getInstance().getType(flux_name); };

	virtual inline void setInitializer(const std::string& initializer_name) { initializer_ = BGInitializer::getInstance().getType(initializer_name); };

	virtual inline const std::vector<real_t> getInitializerVal(const std::vector<real_t>& coords) { return initializer_->Problem(coords); };

	virtual inline const std::vector<real_t> getExactVal(const std::vector<real_t>& coords, const real_t time) { return initializer_->ExactVal(coords, time); }; // for implicit

	virtual inline const std::vector<real_t> calNumConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) { return flux_->numConvFlux(owner_u, neighbor_u, normal); };

	virtual inline const std::vector<real_t> calNumConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) { return flux_->numConvFluxOwnerJacobian(owner_u, neighbor_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calNumConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) { return flux_->numConvFluxNeighborJacobian(owner_u, neighbor_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calComConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) { return flux_->commonConvFlux(owner_u, normal); };

	virtual inline const std::vector<real_t> calComConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) { return flux_->commonConvFluxJacobian(owner_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calNumBdryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundaryConvFlux(owner_u, normal); };

	virtual inline const std::vector<real_t> calNumBdryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundaryConvFluxJacobian(owner_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calComConvFlux(const std::vector<real_t>& u) { return flux_->commonConvFlux(u); };

	virtual inline const std::vector<real_t> calComConvFluxJacobian(const std::vector<real_t>& u) { return flux_->commonConvFluxJacobian(u); } // for implicit

	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u) { return flux_->source(u); };

	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u, const std::vector<real_t>& div_u) { return flux_->source(u); };

	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& assist) { return flux_->source(u); };

	virtual inline bool isViscFlux() const { return flux_->isViscFlux(); };

	virtual inline bool isSource() const { return flux_->isSource(); };

	virtual inline const std::vector<real_t> calComViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) { return flux_->commonViscFlux(owner_u, owner_div_u, normal); };

	virtual inline const std::vector<real_t> calComViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal)
	{ return flux_->commonViscFluxJacobian(owner_u, owner_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calComViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal)
	{ return flux_->commonViscFluxGradJacobian(owner_u, owner_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) { return flux_->numViscFlux(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); };

	virtual inline const std::vector<real_t> calNumViscFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxOwnerGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxOwnerGradJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal);	}; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxNeighborJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxNeighborGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxNeighborGradJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumBdryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundaryViscFlux(owner_u, owner_div_u, normal); };

	virtual inline const std::vector<real_t> calNumBdryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry)
	{ return boundaries_[ibdry]->calBoundaryViscFluxJacobian(owner_u, owner_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumBdryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry)
	{ return boundaries_[ibdry]->calBoundaryViscFluxGradJacobian(owner_u, owner_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumBdrySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundarySolution(owner_u, owner_div_u); };

	virtual inline const std::vector<real_t> calNumBdrySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundarySolutionJacobian(owner_u, owner_div_u); } // for implicit

	virtual inline const std::vector<real_t> calComViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) { return flux_->commonViscFlux(owner_u, owner_div_u); };

	virtual inline const std::vector<real_t> calComViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u)
	{ return flux_->commonViscFluxJacobian(owner_u, owner_div_u); }; // for implicit

	virtual inline const std::vector<real_t> calComViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u)
	{ return flux_->commonViscFluxGradJacobian(owner_u, owner_div_u); }; // for implicit

	virtual inline const std::vector<real_t> calConvWaveVelocity(const std::vector<real_t>& u) { return flux_->convWaveVelocity(u); };

	virtual inline const std::vector<real_t> calViscWaveVelocity(const std::vector<real_t>& u) { return flux_->viscWaveVelocity(u); };

	virtual bool applyPressureFix(const std::vector<real_t>& u) { return false; };

	virtual bool isContact(const std::vector<real_t>& u, const std::vector<real_t>& neighbor_u) const { return false; };

	virtual inline const std::vector<real_t> calPressureCoefficients(const std::vector<real_t>& u) { return std::vector<real_t>(u.size(), 0.0); };
};

class BGConstant : public BGInitializer
{
private:
	static BGConstant BG_Constant_;

protected:
	BGConstant();

	~BGConstant() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name);

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};

class BGDoubleSine : public BGInitializer
{
private:
	static BGDoubleSine BG_double_sine_;

	std::vector<real_t> sine_wavelength_;

protected:
	BGDoubleSine();

	~BGDoubleSine() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name);

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};

class BGUserDefine : public BGInitializer
{
private:
	static BGUserDefine BG_UserDefine_;

protected:
	BGUserDefine();

	~BGUserDefine() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name);

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};
