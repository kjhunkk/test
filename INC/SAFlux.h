
#pragma once

#include <string>
#include <vector>
#include <cmath>
#include <fstream>

#include "SingletonRegistry.h"
#include "../LIB/armadillo"

#define ARMA_USE_LAPACK
#define ARMA_USE_BLAS

class SAFlux : public SingletonRegistry<SAFlux>
{
protected:
	int_t dimension_;

	bool diffusion_on_;

	std::vector<real_t> advection_;

	std::vector<real_t> diffusion_;

	std::vector<real_t> diffusion_wave_;

protected:
	SAFlux();

	~SAFlux() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name);

public:
	inline bool isViscFlux() const { return diffusion_on_; };

	inline bool isSource() const { return false; };

	const std::vector<real_t> commonConvFlux(const std::vector<real_t>& u) const;

	const std::vector<real_t> commonConvFluxJacobian(const std::vector<real_t>& u) const; // for implicit

	const std::vector<real_t> commonConvFlux(const std::vector<real_t>& u, const std::vector<real_t>& normal) const;

	const std::vector<real_t> commonConvFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> commonViscFlux(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const;

	const std::vector<real_t> commonViscFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const; // for implicit

	const std::vector<real_t> commonViscFluxGradJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u) const; // for implicit

	const std::vector<real_t> commonViscFlux(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const;

	const std::vector<real_t> commonViscFluxJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> commonViscFluxGradJacobian(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const;

	const std::vector<real_t> numViscFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFluxOwnerGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> numViscFluxNeighborGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) const; // for implicit

	const std::vector<real_t> convWaveVelocity(const std::vector<real_t>& u) const;

	const std::vector<real_t> viscWaveVelocity(const std::vector<real_t>& u) const;

	const std::vector<real_t> source(const std::vector<real_t>& u) const;

	virtual const std::vector<real_t> numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const = 0; // for implicit
};


class SAUpwind : public SAFlux
{
private:
	static SAUpwind SA_upwind_;

protected:
	SAUpwind();

	~SAUpwind() {};

public:
	virtual const std::vector<real_t> numConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> numConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) const;
};