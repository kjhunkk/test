#pragma once

#include "TimeScheme.h"

class TimeSteady : public TimeScheme
{
protected:
	TimeSteady() {}

	virtual ~TimeSteady() {}

public:
	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& integration) = 0;

	// convergence acceleration
};