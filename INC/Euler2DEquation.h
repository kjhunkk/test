
#pragma once

#include "DataType.h"
#include "Equation.h"
#include "SingletonRegistry.h"
#include "Euler2DFlux.h"
#include "Euler2DBoundary.h"

class Euler2DInitializer : public SingletonRegistry<Euler2DInitializer>
{
protected:
	const int_t dimension_ = 2;

protected:
	Euler2DInitializer() {};

	~Euler2DInitializer() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name) { return; };

	void convertPri2Con(std::vector<real_t>& primitive, const int_t start_points, const int_t end_points) const;

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const = 0;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const = 0; // for implicit
};

class Euler2DEquation : public Equation
{
private:
	Euler2DFlux* flux_;

	Euler2DInitializer* initializer_;

	std::vector<Euler2DBoundary*> boundaries_;

	static Euler2DEquation equation_;

protected:
	Euler2DEquation();

	~Euler2DEquation() {};

public:
	virtual const std::vector<real_t> calPostVariables(const std::vector<real_t>& solutions) const;

	virtual void setBoundaries(const std::vector<int_t>& tag_numbers);

	virtual inline void setFlux(const std::string& flux_name) { flux_ = Euler2DFlux::getInstance().getType(flux_name); };

	virtual inline void setInitializer(const std::string& initializer_name) { initializer_ = Euler2DInitializer::getInstance().getType(initializer_name); };

	virtual inline const std::vector<real_t> getInitializerVal(const std::vector<real_t>& coords) { return initializer_->Problem(coords); };

	virtual inline const std::vector<real_t> getExactVal(const std::vector<real_t>& coords, const real_t time) { return initializer_->ExactVal(coords, time); } // for implicit

	virtual inline const std::vector<real_t> calNumConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) { return flux_->numConvFlux(owner_u, neighbor_u, normal); };

	virtual inline const std::vector<real_t> calNumConvFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) { return flux_->numConvFluxOwnerJacobian(owner_u, neighbor_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calNumConvFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& normal) { return flux_->numConvFluxNeighborJacobian(owner_u, neighbor_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calComConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) { return flux_->commonConvFlux(owner_u, normal); };

	virtual inline const std::vector<real_t> calComConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) { return flux_->commonConvFluxJacobian(owner_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calNumBdryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundaryConvFlux(owner_u, normal); };

	virtual inline const std::vector<real_t> calNumBdryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundaryConvFluxJacobian(owner_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calComConvFlux(const std::vector<real_t>& u) { return flux_->commonConvFlux(u); };

	virtual inline const std::vector<real_t> calComConvFluxJacobian(const std::vector<real_t>& u) { return flux_->commonConvFluxJacobian(u); } // for implicit

	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u) { return flux_->source(u); };

	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u, const std::vector<real_t>& div_u) { return flux_->source(u, div_u); };

	virtual inline const std::vector<real_t> calSource(const std::vector<real_t>& u, const std::vector<real_t>& div_u, const std::vector<real_t>& assist) { return flux_->source(u, div_u); };

	virtual inline bool isViscFlux() const { return flux_->isViscFlux(); };

	virtual inline bool isSource() const { return flux_->isSource(); };

	virtual inline const std::vector<real_t> calComViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) { return flux_->commonViscFlux(owner_u, owner_div_u, normal); };

	virtual inline const std::vector<real_t> calComViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal)
	{ return flux_->commonViscFluxJacobian(owner_u, owner_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calComViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal)
	{ return flux_->commonViscFluxGradJacobian(owner_u, owner_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal) { return flux_->numViscFlux(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); };

	virtual inline const std::vector<real_t> calNumViscFluxOwnerJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxOwnerJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxOwnerGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxOwnerGradJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxNeighborJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxNeighborJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumViscFluxNeighborGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u,
		const std::vector<real_t>& neighbor_u, const std::vector<real_t>& neighbor_div_u, const std::vector<real_t>& normal)
	{ return flux_->numViscFluxNeighborGradJacobian(owner_u, owner_div_u, neighbor_u, neighbor_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumBdryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundaryViscFlux(owner_u, owner_div_u, normal); };

	virtual inline const std::vector<real_t> calNumBdryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry)
	{ return boundaries_[ibdry]->calBoundaryViscFluxJacobian(owner_u, owner_div_u, normal); }; // for implicit

	virtual inline const std::vector<real_t> calNumBdryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry)
	{ return boundaries_[ibdry]->calBoundaryViscFluxGradJacobian(owner_u, owner_div_u, normal);	}; // for implicit

	virtual inline const std::vector<real_t> calNumBdrySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundarySolution(owner_u, owner_div_u, normal); };

	virtual inline const std::vector<real_t> calNumBdrySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal, const int_t ibdry) { return boundaries_[ibdry]->calBoundarySolutionJacobian(owner_u, owner_div_u, normal); } // for implicit

	virtual inline const std::vector<real_t> calComViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u) { return flux_->commonViscFlux(owner_u, owner_div_u); };

	virtual inline const std::vector<real_t> calComViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u)
	{ return flux_->commonViscFluxJacobian(owner_u, owner_div_u); }; // for implicit

	virtual inline const std::vector<real_t> calComViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u)
	{ return flux_->commonViscFluxGradJacobian(owner_u, owner_div_u); }; // for implicit

	virtual inline const std::vector<real_t> calConvWaveVelocity(const std::vector<real_t>& u) { return flux_->convWaveVelocity(u); };

	virtual inline const std::vector<real_t> calViscWaveVelocity(const std::vector<real_t>& u) { return flux_->viscWaveVelocity(u); };

	virtual bool applyPressureFix(const std::vector<real_t>& u);

	virtual bool isContact(const std::vector<real_t>& u, const std::vector<real_t>& neighbor_u) const;

	virtual inline const std::vector<real_t> calPressureCoefficients(const std::vector<real_t>& u) { return flux_->calPressureCoefficients(u); };
};


class Euler2DSodTest : public Euler2DInitializer
{
private:
	static Euler2DSodTest Euler_2d_sodtest_;

protected:
	Euler2DSodTest();

	~Euler2DSodTest() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};

class Euler2DShuOsher : public Euler2DInitializer
{
private:
	static Euler2DShuOsher Euler_2d_shuosher_;

protected:
	Euler2DShuOsher();

	~Euler2DShuOsher() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};

class Euler2DExplosion : public Euler2DInitializer
{
private:
	static Euler2DExplosion Euler_2d_explosion_;

protected:
	Euler2DExplosion();

	~Euler2DExplosion() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};

class Euler2DShockVortex : public Euler2DInitializer
{
private:
	static Euler2DShockVortex Euler_2d_shockvortex_;

	real_t Ms_, Mv_, a_, b_, a2_, b2_, xv_, yv_, split_;

	real_t rho1_, u1_, p1_;

	real_t rho2_, u2_, p2_, vm_;

protected:
	Euler2DShockVortex();

	~Euler2DShockVortex() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};

class Euler2DRiemann : public Euler2DInitializer
{
private:
	static Euler2DRiemann Euler_2d_riemann_;

protected:
	Euler2DRiemann();

	~Euler2DRiemann() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};

class Euler2DFreeStream : public Euler2DInitializer
{
private:
	static Euler2DFreeStream Euler_2d_freestream_;

	real_t Ma_;

	real_t AoA_;

protected:
	Euler2DFreeStream();

	~Euler2DFreeStream() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name);

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};

class Euler2DUserShockTube : public Euler2DInitializer
{
private:
	static Euler2DUserShockTube Euler_2d_user_shocktube_;

	real_t split_;

	std::vector<real_t> UL_;

	std::vector<real_t> UR_;

protected:
	Euler2DUserShockTube();

	~Euler2DUserShockTube() {};

protected:
	virtual void readData(const std::string& region, const std::string& data_file_name);

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};


class Euler2DDoubleMach : public Euler2DInitializer
{
private:
	static Euler2DDoubleMach Euler_2d_double_mach_;

protected:
	Euler2DDoubleMach();

	~Euler2DDoubleMach() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};


class Euler2DShockWedge : public Euler2DInitializer
{
private:
	static Euler2DShockWedge Euler_2d_shock_wedge_;

protected:
	Euler2DShockWedge();

	~Euler2DShockWedge() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};


class Euler2DVortexTransport : public Euler2DInitializer
{
private:
	static Euler2DVortexTransport Euler_2d_vortex_transport_;

protected:
	Euler2DVortexTransport();

	~Euler2DVortexTransport() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};

class Euler2DDoubleSine : public Euler2DInitializer
{
private:
	static Euler2DDoubleSine Euler_2d_double_sine_;

protected:
	Euler2DDoubleSine();

	~Euler2DDoubleSine() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};

class Euler2DTravellingWaves : public Euler2DInitializer
{
private:
	static Euler2DTravellingWaves Euler_2d_travelling_waves_;

protected:
	Euler2DTravellingWaves();

	~Euler2DTravellingWaves() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};


class Euler2DUserDefine : public Euler2DInitializer
{
private:
	static Euler2DUserDefine Euler_2d_user_define_;

protected:
	Euler2DUserDefine();

	~Euler2DUserDefine() {};

public:
	virtual const std::vector<real_t> Problem(const std::vector<real_t>& coord) const;

	virtual const std::vector<real_t> ExactVal(const std::vector<real_t>& coord, const real_t time) const; // for implicit
};