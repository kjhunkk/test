#pragma once

#include "TimeUnsteady.h"
#include "LinearSystemSolver.h"

class TimeUnstImpBDF2 : public TimeUnsteady // for implicit
{
private:
	static TimeUnstImpBDF2 ustd_imp_bdf2_;

protected:
	TimeUnstImpBDF2();

	virtual ~TimeUnstImpBDF2();

public:
	virtual void initialize();

	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);

protected:
	void InitializeVectors(std::shared_ptr<Zone> zone, const real_t dt);

	inline void ConvertSolutionToPetsc(const std::vector<std::vector<real_t> >& solution, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, Vec& vector);

	inline void ConvertPetscToSolution(std::shared_ptr<Zone> zone, const Vec& vector, const int_t num_cells, const std::vector<int_t>& cell_petsc_index, std::vector<std::vector<real_t> >& solution);

	void SelfStartingProcedure(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);

protected:
	// Time integration configuration
	real_t diagonal_factor_;

	Vec delta_;

	Vec intermediate_solution_vector_;

	std::vector<std::vector<real_t> > intermediate_solution_;

	Vec rhs_;

	Vec base_rhs_;

	std::array<Vec, 2> solution_array_;

	Mat system_matrix_;

	// Non-linear solver configuration
	int_t max_newton_iteration_;

	real_t newton_tolerance_;

	real_t recompute_tol_;

	// Linear solver configuration
	LinearSystemSolver* linear_system_solver_;

	// etc
	bool is_initialized_;
};