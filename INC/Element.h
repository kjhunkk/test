#pragma once

#include <vector>
#include <string>
#include <fstream>

#include "MPIHeader.h"
#include "DataType.h"
#include "../LIB/armadillo"

#define ARMA_USE_LAPACK
#define ARMA_USE_BLAS

class Element;

class ElementFactory
{
public:
	ElementFactory();

	~ElementFactory();

	static ElementFactory& getInstance();

	Element* getElement(ElemType element_type);

private:
	Element* newElement(ElemType element_type);

private:
	std::vector<Element*> registry_;
};

class Element
{
protected:
	class ElementMath{
	public:
		static real_t Normalization(std::vector<real_t>& vec);

		static real_t Length(const std::vector<real_t>& vec);

		static real_t DotProduct(const std::vector<real_t>& vec1, const std::vector<real_t>& vec2);

		static real_t TrisFaceArea(const std::vector<real_t>& coords, const int_t dimension);

		static real_t FaceArea(const std::vector<real_t>& coords, const int_t dimension);

		static const std::vector<real_t> CrossProduct(const std::vector<real_t>& vec1, const std::vector<real_t>& vec2);

		static const std::vector<real_t> NormalVector(const std::vector<real_t>& coords, const std::vector<real_t>& inside, const int_t dimension);

		static const std::vector<std::vector<int_t>> GenerateFacetype(const std::vector<int_t>& node_index);

		static bool isPlane(const std::vector<real_t>& face_coords, const real_t* coord, const int_t dimension);

		static bool isSamePoint(const real_t* coord1, const real_t* coord2, const int_t dimension);

		static const std::vector<real_t> LineTransformation(const std::vector<real_t>& line_coords, const real_t* coord);

		static const std::vector<real_t> TrisTransformation(const std::vector<real_t>& tris_coords, const real_t* coord);

		static const std::vector<real_t> QuadTransformation(const std::vector<real_t>& quad_coords, const real_t* coord);
	};
protected:
	// user define
	int_t dimension_;

	std::string element_name_;

	ElemType element_type_;

	int_t element_order_supp_;

	int_t num_faces_;

	std::vector<std::vector<int_t>> face_nodes_P1_;

	// file read
	std::vector<int_t> num_nodes_Pn_; // by order

	std::vector<std::vector<real_t>> node_coords_Pn_; // by order

	// compute
	real_t volume_;

	std::vector<real_t> face_areas_;

	std::vector<real_t> normals_;

	std::vector<ElemType> face_element_type_;

	int_t num_facetypes_;

	std::vector<int_t> facetype_to_face_;

	std::vector<std::vector<std::vector<int_t>>> facetype_nodes_Pn_; // by order

	std::vector<std::vector<int_t>> simplex_nodes_index_; // for hMLPBD_SIM

public:
	Element() {};

	~Element() {};

public:
	inline int_t getDimension() const { return dimension_; };

	inline ElemType getElemType() const { return element_type_; };

	inline const std::string& getElementName() const { return element_name_; };

	inline int_t getElementOrderSupport() const { return element_order_supp_; };

	inline int_t getNumFaces() const { return num_faces_; };

	inline const std::vector<std::vector<int_t>>& getFaceNodes() const { return face_nodes_P1_; };

	inline int_t getNumNodes(const int_t element_order) const { return num_nodes_Pn_[element_order - 1]; };

	inline const std::vector<real_t>& getNodesCoord(const int_t element_order) const { return node_coords_Pn_[element_order - 1]; };

	inline real_t getVolume() const { return volume_; };

	inline real_t getFaceArea(const int_t facetype) const { return face_areas_[facetype_to_face_[facetype]]; };

	inline const real_t* getNormal(const int_t facetype) const { return &normals_[dimension_*facetype_to_face_[facetype]]; };

	inline ElemType getFaceElemType(const int_t facetype) const { return face_element_type_[facetype_to_face_[facetype]]; };

	inline int_t getNumFacetypes() const { return num_facetypes_; };

	inline const std::vector<std::vector<int_t>>& getFacetypeNodes(const int_t element_order) const { return facetype_nodes_Pn_[element_order - 1]; };

	inline const std::vector<std::vector<int_t>>& getSimplexNodesIndex() const { return simplex_nodes_index_; };

	virtual inline int_t getNumForTypeOne() const = 0; // for hMLP_BD

	virtual inline int_t getNumForTypeTwo() const = 0; // for hMLP_BD
};

class LineElement : public Element
{
public:
	LineElement();

	~LineElement() {};

public:
	virtual inline int_t getNumForTypeOne() const { return 0; }; // for hMLP_BD

	virtual inline int_t getNumForTypeTwo() const { return 0; }; // for hMLP_BD
};

class TrisElement : public Element
{
public:
	TrisElement();

	~TrisElement() {};

public:
	virtual inline int_t getNumForTypeOne() const { return 2; }; // for hMLP_BD

	virtual inline int_t getNumForTypeTwo() const { return 1; }; // for hMLP_BD
};

class QuadElement : public Element
{
public:
	QuadElement();

	~QuadElement() {};

public:
	virtual inline int_t getNumForTypeOne() const { return 2; }; // for hMLP_BD

	virtual inline int_t getNumForTypeTwo() const { return 1; }; // for hMLP_BD
};

class TetsElement : public Element
{
public:
	TetsElement();

	~TetsElement() {};

public:
	virtual inline int_t getNumForTypeOne() const { return 2; }; // for hMLP_BD

	virtual inline int_t getNumForTypeTwo() const { return 1; }; // for hMLP_BD
};

class HexaElement : public Element
{
public:
	HexaElement();

	~HexaElement() {};

public:
	virtual inline int_t getNumForTypeOne() const { return 4; }; // for hMLP_BD // not tuned yet

	virtual inline int_t getNumForTypeTwo() const { return 1; }; // for hMLP_BD // not tuned yet
};

class PrisElement : public Element
{
public:
	PrisElement();

	~PrisElement() {};

public:
	virtual inline int_t getNumForTypeOne() const { return 4; }; // for hMLP_BD // not tuned yet

	virtual inline int_t getNumForTypeTwo() const { return 1; }; // for hMLP_BD // not tuned yet
};

class PyraElement : public Element
{
public:
	PyraElement();

	~PyraElement() {};

public:
	virtual inline int_t getNumForTypeOne() const { return 4; }; // for hMLP_BD // not tuned yet

	virtual inline int_t getNumForTypeTwo() const { return 1; }; // for hMLP_BD // not tuned yet
};