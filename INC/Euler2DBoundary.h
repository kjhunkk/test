
#pragma once

#include <vector>
#include <memory>
#include <algorithm>
#include <string>

#include "DataType.h"
#include "MPIHeader.h"
#include "Boundary.h"
#include "Euler2DFlux.h"

class Euler2DBoundary : public Boundary
{
protected:
	Euler2DFlux* flux_;

	const int_t num_states_ = 4;

	const int_t dimension_ = 2;

	real_t alpha_;

	real_t beta_;

public:
	Euler2DBoundary() {};

	~Euler2DBoundary() {};

public:
	void setFlux(Euler2DFlux* flux)	{ flux_ = flux; alpha_ = flux_->getAlpha();	beta_ = flux_->getBeta(); }; // for implicit

	virtual Euler2DBoundary* clone() = 0;

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const = 0; // for implicit
	// calculate jacobian of 0.5*(u(+) - u(-))
};

class Euler2DBCExtrapolate : public Euler2DBoundary
{
private:
	static Euler2DBCExtrapolate Euler_2d_extrapolate_;

public:
	Euler2DBCExtrapolate() { type_ = TO_UPPER("Extrapolate"); BoundaryFactory<Euler2DBoundary>::getInstance().addClone(this); };

	~Euler2DBCExtrapolate() {};

public:
	virtual Euler2DBoundary* clone() { return new Euler2DBCExtrapolate; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class Euler2DBCConstant : public Euler2DBoundary
{
private:
	static Euler2DBCConstant Euler_2d_constant_;

public:
	Euler2DBCConstant() { type_ = TO_UPPER("Constant"); BoundaryFactory<Euler2DBoundary>::getInstance().addClone(this); };

	~Euler2DBCConstant() {};

public:
	virtual Euler2DBoundary* clone() { return new Euler2DBCConstant; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class Euler2DBCWall : public Euler2DBoundary
{
private:
	static Euler2DBCWall Euler_2d_wall_;

public:
	Euler2DBCWall() { type_ = TO_UPPER("Wall"); BoundaryFactory<Euler2DBoundary>::getInstance().addClone(this); };

	~Euler2DBCWall() {};

public:
	virtual Euler2DBoundary* clone() { return new Euler2DBCWall; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class Euler2DBCSlipWall : public Euler2DBoundary
{
private:
	static Euler2DBCSlipWall Euler_2d_slipwall_;

public:
	Euler2DBCSlipWall() { type_ = TO_UPPER("SlipWall"); BoundaryFactory<Euler2DBoundary>::getInstance().addClone(this); };

	~Euler2DBCSlipWall() {};

public:
	virtual Euler2DBoundary* clone() { return new Euler2DBCSlipWall; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class Euler2DBCSubOut : public Euler2DBoundary
{
private:
	static Euler2DBCSubOut Euler_2d_subout_;

public:
	Euler2DBCSubOut() { type_ = TO_UPPER("SubOut"); BoundaryFactory<Euler2DBoundary>::getInstance().addClone(this); };

	~Euler2DBCSubOut() {};

public:
	virtual Euler2DBoundary* clone() { return new Euler2DBCSubOut; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class Euler2DBCSubIn : public Euler2DBoundary
{
private:
	static Euler2DBCSubIn Euler_2d_subin_;

public:
	Euler2DBCSubIn() { type_ = TO_UPPER("SubIn"); BoundaryFactory<Euler2DBoundary>::getInstance().addClone(this); };

	~Euler2DBCSubIn() {};

public:
	virtual Euler2DBoundary* clone() { return new Euler2DBCSubIn; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class Euler2DBCSupOut : public Euler2DBoundary
{
private:
	static Euler2DBCSupOut Euler_2d_supout_;

public:
	Euler2DBCSupOut() { type_ = TO_UPPER("SupOut"); BoundaryFactory<Euler2DBoundary>::getInstance().addClone(this); };

	~Euler2DBCSupOut() {};

public:
	virtual Euler2DBoundary* clone() { return new Euler2DBCSupOut; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class Euler2DBCSupIn : public Euler2DBoundary
{
private:
	static Euler2DBCSupIn Euler_2d_supin_;

public:
	Euler2DBCSupIn() { type_ = TO_UPPER("SupIn"); BoundaryFactory<Euler2DBoundary>::getInstance().addClone(this); };

	~Euler2DBCSupIn() {};

public:
	virtual Euler2DBoundary* clone() { return new Euler2DBCSupIn; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
	
	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};

class Euler2DBCFarField : public Euler2DBoundary
{
private:
	static Euler2DBCFarField Euler_2d_farfield_;

public:
	Euler2DBCFarField() { type_ = TO_UPPER("FarField"); BoundaryFactory<Euler2DBoundary>::getInstance().addClone(this); };

	~Euler2DBCFarField() {};

public:
	virtual Euler2DBoundary* clone() { return new Euler2DBCFarField; };

	virtual const std::vector<real_t> calBoundaryConvFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryConvFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFlux(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundaryViscFluxJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundaryViscFluxGradJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit

	virtual const std::vector<real_t> calBoundarySolution(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const;

	virtual const std::vector<real_t> calBoundarySolutionJacobian(const std::vector<real_t>& owner_u, const std::vector<real_t>& owner_div_u, const std::vector<real_t>& normal) const; // for implicit
};