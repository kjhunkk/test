#pragma once

#include <string>
#include <vector>
#include <cmath>
#include <array>
#include <unordered_map>

#include "../LIB/parmetis.h"

#include <fstream>

#include "DataType.h"
#include "MPIHeader.h"
#include "GridReader.h"
#include "Element.h"

class GridBuilder
{
	friend class Grid;

private:
	int_t dimension_;

	int_t num_global_nodes_;
	int_t num_total_nodes_;
	int_t num_nodes_;
	std::vector<int_t> node_dist_;
	std::vector<real_t> node_coords_;
	std::vector<std::vector<int_t>> peribdry_node_;
	std::vector<std::vector<int_t>> peribdry_node_to_matching_;
	std::vector<int_t> node_global_index_;
	std::vector<std::vector<int_t>> node_to_cells_;
	std::vector<std::vector<int_t>> node_to_vertex_;

	int_t num_global_cells_;
	int_t num_total_cells_;
	int_t num_cells_;
	std::vector<int_t> cell_dist_;
	std::vector<int_t> cell_to_node_ptr_;
	std::vector<int_t> cell_to_node_ind_;
	std::vector<int_t> cell_elem_type_;
	std::vector<int_t> cell_order_;
	std::vector<int_t> cell_to_subnode_ptr_;
	std::vector<int_t> cell_to_subnode_ind_;
	std::vector<int_t> cell_color_;
	std::vector<int_t> cell_global_index_;
	std::vector<std::vector<int_t>> cell_to_cells_;
	std::vector<std::vector<int_t>> cell_to_faces_;

	std::vector<int_t> num_send_cells_;
	std::vector<int_t> num_recv_cells_;
	std::vector<std::vector<int_t>> send_cell_list_;
	std::vector<std::vector<int_t>> recv_cell_list_;

	int_t num_global_bdries_;
	int_t num_total_bdries_;
	int_t num_bdries_;
	std::vector<int_t> bdry_dist_;
	std::vector<int_t> bdry_to_node_ptr_;
	std::vector<int_t> bdry_to_node_ind_;
	std::vector<int_t> bdry_type_;
	std::vector<int_t> bdry_color_;
	std::vector<int_t> bdry_owner_cell_;
	std::vector<int_t> bdry_owner_type_;

	int_t num_global_peribdries_;
	int_t num_total_peribdries_;
	int_t num_peribdries_;
	std::vector<int_t> peribdry_dist_;
	std::vector<int_t> peribdry_to_node_ptr_;
	std::vector<int_t> peribdry_to_node_ind_;
	std::vector<int_t> peribdry_direction_;
	std::vector<int_t> peribdry_color_;
	std::vector<int_t> peribdry_owner_cell_;
	std::vector<int_t> peribdry_neighbor_cell_;
	std::vector<int_t> peribdry_to_matching_node_ind_;
	std::vector<int_t> peribdry_owner_type_;
	std::vector<int_t> peribdry_neighbor_type_;

	int_t num_total_faces_;
	int_t num_faces_;
	std::vector<int_t> face_to_node_ptr_;
	std::vector<int_t> face_to_node_ind_;
	std::vector<int_t> face_owner_cell_;
	std::vector<int_t> face_neighbor_cell_;
	std::vector<int_t> face_owner_type_;
	std::vector<int_t> face_neighbor_type_;

	std::vector<int_t> cell_petsc_index_; // for implicit
	std::vector<int_t> domain_label_; // for implicit

public:
	GridBuilder() {};

	~GridBuilder() {};

public:
	void MakeGrid(const std::string& file_format, const std::string& file_name, const int_t dimension);

private:
	void readGridFileParallel(const std::string& file_format, const std::string& file_name);

	void setCellColor(void);

	void redistributeCellData(void);

	void periodicMatching(void);

	void constructVirtualCellData(void);

	void redistributeBdryData(void);

	void redistributeNodeData(void);

	void constructFaceData(void);

	void constructNodeToCellsData(void);

	void test(void);

	bool isMatch(const real_t* coords1, const real_t* coords2, const int_t direction);

	void communicatePeribdryNode(std::vector<std::vector<int_t>>& peribdry_node, std::vector<std::vector<int_t>>& peribdry_node_to_matching);

	bool isParent(const int_t* cell_node_index, const int_t cell_num_nodes, const int_t* face_node_index, const int_t face_num_nodes);

	bool isSame(const int_t* index1, const int_t num_index1, const int_t* index2, const int_t num_index2);

	int_t findFaceType(const std::vector<std::vector<int_t>>& face_type_nodes_index, const int_t* cell_nodes_index, const int_t* face_nodes_index, const int_t num_face_nodes);

	void petscReordering(void); // for implicit

	void petscReordering2(void); // for implicit(test)
};