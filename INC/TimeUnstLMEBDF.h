#pragma once

#include "TimeUnsteady.h"
#include "LinearSystemSolver.h"

#define VECTOR_ARRAY_CREATOR(arr, ierr) \
	for (int_t i = 0; i < arr.size(); i++) \
		{ ierr = VecCreate(MPI_COMM_WORLD, &arr[i]); }

#define VECTOR_ARRAY_DESTROYER(arr, ierr) \
	for (int_t i = 0; i < arr.size(); i++) \
		{ ierr = VecDestroy(&arr[i]); }

class TimeUnstLMEBDF : public TimeUnsteady // for implicit
{
private:
	static TimeUnstLMEBDF unst_imp_lmebdf_;

protected:
	TimeUnstLMEBDF();

	virtual ~TimeUnstLMEBDF();

public:
	virtual void initialize();

	virtual void integrate(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);

	void integrate2(std::shared_ptr<Zone> zone, const real_t dt, real_t& current_time, int_t& iteration);

	inline int_t getTimeOrder() const { return time_order_; }

	inline void setTimeOrder(const int_t time_order) { time_order_ = time_order; }

protected:
	void InitializeVectors(std::shared_ptr<Zone> zone, const real_t dt);

	int_t SolveStage1(std::shared_ptr<Zone> zone, const real_t diagonal_factor, Mat system_matrix);

	int_t SolveStage2(std::shared_ptr<Zone> zone, const real_t diagonal_factor, Mat system_matrix);

	int_t SolveStage3(std::shared_ptr<Zone> zone, const real_t diagonal_factor, Mat system_matrix);

	void InitializeCoeff();

	const Mat& UpdateSystemMatrix(std::shared_ptr<Zone> zone, const real_t diagonal_factor, const real_t beta);

	void ConvertSolutionArray(Vec& vector, const int_t num_cells, const std::vector<std::vector<real_t> >& solution, const std::vector<int_t>& cell_petsc_index);

	void UpdateHistoryArray(std::vector<Vec>& history_array, const int_t num_cells, const std::vector<std::vector<real_t> >& solution, const std::vector<int_t>& cell_petsc_index);

	void RescaleHistoryArray(const real_t eta);

	void Adaptation();

	void GenerateInitialHistory(std::shared_ptr<Zone> zone, const real_t dt);

protected:
	LinearSystemSolver * linear_system_solver_;

	int_t time_order_;

	int_t current_time_order_;

	int_t max_newton_iteration_;

	real_t newton_tolerance_;

	int_t recompute_step_;

	real_t recompute_tol_;

	real_t old_physical_dt_;

	bool is_initialized_;

	PetscErrorCode ierr_;

	std::array<real_t, 2> WRMS_norm_;

	std::array<real_t, 2> MEBDF_beta_;

	std::vector<real_t> BDF_gamma_;

	std::vector<real_t> MEBDF_gamma_;

	real_t BDF_beta_;

	Vec delta_;

	Vec MEBDF_rhs_;

	std::vector<Vec> history_array_;

	std::vector<Vec> inter_history_array_;

	std::array<Vec, 2> stage_rhs_;

	std::array<Vec, 3> solution_array_;
};