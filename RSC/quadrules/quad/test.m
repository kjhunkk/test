%%
clear all
clc

title = 'gauss-legendre-lobatto-n9-d3-spu.txt'
fid = fopen(title);
quad = textscan(fid,'%f %f %f')

syms r s

index = 1;
for ri = 0:10
    for si = 0:10
        target = r^ri*s^si;
        exact = double(int(int(target,r,-1,1),s,-1,1));
        app = 0;
        for k = 1:length(quad{1})
            app = app + subs(subs(target,r,quad{1}(k)),s,quad{2}(k))*quad{3}(k);
        end
        results(index,1:5) = [ri, si, exact, app, exact - app];
        results_2(ri+1,si+1) = double(exact - app);
        index = index + 1;
    end
end
results_2